.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendConversationMessageMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1663
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1664
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1745
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1753
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1668
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1676
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;II)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1728
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMediaId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSize(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1738
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1715
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMediaId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1724
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;IIJIZ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1697
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setThumbnailPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSize(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p9}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setVideoRotation(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p10}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setRecorderAblePlayback(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1711
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x763f

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setThumbnailPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setWidth(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setHeight(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1692
    return-void
.end method
