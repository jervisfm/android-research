.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SnsAuthResultMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 2320
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7600

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setSuccess(Z)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setAccessToken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setExpire(I)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setRefreshToken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->setPinCode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SnsAuthResultPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2328
    return-void
.end method
