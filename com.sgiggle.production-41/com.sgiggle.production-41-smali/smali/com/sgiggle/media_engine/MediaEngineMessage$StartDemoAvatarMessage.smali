.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$StartDemoAvatarMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartDemoAvatarMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(J)V
    .locals 2
    .parameter

    .prologue
    .line 2604
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7628

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartDemoAvatarMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->setAvatarid(J)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2608
    return-void
.end method
