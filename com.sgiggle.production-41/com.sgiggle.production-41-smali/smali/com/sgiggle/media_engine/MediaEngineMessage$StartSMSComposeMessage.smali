.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StartSMSComposeMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1909
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7649

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setUserChooseToSend(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->addReceivers(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setInfo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1916
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLjava/util/List;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1883
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7649

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setInfo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setUserChooseToSend(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->addAllReceivers(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1890
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLjava/util/List;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
            "Z",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1895
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7649

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setInfo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setUserChooseToSend(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->addAllReceivers(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setAskToSendSms(Z)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setDialogTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->setDialogMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1905
    return-void
.end method
