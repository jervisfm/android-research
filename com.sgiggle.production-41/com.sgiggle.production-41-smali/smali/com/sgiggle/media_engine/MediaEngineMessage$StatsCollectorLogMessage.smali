.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StatsCollectorLogMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2708
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7666

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->addAllParameters(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StatsCollectorLogPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2712
    return-void
.end method
