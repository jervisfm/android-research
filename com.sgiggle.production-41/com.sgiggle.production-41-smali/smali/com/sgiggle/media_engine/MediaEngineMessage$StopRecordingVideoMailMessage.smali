.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "StopRecordingVideoMailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2013
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d7

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2017
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V
    .locals 2
    .parameter

    .prologue
    .line 2019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75d7

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2023
    return-void
.end method
