.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TangoDeviceTokenMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1242
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1243
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 1246
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7588

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setDevicetoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_ANDROID:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->setDevicetokentype(Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;)Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$TangoDeviceTokenPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1251
    return-void
.end method
