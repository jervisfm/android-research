.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$TestMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TestMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$TestPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2720
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2721
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 2724
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7594

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$TestMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->setFoo(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->setBar(I)Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$TestPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2729
    return-void
.end method
