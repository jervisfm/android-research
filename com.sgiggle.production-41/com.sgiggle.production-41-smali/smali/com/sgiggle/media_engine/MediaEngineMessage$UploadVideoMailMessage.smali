.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadVideoMailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2036
    const-string v4, ""

    const-string v7, ""

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    move-wide v5, v2

    move v9, v1

    move v10, v1

    invoke-direct/range {v0 .. v10}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailMessage;-><init>(IJLjava/lang/String;JLjava/lang/String;Ljava/util/List;IZ)V

    .line 2037
    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;JLjava/lang/String;Ljava/util/List;IZ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IJ",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .line 2040
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x75da

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setSize(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setMime(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setContent(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p9}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setRotation(I)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p10}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->setFlip(Z)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p8}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->addAllCallees(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2051
    return-void
.end method
