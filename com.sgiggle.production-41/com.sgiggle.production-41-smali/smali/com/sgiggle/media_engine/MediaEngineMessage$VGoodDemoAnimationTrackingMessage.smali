.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VGoodDemoAnimationTrackingMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 2564
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    const/16 v1, 0x761a

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->setProductMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->setVgoodId(J)Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodInitiatePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2569
    return-void
.end method
