.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewContactDetailMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 2
    .parameter

    .prologue
    .line 2375
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x760a

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2379
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 2381
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    const/16 v1, 0x760a

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 2386
    return-void
.end method
