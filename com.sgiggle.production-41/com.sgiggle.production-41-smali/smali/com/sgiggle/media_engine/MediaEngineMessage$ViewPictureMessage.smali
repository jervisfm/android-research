.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewPictureMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3237
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3238
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 3241
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    const/16 v1, 0x7658

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 3249
    return-void
.end method
