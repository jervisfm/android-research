.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;
.super Lcom/sgiggle/messaging/SerializableMessage;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WandPressedMessage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/messaging/SerializableMessage",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;)V
    .locals 2
    .parameter

    .prologue
    .line 1273
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;

    move-result-object v0

    const/16 v1, 0x7616

    invoke-static {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;->makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;->setWandLocation(Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;)Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$WandPressedPayload;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/SerializableMessage;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    .line 1277
    return-void
.end method
