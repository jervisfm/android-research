.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$event;
.super Ljava/lang/Object;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "event"
.end annotation


# static fields
.field public static final AUDIO_2WAY_AVATAR_IN_PROGRESS_TYPE:I = 0x89ac

.field public static final AUDIO_AVATAR_IN_PROGRESS_TYPE:I = 0x89aa

.field public static final AUDIO_IN_INTIALIZATION_EVENT:I = 0x88ff

.field public static final AUDIO_IN_PROGRESS_EVENT:I = 0x88cf

.field public static final AUDIO_MODE_CHANGED_EVENT:I = 0x890b

.field public static final AUDIO_VIDEO_2WAY_IN_PROGRESS_EVENT:I = 0x88fd

.field public static final AUDIO_VIDEO_IN_PROGRESS_EVENT:I = 0x88d1

.field public static final AVATAR_ERROR_TYPE:I = 0x89b2

.field public static final AVATAR_RENDER_REQUEST_TYPE:I = 0x89ad

.field public static final BADGE_INVITE_EVENT:I = 0x89c3

.field public static final BADGE_STORE_EVENT:I = 0x89c1

.field public static final CALLEE_MISSED_CALL_EVENT:I = 0x8929

.field public static final CALL_DISCONNECTING_EVENT:I = 0x890d

.field public static final CALL_ERROR_EVENT:I = 0x88cb

.field public static final CALL_RECEIVED_EVENT:I = 0x88c9

.field public static final CANCEL_CHOOSE_PICTURE_EVENT:I = 0x125

.field public static final CANCEL_POST_PROCESS_PICTURE_EVENT:I = 0x89df

.field public static final CANCEL_TAKE_PICTURE_EVENT:I = 0x89db

.field public static final CANCEL_UPLOAD_VIDEO_MAIL_RESPONSE_EVENT:I = 0x8963

.field public static final CANCEL_VIEW_PICTURE_EVENT:I = 0x89e2

.field public static final CHOOSE_PICTURE_EVENT:I = 0x89dc

.field public static final CONTACTS_DISPLAY_MAIN_EVENT:I = 0x88e7

.field public static final CONTACT_SEARCH_RESULT_FOUND_SELF_EVENT:I = 0x892b

.field public static final CONTACT_TANGO_CUSTOMER_SUPPORT_EVENT:I = 0x891f

.field public static final CONVERSATION_MESSAGE_SEND_STATUS_EVENT:I = 0x89cc

.field public static final DELETE_CONVERSATION_EVENT:I = 0x89ca

.field public static final DELETE_SINGLE_CONVERSATION_MESSAGE_EVENT:I = 0x89d6

.field public static final DISPLAY_ALERT_NUMBERS_EVENT:I = 0x892e

.field public static final DISPLAY_ANIMATION_EVENT:I = 0x88d9

.field public static final DISPLAY_APPSTORE_UI_EVENT:I = 0x898e

.field public static final DISPLAY_APP_LOG_EVENT:I = 0x892c

.field public static final DISPLAY_AVATAR_PAYMENT_UI_TYPE:I = 0x89b1

.field public static final DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE:I = 0x89ae

.field public static final DISPLAY_AVATAR_PRODUCT_DETAILS_TYPE:I = 0x89af

.field public static final DISPLAY_CALL_LOG_EVENT:I = 0x8914

.field public static final DISPLAY_CALL_QUALITY_SURVEY_UI_TYPE:I = 0x89a8

.field public static final DISPLAY_CONTACT_DETAIL_UI_EVENT:I = 0x8990

.field public static final DISPLAY_CONVERSATION_MESSAGE_EVENT:I = 0x89c7

.field public static final DISPLAY_DEMO_AVATAR_TYPE:I = 0x89b0

.field public static final DISPLAY_FACEBOOK_LIKE_UI_EVENT:I = 0x898f

.field public static final DISPLAY_FAQ_EVENT:I = 0x88fb

.field public static final DISPLAY_GAME_CATALOG_EVENT:I = 0x89e4

.field public static final DISPLAY_GAME_DEMO_EVENT:I = 0x89e5

.field public static final DISPLAY_GAME_IN_CALL_EVENT:I = 0x89e6

.field public static final DISPLAY_INVITE_CONTACT_EVENT:I = 0x8913

.field public static final DISPLAY_MESSAGE_NOTIFICATION_EVENT:I = 0x8924

.field public static final DISPLAY_POSTCALL_UI_EVENT:I = 0x8977

.field public static final DISPLAY_PRODUCT_DETAILS_EVENT:I = 0x8998

.field public static final DISPLAY_PRODUCT_INFO_UI_EVENT:I = 0x8979

.field public static final DISPLAY_REGISTER_USER_EVENT:I = 0x88d7

.field public static final DISPLAY_SETTINGS_EVENT:I = 0x88e9

.field public static final DISPLAY_SMS_SENT_EVENT:I = 0x892f

.field public static final DISPLAY_STORE_EVENT:I = 0x89c0

.field public static final DISPLAY_SUPPORT_WEBSITE_EVENT:I = 0x8934

.field public static final DISPLAY_TANGO_PUSH_CALL_CANCEL_TYPE:I = 0x89ef

.field public static final DISPLAY_TANGO_PUSH_CALL_TYPE:I = 0x89ee

.field public static final DISPLAY_VGOOD_DEMO_SCREEN_EVENT:I = 0x897c

.field public static final DISPLAY_VGOOD_PAYMENT_UI_EVENT:I = 0x8975

.field public static final DISPLAY_VGOOD_PRODUCT_CATALOG_EVENT:I = 0x8997

.field public static final DISPLAY_VGREETING_RECIPIENTS_EVENT:I = 0x89bf

.field public static final DISPLAY_VIDEO_MAIL_NON_TANGO_NOTIFICATION_EVENT:I = 0x895a

.field public static final DISPLAY_VIDEO_MAIL_RECEIVERS_EVENT:I = 0x8959

.field public static final EDIT_VIDEO_MAIL_EVENT:I = 0x8950

.field public static final EMAIL_CONTACT_SEARCH_EVENT:I = 0x8920

.field public static final EMAIL_CONTACT_SEARCH_RESULT_EVENT:I = 0x8922

.field public static final EMAIL_VALIDATION_REQUIRED_EVENT:I = 0x88ec

.field public static final EMAIL_VALIDATION_WRONG_CODE_EVENT:I = 0x8933

.field public static final ENTER_BACKGROUND_EVENT:I = 0x8928

.field public static final FB_DID_LOGIN_EVENT:I = 0x89f8

.field public static final FINISH_PLAY_VIDEO_MAIL_EVENT:I = 0x896f

.field public static final FORWARD_MESSAGE_RESULT_EVENT:I = 0x89d0

.field public static final GAME_ERROR_EVENT:I = 0x899a

.field public static final INVITE_DISPLAY_MAIN_EVENT:I = 0x88df

.field public static final INVITE_EMAIL_COMPOSER_EVENT:I = 0x88f1

.field public static final INVITE_EMAIL_SELECTION_EVENT:I = 0x88e1

.field public static final INVITE_RECOMMENDED_SELECTION_EVENT:I = 0x88f4

.field public static final INVITE_SMS_INSTRUCTION_EVENT:I = 0x88f5

.field public static final INVITE_SMS_SELECTION_EVENT:I = 0x88f3

.field public static final INVITE_SNS_COMPOSER_EVENT:I = 0x8991

.field public static final INVITE_SNS_PUBLISH_RESULT_EVENT:I = 0x8992

.field public static final IN_CALL_ALERT_EVENT:I = 0x8908

.field public static final LAUNCH_LOG_REPORT_EMAIL_EVENT:I = 0x88dc

.field public static final LOAD_VGREETING_PLAYER_TYPE:I = 0x89c2

.field public static final LOGIN_COMPLETED_EVENT:I = 0x88c3

.field public static final LOGIN_ERROR_EVENT:I = 0x88b9

.field public static final LOGIN_EVENT:I = 0x88bd

.field public static final LOGIN_NOTIFICATION_EVENT:I = 0x88bc

.field public static final LOGIN_NOTIFICATION_USER_ACCEPTED_EVENT:I = 0x88bb

.field public static final MAKE_PREMIUM_CALL_EVENT:I = 0x88bf

.field public static final MISSED_CALL_EVENT:I = 0x8901

.field public static final NETWORK_HIGH_BANDWIDTH_EVENT:I = 0x8907

.field public static final NETWORK_LOW_BANDWIDTH_EVENT:I = 0x8905

.field public static final OPEN_CONVERSATION_EVENT:I = 0x89c6

.field public static final OPEN_CONVERSATION_LIST_EVENT:I = 0x89c8

.field public static final PAUSE_PLAY_VIDEO_MAIL_EVENT:I = 0x896e

.field public static final PHONENUMBER_CONTACT_SEARCH_EVENT:I = 0x8921

.field public static final PHONENUMBER_CONTACT_SEARCH_RESULT_EVENT:I = 0x8923

.field public static final PHONE_FORMATTED_EVENT:I = 0x890f

.field public static final PLAY_MESSAGE_ERROR_EVENT:I = 0x89ce

.field public static final PLAY_VIDEO_MAIL_EVENT:I = 0x896c

.field public static final PLAY_VIDEO_MESSAGE_EVENT:I = 0x89cb

.field public static final POST_PROCESS_PICTURE_EVENT:I = 0x89de

.field public static final PROCESS_NOTIFICATION_EVENT:I = 0x88c1

.field public static final PRODUCT_CATALOG_EVENT:I = 0x8982

.field public static final PRODUCT_CATALOG_WAITING_EVENT:I = 0x8981

.field public static final PRODUCT_GREETING_WAITING_EVENT:I = 0x89be

.field public static final PUSH_VALIDATION_REQUIRED_EVENT:I = 0x88ee

.field public static final QUERY_LEAVE_MESSAGE_EVENT:I = 0x89cf

.field public static final READ_STATUS_UPDATE_EVENT:I = 0x89f9

.field public static final RECORD_VIDEO_MAIL_EVENT:I = 0x895e

.field public static final REGISTER_USER_NO_NETWORK_EVENT:I = 0x8912

.field public static final REPORT_PURCHASE_RESULT_EVENT:I = 0x8980

.field public static final RETRIEVE_OFFLINE_MESSAGE_RESULT_EVENT:I = 0x89d4

.field public static final SEARCH_AND_ADD_MAIN_EVENT:I = 0x8916

.field public static final SELECT_CONTACT_EVENT:I = 0x89d2

.field public static final SEND_CALL_ACCEPTED_EVENT:I = 0x88cd

.field public static final SEND_CALL_INVITATION_EVENT:I = 0x88c7

.field public static final SEND_PERSONALINFO_EVENT:I = 0x88d3

.field public static final SEND_PICTURE_EVENT:I = 0x89e0

.field public static final SHAKE_MODE_CHANGE_EVENT:I = 0x891e

.field public static final SHOW_ADVERTISEMENT_EVENT:I = 0x89d8

.field public static final SHOW_MORE_MESSAGE_EVENT:I = 0x89cd

.field public static final SMS_RATE_LIMITED_EVENT:I = 0x892a

.field public static final SMS_VALIDATION_REQUIRED_EVENT:I = 0x88f0

.field public static final SNS_AUTH_FAILED_EVENT:I = 0x8995

.field public static final SNS_PROCESSING_EVENT:I = 0x8994

.field public static final SNS_REQUEST_AUTH_EVENT:I = 0x8993

.field public static final START_PLAY_VIDEO_MAIL_EVENT:I = 0x896d

.field public static final START_RECORDING_VIDEO_MAIL_EVENT:I = 0x895f

.field public static final START_SMS_COMPOSE_EVENT:I = 0x89d1

.field public static final STOP_RECORDING_VIDEO_MAIL_EVENT:I = 0x8960

.field public static final STOP_VIDEO_RINGBACK_EVENT:I = 0x8996

.field public static final SUBSCRIBE_SERVICE_EVENT:I = 0x8971

.field public static final SWITCH_AVATAR_TYPE:I = 0x89b3

.field public static final TAKE_PICTURE_EVENT:I = 0x89da

.field public static final TEST_EVENT:I = 0x8a48

.field public static final TOGGLE_VIDEO_VIEW_BUTTON_BAR_EVENT:I = 0x8973

.field static final UI_EVENT_START:I = 0x88b8

.field public static final UPDATE_CALL_LOG_EVENT:I = 0x8915

.field public static final UPDATE_CONVERSATION_MESSAGE_NOTIFICATION_EVENT:I = 0x89d3

.field public static final UPDATE_CONVERSATION_SUMMARY_EVENT:I = 0x89c9

.field public static final UPDATE_REQUIRED_EVENT:I = 0x8903

.field public static final UPDATE_TANGO_ALERTS_EVENT:I = 0x88de

.field public static final UPDATE_TANGO_USERS_EVENT:I = 0x88dd

.field public static final UPLOAD_VIDEO_MAIL_FINISHED_EVENT:I = 0x8965

.field public static final UPLOAD_VIDEO_MAIL_PROGRESS_EVENT:I = 0x8964

.field public static final UPLOAD_VIDEO_MAIL_RESPONSE_EVENT:I = 0x8962

.field public static final VALIDATION_CODE_INPUT_EVENT:I = 0x891a

.field public static final VALIDATION_CODE_INPUT_WAIT_RESULT_EVENT:I = 0x8925

.field public static final VALIDATION_CODE_REINPUT_EVENT:I = 0x891b

.field public static final VALIDATION_CODE_REINPUT_WAIT_RESULT_EVENT:I = 0x8926

.field public static final VALIDATION_CODE_REQUIRED_EVENT:I = 0x8919

.field public static final VALIDATION_FAILED_TYPE_EVENT:I = 0x88f9

.field public static final VALIDATION_QUERY_OTHER_REGISTERED_DEVICE_EVENT:I = 0x891c

.field public static final VALIDATION_REQUIRED_EVENT:I = 0x88eb

.field public static final VALIDATION_RESULT_EVENT:I = 0x8910

.field public static final VALIDATION_SEND_CODE_RESULT_EVENT:I = 0x8927

.field public static final VALIDATION_SHAKE_REQUIRED_EVENT:I = 0x8917

.field public static final VALIDATION_SHAKE_REQUIRED_TIMEOUT_EVENT:I = 0x8918

.field public static final VALIDATION_VERIFICATION_WITH_OTHER_DEVICE_EVENT:I = 0x891d

.field public static final VGOOD_ANIMATION_COMPLETE_EVENT:I = 0x897a

.field public static final VGOOD_CACHE_MISS_NOTIFICATION_EVENT:I = 0x899e

.field public static final VGOOD_CACHE_MISS_TIMEOUT_EVENT:I = 0x899f

.field public static final VGOOD_DEMO_ANIMATION_COMPLETE_EVENT:I = 0x899d

.field public static final VGOOD_ERROR_EVENT:I = 0x8999

.field public static final VGREETING_PRODUCT_CATALOG_EVENT:I = 0x89bc

.field public static final VGREETING_SEND_RESPONSE_EVENT:I = 0x89bd

.field public static final VIDEO_AVATAR_IN_PROGRESS_TYPE:I = 0x89ab

.field public static final VIDEO_IN_INITIALIZATION_EVENT:I = 0x88d5

.field public static final VIDEO_MAIL_CONFIGURATION_EVENT:I = 0x8951

.field public static final VIDEO_MAIL_ERROR_RESULT_EVENT:I = 0x8953

.field public static final VIDEO_MODE_CHANGED_EVENT:I = 0x8911

.field public static final VIEW_PICTURE_EVENT:I = 0x89e1


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
