.class public final Lcom/sgiggle/media_engine/MediaEngineMessage$message;
.super Ljava/lang/Object;
.source "MediaEngineMessage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/media_engine/MediaEngineMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "message"
.end annotation


# static fields
.field static final ACCEPT_CALL_TYPE:I = 0x7543

.field static final ACKNOWLEDGE_CALLERROR_TYPE:I = 0x7549

.field static final ACKNOWLEDGE_REGISTRATION_ERROR_TYPE:I = 0x7589

.field static final ADD_CONTACT_REQUEST_TYPE:I = 0x758e

.field static final ADD_FAVORITE_CONTACT_TYPE:I = 0x760c

.field static final ADD_VIDEO_TYPE:I = 0x753d

.field static final ADVERTISEMENT_CLICKED_MESSAGE_TYPE:I = 0x7651

.field static final ADVERTISEMENT_SHOWN_MESSAGE_TYPE:I = 0x7650

.field static final ALLOW_ACCESS_ADDRESSBOOK_TYPE:I = 0x756b

.field static final ANCHOR_CHANGED_IN_CONVERSATION_PAGE_TYPE:I = 0x765a

.field static final AUDIO_CONTROL_TYPE:I = 0x7551

.field static final AVATAR_ADD_MESSAGE_TYPE:I = 0x7620

.field static final AVATAR_BACKGROUND_CLEANUP_TYPE:I = 0x762a

.field static final AVATAR_CHANGE_MESSAGE_TYPE:I = 0x7622

.field static final AVATAR_DEMO_ANIMATION_TRACKING_TYPE:I = 0x761c

.field static final AVATAR_REMOVE_MESSAGE_TYPE:I = 0x7621

.field static final AVATAR_RENDER_STATUS_TYPE:I = 0x7623

.field static final BACK_TO_AVATAR_PRODUCT_CATALOG_TYPE:I = 0x7627

.field static final BACK_TO_VGOOD_PRODUCT_CATALOG_TYPE:I = 0x7612

.field static final CALL_ERROR_TYPE:I = 0x754f

.field static final CALL_QUALITY_SURVEY_DATA_TYPE:I = 0x7665

.field static final CANCEL_APPSTORE_TYPE:I = 0x7608

.field static final CANCEL_CALL_QUALITY_SURVEY_TYPE:I = 0x761f

.field static final CANCEL_CHOOSE_PICTURE_TYPE:I = 0x7655

.field static final CANCEL_CONTACT_DETAIL_TYPE:I = 0x760b

.field static final CANCEL_FACEBOOK_LIKE_TYPE:I = 0x7609

.field static final CANCEL_GAME_CATALOG_TYPE:I = 0x765e

.field static final CANCEL_GAME_DEMO_TYPE:I = 0x765f

.field static final CANCEL_IN_APP_PURCHASE_TYPE:I = 0x75f7

.field static final CANCEL_POSTCALL_TYPE:I = 0x75f1

.field static final CANCEL_POST_PROCESS_PICTURE_TYPE:I = 0x7657

.field static final CANCEL_SELECT_CONTACT_TYPE:I = 0x764d

.field static final CANCEL_SGIGGLE_INVITATION_TYPE:I = 0x7547

.field static final CANCEL_TAKE_PICTURE_TYPE:I = 0x7653

.field static final CANCEL_UPLOAD_VIDEO_MAIL_TYPE:I = 0x75df

.field static final CANCEL_VGREETING_PLAYER_TYPE:I = 0x763c

.field static final CANCEL_VGREETING_RECIPIENTS_SELECTION_TYPE:I = 0x7638

.field static final CANCEL_VIDEO_GREETINGS_TYPE:I = 0x7634

.field static final CANCEL_VIDEO_MAIL_RECEIVERS_SELECTION_TYPE:I = 0x75d2

.field static final CANCEL_VIEW_PICTURE_TYPE:I = 0x7659

.field static final CHOOSE_PICTURE_TYPE:I = 0x7654

.field static final CLEAR_BADGE_FOR_INVITES_TYPE:I = 0x767a

.field static final CLEAR_MISSED_CALL_TYPE:I = 0x757f

.field static final CLEAR_UNREAD_MISSED_CALL_NUMBER_TYPE:I = 0x75a5

.field static final CLOSE_CONVERSATION_TYPE:I = 0x7640

.field static final COMPOSING_CONVERSATION_MESSAGE_TYPE:I = 0x764f

.field static final CONFIRM_VIDEO_MAIL_NON_TANGO_NOTIFICATION_TYPE:I = 0x75d3

.field static final CONTACTS_DISPLAY_MAIN_TYPE:I = 0x7567

.field static final CONTACT_SEARCH_CANCEL_TYPE:I = 0x759e

.field static final CONTACT_SEARCH_FAILED_TYPE:I = 0x759d

.field static final CONTACT_SEARCH_REQUEST_TYPE:I = 0x758d

.field static final CONTACT_SEARCH_SUCCESS_TYPE:I = 0x759c

.field static final CONTACT_SEARCH_TYPE_SWITCH_TYPE:I = 0x75a1

.field static final CONTACT_TANGO_CUSTOMER_SUPPORT_TYPE:I = 0x7596

.field static final CONVERSATION_MESSAGE_NOTIFICATION_RECEIVED_TYPE:I = 0x7646

.field static final DELETE_CALL_LOG_TYPE:I = 0x758c

.field static final DELETE_CONVERSATION_TYPE:I = 0x7642

.field static final DELETE_SINGLE_CONVERSATION_MESSAGE_TYPE:I = 0x764e

.field static final DELETE_VIDEO_MAIL_TYPE:I = 0x75cb

.field static final DISABLE_POSTCALL_TYPE:I = 0x7618

.field static final DISMISS_STORE_BADGE_TYPE:I = 0x763a

.field static final DISMISS_UPLOAD_VIDEO_MAIL_FINISHED_TYPE:I = 0x75dc

.field static final DISPLAY_APPSTORE_TYPE:I = 0x7606

.field static final DISPLAY_AVATAR_PAYMENT_UI_TYPE:I = 0x7624

.field static final DISPLAY_AVATAR_PRODUCT_DETAILS_TYPE:I = 0x7626

.field static final DISPLAY_FACEBOOK_LIKE_TYPE:I = 0x7607

.field static final DISPLAY_GAME_CATALOG_TYPE:I = 0x765c

.field static final DISPLAY_GAME_DEMO_TYPE:I = 0x765d

.field static final DISPLAY_PERSONALINFO_TYPE:I = 0x754d

.field static final DISPLAY_PRODUCT_DETAILS_TYPE:I = 0x7613

.field static final DISPLAY_PRODUCT_INFO_UI_TYPE:I = 0x75f5

.field static final DISPLAY_SETTINGS_TYPE:I = 0x7569

.field static final DISPLAY_STORE_TYPE:I = 0x7639

.field static final DISPLAY_VGOOD_DEMO_SCREEN_TYPE:I = 0x75f4

.field static final DISPLAY_VGOOD_PAYMENT_UI_TYPE:I = 0x75ed

.field static final DISPLAY_WELCOME_SCREEN_UI_TYPE:I = 0x75fd

.field static final EDIT_VIDEO_MAIL_TYPE:I = 0x75c7

.field static final EMAIL_CONTACT_SEARCH_TYPE:I = 0x7598

.field static final END_STATE_NO_CHANGE_TYPE:I = 0x754b

.field static final FB_DID_LOGIN_TYPE:I = 0x7670

.field static final FINISH_EDIT_VIDEO_MAIL_TYPE:I = 0x75c8

.field static final FINISH_PLAY_GAME_DEMO_TYPE:I = 0x7660

.field static final FINISH_PLAY_VIDEO_MAIL_TYPE:I = 0x75e6

.field static final FINISH_PLAY_VIDEO_MESSAGE_TYPE:I = 0x7644

.field static final FINISH_SMS_COMPOSE_TYPE:I = 0x764a

.field static final FINISH_UPLOAD_VIDEO_MAIL_TYPE:I = 0x75db

.field static final FINISH_VIDEO_RINGBACK_TYPE:I = 0x7611

.field static final FORWARD_MESSAGE_REQUEST_TYPE:I = 0x7648

.field static final FORWARD_TO_POSTCALL_CONTENT_TYPE:I = 0x7605

.field static final FORWARD_VIDEO_MAIL_TYPE:I = 0x75d0

.field static final GAME_MODE_OFF_TYPE:I = 0x7663

.field static final GAME_MODE_ON_TYPE:I = 0x7662

.field static final INITIATE_VGOOD_TYPE:I = 0x7557

.field static final INVITE_CONTACT_TYPE:I = 0x758a

.field static final INVITE_DISPLAY_MAIN_TYPE:I = 0x7561

.field static final INVITE_EMAIL_COMPOSER_TYPE:I = 0x756d

.field static final INVITE_EMAIL_SELECTION_TYPE:I = 0x7563

.field static final INVITE_EMAIL_SEND_TYPE:I = 0x7565

.field static final INVITE_RECOMMENDED_SELECTED_TYPE:I = 0x7575

.field static final INVITE_RECOMMENDED_SELECTION_TYPE:I = 0x7574

.field static final INVITE_SMS_SELECTED_TYPE:I = 0x7573

.field static final INVITE_SMS_SELECTION_TYPE:I = 0x7571

.field static final INVITE_SMS_SEND_TYPE:I = 0x7576

.field static final INVITE_SNS_PUBLISH_TYPE:I = 0x75ff

.field static final INVITE_VIA_SNS_TYPE:I = 0x75fe

.field static final KEEP_PUSH_NOTIFICATION_ALIVE_TYPE:I = 0x7587

.field static final LAUNCH_LOG_REPORT_EMAIL_TYPE:I = 0x7554

.field static final LEAVE_VIDEO_MAIL_TYPE:I = 0x75d5

.field static final LIKE_VIDEO_RINGBACK_TYPE:I = 0x760f

.field static final LOAD_VGREETING_PLAYER_TYPE:I = 0x763b

.field static final LOCKED_VGOOD_SELECTED_TYPE:I = 0x75fc

.field static final LOGIN_NOTIFICATION_TYPE:I = 0x7532

.field static final LOGIN_NOTIFICATION_USER_ACCEPTED_TYPE:I = 0x7531

.field static final LOGIN_TYPE:I = 0x7535

.field static final MAKE_CALL_TYPE:I = 0x7537

.field static final MAKE_PREMIUM_CALL_TYPE:I = 0x7533

.field static final NAVIGATE_BACK_TYPE:I = 0x7664

.field static final OPEN_CONVERSATION_LIST_TYPE:I = 0x7641

.field static final OPEN_CONVERSATION_TYPE:I = 0x763e

.field static final PAUSE_PLAY_VIDEO_MAIL_TYPE:I = 0x75e5

.field static final PHONENUMBER_CONTACT_SEARCH_TYPE:I = 0x7599

.field static final PLAY_VGOOD_VIDEO_TYPE:I = 0x75f9

.field static final PLAY_VIDEO_MAIL_TYPE:I = 0x75e4

.field static final PLAY_VIDEO_MESSAGE_TYPE:I = 0x7643

.field static final POST_PROCESS_PICTURE_TYPE:I = 0x7656

.field static final PRODUCT_CATALOG_FINISHED_TYPE:I = 0x7604

.field static final PRODUCT_CATALOG_REQUEST_TYPE:I = 0x7603

.field static final PRODUCT_INFO_OK_TYPE:I = 0x75f3

.field static final PURCHASE_ATTEMPT_TYPE:I = 0x7619

.field static final PURCHASE_AVATAR_TYPE:I = 0x7625

.field static final PURCHASE_GAME_TYPE:I = 0x7661

.field static final PURCHASE_VGOOD_TYPE:I = 0x75ef

.field static final PUT_APP_IN_BACKGROUND_TYPE:I = 0x7536

.field static final PUT_APP_IN_FOREGROUND_TYPE:I = 0x7586

.field static final QUERY_LEAVE_MESSAGE_TYPE:I = 0x7647

.field static final QUERY_UNREAD_MISSED_CALL_NUMBER_TYPE:I = 0x75a4

.field static final RECEIVED_CALL_CANCEL_TYPE:I = 0x768e

.field static final RECEIVE_MESSAGE_NOTIFICATION_TYPE:I = 0x759a

.field static final RECEIVE_PUSH_NOTIFICATION_TYPE:I = 0x7534

.field static final REGISTER_USER_TYPE:I = 0x755b

.field static final REJECT_CALL_TYPE:I = 0x7539

.field static final REMOVE_FAVORITE_CONTACT_TYPE:I = 0x760d

.field static final REMOVE_VIDEO_TYPE:I = 0x753f

.field static final REPORT_PURCHASED_AVATAR_TYPE:I = 0x762b

.field static final REPORT_PURCHASE_TYPE:I = 0x7602

.field static final REQUEST_APP_LOG_TYPE:I = 0x75a2

.field static final REQUEST_CALL_LOG_TYPE:I = 0x758b

.field static final REQUEST_FAQ_TYPE:I = 0x7577

.field static final REQUEST_FILTER_CONTACT_TYPE:I = 0x7581

.field static final REQUEST_PHONE_FORMATTING_TYPE:I = 0x7584

.field static final SAVE_PERSONALINFO_TYPE:I = 0x7545

.field static final SEARCH_AND_ADD_DISPLAY_MAIN_TYPE:I = 0x75a0

.field static final SELECT_CONTACT_REQUEST_TYPE:I = 0x764b

.field static final SELECT_CONTACT_RESULT_TYPE:I = 0x764c

.field static final SEND_CONVERSATION_MESSAGE_TYPE:I = 0x763f

.field static final SEND_INVITE_LIST:I = 0x7555

.field static final SEND_SGIGGLE_INVITATION_TYPE:I = 0x7541

.field static final SEND_VALIDATION_CODE_TYPE:I = 0x7583

.field static final SEND_VGREETING_TYPE:I = 0x7636

.field static final SHAKE_ACTION_DETECTED_TYPE:I = 0x7597

.field static final SHOW_MORE_MESSAGE_TYPE:I = 0x7645

.field static final SKIP_VIDEO_RINGBACK_TYPE:I = 0x7610

.field static final SNS_AUTH_RESULT_TYPE:I = 0x7600

.field static final SNS_CANCEL_PROCESS_TYPE:I = 0x7601

.field static final SNS_PROCESSING_TIMEOUT_TYPE:I = 0x760e

.field static final START_DEMO_AVATAR_TYPE:I = 0x7628

.field static final START_RECORDING_VIDEO_MAIL_TYPE:I = 0x75d6

.field static final START_SMS_COMPOSE_TYPE:I = 0x7649

.field static final STATS_COLLECTOR_LOG_TYPE:I = 0x7666

.field static final STOP_BACKGROUND_TASK_TYPE:I = 0x7614

.field static final STOP_RECORDING_VIDEO_MAIL_TYPE:I = 0x75d7

.field static final SUBSCRIBE_SERVICE_TYPE:I = 0x75e9

.field static final SWITCH_CAMERA_TYPE:I = 0x757d

.field static final TAKE_PICTURE_TYPE:I = 0x7652

.field static final TERMINATE_CALL_TYPE:I = 0x753b

.field static final TEST_TYPE:I = 0x7594

.field static final TOGGLE_VIDEO_VIEW_BUTTON_BAR_TYPE:I = 0x75eb

.field static final UPDATE_DEVICE_TOKEN_TYPE:I = 0x7588

.field static final UPLOAD_VIDEO_MAIL_TYPE:I = 0x75da

.field static final VALIDATION_CODE_REQUIRED_SEND_CODE_TYPE:I = 0x7592

.field static final VALIDATION_CODE_REQUIRED_SUBMIT_TYPE:I = 0x7591

.field static final VALIDATION_DISMISS_VERIFICATION_WITH_OTHER_DEVICE_TYPE:I = 0x7595

.field static final VALIDATION_OTHER_REGISTERED_DEVICE_TYPE:I = 0x7593

.field static final VALIDATION_REQUEST_TYPE:I = 0x756f

.field static final VALIDATION_SEND_CODE_FAILED_TYPE:I = 0x759f

.field static final VALIDATION_SEND_CODE_TYPE:I = 0x759b

.field static final VALIDATION_SHAKE_REQUIRED_RETRY_TYPE:I = 0x758f

.field static final VALIDATION_USER_INPUTED_CODE_TYPE:I = 0x75ab

.field static final VALIDATION_USER_NO_CODE_TYPE:I = 0x75ac

.field static final VALIDATION_VERIFY_PHONE_NUMBER_TYPE:I = 0x7590

.field static final VGOOD_ANIMATION_COMPLETE_TYPE:I = 0x75fb

.field static final VGOOD_DEMO_ANIMATION_COMPLETE_TYPE:I = 0x7615

.field static final VGOOD_DEMO_ANIMATION_TRACKING_TYPE:I = 0x761a

.field static final VGREETING_PRODUCT_CATALOG_REQUEST_TYPE:I = 0x7635

.field static final VGREETING_RECIPIENTS_SELECTION_TYPE:I = 0x7637

.field static final VIDEO_MAIL_RECEIVERS_TYPE:I = 0x75d1

.field static final VIEW_CONTACT_DETAIL_TYPE:I = 0x760a

.field static final VIEW_PICTURE_TYPE:I = 0x7658

.field static final VMAIL_ATTEMPT_PURCHASE_TRACKING_TYPE:I = 0x761b

.field static final WAND_PRESSED_TYPE:I = 0x7616

.field static final WELCOME_GO_TO_TYPE:I = 0x7617


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
