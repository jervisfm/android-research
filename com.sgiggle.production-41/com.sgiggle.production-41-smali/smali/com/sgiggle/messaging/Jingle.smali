.class public Lcom/sgiggle/messaging/Jingle;
.super Ljava/lang/Object;
.source "Jingle.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static sendAcceptSessionMessage()V
    .locals 5

    .prologue
    .line 69
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a Accept message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 70
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 71
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x271f

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 74
    return-void
.end method

.method public static sendCloseSessionMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 88
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a close session message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 89
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 90
    const-string v1, "sessionId"

    invoke-virtual {v0, v1, p1}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v1, "jid"

    invoke-virtual {v0, v1, p0}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x271f

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 95
    return-void
.end method

.method public static sendLoginMessage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 17
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a login message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 18
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 19
    const-string v1, "xmppServerName"

    invoke-virtual {v0, v1, p0}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const-string v1, "xmppServerPort"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    const-string v1, "userName"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@prosody"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v1, "userPassword"

    invoke-virtual {v0, v1, p3}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v1, "resource"

    const-string v2, "prosody"

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x2713

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 27
    return-void
.end method

.method public static sendLogoutMessage()V
    .locals 5

    .prologue
    .line 31
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a logout message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 32
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 34
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x2715

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 37
    return-void
.end method

.method public static sendPresenceMessage()V
    .locals 5

    .prologue
    .line 78
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a Presence message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 79
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 80
    const-string v1, "show"

    const-string v2, "mobile"

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x274d

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 84
    return-void
.end method

.method public static sendRegistrationMessage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    const/16 v0, 0x50

    const-string v1, "JAVA: Sending Jingle a login message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 45
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 46
    const-string v1, "xmppServerName"

    invoke-virtual {v0, v1, p0}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v1, "xmppServerPort"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v1, "userName"

    invoke-virtual {v0, v1, p2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "userPassword"

    invoke-virtual {v0, v1, p3}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v1, "resource"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x2711

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 54
    return-void
.end method

.method public static sendStartSessionMessage(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    .line 58
    const/16 v0, 0x50

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JAVA: Sending Jingle a start message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 59
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    .line 60
    const-string v1, "jid"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@prosody"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, "resource"

    const-string v2, "prosody"

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/messaging/KeyValueMapMessage;

    const/16 v4, 0x271b

    invoke-direct {v3, v4, v0}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 65
    return-void
.end method
