.class public Lcom/sgiggle/messaging/KeyValueMap;
.super Ljava/lang/Object;
.source "KeyValueMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/messaging/KeyValueMap$ParseResult;
    }
.end annotation


# static fields
.field static final KEY_FLAT_KEY:Ljava/lang/String; = "__key__"

.field static final KEY_SIZE:Ljava/lang/String; = "__size__"


# instance fields
.field private m_map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    .line 14
    return-void
.end method

.method static parseFlatKey(Ljava/lang/String;)Lcom/sgiggle/messaging/KeyValueMap$ParseResult;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 69
    const-string v0, "__key__"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 70
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 71
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid flat key: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_0
    new-instance v1, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;

    invoke-direct {v1}, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;-><init>()V

    .line 76
    const/4 v2, 0x0

    const/4 v3, 0x1

    sub-int v3, v0, v3

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 77
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_index:I

    .line 78
    const-string v2, "__key__"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_key:Ljava/lang/String;

    .line 79
    return-object v1
.end method


# virtual methods
.method public asKeyValueMapVector()Ljava/util/Vector;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Vector",
            "<",
            "Lcom/sgiggle/messaging/KeyValueMap;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 84
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 86
    const-string v0, "__size__"

    invoke-virtual {p0, v0}, Lcom/sgiggle/messaging/KeyValueMap;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/Error;

    const-string v1, "No \'__size__\' key found."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    const-string v0, "__size__"

    invoke-virtual {p0, v0}, Lcom/sgiggle/messaging/KeyValueMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 92
    invoke-virtual {v1, v0}, Ljava/util/Vector;->setSize(I)V

    .line 93
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 94
    new-instance v2, Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v2}, Lcom/sgiggle/messaging/KeyValueMap;-><init>()V

    invoke-virtual {v1, v2, v0}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 99
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 100
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 101
    invoke-virtual {p0, v0}, Lcom/sgiggle/messaging/KeyValueMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 104
    const-string v4, "__size__"

    if-eq v0, v4, :cond_2

    .line 108
    invoke-static {v0}, Lcom/sgiggle/messaging/KeyValueMap;->parseFlatKey(Ljava/lang/String;)Lcom/sgiggle/messaging/KeyValueMap$ParseResult;

    move-result-object v4

    .line 110
    iget v0, v4, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_index:I

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 111
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v4, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_index:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_3
    iget v0, v4, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_index:I

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/KeyValueMap;

    .line 115
    iget-object v4, v4, Lcom/sgiggle/messaging/KeyValueMap$ParseResult;->m_key:Ljava/lang/String;

    invoke-virtual {v0, v4, v3}, Lcom/sgiggle/messaging/KeyValueMap;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 118
    :cond_4
    return-object v1
.end method

.method public asPairArray()[Ljava/lang/String;
    .locals 7

    .prologue
    .line 29
    iget-object v1, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    .line 30
    const/4 v1, 0x0

    .line 31
    iget-object v2, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v5, v1

    .line 32
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 34
    iget-object v2, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 35
    add-int/lit8 v6, v5, 0x1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v3, v5

    .line 36
    add-int/lit8 v5, v6, 0x1

    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    aput-object v1, v3, v6

    goto :goto_0

    .line 38
    :cond_0
    return-object v3
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public set(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 42
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 43
    iget-object v1, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 44
    const/4 v5, 0x0

    .line 45
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 46
    if-nez v5, :cond_0

    .line 47
    const-string v1, ";"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 49
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 50
    iget-object v2, p0, Lcom/sgiggle/messaging/KeyValueMap;->m_map:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 51
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    const-string v1, "="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 53
    move-object v0, v2

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
