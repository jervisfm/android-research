.class public Lcom/sgiggle/messaging/KeyValueMapMessage;
.super Lcom/sgiggle/messaging/SimpleMessage;
.source "KeyValueMapMessage.java"


# instance fields
.field private m_values:Lcom/sgiggle/messaging/KeyValueMap;


# direct methods
.method public constructor <init>(IJLcom/sgiggle/messaging/KeyValueMap;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/messaging/SimpleMessage;-><init>(IJ)V

    .line 16
    iput-object p4, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    .line 17
    return-void
.end method

.method public constructor <init>(ILcom/sgiggle/messaging/KeyValueMap;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/sgiggle/messaging/SimpleMessage;-><init>(I)V

    .line 11
    iput-object p2, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    .line 12
    return-void
.end method


# virtual methods
.method public final clone()Lcom/sgiggle/messaging/Message;
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/sgiggle/messaging/KeyValueMapMessage;

    invoke-virtual {p0}, Lcom/sgiggle/messaging/KeyValueMapMessage;->getType()I

    move-result v1

    iget-object v2, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/messaging/KeyValueMapMessage;-><init>(ILcom/sgiggle/messaging/KeyValueMap;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/sgiggle/messaging/KeyValueMapMessage;->clone()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    invoke-virtual {v0, p1}, Lcom/sgiggle/messaging/KeyValueMap;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValues()Lcom/sgiggle/messaging/KeyValueMap;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 27
    const-string v1, "t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 28
    invoke-virtual {p0}, Lcom/sgiggle/messaging/KeyValueMapMessage;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 29
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 30
    const-string v1, "s="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 31
    invoke-virtual {p0}, Lcom/sgiggle/messaging/KeyValueMapMessage;->getSequenceId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 32
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 33
    const-string v1, "v="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 34
    iget-object v1, p0, Lcom/sgiggle/messaging/KeyValueMapMessage;->m_values:Lcom/sgiggle/messaging/KeyValueMap;

    invoke-virtual {v1}, Lcom/sgiggle/messaging/KeyValueMap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
