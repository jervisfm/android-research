.class public abstract Lcom/sgiggle/messaging/Message;
.super Ljava/lang/Object;
.source "Message.java"


# static fields
.field public static final COMPONENT_ALL:Ljava/lang/String; = "all"

.field public static final COMPONENT_GUI:Ljava/lang/String; = "gui"

.field public static final COMPONENT_JINGLE:Ljava/lang/String; = "jingle"

.field public static final COMPONENT_TESTING:Ljava/lang/String; = "testing"

.field public static final COMPONENT_UI:Ljava/lang/String; = "ui"

.field public static final COMPONENT_UNDEFINED:Ljava/lang/String; = ""

.field public static final MESSAGE_GUI_END:I = 0x752f

.field public static final MESSAGE_GUI_START:I = 0x4e20

.field public static final MESSAGE_JINGLE_END:I = 0x4e1f

.field public static final MESSAGE_JINGLE_START:I = 0x2710

.field public static final MESSAGE_RESERVED_END:I = 0x270f

.field public static final MESSAGE_RESERVED_START:I = 0x1

.field public static final MESSAGE_TESTING_END:I = 0x3b9af10f

.field public static final MESSAGE_TESTING_START:I = 0x3b9aca00

.field public static final MESSAGE_UI_END:I = 0x9c3f

.field public static final MESSAGE_UI_START:I = 0x7530

.field public static final MESSAGE_UNDEFINED:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract clone()Lcom/sgiggle/messaging/Message;
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/sgiggle/messaging/Message;->clone()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    return-object v0
.end method

.method public abstract deserialize([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation
.end method

.method public abstract getSequenceId()J
.end method

.method public abstract getType()I
.end method

.method public abstract serialize()[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation
.end method

.method public abstract toString()Ljava/lang/String;
.end method
