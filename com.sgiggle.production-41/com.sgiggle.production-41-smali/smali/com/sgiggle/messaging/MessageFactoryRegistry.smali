.class public Lcom/sgiggle/messaging/MessageFactoryRegistry;
.super Ljava/lang/Object;
.source "MessageFactoryRegistry.java"


# static fields
.field private static s_instance:Lcom/sgiggle/messaging/MessageFactoryRegistry;

.field private static s_lock:Ljava/lang/Object;


# instance fields
.field private m_factories:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sgiggle/messaging/MessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private m_nextId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_lock:Ljava/lang/Object;

    .line 72
    new-instance v0, Lcom/sgiggle/messaging/MessageFactoryRegistry;

    invoke-direct {v0}, Lcom/sgiggle/messaging/MessageFactoryRegistry;-><init>()V

    sput-object v0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_instance:Lcom/sgiggle/messaging/MessageFactoryRegistry;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    .line 22
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/messaging/MessageFactoryRegistry;
    .locals 2

    .prologue
    .line 13
    sget-object v0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_lock:Ljava/lang/Object;

    monitor-enter v0

    .line 14
    :try_start_0
    sget-object v1, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_instance:Lcom/sgiggle/messaging/MessageFactoryRegistry;

    if-nez v1, :cond_0

    .line 15
    new-instance v1, Lcom/sgiggle/messaging/MessageFactoryRegistry;

    invoke-direct {v1}, Lcom/sgiggle/messaging/MessageFactoryRegistry;-><init>()V

    sput-object v1, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_instance:Lcom/sgiggle/messaging/MessageFactoryRegistry;

    .line 17
    :cond_0
    sget-object v1, Lcom/sgiggle/messaging/MessageFactoryRegistry;->s_instance:Lcom/sgiggle/messaging/MessageFactoryRegistry;

    monitor-exit v0

    return-object v1

    .line 18
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method public create(I)Lcom/sgiggle/messaging/Message;
    .locals 4
    .parameter

    .prologue
    .line 51
    iget-object v1, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    monitor-enter v1

    .line 52
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 53
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/MessageFactory;

    .line 55
    invoke-interface {v0, p1}, Lcom/sgiggle/messaging/MessageFactory;->create(I)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 56
    if-eqz v0, :cond_0

    .line 57
    monitor-exit v1

    .line 60
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerFactory(Lcom/sgiggle/messaging/MessageFactory;)I
    .locals 4
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    monitor-enter v0

    .line 31
    :try_start_0
    iget v1, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_nextId:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_nextId:I

    .line 32
    iget-object v2, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    monitor-exit v0

    return v1

    .line 34
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public unregisterFactory(I)V
    .locals 3
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    monitor-enter v0

    .line 43
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/messaging/MessageFactoryRegistry;->m_factories:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    monitor-exit v0

    .line 45
    return-void

    .line 44
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
