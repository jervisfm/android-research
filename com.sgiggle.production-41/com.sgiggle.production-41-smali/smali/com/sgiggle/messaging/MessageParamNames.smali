.class public Lcom/sgiggle/messaging/MessageParamNames;
.super Ljava/lang/Object;
.source "MessageParamNames.java"


# static fields
.field public static final AVAILABLE:Ljava/lang/String; = "available"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final JID:Ljava/lang/String; = "jid"

.field public static final PAROSODY:Ljava/lang/String; = "prosody"

.field public static final PRESENCE:Ljava/lang/String; = "presence"

.field public static final RESOURCE:Ljava/lang/String; = "resource"

.field public static final SESSIONID:Ljava/lang/String; = "sessionId"

.field public static final SHOW:Ljava/lang/String; = "show"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final STATUS:Ljava/lang/String; = "status"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
