.class Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;
.super Ljava/lang/Thread;
.source "MessageRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/messaging/MessageRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MessageGetterThread"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private m_running:Z

.field final synthetic this$0:Lcom/sgiggle/messaging/MessageRouter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-class v0, Lcom/sgiggle/messaging/MessageRouter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sgiggle/messaging/MessageRouter;)V
    .locals 1
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    .line 111
    const-string v0, "MessageGetter"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->m_running:Z

    .line 113
    return-void
.end method

.method private dispatchMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 176
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$600(Lcom/sgiggle/messaging/MessageRouter;)Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    move-result-object v0

    monitor-enter v0

    .line 177
    :try_start_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;
    invoke-static {v1}, Lcom/sgiggle/messaging/MessageRouter;->access$600(Lcom/sgiggle/messaging/MessageRouter;)Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->getInterested(Ljava/lang/String;)Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;

    move-result-object v1

    .line 178
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    if-eqz v1, :cond_1

    .line 180
    const/16 v0, 0x50

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Message \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' dispatched to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 181
    sget-boolean v0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 178
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 182
    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 183
    invoke-virtual {v1, v0}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->elementAt(I)Lcom/sgiggle/messaging/MessageReceiver;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/sgiggle/messaging/MessageReceiver;->receiveMessage(Lcom/sgiggle/messaging/Message;)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 186
    :cond_1
    return-void
.end method

.method private isRunning()Z
    .locals 1

    .prologue
    .line 196
    monitor-enter p0

    .line 197
    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->m_running:Z

    monitor-exit p0

    return v0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private parseMessage(Lcom/sgiggle/messaging/MessageRouter$GNMResult;)Lcom/sgiggle/messaging/Message;
    .locals 6
    .parameter

    .prologue
    const/16 v5, 0x50

    const/16 v4, 0x10

    const/4 v3, 0x0

    .line 151
    iget-object v0, p1, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_payload:[B

    if-nez v0, :cond_0

    .line 152
    const-string v0, "MessageRouter: Cannot parse null message payload."

    invoke-static {v4, v5, v0}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V

    move-object v0, v3

    .line 171
    :goto_0
    return-object v0

    .line 157
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageFactoryRegistry;->getInstance()Lcom/sgiggle/messaging/MessageFactoryRegistry;

    move-result-object v0

    iget v1, p1, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_type:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/messaging/MessageFactoryRegistry;->create(I)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 158
    if-nez v0, :cond_1

    move-object v0, v3

    .line 159
    goto :goto_0

    .line 164
    :cond_1
    :try_start_0
    iget-object v1, p1, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_payload:[B

    invoke-virtual {v0, v1}, Lcom/sgiggle/messaging/Message;->deserialize([B)V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageRouter: Error parsing message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v5, v0}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V

    move-object v0, v3

    .line 168
    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/16 v6, 0x50

    .line 118
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 119
    const-string v0, "waiting for message..."

    invoke-static {v6, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 120
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    #calls: Lcom/sgiggle/messaging/MessageRouter;->getNextMessage()Lcom/sgiggle/messaging/MessageRouter$GNMResult;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$100(Lcom/sgiggle/messaging/MessageRouter;)Lcom/sgiggle/messaging/MessageRouter$GNMResult;

    move-result-object v0

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessageRouter: Got message target="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_target:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v0, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 123
    invoke-direct {p0, v0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->parseMessage(Lcom/sgiggle/messaging/MessageRouter$GNMResult;)Lcom/sgiggle/messaging/Message;

    move-result-object v1

    .line 124
    if-eqz v1, :cond_0

    .line 125
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v2

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v2}, Lcom/sgiggle/messaging/MessageRouter;->access$200(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 126
    iget-object v2, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    const/4 v3, 0x0

    #setter for: Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z
    invoke-static {v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->access$302(Lcom/sgiggle/messaging/MessageRouter;Z)Z

    .line 128
    :try_start_0
    iget-object v0, v0, Lcom/sgiggle/messaging/MessageRouter$GNMResult;->m_target:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->dispatchMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 129
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_ackTimeoutMilliseconds:J
    invoke-static {v2}, Lcom/sgiggle/messaging/MessageRouter;->access$400(Lcom/sgiggle/messaging/MessageRouter;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 130
    const/16 v0, 0x50

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "waiting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "...."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_condition:Ljava/util/concurrent/locks/Condition;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$500(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0, v2, v3}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v2

    .line 132
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    .line 133
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$300(Lcom/sgiggle/messaging/MessageRouter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    const/16 v0, 0x10

    const/16 v1, 0x50

    const-string v2, "MessageRouter: TIMEOUT: ack not received after app entered background, ignore this."

    invoke-static {v0, v1, v2}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    :goto_1
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$200(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 136
    :cond_1
    const/16 v0, 0x10

    const/16 v2, 0x50

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MessageRouter: TIMEOUT: ack not received, contiue to get next message. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 141
    :catch_0
    move-exception v0

    .line 142
    const/16 v0, 0x10

    const/16 v1, 0x50

    :try_start_2
    const-string v2, "MessageRouter: the waiting for ack is interrupted"

    invoke-static {v0, v1, v2}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 144
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$200(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    .line 139
    :cond_2
    const/16 v0, 0x50

    :try_start_3
    const-string v1, "MessageRouter: wake up by ack, continue to get next message."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    .line 144
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->access$000()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v1}, Lcom/sgiggle/messaging/MessageRouter;->access$200(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 148
    :cond_3
    return-void
.end method

.method public setStopped()V
    .locals 1

    .prologue
    .line 189
    monitor-enter p0

    .line 190
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->m_running:Z

    .line 191
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    #getter for: Lcom/sgiggle/messaging/MessageRouter;->m_condition:Ljava/util/concurrent/locks/Condition;
    invoke-static {v0}, Lcom/sgiggle/messaging/MessageRouter;->access$500(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 192
    monitor-exit p0

    .line 193
    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
