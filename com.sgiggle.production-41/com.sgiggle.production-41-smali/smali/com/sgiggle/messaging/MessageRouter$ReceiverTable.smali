.class Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;
.super Ljava/lang/Object;
.source "MessageRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/messaging/MessageRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiverTable"
.end annotation


# instance fields
.field private m_map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sgiggle/messaging/MessageRouter;


# direct methods
.method constructor <init>(Lcom/sgiggle/messaging/MessageRouter;)V
    .locals 1
    .parameter

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    .line 231
    return-void
.end method


# virtual methods
.method add(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;

    .line 242
    :goto_0
    invoke-virtual {v0, p2}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->add(Lcom/sgiggle/messaging/MessageReceiver;)V

    .line 243
    return-void

    .line 238
    :cond_0
    new-instance v0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;

    iget-object v1, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    invoke-direct {v0, v1}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;-><init>(Lcom/sgiggle/messaging/MessageRouter;)V

    .line 239
    iget-object v1, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method getInterested(Ljava/lang/String;)Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;
    .locals 1
    .parameter

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x0

    .line 258
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;

    goto :goto_0
.end method

.method remove(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->m_map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;

    .line 251
    invoke-virtual {v0, p2}, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->remove(Lcom/sgiggle/messaging/MessageReceiver;)V

    goto :goto_0
.end method
