.class Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;
.super Ljava/lang/Object;
.source "MessageRouter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/messaging/MessageRouter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ReceiverVector"
.end annotation


# instance fields
.field private m_vector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sgiggle/messaging/MessageReceiver;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sgiggle/messaging/MessageRouter;


# direct methods
.method constructor <init>(Lcom/sgiggle/messaging/MessageRouter;)V
    .locals 1
    .parameter

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->this$0:Lcom/sgiggle/messaging/MessageRouter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->m_vector:Ljava/util/Vector;

    .line 207
    return-void
.end method


# virtual methods
.method add(Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 1
    .parameter

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->m_vector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 211
    return-void
.end method

.method elementAt(I)Lcom/sgiggle/messaging/MessageReceiver;
    .locals 1
    .parameter

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->m_vector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/MessageReceiver;

    return-object v0
.end method

.method remove(Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 1
    .parameter

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->m_vector:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 215
    return-void
.end method

.method size()I
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;->m_vector:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method
