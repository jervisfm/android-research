.class public Lcom/sgiggle/messaging/MessageRouter;
.super Ljava/lang/Object;
.source "MessageRouter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;,
        Lcom/sgiggle/messaging/MessageRouter$ReceiverVector;,
        Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;,
        Lcom/sgiggle/messaging/MessageRouter$GNMResult;
    }
.end annotation


# static fields
.field private static s_instance:Lcom/sgiggle/messaging/MessageRouter;


# instance fields
.field private m_ackTimeoutMilliseconds:J

.field private final m_condition:Ljava/util/concurrent/locks/Condition;

.field private m_enteredBackgroundFlag:Z

.field private final m_lock:Ljava/util/concurrent/locks/Lock;

.field private m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

.field private m_thread:Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274
    new-instance v0, Lcom/sgiggle/messaging/MessageRouter;

    invoke-direct {v0}, Lcom/sgiggle/messaging/MessageRouter;-><init>()V

    sput-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 282
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    .line 283
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_condition:Ljava/util/concurrent/locks/Condition;

    .line 284
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_ackTimeoutMilliseconds:J

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z

    .line 20
    new-instance v0, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    invoke-direct {v0, p0}, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;-><init>(Lcom/sgiggle/messaging/MessageRouter;)V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    .line 21
    new-instance v0, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;

    invoke-direct {v0, p0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;-><init>(Lcom/sgiggle/messaging/MessageRouter;)V

    iput-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_thread:Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;

    .line 22
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_thread:Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->start()V

    .line 23
    return-void
.end method

.method static synthetic access$000()Lcom/sgiggle/messaging/MessageRouter;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/messaging/MessageRouter;)Lcom/sgiggle/messaging/MessageRouter$GNMResult;
    .locals 1
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sgiggle/messaging/MessageRouter;->getNextMessage()Lcom/sgiggle/messaging/MessageRouter$GNMResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/messaging/MessageRouter;)Z
    .locals 1
    .parameter

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sgiggle/messaging/MessageRouter;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sgiggle/messaging/MessageRouter;)J
    .locals 2
    .parameter

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_ackTimeoutMilliseconds:J

    return-wide v0
.end method

.method static synthetic access$500(Lcom/sgiggle/messaging/MessageRouter;)Ljava/util/concurrent/locks/Condition;
    .locals 1
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_condition:Ljava/util/concurrent/locks/Condition;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/messaging/MessageRouter;)Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;
    .locals 1
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    return-object v0
.end method

.method public static getInstance()Lcom/sgiggle/messaging/MessageRouter;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    return-object v0
.end method

.method private native getNextMessage()Lcom/sgiggle/messaging/MessageRouter$GNMResult;
.end method

.method public static initialize()V
    .locals 2

    .prologue
    const/16 v1, 0x50

    .line 64
    const-string v0, "JAVA: Initializing MessageRouter."

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 65
    sget-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "JAVA: MessageRouter already initialized"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 71
    :goto_0
    return-void

    .line 69
    :cond_0
    new-instance v0, Lcom/sgiggle/messaging/MessageRouter;

    invoke-direct {v0}, Lcom/sgiggle/messaging/MessageRouter;-><init>()V

    sput-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    goto :goto_0
.end method

.method private native post(Ljava/lang/String;I[B)V
.end method

.method public static shutdown()V
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    sget-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter;->stop()V

    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/messaging/MessageRouter;->s_instance:Lcom/sgiggle/messaging/MessageRouter;

    goto :goto_0
.end method


# virtual methods
.method public ack()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_condition:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 48
    return-void

    .line 46
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public enterBackground()V
    .locals 2

    .prologue
    .line 52
    const/16 v0, 0x50

    const-string v1, "MessageAgentWithAck:enterBackground()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_enteredBackgroundFlag:Z

    .line 55
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 56
    return-void
.end method

.method public native getMessageName(I)Ljava/lang/String;
.end method

.method public native getNextSequenceId()J
.end method

.method public postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 84
    invoke-virtual {p2}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    invoke-virtual {p2}, Lcom/sgiggle/messaging/Message;->serialize()[B

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sgiggle/messaging/MessageRouter;->post(Ljava/lang/String;I[B)V

    .line 85
    return-void
.end method

.method public registerReceiver(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    monitor-enter v0

    .line 90
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    invoke-virtual {v1, p1, p2}, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->add(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V

    .line 91
    monitor-exit v0

    .line 92
    return-void

    .line 91
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setAckTimeout(J)V
    .locals 1
    .parameter

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 37
    iput-wide p1, p0, Lcom/sgiggle/messaging/MessageRouter;->m_ackTimeoutMilliseconds:J

    .line 38
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 39
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_thread:Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->setStopped()V

    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_thread:Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter$MessageGetterThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public unregisterReceiver(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    monitor-enter v0

    .line 97
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/messaging/MessageRouter;->m_receivers:Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;

    invoke-virtual {v1, p1, p2}, Lcom/sgiggle/messaging/MessageRouter$ReceiverTable;->remove(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V

    .line 98
    monitor-exit v0

    .line 99
    return-void

    .line 98
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
