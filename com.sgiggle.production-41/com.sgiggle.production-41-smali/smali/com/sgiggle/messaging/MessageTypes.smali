.class public final Lcom/sgiggle/messaging/MessageTypes;
.super Ljava/lang/Object;
.source "MessageTypes.java"


# static fields
.field public static final kAcceptBuddy:I = 0x2745

.field public static final kAcceptSession:I = 0x271f

.field public static final kAcceptedBuddy:I = 0x2746

.field public static final kAcceptedSession:I = 0x2720

.field public static final kAutoReply:I = 0x274f

.field public static final kCloseSession:I = 0x2723

.field public static final kClosedSession:I = 0x2724

.field public static final kConnectUser:I = 0x2713

.field public static final kDisConnectUser:I = 0x2715

.field public static final kErrorReport:I = 0x27d8

.field public static final kFilterContact:I = 0x275d

.field public static final kFilterContactUpdate:I = 0x275f

.field public static final kGetBuddyList:I = 0x272f

.field public static final kGetVCard:I = 0x2739

.field public static final kInviteBuddy:I = 0x2743

.field public static final kInvitedBuddy:I = 0x2744

.field public static final kJINGLEOFFESET:I = 0x2710

.field public static final kReceiveBuddyList:I = 0x2730

.field public static final kReceiveBuddyMsg:I = 0x273e

.field public static final kReceivePresence:I = 0x274e

.field public static final kReceivedVCard:I = 0x273a

.field public static final kRegisterUser:I = 0x2711

.field public static final kRegistrationValidationNone:I = 0x2765

.field public static final kRejectBuddy:I = 0x2747

.field public static final kRejectSession:I = 0x2721

.field public static final kRejectedBuddy:I = 0x2748

.field public static final kRejectedSession:I = 0x2722

.field public static final kRequestSession:I = 0x271e

.field public static final kSendBuddyMsg:I = 0x273d

.field public static final kSendPresence:I = 0x274d

.field public static final kSetVCard:I = 0x273b

.field public static final kStartSession:I = 0x271b

.field public static final kStartedSession:I = 0x271c

.field public static final kStateChanged:I = 0x2774

.field public static final kkTriggerRegisterOnContactsLoaded:I = 0x2712


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
