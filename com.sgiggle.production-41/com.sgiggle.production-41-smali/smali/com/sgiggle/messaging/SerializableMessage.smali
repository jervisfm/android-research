.class public Lcom/sgiggle/messaging/SerializableMessage;
.super Lcom/sgiggle/messaging/Message;
.source "SerializableMessage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TPAY",
        "LOAD:Lcom/google/protobuf/GeneratedMessageLite;",
        ">",
        "Lcom/sgiggle/messaging/Message;"
    }
.end annotation


# instance fields
.field private m_className:Ljava/lang/String;

.field private m_clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private m_payload:Lcom/google/protobuf/GeneratedMessageLite;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTPAY",
            "LOAD;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sgiggle/messaging/Message;-><init>()V

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/google/protobuf/GeneratedMessageLite;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTPAY",
            "LOAD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sgiggle/messaging/Message;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;

    .line 38
    iget-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_className:Ljava/lang/String;

    .line 40
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_className:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_clazz:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v0

    .line 42
    const/16 v0, 0x50

    const-string v1, "j: failed to determine class for payload."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x50

    .line 98
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_clazz:Ljava/lang/Class;

    const-string v2, "getBase"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 100
    :try_start_1
    iget-object v2, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    .line 110
    :goto_0
    return-object v0

    .line 102
    :catch_0
    move-exception v0

    .line 103
    const/16 v1, 0x50

    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "j: getBase(1) caught exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    :goto_1
    move-object v0, v5

    .line 110
    goto :goto_0

    .line 104
    :catch_1
    move-exception v0

    .line 105
    const/16 v1, 0x50

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "j: getBase(2) caught exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 107
    :catch_2
    move-exception v0

    .line 108
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "j: getBase(3) caught exception: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->log(ILjava/lang/String;)V

    goto :goto_1
.end method

.method protected static makeBase(I)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 2
    .parameter

    .prologue
    .line 124
    const-wide/16 v0, 0x0

    invoke-static {p0, v0, v1}, Lcom/sgiggle/messaging/SerializableMessage;->makeBase(IJ)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method protected static makeBase(IJ)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 114
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 115
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter;->getNextSequenceId()J

    move-result-wide v0

    .line 117
    :goto_0
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->setSequenceId(J)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0

    :cond_0
    move-wide v0, p1

    goto :goto_0
.end method


# virtual methods
.method public clone()Lcom/sgiggle/messaging/Message;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/sgiggle/messaging/SerializableMessage;->clone()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    return-object v0
.end method

.method public deserialize([B)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    .line 74
    const/4 v1, 0x0

    const-class v2, [B

    aput-object v2, v0, v1

    .line 75
    iget-object v1, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_clazz:Ljava/lang/Class;

    const-string v2, "parseFrom"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 77
    const/4 v1, 0x1

    :try_start_1
    new-array v1, v1, [Ljava/lang/Object;

    .line 78
    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 81
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/GeneratedMessageLite;

    iput-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    return-void

    .line 82
    :catch_0
    move-exception v0

    .line 83
    :try_start_2
    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    .line 91
    :catch_1
    move-exception v0

    .line 92
    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84
    :catch_2
    move-exception v0

    .line 85
    :try_start_3
    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :catch_3
    move-exception v0

    .line 87
    new-instance v1, Ljava/lang/Error;

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_1
.end method

.method public getSequenceId()J
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/sgiggle/messaging/SerializableMessage;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getSequenceId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sgiggle/messaging/SerializableMessage;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getType()I

    move-result v0

    return v0
.end method

.method public payload()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TTPAY",
            "LOAD;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;

    return-object v0
.end method

.method public serialize()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sgiggle/messaging/SerializableMessage;->m_payload:Lcom/google/protobuf/GeneratedMessageLite;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/messaging/SerializableMessage;->getType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/messaging/MessageRouter;->getMessageName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
