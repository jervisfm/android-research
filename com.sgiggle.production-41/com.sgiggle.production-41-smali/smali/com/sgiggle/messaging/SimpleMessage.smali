.class public abstract Lcom/sgiggle/messaging/SimpleMessage;
.super Lcom/sgiggle/messaging/Message;
.source "SimpleMessage.java"


# instance fields
.field private m_sequenceId:J

.field private m_type:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .parameter

    .prologue
    .line 18
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sgiggle/messaging/SimpleMessage;-><init>(IJ)V

    .line 19
    return-void
.end method

.method public constructor <init>(IJ)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sgiggle/messaging/Message;-><init>()V

    .line 10
    iput p1, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_type:I

    .line 11
    iput-wide p2, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    .line 12
    iget-wide v0, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 13
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter;->getNextSequenceId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    .line 15
    :cond_0
    return-void
.end method


# virtual methods
.method public abstract clone()Lcom/sgiggle/messaging/Message;
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/sgiggle/messaging/SimpleMessage;->clone()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    return-object v0
.end method

.method public deserialize([B)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Ljava/lang/Error;

    const-string v1, "SimpleMessage does not support serialization."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSequenceId()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_type:I

    return v0
.end method

.method public serialize()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Error;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Ljava/lang/Error;

    const-string v1, "SimpleMessage does not support serialization."

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSequenceId(J)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-wide p1, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    .line 48
    return-void
.end method

.method public setType(I)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput p1, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_type:I

    .line 36
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "t="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_type:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; s="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sgiggle/messaging/SimpleMessage;->m_sequenceId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
