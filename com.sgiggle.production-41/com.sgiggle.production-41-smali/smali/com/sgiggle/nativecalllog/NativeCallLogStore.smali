.class public Lcom/sgiggle/nativecalllog/NativeCallLogStore;
.super Ljava/lang/Object;
.source "NativeCallLogStore.java"


# static fields
.field private static final CALL_TYPE_INBOUND_CONNECTED:I = 0x0

.field private static final CALL_TYPE_INBOUND_MISSED:I = 0x1

.field private static final CALL_TYPE_OUTBOUND_CONNECTED:I = 0x2

.field private static final CALL_TYPE_OUTBOUND_NOT_ANSWERED:I = 0x3

.field private static final CALL_TYPE_UNKNOWN:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Tango.NativeCallLogStore"

.field private static s_contentResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNativeCallLogEntries()[Lcom/sgiggle/nativecalllog/NativeCallLogEntry;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 37
    const-string v0, "Tango.NativeCallLogStore"

    const-string v1, "getNativeCallLogEntries()..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    sget-object v0, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 40
    const-string v0, "Tango.NativeCallLogStore"

    const-string v1, "getNativeCallLogEntries(): s_contentResolver is null. Return NULL."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 83
    :goto_0
    return-object v0

    .line 44
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 46
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "number"

    aput-object v0, v2, v10

    const/4 v0, 0x1

    const-string v1, "type"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "date"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "duration"

    aput-object v1, v2, v0

    .line 53
    const-string v5, "date DESC"

    .line 55
    sget-object v0, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->s_contentResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_2

    .line 65
    const-string v1, "number"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 66
    const-string v2, "type"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 67
    const-string v3, "date"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 68
    const-string v4, "duration"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 70
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 72
    :cond_1
    new-instance v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;

    invoke-direct {v5}, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;-><init>()V

    .line 73
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;->phoneNumber:Ljava/lang/String;

    .line 74
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;->startTime:J

    .line 75
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    iput-wide v7, v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;->duration:J

    .line 76
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iget-wide v8, v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;->duration:J

    invoke-static {v7, v8, v9}, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->translateCallType(IJ)I

    move-result v7

    iput v7, v5, Lcom/sgiggle/nativecalllog/NativeCallLogEntry;->callType:I

    .line 77
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-nez v5, :cond_1

    .line 82
    :cond_2
    const-string v0, "Tango.NativeCallLogStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getNativeCallLogEntries(): loaded call log entries = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-array v0, v10, [Lcom/sgiggle/nativecalllog/NativeCallLogEntry;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/nativecalllog/NativeCallLogEntry;

    goto/16 :goto_0
.end method

.method private static translateCallType(IJ)I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 96
    packed-switch p0, :pswitch_data_0

    .line 110
    const/4 v0, 0x4

    :goto_0
    return v0

    .line 97
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 101
    :pswitch_2
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 103
    const/4 v0, 0x2

    goto :goto_0

    .line 107
    :cond_0
    const/4 v0, 0x3

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 22
    if-nez p0, :cond_1

    .line 23
    const-string v0, "Tango.NativeCallLogStore"

    const-string v1, "updateContext: context is unexpectedly null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 27
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->s_contentResolver:Landroid/content/ContentResolver;

    .line 28
    sget-object v0, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->s_contentResolver:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    .line 29
    const-string v0, "Tango.NativeCallLogStore"

    const-string v1, "updateContext: getContextResolver() returned null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
