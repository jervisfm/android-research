.class public Lcom/sgiggle/network/Network;
.super Landroid/content/BroadcastReceiver;
.source "Network.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "com.sgiggle.network.Network"

.field private static m_beingEnforced:Z

.field private static m_customWifiSetting:Z

.field private static m_networkInUse:Landroid/net/NetworkInfo;

.field private static m_wifiInUse:Landroid/net/wifi/WifiInfo;

.field private static m_wifiManager:Landroid/net/wifi/WifiManager;

.field private static s_connectivityManager:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 37
    sput-object v1, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    .line 38
    sput-boolean v0, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z

    .line 39
    sput-boolean v0, Lcom/sgiggle/network/Network;->m_beingEnforced:Z

    .line 40
    sput-object v1, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static declared-synchronized enforce3GWhileInCall()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 163
    const-class v0, Lcom/sgiggle/network/Network;

    monitor-enter v0

    :try_start_0
    sget-boolean v1, Lcom/sgiggle/network/Network;->m_beingEnforced:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    move v1, v4

    .line 179
    :goto_0
    monitor-exit v0

    return v1

    .line 167
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/sgiggle/network/Network;->getCustomerWifiSetting()Z

    move-result v1

    sput-boolean v1, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z

    .line 169
    sget-object v1, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eq v1, v4, :cond_1

    .line 172
    :try_start_2
    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 173
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sgiggle/network/Network;->m_beingEnforced:Z

    .line 174
    const-string v1, "com.sgiggle.network.Network"

    const-string v2, "enforce3GInCall disabled Wifi"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_1
    :goto_1
    move v1, v4

    .line 179
    goto :goto_0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    :try_start_3
    const-string v2, "com.sgiggle.network.Network"

    const-string v3, "Caught RuntimeException exception on disable Wifi service: "

    invoke-static {v2, v3, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 163
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getCustomerWifiSetting()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 201
    const-class v0, Lcom/sgiggle/network/Network;

    monitor-enter v0

    const/4 v1, 0x0

    .line 202
    :try_start_0
    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    monitor-exit v0

    return v1

    .line 206
    :cond_1
    :try_start_1
    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-eqz v2, :cond_2

    move v1, v4

    .line 208
    goto :goto_0

    .line 210
    :cond_2
    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiState()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :cond_3
    move v1, v4

    .line 212
    goto :goto_0

    .line 201
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized getNetworkStatus()I
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 148
    const-class v0, Lcom/sgiggle/network/Network;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    if-nez v1, :cond_0

    .line 149
    const-string v1, "com.sgiggle.network.Network"

    const-string v2, "FATAL: s_connectivityManager() = null"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v1, v3

    .line 158
    :goto_0
    monitor-exit v0

    return v1

    .line 153
    :cond_0
    :try_start_1
    sget-object v1, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-nez v1, :cond_1

    .line 154
    const-string v1, "com.sgiggle.network.Network"

    const-string v2, "FATAL: getActiveNetworkInfo() = null"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v3

    .line 155
    goto :goto_0

    .line 158
    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized restoreCustomerWifiSettingAfterCall()Z
    .locals 4

    .prologue
    .line 185
    const-class v0, Lcom/sgiggle/network/Network;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-boolean v1, Lcom/sgiggle/network/Network;->m_beingEnforced:Z

    .line 186
    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    sget-boolean v2, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v1, v2, :cond_0

    .line 189
    :try_start_1
    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    sget-boolean v2, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z

    invoke-virtual {v1, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 190
    const-string v1, "com.sgiggle.network.Network"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restore CustomerWifiSettingAfterCall() to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 196
    :cond_0
    :goto_0
    const/4 v1, 0x1

    monitor-exit v0

    return v1

    .line 191
    :catch_0
    move-exception v1

    .line 192
    :try_start_2
    const-string v2, "com.sgiggle.network.Network"

    const-string v3, "Caught RuntimeException exception on restoring Wifi service: "

    invoke-static {v2, v3, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized updateContext(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    .line 46
    const-class v1, Lcom/sgiggle/network/Network;

    monitor-enter v1

    if-nez p0, :cond_1

    .line 47
    :try_start_0
    const-string v0, "com.sgiggle.network.Network"

    const-string v2, "FATAL: updateContext(context = null)"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 49
    :cond_1
    :try_start_1
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    sput-object v0, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    if-eqz v0, :cond_2

    const-string v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    sput-object v0, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_3

    .line 52
    :cond_2
    const-string v0, "com.sgiggle.network.Network"

    const-string v2, "FATAL: getSystemService() = null"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 55
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/sgiggle/network/Network;->getCustomerWifiSetting()Z

    move-result v0

    sput-boolean v0, Lcom/sgiggle/network/Network;->m_customWifiSetting:Z

    .line 56
    sget-object v0, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    .line 57
    sget-object v0, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connectivity Manager set "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    sget-object v2, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 62
    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    sput-object v2, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "Wifi "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    :cond_4
    const-string v2, "com.sgiggle.network.Network"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {v0}, Lcom/sgiggle/util/FileLogger;->toFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized isSameWifiAccessPoint(Landroid/net/wifi/WifiInfo;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 129
    monitor-enter p0

    .line 130
    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 138
    :goto_0
    monitor-exit p0

    return v0

    .line 134
    :cond_1
    :try_start_1
    sget-object v0, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 136
    const/4 v0, 0x1

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 80
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sgiggle/network/Network;->s_connectivityManager:Landroid/net/ConnectivityManager;

    if-nez v0, :cond_1

    .line 81
    const-string v0, "com.sgiggle.network.Network"

    const-string v1, "Ignoring pre-init network event (normal during reboot)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 86
    :cond_1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-string v0, "com.sgiggle.network.Network"

    const-string v1, "Handling CONNECTIVITY_ACTION"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const-string v0, "Handling CONNECTIVITY_ACTION"

    invoke-static {v0}, Lcom/sgiggle/util/FileLogger;->toFile(Ljava/lang/String;)V

    .line 89
    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    .line 91
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 93
    sget-object v1, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    if-eqz v1, :cond_2

    .line 95
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "old network "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 96
    const-string v2, "com.sgiggle.network.Network"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-static {v1}, Lcom/sgiggle/util/FileLogger;->toFile(Ljava/lang/String;)V

    .line 99
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send network changed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 100
    sput-object v0, Lcom/sgiggle/network/Network;->m_networkInUse:Landroid/net/NetworkInfo;

    .line 101
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3

    .line 103
    sget-object v0, Lcom/sgiggle/network/Network;->m_wifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Wifi "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    invoke-virtual {v1}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 110
    :goto_1
    const-string v1, "com.sgiggle.network.Network"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    invoke-static {v0}, Lcom/sgiggle/util/FileLogger;->toFile(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/sgiggle/network/Network;->sendNetworkChangeMessage()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 107
    :cond_3
    const/4 v0, 0x0

    :try_start_2
    sput-object v0, Lcom/sgiggle/network/Network;->m_wifiInUse:Landroid/net/wifi/WifiInfo;

    move-object v0, v1

    goto :goto_1

    .line 116
    :cond_4
    const-string v1, "ignored unconnected network change "

    .line 117
    if-eqz v0, :cond_5

    .line 118
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    :goto_2
    const-string v1, "com.sgiggle.network.Network"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-static {v0}, Lcom/sgiggle/util/FileLogger;->toFile(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public native sendNetworkChangeMessage()V
.end method
