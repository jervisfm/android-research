.class final Lcom/sgiggle/pjmedia/AudioModeWrapper$1;
.super Landroid/content/BroadcastReceiver;
.source "AudioModeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/pjmedia/AudioModeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 410
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/16 v4, 0x2b

    const/4 v3, 0x0

    .line 413
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 414
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Action received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 415
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 416
    const-string v0, "state"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_2

    .line 417
    const-string v0, "Headset disconnected"

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 418
    invoke-static {v3}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->access$002(Z)Z

    .line 421
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->access$100()Z

    move-result v0

    if-nez v0, :cond_0

    .line 422
    invoke-static {v5}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->enableSpeakerphone(Z)V

    .line 430
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->do_notify()V

    .line 438
    :cond_1
    :goto_1
    return-void

    .line 425
    :cond_2
    const-string v0, "Headset connected"

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 426
    invoke-static {v5}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->access$002(Z)Z

    .line 427
    invoke-static {v3}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->enableSpeakerphone(Z)V

    goto :goto_0

    .line 431
    :cond_3
    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 434
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->access$200()Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INCALL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    if-ne v0, v1, :cond_1

    .line 435
    invoke-virtual {p0}, Lcom/sgiggle/pjmedia/AudioModeWrapper$1;->abortBroadcast()V

    goto :goto_1
.end method
