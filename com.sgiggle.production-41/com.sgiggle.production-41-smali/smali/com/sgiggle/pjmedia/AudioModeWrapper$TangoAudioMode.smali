.class final enum Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
.super Ljava/lang/Enum;
.source "AudioModeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/pjmedia/AudioModeWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TangoAudioMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_INCALL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_INVALID:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_LOCALDEMO:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_NORMAL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_PLAYBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_RING:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field public static final enum AM_RINGBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;


# instance fields
.field private final m_value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 53
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_NORMAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_NORMAL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 54
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_INCALL"

    invoke-direct {v0, v1, v5, v5}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INCALL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 55
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_RING"

    invoke-direct {v0, v1, v6, v6}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RING:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 56
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_RINGBACK"

    invoke-direct {v0, v1, v7, v7}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RINGBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 57
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_PLAYBACK"

    invoke-direct {v0, v1, v8, v8}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_PLAYBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 58
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_LOCALDEMO"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_LOCALDEMO:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 59
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    const-string v1, "AM_INVALID"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INVALID:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 52
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_NORMAL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INCALL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RING:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RINGBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_PLAYBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_LOCALDEMO:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INVALID:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->$VALUES:[Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->m_value:I

    .line 65
    return-void
.end method

.method public static parse(I)Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
    .locals 1
    .parameter

    .prologue
    .line 72
    packed-switch p0, :pswitch_data_0

    .line 79
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INVALID:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    :goto_0
    return-object v0

    .line 73
    :pswitch_0
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_NORMAL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 74
    :pswitch_1
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INCALL:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 75
    :pswitch_2
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RING:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 76
    :pswitch_3
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_RINGBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 77
    :pswitch_4
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_PLAYBACK:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 78
    :pswitch_5
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_LOCALDEMO:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
    .locals 1
    .parameter

    .prologue
    .line 52
    const-class v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->$VALUES:[Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    invoke-virtual {v0}, [Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    return-object v0
.end method


# virtual methods
.method public asIntValue()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->m_value:I

    return v0
.end method
