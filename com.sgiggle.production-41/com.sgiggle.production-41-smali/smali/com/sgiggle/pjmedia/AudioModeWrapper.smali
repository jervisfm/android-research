.class public Lcom/sgiggle/pjmedia/AudioModeWrapper;
.super Ljava/lang/Object;
.source "AudioModeWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/pjmedia/AudioModeWrapper$2;,
        Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
    }
.end annotation


# static fields
.field static final TAG:I = 0x2b

.field private static final mReceiver:Landroid/content/BroadcastReceiver;

.field private static sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

.field private static sContext:Landroid/content/Context;

.field private static sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

.field private static sHasEarpiece:Z

.field private static sHasHeadset:Z

.field private static sIsMicrophoneMuted:Z

.field private static volatile sIsOffHook:Z

.field private static sSpeakerMute:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    .line 23
    sput-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    .line 24
    sput-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsMicrophoneMuted:Z

    .line 26
    sput-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    .line 84
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->AM_INVALID:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 410
    new-instance v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$1;

    invoke-direct {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper$1;-><init>()V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method private static SamsungEarpieceEnableWorkaround(Landroid/media/AudioManager;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x2b

    .line 466
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D700"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 467
    const-string v0, "AudioManager.setMode=MODE_IN_CALL"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 468
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->setMode(I)V

    .line 469
    const-string v0, "AudioManager.setSpeakerphoneOn=false"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 470
    invoke-virtual {p0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 471
    const-string v0, "AudioManager.setMode=MODE_NORMAL"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 472
    invoke-virtual {p0, v3}, Landroid/media/AudioManager;->setMode(I)V

    .line 474
    :cond_0
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .parameter

    .prologue
    .line 18
    sput-boolean p0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 18
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    return v0
.end method

.method static synthetic access$200()Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    return-object v0
.end method

.method public static native do_notify()V
.end method

.method public static enableSpeakerphone(Z)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x2b

    .line 248
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enableSpeakerphone("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 249
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 250
    const-string v0, "setMode called while sContext is null"

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isInPSTNCall()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 260
    const-string v0, "Ignoring call to enableSpeakerphone because in PSTN call"

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    goto :goto_0

    .line 264
    :cond_2
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableSpeakerphone: sHasHeadset = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sHasEarpiece = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 268
    if-nez p0, :cond_3

    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    if-nez v1, :cond_4

    .line 269
    :cond_3
    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->routeToSpeakerPhone(Landroid/media/AudioManager;)V

    goto :goto_0

    .line 270
    :cond_4
    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    if-nez v1, :cond_5

    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    if-eqz v1, :cond_6

    .line 271
    :cond_5
    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->routeToEarpieceOrHeadset(Landroid/media/AudioManager;)V

    goto :goto_0

    .line 273
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableSpeakerphone does not have earpiece: enable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sSpeakerMute = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 278
    if-eqz p0, :cond_7

    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    if-eqz v1, :cond_7

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioManager.setStreamMute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " false"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 280
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v1

    invoke-virtual {v0, v1, v4}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 281
    invoke-static {v4}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->setAudioTrackOutputMute(Z)V

    .line 282
    sput-boolean v4, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    goto/16 :goto_0

    .line 283
    :cond_7
    if-nez p0, :cond_0

    sget-boolean v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    if-nez v1, :cond_0

    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioManager.setStreamMute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " true"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 285
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v1

    invoke-virtual {v0, v1, v5}, Landroid/media/AudioManager;->setStreamMute(IZ)V

    .line 286
    invoke-static {v5}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->setAudioTrackOutputMute(Z)V

    .line 287
    sput-boolean v5, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sSpeakerMute:Z

    goto/16 :goto_0
.end method

.method public static getAudioSource()I
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 373
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 374
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "MB860"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID BIONIC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 376
    :cond_0
    const/4 v0, 0x5

    .line 390
    :goto_0
    return v0

    .line 379
    :cond_1
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 381
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS840"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 382
    goto :goto_0

    .line 384
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 386
    :cond_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID RAZR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 387
    const/4 v0, 0x7

    goto :goto_0

    :cond_4
    move v0, v2

    .line 390
    goto :goto_0
.end method

.method public static getMicMute()Z
    .locals 1

    .prologue
    .line 313
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsMicrophoneMuted:Z

    return v0
.end method

.method public static getMode()I
    .locals 1

    .prologue
    .line 234
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->asIntValue()I

    move-result v0

    return v0
.end method

.method private static getModelSpecificAudioMangerMode()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 338
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SHW-M110S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T959V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 340
    :cond_0
    const/4 v0, 0x2

    .line 343
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_1

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "MZ609"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "DROID RAZR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ADR6410LVW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    move v0, v3

    .line 352
    :cond_1
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x9

    if-lt v1, v2, :cond_2

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9003"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v3

    .line 358
    :cond_2
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SAMSUNG-SGH-I997"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    move v0, v3

    .line 363
    :cond_3
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC One X"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 364
    const/16 v0, 0x2b

    const-string v1, "HTC One X MODE_NORMAL workaround"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    move v0, v4

    .line 368
    :cond_4
    return v0

    :cond_5
    move v0, v4

    goto :goto_0
.end method

.method public static getStreamType()I
    .locals 2

    .prologue
    .line 396
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    .line 397
    const/4 v0, 0x3

    .line 400
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasBluetoothRouted()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x0

    return v0
.end method

.method public static hasEarpiece()Z
    .locals 1

    .prologue
    .line 321
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    return v0
.end method

.method private static hasHardwareAec()Z
    .locals 1

    .prologue
    .line 483
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isGalaxyS2()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isGalaxyS3()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static hasHardwareAgc()Z
    .locals 1

    .prologue
    .line 480
    const/4 v0, 0x0

    return v0
.end method

.method private static hasHardwareNs()Z
    .locals 1

    .prologue
    .line 477
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isGalaxyS2()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isGalaxyS3()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static hasHeadset()Z
    .locals 1

    .prologue
    .line 317
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    return v0
.end method

.method private static isGalaxyS2()Z
    .locals 5

    .prologue
    const/16 v4, 0xf

    const/16 v3, 0xa

    .line 487
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 488
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 489
    const-string v2, "GT-I9100"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    if-eq v1, v4, :cond_5

    :cond_0
    const-string v2, "GT-N7000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    if-eq v1, v4, :cond_5

    :cond_1
    const-string v2, "SAMSUNG-SGH-I717"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    if-eq v1, v3, :cond_5

    :cond_2
    const-string v2, "SPH-D710"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    if-eq v1, v3, :cond_5

    :cond_3
    const-string v2, "SAMSUNG-SGH-I727"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_4

    if-eq v1, v4, :cond_5

    :cond_4
    const-string v2, "GT-I9100"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_6

    if-ne v1, v3, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isGalaxyS3()Z
    .locals 4

    .prologue
    const/16 v3, 0xf

    .line 527
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 528
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 530
    const-string v2, "SAMSUNG-SGH-I747"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    if-eq v1, v3, :cond_2

    :cond_0
    const-string v2, "SGH-T999"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_1

    if-eq v1, v3, :cond_2

    :cond_1
    const-string v2, "GT-I9300"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    if-ne v1, v3, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static isInPSTNCall()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 164
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 165
    if-nez v0, :cond_0

    move v0, v2

    .line 168
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public static isOffHook()Z
    .locals 1

    .prologue
    .line 29
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    return v0
.end method

.method public static isSpeakerphoneOn()Z
    .locals 2

    .prologue
    .line 303
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 304
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    .line 305
    return v0
.end method

.method public static pstnSessionIdle()V
    .locals 2

    .prologue
    .line 40
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    if-nez v0, :cond_0

    .line 41
    const/16 v0, 0x2b

    const-string v1, "Already in PSTN idle state. Ignoring."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 47
    :goto_0
    return-void

    .line 44
    :cond_0
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->setModeInternal(Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;)V

    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    goto :goto_0
.end method

.method public static pstnSessionOffHook()V
    .locals 2

    .prologue
    .line 32
    sget-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    if-eqz v0, :cond_0

    .line 33
    const/16 v0, 0x2b

    const-string v1, "Already in PSTN off-hook mode. Ignoring."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 38
    :goto_0
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsOffHook:Z

    goto :goto_0
.end method

.method protected static registerAudioTrackWrapper(Lcom/sgiggle/pjmedia/AudioTrackWrapper;)V
    .locals 2
    .parameter

    .prologue
    .line 147
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    if-eqz v0, :cond_0

    .line 148
    const/16 v0, 0x2b

    const-string v1, "m_audioTrackWrapper was already set"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 150
    :cond_0
    sput-object p0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    .line 151
    return-void
.end method

.method public static removeFromPreviousContext()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 140
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 141
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    .line 142
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    .line 144
    :cond_0
    return-void
.end method

.method private static routeToEarpieceOrHeadset(Landroid/media/AudioManager;)V
    .locals 2
    .parameter

    .prologue
    const/16 v1, 0x2b

    .line 452
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isInPSTNCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    const-string v0, "Ignoring call to routeToEarpieceOrHeadset because in PSTN call"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 463
    :goto_0
    return-void

    .line 457
    :cond_0
    const-string v0, "Route to earpiece or headphone"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 459
    invoke-static {p0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->SamsungEarpieceEnableWorkaround(Landroid/media/AudioManager;)V

    .line 461
    const-string v0, "AudioManager.setSpeakerphoneOn=false"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 462
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0
.end method

.method private static routeToSpeakerPhone(Landroid/media/AudioManager;)V
    .locals 2
    .parameter

    .prologue
    const/16 v1, 0x2b

    .line 442
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isInPSTNCall()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    const-string v0, "Ignoring call to routeToSpeakerPhone because in PSTN call"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 449
    :goto_0
    return-void

    .line 447
    :cond_0
    const-string v0, "AudioManager.setSpeakerphoneOn=true"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 448
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    goto :goto_0
.end method

.method private static setAudioTrackOutputMute(Z)V
    .locals 2
    .parameter

    .prologue
    .line 294
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    if-nez v0, :cond_0

    .line 295
    const/16 v0, 0x2b

    const-string v1, "sAudioTrackWrapper is null. Call registerAudioTrackWrapper before calling setAudioTrackOutputMute"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 300
    :goto_0
    return-void

    .line 299
    :cond_0
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    invoke-virtual {v0, p0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->setSpeakerMute(Z)V

    goto :goto_0
.end method

.method public static setMicMute(Z)V
    .locals 0
    .parameter

    .prologue
    .line 309
    sput-boolean p0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sIsMicrophoneMuted:Z

    .line 310
    return-void
.end method

.method public static setMode(I)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0x2b

    .line 173
    invoke-static {p0}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->parse(I)Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    move-result-object v0

    .line 175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMode called. Requested Tango audio mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 178
    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 179
    const-string v0, "setMode called while sContext is null"

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 189
    :goto_0
    return-void

    .line 183
    :cond_0
    sget-object v1, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    if-ne v1, v0, :cond_1

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Tried to set audio mode to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " when already in this mode. Request ignored."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    goto :goto_0

    .line 188
    :cond_1
    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->setModeInternal(Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;)V

    goto :goto_0
.end method

.method private static setModeInternal(Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;)V
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x2b

    const/4 v2, 0x0

    .line 194
    .line 195
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper$2;->$SwitchMap$com$sgiggle$pjmedia$AudioModeWrapper$TangoAudioMode:[I

    invoke-virtual {p0}, Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 214
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown mode value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    move v1, v2

    .line 219
    :goto_0
    sput-object p0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sCurrentMode:Lcom/sgiggle/pjmedia/AudioModeWrapper$TangoAudioMode;

    .line 221
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isInPSTNCall()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Droid"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioManager.setMode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 226
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setMode(I)V

    .line 231
    :cond_0
    :goto_1
    return-void

    .line 197
    :pswitch_0
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getModelSpecificAudioMangerMode()I

    move-result v0

    move v1, v0

    .line 198
    goto :goto_0

    :pswitch_1
    move v1, v2

    .line 201
    goto :goto_0

    .line 204
    :pswitch_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    .line 205
    const/4 v0, 0x3

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 208
    goto :goto_0

    .line 211
    :pswitch_3
    const/4 v0, 0x1

    move v1, v0

    .line 212
    goto :goto_0

    .line 229
    :cond_2
    const-string v0, "AudioManager.setMode is not called because in PSTN call"

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected static unregisterAudioTrackWrapper(Lcom/sgiggle/pjmedia/AudioTrackWrapper;)V
    .locals 2
    .parameter

    .prologue
    const/16 v1, 0x2b

    .line 154
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    if-nez v0, :cond_1

    .line 155
    const-string v0, "m_audioTrackWrapper was never set"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 160
    :cond_0
    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    .line 161
    return-void

    .line 156
    :cond_1
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sAudioTrackWrapper:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    if-eq p0, v0, :cond_0

    .line 157
    const-string v0, "trying to unregister a different AudioTrackWrapper instance than the one was registered"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    goto :goto_0
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 8
    .parameter

    .prologue
    const/16 v7, 0x400

    const/16 v6, 0x258

    const/16 v5, 0x2b

    const/4 v4, 0x0

    .line 87
    sput-object p0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    .line 88
    sput-boolean v4, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasHeadset:Z

    .line 90
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 91
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 92
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 94
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 95
    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 96
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->setPriority(I)V

    .line 100
    sget-object v2, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    sget-object v3, Lcom/sgiggle/pjmedia/AudioModeWrapper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 102
    sget-object v0, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sContext:Landroid/content/Context;

    sget-object v2, Lcom/sgiggle/pjmedia/AudioModeWrapper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 105
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-gt v0, v1, :cond_0

    .line 106
    sput-boolean v4, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    .line 110
    :cond_0
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_5

    .line 111
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 112
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 113
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 114
    if-ne v1, v7, :cond_1

    if-eq v0, v6, :cond_2

    :cond_1
    if-ne v0, v7, :cond_3

    if-ne v1, v6, :cond_3

    .line 115
    :cond_2
    sput-boolean v4, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    .line 122
    :cond_3
    :goto_0
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 123
    if-eqz v0, :cond_4

    .line 125
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 126
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v2

    invoke-virtual {v0, v2, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 127
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AudioManager.setStreamVolume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 129
    :cond_4
    const-string v1, "AudioManager.setMode=MODE_NORMAL"

    invoke-static {v5, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 130
    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->setMode(I)V

    .line 131
    return-void

    .line 117
    :cond_5
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Ideos S7"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 118
    sput-boolean v4, Lcom/sgiggle/pjmedia/AudioModeWrapper;->sHasEarpiece:Z

    goto :goto_0
.end method
