.class Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;
.super Ljava/lang/Thread;
.source "AudioRecordWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/pjmedia/AudioRecordWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CaptureThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;


# direct methods
.method public constructor <init>(Lcom/sgiggle/pjmedia/AudioRecordWrapper;)V
    .locals 1
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    .line 45
    const-string v0, "Audio Rec"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/16 v4, 0x16

    .line 49
    const/16 v0, -0xe

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 52
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget v0, v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_streamMode:I

    sget-object v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->PLAYBACKANDRECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    invoke-virtual {v1}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 53
    sget-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->audioSignalingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget v0, v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_frameSize:I

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget-boolean v1, v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_flag_stop:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget-object v1, v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 61
    iget-object v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget-object v1, v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget v2, v2, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_frameSize:I

    invoke-virtual {v1, v0, v2}, Landroid/media/AudioRecord;->read(Ljava/nio/ByteBuffer;I)I

    move-result v1

    .line 62
    if-gez v1, :cond_2

    .line 63
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read error "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    goto :goto_0

    .line 64
    :cond_2
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isOffHook()Z

    move-result v2

    if-nez v2, :cond_1

    .line 65
    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget v3, v3, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_pjmediaStreamPtr:I

    invoke-virtual {v2, v0, v1, v3}, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->sendBytesToPJMedia(Ljava/nio/ByteBuffer;II)I

    move-result v1

    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->this$0:Lcom/sgiggle/pjmedia/AudioRecordWrapper;

    iget v2, v2, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_frameSize:I

    if-eq v1, v2, :cond_1

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bailing, sendBytes returned "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 71
    :cond_3
    return-void
.end method
