.class final enum Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;
.super Ljava/lang/Enum;
.source "AudioRecordWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/pjmedia/AudioRecordWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "StreamMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

.field public static final enum INVALID:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

.field public static final enum PLAYBACK:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

.field public static final enum PLAYBACKANDRECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

.field public static final enum RECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    const-string v1, "PLAYBACK"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->PLAYBACK:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    .line 22
    new-instance v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    const-string v1, "RECORD"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->RECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    .line 23
    new-instance v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    const-string v1, "PLAYBACKANDRECORD"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->PLAYBACKANDRECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    .line 24
    new-instance v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    sget-object v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->PLAYBACK:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->RECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->PLAYBACKANDRECORD:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->$VALUES:[Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;
    .locals 1
    .parameter

    .prologue
    .line 20
    const-class v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->$VALUES:[Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    invoke-virtual {v0}, [Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    return-object v0
.end method
