.class public Lcom/sgiggle/pjmedia/AudioRecordWrapper;
.super Ljava/lang/Object;
.source "AudioRecordWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;,
        Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;
    }
.end annotation


# static fields
.field static final TAG:I = 0x16

.field public static audioSignalingSemaphore:Ljava/util/concurrent/Semaphore;


# instance fields
.field m_audioRecord:Landroid/media/AudioRecord;

.field m_captureThread:Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;

.field m_flag_stop:Z

.field m_frameSize:I

.field m_pjmediaStreamPtr:I

.field m_sampleRateInHz:I

.field m_streamMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    sput-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->audioSignalingSemaphore:Ljava/util/concurrent/Semaphore;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_streamMode:I

    .line 76
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    sget-object v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$StreamMode;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_streamMode:I

    .line 81
    const/16 v0, 0x16

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating AudioRecordWrapper. Sampling frequency: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " frame size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 84
    iput p1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    .line 85
    iput p2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_frameSize:I

    .line 86
    iput p4, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_pjmediaStreamPtr:I

    .line 87
    iput p3, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_streamMode:I

    .line 88
    return-void
.end method

.method private start()Z
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/16 v12, 0x16

    const/4 v11, 0x5

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 93
    iput-boolean v9, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_flag_stop:Z

    .line 94
    const/16 v3, 0x10

    .line 95
    const/4 v4, 0x2

    .line 101
    iget v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    invoke-static {v0, v3, v13}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v6

    .line 102
    mul-int/lit8 v0, v6, 0x3

    .line 104
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "YP-G"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "Samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_5

    .line 108
    mul-int/lit8 v0, v6, 0xa

    move v5, v0

    .line 112
    :goto_0
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getAudioSource()I

    move-result v1

    move v0, v9

    .line 116
    :goto_1
    if-ge v0, v11, :cond_1

    .line 118
    add-int/lit8 v7, v0, 0x1

    .line 120
    :try_start_0
    new-instance v0, Landroid/media/AudioRecord;

    iget v2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-ne v0, v10, :cond_4

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eq v0, v10, :cond_2

    .line 137
    invoke-static {}, Lcom/sgiggle/util/ClientCrashReporter;->getInstance()Lcom/sgiggle/util/ClientCrashReporter;

    move-result-object v0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v4, "Failed to initialze AudioRecord(%d, %d, %d, %d, %d). AudioRecord.State()="

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v9

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v13

    const/4 v1, 0x3

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v1

    const/4 v1, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v1

    iget-object v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v1}, Landroid/media/AudioRecord;->getState()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v11

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/sgiggle/util/ClientCrashReporter;->reportException(Ljava/lang/Exception;)V

    move v0, v9

    .line 164
    :goto_2
    return v0

    .line 122
    :catch_0
    move-exception v0

    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "start exception catched, failed to create new AudioTrack: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 125
    if-ne v7, v11, :cond_0

    .line 126
    invoke-static {}, Lcom/sgiggle/util/ClientCrashReporter;->getInstance()Lcom/sgiggle/util/ClientCrashReporter;

    move-result-object v2

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v6, "Construction of AudioRecord(%d, %d, %d, %d, %d) failed"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v9

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v13

    const/4 v1, 0x3

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v1, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2, v4}, Lcom/sgiggle/util/ClientCrashReporter;->reportException(Ljava/lang/Exception;)V

    move v0, v9

    .line 129
    goto :goto_2

    .line 143
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Creating AudioRecord audioSource="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sampling freq="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " buffer size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v5, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " samples ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    mul-int/lit16 v1, v5, 0x3e8

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    div-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " msec)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " minBufferSize="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    div-int/lit8 v1, v6, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 153
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I727"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_3

    .line 154
    const-string v0, "Samsung Skyrocket: workaround corrupted audio system state"

    invoke-static {v12, v0}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    .line 156
    const-wide/16 v0, 0x3e8

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 160
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 161
    const-string v0, "startRecording ended"

    invoke-static {v12, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 162
    new-instance v0, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;

    invoke-direct {v0, p0}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;-><init>(Lcom/sgiggle/pjmedia/AudioRecordWrapper;)V

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_captureThread:Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;

    .line 163
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_captureThread:Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->start()V

    move v0, v10

    .line 164
    goto/16 :goto_2

    .line 157
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_4
    move v0, v7

    goto/16 :goto_1

    :cond_5
    move v5, v0

    goto/16 :goto_0
.end method

.method private stop()V
    .locals 3

    .prologue
    const/16 v2, 0x16

    .line 169
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 170
    const-string v0, "stop was called but m_audioRecord was never initialized"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 195
    :goto_0
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->setMicMute(Z)V

    .line 179
    const/16 v0, 0x16

    const-string v1, "Calling stop"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 182
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_flag_stop:Z

    .line 184
    const/16 v0, 0x16

    const-string v1, "Calling join"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_captureThread:Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioRecordWrapper$CaptureThread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_1
    const-string v0, "Calling release"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 191
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 192
    const-string v0, "release ended"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_audioRecord:Landroid/media/AudioRecord;

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public delay()I
    .locals 2

    .prologue
    .line 201
    const/4 v0, 0x0

    .line 203
    iget v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    if-lez v1, :cond_0

    .line 204
    iget v0, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_frameSize:I

    div-int/lit8 v0, v0, 0x2

    mul-int/lit16 v0, v0, 0x3e8

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->m_sampleRateInHz:I

    div-int/2addr v0, v1

    .line 206
    :cond_0
    return v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 198
    return-void
.end method

.method public native sendBytesToPJMedia(Ljava/nio/ByteBuffer;II)I
.end method
