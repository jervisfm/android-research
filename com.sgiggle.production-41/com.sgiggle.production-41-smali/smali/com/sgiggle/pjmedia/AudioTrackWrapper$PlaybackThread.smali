.class Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;
.super Ljava/lang/Thread;
.source "AudioTrackWrapper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/pjmedia/AudioTrackWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PlaybackThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;


# direct methods
.method public constructor <init>(Lcom/sgiggle/pjmedia/AudioTrackWrapper;)V
    .locals 1
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    .line 47
    const-string v0, "Audio Play"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v8, 0x18

    const/4 v7, 0x0

    .line 51
    const/16 v0, -0xe

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 54
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v0, v0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    new-array v0, v0, [B

    .line 55
    iget-object v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v1, v1, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    new-array v1, v1, [B

    .line 57
    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v2, v2, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_streamMode:I

    sget-object v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->PLAYBACKANDRECORD:Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;

    invoke-virtual {v3}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 58
    const-string v2, "Waiting for record thread to start..."

    invoke-static {v8, v2}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 59
    sget-object v2, Lcom/sgiggle/pjmedia/AudioRecordWrapper;->audioSignalingSemaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 61
    :cond_0
    const-string v2, "Recording started!"

    invoke-static {v8, v2}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    move v2, v7

    .line 64
    :goto_0
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-boolean v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_1

    .line 77
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isOffHook()Z

    move-result v3

    if-nez v3, :cond_5

    .line 78
    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    iget-object v4, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v4, v4, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_pjmediaStreamPtr:I

    invoke-virtual {v2, v0, v3, v4}, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->getBytesFromPJMedia([BII)I

    move-result v2

    .line 80
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    if-eq v2, v3, :cond_2

    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Bailing, getBytes returned "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 105
    :cond_1
    return-void

    .line 85
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-boolean v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_mute:Z

    if-eqz v3, :cond_4

    .line 86
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v3, v1, v7, v2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v3

    move v9, v3

    move v3, v2

    move v2, v9

    .line 98
    :goto_1
    iget-object v4, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v4, v4, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v4

    .line 99
    iget-object v5, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v5, v5, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playPosition:I

    if-ge v4, v5, :cond_3

    .line 100
    iget-object v5, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iput v7, v5, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playPosition:I

    .line 102
    :cond_3
    iget-object v5, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v5, v5, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_bufferedPlaySamples:Ljava/util/concurrent/atomic/AtomicInteger;

    shr-int/lit8 v2, v2, 0x1

    iget-object v6, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget v6, v6, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playPosition:I

    sub-int v6, v4, v6

    sub-int/2addr v2, v6

    invoke-virtual {v5, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 103
    iget-object v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iput v4, v2, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playPosition:I

    move v2, v3

    .line 104
    goto :goto_0

    .line 88
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v3, v0, v7, v2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v3

    move v9, v3

    move v3, v2

    move v2, v9

    goto :goto_1

    .line 93
    :cond_5
    iget-object v3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->this$0:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    iget-object v3, v3, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v3, v1, v7, v2}, Landroid/media/AudioTrack;->write([BII)I

    move-result v3

    move v9, v3

    move v3, v2

    move v2, v9

    goto :goto_1
.end method
