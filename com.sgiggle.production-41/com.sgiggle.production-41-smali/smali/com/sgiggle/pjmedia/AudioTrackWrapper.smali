.class public Lcom/sgiggle/pjmedia/AudioTrackWrapper;
.super Ljava/lang/Object;
.source "AudioTrackWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;,
        Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;
    }
.end annotation


# static fields
.field static final TAG:I = 0x18

.field static m_instance:Lcom/sgiggle/pjmedia/AudioTrackWrapper;


# instance fields
.field m_audioTrack:Landroid/media/AudioTrack;

.field m_bufferedPlaySamples:Ljava/util/concurrent/atomic/AtomicInteger;

.field m_flag_running:Z

.field m_flag_stop:Z

.field m_frameMSec:J

.field m_frameSize:I

.field m_mute:Z

.field m_pjmediaStreamPtr:I

.field m_playPosition:I

.field m_playbackThread:Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;

.field m_req_start:Z

.field m_req_suspend:Z

.field m_sampleRateInHz:I

.field m_streamMode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    .line 30
    sget-object v0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_streamMode:I

    .line 32
    iput-boolean v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_running:Z

    .line 33
    iput-boolean v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_req_suspend:Z

    .line 34
    iput-boolean v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_req_start:Z

    .line 110
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    .line 30
    sget-object v0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->INVALID:Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$StreamMode;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_streamMode:I

    .line 32
    iput-boolean v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_running:Z

    .line 33
    iput-boolean v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_req_suspend:Z

    .line 34
    iput-boolean v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_req_start:Z

    .line 114
    iput p1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    .line 115
    iput p4, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_pjmediaStreamPtr:I

    .line 116
    iput p2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    .line 117
    iget v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    mul-int/lit16 v0, v0, 0x3e8

    div-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x2

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameMSec:J

    .line 118
    sput-object p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_instance:Lcom/sgiggle/pjmedia/AudioTrackWrapper;

    .line 119
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_bufferedPlaySamples:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 120
    iput v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playPosition:I

    .line 121
    iput p3, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_streamMode:I

    .line 122
    const/16 v0, 0x18

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "frame size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_frameMSec:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms); sampling frequency "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 124
    return-void
.end method

.method private start()Z
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/16 v13, 0x18

    const/4 v12, 0x5

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 127
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "Calling start while the playback in progress"

    invoke-static {v13, v0}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    .line 131
    :cond_0
    iput-boolean v10, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    .line 133
    const/4 v3, 0x4

    .line 134
    const/4 v4, 0x2

    .line 141
    iget v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    invoke-static {v0, v3, v14}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 145
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getStreamType()I

    move-result v1

    move v7, v10

    move v5, v0

    .line 147
    :goto_0
    if-ge v7, v12, :cond_3

    .line 151
    const/16 v0, 0x18

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Creating AudioTrack streamType="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " sampling freq="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v6, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " buffer size="

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    div-int/lit8 v6, v5, 0x2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " samples ("

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit16 v6, v5, 0x3e8

    div-int/lit8 v6, v6, 0x2

    iget v8, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    div-int/2addr v6, v8

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, " msec)"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 156
    new-instance v0, Landroid/media/AudioTrack;

    iget v2, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    .line 160
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    if-eq v0, v11, :cond_3

    .line 161
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected AudioTrack state %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v9}, Landroid/media/AudioTrack;->getState()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-static {v2, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start exception catched, failed to create new AudioTrack: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v13, v2}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 168
    add-int/lit8 v2, v7, 0x1

    .line 170
    const/4 v6, 0x3

    if-ne v2, v6, :cond_1

    .line 172
    iget v5, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    invoke-static {v5, v3, v14}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v5

    mul-int/lit8 v5, v5, 0x4

    .line 175
    :cond_1
    if-ne v2, v12, :cond_2

    .line 176
    invoke-static {}, Lcom/sgiggle/util/ClientCrashReporter;->getInstance()Lcom/sgiggle/util/ClientCrashReporter;

    move-result-object v2

    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v6, "Construction of AudioTrack(%d, %d, %d, %d, %d) failed"

    new-array v7, v12, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v10

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v11

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v7, v14

    const/4 v1, 0x3

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    const/4 v1, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {v2, v4}, Lcom/sgiggle/util/ClientCrashReporter;->reportException(Ljava/lang/Exception;)V

    move v0, v10

    .line 189
    :goto_1
    return v0

    :cond_2
    move v7, v2

    .line 182
    goto/16 :goto_0

    .line 186
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 187
    new-instance v0, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;

    invoke-direct {v0, p0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;-><init>(Lcom/sgiggle/pjmedia/AudioTrackWrapper;)V

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playbackThread:Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;

    .line 188
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playbackThread:Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->start()V

    move v0, v11

    .line 189
    goto :goto_1
.end method

.method private stop()V
    .locals 3

    .prologue
    const/16 v2, 0x18

    .line 193
    iget-boolean v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    const/16 v0, 0x18

    :try_start_0
    const-string v1, "Calling stop"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->stop()V

    .line 198
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_flag_stop:Z

    .line 199
    const/16 v0, 0x18

    const-string v1, "Calling join"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 200
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_playbackThread:Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;

    invoke-virtual {v0}, Lcom/sgiggle/pjmedia/AudioTrackWrapper$PlaybackThread;->join()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :goto_1
    const-string v0, "Calling release"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 206
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 207
    const-string v0, "release ended"

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_audioTrack:Landroid/media/AudioTrack;

    goto :goto_0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public delay()I
    .locals 2

    .prologue
    .line 219
    const/4 v0, 0x0

    .line 221
    iget v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    if-lez v1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_bufferedPlaySamples:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iget v1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_sampleRateInHz:I

    div-int/2addr v0, v1

    .line 225
    :cond_0
    return v0
.end method

.method public native getBytesFromPJMedia([BII)I
.end method

.method public release()V
    .locals 0

    .prologue
    .line 212
    return-void
.end method

.method public setSpeakerMute(Z)V
    .locals 0
    .parameter

    .prologue
    .line 215
    iput-boolean p1, p0, Lcom/sgiggle/pjmedia/AudioTrackWrapper;->m_mute:Z

    .line 216
    return-void
.end method
