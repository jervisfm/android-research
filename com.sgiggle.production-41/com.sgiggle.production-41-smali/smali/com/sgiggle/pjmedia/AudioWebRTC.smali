.class public Lcom/sgiggle/pjmedia/AudioWebRTC;
.super Ljava/lang/Object;
.source "AudioWebRTC.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native loadLibraries(Ljava/lang/String;)V
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 10
    const/16 v0, 0x8f

    const-string v1, "updateContext..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    .line 12
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 14
    invoke-static {v0}, Lcom/sgiggle/pjmedia/AudioWebRTC;->loadLibraries(Ljava/lang/String;)V

    .line 15
    return-void
.end method
