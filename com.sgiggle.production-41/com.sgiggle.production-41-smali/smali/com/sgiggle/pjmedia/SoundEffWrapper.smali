.class public Lcom/sgiggle/pjmedia/SoundEffWrapper;
.super Ljava/lang/Object;
.source "SoundEffWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;
    }
.end annotation


# static fields
.field static final TAG:I = 0x69

.field private static isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

.field private static sAsyncPlayer:Landroid/media/AsyncPlayer;

.field private static sContext:Landroid/content/Context;

.field private static sMediaPlayer:Landroid/media/MediaPlayer;

.field private static sPattern:[J

.field private static sResIDNewMessage:I

.field private static sResIDRingback:I

.field private static sResIDRingtone:I

.field private static sVibrator:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 238
    sput v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingtone:I

    .line 239
    sput v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingback:I

    .line 240
    sput v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDNewMessage:I

    .line 243
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sPattern:[J

    .line 244
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->NONE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    return-void

    .line 243
    :array_0
    .array-data 0x8
        0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xfat 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xfat 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
        0xfat 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method static synthetic access$000()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic access$002(Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;
    .locals 0
    .parameter

    .prologue
    .line 16
    sput-object p0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    return-object p0
.end method

.method static synthetic access$102(Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;)Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;
    .locals 0
    .parameter

    .prologue
    .line 16
    sput-object p0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    return-object p0
.end method

.method private static noVibrateInPSTNCall()Z
    .locals 2

    .prologue
    .line 80
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Bliss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Rhyme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Runnymede"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation XL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    :cond_1
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->isInPSTNCall()Z

    move-result v0

    .line 84
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized playFile([B)V
    .locals 4
    .parameter

    .prologue
    .line 114
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 116
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    .line 119
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 121
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 122
    const/4 v2, 0x0

    sput-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 123
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->NONE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    .line 126
    :cond_0
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    sput-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 127
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->FILE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    .line 130
    new-instance v2, Lcom/sgiggle/pjmedia/SoundEffWrapper$1;

    invoke-direct {v2}, Lcom/sgiggle/pjmedia/SoundEffWrapper$1;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 147
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 148
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 149
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepare()V

    .line 150
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 151
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 158
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 153
    :catch_0
    move-exception v1

    .line 155
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 114
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized playNewMessageSound()V
    .locals 2

    .prologue
    .line 162
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sget v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDNewMessage:I

    invoke-static {v1}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->playResouce(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 163
    monitor-exit v0

    return-void

    .line 162
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized playResouce(I)V
    .locals 4
    .parameter

    .prologue
    .line 166
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 169
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 171
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 172
    const/4 v1, 0x0

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 173
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->NONE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    .line 176
    :cond_0
    const/16 v1, 0x69

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playResouce "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 178
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    invoke-static {v1, p0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 179
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->FILE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    .line 181
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_2

    .line 185
    const/16 v1, 0x69

    const-string v2, "MediaPlayer.create failed"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 190
    :cond_2
    :try_start_1
    new-instance v1, Lcom/sgiggle/pjmedia/SoundEffWrapper$2;

    invoke-direct {v1}, Lcom/sgiggle/pjmedia/SoundEffWrapper$2;-><init>()V

    .line 204
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 205
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 206
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized playRingback()V
    .locals 4

    .prologue
    .line 89
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    if-eqz v1, :cond_1

    .line 91
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->FILE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    if-ne v1, v2, :cond_0

    .line 92
    invoke-static {}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->stop()V

    .line 94
    :cond_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_1

    .line 95
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    sget v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingback:I

    invoke-static {v1, v2}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v1

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 96
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_2

    .line 100
    const/16 v1, 0x69

    const-string v2, "MediaPlayer.create failed"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_1
    :goto_0
    monitor-exit v0

    return-void

    .line 103
    :cond_2
    :try_start_1
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 104
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Droid"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 105
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    const v2, 0x3c03126f

    const v3, 0x3c03126f

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 107
    :cond_3
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->start()V

    .line 108
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->RINGBACK:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized playRingtone()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 40
    const-class v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    if-eqz v0, :cond_5

    .line 42
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->FILE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    if-ne v0, v2, :cond_0

    .line 43
    invoke-static {}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->stop()V

    .line 46
    :cond_0
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_4

    .line 47
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    const-string v2, "audio"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 49
    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "HTC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_6

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "HTC Wildfire"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v4

    .line 51
    :goto_0
    if-nez v2, :cond_4

    invoke-static {}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->noVibrateInPSTNCall()Z

    move-result v2

    if-nez v2, :cond_4

    .line 53
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getVibrateSetting(I)I

    move-result v2

    .line 54
    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    .line 56
    if-ne v0, v6, :cond_1

    if-eq v2, v4, :cond_3

    :cond_1
    if-nez v0, :cond_2

    if-eq v2, v6, :cond_3

    :cond_2
    if-ne v0, v4, :cond_4

    .line 59
    :cond_3
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    const-string v2, "vibrator"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    sput-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    .line 60
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sPattern:[J

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate([JI)V

    .line 66
    :cond_4
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    if-nez v0, :cond_5

    .line 67
    new-instance v0, Landroid/media/AsyncPlayer;

    const-string v2, ""

    invoke-direct {v0, v2}, Landroid/media/AsyncPlayer;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    .line 68
    sget-object v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 69
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android.resource://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingtone:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 70
    sget-object v2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    sget-object v3, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;

    const/4 v4, 0x1

    const/4 v5, 0x2

    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/media/AsyncPlayer;->play(Landroid/content/Context;Landroid/net/Uri;ZI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    :cond_5
    monitor-exit v1

    return-void

    :cond_6
    move v2, v5

    .line 49
    goto :goto_0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized removeFromPreviousContext()V
    .locals 2

    .prologue
    .line 36
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    monitor-exit v0

    return-void

    .line 36
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized setRingResID(IIII)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 231
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sput p0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingtone:I

    .line 232
    sput p1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDRingback:I

    .line 233
    sput p2, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sResIDNewMessage:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    monitor-exit v0

    return-void

    .line 231
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized stop()V
    .locals 2

    .prologue
    .line 211
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 212
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->stop()V

    .line 213
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 214
    const/4 v1, 0x0

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sMediaPlayer:Landroid/media/MediaPlayer;

    .line 217
    :cond_0
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_1

    .line 218
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->cancel()V

    .line 219
    const/4 v1, 0x0

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sVibrator:Landroid/os/Vibrator;

    .line 221
    :cond_1
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    if-eqz v1, :cond_2

    .line 222
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    invoke-virtual {v1}, Landroid/media/AsyncPlayer;->stop()V

    .line 223
    const/4 v1, 0x0

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sAsyncPlayer:Landroid/media/AsyncPlayer;

    .line 226
    :cond_2
    sget-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;->NONE:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;

    sput-object v1, Lcom/sgiggle/pjmedia/SoundEffWrapper;->isplaying:Lcom/sgiggle/pjmedia/SoundEffWrapper$PLAYBACKTYPE;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    monitor-exit v0

    return-void

    .line 211
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static declared-synchronized updateContext(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 27
    const-class v0, Lcom/sgiggle/pjmedia/SoundEffWrapper;

    monitor-enter v0

    :try_start_0
    sput-object p0, Lcom/sgiggle/pjmedia/SoundEffWrapper;->sContext:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 28
    monitor-exit v0

    return-void

    .line 27
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method
