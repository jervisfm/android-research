.class public Lcom/sgiggle/production/AbsoluteLayout;
.super Landroid/view/ViewGroup;
.source "AbsoluteLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 27
    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 31
    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 43
    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .parameter

    .prologue
    .line 46
    instance-of v0, p1, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;

    return v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .parameter

    .prologue
    .line 50
    new-instance v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;-><init>(Lcom/sgiggle/production/AbsoluteLayout;Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/sgiggle/production/AbsoluteLayout;->getChildCount()I

    move-result v1

    .line 56
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 57
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/AbsoluteLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 58
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v4, 0x8

    if-eq v0, v4, :cond_0

    .line 59
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;

    .line 60
    iget v4, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->leftMargin:I

    .line 61
    iget v5, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->topMargin:I

    .line 62
    iget v6, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->width:I

    .line 63
    iget v0, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->height:I

    .line 64
    add-int/2addr v6, v4

    add-int/2addr v0, v5

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 56
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 67
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 71
    .line 73
    invoke-virtual {p0}, Lcom/sgiggle/production/AbsoluteLayout;->getChildCount()I

    move-result v1

    move v2, v0

    move v3, v0

    move v4, v0

    .line 74
    :goto_0
    if-ge v2, v1, :cond_1

    .line 75
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/AbsoluteLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_2

    .line 77
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;

    .line 78
    iget v5, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->leftMargin:I

    .line 79
    iget v6, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->topMargin:I

    .line 80
    iget v7, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->width:I

    .line 81
    iget v0, v0, Lcom/sgiggle/production/AbsoluteLayout$LayoutParams;->height:I

    .line 82
    add-int v8, v5, v7

    if-le v8, v4, :cond_0

    .line 83
    add-int v4, v5, v7

    .line 84
    :cond_0
    add-int v5, v6, v0

    if-le v5, v3, :cond_2

    .line 85
    add-int/2addr v0, v6

    move v3, v4

    .line 74
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move v4, v3

    move v3, v0

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0, v4, v3}, Lcom/sgiggle/production/AbsoluteLayout;->setMeasuredDimension(II)V

    .line 90
    return-void

    :cond_2
    move v0, v3

    move v3, v4

    goto :goto_1
.end method
