.class Lcom/sgiggle/production/AccountVerifier;
.super Ljava/lang/Object;
.source "AccountVerifier.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.AccountVerifier"

.field private static s_me:Lcom/sgiggle/production/AccountVerifier;


# instance fields
.field private m_application:Lcom/sgiggle/production/TangoApp;

.field private m_queryOtherAlert:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/TangoApp;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sgiggle/production/AccountVerifier;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/AccountVerifier;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AccountVerifier;->replyToQueryForOtherDevice(Z)V

    return-void
.end method

.method static synthetic access$102(Lcom/sgiggle/production/AccountVerifier;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19
    iput-object p1, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static getDefault()Lcom/sgiggle/production/AccountVerifier;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/sgiggle/production/AccountVerifier;->s_me:Lcom/sgiggle/production/AccountVerifier;

    return-object v0
.end method

.method static init(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 34
    new-instance v0, Lcom/sgiggle/production/AccountVerifier;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AccountVerifier;-><init>(Lcom/sgiggle/production/TangoApp;)V

    sput-object v0, Lcom/sgiggle/production/AccountVerifier;->s_me:Lcom/sgiggle/production/AccountVerifier;

    .line 35
    return-void
.end method

.method private replyToQueryForOtherDevice(Z)V
    .locals 3
    .parameter

    .prologue
    .line 113
    const-string v0, "Tango.AccountVerifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "replyToQueryForOtherDevice(registeredOther="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$OtherRegisteredDeviceMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OtherRegisteredDeviceMessage;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 116
    return-void
.end method


# virtual methods
.method public dismissQueryForOtherDeviceAlert()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "Tango.AccountVerifier"

    const-string v1, "Dismiss the existing Query-for-other-device alert..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    .line 96
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    .line 97
    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 99
    :cond_0
    return-void
.end method

.method public displayQueryOtherDeviceAlert(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/sgiggle/production/AccountVerifier;->dismissQueryForOtherDeviceAlert()V

    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_application:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    const v1, 0x7f09008d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 64
    const v2, 0x7f09008e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 69
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 70
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    .line 71
    const v0, 0x7f090021

    new-instance v1, Lcom/sgiggle/production/AccountVerifier$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AccountVerifier$1;-><init>(Lcom/sgiggle/production/AccountVerifier;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    const v0, 0x7f090022

    new-instance v1, Lcom/sgiggle/production/AccountVerifier$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AccountVerifier$2;-><init>(Lcom/sgiggle/production/AccountVerifier;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 87
    return-void
.end method

.method public displayVerificationOtherDeviceNotice(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 104
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09008f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 107
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 108
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissVerificationWithOtherDeviceMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissVerificationWithOtherDeviceMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 110
    return-void
.end method

.method public handleNewMessage(Lcom/sgiggle/messaging/Message;Landroid/app/Activity;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 42
    const-string v0, "Tango.AccountVerifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 57
    :goto_0
    return-void

    .line 46
    :pswitch_0
    const-string v0, "Tango.AccountVerifier"

    const-string v1, "Display query of other device notice"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/AccountVerifier;->displayQueryOtherDeviceAlert(Landroid/content/Context;)V

    goto :goto_0

    .line 52
    :pswitch_1
    const-string v0, "Tango.AccountVerifier"

    const-string v1, "Display veification other device notice"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/AccountVerifier;->displayVerificationOtherDeviceNotice(Landroid/content/Context;)V

    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x891c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .parameter

    .prologue
    .line 120
    const-string v0, "Tango.AccountVerifier"

    const-string v1, "onDismiss() [DialogInterface]"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/AccountVerifier;->m_queryOtherAlert:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_0

    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/AccountVerifier;->replyToQueryForOtherDevice(Z)V

    .line 124
    :cond_0
    return-void
.end method
