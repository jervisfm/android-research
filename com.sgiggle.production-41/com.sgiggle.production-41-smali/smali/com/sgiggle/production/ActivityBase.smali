.class public Lcom/sgiggle/production/ActivityBase;
.super Landroid/app/Activity;
.source "ActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "Tango.ActivityBase"

.field private static final VDBG:Z = true


# instance fields
.field protected m_firstMessage:Lcom/sgiggle/messaging/Message;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 27
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    :goto_0
    const-string v0, "Tango.ActivityBase"

    const-string v1, "ActivityBase()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    return-void

    .line 28
    :catch_0
    move-exception v0

    .line 29
    const-string v1, "Tango.ActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected getFirstMessage()Lcom/sgiggle/messaging/Message;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    return-object v0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 82
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 101
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Unsupported message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-void

    .line 87
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;

    .line 88
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 90
    invoke-virtual {v1, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->create(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 95
    :sswitch_1
    new-instance v0, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 96
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x88eb -> :sswitch_0
        0x892a -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 52
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/sgiggle/production/ActivityBase;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/ActivityBase;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 59
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(): First time created: message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    return-void

    .line 61
    :cond_0
    check-cast v0, Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;

    .line 62
    iget-object v0, v0, Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v0, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 63
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(): Restore activity: message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .parameter

    .prologue
    .line 69
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 70
    const-string v1, "Tango.ActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent(): Message = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 74
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 115
    const-string v0, "Tango.ActivityBase"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 117
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 118
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 40
    const-string v0, "Tango.ActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRetainNonConfigurationInstance(): message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    new-instance v0, Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;

    invoke-direct {v0}, Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;-><init>()V

    .line 42
    iget-object v1, p0, Lcom/sgiggle/production/ActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v1, v0, Lcom/sgiggle/production/ActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    .line 43
    return-object v0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 108
    const-string v0, "Tango.ActivityBase"

    const-string v1, "onUserLeaveHint()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-super {p0}, Landroid/app/Activity;->onUserLeaveHint()V

    .line 110
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 111
    return-void
.end method
