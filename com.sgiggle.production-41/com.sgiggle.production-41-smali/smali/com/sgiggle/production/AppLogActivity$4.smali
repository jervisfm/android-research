.class Lcom/sgiggle/production/AppLogActivity$4;
.super Ljava/lang/Object;
.source "AppLogActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/AppLogActivity;->share()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AppLogActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AppLogActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sgiggle/production/AppLogActivity$4;->this$0:Lcom/sgiggle/production/AppLogActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$4;->this$0:Lcom/sgiggle/production/AppLogActivity;

    const/4 v1, 0x0

    #calls: Lcom/sgiggle/production/AppLogActivity;->dump(Z)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sgiggle/production/AppLogActivity;->access$200(Lcom/sgiggle/production/AppLogActivity;Z)Ljava/lang/String;

    move-result-object v0

    .line 355
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 358
    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 360
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "MMM d, yyyy HH:mm:ss ZZZZ"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 363
    const-string v3, "android.intent.extra.SUBJECT"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Tango Log: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 367
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$4;->this$0:Lcom/sgiggle/production/AppLogActivity;

    const-string v2, "Share Tango Log ..."

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/AppLogActivity;->startActivity(Landroid/content/Intent;)V

    .line 369
    return-void
.end method
