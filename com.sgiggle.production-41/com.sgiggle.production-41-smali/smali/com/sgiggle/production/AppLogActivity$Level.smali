.class final enum Lcom/sgiggle/production/AppLogActivity$Level;
.super Ljava/lang/Enum;
.source "AppLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AppLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Level"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/AppLogActivity$Level;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum D:Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum E:Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum F:Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum I:Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum V:Lcom/sgiggle/production/AppLogActivity$Level;

.field public static final enum W:Lcom/sgiggle/production/AppLogActivity$Level;

.field private static byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;


# instance fields
.field private m_color:I

.field private m_hexColor:Ljava/lang/String;

.field private m_title:Ljava/lang/String;

.field private m_value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 186
    new-instance v0, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v1, "V"

    const-string v4, "#121212"

    const-string v5, "Verbose"

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 187
    new-instance v3, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v4, "D"

    const-string v7, "#00006C"

    const-string v8, "Debug"

    move v5, v9

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->D:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 188
    new-instance v3, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v4, "I"

    const-string v7, "#20831B"

    const-string v8, "Info"

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->I:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 189
    new-instance v3, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v4, "W"

    const-string v7, "#FD7916"

    const-string v8, "Warn"

    move v5, v11

    move v6, v11

    invoke-direct/range {v3 .. v8}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->W:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 190
    new-instance v3, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v4, "E"

    const-string v7, "#FD0010"

    const-string v8, "Error"

    move v5, v12

    move v6, v12

    invoke-direct/range {v3 .. v8}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->E:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 191
    new-instance v3, Lcom/sgiggle/production/AppLogActivity$Level;

    const-string v4, "F"

    const/4 v5, 0x5

    const/4 v6, 0x5

    const-string v7, "#ff0066"

    const-string v8, "Fatal"

    invoke-direct/range {v3 .. v8}, Lcom/sgiggle/production/AppLogActivity$Level;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    sput-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->F:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 185
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->D:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v9

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->I:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->W:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->E:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->F:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v3, v0, v1

    sput-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->$VALUES:[Lcom/sgiggle/production/AppLogActivity$Level;

    .line 193
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/production/AppLogActivity$Level;

    sput-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    .line 196
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v2

    .line 197
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->D:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v9

    .line 198
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->I:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v10

    .line 199
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->W:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v11

    .line 200
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    sget-object v1, Lcom/sgiggle/production/AppLogActivity$Level;->E:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v1, v0, v12

    .line 201
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/production/AppLogActivity$Level;->F:Lcom/sgiggle/production/AppLogActivity$Level;

    aput-object v2, v0, v1

    .line 202
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 209
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 210
    iput p3, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_value:I

    .line 211
    iput-object p4, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_hexColor:Ljava/lang/String;

    .line 212
    invoke-static {p4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_color:I

    .line 213
    iput-object p5, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_title:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public static getByOrder(I)Lcom/sgiggle/production/AppLogActivity$Level;
    .locals 1
    .parameter

    .prologue
    .line 229
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->byOrder:[Lcom/sgiggle/production/AppLogActivity$Level;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/AppLogActivity$Level;
    .locals 1
    .parameter

    .prologue
    .line 185
    const-class v0, Lcom/sgiggle/production/AppLogActivity$Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AppLogActivity$Level;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/AppLogActivity$Level;
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->$VALUES:[Lcom/sgiggle/production/AppLogActivity$Level;

    invoke-virtual {v0}, [Lcom/sgiggle/production/AppLogActivity$Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/AppLogActivity$Level;

    return-object v0
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_color:I

    return v0
.end method

.method public getHexColor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_hexColor:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_title:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/sgiggle/production/AppLogActivity$Level;->m_value:I

    return v0
.end method
