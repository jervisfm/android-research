.class Lcom/sgiggle/production/AppLogActivity$LogEntry;
.super Ljava/lang/Object;
.source "AppLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AppLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LogEntry"
.end annotation


# instance fields
.field private level:Lcom/sgiggle/production/AppLogActivity$Level;

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/production/AppLogActivity$Level;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    iput-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->level:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 239
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->text:Ljava/lang/String;

    .line 242
    iput-object p1, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->text:Ljava/lang/String;

    .line 243
    iput-object p2, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->level:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 244
    return-void
.end method


# virtual methods
.method public getLevel()Lcom/sgiggle/production/AppLogActivity$Level;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->level:Lcom/sgiggle/production/AppLogActivity$Level;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntry;->text:Ljava/lang/String;

    return-object v0
.end method
