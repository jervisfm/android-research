.class Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AppLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AppLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LogEntryAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/production/AppLogActivity$LogEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private m_activity:Landroid/app/Activity;

.field private m_entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/AppLogActivity$LogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private m_resourceId:I

.field final synthetic this$0:Lcom/sgiggle/production/AppLogActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/AppLogActivity;Landroid/app/Activity;ILjava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/AppLogActivity$LogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    iput-object p1, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->this$0:Lcom/sgiggle/production/AppLogActivity;

    .line 263
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 264
    iput-object p2, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_activity:Landroid/app/Activity;

    .line 265
    iput p3, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_resourceId:I

    .line 266
    iput-object p4, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_entries:Ljava/util/List;

    .line 267
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    return v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_entries:Ljava/util/List;

    .line 302
    return-void
.end method

.method public getLogEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/AppLogActivity$LogEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_entries:Ljava/util/List;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_entries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AppLogActivity$LogEntry;

    .line 273
    if-nez p2, :cond_0

    .line 274
    iget-object v1, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 275
    iget v2, p0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->m_resourceId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 280
    :goto_0
    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntry;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntry;->getLevel()Lcom/sgiggle/production/AppLogActivity$Level;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$Level;->getColor()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 283
    return-object v1

    .line 277
    :cond_0
    check-cast p2, Landroid/widget/TextView;

    move-object v1, p2

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 293
    const/4 v0, 0x0

    return v0
.end method
