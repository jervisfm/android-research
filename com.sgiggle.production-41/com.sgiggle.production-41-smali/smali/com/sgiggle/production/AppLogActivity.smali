.class public Lcom/sgiggle/production/AppLogActivity;
.super Landroid/app/ListActivity;
.source "AppLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;,
        Lcom/sgiggle/production/AppLogActivity$LogEntry;,
        Lcom/sgiggle/production/AppLogActivity$Level;
    }
.end annotation


# static fields
.field private static final MENU_JUMP_BOTTOM:I = 0x3

.field private static final MENU_JUMP_TOP:I = 0x2

.field private static final MENU_SHARE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.LogActivity"

.field private static m_logEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/AppLogActivity$LogEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_logEntryAdapter:Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

.field private m_logList:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 94
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 96
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    const-string v1, "Tango.LogActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sgiggle/production/AppLogActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logList:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/AppLogActivity;)Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logEntryAdapter:Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/AppLogActivity;Z)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AppLogActivity;->dump(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private dump(Z)Ljava/lang/String;
    .locals 6
    .parameter

    .prologue
    .line 324
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 325
    sget-object v0, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 327
    sget-object v2, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v3, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AppLogActivity$LogEntry;

    .line 328
    if-nez p1, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntry;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 330
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object v0, v3

    :goto_1
    move-object v3, v0

    .line 343
    goto :goto_0

    .line 332
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntry;->getLevel()Lcom/sgiggle/production/AppLogActivity$Level;

    move-result-object v4

    .line 333
    if-nez v4, :cond_1

    move-object v4, v3

    .line 338
    :goto_2
    const-string v5, "<font color=\""

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    invoke-virtual {v3}, Lcom/sgiggle/production/AppLogActivity$Level;->getHexColor()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    const-string v3, "\" face=\"sans-serif\"><b>"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 341
    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntry;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string v0, "</b></font><br/>\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v4

    goto :goto_1

    :cond_1
    move-object v3, v4

    .line 336
    goto :goto_2

    .line 346
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private jumpBottom()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logList:Landroid/widget/ListView;

    new-instance v1, Lcom/sgiggle/production/AppLogActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AppLogActivity$3;-><init>(Lcom/sgiggle/production/AppLogActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 321
    return-void
.end method

.method private jumpTop()V
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logList:Landroid/widget/ListView;

    new-instance v1, Lcom/sgiggle/production/AppLogActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AppLogActivity$2;-><init>(Lcom/sgiggle/production/AppLogActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 313
    return-void
.end method

.method private share()V
    .locals 2

    .prologue
    .line 350
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sgiggle/production/AppLogActivity$4;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AppLogActivity$4;-><init>(Lcom/sgiggle/production/AppLogActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 371
    return-void
.end method

.method static storeAppLogEntries(Lcom/sgiggle/messaging/Message;)V
    .locals 5
    .parameter

    .prologue
    .line 54
    .line 56
    invoke-virtual {p0}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    const-string v0, "Tango.LogActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "storeAppLogEntries(): Unsupported message = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_0
    return-void

    .line 58
    :pswitch_0
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppLogEvent;

    .line 59
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAppLogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getEntriesList()Ljava/util/List;

    move-result-object v0

    .line 67
    const-string v1, "Tango.LogActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "storeAppLogEntries(): # of entries = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 70
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    .line 76
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getSeverity()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 83
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->D:Lcom/sgiggle/production/AppLogActivity$Level;

    .line 86
    :goto_2
    new-instance v4, Lcom/sgiggle/production/AppLogActivity$LogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0, v3}, Lcom/sgiggle/production/AppLogActivity$LogEntry;-><init>(Ljava/lang/String;Lcom/sgiggle/production/AppLogActivity$Level;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    :sswitch_0
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->V:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 78
    :sswitch_1
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->D:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 79
    :sswitch_2
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->I:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 80
    :sswitch_3
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->W:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 81
    :sswitch_4
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->E:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 82
    :sswitch_5
    sget-object v3, Lcom/sgiggle/production/AppLogActivity$Level;->F:Lcom/sgiggle/production/AppLogActivity$Level;

    goto :goto_2

    .line 89
    :cond_0
    sput-object v1, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x892c
        :pswitch_0
    .end packed-switch

    .line 76
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 140
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 142
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 167
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 179
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 169
    :pswitch_0
    const-string v0, "Jumping to top of log ..."

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 170
    invoke-direct {p0}, Lcom/sgiggle/production/AppLogActivity;->jumpTop()V

    move v0, v2

    .line 171
    goto :goto_0

    .line 174
    :pswitch_1
    const-string v0, "Jumping to bottom of log ..."

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 175
    invoke-direct {p0}, Lcom/sgiggle/production/AppLogActivity;->jumpBottom()V

    move v0, v2

    .line 176
    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 104
    const-string v0, "Tango.LogActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 107
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLogActivity;->setContentView(I)V

    .line 109
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logList:Landroid/widget/ListView;

    .line 110
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logList:Landroid/widget/ListView;

    new-instance v1, Lcom/sgiggle/production/AppLogActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AppLogActivity$1;-><init>(Lcom/sgiggle/production/AppLogActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 121
    new-instance v0, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

    const v1, 0x7f030031

    sget-object v2, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;-><init>(Lcom/sgiggle/production/AppLogActivity;Landroid/app/Activity;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logEntryAdapter:Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

    .line 122
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logEntryAdapter:Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLogActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 124
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Tango Log ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " entries)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLogActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Loading "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " log entries..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 127
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 146
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 148
    const-string v0, "Share"

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x1080052

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 150
    return v2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 131
    const-string v0, "Tango.LogActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 134
    sget-object v0, Lcom/sgiggle/production/AppLogActivity;->m_logEntries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 135
    iget-object v0, p0, Lcom/sgiggle/production/AppLogActivity;->m_logEntryAdapter:Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/AppLogActivity$LogEntryAdapter;->clear()V

    .line 136
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 155
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 161
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 157
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/AppLogActivity;->share()V

    .line 158
    const/4 v0, 0x1

    goto :goto_0

    .line 155
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
