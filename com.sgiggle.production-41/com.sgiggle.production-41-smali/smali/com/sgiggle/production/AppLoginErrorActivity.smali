.class public Lcom/sgiggle/production/AppLoginErrorActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "AppLoginErrorActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.AppLoginErrorActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 15
    const-string v0, "Tango.AppLoginErrorActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 17
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLoginErrorActivity;->setContentView(I)V

    .line 18
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AppLoginErrorActivity;->setRequestedOrientation(I)V

    .line 19
    return-void
.end method
