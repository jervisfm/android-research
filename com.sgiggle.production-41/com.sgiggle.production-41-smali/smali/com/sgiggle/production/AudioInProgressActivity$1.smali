.class Lcom/sgiggle/production/AudioInProgressActivity$1;
.super Landroid/os/Handler;
.source "AudioInProgressActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AudioInProgressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AudioInProgressActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_isDestroyed:Z
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$000(Lcom/sgiggle/production/AudioInProgressActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler: ignoring message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; we\'re destroyed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :goto_0
    return-void

    .line 136
    :cond_0
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " call state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/AudioInProgressActivity;->access$100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 140
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControls:Landroid/view/View;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$200(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 144
    :pswitch_2
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->rejectIncomingCall()V

    goto :goto_0

    .line 148
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;
    invoke-static {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->access$100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/production/CallSession;

    move-result-object v1

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    #calls: Lcom/sgiggle/production/AudioInProgressActivity;->showIgnoredCallAlert(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/AudioInProgressActivity;->access$300(Lcom/sgiggle/production/AudioInProgressActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :pswitch_4
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$1;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
