.class Lcom/sgiggle/production/AudioInProgressActivity$11;
.super Ljava/lang/Object;
.source "AudioInProgressActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/AudioInProgressActivity;->createCallErrorDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AudioInProgressActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 983
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$11;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 985
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;

    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$11;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->_callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 987
    return-void
.end method
