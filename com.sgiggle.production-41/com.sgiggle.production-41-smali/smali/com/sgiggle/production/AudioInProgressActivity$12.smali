.class Lcom/sgiggle/production/AudioInProgressActivity$12;
.super Ljava/lang/Object;
.source "AudioInProgressActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/AudioInProgressActivity;->createIgnoredCallDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AudioInProgressActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$12;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .parameter

    .prologue
    .line 1025
    const-string v0, "Tango.AudioUI"

    const-string v1, "onCancel(DialogInterface) Cancel the Ignored-Call dialog..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$12;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #calls: Lcom/sgiggle/production/AudioInProgressActivity;->clearIgnoredCall()V
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1200(Lcom/sgiggle/production/AudioInProgressActivity;)V

    .line 1027
    return-void
.end method
