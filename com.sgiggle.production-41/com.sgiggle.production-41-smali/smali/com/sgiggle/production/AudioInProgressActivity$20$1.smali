.class Lcom/sgiggle/production/AudioInProgressActivity$20$1;
.super Ljava/lang/Object;
.source "AudioInProgressActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/AudioInProgressActivity$20;->onCompletion(Landroid/media/MediaPlayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sgiggle/production/AudioInProgressActivity$20;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity$20;)V
    .locals 0
    .parameter

    .prologue
    .line 1174
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$20$1;->this$1:Lcom/sgiggle/production/AudioInProgressActivity$20;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .parameter

    .prologue
    .line 1177
    const-string v0, "Tango.AudioUI"

    const-string v1, "video ringback finished playing"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1178
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$20$1;->this$1:Lcom/sgiggle/production/AudioInProgressActivity$20;

    iget-object v0, v0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    const/4 v1, 0x1

    #setter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_rbFinishedPlaying:Z
    invoke-static {v0, v1}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1402(Lcom/sgiggle/production/AudioInProgressActivity;Z)Z

    .line 1179
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$20$1;->this$1:Lcom/sgiggle/production/AudioInProgressActivity$20;

    iget-object v0, v0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_rbButtons:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1500(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/LinearLayout;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1180
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishVideoRingbackMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishVideoRingbackMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1182
    return-void
.end method
