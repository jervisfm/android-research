.class Lcom/sgiggle/production/AudioInProgressActivity$20;
.super Ljava/lang/Object;
.source "AudioInProgressActivity.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/AudioInProgressActivity;->playVideoRingback(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AudioInProgressActivity;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1170
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    iput-object p2, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->val$path:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3
    .parameter

    .prologue
    .line 1172
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "video ringback to play "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1173
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1600(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/VideoView;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 1174
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1600(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/VideoView;

    move-result-object v0

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$20$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$20$1;-><init>(Lcom/sgiggle/production/AudioInProgressActivity$20;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1185
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$20;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1600(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/VideoView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 1186
    return-void
.end method
