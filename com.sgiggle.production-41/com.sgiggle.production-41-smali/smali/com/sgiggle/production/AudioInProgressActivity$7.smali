.class Lcom/sgiggle/production/AudioInProgressActivity$7;
.super Ljava/lang/Object;
.source "AudioInProgressActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AudioInProgressActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AudioInProgressActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 698
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity$7;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 700
    .line 701
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$7;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iget-wide v0, v0, Lcom/sgiggle/production/CallSession;->m_callStartTime:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 702
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity$7;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/AudioInProgressActivity;->access$100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-wide v2, v2, Lcom/sgiggle/production/CallSession;->m_callStartTime:J

    sub-long/2addr v0, v2

    .line 703
    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 705
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity$7;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #calls: Lcom/sgiggle/production/AudioInProgressActivity;->updateElapsedTimeWidget(J)V
    invoke-static {v2, v0, v1}, Lcom/sgiggle/production/AudioInProgressActivity;->access$900(Lcom/sgiggle/production/AudioInProgressActivity;J)V

    .line 707
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity$7;->this$0:Lcom/sgiggle/production/AudioInProgressActivity;

    #getter for: Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->access$1000(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v1, 0xfa

    invoke-virtual {v0, p0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 708
    return-void

    :cond_0
    move-wide v0, v2

    goto :goto_0
.end method
