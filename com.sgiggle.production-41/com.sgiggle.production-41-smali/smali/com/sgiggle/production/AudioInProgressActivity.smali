.class public Lcom/sgiggle/production/AudioInProgressActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "AudioInProgressActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sgiggle/production/manager/ProximityManager$Listener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/AudioInProgressActivity$21;
    }
.end annotation


# static fields
.field private static final DIALOG_CALL_ERROR:I = 0x3

.field private static final DIALOG_CALL_ON_HOLD:I = 0x0

.field private static final DIALOG_IGNORED_CALL:I = 0x2

.field private static final DIALOG_LEAVE_MESSAGE:I = 0x1

.field private static final FINISH_ACTIVITY:I = 0x6

.field private static final FINISH_ACTIVITY_DELAY:I = 0x3e8

.field private static final REJECT_CALL_DELAY:I = 0x2bc

.field private static final SEND_REJECT_CALL:I = 0x2

.field private static final SHOW_IGNORED_CALL:I = 0x3

.field private static final SHOW_IGNORED_CALL_DELAY:I = 0x1f4

.field private static final SHOW_INCALL_CONTROLS:I = 0x1

.field private static final SHOW_INCALL_CONTROLS_DELAY:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "Tango.AudioUI"

.field private static final TIME_UPDATE_INTERVAL:I = 0xfa


# instance fields
.field private _callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

.field private answerCallControls:Landroid/view/View;

.field private m_answerCallBtn:Landroid/widget/Button;

.field private m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

.field private m_callErrorDialogShowing:Z

.field private m_callOnHoldDialogShowing:Z

.field private m_callStatusTextView:Landroid/widget/TextView;

.field private m_declineCallBtn:Landroid/widget/Button;

.field private m_elapsedTimeTextView:Landroid/widget/TextView;

.field private m_endButton:Landroid/widget/RelativeLayout;

.field private m_footerImage:Landroid/widget/ImageView;

.field private m_fullWakeLock:Landroid/os/PowerManager$WakeLock;

.field private m_handler:Landroid/os/Handler;

.field private m_ignoredCallDialogShowing:Z

.field private m_inCallControls:Landroid/view/View;

.field private m_inCallControlsRow2:Landroid/widget/LinearLayout;

.field private m_isDestroyed:Z

.field private m_leaveMessageDialogShowing:Z

.field private m_mainFrame:Landroid/view/ViewGroup;

.field private m_muteButton:Landroid/widget/ToggleButton;

.field private m_nameTextView:Landroid/widget/TextView;

.field private m_peerDisplayname:Ljava/lang/String;

.field private m_photoImageView:Landroid/widget/ImageView;

.field private m_proximityFactory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

.field private m_proximityManager:Lcom/sgiggle/production/manager/ProximityManager;

.field private m_rbButtons:Landroid/widget/LinearLayout;

.field private m_rbFinishedPlaying:Z

.field private m_rbLayout:Landroid/widget/RelativeLayout;

.field private m_rbPlayerView:Landroid/widget/VideoView;

.field private m_session:Lcom/sgiggle/production/CallSession;

.field private m_speakerButton:Landroid/widget/ToggleButton;

.field private m_textColorConnected:I

.field private m_textColorEnded:I

.field private m_timeHandler:Landroid/os/Handler;

.field private m_topUserInfo:Landroid/widget/LinearLayout;

.field private m_updateTimeTask:Ljava/lang/Runnable;

.field private m_videoButton:Landroid/widget/ToggleButton;

.field private m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 98
    new-instance v0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-direct {v0}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    .line 112
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_isDestroyed:Z

    .line 113
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_leaveMessageDialogShowing:Z

    .line 114
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_ignoredCallDialogShowing:Z

    .line 115
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callErrorDialogShowing:Z

    .line 116
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callOnHoldDialogShowing:Z

    .line 128
    new-instance v0, Lcom/sgiggle/production/AudioInProgressActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AudioInProgressActivity$1;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    .line 684
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;

    .line 698
    new-instance v0, Lcom/sgiggle/production/AudioInProgressActivity$7;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AudioInProgressActivity$7;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_updateTimeTask:Ljava/lang/Runnable;

    return-void
.end method

.method private acceptIncomingCall()V
    .locals 4

    .prologue
    .line 572
    const-string v0, "Tango.AudioUI"

    const-string v1, "LEFT_HANDLE: answer!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->hideIncomingCallWidget()V

    .line 574
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->answerIncomingCall()V

    .line 576
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 577
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/AudioInProgressActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_isDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/production/CallSession;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sgiggle/production/AudioInProgressActivity;)Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->_callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->clearIgnoredCall()V

    return-void
.end method

.method static synthetic access$1302(Lcom/sgiggle/production/AudioInProgressActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_leaveMessageDialogShowing:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/sgiggle/production/AudioInProgressActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbFinishedPlaying:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbButtons:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/widget/VideoView;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/AudioInProgressActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControls:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/AudioInProgressActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->showIgnoredCallAlert(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->hangUpCall()V

    return-void
.end method

.method static synthetic access$500(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->acceptIncomingCall()V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->rejectCall()V

    return-void
.end method

.method static synthetic access$700(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->skipVideoRingBack()V

    return-void
.end method

.method static synthetic access$800(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->likeVideoRingBack()V

    return-void
.end method

.method static synthetic access$900(Lcom/sgiggle/production/AudioInProgressActivity;J)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/AudioInProgressActivity;->updateElapsedTimeWidget(J)V

    return-void
.end method

.method private clearIgnoredCall()V
    .locals 4

    .prologue
    .line 562
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 564
    return-void
.end method

.method private createCallErrorDialog()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 975
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->_callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    if-nez v0, :cond_0

    .line 976
    const/4 v0, 0x0

    .line 998
    :goto_0
    return-object v0

    .line 979
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 980
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->_callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sgiggle/production/Utils;->getStringFromResource(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f090027

    new-instance v3, Lcom/sgiggle/production/AudioInProgressActivity$11;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/AudioInProgressActivity$11;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v2, Lcom/sgiggle/production/AudioInProgressActivity$10;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/AudioInProgressActivity$10;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 998
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0
.end method

.method private createCallOnHoldDialog()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 1039
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1040
    const v1, 0x7f090098

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090099

    new-instance v3, Lcom/sgiggle/production/AudioInProgressActivity$15;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/AudioInProgressActivity$15;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09009a

    new-instance v3, Lcom/sgiggle/production/AudioInProgressActivity$14;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/AudioInProgressActivity$14;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1064
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private createIgnoredCallDialog()Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1007
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1008
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_peerDisplayname:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1010
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1011
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09001f

    new-instance v3, Lcom/sgiggle/production/AudioInProgressActivity$13;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/AudioInProgressActivity$13;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$12;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$12;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 1030
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private createLeaveMessageDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 1072
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_leaveMessageDialogShowing:Z

    .line 1074
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->create(Landroid/app/Activity;Ljava/lang/String;Z)Lcom/sgiggle/production/dialog/TangoAlertDialog;

    move-result-object v0

    .line 1078
    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$16;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$16;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setExtraOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1085
    return-object v0
.end method

.method private dismissKeyguard(Z)V
    .locals 4
    .parameter

    .prologue
    const/high16 v3, 0x40

    const/high16 v2, 0x20

    const/high16 v1, 0x8

    .line 764
    if-eqz p1, :cond_0

    .line 765
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->addFlags(I)V

    .line 766
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 767
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 773
    :goto_0
    return-void

    .line 769
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Window;->clearFlags(I)V

    .line 770
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 771
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private handleAudioInProgressEvent()V
    .locals 3

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->onAudioModeUpdated()V

    .line 482
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090051

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 484
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->getPeerPhoto()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 485
    if-eqz v0, :cond_0

    .line 486
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_photoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 488
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->setVolumeControlStream(I)V

    .line 489
    return-void
.end method

.method private handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 498
    const-string v0, "Tango.AudioUI"

    const-string v1, "Handle Call Error Event: displaying call error dialog"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->_callErrorEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    .line 500
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->showDialog(I)V

    .line 501
    return-void
.end method

.method private handleIncomingCallEvent()V
    .locals 3

    .prologue
    .line 461
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingIgnoredCallAlert()V

    .line 462
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingRecordVideoMessageAlert()V

    .line 463
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingCallErrorAlert()V

    .line 464
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingCallOnHoldAlert()V

    .line 465
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->showIncomingCallWidget()V

    .line 466
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControls:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 468
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_nameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 471
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->getPeerPhoto()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 472
    if-eqz v0, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_photoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 477
    :cond_0
    return-void
.end method

.method private handleSendCallInvitationEvent()V
    .locals 3

    .prologue
    .line 443
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->hideIncomingCallWidget()V

    .line 444
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControls:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 446
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_nameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 447
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 449
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->getPeerPhoto()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 450
    if-eqz v0, :cond_0

    .line 451
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_photoImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 454
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallBackground()V

    .line 455
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallControls()V

    .line 458
    return-void
.end method

.method private hangUpCall()V
    .locals 4

    .prologue
    .line 749
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 750
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallBackground()V

    .line 751
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallControls()V

    .line 752
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 754
    return-void
.end method

.method private hideIncomingCallWidget()V
    .locals 2

    .prologue
    .line 599
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->answerCallControls:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 600
    return-void
.end method

.method private likeVideoRingBack()V
    .locals 3

    .prologue
    .line 1200
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$LikeVideoRingbackMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$LikeVideoRingbackMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1202
    return-void
.end method

.method private moveToBackgroundIfNeeded()V
    .locals 4

    .prologue
    .line 932
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.sgiggle.production.tangoapp.EXTRA_APP_STATE"

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp$AppState;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 934
    const-string v1, "Tango.AudioUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AppState (Before-call): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 935
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp$AppState;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp$AppState;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 937
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->moveTaskToBack(Z)Z

    .line 939
    :cond_1
    return-void
.end method

.method private rejectCall()V
    .locals 4

    .prologue
    .line 580
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 581
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 582
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallBackground()V

    .line 583
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x2bc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 584
    return-void
.end method

.method private showIgnoredCallAlert(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 509
    iput-object p1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_peerDisplayname:Ljava/lang/String;

    .line 510
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 511
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->showDialog(I)V

    .line 512
    return-void
.end method

.method private showIncomingCallWidget()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 606
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->answerCallControls:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_footerImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 608
    return-void
.end method

.method private skipVideoRingBack()V
    .locals 3

    .prologue
    .line 1205
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SkipVideoRingbackMessage;

    iget-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbFinishedPlaying:Z

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SkipVideoRingbackMessage;-><init>(Z)V

    .line 1207
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1208
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 1210
    return-void
.end method

.method private updateElapsedTimeWidget(J)V
    .locals 2
    .parameter

    .prologue
    .line 691
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_elapsedTimeTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 696
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_elapsedTimeTextView:Landroid/widget/TextView;

    invoke-static {p1, p2}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateInCallBackground()V
    .locals 3

    .prologue
    .line 621
    const v0, 0x7f02000e

    .line 623
    sget-object v1, Lcom/sgiggle/production/AudioInProgressActivity$21;->$SwitchMap$com$sgiggle$production$CallSession$CallState:[I

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v2}, Lcom/sgiggle/production/CallSession$CallState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 640
    :pswitch_0
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_mainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    .line 641
    return-void

    .line 623
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateInCallControls()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 648
    sget-object v0, Lcom/sgiggle/production/AudioInProgressActivity$21;->$SwitchMap$com$sgiggle$production$CallSession$CallState:[I

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession$CallState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 676
    :goto_0
    :pswitch_0
    return-void

    .line 655
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_endButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 656
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 657
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 658
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    goto :goto_0

    .line 662
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_endButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 663
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 664
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 665
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v2}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    goto :goto_0

    .line 670
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_endButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 671
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 672
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    .line 673
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setEnabled(Z)V

    goto :goto_0

    .line 648
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public dismissPendingCallErrorAlert()V
    .locals 1

    .prologue
    .line 551
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callErrorDialogShowing:Z

    if-eqz v0, :cond_0

    .line 553
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 557
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callErrorDialogShowing:Z

    .line 559
    :cond_0
    return-void

    .line 554
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public dismissPendingCallOnHoldAlert()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 515
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callOnHoldDialogShowing:Z

    if-eqz v0, :cond_0

    .line 517
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 521
    :goto_0
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callOnHoldDialogShowing:Z

    .line 523
    :cond_0
    return-void

    .line 518
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public dismissPendingIgnoredCallAlert()V
    .locals 1

    .prologue
    .line 529
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_ignoredCallDialogShowing:Z

    if-eqz v0, :cond_0

    .line 531
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->clearIgnoredCall()V

    .line 532
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 536
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_ignoredCallDialogShowing:Z

    .line 538
    :cond_0
    return-void

    .line 533
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public dismissPendingRecordVideoMessageAlert()V
    .locals 1

    .prologue
    .line 541
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_leaveMessageDialogShowing:Z

    if-eqz v0, :cond_0

    .line 543
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->removeDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 544
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 9
    .parameter

    .prologue
    const-wide/16 v7, 0x1f4

    const v6, 0x7f090052

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 303
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-nez p1, :cond_0

    .line 306
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 440
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 312
    sparse-switch v0, :sswitch_data_0

    .line 434
    const-string v1, "Tango.AudioUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNewMessage(): Unsupported message-type = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallBackground()V

    .line 439
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->updateInCallControls()V

    goto :goto_0

    .line 315
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;

    .line 316
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->handleSendCallInvitationEvent()V

    .line 317
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoRingback()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingback()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 318
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingback()Ljava/lang/String;

    move-result-object v1

    .line 319
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoRingbackPrologue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoRingbackPrologue()Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_2
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    if-lt v2, v5, :cond_3

    move v2, v4

    .line 323
    :goto_3
    if-eqz v2, :cond_4

    .line 324
    const-string v0, "Tango.AudioUI"

    const-string v1, "do not show the video ringback since we do not have a layout for tablet"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishVideoRingbackMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishVideoRingbackMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_1

    .line 319
    :cond_2
    const-string v0, ""

    goto :goto_2

    :cond_3
    move v2, v3

    .line 320
    goto :goto_3

    .line 328
    :cond_4
    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/AudioInProgressActivity;->playVideoRingback(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 339
    :sswitch_1
    invoke-direct {p0, v4}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 342
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->isLaunchFromLockScreen()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 345
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/TangoApp;->setLaunchFromLockScreen(Z)V

    .line 349
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->hideIncomingCallWidget()V

    .line 350
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v4, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 353
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 354
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v5}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v5}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v5}, Landroid/widget/ToggleButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 361
    :sswitch_2
    const-string v0, "Tango.AudioUI"

    const-string v1, "handleNewMessage(): Audio-In-Initialization: Reset call-timer."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    goto/16 :goto_1

    .line 368
    :sswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_6

    .line 369
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 371
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7, v8}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 378
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->clearIgnoredCall()V

    .line 379
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    goto/16 :goto_1

    .line 386
    :sswitch_4
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->isLaunchFromLockScreen()Z

    move-result v0

    if-nez v0, :cond_7

    .line 387
    invoke-direct {p0, v4}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 389
    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->handleIncomingCallEvent()V

    goto/16 :goto_1

    .line 394
    :sswitch_5
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 395
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;

    .line 396
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodsPurchased()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->initVGoodStatus(Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;Z)V

    .line 397
    const-string v0, "VGOOD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "-- X -- support:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", purchased:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodPurchased:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->handleAudioInProgressEvent()V

    .line 399
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v3}, Landroid/widget/ToggleButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 406
    :sswitch_6
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 407
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 409
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_elapsedTimeTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_textColorEnded:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 410
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x6

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 415
    :sswitch_7
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 416
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    .line 417
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    goto/16 :goto_1

    .line 422
    :sswitch_8
    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_isDestroyed:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 423
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 424
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/AudioInProgressActivity;->showDialog(I)V

    .line 429
    invoke-direct {p0, v3}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    goto/16 :goto_1

    .line 312
    nop

    :sswitch_data_0
    .sparse-switch
        0x88c7 -> :sswitch_0
        0x88c9 -> :sswitch_4
        0x88cb -> :sswitch_7
        0x88cd -> :sswitch_1
        0x88cf -> :sswitch_5
        0x88ff -> :sswitch_2
        0x8901 -> :sswitch_3
        0x890d -> :sswitch_6
        0x89cf -> :sswitch_8
    .end sparse-switch
.end method

.method onAudioModeUpdated()V
    .locals 3

    .prologue
    .line 492
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Updating audio button states | Mute Button on: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " | Mute on: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->getMicMute()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v1, v1, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 494
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v1, v1, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 495
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1125
    const-string v0, "Tango.AudioUI"

    const-string v1, "onBackPressed() Ignore the Back key."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1126
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .parameter

    .prologue
    .line 569
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 715
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 717
    packed-switch v0, :pswitch_data_0

    .line 743
    const-string v1, "Tango.AudioUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick: unexpected click: View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    :goto_0
    return-void

    .line 719
    :pswitch_0
    const-string v0, "Tango.AudioUI"

    const-string v1, "onClick(hangup_button)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->hangUpCall()V

    goto :goto_0

    .line 724
    :pswitch_1
    const-string v0, "Tango.AudioUI"

    const-string v1, "onClick(send_video_button)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;

    iget-object v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 730
    :pswitch_2
    const-string v0, "Tango.AudioUI"

    const-string v1, "onClick(mute_button)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v3, v3, Lcom/sgiggle/production/CallSession;->m_muted:Z

    if-nez v3, :cond_0

    move v3, v4

    :goto_1
    invoke-static {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    :cond_0
    move v3, v5

    goto :goto_1

    .line 736
    :pswitch_3
    const-string v0, "Tango.AudioUI"

    const-string v1, "onClick(speaker_button)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    if-nez v2, :cond_1

    move v2, v4

    :goto_2
    invoke-static {v4, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    :cond_1
    move v2, v5

    goto :goto_2

    .line 717
    :pswitch_data_0
    .packed-switch 0x7f0a001a
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 160
    const-string v0, "Tango.AudioUI"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->isLaunchFromLockScreen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 170
    :cond_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 172
    const v0, 0x7f030004

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->setContentView(I)V

    .line 178
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$2;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->setButtonHandler(Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;)V

    .line 191
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    .line 194
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 195
    const v1, 0x1000001a

    const-string v2, "Tango.AudioUI"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_fullWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 197
    new-instance v0, Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/receiver/WifiLockReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    .line 201
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    .line 203
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_1

    .line 204
    const-string v0, "Tango.AudioUI"

    const-string v1, "onCreate(): Call session is null. End this screen."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 206
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    .line 209
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->moveToBackgroundIfNeeded()V

    .line 294
    :goto_0
    return-void

    .line 215
    :cond_1
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setAudioActivityInstance(Lcom/sgiggle/production/AudioInProgressActivity;)V

    .line 219
    const v0, 0x7f0a000b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_topUserInfo:Landroid/widget/LinearLayout;

    .line 220
    const v0, 0x7f0a0015

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->answerCallControls:Landroid/view/View;

    .line 221
    const v0, 0x7f0a0016

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_answerCallBtn:Landroid/widget/Button;

    .line 222
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_answerCallBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$3;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    const v0, 0x7f0a0017

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_declineCallBtn:Landroid/widget/Button;

    .line 229
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_declineCallBtn:Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$4;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$4;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    const v0, 0x7f0a0018

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControls:Landroid/view/View;

    .line 240
    const v0, 0x7f0a000a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_footerImage:Landroid/widget/ImageView;

    .line 243
    const v0, 0x7f0a0008

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_mainFrame:Landroid/view/ViewGroup;

    .line 244
    const v0, 0x7f0a000f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callStatusTextView:Landroid/widget/TextView;

    .line 245
    const v0, 0x7f0a000d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_elapsedTimeTextView:Landroid/widget/TextView;

    .line 246
    const v0, 0x7f0a000e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_nameTextView:Landroid/widget/TextView;

    .line 247
    const v0, 0x7f0a000c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_photoImageView:Landroid/widget/ImageView;

    .line 248
    const v0, 0x7f0a0011

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/VideoView;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    .line 249
    const v0, 0x7f0a0010

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbLayout:Landroid/widget/RelativeLayout;

    .line 250
    const v0, 0x7f0a0012

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbButtons:Landroid/widget/LinearLayout;

    .line 251
    const v0, 0x7f0a0013

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 252
    const v1, 0x7f0a0014

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 253
    new-instance v2, Lcom/sgiggle/production/AudioInProgressActivity$5;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/AudioInProgressActivity$5;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 259
    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$6;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$6;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 267
    const v0, 0x7f0a0019

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControlsRow2:Landroid/widget/LinearLayout;

    .line 268
    const v0, 0x7f0a001c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    .line 269
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 270
    const v0, 0x7f0a001a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    .line 271
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_muteButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    const v0, 0x7f0a001b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    .line 273
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_speakerButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    const v0, 0x7f0a001d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_endButton:Landroid/widget/RelativeLayout;

    .line 275
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_endButton:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    .line 279
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_textColorConnected:I

    .line 280
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_textColorEnded:I

    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_elapsedTimeTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_textColorConnected:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 284
    new-instance v0, Lcom/sgiggle/production/manager/ProximityManager;

    invoke-direct {v0, p0, p0}, Lcom/sgiggle/production/manager/ProximityManager;-><init>(Landroid/content/Context;Lcom/sgiggle/production/manager/ProximityManager$Listener;)V

    iput-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityManager:Lcom/sgiggle/production/manager/ProximityManager;

    .line 287
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_2

    .line 288
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->handleSendCallInvitationEvent()V

    .line 291
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 293
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->enableKeyguard()V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 943
    .line 945
    packed-switch p1, :pswitch_data_0

    .line 962
    const/4 v0, 0x0

    .line 965
    :goto_0
    return-object v0

    .line 947
    :pswitch_0
    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callOnHoldDialogShowing:Z

    .line 948
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->createCallOnHoldDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 951
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->createLeaveMessageDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 954
    :pswitch_2
    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_ignoredCallDialogShowing:Z

    .line 955
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->createIgnoredCallDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 958
    :pswitch_3
    iput-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_callErrorDialogShowing:Z

    .line 959
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->createCallErrorDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 945
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 890
    const-string v0, "Tango.AudioUI"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 894
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 895
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 896
    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$9;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$9;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    .line 901
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 902
    const-string v0, "Tango.AudioUI"

    const-string v1, "onStop: could not abandon audiofocus"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 905
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingIgnoredCallAlert()V

    .line 906
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAudioActivityInstance(Lcom/sgiggle/production/AudioInProgressActivity;)V

    .line 911
    iput-boolean v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_isDestroyed:Z

    .line 919
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    const/4 v2, 0x0

    invoke-static {v3, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 928
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->moveToBackgroundIfNeeded()V

    .line 929
    return-void
.end method

.method public onGrabbedStateChange(Landroid/view/View;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 591
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onGrabbedStateChange: grabbedState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_INCOMING:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_0

    .line 1112
    packed-switch p1, :pswitch_data_0

    .line 1120
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 1115
    :pswitch_0
    const-string v0, "Tango.AudioUI"

    const-string v1, "On volume key pressed, silence the ringtone."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    invoke-static {}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->stop()V

    goto :goto_0

    .line 1112
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 846
    const-string v0, "Tango.AudioUI"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 847
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 850
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityManager:Lcom/sgiggle/production/manager/ProximityManager;

    invoke-virtual {v0}, Lcom/sgiggle/production/manager/ProximityManager;->disable()V

    .line 851
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_updateTimeTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 852
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 854
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 855
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 862
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->moveToBackgroundIfNeeded()V

    .line 863
    return-void
.end method

.method public onProximityChanged(Z)V
    .locals 3
    .parameter

    .prologue
    .line 1093
    const-string v0, "Tango.AudioUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "proximityChanged(isTooClose:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    if-eqz p1, :cond_2

    .line 1095
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_CONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    if-nez v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    invoke-virtual {v0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->handleProximityNear(Landroid/app/Activity;)V

    .line 1106
    :cond_1
    :goto_0
    return-void

    .line 1104
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    invoke-virtual {v0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->handleProximityFar(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 4

    .prologue
    .line 788
    const-string v0, "Tango.AudioUI"

    const-string v1, "onRestart()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onRestart()V

    .line 794
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 795
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v3, v3, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    invoke-static {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 800
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 804
    const-string v0, "Tango.AudioUI"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 808
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.VoIP_BLUETOOTH"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 809
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 811
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->isLaunchFromLockScreen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_leaveMessageDialogShowing:Z

    if-nez v0, :cond_0

    .line 816
    invoke-direct {p0, v5}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissKeyguard(Z)V

    .line 820
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityManager:Lcom/sgiggle/production/manager/ProximityManager;

    invoke-virtual {v0}, Lcom/sgiggle/production/manager/ProximityManager;->enable()V

    .line 821
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_videoButton:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v4}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 822
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_updateTimeTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 823
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_timeHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_updateTimeTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 827
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    if-eqz v0, :cond_1

    .line 828
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/AudioInProgressActivity;->showDialog(I)V

    .line 832
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2

    .line 833
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 834
    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$8;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$8;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    .line 839
    invoke-virtual {v0, v1, v6, v6}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    if-eq v0, v5, :cond_2

    .line 840
    const-string v0, "Tango.AudioUI"

    const-string v1, "onResume: could not request audiofocus"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    :cond_2
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 777
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 779
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 780
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 781
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 783
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 784
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 867
    const-string v0, "Tango.AudioUI"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStop()V

    .line 871
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    invoke-virtual {v0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->handleProximityFar(Landroid/app/Activity;)V

    .line 874
    invoke-virtual {p0}, Lcom/sgiggle/production/AudioInProgressActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x20

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 875
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_fullWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v1, 0x7530

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 878
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/receiver/WifiLockReceiver;->releaseWifiLock(Landroid/content/Context;)V

    .line 880
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AudioInProgressActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 886
    :goto_0
    return-void

    .line 881
    :catch_0
    move-exception v0

    .line 882
    const-string v1, "Tango.AudioUI"

    const-string v2, "got exception for unregisterReceiver()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 883
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public playVideoRingback(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1130
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1131
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbButtons:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1132
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_topUserInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1133
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControlsRow2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1135
    iput-boolean v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbFinishedPlaying:Z

    .line 1136
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 1137
    if-eqz v0, :cond_1

    move-object v1, p2

    .line 1140
    :goto_0
    const-string v2, "Tango.AudioUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "video ringback to play "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1141
    iget-object v2, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v2, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 1142
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    new-instance v2, Lcom/sgiggle/production/AudioInProgressActivity$17;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/AudioInProgressActivity$17;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1146
    iget-object v1, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    new-instance v2, Lcom/sgiggle/production/AudioInProgressActivity$18;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/AudioInProgressActivity$18;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1157
    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$19;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AudioInProgressActivity$19;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 1189
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 1190
    return-void

    .line 1170
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    new-instance v1, Lcom/sgiggle/production/AudioInProgressActivity$20;

    invoke-direct {v1, p0, p2}, Lcom/sgiggle/production/AudioInProgressActivity$20;-><init>(Lcom/sgiggle/production/AudioInProgressActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    goto :goto_1

    :cond_1
    move-object v1, p1

    goto :goto_0
.end method

.method public stopVideoRingback()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1193
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbPlayerView:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 1194
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_rbLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1195
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_topUserInfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1196
    iget-object v0, p0, Lcom/sgiggle/production/AudioInProgressActivity;->m_inCallControlsRow2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1197
    return-void
.end method
