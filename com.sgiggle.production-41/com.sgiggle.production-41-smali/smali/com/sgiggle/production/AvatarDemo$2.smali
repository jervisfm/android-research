.class Lcom/sgiggle/production/AvatarDemo$2;
.super Landroid/telephony/PhoneStateListener;
.source "AvatarDemo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/AvatarDemo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/AvatarDemo;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/AvatarDemo;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sgiggle/production/AvatarDemo$2;->this$0:Lcom/sgiggle/production/AvatarDemo;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 104
    if-ne p1, v2, :cond_1

    .line 105
    const-string v0, "AvatarDemo"

    const-string v1, "onCallStateChanged(int state, String incomingNumber) CALL_STATE_RINGING"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo$2;->this$0:Lcom/sgiggle/production/AvatarDemo;

    #setter for: Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z
    invoke-static {v0, v2}, Lcom/sgiggle/production/AvatarDemo;->access$102(Lcom/sgiggle/production/AvatarDemo;Z)Z

    .line 111
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/telephony/PhoneStateListener;->onCallStateChanged(ILjava/lang/String;)V

    .line 112
    return-void

    .line 107
    :cond_1
    if-eqz p1, :cond_2

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 108
    :cond_2
    const-string v0, "AvatarDemo"

    const-string v1, "onCallStateChanged(int state, String incomingNumber) CALL_STATE_IDLE || CALL_STATE_OFFHOOK"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo$2;->this$0:Lcom/sgiggle/production/AvatarDemo;

    const/4 v1, 0x0

    #setter for: Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z
    invoke-static {v0, v1}, Lcom/sgiggle/production/AvatarDemo;->access$102(Lcom/sgiggle/production/AvatarDemo;Z)Z

    goto :goto_0
.end method
