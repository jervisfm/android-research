.class public Lcom/sgiggle/production/AvatarDemo;
.super Lcom/sgiggle/production/BillingSupportBaseActivity;
.source "AvatarDemo.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final HIDE_TIP_BUBBLE:I = 0x3e7

.field private static final TAG:Ljava/lang/String; = "AvatarDemo"

.field private static final TIP_WAIT_TIME:I = 0xfa0

.field private static s_instance:Lcom/sgiggle/production/AvatarDemo;


# instance fields
.field private bottomBubble:Landroid/view/View;

.field private handlerEvent:Landroid/os/Handler;

.field private handlerTimer:Landroid/os/Handler;

.field private m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

.field private m_avatarPaused:Z

.field private m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

.field private m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

.field private m_avatarUpdated:Z

.field private m_btnClicked:Z

.field private m_buy:Landroid/widget/Button;

.field private m_is_incoming_call:Z

.field private m_phoneStateListener:Landroid/telephony/PhoneStateListener;

.field private m_pm:Landroid/os/PowerManager;

.field m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

.field private m_productMarketId:Ljava/lang/String;

.field m_progressDialog:Landroid/app/ProgressDialog;

.field private m_purchased:Z

.field private m_receiver:Landroid/content/BroadcastReceiver;

.field private m_selector:Landroid/view/View;

.field private m_selectorIn:Landroid/view/animation/Animation;

.field private m_wand:Landroid/widget/ImageView;

.field private taskHideTipBubble:Ljava/lang/Runnable;

.field private wandBubble:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->handlerTimer:Landroid/os/Handler;

    .line 72
    iput-boolean v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_purchased:Z

    .line 73
    iput-boolean v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_btnClicked:Z

    .line 75
    iput-boolean v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    .line 76
    iput-boolean v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarPaused:Z

    .line 83
    new-instance v0, Lcom/sgiggle/production/AvatarDemo$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AvatarDemo$1;-><init>(Lcom/sgiggle/production/AvatarDemo;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->handlerEvent:Landroid/os/Handler;

    .line 102
    new-instance v0, Lcom/sgiggle/production/AvatarDemo$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AvatarDemo$2;-><init>(Lcom/sgiggle/production/AvatarDemo;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 118
    new-instance v0, Lcom/sgiggle/production/AvatarDemo$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/AvatarDemo$3;-><init>(Lcom/sgiggle/production/AvatarDemo;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->taskHideTipBubble:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/AvatarDemo;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sgiggle/production/AvatarDemo;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/AvatarDemo;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->handlerEvent:Landroid/os/Handler;

    return-object v0
.end method

.method private static clearActiveInstance(Lcom/sgiggle/production/AvatarDemo;)V
    .locals 1
    .parameter

    .prologue
    .line 135
    sget-object v0, Lcom/sgiggle/production/AvatarDemo;->s_instance:Lcom/sgiggle/production/AvatarDemo;

    if-ne v0, p0, :cond_0

    .line 136
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/AvatarDemo;->s_instance:Lcom/sgiggle/production/AvatarDemo;

    .line 137
    :cond_0
    return-void
.end method

.method public static getActiveInstance()Lcom/sgiggle/production/AvatarDemo;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/sgiggle/production/AvatarDemo;->s_instance:Lcom/sgiggle/production/AvatarDemo;

    return-object v0
.end method

.method private hideWand()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 319
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09011c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 321
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    const v1, 0x7f0200df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 322
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 323
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_selector:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 324
    return-void
.end method

.method private markProductPurchased()V
    .locals 2

    .prologue
    .line 540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_purchased:Z

    .line 541
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 542
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    const v1, 0x7f0900fb

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 543
    return-void
.end method

.method private onBuyClick()V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 432
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->showProgressDialog()V

    .line 434
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 435
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->purchaseFree(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0
.end method

.method private onWandClick()V
    .locals 0

    .prologue
    .line 474
    return-void
.end method

.method private purchaseFree(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 10
    .parameter

    .prologue
    .line 442
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    .line 443
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v2

    .line 444
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 445
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 446
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 450
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;

    const-string v3, "Free avatar"

    const/4 v4, 0x1

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    const/4 v9, 0x0

    move-object v8, v5

    invoke-direct/range {v0 .. v9}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Z)V

    .line 455
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 456
    return-void
.end method

.method private reportPurchased(Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 459
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    .line 460
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarDemo;->markProductPurchased()V

    .line 461
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 462
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAvatarDemoActivity(Lcom/sgiggle/production/AvatarDemo;)V

    .line 463
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    .line 464
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v2

    .line 465
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 466
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 467
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchasedAvatarMessage;

    const/4 v3, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchasedAvatarMessage;-><init>(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 469
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 470
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onBackPressed()V

    .line 471
    return-void
.end method

.method private static setActiveInstance(Lcom/sgiggle/production/AvatarDemo;)V
    .locals 0
    .parameter

    .prologue
    .line 131
    sput-object p0, Lcom/sgiggle/production/AvatarDemo;->s_instance:Lcom/sgiggle/production/AvatarDemo;

    .line 132
    return-void
.end method


# virtual methods
.method public confirmPurchaseFailed()V
    .locals 0

    .prologue
    .line 564
    return-void
.end method

.method public goBack()V
    .locals 3

    .prologue
    .line 555
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    .line 556
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToAvatarProductCatalogMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToAvatarProductCatalogMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 558
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onBackPressed()V

    .line 559
    return-void
.end method

.method handleDisplay(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;)V
    .locals 6
    .parameter

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 269
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getVgoodBundleList()Ljava/util/List;

    move-result-object v2

    .line 270
    const-string v0, "AvatarDemo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "avatars bundle size:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->showProgressDialog()V

    .line 276
    iput-boolean v4, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    .line 277
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 278
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_productMarketId:Ljava/lang/String;

    .line 279
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getLabel()Ljava/lang/String;

    move-result-object v3

    .line 280
    const v0, 0x7f0a0028

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 281
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 284
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    iget-object v1, v0, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 285
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v0, v1, :cond_3

    .line 286
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarDemo;->markProductPurchased()V

    .line 289
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getAllCached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 291
    if-eqz v0, :cond_4

    .line 292
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v1

    .line 293
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    .line 294
    invoke-virtual {p0, v1, v2, v3}, Lcom/sgiggle/production/AvatarDemo;->play(Ljava/lang/String;J)V

    .line 298
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    const v1, 0x7f0200df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 300
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 220
    if-nez p1, :cond_1

    .line 221
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 227
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 252
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;

    .line 253
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 259
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->hideProgressDialog()V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_btnClicked:Z

    goto :goto_0

    .line 229
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;

    .line 230
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/AvatarDemo;->handleDisplay(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailEvent;)V

    goto :goto_0

    .line 234
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayDemoAvatarEvent;

    .line 235
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayDemoAvatarEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->startAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    goto :goto_0

    .line 239
    :sswitch_3
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;

    .line 240
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarPaused:Z

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->handleRenderRequest(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;)V

    .line 242
    iget-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getUpdatedTracks()I

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->handlerTimer:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->taskHideTipBubble:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->handlerTimer:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->taskHideTipBubble:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 246
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarUpdated:Z

    goto :goto_0

    .line 257
    :cond_2
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AvatarDemo;->reportPurchased(Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;)V

    goto :goto_1

    .line 227
    :sswitch_data_0
    .sparse-switch
        0x8980 -> :sswitch_0
        0x89ad -> :sswitch_3
        0x89af -> :sswitch_1
        0x89b0 -> :sswitch_2
    .end sparse-switch
.end method

.method hideProgressDialog()V
    .locals 1

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 524
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 526
    :cond_0
    return-void
.end method

.method onAnimationFinished()V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_selector:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 391
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 384
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 385
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAvatarDemoActivity(Lcom/sgiggle/production/AvatarDemo;)V

    .line 386
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->goBack()V

    .line 387
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 419
    iget-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_btnClicked:Z

    if-nez v0, :cond_0

    .line 420
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_btnClicked:Z

    .line 421
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a002b

    if-ne v0, v1, :cond_1

    .line 422
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarDemo;->onWandClick()V

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 423
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0029

    if-ne v0, v1, :cond_0

    .line 424
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarDemo;->onBuyClick()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0a0022

    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 141
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 142
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/AvatarDemo;->requestWindowFeature(I)Z

    .line 143
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->setContentView(I)V

    .line 144
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setAvatarDemoActivity(Lcom/sgiggle/production/AvatarDemo;)V

    .line 147
    const-string v0, "AvatarDemo"

    const-string v1, "onCreate(Bundle savedInstanceState) enter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iput-object v5, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    .line 149
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_pm:Landroid/os/PowerManager;

    .line 151
    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 153
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setZOrderMediaOverlay(Z)V

    .line 154
    new-instance v0, Lcom/sgiggle/production/avatar/AvatarRenderer;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v2, v5}, Lcom/sgiggle/production/avatar/AvatarRenderer;-><init>(IZLcom/sgiggle/production/CallSession;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    .line 155
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 157
    const v0, 0x7f0a002b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 161
    const v0, 0x7f0a002c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    .line 162
    const v0, 0x7f0a002d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 166
    const v0, 0x7f0a0029

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    const v0, 0x7f0a002a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 170
    const v0, 0x7f0a0027

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 172
    const v0, 0x7f040008

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_selectorIn:Landroid/view/animation/Animation;

    .line 174
    invoke-static {p0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->InitEngine(Landroid/content/Context;)V

    .line 175
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 176
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 504
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 505
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 342
    const-string v0, "AvatarDemo"

    const-string v1, "Received onPause Event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/TangoApp;->setCurrentActivityInstance(Landroid/app/Activity;)V

    .line 345
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPause()V

    .line 346
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 347
    invoke-static {p0}, Lcom/sgiggle/production/AvatarDemo;->clearActiveInstance(Lcom/sgiggle/production/AvatarDemo;)V

    .line 348
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onPause()V

    .line 349
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 350
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeGraphics()V

    .line 351
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->RenderClear()V

    .line 353
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z

    if-eqz v0, :cond_1

    .line 354
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/TangoApp;->setAvatarDemoActivity(Lcom/sgiggle/production/AvatarDemo;)V

    .line 356
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarPaused:Z

    .line 357
    iget-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z

    if-eqz v0, :cond_2

    .line 358
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->goBack()V

    .line 363
    :goto_0
    return-void

    .line 360
    :cond_2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarBackgroundCleanupMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarBackgroundCleanupMessage;-><init>()V

    .line 361
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 328
    const-string v0, "AvatarDemo"

    const-string v1, "Received onResume Event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    iput-boolean v2, p0, Lcom/sgiggle/production/AvatarDemo;->m_btnClicked:Z

    .line 331
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onResume()V

    .line 332
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onResume()V

    .line 333
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 334
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    .line 335
    invoke-static {p0}, Lcom/sgiggle/production/AvatarDemo;->setActiveInstance(Lcom/sgiggle/production/AvatarDemo;)V

    .line 336
    iput-boolean v2, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarPaused:Z

    .line 337
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setCurrentActivityInstance(Landroid/app/Activity;)V

    .line 338
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 371
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 180
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onStart()V

    .line 182
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z

    .line 183
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 184
    if-eqz v0, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 188
    :cond_0
    const-string v0, "AvatarDemo"

    const-string v1, "onStart() enter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 191
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 192
    new-instance v1, Lcom/sgiggle/production/receiver/ScreenReceiver;

    invoke-direct {v1}, Lcom/sgiggle/production/receiver/ScreenReceiver;-><init>()V

    iput-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    .line 193
    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/AvatarDemo;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 196
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 198
    :cond_1
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 205
    iput-object v3, p0, Lcom/sgiggle/production/AvatarDemo;->m_receiver:Landroid/content/BroadcastReceiver;

    .line 208
    :cond_0
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 209
    if-eqz v0, :cond_1

    .line 210
    iget-object v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 211
    iput-object v3, p0, Lcom/sgiggle/production/AvatarDemo;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 212
    iput-boolean v2, p0, Lcom/sgiggle/production/AvatarDemo;->m_is_incoming_call:Z

    .line 215
    :cond_1
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onStop()V

    .line 216
    return-void
.end method

.method public onUserLeaveHint()V
    .locals 3

    .prologue
    .line 376
    const-string v0, "AvatarDemo"

    const-string v1, "Received onUserLeaveHint Event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarBackgroundCleanupMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarBackgroundCleanupMessage;-><init>()V

    .line 378
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 379
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onUserLeaveHint()V

    .line 380
    return-void
.end method

.method play(Ljava/lang/String;J)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 396
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartDemoAvatarMessage;

    invoke-direct {v0, p2, p3}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartDemoAvatarMessage;-><init>(J)V

    .line 397
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 398
    return-void
.end method

.method public purchaseProcessed()V
    .locals 0

    .prologue
    .line 537
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 2
    .parameter

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_buy:Landroid/widget/Button;

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/sgiggle/production/AvatarDemo;->m_purchased:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 548
    if-nez p1, :cond_0

    .line 549
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarDemo;->showDialog(I)V

    .line 551
    :cond_0
    return-void

    .line 547
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 530
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne p2, v0, :cond_0

    .line 531
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarDemo;->markProductPurchased()V

    .line 533
    :cond_0
    return-void
.end method

.method showProgressDialog()V
    .locals 2

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->hideProgressDialog()V

    .line 509
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 510
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0900fc

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/AvatarDemo;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 511
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 512
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sgiggle/production/AvatarDemo$4;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AvatarDemo$4;-><init>(Lcom/sgiggle/production/AvatarDemo;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 518
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 519
    return-void
.end method

.method showWand(Z)V
    .locals 3
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 306
    if-eqz p1, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 310
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->wandBubble:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09011b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 311
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_selector:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 316
    :cond_0
    return-void
.end method

.method startAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V
    .locals 2
    .parameter

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_avatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 404
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarDemo;->hideProgressDialog()V

    .line 407
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f09011c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 408
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->bottomBubble:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 410
    iget-object v0, p0, Lcom/sgiggle/production/AvatarDemo;->m_wand:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 415
    :cond_0
    return-void
.end method
