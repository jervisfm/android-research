.class public Lcom/sgiggle/production/AvatarSubscriptionActivity;
.super Lcom/sgiggle/production/BillingSupportBaseActivity;
.source "AvatarSubscriptionActivity.java"

# interfaces
.implements Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.AvatarSub"

.field private static s_instance:Lcom/sgiggle/production/AvatarSubscriptionActivity;


# instance fields
.field private m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

.field private m_footer:Landroid/view/View;

.field private m_progressDialog:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;-><init>()V

    return-void
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/AvatarSubscriptionActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 50
    const-string v0, "Tango.AvatarSub"

    const-string v1, "clearRunningInstance"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    sget-object v0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->s_instance:Lcom/sgiggle/production/AvatarSubscriptionActivity;

    if-ne v0, p0, :cond_0

    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->s_instance:Lcom/sgiggle/production/AvatarSubscriptionActivity;

    .line 53
    :cond_0
    return-void
.end method

.method private dismissNewBadge()V
    .locals 3

    .prologue
    .line 289
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;

    const-string v1, "product.category.avatar"

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;-><init>(Ljava/lang/String;)V

    .line 291
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 292
    return-void
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/AvatarSubscriptionActivity;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->s_instance:Lcom/sgiggle/production/AvatarSubscriptionActivity;

    return-object v0
.end method

.method private hideErrorMessage()V
    .locals 2

    .prologue
    .line 235
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 236
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 237
    return-void
.end method

.method private purchaseFree(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 10
    .parameter

    .prologue
    .line 173
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    .line 174
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v2

    .line 175
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 176
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 177
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    .line 181
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;

    const-string v3, "Free avatar"

    const/4 v4, 0x1

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    const/4 v9, 0x0

    move-object v8, v5

    invoke-direct/range {v0 .. v9}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAvatarMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Z)V

    .line 186
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 187
    return-void
.end method

.method private reportPurchased(Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 190
    iget-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setProductPurchasedFlag(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/AvatarSubscriptionActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 45
    const-string v0, "Tango.AvatarSub"

    const-string v1, "setRunningInstance"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    sput-object p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->s_instance:Lcom/sgiggle/production/AvatarSubscriptionActivity;

    .line 47
    return-void
.end method

.method private showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 3
    .parameter

    .prologue
    .line 194
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarProductDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 196
    return-void
.end method

.method private showErrorMessage()V
    .locals 2

    .prologue
    .line 230
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 231
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    return-void
.end method


# virtual methods
.method public confirmPurchaseFailed()V
    .locals 0

    .prologue
    .line 264
    return-void
.end method

.method protected getFooterView()Landroid/view/View;
    .locals 3

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030007

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 81
    return-object v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 258
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->onBackPressed()V

    .line 259
    return-void
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 136
    const-string v0, "Tango.AvatarSub"

    const-string v1, "handleNewMessage"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    if-nez p1, :cond_0

    .line 168
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 143
    sparse-switch v0, :sswitch_data_0

    .line 165
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 146
    :sswitch_0
    const-string v0, "Tango.AvatarSub"

    const-string v1, "handleNewMessage:DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;

    .line 148
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->onProductListChanged(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;)V

    .line 149
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->dismissNewBadge()V

    goto :goto_0

    .line 154
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;

    .line 155
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showErrorMessage()V

    .line 161
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->hideProgressDialog()V

    goto :goto_0

    .line 159
    :cond_1
    invoke-direct {p0, p1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->reportPurchased(Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;)V

    goto :goto_1

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x8980 -> :sswitch_1
        0x89ae -> :sswitch_0
    .end sparse-switch
.end method

.method hideProgressDialog()V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 286
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 109
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;-><init>()V

    .line 110
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 111
    return-void
.end method

.method public onClick(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 116
    const-string v0, "Tango.AvatarSub"

    const-string v1, "onClick"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const v0, 0x7f0a0029

    if-ne p1, v0, :cond_3

    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showProgressDialog()V

    .line 124
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 125
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0

    .line 127
    :cond_2
    invoke-direct {p0, p2}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->purchaseFree(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0

    .line 128
    :cond_3
    const v0, 0x7f0a0033

    if-ne p1, v0, :cond_0

    .line 129
    invoke-direct {p0, p2}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->setContentView(I)V

    .line 60
    new-instance v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setOnClickListener(Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;)V

    .line 63
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 64
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getFooterView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_footer:Landroid/view/View;

    .line 67
    iget-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_footer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 69
    iget-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 70
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 71
    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 73
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 75
    invoke-static {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->setRunningInstance(Lcom/sgiggle/production/AvatarSubscriptionActivity;)V

    .line 76
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onDestroy()V

    .line 103
    const-string v0, "Tango.AvatarSub"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-static {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->clearRunningInstance(Lcom/sgiggle/production/AvatarSubscriptionActivity;)V

    .line 105
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 201
    invoke-direct {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 202
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setCurrentActivityInstance(Landroid/app/Activity;)V

    .line 94
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 95
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 96
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeEngine()V

    .line 97
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPause()V

    .line 98
    return-void
.end method

.method public onProductListChanged(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 206
    const-string v0, "Tango.AvatarSub"

    const-string v1, "onProductListChanged: Enter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    if-eq v0, v1, :cond_0

    .line 208
    const-string v0, "Tango.AvatarSub"

    const-string v1, "onProductListChanged:evt.payload().hasError()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->hideProgressDialog()V

    .line 210
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showErrorMessage()V

    .line 227
    :goto_0
    return-void

    .line 214
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryList()Ljava/util/List;

    move-result-object v0

    .line 215
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 216
    invoke-direct {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->hideErrorMessage()V

    .line 218
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setProducts(Ljava/util/List;)V

    .line 219
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAllCached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 220
    const-string v0, "Tango.AvatarSub"

    const-string v1, "onProductListChanged:evt.payload().getAllCached()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 222
    iget-object v1, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_footer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 223
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->hideProgressDialog()V

    goto :goto_0

    .line 226
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showProgressDialog()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 86
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onResume()V

    .line 87
    const-string v0, "Tango.AvatarSub"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AvatarSubscriptionActivity::onResume::hashCode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setCurrentActivityInstance(Landroid/app/Activity;)V

    .line 89
    return-void
.end method

.method public purchaseProcessed()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 1
    .parameter

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setBillingSupported(Z)V

    .line 251
    if-nez p1, :cond_0

    .line 252
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->showDialog(I)V

    .line 254
    :cond_0
    return-void
.end method

.method public setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->notifyDataSetChanged()V

    .line 242
    return-void
.end method

.method showProgressDialog()V
    .locals 2

    .prologue
    .line 267
    const-string v0, "Tango.AvatarSub"

    const-string v1, "showProgressDialog"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-virtual {p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->hideProgressDialog()V

    .line 269
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 270
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0900fe

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 272
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sgiggle/production/AvatarSubscriptionActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/AvatarSubscriptionActivity$1;-><init>(Lcom/sgiggle/production/AvatarSubscriptionActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 278
    iget-object v0, p0, Lcom/sgiggle/production/AvatarSubscriptionActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 279
    return-void
.end method
