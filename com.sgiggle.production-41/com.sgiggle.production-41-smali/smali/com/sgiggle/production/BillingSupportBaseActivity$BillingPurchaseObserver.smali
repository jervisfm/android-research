.class Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;
.super Ljava/lang/Object;
.source "BillingSupportBaseActivity.java"

# interfaces
.implements Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/BillingSupportBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BillingPurchaseObserver"
.end annotation


# instance fields
.field m_handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/BillingSupportBaseActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/BillingSupportBaseActivity;Lcom/sgiggle/production/BillingSupportBaseActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/BillingSupportBaseActivity;)V

    return-void
.end method


# virtual methods
.method public onBillingSupported(Z)V
    .locals 3
    .parameter

    .prologue
    .line 71
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBillingSupported:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/payments/BillingServiceManager;->setSupported(Z)V

    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->setBillingSupported(Z)V

    .line 76
    if-eqz p1, :cond_0

    .line 77
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->restoreTransactions()V

    .line 78
    :cond_0
    return-void
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 134
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResponseCodeReceived, responseCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    sget-object v0, Lcom/sgiggle/production/BillingSupportBaseActivity$2;->$SwitchMap$com$sgiggle$production$payments$Constants$ResponseCode:[I

    invoke-virtual {p2}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 138
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->purchaseProcessed()V

    goto :goto_0

    .line 142
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v3, v1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 143
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPurchaseCancelled()V

    goto :goto_0

    .line 149
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v3, v1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    goto :goto_0

    .line 155
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v3, v1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 156
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    #getter for: Lcom/sgiggle/production/BillingSupportBaseActivity;->isActive:Z
    invoke-static {v0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->access$100(Lcom/sgiggle/production/BillingSupportBaseActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->showDialog(I)V

    goto :goto_0

    .line 136
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 165
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Transactions response received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    sget-object v0, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne p2, v0, :cond_0

    .line 167
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->setStoreTransactionRestored()V

    .line 168
    const-string v0, "BillingService"

    const-string v1, "Completed restoring transactions"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_0
    return-void

    .line 170
    :cond_0
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Transactions Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 96
    iget-object v10, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->m_handler:Landroid/os/Handler;

    new-instance v0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver$1;-><init>(Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 103
    return-void
.end method

.method public postPurchaseStateChange2(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 109
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postPurchaseStateChange: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    sget-object v0, Lcom/sgiggle/production/BillingSupportBaseActivity$2;->$SwitchMap$com$sgiggle$production$payments$Constants$PurchaseState:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 129
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-virtual {v0, p2, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 130
    return-void

    .line 122
    :pswitch_1
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    iget-object v0, v0, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 83
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->skipWelcomePageOnce()V

    .line 84
    iget-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/BillingSupportBaseActivity;

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/BillingSupportBaseActivity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    goto :goto_0
.end method
