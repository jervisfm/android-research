.class public abstract Lcom/sgiggle/production/BillingSupportBaseActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "BillingSupportBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/BillingSupportBaseActivity$2;,
        Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;
    }
.end annotation


# static fields
.field protected static final DIALOG_BILLING_NOT_SUPPORTED:I = 0x2

.field protected static final DIALOG_SHOW_ERROR:I = 0x1

.field private static final TAG:Ljava/lang/String; = "billingSupportActivity"


# instance fields
.field private isActive:Z

.field private m_purchaseObserver:Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 29
    new-instance v0, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/BillingSupportBaseActivity;Lcom/sgiggle/production/BillingSupportBaseActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->m_purchaseObserver:Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->isActive:Z

    .line 67
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/BillingSupportBaseActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->isActive:Z

    return v0
.end method


# virtual methods
.method public abstract confirmPurchaseFailed()V
.end method

.method public abstract goBack()V
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 184
    packed-switch p1, :pswitch_data_0

    .line 198
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 186
    :pswitch_0
    invoke-static {p0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getBillingNotSupportedDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 188
    :pswitch_1
    new-instance v0, Lcom/sgiggle/production/BillingSupportBaseActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/BillingSupportBaseActivity$1;-><init>(Lcom/sgiggle/production/BillingSupportBaseActivity;)V

    .line 195
    invoke-static {p0, v0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getPurchaseFailedDialog(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 57
    const-string v0, "billingSupportActivity"

    const-string v1, "onPuase"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->isActive:Z

    .line 60
    invoke-virtual {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->m_purchaseObserver:Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->unregisterObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)Z

    .line 61
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->unbindService()V

    .line 62
    return-void
.end method

.method public onPurchaseCancelled()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 45
    const-string v0, "billingSupportActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->isActive:Z

    .line 48
    invoke-virtual {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/BillingSupportBaseActivity;->m_purchaseObserver:Lcom/sgiggle/production/BillingSupportBaseActivity$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->registerObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)V

    .line 49
    invoke-virtual {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    move-object v0, p0

    .line 52
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->bindServiceAndSetContext(Landroid/content/Context;)V

    .line 53
    return-void
.end method

.method protected purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 3
    .parameter

    .prologue
    .line 176
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    .line 177
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v1

    .line 178
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z

    .line 179
    return-void
.end method

.method public abstract purchaseProcessed()V
.end method

.method public abstract setBillingSupported(Z)V
.end method

.method public abstract setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
.end method
