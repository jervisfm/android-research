.class Lcom/sgiggle/production/C2dmReceiver$1;
.super Landroid/os/Handler;
.source "C2dmReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/C2dmReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/C2dmReceiver;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/C2dmReceiver;)V
    .locals 0
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .parameter

    .prologue
    .line 111
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 147
    :goto_0
    return-void

    .line 113
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 114
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleRegistrationSuccess(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sgiggle/production/C2dmReceiver;->access$000(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;)V

    goto :goto_0

    .line 118
    :pswitch_1
    iget-object v1, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleUnregistration()V
    invoke-static {v1}, Lcom/sgiggle/production/C2dmReceiver;->access$100(Lcom/sgiggle/production/C2dmReceiver;)V

    goto :goto_0

    .line 122
    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 123
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleRegistrationError(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sgiggle/production/C2dmReceiver;->access$200(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;)V

    goto :goto_0

    .line 127
    :pswitch_3
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, Lcom/sgiggle/production/C2dmReceiver$CallPayload;

    move-object v7, v0

    .line 128
    iget-object v1, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    iget-object v2, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_peerJid:Ljava/lang/String;

    iget-object v3, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_uniqueId:Ljava/lang/String;

    iget-object v4, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_sessionId:Ljava/lang/String;

    iget-object v5, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_swiftIp:Ljava/lang/String;

    iget v6, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_swiftTcpPort:I

    iget v7, v7, Lcom/sgiggle/production/C2dmReceiver$CallPayload;->m_swiftUdpPort:I

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleCallPush(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    invoke-static/range {v1 .. v7}, Lcom/sgiggle/production/C2dmReceiver;->access$300(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0

    .line 132
    :pswitch_4
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sgiggle/production/C2dmReceiver$MessagePayload;

    .line 133
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    iget-object v3, v1, Lcom/sgiggle/production/C2dmReceiver$MessagePayload;->m_title:Ljava/lang/String;

    iget-object v1, v1, Lcom/sgiggle/production/C2dmReceiver$MessagePayload;->m_message:Ljava/lang/String;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleMessagePush(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v1}, Lcom/sgiggle/production/C2dmReceiver;->access$400(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    :pswitch_5
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;

    .line 138
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleActionMessagePush(Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;)V
    invoke-static {v2, v1}, Lcom/sgiggle/production/C2dmReceiver;->access$500(Lcom/sgiggle/production/C2dmReceiver;Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;)V

    goto :goto_0

    .line 142
    :pswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;

    .line 143
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver$1;->this$0:Lcom/sgiggle/production/C2dmReceiver;

    #calls: Lcom/sgiggle/production/C2dmReceiver;->handleConversationMessagePush(Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;)V
    invoke-static {v2, v1}, Lcom/sgiggle/production/C2dmReceiver;->access$600(Lcom/sgiggle/production/C2dmReceiver;Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;)V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
