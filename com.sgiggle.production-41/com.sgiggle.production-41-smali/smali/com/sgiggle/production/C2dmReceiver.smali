.class public Lcom/sgiggle/production/C2dmReceiver;
.super Lcom/google/android/c2dm/C2DMBaseReceiver;
.source "C2dmReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;,
        Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;,
        Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;,
        Lcom/sgiggle/production/C2dmReceiver$MessagePayload;,
        Lcom/sgiggle/production/C2dmReceiver$CallPayload;
    }
.end annotation


# static fields
.field public static final ACTION_TYPE_CALL:Ljava/lang/String; = "offer-call"

.field public static final ACTION_TYPE_TC_NEW_MESSAGE:Ljava/lang/String; = "newMessage"

.field public static final ACTION_TYPE_TC_VIDEOMAIL:Ljava/lang/String; = "vm"

.field private static final MESSAGE_C2DM_ACTION_MESSAGE:I = 0x6

.field private static final MESSAGE_C2DM_CALL:I = 0x4

.field private static final MESSAGE_C2DM_CONVERSATION_MESSAGE:I = 0x7

.field private static final MESSAGE_C2DM_ERROR:I = 0x3

.field private static final MESSAGE_C2DM_MESSAGE:I = 0x5

.field private static final MESSAGE_C2DM_REGISTERED:I = 0x1

.field private static final MESSAGE_C2DM_UNREGISTERED:I = 0x2

.field private static final PUSH_TYPE_ALERT_COUNT:I = 0x3

.field private static final PUSH_TYPE_CALL:I = 0x0

.field private static final PUSH_TYPE_MESSAGE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.C2dmReceiver"


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 152
    const-string v0, "tangodeveloper@gmail.com"

    invoke-direct {p0, v0}, Lcom/google/android/c2dm/C2DMBaseReceiver;-><init>(Ljava/lang/String;)V

    .line 108
    new-instance v0, Lcom/sgiggle/production/C2dmReceiver$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/C2dmReceiver$1;-><init>(Lcom/sgiggle/production/C2dmReceiver;)V

    iput-object v0, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    .line 153
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sgiggle/production/C2dmReceiver;->handleRegistrationSuccess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/C2dmReceiver;)V
    .locals 0
    .parameter

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sgiggle/production/C2dmReceiver;->handleUnregistration()V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sgiggle/production/C2dmReceiver;->handleRegistrationError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct/range {p0 .. p6}, Lcom/sgiggle/production/C2dmReceiver;->handleCallPush(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/C2dmReceiver;->handleMessagePush(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sgiggle/production/C2dmReceiver;Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sgiggle/production/C2dmReceiver;->handleActionMessagePush(Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/C2dmReceiver;Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/sgiggle/production/C2dmReceiver;->handleConversationMessagePush(Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;)V

    return-void
.end method

.method private handleActionMessagePush(Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;)V
    .locals 7
    .parameter

    .prologue
    .line 362
    const-string v0, "Tango.C2dmReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "C2dm Action message received: title="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;->m_title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;->m_message:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;->m_type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    iget-object v0, p1, Lcom/sgiggle/production/C2dmReceiver$ActionMessagePayload;->m_type:Ljava/lang/String;

    const-string v1, "offer-call"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    check-cast p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;

    .line 366
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_title:Ljava/lang/String;

    iget-object v2, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_type:Ljava/lang/String;

    iget-object v4, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_firstName:Ljava/lang/String;

    iget-object v5, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_lastName:Ljava/lang/String;

    iget-object v6, p1, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;->m_accountId:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/TangoApp;->onC2dmCallMessageReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_0
    return-void
.end method

.method private handleCallPush(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "C2dm call received: peerJid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uniqueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 344
    if-eqz p3, :cond_0

    .line 346
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sessionId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 348
    :cond_0
    if-eqz p4, :cond_1

    .line 350
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", swiftIp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", swiftTcpPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", swiftUdpPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 352
    :cond_1
    const-string v1, "Tango.C2dmReceiver"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/TangoApp;->onC2dmCallReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 354
    return-void
.end method

.method private handleConversationMessagePush(Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;)V
    .locals 6
    .parameter

    .prologue
    .line 371
    const-string v0, "Tango.C2dmReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "C2dm Conversation message received: type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_messageType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    iget v1, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_messageType:I

    iget-object v2, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_messageId:Ljava/lang/String;

    iget-object v3, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_peerAccountId:Ljava/lang/String;

    iget-object v4, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_peerName:Ljava/lang/String;

    iget-object v5, p1, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;->m_messageContent:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/TangoApp;->onC2dmConversationMessageReceived(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    return-void
.end method

.method private handleMessagePush(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 357
    const-string v0, "Tango.C2dmReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "C2dm message received: title="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/TangoApp;->onC2dmMessageReceived(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method private handleRegistrationError(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 339
    const-string v0, "Tango.C2dmReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "C2dm registration failed, should try again later:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    return-void
.end method

.method private handleRegistrationSuccess(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 330
    const-string v0, "Tango.C2dmReceiver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "C2dm registration completed: registrationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/TangoApp;->onC2dmRegistrationIdReceived(Ljava/lang/String;)V

    .line 332
    return-void
.end method

.method private handleUnregistration()V
    .locals 2

    .prologue
    .line 335
    const-string v0, "Tango.C2dmReceiver"

    const-string v1, "C2dm unregistration done, new messages from the authorized sender will be rejected."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    return-void
.end method

.method private parseNameFromC2dmAlert(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 300
    const-string v0, ""

    .line 301
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 317
    :goto_0
    return-object v0

    .line 304
    :cond_0
    const-string v1, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Retrieve "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    const-string v1, "-"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 307
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 308
    add-int/lit8 v1, v1, 0x1

    .line 309
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 311
    :try_start_0
    invoke-virtual {p2, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 316
    :goto_1
    const-string v1, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Name in C2dm Alert "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    :catch_0
    move-exception v1

    .line 313
    const-string v1, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed in retriving "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public onCreate()V
    .locals 4

    .prologue
    .line 157
    invoke-super {p0}, Lcom/google/android/c2dm/C2DMBaseReceiver;->onCreate()V

    .line 159
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 162
    const-string v1, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onError(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 180
    return-void
.end method

.method public onMessage(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 184
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 185
    if-nez v0, :cond_0

    .line 186
    const-string v0, "Tango.C2dmReceiver"

    const-string v1, "C2dm intent \'s extra-bundle is null. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :goto_0
    return-void

    .line 191
    :cond_0
    const-string v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 192
    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, v7

    .line 195
    :goto_1
    const-string v2, "Tango.C2dmReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Handling C2dm intent of type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    packed-switch v1, :pswitch_data_0

    .line 292
    :pswitch_0
    const-string v0, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "C2dm unknown push type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 193
    :cond_1
    const-string v2, "3"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x3

    goto :goto_1

    .line 199
    :pswitch_1
    const-string v1, "peerJid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 200
    const-string v1, "uniqueId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 201
    const-string v1, "sessionId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 202
    const-string v1, "swift_ip"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 206
    :try_start_0
    const-string v1, "swift_tcp_port"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 207
    :try_start_1
    const-string v6, "swift_udp_port"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    move v7, v0

    move v6, v1

    .line 213
    :goto_2
    iget-object v8, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v9, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v10, 0x4

    new-instance v0, Lcom/sgiggle/production/C2dmReceiver$CallPayload;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/sgiggle/production/C2dmReceiver$CallPayload;-><init>(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v9, v10, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 209
    :catch_0
    move-exception v1

    move v1, v9

    .line 211
    :goto_3
    const-string v6, "Tango.C2dmReceiver"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "swift_tcp_port or swift_udp_port string-to-int conversion fails, ignore this push "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Landroid/os/Bundle;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v9

    move v6, v1

    goto :goto_2

    .line 219
    :pswitch_2
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 220
    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 221
    iget-object v2, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v4, 0x5

    new-instance v5, Lcom/sgiggle/production/C2dmReceiver$MessagePayload;

    invoke-direct {v5, p0, v1, v0}, Lcom/sgiggle/production/C2dmReceiver$MessagePayload;-><init>(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 227
    :pswitch_3
    const-string v1, "ac"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 228
    const-string v2, "title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 231
    const-string v4, "Tango.C2dmReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "receive PUSH_TYPE_ALERT action "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " title "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    const-string v4, "offer-call"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 234
    const-string v1, "raid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 235
    const-string v1, ""

    .line 236
    const-string v4, ""

    .line 237
    const-string v5, "npos"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    const-string v5, ","

    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 239
    const/4 v7, -0x1

    if-eq v5, v7, :cond_7

    .line 241
    add-int/lit8 v1, v5, 0x1

    invoke-virtual {v0, v9, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 242
    invoke-direct {p0, v4, v3}, Lcom/sgiggle/production/C2dmReceiver;->parseNameFromC2dmAlert(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 243
    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 244
    invoke-direct {p0, v0, v3}, Lcom/sgiggle/production/C2dmReceiver;->parseNameFromC2dmAlert(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    .line 246
    :goto_4
    iget-object v7, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v9, 0x6

    new-instance v0, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/C2dmReceiver$CallMessagePayload;-><init>(Lcom/sgiggle/production/C2dmReceiver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 250
    :cond_2
    const-string v2, "newMessage"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "vm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 257
    :cond_3
    const-string v2, "messageType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 259
    if-nez v2, :cond_5

    .line 260
    const-string v2, "vm"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v7

    .line 265
    :goto_5
    const-string v2, "Tango.C2dmReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMessage: TC message type missing, fallback to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 274
    :goto_6
    const-string v1, "aid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 275
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 276
    const-string v1, "senderName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 277
    const-string v1, "messageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 279
    iget-object v7, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v8, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v9, 0x7

    new-instance v0, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/C2dmReceiver$ConversationMessagePayload;-><init>(Lcom/sgiggle/production/C2dmReceiver;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_4
    move v1, v9

    .line 263
    goto :goto_5

    .line 268
    :cond_5
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move v2, v1

    goto :goto_6

    .line 286
    :cond_6
    const-string v0, "Tango.C2dmReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "C2dm message has invalid action for TC:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 209
    :catch_1
    move-exception v6

    goto/16 :goto_3

    :cond_7
    move-object v5, v1

    goto/16 :goto_4

    :cond_8
    move v1, v9

    goto/16 :goto_1

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onRegistered(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 170
    return-void
.end method

.method public onUnregistered(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/C2dmReceiver;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 175
    return-void
.end method
