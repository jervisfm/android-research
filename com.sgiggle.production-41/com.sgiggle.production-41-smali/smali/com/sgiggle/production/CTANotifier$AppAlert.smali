.class public Lcom/sgiggle/production/CTANotifier$AppAlert;
.super Ljava/lang/Object;
.source "CTANotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CTANotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppAlert"
.end annotation


# instance fields
.field public final MILLISECS_IN_24HRS:I

.field private m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

.field private m_time_ms:J


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V
    .locals 2
    .parameter

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const v0, 0x5265c00

    iput v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->MILLISECS_IN_24HRS:I

    .line 47
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_time_ms:J

    .line 50
    iput-object p1, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 51
    return-void
.end method


# virtual methods
.method public CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_time_ms:J

    return-wide v0
.end method

.method public isEqual(Lcom/sgiggle/production/CTANotifier$AppAlert;)Z
    .locals 1
    .parameter

    .prologue
    .line 75
    invoke-virtual {p1}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isEqual(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Z

    move-result v0

    return v0
.end method

.method public isEqual(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Z
    .locals 2
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpired()Z
    .locals 4

    .prologue
    .line 53
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_time_ms:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExpiredAfter(J)Z
    .locals 4
    .parameter

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_time_ms:J

    sub-long/2addr v0, p1

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRegistrationRequired()Z
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    .line 67
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUpgradeRequired()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    .line 72
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_UPGRADE:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidationRequired()Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier$AppAlert;->m_alertBuffer:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    .line 60
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
