.class public Lcom/sgiggle/production/CTANotifier;
.super Ljava/lang/Object;
.source "CTANotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CTANotifier$AppAlert;
    }
.end annotation


# static fields
.field private static final STORAGE_FULL_ALERT:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Tango.CTANotifier"

.field private static s_me:Lcom/sgiggle/production/CTANotifier;


# instance fields
.field private m_context:Landroid/content/Context;

.field private m_lastCtaAlertList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private m_mapRecentMsgs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_mapRecentMsgs:Ljava/util/HashMap;

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_lastCtaAlertList:Ljava/util/List;

    .line 91
    iput-object p1, p0, Lcom/sgiggle/production/CTANotifier;->m_context:Landroid/content/Context;

    .line 92
    return-void
.end method

.method public static extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/messaging/Message;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 239
    invoke-virtual {p0}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 267
    :goto_0
    if-eqz v0, :cond_2

    .line 268
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 270
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    .line 271
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getStorageAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 273
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 241
    :sswitch_0
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;

    .line 242
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getAlertsList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 246
    :sswitch_1
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoAlertsEvent;

    .line 247
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoAlertsEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateAlertsPayload;->getAlertsList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 251
    :sswitch_2
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;

    .line 252
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getAlertsList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 256
    :sswitch_3
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;

    .line 257
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAlertsList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 261
    :sswitch_4
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;

    .line 262
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAlertsList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 278
    :cond_2
    return-object v0

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0x88c3 -> :sswitch_0
        0x88de -> :sswitch_1
        0x88e9 -> :sswitch_2
        0x8997 -> :sswitch_3
        0x89ae -> :sswitch_4
    .end sparse-switch
.end method

.method public static getDefault()Lcom/sgiggle/production/CTANotifier;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/sgiggle/production/CTANotifier;->s_me:Lcom/sgiggle/production/CTANotifier;

    return-object v0
.end method

.method private static getStorageAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 5

    .prologue
    const v4, 0x7f09011f

    .line 283
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    .line 284
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v1

    .line 288
    invoke-static {v0}, Lcom/sgiggle/localstorage/LocalStorage;->isUsingExternalStorage(Landroid/content/Context;)Z

    move-result v2

    .line 289
    if-eqz v2, :cond_0

    const-string v2, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 290
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 291
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090121

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 296
    :goto_0
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 297
    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 298
    invoke-virtual {v1, v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    .line 299
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0

    .line 293
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 294
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090120

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 95
    new-instance v0, Lcom/sgiggle/production/CTANotifier;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/CTANotifier;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sgiggle/production/CTANotifier;->s_me:Lcom/sgiggle/production/CTANotifier;

    .line 96
    return-void
.end method


# virtual methods
.method public cancelAlert(I)Z
    .locals 3
    .parameter

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_mapRecentMsgs:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 217
    :goto_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/CTANotifier;->getNotificationId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 218
    return v0

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected cancelRegistrationAlerts()V
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 212
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 213
    return-void
.end method

.method protected cancelValidationAlerts()V
    .locals 1

    .prologue
    .line 201
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 202
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 203
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_EMAIL_NUMBER:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 204
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_VALIDATE_REGISTRATION_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    .line 205
    return-void
.end method

.method public cancelValidationAndRegistrationAlerts()V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/sgiggle/production/CTANotifier;->cancelValidationAlerts()V

    .line 194
    invoke-virtual {p0}, Lcom/sgiggle/production/CTANotifier;->cancelRegistrationAlerts()V

    .line 195
    return-void
.end method

.method public checkAndUpdateMap(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 103
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->hasSeverity()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    .line 113
    :goto_0
    return v0

    .line 106
    :cond_0
    new-instance v1, Lcom/sgiggle/production/CTANotifier$AppAlert;

    invoke-direct {v1, p1}, Lcom/sgiggle/production/CTANotifier$AppAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 107
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_mapRecentMsgs:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 109
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isExpiredAfter(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_mapRecentMsgs:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sgiggle/production/CTANotifier$AppAlert;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v4

    .line 113
    goto :goto_0
.end method

.method public getLastCtaAlertList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_lastCtaAlertList:Ljava/util/List;

    return-object v0
.end method

.method public getNotificationId(I)I
    .locals 1
    .parameter

    .prologue
    .line 227
    add-int/lit16 v0, p1, 0x3e8

    return v0
.end method

.method public publishCTA2NotificationBar(Lcom/sgiggle/messaging/Message;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 156
    const-string v0, "Tango.CTANotifier"

    const-string v1, "publishCTA2NotificationBar(list of CTA)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-static {p1}, Lcom/sgiggle/production/CTANotifier;->extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;

    move-result-object v0

    .line 160
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 162
    if-eqz v0, :cond_0

    .line 163
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 164
    new-instance v3, Lcom/sgiggle/production/CTANotifier$AppAlert;

    invoke-direct {v3, v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 165
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->publishCTA2NotificationBar(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 167
    invoke-virtual {v3}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move v0, v5

    .line 172
    :goto_1
    if-eqz v0, :cond_2

    .line 173
    const/4 v2, 0x0

    .line 174
    iget-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_mapRecentMsgs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 175
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 176
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CTANotifier;->cancelAlert(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v5

    .line 178
    goto :goto_1

    .line 183
    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public publishCTA2NotificationBar(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 117
    const-string v0, "Tango.CTANotifier"

    const-string v1, "publishCTA2NotificationBar(tangoCTA)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/CTANotifier;->checkAndUpdateMap(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 147
    :goto_0
    return-void

    .line 122
    :cond_0
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f0200ab

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 125
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sgiggle/production/CTANotifier;->m_context:Landroid/content/Context;

    const-class v3, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 127
    const-string v2, "OPEN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    iget-object v2, p0, Lcom/sgiggle/production/CTANotifier;->m_context:Landroid/content/Context;

    invoke-static {v2, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 137
    const-string v2, "Tango.CTANotifier"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notification: title=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\', message=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    iget-object v2, p0, Lcom/sgiggle/production/CTANotifier;->m_context:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 143
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 144
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getSeverity()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->ordinal()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sgiggle/production/CTANotifier;->getNotificationId(I)I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method

.method public saveLastCtaAlertList(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 150
    const-string v0, "Tango.CTANotifier"

    const-string v1, "saveLastCtaAlertList"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-static {p1}, Lcom/sgiggle/production/CTANotifier;->extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CTANotifier;->m_lastCtaAlertList:Ljava/util/List;

    .line 152
    return-void
.end method
