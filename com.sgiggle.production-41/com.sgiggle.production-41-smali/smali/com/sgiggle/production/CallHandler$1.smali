.class Lcom/sgiggle/production/CallHandler$1;
.super Landroid/telephony/PhoneStateListener;
.source "CallHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/CallHandler;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/CallHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 345
    iput-object p1, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 349
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    if-nez v0, :cond_1

    .line 350
    const-string v0, "Tango.CallHandler"

    const-string v1, "onCallStateChanged(): Not in a Tango call. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    if-ne p1, v2, :cond_2

    .line 355
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$100(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    goto :goto_0

    .line 358
    :cond_2
    if-ne p1, v3, :cond_4

    .line 362
    sget-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    if-eqz v0, :cond_3

    .line 363
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_0

    .line 364
    const-string v0, "Tango.CallHandler"

    const-string v1, "onCallStateChanged(): Put Tango call on hold..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iput-boolean v2, v0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    .line 368
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    invoke-static {v3, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 373
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->pstnSessionOffHook()V

    .line 377
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$100(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 379
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call-Waiting in effect. Old-muted = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 385
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v0

    .line 386
    const-string v1, "Tango.CallHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "On new telephone call: Terminate active Tango call with : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 390
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$100(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    goto/16 :goto_0

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    sget-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    if-eqz v0, :cond_0

    .line 394
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCallStateChanged(): Is mute: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->pstnSessionIdle()V

    .line 400
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-static {v3, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 404
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCallStateChanged(): Is mute: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v2}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler$1;->this$0:Lcom/sgiggle/production/CallHandler;

    #getter for: Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;
    invoke-static {v0}, Lcom/sgiggle/production/CallHandler;->access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    goto/16 :goto_0
.end method
