.class public final enum Lcom/sgiggle/production/CallHandler$VideoMode;
.super Ljava/lang/Enum;
.source "CallHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VideoMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/CallHandler$VideoMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/CallHandler$VideoMode;

.field public static final enum VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

.field public static final enum VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36
    new-instance v0, Lcom/sgiggle/production/CallHandler$VideoMode;

    const-string v1, "VIDEO_ON"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/CallHandler$VideoMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    .line 37
    new-instance v0, Lcom/sgiggle/production/CallHandler$VideoMode;

    const-string v1, "VIDEO_OFF"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/CallHandler$VideoMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/CallHandler$VideoMode;

    sget-object v1, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/CallHandler$VideoMode;->$VALUES:[Lcom/sgiggle/production/CallHandler$VideoMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/CallHandler$VideoMode;
    .locals 1
    .parameter

    .prologue
    .line 35
    const-class v0, Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallHandler$VideoMode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/CallHandler$VideoMode;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sgiggle/production/CallHandler$VideoMode;->$VALUES:[Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual {v0}, [Lcom/sgiggle/production/CallHandler$VideoMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/CallHandler$VideoMode;

    return-object v0
.end method
