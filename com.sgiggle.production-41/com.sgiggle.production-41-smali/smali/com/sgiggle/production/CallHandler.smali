.class public Lcom/sgiggle/production/CallHandler;
.super Ljava/lang/Object;
.source "CallHandler.java"

# interfaces
.implements Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CallHandler$VideoMode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.CallHandler"

.field private static s_me:Lcom/sgiggle/production/CallHandler;


# instance fields
.field private m_application:Lcom/sgiggle/production/TangoApp;

.field private m_callSession:Lcom/sgiggle/production/CallSession;

.field private m_callState:I

.field private m_phoneStateListener:Landroid/telephony/PhoneStateListener;

.field private m_telephonyMgr:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/TangoApp;)V
    .locals 3
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/CallHandler;->m_callState:I

    .line 345
    new-instance v0, Lcom/sgiggle/production/CallHandler$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/CallHandler$1;-><init>(Lcom/sgiggle/production/CallHandler;)V

    iput-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 51
    iput-object p1, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 52
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    .line 55
    sget-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Lcom/sgiggle/production/receiver/VoIPStateListener;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/receiver/VoIPStateListener;-><init>(Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;)V

    .line 57
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 58
    const-string v2, "android.intent.action.VoIP_RESUME_CALL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1, v0, v1}, Lcom/sgiggle/production/TangoApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 61
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/CallSession;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/CallHandler;)Lcom/sgiggle/production/TangoApp;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    return-object v0
.end method

.method private broadcastCallState(I)V
    .locals 3
    .parameter

    .prologue
    .line 324
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "broadcastCallState(): new State = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget v0, p0, Lcom/sgiggle/production/CallHandler;->m_callState:I

    if-ne v0, p1, :cond_1

    .line 327
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "broadcastCallState(): new State = current = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Do nothing."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 331
    :cond_1
    iput p1, p0, Lcom/sgiggle/production/CallHandler;->m_callState:I

    .line 335
    sget-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PS_CALL_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 337
    const-string v1, "state"

    iget v2, p0, Lcom/sgiggle/production/CallHandler;->m_callState:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 338
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/TangoApp;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static getDefault()Lcom/sgiggle/production/CallHandler;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/sgiggle/production/CallHandler;->s_me:Lcom/sgiggle/production/CallHandler;

    return-object v0
.end method

.method static init(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 64
    new-instance v0, Lcom/sgiggle/production/CallHandler;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/CallHandler;-><init>(Lcom/sgiggle/production/TangoApp;)V

    sput-object v0, Lcom/sgiggle/production/CallHandler;->s_me:Lcom/sgiggle/production/CallHandler;

    .line 65
    return-void
.end method


# virtual methods
.method public answerIncomingCall()V
    .locals 4

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    .line 154
    const-string v0, "Tango.CallHandler"

    const-string v1, "answerIncomingCall(): No current call session."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallHandler;->broadcastCallState(I)V

    .line 160
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$AcceptCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcceptCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public endCallSession()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 311
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_1

    .line 312
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "endCallSession(): ... with peerAccountId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v2}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " m_callerInitVideoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-direct {p0, v3}, Lcom/sgiggle/production/CallHandler;->broadcastCallState(I)V

    .line 316
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 319
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    .line 321
    :cond_1
    return-void
.end method

.method public getCallSession()Lcom/sgiggle/production/CallSession;
    .locals 3

    .prologue
    .line 75
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCallSession(): updating call session m_callSession="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    return-object v0
.end method

.method public handleCSCallResume()V
    .locals 4

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_0

    .line 416
    const-string v0, "Tango.CallHandler"

    const-string v1, "CS Call resuming"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 420
    invoke-virtual {p0}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 421
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 423
    :cond_0
    return-void
.end method

.method rejectIncomingCall()V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    .line 169
    const-string v0, "Tango.CallHandler"

    const-string v1, "rejectIncomingCall(): No current call session."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :goto_0
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallHandler;->broadcastCallState(I)V

    .line 175
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$RejectCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$RejectCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public sendCallMessage(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 97
    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    .line 98
    return-void
.end method

.method public sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    if-ne p5, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->hasVideoActivity()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 86
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    const v1, 0x7f090118

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_application:Lcom/sgiggle/production/TangoApp;

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_2
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendCallMessage(): peerName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " m_callerInitVideoCall = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;

    sget-object v3, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    if-ne p5, v3, :cond_3

    const/4 v3, 0x1

    :goto_1
    invoke-direct {v2, p1, p2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method startCallSession(Lcom/sgiggle/messaging/Message;)Lcom/sgiggle/production/CallSession;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 107
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 140
    :cond_0
    :goto_0
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/sgiggle/production/CallHandler;->broadcastCallState(I)V

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_telephonyMgr:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_phoneStateListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    return-object v0

    .line 109
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    .line 110
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;

    .line 111
    const-string v1, "Tango.CallHandler"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startCallSession - CALL_RECEIVED_EVENT, peerName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | peerAccountId:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v1, Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallReceivedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_INCOMING:Lcom/sgiggle/production/CallSession$CallState;

    invoke-direct {v1, v0, v2}, Lcom/sgiggle/production/CallSession;-><init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/production/CallSession$CallState;)V

    iput-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    goto :goto_0

    .line 122
    :pswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;

    .line 123
    const-string v1, "Tango.CallHandler"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startCallSession - SEND_CALL_INVITATION_EVENT, peerName:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " | peerAccountId:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_2

    .line 126
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "should be wrong, CallSession should be created by SEND_CALL_INVITATION_EVENT on the caller side"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_2
    new-instance v1, Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    invoke-direct {v1, v0, v2}, Lcom/sgiggle/production/CallSession;-><init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/production/CallSession$CallState;)V

    iput-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    .line 129
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoMode()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoMode()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    .line 130
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 131
    if-eqz v0, :cond_0

    .line 132
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendCallInvitationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto/16 :goto_0

    .line 129
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x88c7
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method updateCallSession(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_1

    .line 184
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateCallSession(): No current call session. Ignore: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 188
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 190
    :sswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_CONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    goto :goto_0

    .line 194
    :sswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/sgiggle/production/CallSession;->m_callStartTime:J

    .line 195
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;

    .line 196
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoMode()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 198
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_callerInitVideoCall: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 203
    :sswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 205
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallHandler;->broadcastCallState(I)V

    .line 208
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;

    .line 209
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasSpeakerOn()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getSpeakerOn()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    .line 211
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioMode: AUDIO_IN_PROGRESS_EVENT: speakerOn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasMuted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getMuted()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_muted:Z

    .line 215
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioMode: AUDIO_IN_PROGRESS_EVENT: muted="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasVideoMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 219
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVideoMode()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 220
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "m_callerInitVideoCall: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto/16 :goto_0

    .line 227
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;

    .line 228
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasSpeakeron()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 229
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getSpeakeron()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    .line 230
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioMode: AUDIO_MODE_CHANGED_EVENT: speakerOn="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasMuted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioModeChangedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getMuted()Z

    move-result v0

    iput-boolean v0, v1, Lcom/sgiggle/production/CallSession;->m_muted:Z

    .line 234
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v0, "Tango.CallHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AudioMode: AUDIO_MODE_CHANGED_EVENT: muted="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_muted:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 241
    :sswitch_5
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;

    .line 242
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 243
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 244
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 245
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/CallSession;->updateVgoodSelectorData(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)V

    goto/16 :goto_0

    .line 250
    :sswitch_6
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;

    .line 251
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 252
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 253
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 254
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideo2WayInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/CallSession;->updateVgoodSelectorData(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)V

    goto/16 :goto_0

    .line 259
    :sswitch_7
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sgiggle/production/CallSession;->m_showLowBandwidth:Z

    goto/16 :goto_0

    .line 263
    :sswitch_8
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sgiggle/production/CallSession;->m_showLowBandwidth:Z

    goto/16 :goto_0

    .line 267
    :sswitch_9
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;

    .line 268
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/CallSession;->setInCallAlertEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;)V

    goto/16 :goto_0

    .line 273
    :sswitch_a
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoModeChangedEvent;

    .line 274
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoModeChangedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoModePayload;->getCameraPosition()Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto/16 :goto_0

    .line 283
    :sswitch_b
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;

    .line 284
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_7

    .line 285
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 289
    :cond_6
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto/16 :goto_0

    .line 286
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_6

    .line 287
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_1

    .line 293
    :sswitch_c
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;

    .line 294
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 295
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto/16 :goto_0

    .line 299
    :sswitch_d
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;

    .line 300
    iget-object v1, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 301
    iget-object v0, p0, Lcom/sgiggle/production/CallHandler;->m_callSession:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto/16 :goto_0

    .line 188
    nop

    :sswitch_data_0
    .sparse-switch
        0x88cd -> :sswitch_1
        0x88cf -> :sswitch_3
        0x88d1 -> :sswitch_5
        0x88d9 -> :sswitch_0
        0x88fd -> :sswitch_6
        0x88ff -> :sswitch_2
        0x8905 -> :sswitch_7
        0x8907 -> :sswitch_8
        0x8908 -> :sswitch_9
        0x890b -> :sswitch_4
        0x8911 -> :sswitch_a
        0x89aa -> :sswitch_d
        0x89ab -> :sswitch_b
        0x89ac -> :sswitch_c
    .end sparse-switch
.end method
