.class Lcom/sgiggle/production/CallLogActivity$1;
.super Landroid/os/Handler;
.source "CallLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/CallLogActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/CallLogActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 159
    iput-object p1, p0, Lcom/sgiggle/production/CallLogActivity$1;->this$0:Lcom/sgiggle/production/CallLogActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$1;->this$0:Lcom/sgiggle/production/CallLogActivity;

    #getter for: Lcom/sgiggle/production/CallLogActivity;->m_isDestroyed:Z
    invoke-static {v0}, Lcom/sgiggle/production/CallLogActivity;->access$000(Lcom/sgiggle/production/CallLogActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler: ignoring message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; we\'re destroyed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_0
    return-void

    .line 167
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$1;->this$0:Lcom/sgiggle/production/CallLogActivity;

    #calls: Lcom/sgiggle/production/CallLogActivity;->doUpdateLogEntries()V
    invoke-static {v0}, Lcom/sgiggle/production/CallLogActivity;->access$100(Lcom/sgiggle/production/CallLogActivity;)V

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
