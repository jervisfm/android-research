.class Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;
.super Landroid/widget/ArrayAdapter;
.source "CallLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CallLogAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
        ">;"
    }
.end annotation


# instance fields
.field private layoutParamsHasDuration:Landroid/widget/RelativeLayout$LayoutParams;

.field private layoutParamsNoDuration:Landroid/widget/RelativeLayout$LayoutParams;

.field private m_entries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final m_inflater:Landroid/view/LayoutInflater;

.field private final m_resources:Landroid/content/res/Resources;

.field private final m_textViewResourceId:I

.field final synthetic this$0:Lcom/sgiggle/production/CallLogActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/CallLogActivity;Landroid/content/Context;ILjava/util/List;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v6, 0x7f0a0038

    const/16 v5, 0x9

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 316
    iput-object p1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    .line 317
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 311
    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_resources:Landroid/content/res/Resources;

    .line 318
    iput p3, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_textViewResourceId:I

    .line 319
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 320
    iput-object p4, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_entries:Ljava/util/List;

    .line 322
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsHasDuration:Landroid/widget/RelativeLayout$LayoutParams;

    .line 323
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsHasDuration:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 324
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsHasDuration:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 325
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsHasDuration:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 327
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsNoDuration:Landroid/widget/RelativeLayout$LayoutParams;

    .line 328
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsNoDuration:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xf

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsNoDuration:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 330
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->layoutParamsNoDuration:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 331
    return-void
.end method

.method private formatDuration(I)Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 433
    .line 437
    div-int/lit16 v0, p1, 0xe10

    .line 438
    rem-int/lit16 v1, p1, 0xe10

    div-int/lit8 v1, v1, 0x3c

    .line 439
    rem-int/lit8 v2, p1, 0x3c

    .line 441
    const-string v3, "%02d"

    .line 443
    if-nez v0, :cond_0

    .line 444
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 446
    :goto_0
    return-object v0

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private formatWhen(J)Ljava/lang/String;
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 406
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    .line 407
    const-wide/16 v1, 0x3e8

    mul-long/2addr v1, p1

    int-to-long v3, v0

    add-long v0, v1, v3

    .line 409
    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    .line 410
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 412
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    .line 413
    invoke-virtual {v3, v0, v1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 416
    invoke-virtual {v3, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {v3, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {v3, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    invoke-virtual {v2, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 419
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_resources:Landroid/content/res/Resources;

    const v2, 0x7f09007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 429
    :goto_0
    return-object v0

    .line 422
    :cond_0
    const-wide/32 v4, 0x5265c00

    add-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 423
    invoke-virtual {v3, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {v3, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {v3, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    invoke-virtual {v2, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 426
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_resources:Landroid/content/res/Resources;

    const v2, 0x7f09007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 429
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private localeDateString(JLjava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter
    .parameter

    .prologue
    const-wide/16 v3, 0x3e8

    .line 390
    if-eqz p3, :cond_0

    .line 391
    const v0, 0x80001

    .line 393
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    mul-long/2addr v3, p1

    invoke-static {v2, v3, v4, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    :goto_0
    return-object v0

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    #getter for: Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;
    invoke-static {v0}, Lcom/sgiggle/production/CallLogActivity;->access$200(Lcom/sgiggle/production/CallLogActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 397
    const-string v0, "Tango.CallLogUI"

    const-string v1, "!!Got null m_dateFormat, this should not happen!!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    #calls: Lcom/sgiggle/production/CallLogActivity;->getDateFormatString()Ljava/lang/String;
    invoke-static {v1}, Lcom/sgiggle/production/CallLogActivity;->access$300(Lcom/sgiggle/production/CallLogActivity;)Ljava/lang/String;

    move-result-object v1

    #setter for: Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sgiggle/production/CallLogActivity;->access$202(Lcom/sgiggle/production/CallLogActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 400
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    #getter for: Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;
    invoke-static {v1}, Lcom/sgiggle/production/CallLogActivity;->access$200(Lcom/sgiggle/production/CallLogActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 401
    new-instance v1, Ljava/util/Date;

    mul-long v2, p1, v3

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 345
    if-nez p2, :cond_0

    .line 346
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_inflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_textViewResourceId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 347
    new-instance v2, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;-><init>(Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;)V

    .line 348
    const v0, 0x7f0a000e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 349
    const v0, 0x7f0a0038

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->when:Landroid/widget/TextView;

    .line 350
    const v0, 0x7f0a0039

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    .line 351
    sget-object v0, Lcom/sgiggle/production/CallLogActivity$EntryType;->ENTRY_TYPE_UNKNOWN:Lcom/sgiggle/production/CallLogActivity$EntryType;

    iput-object v0, v2, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    .line 352
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v7, v2

    move-object v2, v1

    move-object v1, v7

    .line 357
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_entries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 358
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->when:Landroid/widget/TextView;

    iget-wide v4, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_when:J

    invoke-direct {p0, v4, v5}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->formatWhen(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    sget-object v4, Lcom/sgiggle/production/CallLogActivity$EntryType;->ENTRY_TYPE_MISSED_CALL:Lcom/sgiggle/production/CallLogActivity$EntryType;

    if-ne v3, v4, :cond_1

    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    iget-object v4, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    if-eq v3, v4, :cond_1

    .line 363
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_resources:Landroid/content/res/Resources;

    const v5, 0x7f07001c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 365
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    iget-object v0, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    iput-object v0, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    .line 368
    iget-object v0, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 369
    iget-object v0, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 386
    :goto_1
    return-object v2

    .line 354
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;

    move-object v1, v0

    move-object v2, p2

    goto :goto_0

    .line 371
    :cond_1
    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    iget-object v4, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    if-eq v3, v4, :cond_2

    .line 373
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->m_resources:Landroid/content/res/Resources;

    const v5, 0x7f07001b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 374
    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    iput-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    .line 375
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 378
    :cond_2
    iget v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_direction:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 379
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    const v4, 0x7f0200a7

    invoke-virtual {v3, v6, v6, v4, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 380
    iget-object v3, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->this$0:Lcom/sgiggle/production/CallLogActivity;

    invoke-virtual {v4}, Lcom/sgiggle/production/CallLogActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    const/high16 v5, 0x40a0

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f00

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 383
    :cond_3
    iget-object v1, v1, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter$ViewHolder;->duration:Landroid/widget/TextView;

    iget v0, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_duration:I

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;->formatDuration(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
