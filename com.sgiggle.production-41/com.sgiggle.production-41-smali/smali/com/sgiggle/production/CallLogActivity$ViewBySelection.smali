.class final enum Lcom/sgiggle/production/CallLogActivity$ViewBySelection;
.super Ljava/lang/Enum;
.source "CallLogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallLogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewBySelection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/CallLogActivity$ViewBySelection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

.field public static final enum VIEW_BY_ALL:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

.field public static final enum VIEW_BY_MISSED:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 63
    new-instance v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    const-string v1, "VIEW_BY_ALL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_ALL:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    .line 64
    new-instance v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    const-string v1, "VIEW_BY_MISSED"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_MISSED:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    sget-object v1, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_ALL:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_MISSED:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->$VALUES:[Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/CallLogActivity$ViewBySelection;
    .locals 1
    .parameter

    .prologue
    .line 62
    const-class v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/CallLogActivity$ViewBySelection;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->$VALUES:[Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    invoke-virtual {v0}, [Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    return-object v0
.end method
