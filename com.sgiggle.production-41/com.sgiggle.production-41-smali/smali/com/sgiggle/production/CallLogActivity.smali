.class public Lcom/sgiggle/production/CallLogActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "CallLogActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;,
        Lcom/sgiggle/production/CallLogActivity$CallLogEntry;,
        Lcom/sgiggle/production/CallLogActivity$EntryType;,
        Lcom/sgiggle/production/CallLogActivity$ViewBySelection;
    }
.end annotation


# static fields
.field private static final DELETE_ALL_ID:I = 0x4

.field private static final DELETE_ID:I = 0x7

.field private static final PREF_VIEW_INDEX:Ljava/lang/String; = "view_index"

.field private static final PREF_VIEW_TOP:Ljava/lang/String; = "view_top"

.field private static final SHOW_CALL_LOG_FIRST_TIME:I = 0x1

.field private static final SHOW_CALL_LOG_FIRST_TIME_DELAY:I = 0x3a98

.field private static final TAG:Ljava/lang/String; = "Tango.CallLogUI"

.field private static final VDBG:Z = true

.field private static m_logEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private static m_receivedEntriesFromServer:Z


# instance fields
.field private m_adapter:Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;

.field private m_dateFormat:Ljava/lang/String;

.field private m_displayedEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private m_emptyView:Landroid/widget/TextView;

.field private m_handler:Landroid/os/Handler;

.field private m_isDestroyed:Z

.field private m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

.field private m_listWrapper:Landroid/view/View;

.field private m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

.field private m_prefs:Landroid/content/SharedPreferences;

.field private m_progressView:Landroid/view/ViewGroup;

.field private m_viewBy:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    .line 70
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/CallLogActivity;->m_receivedEntriesFromServer:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 66
    sget-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_ALL:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_viewBy:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    .line 74
    sget-object v0, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_displayedEntries:Ljava/util/List;

    .line 94
    iput-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 95
    iput-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;

    .line 152
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_isDestroyed:Z

    .line 159
    new-instance v0, Lcom/sgiggle/production/CallLogActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/CallLogActivity$1;-><init>(Lcom/sgiggle/production/CallLogActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_handler:Landroid/os/Handler;

    .line 306
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/CallLogActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_isDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/CallLogActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->doUpdateLogEntries()V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/CallLogActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sgiggle/production/CallLogActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sgiggle/production/CallLogActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->getDateFormatString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/CallLogActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->deleteAllCallLog()V

    return-void
.end method

.method private deleteAllCallLog()V
    .locals 3

    .prologue
    .line 574
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;-><init>()V

    .line 575
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 576
    return-void
.end method

.method private deleteCallLog(Lcom/sgiggle/production/CallLogActivity$CallLogEntry;)V
    .locals 3
    .parameter

    .prologue
    .line 579
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    iget-wide v1, p1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_when:J

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setStartTime(J)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    .line 580
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteCallLogMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)V

    .line 581
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 582
    return-void
.end method

.method private displayLogEntries(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CallLogActivity$CallLogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 256
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 259
    iget-object v3, p0, Lcom/sgiggle/production/CallLogActivity;->m_viewBy:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    sget-object v4, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_MISSED:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    if-ne v3, v4, :cond_1

    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_type:Lcom/sgiggle/production/CallLogActivity$EntryType;

    sget-object v4, Lcom/sgiggle/production/CallLogActivity$EntryType;->ENTRY_TYPE_MISSED_CALL:Lcom/sgiggle/production/CallLogActivity$EntryType;

    if-ne v3, v4, :cond_0

    .line 261
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 265
    :cond_2
    const-string v0, "Tango.CallLogUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "displayLogEntries(): Filtered "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    new-instance v0, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;

    const v2, 0x7f03000b

    invoke-direct {v0, p0, p0, v2, v1}, Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;-><init>(Lcom/sgiggle/production/CallLogActivity;Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_adapter:Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;

    .line 268
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_adapter:Lcom/sgiggle/production/CallLogActivity$CallLogAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 269
    return-void
.end method

.method private doUpdateLogEntries()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_progressView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setEmptyView(Landroid/view/View;)V

    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_progressView:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 248
    :cond_0
    sget-object v0, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_displayedEntries:Ljava/util/List;

    .line 249
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_displayedEntries:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->displayLogEntries(Ljava/util/List;)V

    .line 251
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->restoreScrollPositionInList()V

    .line 252
    return-void
.end method

.method private getDateFormatString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 629
    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    .line 630
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v4

    .line 631
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 632
    aget-char v3, v0, v2

    sparse-switch v3, :sswitch_data_0

    .line 631
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 634
    :sswitch_0
    const-string v3, "dd/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 638
    :sswitch_1
    const-string v3, "MM/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 642
    :sswitch_2
    const-string v3, "yy/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 647
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    invoke-virtual {v1, v4, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 632
    nop

    :sswitch_data_0
    .sparse-switch
        0x4d -> :sswitch_1
        0x64 -> :sswitch_0
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private onDeleteAllMenuSelected()V
    .locals 3

    .prologue
    .line 551
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity;->getParent()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 553
    const v1, 0x7f090082

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 554
    const v1, 0x7f090083

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 555
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 556
    const v1, 0x7f09001f

    new-instance v2, Lcom/sgiggle/production/CallLogActivity$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/CallLogActivity$2;-><init>(Lcom/sgiggle/production/CallLogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 561
    const v1, 0x7f090020

    new-instance v2, Lcom/sgiggle/production/CallLogActivity$3;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/CallLogActivity$3;-><init>(Lcom/sgiggle/production/CallLogActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 566
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 567
    const v1, 0x7f020092

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setIcon(I)V

    .line 568
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 569
    return-void
.end method

.method private openContactDetailPage(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 604
    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ActivityStack;

    .line 605
    const-class v1, Lcom/sgiggle/production/ContactDetailActivity;

    invoke-virtual {v0, v1, p1}, Lcom/sgiggle/production/ActivityStack;->pushWithMessage(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    .line 606
    return-void
.end method

.method private restoreScrollPositionInList()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 620
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "view_index"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 621
    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v2, "view_top"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 622
    if-eq v0, v4, :cond_0

    .line 623
    iget-object v2, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v2, v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setSelectionFromTop(II)V

    .line 624
    const-string v2, "Tango.CallLogUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreScrollPositionInList: index="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", top="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    :cond_0
    return-void
.end method

.method private saveScrollPositionInList()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 609
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->getFirstVisiblePosition()I

    move-result v0

    .line 610
    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 611
    if-nez v1, :cond_0

    move v1, v2

    .line 612
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/production/CallLogActivity;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 613
    const-string v3, "view_index"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 614
    const-string v3, "view_top"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 615
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 616
    const-string v2, "Tango.CallLogUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "saveScrollPositionInList: index="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", top="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    return-void

    .line 611
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0
.end method

.method private showContactDetailPage(Lcom/sgiggle/production/CallLogActivity$CallLogEntry;)V
    .locals 4
    .parameter

    .prologue
    .line 594
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;

    invoke-virtual {p1}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 596
    return-void
.end method

.method static storeLogEntries(Lcom/sgiggle/messaging/Message;Landroid/content/Context;)V
    .locals 15
    .parameter
    .parameter

    .prologue
    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 100
    .line 103
    invoke-virtual {p0}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 118
    const-string v1, "Tango.CallLogUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "storeLogEntries(): Unsupported message = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    return-void

    .line 105
    :pswitch_0
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallLogEvent;

    .line 106
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallLogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getEntriesList()Ljava/util/List;

    move-result-object v2

    .line 107
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayCallLogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    move-result-object v1

    .line 122
    :goto_1
    const-string v3, "Tango.CallLogUI"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "storeLogEntries(): # of entries = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", result-type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 125
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-object v10, v0

    .line 126
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    if-ne v1, v2, :cond_2

    sget-object v1, Lcom/sgiggle/production/CallLogActivity$EntryType;->ENTRY_TYPE_MISSED_CALL:Lcom/sgiggle/production/CallLogActivity$EntryType;

    move-object v8, v1

    .line 129
    :goto_3
    new-instance v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNamePrefix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getFirstName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getMiddleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getLastName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNameSuffix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getAccountId()Ljava/lang/String;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/production/CallLogActivity$EntryType;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    if-eq v2, v3, :cond_0

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    if-ne v2, v3, :cond_3

    .line 134
    :cond_0
    iput v13, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_direction:I

    .line 139
    :goto_4
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getEmail()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_email:Ljava/lang/String;

    .line 140
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_phoneNumber:Ljava/lang/String;

    .line 141
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getStartTime()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_when:J

    .line 142
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDuration()I

    move-result v2

    iput v2, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_duration:I

    .line 143
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDeviceContactId()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_deviceContactId:J

    .line 144
    invoke-interface {v11, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 111
    :pswitch_1
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateCallLogEvent;

    .line 112
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateCallLogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getEntriesList()Ljava/util/List;

    move-result-object v2

    .line 113
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateCallLogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    move-result-object v1

    .line 114
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    if-ne v1, v3, :cond_1

    move v3, v14

    :goto_5
    sput-boolean v3, Lcom/sgiggle/production/CallLogActivity;->m_receivedEntriesFromServer:Z

    goto/16 :goto_1

    :cond_1
    move v3, v13

    goto :goto_5

    .line 126
    :cond_2
    sget-object v1, Lcom/sgiggle/production/CallLogActivity$EntryType;->ENTRY_TYPE_NORMAL_CALL:Lcom/sgiggle/production/CallLogActivity$EntryType;

    move-object v8, v1

    goto :goto_3

    .line 136
    :cond_3
    iput v14, v1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_direction:I

    goto :goto_4

    .line 147
    :cond_4
    sput-object v11, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    goto/16 :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x8914
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private videoCall(Lcom/sgiggle/production/CallLogActivity$CallLogEntry;)V
    .locals 6
    .parameter

    .prologue
    .line 585
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_accountId:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->displayName()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p1, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_deviceContactId:J

    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    .line 587
    return-void
.end method


# virtual methods
.method protected handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 223
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-nez p1, :cond_0

    .line 239
    :goto_0
    return-void

    .line 229
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 236
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Unsupported message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 231
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    .line 232
    invoke-direct {p0, p1}, Lcom/sgiggle/production/CallLogActivity;->openContactDetailPage(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V

    goto :goto_0

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x8990
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 682
    packed-switch p2, :pswitch_data_0

    .line 690
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_displayedEntries:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->displayLogEntries(Ljava/util/List;)V

    .line 691
    return-void

    .line 684
    :pswitch_0
    sget-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_ALL:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_viewBy:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    goto :goto_0

    .line 687
    :pswitch_1
    sget-object v0, Lcom/sgiggle/production/CallLogActivity$ViewBySelection;->VIEW_BY_MISSED:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_viewBy:Lcom/sgiggle/production/CallLogActivity$ViewBySelection;

    goto :goto_0

    .line 682
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a003c
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter

    .prologue
    .line 520
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onContextItemSelected()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    const/4 v0, 0x1

    .line 522
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 527
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    .line 529
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 530
    return v0

    .line 524
    :pswitch_0
    iget-object v1, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/CallLogActivity;->deleteCallLog(Lcom/sgiggle/production/CallLogActivity$CallLogEntry;)V

    goto :goto_0

    .line 522
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 177
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 179
    const v0, 0x7f03000c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->setContentView(I)V

    .line 181
    const-string v0, "Tango.CallLogUI"

    invoke-virtual {p0, v0, v2}, Lcom/sgiggle/production/CallLogActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_prefs:Landroid/content/SharedPreferences;

    .line 183
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_progressView:Landroid/view/ViewGroup;

    .line 185
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    .line 186
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listWrapper:Landroid/view/View;

    .line 187
    const v0, 0x7f0a003f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_emptyView:Landroid/widget/TextView;

    .line 188
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 189
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setItemsCanFocus(Z)V

    .line 191
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setClickable(Z)V

    .line 193
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 196
    const v0, 0x7f0a003b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 197
    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 198
    const v1, 0x7f0a003c

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 199
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 510
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onCreateContextMenu()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    if-eqz v0, :cond_0

    .line 512
    invoke-super {p0, p1, p2, p3}, Lcom/sgiggle/production/ActivityBase;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 513
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->displayName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 514
    const/4 v0, 0x7

    const v1, 0x7f090084

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 516
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 535
    const/4 v0, 0x4

    const v1, 0x7f090082

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 537
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 499
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->unregisterForContextMenu(Landroid/view/View;)V

    .line 501
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 505
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_isDestroyed:Z

    .line 506
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 463
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 464
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 465
    const-string v1, "Tango.CallLogUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick(): Call to ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->displayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;->m_accountId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    invoke-direct {p0, v0}, Lcom/sgiggle/production/CallLogActivity;->showContactDetailPage(Lcom/sgiggle/production/CallLogActivity$CallLogEntry;)V

    .line 467
    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    .line 471
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onItemLongClick()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 473
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_longClickedEntry:Lcom/sgiggle/production/CallLogActivity$CallLogEntry;

    .line 474
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 652
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 653
    const-string v0, "Tango.CallLogUI"

    const-string v1, "ignore back key down"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    const/4 v0, 0x0

    .line 662
    :goto_0
    return v0

    .line 656
    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 657
    const-string v0, "Tango.CallLogUI"

    const-string v1, "open options menu"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity;->openOptionsMenu()V

    .line 659
    const/4 v0, 0x1

    goto :goto_0

    .line 661
    :cond_1
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key down "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handled by super class"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 667
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 668
    const-string v0, "Tango.CallLogUI"

    const-string v1, "ignore back key up"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    const/4 v0, 0x0

    .line 676
    :goto_0
    return v0

    .line 671
    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 673
    const/4 v0, 0x1

    goto :goto_0

    .line 675
    :cond_1
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "key up "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handled by super class"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 542
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 547
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 544
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->onDeleteAllMenuSelected()V

    .line 545
    const/4 v0, 0x1

    goto :goto_0

    .line 542
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 491
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 494
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->saveScrollPositionInList()V

    .line 495
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 479
    const-string v0, "Tango.CallLogUI"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 482
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->getDateFormatString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_dateFormat:Ljava/lang/String;

    .line 484
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_displayedEntries:Ljava/util/List;

    sget-object v1, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    if-eq v0, v1, :cond_0

    .line 485
    invoke-virtual {p0}, Lcom/sgiggle/production/CallLogActivity;->updateLogEntries()V

    .line 487
    :cond_0
    return-void
.end method

.method updateLogEntries()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 206
    const-string v0, "Tango.CallLogUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateLogEntries(): # of entries = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/CallLogActivity;->m_logEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    sget-boolean v0, Lcom/sgiggle/production/CallLogActivity;->m_receivedEntriesFromServer:Z

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 210
    invoke-direct {p0}, Lcom/sgiggle/production/CallLogActivity;->doUpdateLogEntries()V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sgiggle/production/CallLogActivity;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x3a98

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
