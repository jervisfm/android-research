.class public final enum Lcom/sgiggle/production/CallSession$CallState;
.super Ljava/lang/Enum;
.source "CallSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CallSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CallState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/CallSession$CallState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_CONNECTING:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_DISCONNECTING:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_INCOMING:Lcom/sgiggle/production/CallSession$CallState;

.field public static final enum CALL_STATE_UNDEFINED:Lcom/sgiggle/production/CallSession$CallState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_UNDEFINED"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_UNDEFINED:Lcom/sgiggle/production/CallSession$CallState;

    .line 26
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_INCOMING"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_INCOMING:Lcom/sgiggle/production/CallSession$CallState;

    .line 27
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_DIALING"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    .line 28
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_CONNECTING"

    invoke-direct {v0, v1, v6}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_CONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    .line 29
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_ACTIVE"

    invoke-direct {v0, v1, v7}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    .line 30
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_DISCONNECTING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    .line 31
    new-instance v0, Lcom/sgiggle/production/CallSession$CallState;

    const-string v1, "CALL_STATE_DISCONNECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/CallSession$CallState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    .line 23
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_UNDEFINED:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_INCOMING:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_CONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/production/CallSession$CallState;->$VALUES:[Lcom/sgiggle/production/CallSession$CallState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/CallSession$CallState;
    .locals 1
    .parameter

    .prologue
    .line 23
    const-class v0, Lcom/sgiggle/production/CallSession$CallState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallSession$CallState;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/CallSession$CallState;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/sgiggle/production/CallSession$CallState;->$VALUES:[Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v0}, [Lcom/sgiggle/production/CallSession$CallState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/CallSession$CallState;

    return-object v0
.end method
