.class public Lcom/sgiggle/production/CallSession;
.super Ljava/lang/Object;
.source "CallSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CallSession$CallState;
    }
.end annotation


# instance fields
.field private mEmptySlotCount:I

.field private mVGoodData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field

.field public m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

.field public m_callOnHold:Z

.field public m_callStartTime:J

.field public m_callState:Lcom/sgiggle/production/CallSession$CallState;

.field public m_callerInitVideoCall:Z

.field public m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

.field public m_inCallAlertText:Ljava/lang/String;

.field public m_localName:Ljava/lang/String;

.field public m_muted:Z

.field private m_peerAccountId:Ljava/lang/String;

.field public m_peerContactId:J

.field public m_peerName:Ljava/lang/String;

.field private m_pipSwapped:Z

.field public m_showAnimation:Z

.field public m_showInCallAlert:Z

.field public m_showLowBandwidth:Z

.field public m_speakerOn:Z

.field public m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;Lcom/sgiggle/production/CallSession$CallState;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_UNDEFINED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_peerContactId:J

    .line 42
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    .line 45
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    .line 46
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_muted:Z

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_callStartTime:J

    .line 49
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 52
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 53
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 54
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 55
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showLowBandwidth:Z

    .line 57
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showInCallAlert:Z

    .line 60
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showAnimation:Z

    .line 62
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->mVGoodData:Ljava/util/List;

    .line 64
    iput v2, p0, Lcom/sgiggle/production/CallSession;->mEmptySlotCount:I

    .line 72
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_peerAccountId:Ljava/lang/String;

    .line 73
    iput-object p2, p0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 74
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasLocalDisplayname()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getLocalDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_localName:Ljava/lang/String;

    .line 78
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->hasDeviceContactId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 79
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDeviceContactId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_peerContactId:J

    .line 81
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/sgiggle/production/CallSession$CallState;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_UNDEFINED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 39
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_peerContactId:J

    .line 42
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    .line 45
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    .line 46
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_muted:Z

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_callStartTime:J

    .line 49
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 52
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 53
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 54
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 55
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showLowBandwidth:Z

    .line 57
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showInCallAlert:Z

    .line 60
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_showAnimation:Z

    .line 62
    iput-boolean v2, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->mVGoodData:Ljava/util/List;

    .line 64
    iput v2, p0, Lcom/sgiggle/production/CallSession;->mEmptySlotCount:I

    .line 67
    iput-object p1, p0, Lcom/sgiggle/production/CallSession;->m_peerAccountId:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 69
    return-void
.end method


# virtual methods
.method public getEmptySlotCount()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/sgiggle/production/CallSession;->mEmptySlotCount:I

    return v0
.end method

.method public getPeerAccountId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_peerAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public getPeerPhoto()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 97
    iget-wide v1, p0, Lcom/sgiggle/production/CallSession;->m_peerContactId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 98
    iget-wide v0, p0, Lcom/sgiggle/production/CallSession;->m_peerContactId:J

    invoke-static {v0, v1}, Lcom/sgiggle/contacts/ContactStore;->getPhotoByContactId(J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 100
    :cond_0
    return-object v0
.end method

.method public getVGoodData()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->mVGoodData:Ljava/util/List;

    return-object v0
.end method

.method public isPipSwapped()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    return v0
.end method

.method public pipSwappable()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetPipSwapped()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    .line 130
    return-void
.end method

.method public setEmptySlotCount(I)V
    .locals 0
    .parameter

    .prologue
    .line 157
    iput p1, p0, Lcom/sgiggle/production/CallSession;->mEmptySlotCount:I

    .line 158
    return-void
.end method

.method public setInCallAlertEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 104
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getHide()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    iput-boolean v0, p0, Lcom/sgiggle/production/CallSession;->m_showInCallAlert:Z

    .line 105
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    move-result-object v0

    .line 106
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 108
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->SWITCH_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    if-ne v0, v2, :cond_2

    .line 109
    const v0, 0x7f090059

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_inCallAlertText:Ljava/lang/String;

    .line 118
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v3

    .line 104
    goto :goto_0

    .line 111
    :cond_2
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    if-ne v0, v2, :cond_3

    .line 112
    const v0, 0x7f09007c

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_inCallAlertText:Ljava/lang/String;

    goto :goto_1

    .line 114
    :cond_3
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->PASS_THROUGH:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InCallAlertEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->m_inCallAlertText:Ljava/lang/String;

    goto :goto_1
.end method

.method public setPipSwapped(Z)V
    .locals 0
    .parameter

    .prologue
    .line 125
    iput-boolean p1, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    .line 126
    return-void
.end method

.method public setVGoodData(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sgiggle/production/CallSession;->mVGoodData:Ljava/util/List;

    .line 150
    return-void
.end method

.method public swapPip()V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    iget-boolean v0, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sgiggle/production/CallSession;->m_pipSwapped:Z

    .line 135
    :cond_0
    return-void

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updatePeerAccountId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sgiggle/production/CallSession;->m_peerAccountId:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public updateVgoodSelectorData(Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;)V
    .locals 2
    .parameter

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodBundleList()Ljava/util/List;

    move-result-object v0

    .line 162
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 163
    iput-object v0, p0, Lcom/sgiggle/production/CallSession;->mVGoodData:Ljava/util/List;

    .line 165
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getEmptySlotCount()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/CallSession;->mEmptySlotCount:I

    .line 166
    return-void
.end method
