.class public Lcom/sgiggle/production/ContactDetailActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "ContactDetailActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ContactDetail"


# instance fields
.field private callDuration:Landroid/widget/TextView;

.field private callInfo:Landroid/widget/TextView;

.field private callTime:Landroid/widget/TextView;

.field private conversationButton:Landroid/widget/Button;

.field private favoriteButton:Landroid/widget/Button;

.field private inviteButton:Landroid/widget/Button;

.field private latestEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

.field private mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

.field private m_dateFormat:Ljava/lang/String;

.field private m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

.field private userImage:Landroid/widget/ImageView;

.field private userName:Landroid/widget/TextView;

.field private videoButton:Landroid/widget/Button;

.field private voiceButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_dateFormat:Ljava/lang/String;

    return-void
.end method

.method private formatDuration(I)Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 324
    .line 328
    div-int/lit16 v0, p1, 0xe10

    .line 329
    rem-int/lit16 v1, p1, 0xe10

    div-int/lit8 v1, v1, 0x3c

    .line 330
    rem-int/lit8 v2, p1, 0x3c

    .line 332
    const-string v3, "%02d"

    .line 334
    if-nez v0, :cond_0

    .line 335
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private formatWhen(J)Ljava/lang/String;
    .locals 9
    .parameter

    .prologue
    const-wide/16 v7, 0x3e8

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 299
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 300
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 302
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    .line 303
    mul-long v2, p1, v7

    invoke-virtual {v1, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 306
    invoke-virtual {v1, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1, v5}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v5}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/ContactDetailActivity;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 312
    :cond_0
    const-wide/32 v2, 0x15180

    add-long/2addr v2, p1

    mul-long/2addr v2, v7

    invoke-virtual {v1, v2, v3}, Ljava/util/GregorianCalendar;->setTimeInMillis(J)V

    .line 313
    invoke-virtual {v1, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v4}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_1

    invoke-virtual {v1, v5}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v5}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_1

    invoke-virtual {v1, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    if-ne v1, v0, :cond_1

    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/ContactDetailActivity;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 320
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/ContactDetailActivity;->localeDateString(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDateFormatString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 342
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v0

    .line 343
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move v2, v4

    .line 344
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 345
    aget-char v3, v0, v2

    sparse-switch v3, :sswitch_data_0

    .line 344
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 347
    :sswitch_0
    const-string v3, "dd/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 351
    :sswitch_1
    const-string v3, "MM/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 355
    :sswitch_2
    const-string v3, "yy/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 360
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    const/4 v2, 0x1

    sub-int/2addr v0, v2

    invoke-virtual {v1, v4, v0}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 345
    nop

    :sswitch_data_0
    .sparse-switch
        0x4d -> :sswitch_1
        0x64 -> :sswitch_0
        0x79 -> :sswitch_2
    .end sparse-switch
.end method

.method private localeDateString(JLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter

    .prologue
    const-wide/16 v2, 0x3e8

    .line 288
    if-eqz p3, :cond_0

    .line 289
    const v0, 0x80001

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    mul-long/2addr v2, p1

    invoke-static {p0, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 294
    :goto_0
    return-object v0

    .line 293
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_dateFormat:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 294
    new-instance v1, Ljava/util/Date;

    mul-long/2addr v2, p1

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 4
    .parameter

    .prologue
    .line 257
    const-string v0, "Tango.ContactDetail"

    const-string v1, "contact detail show invite"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 259
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    iget-wide v2, p1, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    .line 261
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;-><init>(Ljava/util/List;)V

    .line 264
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 265
    return-void
.end method

.method private updateLayoutFromEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f0a0063

    const v7, 0x7f0a0062

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 122
    if-nez p1, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/Utils$UIContact;->convertFromMessageContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/production/Utils$UIContact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-object v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-object v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    .line 129
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 130
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 135
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->userName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v1}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-wide v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-boolean v0, v0, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    if-eqz v0, :cond_8

    .line 139
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 140
    if-eqz v0, :cond_8

    .line 142
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->userImage:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v0, v6

    .line 145
    :goto_2
    if-nez v0, :cond_2

    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->userImage:Landroid/widget/ImageView;

    const v1, 0x7f020090

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasEntry()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 149
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callInfo:Landroid/widget/TextView;

    const v1, 0x7f09011e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 151
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->callDuration:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDuration()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->formatDuration(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->callTime:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getStartTime()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/sgiggle/production/ContactDetailActivity;->formatWhen(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :goto_3
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_RECENT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_MESSAGES_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    if-ne v0, v1, :cond_6

    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-wide v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 166
    invoke-virtual {p0, v8}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 167
    invoke-virtual {p0, v7}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :goto_4
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->checkFavoriteStat()V

    .line 175
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasAccountValidated()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getAccountValidated()Z

    move-result v0

    .line 176
    :goto_5
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->conversationButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    .line 132
    :cond_4
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 133
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 155
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callInfo:Landroid/widget/TextView;

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callDuration:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callTime:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 169
    :cond_6
    invoke-virtual {p0, v8}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 170
    invoke-virtual {p0, v7}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    :cond_7
    move v0, v6

    .line 175
    goto :goto_5

    :cond_8
    move v0, v4

    goto/16 :goto_2
.end method


# virtual methods
.method protected checkFavoriteStat()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-boolean v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    const v1, 0x7f02007f

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 277
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900f5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 274
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    const v1, 0x7f020080

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_0
.end method

.method protected handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 230
    const-string v0, "Tango.ContactDetail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    if-nez p1, :cond_0

    .line 233
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 254
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 251
    const-string v0, "Tango.ContactDetail"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Unsupported message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;

    .line 240
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    .line 241
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/InviteDialogCreater;->doInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)V

    goto :goto_0

    .line 245
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    .line 246
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactDetailActivity;->updateLayoutFromEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V

    .line 247
    iput-object p1, p0, Lcom/sgiggle/production/ContactDetailActivity;->latestEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    goto :goto_0

    .line 237
    :sswitch_data_0
    .sparse-switch
        0x8913 -> :sswitch_0
        0x8990 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 281
    const-string v0, "Tango.ContactDetail"

    const-string v1, "contact detail on back key"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelContactDetailMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelContactDetailMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 284
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    if-nez v0, :cond_0

    .line 221
    :goto_0
    return-void

    .line 183
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 184
    const-string v1, "Tango.ContactDetail"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick(View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 195
    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 196
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-object v2, v2, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    .line 197
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-boolean v1, v1, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    if-eqz v1, :cond_1

    .line 199
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveFavoriteContactMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveFavoriteContactMessage;-><init>(Ljava/util/List;)V

    .line 201
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 202
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    .line 209
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->checkFavoriteStat()V

    goto :goto_0

    .line 187
    :pswitch_2
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-object v1, v1, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v2}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-wide v3, v3, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    goto :goto_0

    .line 191
    :pswitch_3
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-object v1, v1, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v2}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-wide v3, v3, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    goto/16 :goto_0

    .line 204
    :cond_1
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$AddFavoriteContactMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddFavoriteContactMessage;-><init>(Ljava/util/List;)V

    .line 206
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 207
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    goto :goto_1

    .line 212
    :pswitch_4
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V

    goto/16 :goto_0

    .line 216
    :pswitch_5
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;

    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v1}, Lcom/sgiggle/production/Utils$UIContact;->convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    .line 218
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x7f0a0063
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 54
    const-string v0, "Tango.ContactDetail"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 56
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->setContentView(I)V

    .line 57
    new-instance v0, Lcom/sgiggle/production/InviteDialogCreater;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getParent()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/InviteDialogCreater;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

    .line 59
    const v0, 0x7f0a005c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->userName:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0a005b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->userImage:Landroid/widget/ImageView;

    .line 61
    const v0, 0x7f0a0067

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->videoButton:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0a0068

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->voiceButton:Landroid/widget/Button;

    .line 63
    const v0, 0x7f0a0063

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    .line 65
    const v0, 0x7f0a006d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->inviteButton:Landroid/widget/Button;

    .line 66
    const v0, 0x7f0a006a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->conversationButton:Landroid/widget/Button;

    .line 68
    const v0, 0x7f0a0061

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callDuration:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a0060

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callTime:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0a005e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->callInfo:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->videoButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->voiceButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->favoriteButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->inviteButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->conversationButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    invoke-direct {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getDateFormatString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_dateFormat:Ljava/lang/String;

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    const v0, 0x7f0900f8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 85
    const-string v2, "<b>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</b>&nbsp;"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    :cond_0
    const v0, 0x7f0900f9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const v0, 0x7f0a006f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 90
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 93
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 97
    invoke-direct {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getDateFormatString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_dateFormat:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x8990

    if-eq v0, v1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    if-eqz v0, :cond_3

    .line 106
    const/4 v0, 0x1

    .line 107
    iget-object v1, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iget-boolean v1, v1, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    move v2, v0

    .line 109
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->latestEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->latestEvent:Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->updateLayoutFromEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V

    .line 115
    :goto_2
    if-eqz v2, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sgiggle/production/ContactDetailActivity;->m_uiContact:Lcom/sgiggle/production/Utils$UIContact;

    iput-boolean v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    .line 117
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->checkFavoriteStat()V

    goto :goto_0

    .line 112
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    .line 113
    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactDetailActivity;->updateLayoutFromEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method
