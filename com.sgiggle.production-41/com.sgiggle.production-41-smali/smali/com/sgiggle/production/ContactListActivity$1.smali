.class Lcom/sgiggle/production/ContactListActivity$1;
.super Landroid/os/Handler;
.source "ContactListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactListActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 302
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$1;->this$0:Lcom/sgiggle/production/ContactListActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$1;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_isDestroyed:Z
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$100(Lcom/sgiggle/production/ContactListActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler: ignoring message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; we\'re destroyed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    :goto_0
    return-void

    .line 310
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 312
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$1;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #calls: Lcom/sgiggle/production/ContactListActivity;->doUpdateContacts()V
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$200(Lcom/sgiggle/production/ContactListActivity;)V

    goto :goto_0

    .line 316
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$1;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #calls: Lcom/sgiggle/production/ContactListActivity;->performAccountSync()V
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$300(Lcom/sgiggle/production/ContactListActivity;)V

    goto :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
