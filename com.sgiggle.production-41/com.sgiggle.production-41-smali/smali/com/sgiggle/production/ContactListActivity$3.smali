.class Lcom/sgiggle/production/ContactListActivity$3;
.super Ljava/lang/Object;
.source "ContactListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ContactListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactListActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 381
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick(View "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$400(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/production/ListViewIgnoreBackKey;

    move-result-object v1

    monitor-enter v1

    .line 384
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$500(Lcom/sgiggle/production/ContactListActivity;)Landroid/widget/TextView;

    move-result-object v0

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$600(Lcom/sgiggle/production/ContactListActivity;)Landroid/view/View;

    move-result-object v0

    if-ne p2, v0, :cond_1

    .line 385
    :cond_0
    const-string v0, "Tango.ContactsActivity"

    const-string v2, "onItemClick: header or footer was clicked, which should not happen!"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    monitor-exit v1

    .line 398
    :goto_0
    return-void

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$700(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;
    invoke-static {v2}, Lcom/sgiggle/production/ContactListActivity;->access$400(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/production/ListViewIgnoreBackKey;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p3, v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 390
    if-eqz v0, :cond_2

    .line 391
    iget-object v2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 392
    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #calls: Lcom/sgiggle/production/ContactListActivity;->showContactDetailPage(Lcom/sgiggle/production/Utils$UIContact;)V
    invoke-static {v2, v0}, Lcom/sgiggle/production/ContactListActivity;->access$800(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/production/Utils$UIContact;)V

    .line 397
    :cond_2
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 394
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity$3;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #calls: Lcom/sgiggle/production/ContactListActivity;->requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V
    invoke-static {v2, v0}, Lcom/sgiggle/production/ContactListActivity;->access$000(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/production/Utils$UIContact;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
