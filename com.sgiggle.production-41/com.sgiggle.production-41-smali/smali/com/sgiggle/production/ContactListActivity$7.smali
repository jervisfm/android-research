.class Lcom/sgiggle/production/ContactListActivity$7;
.super Ljava/lang/Object;
.source "ContactListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ContactListActivity;->displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactListActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1031
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$7;->this$0:Lcom/sgiggle/production/ContactListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$7;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I
    invoke-static {v0}, Lcom/sgiggle/production/ContactListActivity;->access$1300(Lcom/sgiggle/production/ContactListActivity;)I

    move-result v0

    if-lez v0, :cond_0

    .line 1035
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity$7;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #getter for: Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v1}, Lcom/sgiggle/production/ContactListActivity;->access$1400(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 1036
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1037
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$7;->this$0:Lcom/sgiggle/production/ContactListActivity;

    const/4 v1, 0x0

    #setter for: Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactListActivity;->access$1302(Lcom/sgiggle/production/ContactListActivity;I)I

    .line 1038
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$7;->this$0:Lcom/sgiggle/production/ContactListActivity;

    const/4 v1, 0x0

    #setter for: Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactListActivity;->access$1402(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 1040
    :cond_0
    return-void
.end method
