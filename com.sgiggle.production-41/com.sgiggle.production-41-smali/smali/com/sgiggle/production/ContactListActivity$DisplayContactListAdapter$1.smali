.class Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$1;
.super Ljava/lang/Object;
.source "ContactListActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$1;->this$1:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter

    .prologue
    .line 212
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    move-object v4, v0

    .line 213
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "button clicked for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    iget-object v1, v4, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 215
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    iget-object v2, v4, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, v4, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    sget-object v6, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v1 .. v6}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    .line 220
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$1;->this$1:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    iget-object v1, v1, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->this$0:Lcom/sgiggle/production/ContactListActivity;

    #calls: Lcom/sgiggle/production/ContactListActivity;->requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V
    invoke-static {v1, v4}, Lcom/sgiggle/production/ContactListActivity;->access$000(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/production/Utils$UIContact;)V

    goto :goto_0
.end method
