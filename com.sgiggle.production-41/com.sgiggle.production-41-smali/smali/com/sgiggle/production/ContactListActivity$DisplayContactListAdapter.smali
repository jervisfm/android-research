.class Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;
.super Lcom/sgiggle/production/adapter/ContactListAdapter;
.source "ContactListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DisplayContactListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field protected final m_textViewResourceId:I

.field final synthetic this$0:Lcom/sgiggle/production/ContactListActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/ContactListActivity;ILcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->this$0:Lcom/sgiggle/production/ContactListActivity;

    .line 157
    invoke-direct {p0, p3, p4, p5}, Lcom/sgiggle/production/adapter/ContactListAdapter;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;)V

    .line 158
    iput p2, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->m_textViewResourceId:I

    .line 159
    return-void
.end method


# virtual methods
.method protected fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const v6, 0x7f020090

    const/4 v5, 0x0

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;

    .line 175
    iget-object v1, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    iget-object v1, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIContact;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 178
    iget-wide v1, p2, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 179
    iget-boolean v1, p2, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    if-eqz v1, :cond_2

    .line 180
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 182
    if-eqz v1, :cond_1

    .line 183
    iget-object v2, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 195
    :cond_0
    :goto_0
    const v1, 0x7f0a007e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 196
    iget-object v2, p2, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 198
    const v2, 0x7f0200d6

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 200
    iget-object v0, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 206
    :goto_1
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setFocusable(Z)V

    .line 207
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    .line 208
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 209
    new-instance v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$1;-><init>(Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    return-void

    .line 186
    :cond_1
    iget-object v1, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 187
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v1

    iget-object v2, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v1, p2, v2}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 191
    :cond_2
    iget-object v1, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 202
    :cond_3
    const v2, 0x7f0200e6

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 204
    iget-object v0, v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1
.end method

.method protected getContactView(Lcom/sgiggle/production/Utils$UIContact;)Landroid/view/View;
    .locals 3
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->m_textViewResourceId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 164
    new-instance v2, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;-><init>(Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;)V

    .line 165
    const v0, 0x7f0a007b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->image:Landroid/widget/ImageView;

    .line 166
    const v0, 0x7f0a007c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 167
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 168
    return-object v1
.end method
