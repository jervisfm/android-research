.class Lcom/sgiggle/production/ContactListActivity$Section;
.super Ljava/lang/Object;
.source "ContactListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Section"
.end annotation


# instance fields
.field private m_count:I

.field private m_displayLetter:Ljava/lang/Character;

.field private m_headerIndex:I

.field private final m_idLetter:Ljava/lang/Character;

.field private m_startIndex:I

.field final synthetic this$0:Lcom/sgiggle/production/ContactListActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/ContactListActivity;Ljava/lang/Character;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 690
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->this$0:Lcom/sgiggle/production/ContactListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 686
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    .line 687
    iput v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    .line 688
    iput v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    .line 691
    iput-object p2, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    .line 692
    iput-object p2, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    .line 693
    return-void
.end method


# virtual methods
.method public copyDisplayContentFrom(Lcom/sgiggle/production/ContactListActivity$Section;)V
    .locals 3
    .parameter

    .prologue
    .line 703
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Section::copyDisplayContentFrom(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " <= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    iget-object v0, p1, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    .line 705
    iget v0, p1, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    .line 706
    iget v0, p1, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    .line 707
    iget v0, p1, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    .line 708
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 711
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    return v0
.end method

.method public getHeaderIndex()I
    .locals 1

    .prologue
    .line 734
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    return v0
.end method

.method public getIdLetter()C
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 731
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    return v0
.end method

.method public increment()V
    .locals 1

    .prologue
    .line 714
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    .line 715
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 696
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    .line 697
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    .line 698
    iput v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    .line 699
    iput v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    .line 700
    return-void
.end method

.method public setHeaderIndex(I)V
    .locals 0
    .parameter

    .prologue
    .line 733
    iput p1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .parameter

    .prologue
    .line 730
    iput p1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 723
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_idLetter:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": display="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_count:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_startIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", headerIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_headerIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity$Section;->m_displayLetter:Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
