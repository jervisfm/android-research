.class public Lcom/sgiggle/production/ContactListActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "ContactListActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ContactListActivity$Section;,
        Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;
    }
.end annotation


# static fields
.field private static final ADD_CONTACT_ID:I = 0x6

.field private static final ALL_TOGGLE_AVAILABLE:Z = false

.field private static final DEBUG_EMAIL_PHONE_SELECTION:Z = false

.field private static final DEFAULT_CONTACT_LIST_SELECTION:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection; = null

.field private static final DIALOG_REFRESH_CONTACT:I = 0x0

.field private static final PERFORM_ACCOUNT_SYNC:I = 0x2

.field private static final PERFORM_ACCOUNT_SYNC_DELAY:I = 0x1388

.field private static final PREF_VIEW_ALL_INDEX:Ljava/lang/String; = "view_all_index"

.field private static final PREF_VIEW_ALL_TOP:Ljava/lang/String; = "view_all_top"

.field private static final PREF_VIEW_TANGO_INDEX:Ljava/lang/String; = "view_index"

.field private static final PREF_VIEW_TANGO_TOP:Ljava/lang/String; = "view_top"

.field private static final REFRESH_CONTACTS_ID:I = 0x4

.field private static final REQUEST_CODE_ADD_CONTACT:I = 0x0

.field private static final SEARCH_ID:I = 0x5

.field private static final SEARCH_INPUT_KEY_ROTATION:Ljava/lang/String; = "*ffc"

.field private static final SHOW_CONTACTS_FIRST_TIME:I = 0x1

.field private static final SHOW_CONTACTS_FIRST_TIME_DELAY:I = 0x3a98

.field private static final TAG:Ljava/lang/String; = "Tango.ContactsActivity"

.field private static final USER_GROUP_SELECTION:Ljava/lang/String; = "user_grp_selection"

.field private static final VDBG:Z = true

.field private static m_allContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private static m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

.field private static m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;


# instance fields
.field private mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

.field private m_Inflater:Landroid/view/LayoutInflater;

.field private m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

.field private m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

.field private m_alertCount:I

.field private m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

.field private m_contacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_emptyInviteButton:Landroid/widget/Button;

.field private m_emptyTangoListText:Ljava/lang/String;

.field private m_emptyView:Landroid/view/View;

.field private m_emptyViewText:Landroid/widget/TextView;

.field private m_footerView:Landroid/view/View;

.field private m_footerViewCount:Landroid/widget/TextView;

.field private m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field private m_handler:Landroid/os/Handler;

.field private m_hasRefreshProgressDlg:Z

.field private m_header:Landroid/widget/TextView;

.field private m_isDestroyed:Z

.field private m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

.field private m_onTouch:Z

.field private m_prefs:Landroid/content/SharedPreferences;

.field private m_query:Ljava/lang/String;

.field private m_scrollState:I

.field private m_searchTitleView:Landroid/widget/TextView;

.field private m_syncAccountPending:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->DEFAULT_CONTACT_LIST_SELECTION:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    .line 94
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 95
    invoke-static {}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 90
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_syncAccountPending:Z

    .line 125
    iput-boolean v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_onTouch:Z

    .line 126
    iput v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_scrollState:I

    .line 296
    iput-boolean v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_isDestroyed:Z

    .line 302
    new-instance v0, Lcom/sgiggle/production/ContactListActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ContactListActivity$1;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_handler:Landroid/os/Handler;

    .line 682
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/ContactListActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_isDestroyed:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sgiggle/production/ContactListActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lcom/sgiggle/production/ContactListActivity;->m_scrollState:I

    return p1
.end method

.method static synthetic access$1102(Lcom/sgiggle/production/ContactListActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput-boolean p1, p0, Lcom/sgiggle/production/ContactListActivity;->m_onTouch:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sgiggle/production/ContactListActivity;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sgiggle/production/ContactListActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 74
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sgiggle/production/ContactListActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->doUpdateContacts()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->performAccountSync()V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/production/ListViewIgnoreBackKey;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/ContactListActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/ContactListActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/ContactListActivity;)Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sgiggle/production/ContactListActivity;Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->showContactDetailPage(Lcom/sgiggle/production/Utils$UIContact;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->saveScrollPositionInList()V

    return-void
.end method

.method private createOrSyncContactsWithDevice()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 809
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 810
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 812
    array-length v0, v0

    if-lez v0, :cond_0

    .line 813
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "Tango sync account has been created. Sync contacts instead..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->performAccountSync()V

    .line 850
    :goto_0
    return-void

    .line 818
    :cond_0
    const-string v0, "Tango.ContactsActivity"

    const-string v3, "Create Tango sync account..."

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 819
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 821
    new-instance v6, Lcom/sgiggle/production/ContactListActivity$6;

    invoke-direct {v6, p0}, Lcom/sgiggle/production/ContactListActivity$6;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, p0

    move-object v7, v2

    .line 844
    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method private displayContacts(Ljava/util/List;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 561
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "displayContacts(): New list-size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    iget-object v6, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    monitor-enter v6

    .line 565
    :try_start_0
    new-instance v0, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    const v2, 0x7f030019

    sget-object v3, Lcom/sgiggle/production/ContactListActivity;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    sget-object v4, Lcom/sgiggle/production/ContactListActivity;->DEFAULT_CONTACT_LIST_SELECTION:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iget-object v5, p0, Lcom/sgiggle/production/ContactListActivity;->m_Inflater:Landroid/view/LayoutInflater;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;-><init>(Lcom/sgiggle/production/ContactListActivity;ILcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    .line 566
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->loadGroups(Ljava/util/List;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;)V

    .line 567
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->notifyDataSetChanged()V

    .line 575
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;->getContactCount()I

    move-result v0

    .line 576
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->removeFooterView(Landroid/view/View;)Z

    .line 577
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->removeHeaderView(Landroid/view/View;)Z

    .line 578
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 580
    if-lez v0, :cond_0

    .line 581
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v2, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v1, v2, :cond_1

    .line 582
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerViewCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0004

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 598
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setFastScrollEnabled(Z)V

    .line 599
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_adapter:Lcom/sgiggle/production/ContactListActivity$DisplayContactListAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 600
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setFastScrollEnabled(Z)V

    .line 601
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->forceOnSizeChanged()V

    .line 602
    monitor-exit v6

    .line 603
    return-void

    .line 585
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerViewCount:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0003

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 602
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 591
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0002

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 592
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private doUpdateContacts()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 461
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "doUpdateContacts()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    iget-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_hasRefreshProgressDlg:Z

    if-eqz v0, :cond_0

    .line 464
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/Activity;->dismissDialog(I)V

    .line 465
    iput-boolean v3, p0, Lcom/sgiggle/production/ContactListActivity;->m_hasRefreshProgressDlg:Z

    .line 468
    :cond_0
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    .line 470
    if-eqz v0, :cond_1

    .line 471
    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contacts:Ljava/util/List;

    .line 473
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_searchTitleView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 474
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v3}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 475
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView()V

    .line 476
    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->displayContacts(Ljava/util/List;)V

    .line 477
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->restoreScrollPositionInList()V

    .line 480
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_syncAccountPending:Z

    if-eqz v0, :cond_2

    .line 481
    iput-boolean v3, p0, Lcom/sgiggle/production/ContactListActivity;->m_syncAccountPending:Z

    .line 482
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->createOrSyncContactsWithDevice()V

    .line 484
    :cond_2
    return-void
.end method

.method static findContact(Ljava/lang/String;)Lcom/sgiggle/production/Utils$UIContact;
    .locals 3
    .parameter

    .prologue
    .line 286
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 287
    iget-object v2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getGroupSelectionType()I
    .locals 3

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "user_grp_selection"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getTangoContacts()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    if-eq v0, v1, :cond_0

    .line 269
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "getTangoContacts: Tango contacts not yet retrieved from Tango server."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    const/4 v0, 0x0

    .line 279
    :goto_0
    return-object v0

    .line 273
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 274
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 275
    iget-object v3, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 276
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 279
    goto :goto_0
.end method

.method private handleSearch(Ljava/lang/String;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 653
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSearch: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    .line 656
    if-nez v0, :cond_0

    .line 657
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "handleSearch: No contacts. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    :goto_0
    return-void

    .line 660
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 662
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 664
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 665
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 667
    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    .line 668
    invoke-virtual {v4, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    .line 669
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 673
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_searchTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 674
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 675
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_searchTitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090055

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v7, [Ljava/lang/Object;

    aput-object p1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 677
    invoke-direct {p0, v7}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView(Z)V

    .line 678
    invoke-direct {p0, v1}, Lcom/sgiggle/production/ContactListActivity;->displayContacts(Ljava/util/List;)V

    goto :goto_0
.end method

.method private hideTangoAlerts()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1059
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    .line 1060
    iput-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 1061
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/ContactListActivity;->displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V

    .line 1062
    return-void
.end method

.method private openContactDetailPage(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 555
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "open the contact detail page when receiving the event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ActivityStack;

    .line 557
    const-class v1, Lcom/sgiggle/production/ContactDetailActivity;

    invoke-virtual {v0, v1, p1}, Lcom/sgiggle/production/ActivityStack;->pushWithMessage(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    .line 558
    return-void
.end method

.method private performAccountSync()V
    .locals 3

    .prologue
    .line 789
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "performAccountSync() ..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    const v0, 0x7f090087

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 791
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 792
    array-length v1, v0

    if-lez v1, :cond_0

    .line 793
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 794
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 795
    const-string v2, "com.android.contacts"

    invoke-static {v0, v2, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 797
    :cond_0
    return-void
.end method

.method private postRequestContactFilter()V
    .locals 3

    .prologue
    .line 866
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_syncAccountPending:Z

    .line 867
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFilterContactMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFilterContactMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 869
    return-void
.end method

.method private requestInvite2Tango(Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 4
    .parameter

    .prologue
    .line 742
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    iget-wide v2, p1, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    .line 744
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 745
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteContactMessage;-><init>(Ljava/util/List;)V

    .line 748
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 750
    return-void
.end method

.method private restoreGroupSelectionType()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 989
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "user_grp_selection"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 990
    if-eq v0, v2, :cond_0

    .line 991
    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 992
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 1001
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v0, v1, :cond_1

    .line 1002
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "restoreGroupSelectionType: ALL is not available, forcing to TANGO"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 1004
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->saveUserGroupSelectionType()V

    .line 1006
    :cond_1
    return-void

    .line 993
    :cond_2
    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 994
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    goto :goto_0

    .line 996
    :cond_3
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    goto :goto_0
.end method

.method private restoreScrollPositionInList()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 958
    iget-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_onTouch:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_scrollState:I

    if-eqz v0, :cond_2

    .line 959
    :cond_0
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "restoreScrollPositionInList/skip"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    :cond_1
    :goto_0
    return-void

    .line 965
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->getGroupSelectionType()I

    move-result v0

    .line 971
    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 972
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "view_index"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 973
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v2, "view_top"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 974
    const-string v2, "Tango.ContactsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "restoreScrollPositionInList/tango: index="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", top="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v1

    move v1, v0

    move v0, v6

    .line 977
    :goto_1
    if-eq v1, v5, :cond_1

    .line 978
    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v2, v1, v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_3
    move v0, v3

    move v1, v5

    goto :goto_1
.end method

.method private saveScrollPositionInList()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 939
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->getFirstVisiblePosition()I

    move-result v0

    .line 940
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 941
    if-nez v1, :cond_1

    move v1, v2

    .line 942
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 943
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->getGroupSelectionType()I

    move-result v3

    .line 944
    sget-object v4, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v4}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    .line 945
    const-string v3, "view_all_index"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 946
    const-string v3, "view_all_top"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 947
    const-string v3, "Tango.ContactsActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveScrollPositionInList/all: index="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", top="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    :cond_0
    :goto_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 955
    return-void

    .line 941
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    goto :goto_0

    .line 949
    :cond_2
    sget-object v4, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v4}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 950
    const-string v3, "view_index"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 951
    const-string v3, "view_top"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 952
    const-string v3, "Tango.ContactsActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "saveScrollPositionInList/tango: index="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", top="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private saveUserGroupSelectionType()V
    .locals 3

    .prologue
    .line 983
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 984
    const-string v1, "user_grp_selection"

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v2}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 985
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 986
    return-void
.end method

.method private showAddContactActivity()V
    .locals 3

    .prologue
    .line 800
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.INSERT"

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 802
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/ContactListActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 806
    :goto_0
    return-void

    .line 803
    :catch_0
    move-exception v0

    .line 804
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "Not activity was found for ACTION_INSERT (for adding contact)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showContactDetailPage(Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 4
    .parameter

    .prologue
    .line 544
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "send message to client core to open the detail page of a selected UI contact"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;

    invoke-virtual {p1}, Lcom/sgiggle/production/Utils$UIContact;->convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 547
    return-void
.end method

.method static storeContacts(Lcom/sgiggle/messaging/Message;Landroid/content/Context;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 235
    invoke-static {p1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 238
    invoke-virtual {p0}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 246
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "storeContacts(): Unsupported message = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :goto_0
    return-void

    .line 240
    :pswitch_0
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoUsersEvent;

    .line 241
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoUsersEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getContactsList()Ljava/util/List;

    move-result-object v1

    .line 242
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateTangoUsersEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 250
    const-string v0, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "storeContacts(): # of entries = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", source-type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 253
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 254
    invoke-static {v0}, Lcom/sgiggle/production/Utils$UIContact;->convertFromMessageContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/production/Utils$UIContact;

    move-result-object v0

    .line 255
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 258
    :cond_0
    new-instance v0, Lcom/sgiggle/production/Utils$ContactComparator;

    sget-object v1, Lcom/sgiggle/production/ContactListActivity;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    invoke-virtual {v1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/Utils$ContactComparator;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 259
    sput-object v2, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x88dd
        :pswitch_0
    .end packed-switch
.end method

.method private switchFooterFrame(Z)V
    .locals 2
    .parameter

    .prologue
    .line 1084
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1085
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1086
    return-void

    .line 1085
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private updateEmptyView()V
    .locals 1

    .prologue
    .line 1065
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView(Z)V

    .line 1066
    return-void
.end method

.method private updateEmptyView(Z)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0x8

    .line 1069
    if-eqz p1, :cond_0

    .line 1070
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyViewText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1071
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyInviteButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1081
    :goto_0
    return-void

    .line 1073
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v0, v1, :cond_1

    .line 1074
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyViewText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1075
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyInviteButton:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 1077
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyViewText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyTangoListText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1078
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyInviteButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V
    .locals 7
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 1013
    if-nez p1, :cond_2

    .line 1025
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    if-lez v0, :cond_3

    move v0, v4

    .line 1027
    :goto_1
    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->switchFooterFrame(Z)V

    .line 1029
    if-eqz v0, :cond_1

    .line 1030
    const v0, 0x7f0a0003

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1031
    new-instance v1, Lcom/sgiggle/production/ContactListActivity$7;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$7;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1043
    const v0, 0x7f0a0005

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1046
    iget v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    if-ne v1, v4, :cond_4

    .line 1047
    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 1051
    :goto_2
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1052
    new-instance v1, Landroid/text/style/UnderlineSpan;

    invoke-direct {v1}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v2, v1, v6, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1053
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1055
    :cond_1
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "displayTangoAlerts()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    return-void

    .line 1016
    :cond_2
    iput v6, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    .line 1017
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 1018
    invoke-static {p1}, Lcom/sgiggle/production/CTANotifier;->extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;

    move-result-object v0

    .line 1019
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 1020
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    .line 1021
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_alert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    goto :goto_0

    :cond_3
    move v0, v6

    .line 1025
    goto :goto_1

    .line 1049
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0008

    iget v3, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/sgiggle/production/ContactListActivity;->m_alertCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method protected handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 493
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNewMessage(): Message = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    if-nez p1, :cond_0

    .line 496
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 537
    :goto_0
    return-void

    .line 500
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 534
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNewMessage(): Unsupported message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 502
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;

    .line 503
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayInviteContactEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    .line 504
    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

    invoke-virtual {v2, v1}, Lcom/sgiggle/production/InviteDialogCreater;->doInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)V

    goto :goto_0

    .line 509
    :sswitch_1
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;

    move-object v1, v0

    .line 510
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasSpecifiedEmptyListPrompt()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getSpecifiedEmptyListPrompt()Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyTangoListText:Ljava/lang/String;

    .line 512
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView()V

    .line 513
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 510
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 518
    :sswitch_2
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 523
    :sswitch_3
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->hideTangoAlerts()V

    goto :goto_0

    .line 529
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;

    .line 530
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->openContactDetailPage(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayContactDetailEvent;)V

    goto :goto_0

    .line 500
    nop

    :sswitch_data_0
    .sparse-switch
        0x88c3 -> :sswitch_1
        0x88dd -> :sswitch_2
        0x8910 -> :sswitch_3
        0x8913 -> :sswitch_0
        0x8990 -> :sswitch_4
    .end sparse-switch
.end method

.method public handleSearchIntent(Landroid/content/Intent;)V
    .locals 5
    .parameter

    .prologue
    .line 606
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSearchIntent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 608
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    .line 609
    if-nez v0, :cond_0

    .line 611
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    .line 650
    :goto_0
    return-void

    .line 614
    :cond_0
    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TabActivityBase;->setContactSearchActivity(Lcom/sgiggle/production/ContactListActivity;)V

    .line 615
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    .line 617
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "*debug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 619
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "*debug1#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 620
    invoke-static {v0}, Lcom/sgiggle/production/TangoApp;->saveScreenLoggerOption(Z)V

    .line 621
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set screen-logger = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->handleSearch(Ljava/lang/String;)V

    goto :goto_0

    .line 622
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "*vmailr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 624
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "*vmailr1#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 625
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->setForceVideomailReplay(Landroid/content/Context;Z)V

    .line 626
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set forceVideomailReplay = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 627
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "*ffc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 629
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 630
    const-string v1, "*ffc"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    .line 631
    const/4 v2, -0x1

    .line 632
    if-le v0, v1, :cond_4

    .line 634
    :try_start_0
    iget-object v3, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 641
    :goto_2
    invoke-static {v0}, Lcom/sgiggle/production/TangoApp;->saveFrontCameraRotationOption(I)V

    .line 642
    const-string v1, "Tango.ContactsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set camera rotation option = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 636
    :catch_0
    move-exception v0

    .line 638
    const-string v1, "Tango.ContactsActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid rotation value"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v2

    goto :goto_2

    .line 648
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

    const v1, 0x7f0a0074

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto/16 :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 854
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult(): requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    packed-switch p1, :pswitch_data_0

    .line 863
    :cond_0
    :goto_0
    return-void

    .line 857
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 858
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->postRequestContactFilter()V

    goto :goto_0

    .line 855
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1145
    packed-switch p2, :pswitch_data_0

    .line 1157
    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contacts:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1158
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contacts:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->displayContacts(Ljava/util/List;)V

    .line 1160
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView()V

    .line 1161
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->saveUserGroupSelectionType()V

    .line 1162
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->restoreScrollPositionInList()V

    .line 1163
    return-void

    .line 1147
    :pswitch_1
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    goto :goto_0

    .line 1150
    :pswitch_2
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    goto :goto_0

    .line 1153
    :pswitch_3
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    goto :goto_0

    .line 1145
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a0072
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 324
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 326
    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->setContentView(I)V

    .line 328
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->setDefaultKeyMode(I)V

    .line 330
    const-string v0, "Tango.ContactsActivity"

    invoke-virtual {p0, v0, v3}, Lcom/sgiggle/production/ContactListActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_prefs:Landroid/content/SharedPreferences;

    .line 331
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->restoreGroupSelectionType()V

    .line 333
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_Inflater:Landroid/view/LayoutInflater;

    .line 334
    new-instance v0, Lcom/sgiggle/production/InviteDialogCreater;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_Inflater:Landroid/view/LayoutInflater;

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/InviteDialogCreater;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->mInviteDialogCreater:Lcom/sgiggle/production/InviteDialogCreater;

    .line 336
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    .line 337
    const v0, 0x7f0a003f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyView:Landroid/view/View;

    .line 338
    const v0, 0x7f0a0076

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyViewText:Landroid/widget/TextView;

    .line 339
    const v0, 0x7f0a0077

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyInviteButton:Landroid/widget/Button;

    .line 340
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyInviteButton:Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/ContactListActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$2;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 352
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setEmptyView(Landroid/view/View;)V

    .line 353
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_emptyTangoListText:Ljava/lang/String;

    .line 356
    const v0, 0x7f0a0071

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

    .line 357
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_contactListGroupRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 366
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030017

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;

    .line 367
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerView:Landroid/view/View;

    const v1, 0x7f0a0079

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_footerViewCount:Landroid/widget/TextView;

    .line 369
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->updateEmptyView()V

    .line 371
    const v0, 0x7f0a0070

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_searchTitleView:Landroid/widget/TextView;

    .line 373
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    .line 374
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    const/high16 v1, -0x100

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 375
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_header:Landroid/widget/TextView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setItemsCanFocus(Z)V

    .line 377
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setClickable(Z)V

    .line 378
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    new-instance v1, Lcom/sgiggle/production/ContactListActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$3;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 402
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->handleSearchIntent(Landroid/content/Intent;)V

    .line 404
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    new-instance v1, Lcom/sgiggle/production/ContactListActivity$4;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$4;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 416
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    new-instance v1, Lcom/sgiggle/production/ContactListActivity$5;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$5;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 424
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->videomailSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 425
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 427
    :cond_0
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 1090
    const-string v0, "Tango.ContactsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateDialog type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    packed-switch p1, :pswitch_data_0

    .line 1107
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1093
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 1094
    const v1, 0x7f090040

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/ContactListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1095
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1096
    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1097
    new-instance v1, Lcom/sgiggle/production/ContactListActivity$8;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactListActivity$8;-><init>(Lcom/sgiggle/production/ContactListActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 1091
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 756
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 757
    const/4 v0, 0x1

    .line 764
    :goto_0
    return v0

    .line 760
    :cond_0
    const/4 v0, 0x4

    const v1, 0x7f09003d

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a4

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 761
    const/4 v0, 0x5

    const v1, 0x7f090041

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 762
    const/4 v0, 0x6

    const v1, 0x7f09003e

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 764
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 924
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 925
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 927
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 928
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    .line 929
    if-eqz v0, :cond_0

    .line 930
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TabActivityBase;->setContactSearchActivity(Lcom/sgiggle/production/ContactListActivity;)V

    .line 935
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_isDestroyed:Z

    .line 936
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1113
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1114
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "ignore back key down"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    const/4 v0, 0x0

    .line 1122
    :goto_0
    return v0

    .line 1117
    :cond_0
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 1118
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "open options menu"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1119
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->openOptionsMenu()V

    .line 1120
    const/4 v0, 0x1

    goto :goto_0

    .line 1122
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1127
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 1128
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1129
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->finish()V

    move v0, v1

    .line 1139
    :goto_0
    return v0

    .line 1132
    :cond_0
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "ignore back key up"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1133
    const/4 v0, 0x0

    goto :goto_0

    .line 1135
    :cond_1
    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 1137
    goto :goto_0

    .line 1139
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 431
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onNewIntent()."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleSearchIntent(Landroid/content/Intent;)V

    .line 435
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 769
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 785
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 773
    :pswitch_0
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->clear()V

    .line 774
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 775
    iput-boolean v2, p0, Lcom/sgiggle/production/ContactListActivity;->m_hasRefreshProgressDlg:Z

    .line 776
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->postRequestContactFilter()V

    move v0, v2

    .line 777
    goto :goto_0

    .line 779
    :pswitch_1
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->onSearchRequested()Z

    move v0, v2

    .line 780
    goto :goto_0

    .line 782
    :pswitch_2
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->showAddContactActivity()V

    move v0, v2

    .line 783
    goto :goto_0

    .line 769
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 912
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 914
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->stop()V

    .line 917
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 918
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->saveScrollPositionInList()V

    .line 920
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 879
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 881
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->start()V

    .line 885
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 886
    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_allContacts:Ljava/util/List;

    .line 887
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/ContactListActivity;->m_contacts:Ljava/util/List;

    if-eq v0, v1, :cond_0

    .line 888
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onResume: Display new Tango contacts..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->updateContacts()V

    .line 893
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivity;->displayTangoAlerts(Lcom/sgiggle/messaging/Message;)V

    .line 902
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->requestFocus()Z

    .line 907
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_listView:Lcom/sgiggle/production/ListViewIgnoreBackKey;

    invoke-virtual {v0}, Lcom/sgiggle/production/ListViewIgnoreBackKey;->clearFocus()V

    .line 908
    return-void
.end method

.method public onSearchRequested()Z
    .locals 2

    .prologue
    .line 873
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "onSearchRequested()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 874
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method updateContacts()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 441
    const-string v0, "Tango.ContactsActivity"

    const-string v1, "updateContacts()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_groupSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/sgiggle/production/ContactListActivity;->m_sourceType:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    if-ne v0, v1, :cond_3

    .line 448
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_hasRefreshProgressDlg:Z

    if-nez v0, :cond_2

    .line 449
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    .line 450
    iput-boolean v3, p0, Lcom/sgiggle/production/ContactListActivity;->m_hasRefreshProgressDlg:Z

    .line 451
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x3a98

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 458
    :cond_2
    :goto_0
    return-void

    .line 455
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 456
    invoke-direct {p0}, Lcom/sgiggle/production/ContactListActivity;->doUpdateContacts()V

    goto :goto_0
.end method
