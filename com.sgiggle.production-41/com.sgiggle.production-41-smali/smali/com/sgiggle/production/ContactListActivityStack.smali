.class public Lcom/sgiggle/production/ContactListActivityStack;
.super Lcom/sgiggle/production/ActivityStack;
.source "ContactListActivityStack.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ContactListActivityStack"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityStack;-><init>()V

    return-void
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 31
    const-string v0, "Tango.ContactListActivityStack"

    const-string v1, "handleIntent"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 32
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivityStack;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleSearchIntent(Landroid/content/Intent;)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 16
    const-string v0, "Tango.ContactListActivityStack"

    const-string v1, "onCreat"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 17
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityStack;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const-string v0, "ContactListActivity"

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sgiggle/production/ContactListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/ContactListActivityStack;->push(Ljava/lang/String;Landroid/content/Intent;)V

    .line 19
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivityStack;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactListActivityStack;->handleIntent(Landroid/content/Intent;)V

    .line 20
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactListActivityStack;->currentActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .parameter

    .prologue
    .line 24
    const-string v0, "Tango.ContactListActivityStack"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewIntent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/ContactListActivityStack;->setIntent(Landroid/content/Intent;)V

    .line 26
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactListActivityStack;->handleIntent(Landroid/content/Intent;)V

    .line 27
    return-void
.end method

.method public onSearchRequested()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/ContactListActivityStack;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-le v0, v4, :cond_0

    move v0, v4

    .line 56
    :goto_0
    return v0

    .line 43
    :cond_0
    const-string v0, "Tango.ContactListActivityStack"

    const-string v1, "onSearchRequested()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v1

    .line 45
    if-eqz v1, :cond_1

    .line 46
    const-string v0, "Tango.ContactListActivityStack"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSearchRequested: tabsUi="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-virtual {v1, v4}, Lcom/sgiggle/production/TabActivityBase;->onSearchModeEntered(Z)V

    .line 48
    const-string v0, "search"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactListActivityStack;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 49
    new-instance v2, Lcom/sgiggle/production/ContactListActivityStack$1;

    invoke-direct {v2, p0, v1}, Lcom/sgiggle/production/ContactListActivityStack$1;-><init>(Lcom/sgiggle/production/ContactListActivityStack;Lcom/sgiggle/production/TabActivityBase;)V

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->setOnCancelListener(Landroid/app/SearchManager$OnCancelListener;)V

    .line 56
    :cond_1
    invoke-super {p0}, Lcom/sgiggle/production/ActivityStack;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method
