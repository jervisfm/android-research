.class Lcom/sgiggle/production/ContactSelectionActivity$2;
.super Ljava/lang/Object;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 479
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 481
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$000(Lcom/sgiggle/production/ContactSelectionActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 482
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 483
    iput-boolean v1, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    .line 484
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_adapter:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$400(Lcom/sgiggle/production/ContactSelectionActivity;)Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->notifyDataSetChanged()V

    goto :goto_1

    :cond_0
    move v1, v3

    .line 481
    goto :goto_0

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    :goto_2
    #setter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$002(Lcom/sgiggle/production/ContactSelectionActivity;I)I

    .line 487
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$2;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->onCheckedItemChanged()V
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$200(Lcom/sgiggle/production/ContactSelectionActivity;)V

    .line 488
    return-void

    :cond_2
    move v1, v3

    .line 486
    goto :goto_2
.end method
