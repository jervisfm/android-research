.class Lcom/sgiggle/production/ContactSelectionActivity$3;
.super Ljava/lang/Object;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 503
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity$3;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 506
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 509
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$3;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 510
    iget-boolean v3, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v3, :cond_0

    .line 514
    iget-object v0, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 516
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$3;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->onContactsPicked(Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$500(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/util/ArrayList;)V

    .line 517
    return-void
.end method
