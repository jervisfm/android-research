.class Lcom/sgiggle/production/ContactSelectionActivity$4;
.super Ljava/lang/Object;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 520
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 522
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;
    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$600(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #setter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$602(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 524
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #getter for: Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;

    move-result-object v1

    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->displayContacts(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$700(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/util/List;)V

    .line 528
    :goto_0
    return-void

    .line 526
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$4;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->onContactsPicked(Ljava/util/ArrayList;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$500(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/util/ArrayList;)V

    goto :goto_0
.end method
