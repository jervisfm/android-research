.class Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$1;
.super Ljava/lang/Object;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 437
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$1;->this$0:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 439
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 440
    if-eqz v0, :cond_0

    .line 441
    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$1;->this$0:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/ContactSelectionActivity;

    .line 442
    iget-boolean v2, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    if-nez v2, :cond_1

    move v2, v4

    :goto_0
    iput-boolean v2, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    .line 443
    iget-boolean v2, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v2, :cond_2

    move v2, v4

    :goto_1
    invoke-static {v1, v2}, Lcom/sgiggle/production/ContactSelectionActivity;->access$012(Lcom/sgiggle/production/ContactSelectionActivity;I)I

    .line 444
    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->checkSelectionLimitReached(Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z
    invoke-static {v1, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->access$100(Lcom/sgiggle/production/ContactSelectionActivity;Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 445
    #calls: Lcom/sgiggle/production/ContactSelectionActivity;->onCheckedItemChanged()V
    invoke-static {v1}, Lcom/sgiggle/production/ContactSelectionActivity;->access$200(Lcom/sgiggle/production/ContactSelectionActivity;)V

    .line 450
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v3

    .line 442
    goto :goto_0

    .line 443
    :cond_2
    const/4 v2, -0x1

    goto :goto_1

    .line 447
    :cond_3
    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2
.end method
