.class Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ContactArrayAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;",
        ">;",
        "Landroid/widget/SectionIndexer;"
    }
.end annotation


# instance fields
.field private alphaIndexer:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final m_Inflater:Landroid/view/LayoutInflater;

.field private m_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private final m_textViewResourceId:I

.field private sections:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 362
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 363
    invoke-static {p1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v1

    .line 364
    iput p2, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_textViewResourceId:I

    .line 366
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_Inflater:Landroid/view/LayoutInflater;

    .line 367
    iput-object p3, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_items:Ljava/util/List;

    .line 369
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->alphaIndexer:Ljava/util/HashMap;

    move v2, v5

    .line 370
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 372
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 373
    iget-object v3, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 374
    iget-object v3, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 377
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 378
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 379
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 380
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->sections:[Ljava/lang/String;

    .line 381
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->sections:[Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 382
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPositionForSection(I)I
    .locals 2
    .parameter

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->alphaIndexer:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->sections:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1
    .parameter

    .prologue
    .line 391
    const/4 v0, 0x1

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->sections:[Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 411
    if-nez p2, :cond_0

    .line 412
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_Inflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_textViewResourceId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 413
    new-instance v2, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;-><init>()V

    .line 414
    const v0, 0x7f0a000e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 415
    const v0, 0x7f0a00b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->subLabel:Landroid/widget/TextView;

    .line 416
    const v0, 0x7f0a00b4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v2, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    .line 417
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v5, v2

    move-object v2, v1

    move-object v1, v5

    .line 422
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 423
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->name:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 426
    iget-object v3, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v3

    .line 427
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 428
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->name:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 433
    :goto_1
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->subLabel:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 434
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    iget-boolean v4, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 436
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 437
    iget-object v0, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->checkBox:Landroid/widget/CheckBox;

    new-instance v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$1;-><init>(Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    return-object v2

    .line 419
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;

    move-object v1, v0

    move-object v2, p2

    goto :goto_0

    .line 430
    :cond_1
    iget-object v3, v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter$ViewHolder;->name:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1
.end method
