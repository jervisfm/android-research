.class Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;
.super Lcom/sgiggle/production/Utils$UIContact;
.source "ContactSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ContactSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactItem"
.end annotation


# instance fields
.field public m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field public m_selected:Z

.field public m_subLabel:Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/production/ContactSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ContactSelectionActivity;Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->this$0:Lcom/sgiggle/production/ContactSelectionActivity;

    .line 329
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/Utils$UIContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    .line 332
    iput-object p2, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 333
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    .line 335
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public displayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    goto :goto_0
.end method
