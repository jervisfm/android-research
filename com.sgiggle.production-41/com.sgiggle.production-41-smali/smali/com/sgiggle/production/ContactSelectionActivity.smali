.class public abstract Lcom/sgiggle/production/ContactSelectionActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "ContactSelectionActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;,
        Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;
    }
.end annotation


# static fields
.field public static final CONTACT_SELECTION_UNLIMITED:I = -0x1

.field private static final DIALOG_REACHED_LIMIT:I = 0x0

.field public static final EXTRA_FULL_SCREEN:Ljava/lang/String; = "FullScreen"

.field public static final EXTRA_FULL_SCREEN_DEFAULT:Z = false

.field public static final EXTRA_LOCK_IN_PORTRAIT:Ljava/lang/String; = "LockInPortrait"

.field public static final EXTRA_LOCK_IN_PORTRAIT_DEFAULT:Z = false

.field public static final EXTRA_SELECTED_CONTACTS:Ljava/lang/String; = "SelectedContacts"

.field public static final REQUEST_CODE_SELECT_CONTACTS:I = 0x14

.field private static final SEARCH_ID:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.ContactSelectionActivity"

.field private static s_latestContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_adapter:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

.field private m_allButton:Landroid/widget/CheckBox;

.field private m_cancelListener:Landroid/view/View$OnClickListener;

.field private m_contactItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_emptyView:Landroid/widget/TextView;

.field private m_header:Landroid/widget/TextView;

.field private m_listView:Landroid/widget/ListView;

.field private m_localContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private m_okButton:Landroid/widget/Button;

.field private m_okListener:Landroid/view/View$OnClickListener;

.field private m_query:Ljava/lang/String;

.field private m_selectAllListener:Landroid/view/View$OnClickListener;

.field private m_selectAllPanel:Landroid/view/ViewGroup;

.field private m_selectLimit:I

.field private m_selectedCount:I

.field private m_titleView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/ContactSelectionActivity;->s_latestContacts:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    .line 105
    sget-object v0, Lcom/sgiggle/production/ContactSelectionActivity;->s_latestContacts:Ljava/util/List;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_localContacts:Ljava/util/List;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    .line 109
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    .line 479
    new-instance v0, Lcom/sgiggle/production/ContactSelectionActivity$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ContactSelectionActivity$2;-><init>(Lcom/sgiggle/production/ContactSelectionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllListener:Landroid/view/View$OnClickListener;

    .line 503
    new-instance v0, Lcom/sgiggle/production/ContactSelectionActivity$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ContactSelectionActivity$3;-><init>(Lcom/sgiggle/production/ContactSelectionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okListener:Landroid/view/View$OnClickListener;

    .line 520
    new-instance v0, Lcom/sgiggle/production/ContactSelectionActivity$4;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ContactSelectionActivity$4;-><init>(Lcom/sgiggle/production/ContactSelectionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_cancelListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/ContactSelectionActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 64
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    return v0
.end method

.method static synthetic access$002(Lcom/sgiggle/production/ContactSelectionActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    iput p1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    return p1
.end method

.method static synthetic access$012(Lcom/sgiggle/production/ContactSelectionActivity;I)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 64
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/ContactSelectionActivity;Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactSelectionActivity;->checkSelectionLimitReached(Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/ContactSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->onCheckedItemChanged()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/ContactSelectionActivity;)Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_adapter:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactSelectionActivity;->onContactsPicked(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/ContactSelectionActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sgiggle/production/ContactSelectionActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ContactSelectionActivity;->displayContacts(Ljava/util/List;)V

    return-void
.end method

.method private checkSelectionLimitReached(Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 544
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    if-le v0, v1, :cond_0

    .line 545
    iput-boolean v2, p1, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    .line 546
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    .line 547
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->onSelectionLimitReached()V

    move v0, v3

    .line 550
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private displayContacts(Ljava/util/List;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 213
    const-string v0, "Tango.ContactSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "displayContacts(): New list-size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_titleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getTitleString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 221
    const v0, 0x7f0b0001

    .line 230
    :goto_0
    new-instance v1, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    const v2, 0x7f03002d

    invoke-direct {v1, p0, v2, p1}, Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_adapter:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    .line 231
    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_adapter:Lcom/sgiggle/production/ContactSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 232
    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 233
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 234
    if-lez v1, :cond_1

    .line 235
    iget-object v2, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_header:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    :goto_1
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    if-le v1, v0, :cond_2

    .line 241
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 245
    :goto_2
    return-void

    .line 223
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_titleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 227
    const v0, 0x7f0b0002

    goto :goto_0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_header:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 243
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_2
.end method

.method public static getSupportedMediaType(Landroid/content/Context;)Lcom/sgiggle/xmpp/SessionMessages$MediaType;
    .locals 2
    .parameter

    .prologue
    .line 600
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "SMS supported, can ask for SMS and EMAIL contacts."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->SMS_EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    .line 605
    :goto_0
    return-object v0

    .line 604
    :cond_0
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "SMS not supported, can ask for EMAIL contacts only."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaType;->EMAIL:Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    goto :goto_0
.end method

.method private handleSearch(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    .line 248
    const-string v0, "Tango.ContactSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSearch(query="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 251
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 253
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 254
    iget-object v4, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 257
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    :cond_2
    invoke-direct {p0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->displayContacts(Ljava/util/List;)V

    .line 262
    return-void
.end method

.method private onCheckedItemChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 558
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okButton:Landroid/widget/Button;

    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->getOkButtonString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 559
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okButton:Landroid/widget/Button;

    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    if-lez v1, :cond_0

    move v1, v4

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 560
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    iget-object v2, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_1

    move v1, v4

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 561
    return-void

    :cond_0
    move v1, v3

    .line 559
    goto :goto_0

    :cond_1
    move v1, v3

    .line 560
    goto :goto_1
.end method

.method private onContactsPicked(Ljava/util/ArrayList;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    iget v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 494
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setResult(I)V

    .line 500
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->finish()V

    .line 501
    return-void

    .line 497
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "SelectedContacts"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 498
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public static setLatestContactList(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    sput-object p0, Lcom/sgiggle/production/ContactSelectionActivity;->s_latestContacts:Ljava/util/List;

    .line 297
    return-void
.end method

.method private setupView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 265
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setContentView(I)V

    .line 268
    const v0, 0x7f0a00b6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    .line 269
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    const v0, 0x7f0a00b8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okButton:Landroid/widget/Button;

    .line 272
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 274
    const v0, 0x7f0a00b9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 275
    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_cancelListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setDefaultKeyMode(I)V

    .line 280
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    .line 281
    const v0, 0x7f0a003f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 284
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_titleView:Landroid/widget/TextView;

    .line 285
    const v0, 0x7f0a00b5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    .line 287
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_header:Landroid/widget/TextView;

    .line 288
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_header:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 289
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 292
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_okButton:Landroid/widget/Button;

    invoke-virtual {p0, v3}, Lcom/sgiggle/production/ContactSelectionActivity;->getOkButtonString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 293
    return-void
.end method


# virtual methods
.method public fillList()V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 303
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "setContacts ignored, since the list has already been set."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_localContacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 308
    iget-object v2, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    new-instance v3, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    invoke-direct {v3, p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;-><init>(Lcom/sgiggle/production/ContactSelectionActivity;Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 311
    :cond_1
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v0

    .line 312
    iget-object v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    new-instance v2, Lcom/sgiggle/production/Utils$ContactComparator;

    invoke-direct {v2, v0}, Lcom/sgiggle/production/Utils$ContactComparator;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 313
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->displayContacts(Ljava/util/List;)V

    .line 316
    invoke-direct {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->onCheckedItemChanged()V

    goto :goto_0
.end method

.method protected abstract getOkButtonString(I)Ljava/lang/String;
.end method

.method protected getSelectionLimit()I
    .locals 1

    .prologue
    .line 584
    const/4 v0, -0x1

    return v0
.end method

.method protected getSelectionLimitReachedString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 591
    const-string v0, ""

    return-object v0
.end method

.method protected getTitleString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 575
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 533
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 535
    iput-object v2, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    .line 536
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->displayContacts(Ljava/util/List;)V

    .line 540
    :goto_0
    return-void

    .line 538
    :cond_0
    invoke-direct {p0, v2}, Lcom/sgiggle/production/ContactSelectionActivity;->onContactsPicked(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 118
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_1

    .line 125
    const-string v1, "FullScreen"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 127
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x800

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 131
    :cond_0
    const-string v1, "LockInPortrait"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setRequestedOrientation(I)V

    .line 136
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->setupView()V

    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_localContacts:Ljava/util/List;

    if-nez v0, :cond_2

    .line 139
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_localContacts:Ljava/util/List;

    .line 141
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    .line 143
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getSelectionLimit()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectLimit:I

    .line 145
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->fillList()V

    .line 146
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 150
    packed-switch p1, :pswitch_data_0

    .line 162
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 152
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 153
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->getSelectionLimitReachedString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 154
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/ContactSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/ContactSelectionActivity$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/ContactSelectionActivity$1;-><init>(Lcom/sgiggle/production/ContactSelectionActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 150
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 170
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v2

    .line 175
    :goto_0
    return v0

    .line 174
    :cond_0
    const v0, 0x7f090041

    invoke-interface {p1, v1, v2, v1, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    move v0, v2

    .line 175
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/ContactSelectionActivity;->s_latestContacts:Ljava/util/List;

    .line 191
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 192
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 465
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 466
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;

    .line 467
    iget-boolean v1, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    if-nez v1, :cond_1

    move v1, v3

    :goto_0
    iput-boolean v1, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    .line 468
    iget v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    iget-boolean v2, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    add-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_selectedCount:I

    .line 469
    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->checkSelectionLimitReached(Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 470
    invoke-direct {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->onCheckedItemChanged()V

    .line 472
    const v1, 0x7f0a00b4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 473
    if-eqz v1, :cond_0

    .line 474
    iget-boolean v0, v0, Lcom/sgiggle/production/ContactSelectionActivity$ContactItem;->m_selected:Z

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 477
    :cond_0
    return-void

    .line 467
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 468
    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 196
    const-string v0, "Tango.ContactSelectionActivity"

    const-string v1, "onNewIntent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_contactItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 202
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/sgiggle/production/ContactSelectionActivity;->m_query:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->handleSearch(Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 180
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 185
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 182
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/ContactSelectionActivity;->onSearchRequested()Z

    .line 183
    const/4 v0, 0x1

    goto :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onSelectionLimitReached()V
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ContactSelectionActivity;->showDialog(I)V

    .line 555
    return-void
.end method
