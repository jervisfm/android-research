.class public Lcom/sgiggle/production/CountryListActivity$CountryData;
.super Ljava/lang/Object;
.source "CountryListActivity.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/CountryListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CountryData"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sgiggle/production/CountryListActivity$CountryData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public m_cid:Ljava/lang/String;

.field public m_code:Ljava/lang/String;

.field public m_isocc:Ljava/lang/String;

.field public m_name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/sgiggle/production/CountryListActivity$CountryData$1;

    invoke-direct {v0}, Lcom/sgiggle/production/CountryListActivity$CountryData$1;-><init>()V

    sput-object v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    .line 126
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 127
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    .line 128
    const/4 v1, 0x1

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    .line 129
    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    .line 130
    const/4 v1, 0x3

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_isocc:Ljava/lang/String;

    .line 131
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sgiggle/production/CountryListActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sgiggle/production/CountryListActivity$CountryData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    .line 112
    iput-object p2, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    .line 113
    iput-object p3, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    .line 114
    iput-object p4, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_isocc:Ljava/lang/String;

    .line 115
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 119
    const-string v0, "%5s  %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    :goto_0
    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 140
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_isocc:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 141
    return-void
.end method
