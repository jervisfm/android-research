.class public Lcom/sgiggle/production/CountryListActivity;
.super Landroid/app/Activity;
.source "CountryListActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/CountryListActivity$CountryData;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.CountryListActivity"

.field private static s_countryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CountryListActivity$CountryData;",
            ">;"
        }
    .end annotation
.end field

.field private static s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

.field private static s_selectedCountryId:Ljava/lang/String;


# instance fields
.field private m_query:Ljava/lang/String;

.field private m_searchTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 104
    return-void
.end method

.method public static buildSelectableCountriesFromList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/CountryListActivity$CountryData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 229
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 230
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryid()Ljava/lang/String;

    move-result-object v3

    .line 231
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v4

    .line 232
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryname()Ljava/lang/String;

    move-result-object v5

    .line 233
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisocc()Ljava/lang/String;

    move-result-object v0

    .line 234
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    if-eqz v5, :cond_0

    if-eqz v0, :cond_0

    .line 238
    new-instance v6, Lcom/sgiggle/production/CountryListActivity$CountryData;

    invoke-direct {v6, v3, v4, v5, v0}, Lcom/sgiggle/production/CountryListActivity$CountryData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 242
    :cond_1
    return-object v1
.end method

.method private displayCountries(Ljava/util/List;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/CountryListActivity$CountryData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 71
    const/4 v0, -0x1

    .line 72
    const/4 v1, 0x0

    .line 74
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v3, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 75
    add-int/lit8 v3, v3, 0x1

    .line 76
    iget-object v0, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    sget-object v4, Lcom/sgiggle/production/CountryListActivity;->s_selectedCountryId:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "Tango.CountryListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectedCountry - Index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 84
    :goto_0
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CountryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 85
    const v1, 0x7f0a003f

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/CountryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 86
    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090057

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 88
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030022

    invoke-direct {v1, p0, v3, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 91
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 92
    if-ltz v2, :cond_1

    .line 93
    invoke-virtual {v0, v2, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 94
    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 97
    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setTextFilterEnabled(Z)V

    .line 98
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 99
    return-void

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method private handleSearch(Ljava/lang/String;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 179
    const-string v0, "Tango.CountryListActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSearch: ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 182
    sget-object v0, Lcom/sgiggle/production/CountryListActivity;->s_countryList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 183
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 185
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_searchTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_searchTitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090055

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 191
    iget-object v4, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 193
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 197
    :cond_1
    invoke-direct {p0, v3}, Lcom/sgiggle/production/CountryListActivity;->displayCountries(Ljava/util/List;)V

    .line 198
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 48
    const-string v0, "Tango.CountryListActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f030023

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CountryListActivity;->setContentView(I)V

    .line 52
    const v0, 0x7f0a0070

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/CountryListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_searchTitleView:Landroid/widget/TextView;

    .line 54
    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 56
    const-string v1, "android.intent.action.SEARCH"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const-string v1, "query"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CountryListActivity;->handleSearch(Ljava/lang/String;)V

    .line 67
    :goto_0
    return-void

    .line 62
    :cond_0
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/CountryListActivity;->setDefaultKeyMode(I)V

    .line 63
    const-string v1, "countries"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    sput-object v1, Lcom/sgiggle/production/CountryListActivity;->s_countryList:Ljava/util/List;

    .line 64
    const-string v1, "selectedCountryId"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/CountryListActivity;->s_selectedCountryId:Ljava/lang/String;

    .line 66
    sget-object v0, Lcom/sgiggle/production/CountryListActivity;->s_countryList:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/CountryListActivity;->displayCountries(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 216
    const-string v0, "Tango.CountryListActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 219
    sput-object v2, Lcom/sgiggle/production/CountryListActivity;->s_selectedCountryId:Ljava/lang/String;

    .line 220
    sput-object v2, Lcom/sgiggle/production/CountryListActivity;->s_countryList:Ljava/util/List;

    .line 221
    sput-object v2, Lcom/sgiggle/production/CountryListActivity;->s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 223
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 224
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 156
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 157
    iget-object v1, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    .line 158
    iget-object v2, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    .line 159
    iget-object v3, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    .line 160
    iget-object v4, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_isocc:Ljava/lang/String;

    .line 161
    const-string v4, "Tango.CountryListActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onItemClick: position = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", Id = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ", Country = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v1, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 166
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 167
    const-string v2, "selectedCountry"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 168
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/CountryListActivity;->setResult(ILandroid/content/Intent;)V

    .line 175
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->finish()V

    .line 176
    return-void

    .line 173
    :cond_0
    sput-object v0, Lcom/sgiggle/production/CountryListActivity;->s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 202
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 203
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lcom/sgiggle/production/CountryListActivity;->s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

    if-eqz v0, :cond_0

    .line 205
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 206
    const-string v1, "selectedCountry"

    sget-object v2, Lcom/sgiggle/production/CountryListActivity;->s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 207
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/CountryListActivity;->setResult(ILandroid/content/Intent;)V

    .line 208
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/CountryListActivity;->s_result:Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 209
    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->finish()V

    .line 212
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 247
    const-string v0, "Tango.CountryListActivity"

    const-string v1, "onSearchRequested()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v0, p0, Lcom/sgiggle/production/CountryListActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    move v0, v4

    .line 262
    :goto_0
    return v0

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/CountryListActivity;->getParent()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TabActivityBase;

    .line 252
    if-eqz v0, :cond_1

    .line 253
    const-string v1, "Tango.CountryListActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSearchRequested: tabsUi="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-virtual {v0, v4}, Lcom/sgiggle/production/TabActivityBase;->onSearchModeEntered(Z)V

    .line 255
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/CountryListActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/SearchManager;

    .line 256
    new-instance v2, Lcom/sgiggle/production/CountryListActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/sgiggle/production/CountryListActivity$1;-><init>(Lcom/sgiggle/production/CountryListActivity;Lcom/sgiggle/production/TabActivityBase;)V

    invoke-virtual {v1, v2}, Landroid/app/SearchManager;->setOnDismissListener(Landroid/app/SearchManager$OnDismissListener;)V

    .line 262
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method
