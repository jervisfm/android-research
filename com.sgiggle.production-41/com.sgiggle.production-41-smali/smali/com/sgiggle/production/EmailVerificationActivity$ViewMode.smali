.class final enum Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;
.super Ljava/lang/Enum;
.source "EmailVerificationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/EmailVerificationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

.field public static final enum VERIFICATION_DEVICE_BY_EMAIL:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

.field public static final enum VERIFICATION_DEVICE_BY_PUSH:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

.field public static final enum VERIFICATION_DEVICE_BY_SMS:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

.field public static final enum VERIFICATION_DEVICE_SMS_SENT:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    const-string v1, "VERIFICATION_DEVICE_BY_EMAIL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_EMAIL:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 36
    new-instance v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    const-string v1, "VERIFICATION_DEVICE_BY_PUSH"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_PUSH:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 37
    new-instance v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    const-string v1, "VERIFICATION_DEVICE_BY_SMS"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_SMS:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 38
    new-instance v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    const-string v1, "VERIFICATION_DEVICE_SMS_SENT"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_SMS_SENT:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 34
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_EMAIL:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_PUSH:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_SMS:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_SMS_SENT:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->$VALUES:[Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;
    .locals 1
    .parameter

    .prologue
    .line 34
    const-class v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->$VALUES:[Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    invoke-virtual {v0}, [Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    return-object v0
.end method
