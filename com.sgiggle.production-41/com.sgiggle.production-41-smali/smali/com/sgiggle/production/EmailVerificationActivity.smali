.class public Lcom/sgiggle/production/EmailVerificationActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "EmailVerificationActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/EmailVerificationActivity$2;,
        Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.EmailVerificationActivity"


# instance fields
.field private m_WhatToDoLabel:Landroid/widget/TextView;

.field private m_notReceiveLabel:Landroid/widget/TextView;

.field private m_sentLabel:Landroid/widget/TextView;

.field private m_submitButton:Landroid/widget/Button;

.field private m_switchModeLink:Landroid/widget/TextView;

.field private m_userInfoLabel:Landroid/widget/TextView;

.field private m_verificationCodeEditText:Landroid/widget/EditText;

.field private m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 41
    sget-object v0, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_EMAIL:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    return-void
.end method


# virtual methods
.method protected broadCastNoChange()V
    .locals 3

    .prologue
    .line 260
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    .line 261
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 262
    return-void
.end method

.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f090174

    const/16 v3, 0x8

    const v6, 0x7f09017f

    const v5, 0x7f09017e

    const/4 v4, 0x0

    .line 85
    const-string v0, "Tango.EmailVerificationActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    if-nez p1, :cond_0

    .line 88
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 186
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 183
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 94
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationRequiredEvent;

    .line 95
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$EmailValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$EmailVerificationPayload;

    .line 96
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$EmailVerificationPayload;->getIsPhone()Z

    move-result v1

    .line 97
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$EmailVerificationPayload;->getEmail()Ljava/lang/String;

    move-result-object v2

    .line 98
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$EmailVerificationPayload;->getNormalizedNumber()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 101
    :goto_1
    sget-object v3, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_EMAIL:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    iput-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 102
    iget-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 104
    iget-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(I)V

    .line 105
    iget-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(I)V

    .line 106
    iget-object v3, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v2, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_WhatToDoLabel:Landroid/widget/TextView;

    const v3, 0x7f090175

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 108
    iget-object v2, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_notReceiveLabel:Landroid/widget/TextView;

    const v3, 0x7f09017a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 109
    if-nez v1, :cond_1

    invoke-static {}, Lcom/sgiggle/iphelper/IpHelper;->isSmartPhone()Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    const v1, 0x7f09017b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    move v0, v4

    .line 99
    goto :goto_1

    .line 116
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 122
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$PushValidationRequiredEvent;

    .line 123
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$PushValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BoolPayload;

    .line 124
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BoolPayload;->getValue()Z

    move-result v0

    .line 126
    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_PUSH:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    iput-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 127
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 128
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 130
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(I)V

    .line 131
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_WhatToDoLabel:Landroid/widget/TextView;

    const v2, 0x7f090178

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 132
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_notReceiveLabel:Landroid/widget/TextView;

    const v2, 0x7f09017c

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 134
    if-eqz v0, :cond_4

    .line 136
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    const v1, 0x7f090176

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    const v1, 0x7f09017b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 141
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    const v1, 0x7f090177

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    const v1, 0x7f09017d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 150
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSValidationRequiredEvent;

    .line 151
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SMSValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSVerificationPayload;

    .line 152
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSVerificationPayload;->getNormalizedNumber()Ljava/lang/String;

    move-result-object v0

    .line 154
    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_BY_SMS:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    iput-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 155
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 156
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 157
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setText(I)V

    .line 158
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(I)V

    .line 159
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_WhatToDoLabel:Landroid/widget/TextView;

    const v1, 0x7f090175

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 161
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_notReceiveLabel:Landroid/widget/TextView;

    const v1, 0x7f09017a

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 162
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 166
    :sswitch_3
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySMSSentEvent;

    .line 167
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySMSSentEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;

    .line 168
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;->getText()Ljava/lang/String;

    move-result-object v0

    .line 170
    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->VERIFICATION_DEVICE_SMS_SENT:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    iput-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    .line 171
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 173
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    const v2, 0x7f09001f

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 174
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    const v2, 0x7f090181

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 175
    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_WhatToDoLabel:Landroid/widget/TextView;

    const v1, 0x7f090182

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 177
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_notReceiveLabel:Landroid/widget/TextView;

    const v1, 0x7f090180

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 178
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    .line 92
    nop

    :sswitch_data_0
    .sparse-switch
        0x88ec -> :sswitch_0
        0x88ee -> :sswitch_1
        0x88f0 -> :sswitch_2
        0x892f -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sgiggle/production/EmailVerificationActivity;->broadCastNoChange()V

    .line 256
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    .line 257
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 215
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 216
    const-string v1, "Tango.EmailVerificationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick(View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    packed-switch v0, :pswitch_data_0

    .line 243
    const-string v1, "Tango.EmailVerificationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick: unexpected click: View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 220
    :pswitch_0
    sget-object v1, Lcom/sgiggle/production/EmailVerificationActivity$2;->$SwitchMap$com$sgiggle$production$EmailVerificationActivity$ViewMode:[I

    iget-object v2, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_viewMode:Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;

    invoke-virtual {v2}, Lcom/sgiggle/production/EmailVerificationActivity$ViewMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 237
    const-string v1, "Tango.EmailVerificationActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick: unexpected mode: View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 224
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserInputedCodeMessage;

    iget-object v1, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserInputedCodeMessage;-><init>(Ljava/lang/String;)V

    .line 230
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 234
    :pswitch_2
    invoke-virtual {p0}, Lcom/sgiggle/production/EmailVerificationActivity;->broadCastNoChange()V

    goto :goto_0

    .line 218
    nop

    :pswitch_data_0
    .packed-switch 0x7f0a00a3
        :pswitch_0
    .end packed-switch

    .line 220
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 53
    const-string v0, "Tango.EmailVerificationActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f030024

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->setContentView(I)V

    .line 57
    const v0, 0x7f0a009e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_sentLabel:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0a009f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_userInfoLabel:Landroid/widget/TextView;

    .line 59
    const v0, 0x7f0a00a0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_WhatToDoLabel:Landroid/widget/TextView;

    .line 60
    const v0, 0x7f0a00a2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_verificationCodeEditText:Landroid/widget/EditText;

    .line 62
    const v0, 0x7f0a00a3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    .line 63
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_submitButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_notReceiveLabel:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 67
    iget-object v0, p0, Lcom/sgiggle/production/EmailVerificationActivity;->m_switchModeLink:Landroid/widget/TextView;

    new-instance v1, Lcom/sgiggle/production/EmailVerificationActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/EmailVerificationActivity$1;-><init>(Lcom/sgiggle/production/EmailVerificationActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setEmailVerificationActivityInstance(Lcom/sgiggle/production/EmailVerificationActivity;)V

    .line 75
    invoke-virtual {p0}, Lcom/sgiggle/production/EmailVerificationActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/EmailVerificationActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 76
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 200
    const-string v0, "Tango.EmailVerificationActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 202
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setEmailVerificationActivityInstance(Lcom/sgiggle/production/EmailVerificationActivity;)V

    .line 203
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 190
    const-string v0, "Tango.EmailVerificationActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 192
    invoke-virtual {p0}, Lcom/sgiggle/production/EmailVerificationActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setEmailVerificationActivityInstance(Lcom/sgiggle/production/EmailVerificationActivity;)V

    .line 196
    :cond_0
    return-void
.end method

.method protected onSwitchModeClicked()V
    .locals 3

    .prologue
    .line 249
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserNoCodeMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationUserNoCodeMessage;-><init>()V

    .line 250
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 251
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method
