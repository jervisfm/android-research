.class public Lcom/sgiggle/production/FacebookLikePageActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "FacebookLikePageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/FacebookLikePageActivity$1;,
        Lcom/sgiggle/production/FacebookLikePageActivity$MyWebViewClient;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.FacebookLikePageActivity"


# instance fields
.field private m_progressView:Landroid/view/ViewGroup;

.field private m_webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 50
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/FacebookLikePageActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/FacebookLikePageActivity;)Landroid/view/ViewGroup;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_progressView:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelFacebookLikeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelFacebookLikeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 70
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 27
    const-string v0, "Tango.FacebookLikePageActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const v0, 0x7f030058

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/FacebookLikePageActivity;->setContentView(I)V

    .line 32
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/FacebookLikePageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_progressView:Landroid/view/ViewGroup;

    .line 33
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/FacebookLikePageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_webView:Landroid/webkit/WebView;

    .line 36
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/FacebookLikePageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 37
    invoke-virtual {p0}, Lcom/sgiggle/production/FacebookLikePageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 40
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/sgiggle/production/FacebookLikePageActivity$MyWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/FacebookLikePageActivity$MyWebViewClient;-><init>(Lcom/sgiggle/production/FacebookLikePageActivity;Lcom/sgiggle/production/FacebookLikePageActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 44
    iget-object v0, p0, Lcom/sgiggle/production/FacebookLikePageActivity;->m_webView:Landroid/webkit/WebView;

    const-string v1, "http://www.facebook.com/TangoMe"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 74
    const-string v0, "Tango.FacebookLikePageActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 76
    return-void
.end method
