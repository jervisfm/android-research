.class public abstract Lcom/sgiggle/production/FragmentActivityBase;
.super Landroid/support/v4/app/FragmentActivity;
.source "FragmentActivityBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "Tango.FragmentActivityBase"

.field private static final VDBG:Z = true


# instance fields
.field protected m_firstMessage:Lcom/sgiggle/messaging/Message;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 24
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    const-string v0, "Tango.FragmentActivityBase"

    const-string v1, "FragmentActivityBase()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    return-void

    .line 25
    :catch_0
    move-exception v0

    .line 27
    const-string v1, "Tango.FragmentActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected finishIfResumedAfterKilled()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected getFirstMessage()Lcom/sgiggle/messaging/Message;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    return-object v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 113
    const-string v0, "Tango.FragmentActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    return-void
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 105
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 50
    const-string v0, "Tango.FragmentActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 54
    invoke-virtual {p0}, Lcom/sgiggle/production/FragmentActivityBase;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    .line 55
    if-nez v0, :cond_1

    .line 56
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/FragmentActivityBase;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 57
    const-string v0, "Tango.FragmentActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(): First time created: message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/FragmentActivityBase;->shouldOnCreateIntentHaveMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/FragmentActivityBase;->finishIfResumedAfterKilled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "Tango.FragmentActivityBase"

    const-string v1, "onCreate: activity was likely resumed after being killed, notifying TangoApp."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 69
    :cond_0
    return-void

    .line 59
    :cond_1
    check-cast v0, Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;

    .line 60
    iget-object v0, v0, Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v0, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 61
    const-string v0, "Tango.FragmentActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(): Restore activity: message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .parameter

    .prologue
    .line 91
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 92
    const-string v1, "Tango.FragmentActivityBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent(): Message = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/FragmentActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 96
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 125
    const-string v0, "Tango.FragmentActivityBase"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 127
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 128
    return-void
.end method

.method public onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 38
    const-string v0, "Tango.FragmentActivityBase"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRetainCustomNonConfigurationInstance(): message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    new-instance v0, Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;

    invoke-direct {v0}, Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;-><init>()V

    .line 40
    iget-object v1, p0, Lcom/sgiggle/production/FragmentActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v1, v0, Lcom/sgiggle/production/FragmentActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    .line 41
    return-object v0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 118
    const-string v0, "Tango.FragmentActivityBase"

    const-string v1, "onUserLeaveHint()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onUserLeaveHint()V

    .line 120
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 121
    return-void
.end method

.method protected shouldOnCreateIntentHaveMessage()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method
