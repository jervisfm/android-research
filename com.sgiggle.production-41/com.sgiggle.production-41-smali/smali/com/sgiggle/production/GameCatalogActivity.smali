.class public Lcom/sgiggle/production/GameCatalogActivity;
.super Lcom/sgiggle/production/GenericProductCatalogActivity;
.source "GameCatalogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;-><init>()V

    return-void
.end method

.method private dismissNewBadge()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;

    const-string v1, "product.category.game"

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;-><init>(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 50
    return-void
.end method


# virtual methods
.method protected getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f090104

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GameCatalogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 15
    if-nez p1, :cond_0

    .line 29
    :goto_0
    return-void

    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 28
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 21
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    .line 22
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/GameCatalogActivity;->onNewProductCatalogPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)V

    .line 23
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAllCached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/GameCatalogActivity;->dismissNewBadge()V

    goto :goto_1

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x89e4
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelGameCatalogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelGameCatalogMessage;-><init>()V

    .line 39
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 40
    return-void
.end method

.method protected showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 3
    .parameter

    .prologue
    .line 32
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 34
    return-void
.end method
