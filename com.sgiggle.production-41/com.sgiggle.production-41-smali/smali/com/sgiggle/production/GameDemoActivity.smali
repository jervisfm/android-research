.class public Lcom/sgiggle/production/GameDemoActivity;
.super Lcom/sgiggle/production/GenericProductDemoActivity;
.source "GameDemoActivity.java"


# instance fields
.field private m_gameStarted:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getBottomBubbleTextResId()I
    .locals 1

    .prologue
    .line 83
    const v0, 0x7f0901a4

    return v0
.end method

.method protected getRootView()I
    .locals 1

    .prologue
    .line 79
    const v0, 0x7f030028

    return v0
.end method

.method protected getUpperBubbleTextResId()I
    .locals 1

    .prologue
    .line 87
    const v0, 0x7f0901a5

    return v0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 25
    if-nez p1, :cond_0

    .line 39
    :goto_0
    return-void

    .line 27
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    .line 29
    packed-switch v1, :pswitch_data_0

    .line 38
    :goto_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 31
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoEvent;

    move-object v1, v0

    .line 32
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameDemoEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/GameDemoActivity;->onNewProductDetailPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)V

    goto :goto_1

    .line 29
    :pswitch_data_0
    .packed-switch 0x89e5
        :pswitch_0
    .end packed-switch
.end method

.method isCafeViewFullScreen()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    return v0
.end method

.method onAnimationFinished()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_gameStarted:Z

    .line 63
    invoke-super {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onAnimationFinished()V

    .line 64
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_gameStarted:Z

    .line 57
    invoke-super {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onAnimationStarted()V

    .line 58
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 69
    iget-boolean v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_gameStarted:Z

    if-nez v0, :cond_0

    .line 70
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelGameDemoMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelGameDemoMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 72
    invoke-super {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onBackPressed()V

    .line 74
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_gameStarted:Z

    .line 75
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 17
    invoke-super {p0, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    iget-object v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeView;->setDeliverTouchToCafe(Z)V

    .line 20
    iget-object v0, p0, Lcom/sgiggle/production/GameDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setCenterSingleItem(Z)V

    .line 21
    return-void
.end method

.method protected play(Ljava/lang/String;J)V
    .locals 9
    .parameter
    .parameter

    .prologue
    .line 42
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetGodMode(Z)V

    .line 43
    const-string v0, "Surprise.Cafe"

    invoke-static {p1, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x0

    const-string v2, "Surprise.Cafe"

    const-string v3, "Main"

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    move-object v1, p1

    move-wide v6, p2

    invoke-static/range {v0 .. v8}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)I

    .line 47
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;

    iget-object v1, p0, Lcom/sgiggle/production/GameDemoActivity;->m_productMarketId:Ljava/lang/String;

    invoke-direct {v0, p2, p3, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;-><init>(JLjava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/production/GameDemoActivity;->onAnimationStarted()V

    .line 52
    return-void
.end method
