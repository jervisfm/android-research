.class public Lcom/sgiggle/production/GameInCallActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "GameInCallActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.GameInCallActivity"

.field private static s_instance:Lcom/sgiggle/production/GameInCallActivity;


# instance fields
.field private m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

.field private m_gameId:J

.field private m_gamePath:Ljava/lang/String;

.field private m_resumed:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    return-void
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/GameInCallActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    sget-object v0, Lcom/sgiggle/production/GameInCallActivity;->s_instance:Lcom/sgiggle/production/GameInCallActivity;

    if-ne v0, p0, :cond_0

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/GameInCallActivity;->s_instance:Lcom/sgiggle/production/GameInCallActivity;

    .line 39
    :cond_0
    return-void
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sgiggle/production/GameInCallActivity;->s_instance:Lcom/sgiggle/production/GameInCallActivity;

    return-object v0
.end method

.method private handleGame(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V
    .locals 3
    .parameter

    .prologue
    .line 80
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gamePath:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gameId:J

    .line 82
    iget-boolean v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_resumed:Z

    if-eqz v0, :cond_0

    .line 83
    iget-wide v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gameId:J

    iget-object v2, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gamePath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sgiggle/production/GameInCallActivity;->playGame(JLjava/lang/String;)V

    .line 85
    :cond_0
    return-void
.end method

.method private playGame(JLjava/lang/String;)V
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 103
    iget-object v1, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gamePath:Ljava/lang/String;

    const-string v2, "Surprise.Cafe"

    invoke-static {v1, v2}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v2, "Surprise.Cafe"

    const-string v3, "Main"

    const-wide/16 v4, 0x0

    move-object v1, p3

    move-wide v6, p1

    move v8, v0

    invoke-static/range {v0 .. v8}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JJZ)I

    .line 106
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/GameInCallActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    sput-object p0, Lcom/sgiggle/production/GameInCallActivity;->s_instance:Lcom/sgiggle/production/GameInCallActivity;

    .line 34
    return-void
.end method


# virtual methods
.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 58
    if-nez p1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 63
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 72
    :sswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/GameInCallActivity;->onBackPressed()V

    goto :goto_0

    .line 65
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameInCallEvent;

    .line 66
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameInCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodBundleList()Ljava/util/List;

    move-result-object v0

    .line 67
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 68
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/GameInCallActivity;->handleGame(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)V

    goto :goto_0

    .line 63
    nop

    :sswitch_data_0
    .sparse-switch
        0x897a -> :sswitch_0
        0x89e6 -> :sswitch_1
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    .line 127
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 129
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 43
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/sgiggle/production/GameInCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 45
    invoke-virtual {p0}, Lcom/sgiggle/production/GameInCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 46
    const v0, 0x7f030029

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GameInCallActivity;->setContentView(I)V

    .line 47
    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GameInCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeView;

    iput-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeView;->setZOrderOnTop(Z)V

    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeView;->setDeliverTouchToCafe(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/production/GameInCallActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GameInCallActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 53
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->enableKeyguard()V

    .line 54
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 120
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 121
    invoke-static {p0}, Lcom/sgiggle/production/GameInCallActivity;->clearRunningInstance(Lcom/sgiggle/production/GameInCallActivity;)V

    .line 122
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 111
    iget-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onPause()V

    .line 112
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 113
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 114
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeEngine()V

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_resumed:Z

    .line 116
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 90
    invoke-static {p0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->InitEngine(Landroid/content/Context;)V

    .line 91
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetCallbacks()V

    .line 92
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetGodMode(Z)V

    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gamePath:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 94
    iget-wide v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gameId:J

    iget-object v2, p0, Lcom/sgiggle/production/GameInCallActivity;->m_gamePath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/sgiggle/production/GameInCallActivity;->playGame(JLjava/lang/String;)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onResume()V

    .line 97
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 98
    invoke-static {p0}, Lcom/sgiggle/production/GameInCallActivity;->setRunningInstance(Lcom/sgiggle/production/GameInCallActivity;)V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/GameInCallActivity;->m_resumed:Z

    .line 100
    return-void
.end method

.method protected onUserLeaveHint()V
    .locals 4

    .prologue
    .line 133
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameModeOffMessage;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 135
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onUserLeaveHint()V

    .line 136
    return-void
.end method
