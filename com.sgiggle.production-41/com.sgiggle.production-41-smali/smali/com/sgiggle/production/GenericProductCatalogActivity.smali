.class public abstract Lcom/sgiggle/production/GenericProductCatalogActivity;
.super Lcom/sgiggle/production/BillingSupportBaseActivity;
.source "GenericProductCatalogActivity.java"

# interfaces
.implements Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static s_instance:Lcom/sgiggle/production/GenericProductCatalogActivity;


# instance fields
.field protected m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

.field private m_footer:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;-><init>()V

    return-void
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/GenericProductCatalogActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    sget-object v0, Lcom/sgiggle/production/GenericProductCatalogActivity;->s_instance:Lcom/sgiggle/production/GenericProductCatalogActivity;

    if-ne v0, p0, :cond_0

    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/GenericProductCatalogActivity;->s_instance:Lcom/sgiggle/production/GenericProductCatalogActivity;

    .line 39
    :cond_0
    return-void
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sgiggle/production/GenericProductCatalogActivity;->s_instance:Lcom/sgiggle/production/GenericProductCatalogActivity;

    return-object v0
.end method

.method private hideErrorMessage()V
    .locals 2

    .prologue
    .line 131
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 132
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 133
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/GenericProductCatalogActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    sput-object p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->s_instance:Lcom/sgiggle/production/GenericProductCatalogActivity;

    .line 34
    return-void
.end method

.method private showErrorMessage()V
    .locals 2

    .prologue
    .line 124
    const v0, 0x7f0a015b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 125
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->getErrorMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 127
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 128
    return-void
.end method


# virtual methods
.method public confirmPurchaseFailed()V
    .locals 0

    .prologue
    .line 160
    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const v0, 0x7f090102

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getFooterView()Landroid/view/View;
    .locals 3

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03005f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 66
    return-object v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->onBackPressed()V

    .line 155
    return-void
.end method

.method public onClick(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 87
    const v0, 0x7f0a0029

    if-ne p1, v0, :cond_1

    .line 88
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/GenericProductCatalogActivity;->purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    const v0, 0x7f0a0033

    if-ne p1, v0, :cond_0

    .line 90
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/GenericProductCatalogActivity;->showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 43
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v0, 0x7f030061

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->setContentView(I)V

    .line 46
    new-instance v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setOnClickListener(Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;)V

    .line 49
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 50
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 52
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->getFooterView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_footer:Landroid/view/View;

    .line 53
    iget-object v1, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_footer:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 55
    iget-object v1, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 56
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 57
    const v1, 0x1020004

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 59
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 61
    invoke-static {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->setRunningInstance(Lcom/sgiggle/production/GenericProductCatalogActivity;)V

    .line 62
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 81
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onDestroy()V

    .line 82
    invoke-static {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->clearRunningInstance(Lcom/sgiggle/production/GenericProductCatalogActivity;)V

    .line 83
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 99
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 100
    return-void
.end method

.method protected onNewProductCatalogPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)V
    .locals 2
    .parameter

    .prologue
    .line 103
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->showErrorMessage()V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryList()Ljava/util/List;

    move-result-object v0

    .line 109
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 110
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->hideErrorMessage()V

    .line 112
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setProducts(Ljava/util/List;)V

    .line 113
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAllCached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const v0, 0x7f0a0035

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 115
    iget-object v1, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_footer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 76
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPause()V

    .line 77
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onResume()V

    .line 72
    return-void
.end method

.method public purchaseProcessed()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 1
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setBillingSupported(Z)V

    .line 147
    if-nez p1, :cond_0

    .line 148
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductCatalogActivity;->showDialog(I)V

    .line 150
    :cond_0
    return-void
.end method

.method public setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductCatalogActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->notifyDataSetChanged()V

    .line 138
    return-void
.end method

.method protected abstract showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
.end method
