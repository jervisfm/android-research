.class public abstract Lcom/sgiggle/production/GenericProductDemoActivity;
.super Lcom/sgiggle/production/BillingSupportBaseActivity;
.source "GenericProductDemoActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static s_instance:Lcom/sgiggle/production/GenericProductDemoActivity;


# instance fields
.field private bottomBubble:Landroid/view/View;

.field private m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

.field private m_btnClicked:Z

.field private m_buy:Landroid/widget/Button;

.field protected m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

.field m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

.field protected m_productMarketId:Ljava/lang/String;

.field m_progressDialog:Landroid/app/ProgressDialog;

.field private m_purchased:Z

.field protected m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

.field private m_selectorIn:Landroid/view/animation/Animation;

.field private m_wand:Landroid/widget/ImageView;

.field private wandBubble:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;-><init>()V

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_purchased:Z

    .line 55
    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_btnClicked:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/GenericProductDemoActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->bottomBubble:Landroid/view/View;

    return-object v0
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/GenericProductDemoActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 69
    sget-object v0, Lcom/sgiggle/production/GenericProductDemoActivity;->s_instance:Lcom/sgiggle/production/GenericProductDemoActivity;

    if-ne v0, p0, :cond_0

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/GenericProductDemoActivity;->s_instance:Lcom/sgiggle/production/GenericProductDemoActivity;

    .line 71
    :cond_0
    return-void
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sgiggle/production/GenericProductDemoActivity;->s_instance:Lcom/sgiggle/production/GenericProductDemoActivity;

    return-object v0
.end method

.method private hideWand()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 164
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->wandBubble:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_wand:Landroid/widget/ImageView;

    const v1, 0x7f0200df

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->setVisibility(I)V

    .line 168
    return-void
.end method

.method private markProductPurchased()V
    .locals 2

    .prologue
    .line 315
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_purchased:Z

    .line 316
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_buy:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 317
    return-void
.end method

.method private onBuyClick()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 246
    return-void
.end method

.method private onWandClick()V
    .locals 4

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->hideWand()V

    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selectorIn:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sgiggle/production/GenericProductDemoActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/GenericProductDemoActivity$1;-><init>(Lcom/sgiggle/production/GenericProductDemoActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 263
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selectorIn:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 264
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_DEMO:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_btnClicked:Z

    .line 267
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/GenericProductDemoActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    sput-object p0, Lcom/sgiggle/production/GenericProductDemoActivity;->s_instance:Lcom/sgiggle/production/GenericProductDemoActivity;

    .line 66
    return-void
.end method


# virtual methods
.method public confirmPurchaseFailed()V
    .locals 0

    .prologue
    .line 330
    return-void
.end method

.method protected getBottomBubbleTextResId()I
    .locals 1

    .prologue
    .line 333
    const v0, 0x7f09011a

    return v0
.end method

.method protected getRootView()I
    .locals 1

    .prologue
    .line 74
    const v0, 0x7f03002a

    return v0
.end method

.method protected getUpperBubbleTextResId()I
    .locals 1

    .prologue
    .line 337
    const v0, 0x7f090119

    return v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onBackPressed()V

    .line 199
    return-void
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 112
    if-nez p1, :cond_0

    .line 125
    :goto_0
    return-void

    .line 115
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 117
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 119
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onAnimationComplete()V

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x899d
        :pswitch_0
    .end packed-switch
.end method

.method hideProgressDialog()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 299
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 301
    :cond_0
    return-void
.end method

.method isCafeViewFullScreen()Z
    .locals 1

    .prologue
    .line 341
    const/4 v0, 0x0

    return v0
.end method

.method protected onAnimationComplete()V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEnabled(Z)V

    .line 225
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->clearHighlight()V

    .line 228
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->isCafeViewFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const v0, 0x7f0a0083

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 231
    :cond_0
    return-void
.end method

.method onAnimationFinished()V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setEnabled(Z)V

    .line 203
    return-void
.end method

.method protected onAnimationStarted()V
    .locals 2

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->isCafeViewFullScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const v0, 0x7f0a0083

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 221
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_btnClicked:Z

    if-nez v0, :cond_0

    .line 236
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_btnClicked:Z

    .line 237
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a002b

    if-ne v0, v1, :cond_1

    .line 238
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onWandClick()V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0029

    if-ne v0, v1, :cond_0

    .line 240
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onBuyClick()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0a0022

    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 79
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/GenericProductDemoActivity;->requestWindowFeature(I)Z

    .line 81
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->getRootView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->setContentView(I)V

    .line 83
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/HorizontalListView;

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    .line 84
    new-instance v0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEmptyCount(I)V

    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 89
    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeView;

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeView;->setZOrderOnTop(Z)V

    .line 92
    const v0, 0x7f0a002b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_wand:Landroid/widget/ImageView;

    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v0, 0x7f0a002c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->wandBubble:Landroid/view/View;

    .line 95
    const v0, 0x7f0a002d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->bottomBubble:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->wandBubble:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->bottomBubble:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 99
    const v0, 0x7f0a0029

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_buy:Landroid/widget/Button;

    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v0, 0x7f0a002a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 103
    const v0, 0x7f0a0027

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 105
    const v0, 0x7f040008

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selectorIn:Landroid/view/animation/Animation;

    .line 107
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 108
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onDestroy()V

    .line 193
    invoke-static {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->clearRunningInstance(Lcom/sgiggle/production/GenericProductDemoActivity;)V

    .line 194
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 272
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 273
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v1

    .line 274
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v2

    .line 275
    invoke-virtual {p0, v1, v2, v3}, Lcom/sgiggle/production/GenericProductDemoActivity;->play(Ljava/lang/String;J)V

    .line 276
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEnabled(Z)V

    .line 277
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {v0, p3}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setHighlight(I)V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->bottomBubble:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 280
    return-void
.end method

.method onNewProductDetailPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)V
    .locals 3
    .parameter

    .prologue
    .line 128
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getVgoodBundleList()Ljava/util/List;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setData(Ljava/util/List;Z)V

    .line 131
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getShowWand()Z

    move-result v0

    .line 132
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->showWand(Z)V

    .line 134
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 135
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_productMarketId:Ljava/lang/String;

    .line 136
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_product:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getLabel()Ljava/lang/String;

    move-result-object v1

    .line 137
    const v0, 0x7f0a0028

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_buy:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    iget-object v0, v0, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 142
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v0, v1, :cond_1

    .line 143
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->markProductPurchased()V

    .line 146
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;->getAllCached()Z

    move-result v0

    if-nez v0, :cond_2

    .line 147
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->showProgressDialog()V

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->hideProgressDialog()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPause()V

    .line 184
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onPause()V

    .line 185
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 186
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 187
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeEngine()V

    .line 188
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_btnClicked:Z

    .line 173
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onResume()V

    .line 174
    invoke-static {p0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->InitEngine(Landroid/content/Context;)V

    .line 175
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetCallbacks()V

    .line 176
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onResume()V

    .line 177
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 178
    invoke-static {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->setRunningInstance(Lcom/sgiggle/production/GenericProductDemoActivity;)V

    .line 179
    return-void
.end method

.method protected play(Ljava/lang/String;J)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 206
    const-string v0, "Surprise.Cafe"

    invoke-static {p1, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const-wide/16 v1, 0x0

    const/4 v5, 0x1

    move-object v0, p1

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(Ljava/lang/String;JJZ)I

    .line 209
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;

    iget-object v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_productMarketId:Ljava/lang/String;

    invoke-direct {v0, p2, p3, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodDemoAnimationTrackingMessage;-><init>(JLjava/lang/String;)V

    .line 211
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 213
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onAnimationStarted()V

    .line 214
    return-void
.end method

.method public purchaseProcessed()V
    .locals 0

    .prologue
    .line 312
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 2
    .parameter

    .prologue
    .line 321
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_buy:Landroid/widget/Button;

    if-eqz p1, :cond_1

    iget-boolean v1, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_purchased:Z

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 322
    if-nez p1, :cond_0

    .line 323
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/GenericProductDemoActivity;->showDialog(I)V

    .line 325
    :cond_0
    return-void

    .line 321
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 305
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne p2, v0, :cond_0

    .line 306
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->markProductPurchased()V

    .line 308
    :cond_0
    return-void
.end method

.method showProgressDialog()V
    .locals 2

    .prologue
    .line 283
    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->hideProgressDialog()V

    .line 284
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 285
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f090123

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/GenericProductDemoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 286
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 287
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/sgiggle/production/GenericProductDemoActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/GenericProductDemoActivity$2;-><init>(Lcom/sgiggle/production/GenericProductDemoActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 293
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 294
    return-void
.end method

.method showWand(Z)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 153
    if-eqz p1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_wand:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 155
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->wandBubble:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->wandBubble:Landroid/view/View;

    const v1, 0x7f0a0023

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->getUpperBubbleTextResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/GenericProductDemoActivity;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setVisibility(I)V

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->hideWand()V

    goto :goto_0
.end method
