.class public Lcom/sgiggle/production/InviteActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "InviteActivity.java"


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 18
    const-string v0, "Tango.InviteActivity"

    iput-object v0, p0, Lcom/sgiggle/production/InviteActivity;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 7
    .parameter

    .prologue
    const v6, 0x7f0a00b1

    const/4 v5, 0x1

    .line 40
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 42
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;

    .line 43
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    .line 45
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->hasSpecifiedInvitePrompt()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSpecifiedInvitePrompt()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 47
    :goto_1
    const v1, 0x7f0a00ae

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 48
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v1

    .line 51
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSns()Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string v0, "Tango.InviteActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sns type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const v0, 0x7f0a00af

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 54
    new-instance v3, Lcom/sgiggle/production/InviteActivity$1;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/InviteActivity$1;-><init>(Lcom/sgiggle/production/InviteActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    if-ne v1, v3, :cond_2

    .line 61
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 67
    :goto_2
    const v0, 0x7f0a00b0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 68
    new-instance v3, Lcom/sgiggle/production/InviteActivity$2;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/InviteActivity$2;-><init>(Lcom/sgiggle/production/InviteActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    if-ne v1, v3, :cond_3

    .line 76
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 81
    :goto_3
    const-string v0, "Tango.InviteActivity"

    const-string v1, "try to show Weibo button"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    const-string v0, "weibo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 84
    const-string v0, "Tango.InviteActivity"

    const-string v1, "show Weibo button"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {p0, v6}, Lcom/sgiggle/production/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 86
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 87
    new-instance v1, Lcom/sgiggle/production/InviteActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/InviteActivity$3;-><init>(Lcom/sgiggle/production/InviteActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 45
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto/16 :goto_1

    .line 64
    :cond_2
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isEmailIntentAvailable(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_2

    .line 79
    :cond_3
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_3

    .line 96
    :cond_4
    invoke-virtual {p0, v6}, Lcom/sgiggle/production/InviteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 97
    invoke-virtual {v0}, Landroid/widget/Button;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 98
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x88df
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 22
    const-string v0, "Tango.InviteActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 23
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 24
    const v0, 0x7f03002c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteActivity;->setContentView(I)V

    .line 25
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 29
    const-string v0, "Tango.InviteActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 31
    return-void
.end method
