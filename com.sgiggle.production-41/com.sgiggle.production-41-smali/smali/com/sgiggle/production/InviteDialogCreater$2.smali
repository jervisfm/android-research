.class Lcom/sgiggle/production/InviteDialogCreater$2;
.super Ljava/lang/Object;
.source "InviteDialogCreater.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/InviteDialogCreater;->showDialogInviteToTango(Ljava/util/List;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/InviteDialogCreater;

.field final synthetic val$contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field final synthetic val$inviteMultiplePos:I

.field final synthetic val$sendVideomailEntryPos:I

.field final synthetic val$validInviteContacts:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/InviteDialogCreater;ILcom/sgiggle/xmpp/SessionMessages$Contact;ILjava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->this$0:Lcom/sgiggle/production/InviteDialogCreater;

    iput p2, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$sendVideomailEntryPos:I

    iput-object p3, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iput p4, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$inviteMultiplePos:I

    iput-object p5, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$validInviteContacts:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 216
    iget-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->this$0:Lcom/sgiggle/production/InviteDialogCreater;

    #setter for: Lcom/sgiggle/production/InviteDialogCreater;->m_dialogEntryChosen:Z
    invoke-static {v0, v2}, Lcom/sgiggle/production/InviteDialogCreater;->access$002(Lcom/sgiggle/production/InviteDialogCreater;Z)Z

    .line 218
    iget v0, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$sendVideomailEntryPos:I

    if-ne p2, v0, :cond_0

    .line 220
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)V

    .line 221
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 232
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 233
    return-void

    .line 222
    :cond_0
    iget v0, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$inviteMultiplePos:I

    if-ne p2, v0, :cond_1

    .line 224
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 227
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 230
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->this$0:Lcom/sgiggle/production/InviteDialogCreater;

    iget-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$2;->val$validInviteContacts:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #calls: Lcom/sgiggle/production/InviteDialogCreater;->sendInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    invoke-static {v1, v0}, Lcom/sgiggle/production/InviteDialogCreater;->access$100(Lcom/sgiggle/production/InviteDialogCreater;Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    goto :goto_0
.end method
