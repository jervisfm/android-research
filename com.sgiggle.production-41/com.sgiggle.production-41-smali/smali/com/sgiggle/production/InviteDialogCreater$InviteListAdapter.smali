.class Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "InviteDialogCreater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/InviteDialogCreater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "InviteListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

.field private mArray:[Ljava/lang/String;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLayoutViewResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;I[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 315
    invoke-direct {p0, p1, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 316
    iput-object p4, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mArray:[Ljava/lang/String;

    .line 317
    iput-object p2, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 318
    iput p3, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mLayoutViewResId:I

    .line 319
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 322
    if-nez p2, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mLayoutViewResId:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 324
    new-instance v0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;-><init>(Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;)V

    iput-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    .line 325
    iget-object v2, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    const v0, 0x7f0a007a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    .line 326
    iget-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    .line 330
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    iget-object v1, v1, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;->name:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->mArray:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    return-object v0

    .line 328
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    iput-object v0, p0, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;->holder:Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter$ViewHolder;

    move-object v0, p2

    goto :goto_0
.end method
