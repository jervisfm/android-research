.class public Lcom/sgiggle/production/InviteDialogCreater;
.super Ljava/lang/Object;
.source "InviteDialogCreater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/InviteDialogCreater$4;,
        Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialogShowing:Z

.field private mInflater:Landroid/view/LayoutInflater;

.field private m_dialogEntryChosen:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    .line 41
    iput-object p1, p0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    .line 42
    iput-object p2, p0, Lcom/sgiggle/production/InviteDialogCreater;->mInflater:Landroid/view/LayoutInflater;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/InviteDialogCreater;)Z
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteDialogCreater;->m_dialogEntryChosen:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sgiggle/production/InviteDialogCreater;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sgiggle/production/InviteDialogCreater;->m_dialogEntryChosen:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sgiggle/production/InviteDialogCreater;Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteDialogCreater;->sendInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    return-void
.end method

.method static synthetic access$202(Lcom/sgiggle/production/InviteDialogCreater;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    return p1
.end method

.method private addDismissListenerToDialog(Landroid/app/AlertDialog;)V
    .locals 1
    .parameter

    .prologue
    .line 243
    new-instance v0, Lcom/sgiggle/production/InviteDialogCreater$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/InviteDialogCreater$3;-><init>(Lcom/sgiggle/production/InviteDialogCreater;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteDialogCreater;->m_dialogEntryChosen:Z

    .line 255
    return-void
.end method

.method private sendInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 6
    .parameter

    .prologue
    .line 259
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 261
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    .line 265
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 292
    :goto_0
    return-void

    .line 270
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 271
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    .line 272
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v3

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryname()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountrycodenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryid()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0
.end method

.method private showDialogForInviteeNoEmailNoNumber()V
    .locals 3

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    if-eqz v0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 54
    const v1, 0x7f090066

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(I)V

    .line 55
    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    const v2, 0x7f090067

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 56
    iget-object v1, p0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    const v2, 0x7f090020

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/InviteDialogCreater$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/InviteDialogCreater$1;-><init>(Lcom/sgiggle/production/InviteDialogCreater;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 62
    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteDialogCreater;->addDismissListenerToDialog(Landroid/app/AlertDialog;)V

    .line 63
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    goto :goto_0
.end method

.method private showDialogInviteToTango(Ljava/util/List;Z)V
    .locals 24
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 73
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    move v2, v0

    if-eqz v2, :cond_0

    .line 240
    :goto_0
    return-void

    .line 76
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-static {v2}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v3

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-static {v2}, Lcom/sgiggle/production/Utils;->isEmailIntentAvailable(Landroid/content/Context;)Z

    move-result v4

    .line 78
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->videomailSupported()Z

    move-result v5

    .line 80
    new-instance v8, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-direct {v8, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    const v2, 0x7f090066

    invoke-virtual {v8, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 83
    const/4 v2, 0x1

    invoke-virtual {v8, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 85
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f090068

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f090069

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f09006a

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f09006b

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 89
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f09006c

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 90
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f09006d

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 92
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 93
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 97
    const/4 v2, 0x0

    .line 98
    const/4 v15, 0x0

    .line 99
    const/16 v16, 0x0

    .line 101
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move-object/from16 v18, v2

    move-object/from16 v23, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v23

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 102
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v19

    .line 103
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v20

    .line 105
    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v21

    if-nez v21, :cond_2

    .line 106
    if-eqz v4, :cond_1

    .line 107
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v19, v20, v21

    move-object v0, v13

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object v0, v14

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_1
    if-nez v15, :cond_e

    move-object/from16 v2, v19

    move-object/from16 v15, v16

    move-object/from16 v16, v18

    :goto_2
    move-object/from16 v18, v16

    move-object/from16 v16, v15

    move-object v15, v2

    .line 147
    goto :goto_1

    .line 115
    :cond_2
    if-eqz v3, :cond_e

    if-eqz v20, :cond_e

    invoke-virtual/range {v20 .. v20}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_e

    .line 119
    if-nez v18, :cond_3

    move-object/from16 v18, v20

    .line 123
    :cond_3
    sget-object v19, Lcom/sgiggle/production/InviteDialogCreater$4;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$PhoneType:[I

    invoke-virtual/range {v20 .. v20}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->ordinal()I

    move-result v21

    aget v19, v19, v21

    packed-switch v19, :pswitch_data_0

    move-object/from16 v19, v16

    move-object/from16 v16, v12

    .line 144
    :goto_3
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v20}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v20

    aput-object v20, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object v0, v14

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, v15

    move-object/from16 v16, v18

    move-object/from16 v15, v19

    goto :goto_2

    .line 128
    :pswitch_0
    if-nez v16, :cond_d

    move-object/from16 v16, v6

    move-object/from16 v19, v20

    .line 129
    goto :goto_3

    :pswitch_1
    move-object/from16 v19, v16

    move-object/from16 v16, v9

    .line 134
    goto :goto_3

    :pswitch_2
    move-object/from16 v19, v16

    move-object/from16 v16, v10

    .line 137
    goto :goto_3

    :pswitch_3
    move-object/from16 v19, v16

    move-object/from16 v16, v11

    .line 140
    goto :goto_3

    .line 150
    :cond_4
    if-eqz p2, :cond_8

    if-eqz v5, :cond_8

    if-nez v15, :cond_5

    if-eqz v3, :cond_8

    if-eqz v18, :cond_8

    :cond_5
    const/4 v2, 0x1

    .line 157
    :goto_4
    if-eqz v2, :cond_b

    .line 160
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    .line 163
    if-eqz p1, :cond_6

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 164
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 165
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 166
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 167
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 168
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 169
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 170
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 175
    :cond_6
    if-eqz v15, :cond_9

    .line 176
    invoke-virtual {v3, v15}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 183
    :cond_7
    :goto_5
    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    .line 186
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v3

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v4, v0

    const v5, 0x7f09006f

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v14, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v4

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v5, v0

    const v6, 0x7f09006e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v14, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v2

    move v6, v3

    .line 208
    :goto_6
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v14, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    .line 210
    new-instance v9, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v3, v0

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mInflater:Landroid/view/LayoutInflater;

    move-object v10, v0

    const v11, 0x7f030018

    invoke-direct {v9, v3, v10, v11, v2}, Lcom/sgiggle/production/InviteDialogCreater$InviteListAdapter;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;I[Ljava/lang/String;)V

    .line 213
    const/4 v10, 0x0

    new-instance v2, Lcom/sgiggle/production/InviteDialogCreater$2;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/sgiggle/production/InviteDialogCreater$2;-><init>(Lcom/sgiggle/production/InviteDialogCreater;ILcom/sgiggle/xmpp/SessionMessages$Contact;ILjava/util/List;)V

    invoke-virtual {v8, v9, v10, v2}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 236
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 237
    move-object/from16 v0, p0

    move-object v1, v2

    invoke-direct {v0, v1}, Lcom/sgiggle/production/InviteDialogCreater;->addDismissListenerToDialog(Landroid/app/AlertDialog;)V

    .line 238
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 239
    const/4 v2, 0x1

    move v0, v2

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/production/InviteDialogCreater;->mDialogShowing:Z

    goto/16 :goto_0

    .line 150
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 177
    :cond_9
    if-eqz v16, :cond_a

    .line 178
    move-object v0, v3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    goto :goto_5

    .line 179
    :cond_a
    if-eqz v18, :cond_7

    .line 180
    move-object v0, v3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    goto/16 :goto_5

    .line 195
    :cond_b
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_c

    .line 196
    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/production/InviteDialogCreater;->showDialogForInviteeNoEmailNoNumber()V

    goto/16 :goto_0

    .line 200
    :cond_c
    const/4 v2, -0x1

    .line 201
    const/4 v3, 0x0

    .line 204
    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v4

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/InviteDialogCreater;->mContext:Landroid/content/Context;

    move-object v5, v0

    const v6, 0x7f09006f

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v14, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v5, v3

    move v6, v4

    move v4, v2

    goto :goto_6

    :cond_d
    move-object/from16 v19, v16

    move-object/from16 v16, v6

    goto/16 :goto_3

    :cond_e
    move-object v2, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v18

    goto/16 :goto_2

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public doInvite2Tango(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)V
    .locals 3
    .parameter

    .prologue
    .line 300
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getContactsList()Ljava/util/List;

    move-result-object v0

    .line 301
    const/4 v1, 0x1

    .line 302
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasAccountValidated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 304
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getAccountValidated()Z

    move-result v1

    .line 306
    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/InviteDialogCreater;->showDialogInviteToTango(Ljava/util/List;Z)V

    .line 307
    return-void
.end method
