.class Lcom/sgiggle/production/InviteSelectionActivity$1;
.super Ljava/lang/Object;
.source "InviteSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/InviteSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/InviteSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/InviteSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 907
    iput-object p1, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 909
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$400(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    .line 923
    :goto_0
    return-void

    .line 914
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$400(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->isAllItemChecked()Z

    move-result v0

    if-nez v0, :cond_2

    move v1, v4

    .line 915
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$400(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->getDisplayedItems()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 916
    iget-boolean v3, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eq v3, v1, :cond_1

    .line 917
    iput-boolean v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 918
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    if-eqz v1, :cond_3

    move v3, v4

    :goto_3
    invoke-static {v0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->access$012(Lcom/sgiggle/production/InviteSelectionActivity;I)I

    goto :goto_2

    .line 914
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_1

    .line 918
    :cond_3
    const/4 v3, -0x1

    goto :goto_3

    .line 921
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$400(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->notifyDataSetChanged()V

    .line 922
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #calls: Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$300(Lcom/sgiggle/production/InviteSelectionActivity;)V

    goto :goto_0
.end method
