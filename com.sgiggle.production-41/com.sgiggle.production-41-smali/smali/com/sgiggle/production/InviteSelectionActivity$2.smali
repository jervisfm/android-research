.class Lcom/sgiggle/production/InviteSelectionActivity$2;
.super Ljava/lang/Object;
.source "InviteSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/InviteSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/InviteSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/InviteSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 926
    iput-object p1, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter

    .prologue
    .line 928
    .line 929
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$000(Lcom/sgiggle/production/InviteSelectionActivity;)I

    move-result v0

    if-nez v0, :cond_1

    .line 930
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    .line 931
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 986
    :cond_0
    :goto_0
    return-void

    .line 932
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_4

    .line 933
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 934
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$500(Lcom/sgiggle/production/InviteSelectionActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 935
    iget-boolean v3, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v3, :cond_2

    .line 937
    const-string v3, "Tango.InviteSelectionActivity"

    const-string v4, "onClick(): Send email :: email=%s; phonenumber=%s; firstname=%s; lastname=%s"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 938
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_middleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_nameSuffix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v0, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 948
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 950
    :cond_3
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerMessage;-><init>(Ljava/util/List;)V

    .line 951
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 952
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_0

    .line 954
    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 955
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$500(Lcom/sgiggle/production/InviteSelectionActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 956
    iget-boolean v3, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v3, :cond_6

    .line 958
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_middleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_nameSuffix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v3

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v4

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountrycodenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setCountryCode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v4

    iget-object v0, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 975
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    invoke-static {v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v2, :cond_8

    .line 977
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectedMessage;-><init>(Ljava/util/List;)V

    .line 984
    :goto_3
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 982
    :cond_8
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity$2;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;
    invoke-static {v2}, Lcom/sgiggle/production/InviteSelectionActivity;->access$600(Lcom/sgiggle/production/InviteSelectionActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectedMessage;-><init>(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_3
.end method
