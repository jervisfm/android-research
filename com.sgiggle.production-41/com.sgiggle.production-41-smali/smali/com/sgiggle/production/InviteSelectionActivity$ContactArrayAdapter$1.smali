.class Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter$1;
.super Ljava/lang/Object;
.source "InviteSelectionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 865
    iput-object p1, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 867
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 868
    if-eqz v0, :cond_1

    .line 869
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter$1;->this$0:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/InviteSelectionActivity;

    .line 870
    iget-boolean v2, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-nez v2, :cond_2

    move v2, v3

    :goto_0
    iput-boolean v2, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 871
    iget-boolean v2, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    invoke-static {v1, v2}, Lcom/sgiggle/production/InviteSelectionActivity;->access$012(Lcom/sgiggle/production/InviteSelectionActivity;I)I

    .line 872
    #getter for: Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    invoke-static {v1}, Lcom/sgiggle/production/InviteSelectionActivity;->access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-eq v2, v3, :cond_0

    #calls: Lcom/sgiggle/production/InviteSelectionActivity;->reachEmailSelectedLimit(Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z
    invoke-static {v1, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->access$200(Lcom/sgiggle/production/InviteSelectionActivity;Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 873
    :cond_0
    #calls: Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V
    invoke-static {v1}, Lcom/sgiggle/production/InviteSelectionActivity;->access$300(Lcom/sgiggle/production/InviteSelectionActivity;)V

    .line 878
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v2, v4

    .line 870
    goto :goto_0

    .line 871
    :cond_3
    const/4 v2, -0x1

    goto :goto_1

    .line 875
    :cond_4
    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_2
.end method
