.class Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;
.super Lcom/sgiggle/production/Utils$UIContact;
.source "InviteSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/InviteSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactItem"
.end annotation


# instance fields
.field public m_selected:Z

.field public m_subLabel:Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/production/InviteSelectionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 748
    iput-object p1, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 749
    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/Utils$UIContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 750
    return-void
.end method

.method constructor <init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 754
    iput-object p1, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->this$0:Lcom/sgiggle/production/InviteSelectionActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    .line 755
    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/Utils$UIContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 756
    iput-object p8, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    .line 757
    return-void
.end method


# virtual methods
.method public displayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 761
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    goto :goto_0
.end method
