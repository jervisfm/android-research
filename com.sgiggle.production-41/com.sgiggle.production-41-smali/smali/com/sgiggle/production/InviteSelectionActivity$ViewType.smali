.class final enum Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
.super Ljava/lang/Enum;
.source "InviteSelectionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/InviteSelectionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/InviteSelectionActivity$ViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

.field public static final enum VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

.field public static final enum VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

.field public static final enum VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

.field public static final enum VIEW_TYPE_INVITE_WEIBO:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    const-string v1, "VIEW_TYPE_INVITE_EMAIL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 65
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    const-string v1, "VIEW_TYPE_INVITE_SMS"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 66
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    const-string v1, "VIEW_TYPE_INVITE_RECOMMENDED"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 67
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    const-string v1, "VIEW_TYPE_INVITE_WEIBO"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_WEIBO:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 63
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_WEIBO:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->$VALUES:[Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    .locals 1
    .parameter

    .prologue
    .line 63
    const-class v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->$VALUES:[Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-virtual {v0}, [Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    return-object v0
.end method
