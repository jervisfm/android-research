.class public Lcom/sgiggle/production/InviteSelectionActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "InviteSelectionActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/InviteSelectionActivity$4;,
        Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;,
        Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;,
        Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    }
.end annotation


# static fields
.field private static final DIALOG_INVITE_REACH_LIMIT:I = 0x0

.field private static final REQUEST_CODE_INVITE_EMAIL:I = 0x0

.field private static final REQUEST_CODE_INVITE_SMS:I = 0x1

.field private static final SEARCH_ID:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Tango.InviteSelectionActivity"


# instance fields
.field private m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

.field private m_allButton:Landroid/widget/CheckBox;

.field private final m_contactItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_emailCapable:Z

.field private m_emptyView:Landroid/widget/TextView;

.field private m_header:Landroid/widget/TextView;

.field private m_listView:Landroid/widget/ListView;

.field private m_listViewPlaceholder:Landroid/widget/TextView;

.field private m_listViewWrapper:Landroid/view/View;

.field private m_query:Ljava/lang/String;

.field private m_recommendationAlgo:Ljava/lang/String;

.field private m_recommendationAlgoForSelectedInvitees:Ljava/lang/String;

.field private m_recommendedButton:Landroid/widget/RadioButton;

.field private m_recommendedButtonLabel:Ljava/lang/String;

.field private m_selectAllListener:Landroid/view/View$OnClickListener;

.field private m_selectAllPanel:Landroid/view/ViewGroup;

.field private m_selectedCount:I

.field private final m_selectedInvitees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private m_sendButton:Landroid/widget/Button;

.field private m_sendInvitesListener:Landroid/view/View$OnClickListener;

.field private m_smsCapable:Z

.field private m_startedAsList:Z

.field private m_titleView:Landroid/widget/TextView;

.field private m_typeRadioGroup:Landroid/widget/RadioGroup;

.field private m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

.field private m_weiboCapable:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 85
    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    .line 86
    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    .line 87
    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_weiboCapable:Z

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    .line 93
    iput v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 95
    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_startedAsList:Z

    .line 907
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/InviteSelectionActivity$1;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllListener:Landroid/view/View$OnClickListener;

    .line 926
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/InviteSelectionActivity$2;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendInvitesListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/InviteSelectionActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    return v0
.end method

.method static synthetic access$012(Lcom/sgiggle/production/InviteSelectionActivity;I)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/InviteSelectionActivity;Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->reachEmailSelectedLimit(Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/InviteSelectionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/InviteSelectionActivity;)Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/InviteSelectionActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/InviteSelectionActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;

    return-object v0
.end method

.method private displayContacts(Ljava/util/List;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v7, 0x8

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 370
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "displayContacts(): New list-size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_titleView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_0

    .line 377
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 382
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 383
    const v0, 0x7f0b0001

    .line 393
    :goto_1
    new-instance v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    const v2, 0x7f03002d

    invoke-direct {v1, p0, v2, p1}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    .line 394
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 395
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v2}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_3

    move v2, v4

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 398
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v1, v6}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 399
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 401
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 402
    if-lez v1, :cond_4

    .line 403
    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_header:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v0, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    :goto_3
    const/16 v0, 0x1f4

    if-le v1, v0, :cond_5

    .line 409
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 414
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 417
    invoke-direct {p0, v6}, Lcom/sgiggle/production/InviteSelectionActivity;->showProgressView(Z)V

    .line 419
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V

    .line 420
    return-void

    .line 378
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_1

    .line 379
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090031

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 381
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090032

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_titleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090055

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 389
    const v0, 0x7f0b0002

    .line 390
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_titleView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_3
    move v2, v6

    .line 395
    goto/16 :goto_2

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_header:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 411
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method private getSelectedType()Lcom/sgiggle/production/InviteSelectionActivity$ViewType;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 196
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 190
    :pswitch_0
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    goto :goto_0

    .line 192
    :pswitch_1
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    goto :goto_0

    .line 194
    :pswitch_2
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    goto :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x7f0a00bc
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private getSubLabelForSMSInvitee(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 603
    const/4 v0, 0x0

    .line 604
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    if-ne v1, v2, :cond_1

    .line 605
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 613
    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_4

    .line 614
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 616
    :goto_1
    return-object v0

    .line 606
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    if-ne v1, v2, :cond_2

    .line 607
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 608
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    if-ne v1, v2, :cond_3

    .line 609
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 610
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    if-ne v1, v2, :cond_0

    .line 611
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 616
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private handleEmailComposerEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;)V
    .locals 13
    .parameter

    .prologue
    .line 489
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "handleEmailComposerEvent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_startedAsList:Z

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->setVisible(Z)V

    .line 494
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInviteeCount()I

    move-result v9

    .line 495
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleEmailComposerEvent(): nInviteeCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    if-nez v9, :cond_0

    .line 546
    :goto_0
    return-void

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 500
    new-array v10, v9, [Ljava/lang/String;

    .line 501
    const/4 v0, 0x0

    move v11, v0

    :goto_1
    if-ge v11, v9, :cond_1

    .line 502
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0, v11}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v12

    .line 503
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "handleEmailComposerEvent(): email=%s; phonenumber=%s; nameprefix=%s; firstname=%s; middlename=%s; lastname=%s; namesuffix=%s; displayname=%s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmail()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumber()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefix()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstname()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlename()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x5

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastname()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffix()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplayname()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmail()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v11

    .line 508
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNameprefix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getFirstname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getMiddlename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getLastname()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getNamesuffix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getDisplayname()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getEmail()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    invoke-virtual {v12}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->getPhonenumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    .line 516
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 517
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto/16 :goto_1

    .line 521
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasSpecifiedSubject()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedSubject()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 524
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->hasSpecifiedContent()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailComposerPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v0

    .line 527
    :goto_3
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 528
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    const-string v3, "android.intent.extra.EMAIL"

    invoke-virtual {v2, v3, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 530
    const-string v3, "android.intent.extra.SUBJECT"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 531
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 532
    const-string v0, "message/html"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 533
    const/high16 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 541
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, v2, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 542
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "Invites have been sent out."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 543
    :catch_0
    move-exception v0

    .line 544
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "Not activity was found for ACTION_SEND (for sending Invites)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 521
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 524
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private handleEmailSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;)V
    .locals 14
    .parameter

    .prologue
    const v2, 0x7f0a00bd

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 440
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "handleInviteDisplayMainEvent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 444
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 486
    :goto_0
    return-void

    .line 452
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->resetInviteSelectionUI()V

    .line 454
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 455
    iput-boolean v13, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_startedAsList:Z

    .line 456
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 458
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getContactCount()I

    move-result v9

    move v10, v12

    .line 462
    :goto_1
    if-ge v10, v9, :cond_5

    .line 463
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    invoke-virtual {v0, v10}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getContact(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v11

    .line 464
    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v0

    if-nez v0, :cond_2

    .line 465
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "Ignore Contact that has no emaill."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    :goto_2
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_1

    .line 468
    :cond_2
    new-instance v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    iget-object v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    .line 476
    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    :goto_3
    iput-object v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    .line 478
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;

    invoke-virtual {v1, v10}, Lcom/sgiggle/xmpp/SessionMessages$InviteEmailSelectionPayload;->getSelected(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 479
    iget v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    iget-boolean v2, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v2, :cond_4

    move v2, v13

    :goto_4
    add-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 480
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 476
    :cond_3
    const-string v1, ""

    goto :goto_3

    :cond_4
    move v2, v12

    .line 479
    goto :goto_4

    .line 483
    :cond_5
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v0

    .line 484
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    new-instance v2, Lcom/sgiggle/production/Utils$ContactComparator;

    invoke-direct {v2, v0}, Lcom/sgiggle/production/Utils$ContactComparator;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 485
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->displayContacts(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method private handleInviteDisplayMainEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 290
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "handleInviteDisplayMainEvent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;

    .line 296
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getEmailinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    if-ne v1, v2, :cond_0

    .line 297
    iput-boolean v4, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    .line 304
    :goto_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSmsinvitetype()Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$InviteSendType;

    if-ne v1, v2, :cond_1

    .line 305
    iput-boolean v4, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    .line 312
    :goto_1
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteOptionsPayload;->getSns()Ljava/lang/String;

    move-result-object v0

    .line 313
    const-string v1, "Tango.InviteSelectionActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sns type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const-string v1, "weibo"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_weiboCapable:Z

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 318
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 321
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->updateTypeToggleVisibility()V

    .line 323
    return v4

    .line 300
    :cond_0
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isEmailIntentAvailable(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    goto :goto_0

    .line 308
    :cond_1
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    goto :goto_1
.end method

.method private handleRecommendedSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;)V
    .locals 5
    .parameter

    .prologue
    .line 620
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_0

    .line 621
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContactsCount()I

    move-result v1

    .line 622
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContactsList()Ljava/util/List;

    move-result-object v2

    .line 624
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->hasRecommendationAlgorithm()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;

    .line 631
    :goto_0
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleRecommendedSelectionEvent() - handleSMSSelectionEvent:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ,m_viewType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    const v0, 0x7f0a00be

    sget-object v3, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/sgiggle/production/InviteSelectionActivity;->handleSMSSelectionEvent(ILcom/sgiggle/production/InviteSelectionActivity$ViewType;ILjava/util/List;)V

    .line 634
    :cond_0
    return-void

    .line 627
    :cond_1
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v3, "handleRecommendedSelectionEvent() - event does not have recommendation algorithm field"

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleSMSInstructionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;)V
    .locals 15
    .parameter

    .prologue
    const/4 v14, 0x1

    .line 637
    iget-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_startedAsList:Z

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->setVisible(Z)V

    .line 641
    invoke-static {}, Lcom/sgiggle/production/Utils;->getSmsContactSeparator()C

    move-result v11

    .line 642
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 643
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 644
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getContactList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-object v10, v0

    .line 645
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 646
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 648
    :cond_0
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    new-instance v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v9

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    invoke-virtual {v10}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    .line 658
    iput-boolean v14, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 659
    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 662
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgoForSelectedInvitees:Ljava/lang/String;

    .line 663
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasRecommendationAlgorithm()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 664
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getRecommendationAlgorithm()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgoForSelectedInvitees:Ljava/lang/String;

    .line 668
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->hasSpecifiedContent()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectedPayload;->getSpecifiedContent()Ljava/lang/String;

    move-result-object v1

    .line 670
    :goto_1
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2, v1, v14}, Lcom/sgiggle/production/InviteSelectionActivity;->sendSms(Ljava/lang/String;Ljava/lang/String;I)V

    .line 671
    return-void

    .line 668
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private handleSMSSelectionEvent(ILcom/sgiggle/production/InviteSelectionActivity$ViewType;ILjava/util/List;)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/sgiggle/production/InviteSelectionActivity$ViewType;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 559
    const-string v1, "Tango.InviteSelectionActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleSMSSelectionEvent view type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    if-eq v1, p1, :cond_0

    const v1, 0x7f0a00be

    if-eq p1, v1, :cond_0

    .line 563
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v1, p1}, Landroid/widget/RadioGroup;->check(I)V

    .line 569
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v2, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 600
    :goto_0
    return-void

    .line 573
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->resetInviteSelectionUI()V

    .line 575
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 576
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_startedAsList:Z

    .line 577
    iput-object p2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 581
    const/4 v1, 0x0

    move v10, v1

    :goto_1
    if-ge v10, p3, :cond_4

    .line 582
    invoke-interface {p4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-object v9, v0

    .line 583
    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v1

    if-nez v1, :cond_2

    .line 584
    const-string v1, "Tango.InviteSelectionActivity"

    const-string v2, "Ignore Contact without a phone-number."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :goto_2
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_1

    .line 587
    :cond_2
    new-instance v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v8

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 590
    invoke-virtual {v9}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    .line 591
    invoke-direct {p0, v9}, Lcom/sgiggle/production/InviteSelectionActivity;->getSubLabelForSMSInvitee(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_subLabel:Ljava/lang/String;

    .line 592
    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 593
    iget v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    iget-boolean v3, v1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    :goto_3
    add-int/2addr v2, v3

    iput v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 594
    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 593
    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    .line 597
    :cond_4
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v1

    .line 598
    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    new-instance v3, Lcom/sgiggle/production/Utils$ContactComparator;

    invoke-direct {v3, v1}, Lcom/sgiggle/production/Utils$ContactComparator;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 599
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->displayContacts(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method private handleSMSSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;)V
    .locals 4
    .parameter

    .prologue
    .line 549
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "handleSMSSelectionEvent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContactsCount()I

    move-result v1

    .line 552
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteSMSSelectionPayload;->getContactsList()Ljava/util/List;

    move-result-object v0

    .line 553
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;

    .line 554
    const v2, 0x7f0a00bc

    sget-object v3, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v2, v3, v1, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->handleSMSSelectionEvent(ILcom/sgiggle/production/InviteSelectionActivity$ViewType;ILjava/util/List;)V

    .line 555
    return-void
.end method

.method private handleSearch(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    .line 423
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSearch(query="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 426
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 428
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 429
    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->displayName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 432
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 436
    :cond_2
    invoke-direct {p0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->displayContacts(Ljava/util/List;)V

    .line 437
    return-void
.end method

.method private isSelectedTypeActive()Z
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getSelectedType()Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    move-result-object v0

    .line 178
    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    if-nez v1, :cond_2

    :cond_0
    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    if-nez v1, :cond_2

    :cond_1
    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne v0, v1, :cond_3

    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onCheckedItemChanged()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1047
    iget v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    if-nez v0, :cond_0

    .line 1048
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090038

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1049
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1056
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_adapter:Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/InviteSelectionActivity$ContactArrayAdapter;->isAllItemChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v7

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1057
    return-void

    .line 1051
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f0b

    iget v3, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    new-array v4, v7, [Ljava/lang/Object;

    iget v5, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1053
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_1
    move v1, v6

    .line 1056
    goto :goto_1
.end method

.method private reachEmailSelectedLimit(Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1037
    iget v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    const/16 v1, 0x1f4

    if-le v0, v1, :cond_0

    .line 1038
    iput-boolean v2, p1, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 1039
    iget v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 1040
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/InviteSelectionActivity;->showDialog(I)V

    move v0, v3

    .line 1043
    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method private resetInviteSelectionUI()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1146
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1147
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1148
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1149
    iput-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    .line 1150
    iput v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 1151
    iput-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 1153
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V

    .line 1154
    return-void
.end method

.method private sendSms(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 680
    invoke-static {p1, p2}, Lcom/sgiggle/production/Utils;->getSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 682
    :try_start_0
    invoke-virtual {p0, v0, p3}, Lcom/sgiggle/production/InviteSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 686
    :goto_0
    return-void

    .line 683
    :catch_0
    move-exception v0

    .line 684
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "Not activity was found for ACTION_VIEW (for sending SMS)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showProgressView(Z)V
    .locals 4
    .parameter

    .prologue
    const v0, 0x7f0a00c4

    const v3, 0x7f0a00ba

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 331
    if-eqz p1, :cond_0

    .line 332
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 333
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 338
    :goto_0
    return-void

    .line 335
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 336
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1081
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-ne p1, v0, :cond_0

    .line 1082
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring switchToType, already in type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1143
    :goto_0
    return-void

    .line 1085
    :cond_0
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchToType, to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1088
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$4;->$SwitchMap$com$sgiggle$production$InviteSelectionActivity$ViewType:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1090
    :pswitch_0
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    if-eqz v0, :cond_1

    .line 1091
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1092
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1094
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionMessage;-><init>()V

    .line 1095
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 1097
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->resetInviteSelectionUI()V

    .line 1098
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1099
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    const v1, 0x7f090075

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1100
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1101
    invoke-direct {p0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->showProgressView(Z)V

    goto :goto_0

    .line 1105
    :pswitch_1
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    if-eqz v0, :cond_2

    .line 1106
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1107
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1109
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionMessage;-><init>()V

    .line 1110
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 1112
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->resetInviteSelectionUI()V

    .line 1113
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1114
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    const v1, 0x7f090074

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1115
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1116
    invoke-direct {p0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->showProgressView(Z)V

    goto/16 :goto_0

    .line 1120
    :pswitch_2
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 1121
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    if-eqz v0, :cond_3

    .line 1122
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1123
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1125
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionMessage;-><init>()V

    .line 1126
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 1128
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->resetInviteSelectionUI()V

    .line 1129
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1130
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    const v1, 0x7f090076

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1131
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1132
    invoke-direct {p0, v3}, Lcom/sgiggle/production/InviteSelectionActivity;->showProgressView(Z)V

    goto/16 :goto_0

    .line 1136
    :pswitch_3
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_WEIBO:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    .line 1138
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteViaSnsMessage;

    const-string v1, "weibo"

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteViaSnsMessage;-><init>(Ljava/lang/String;)V

    .line 1139
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1140
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "send InviteViaSnsMessage() to SM"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1088
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateTypeToggleVisibility()V
    .locals 6

    .prologue
    const v5, 0x7f0a00bf

    const v4, 0x7f0a00bd

    const v3, 0x7f0a00bc

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 345
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_weiboCapable:Z

    if-eqz v0, :cond_1

    .line 346
    const v0, 0x7f0a00c0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 347
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 354
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 355
    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emailCapable:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_smsCapable:Z

    if-nez v0, :cond_2

    .line 356
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    if-eq v0, v4, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v4}, Landroid/widget/RadioGroup;->check(I)V

    .line 358
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    .line 367
    :cond_0
    :goto_1
    return-void

    .line 349
    :cond_1
    const v0, 0x7f0a00c0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 350
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 362
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 363
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->check(I)V

    .line 364
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    goto :goto_1
.end method


# virtual methods
.method protected handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 245
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 279
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Unsupported message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :goto_0
    return-void

    .line 249
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;

    .line 250
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleInviteDisplayMainEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainEvent;)Z

    goto :goto_0

    .line 254
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;

    .line 255
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleEmailSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSelectionEvent;)V

    goto :goto_0

    .line 259
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;

    .line 260
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleEmailComposerEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailComposerEvent;)V

    goto :goto_0

    .line 264
    :sswitch_3
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;

    .line 265
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleSMSSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSelectionEvent;)V

    goto :goto_0

    .line 269
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;

    .line 270
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleRecommendedSelectionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteRecommendedSelectionEvent;)V

    goto :goto_0

    .line 274
    :sswitch_5
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;

    .line 275
    invoke-direct {p0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleSMSInstructionEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSInstructionEvent;)V

    goto :goto_0

    .line 247
    :sswitch_data_0
    .sparse-switch
        0x88df -> :sswitch_0
        0x88e1 -> :sswitch_1
        0x88f1 -> :sswitch_2
        0x88f3 -> :sswitch_3
        0x88f4 -> :sswitch_4
        0x88f5 -> :sswitch_5
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 690
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult(): requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    packed-switch p1, :pswitch_data_0

    .line 737
    :goto_0
    return-void

    .line 698
    :pswitch_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 699
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 700
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_email:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_middleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_nameSuffix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v0, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 708
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 710
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;

    invoke-direct {v3, v1, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteEmailSendMessage;-><init>(Ljava/util/List;Z)V

    invoke-virtual {v0, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 716
    :pswitch_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 717
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 718
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_phoneNumber:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setPhonenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_firstName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_middleName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_lastName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_nameSuffix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v3

    iget-object v0, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_displayName:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    .line 728
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 732
    :cond_1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgoForSelectedInvitees:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSMSSendMessage;-><init>(Ljava/util/List;Ljava/lang/String;Z)V

    .line 733
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 691
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 991
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onBackPressed()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 992
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 993
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    .line 994
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->displayContacts(Ljava/util/List;)V

    .line 998
    :goto_0
    return-void

    .line 996
    :cond_0
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedInvitees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 105
    iput-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgo:Ljava/lang/String;

    .line 106
    iput-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendationAlgoForSelectedInvitees:Ljava/lang/String;

    .line 108
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->setContentView(I)V

    .line 114
    const v0, 0x7f0a00b6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    .line 115
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_allButton:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 118
    const v0, 0x7f0a00b8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_sendInvitesListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->setDefaultKeyMode(I)V

    .line 124
    const v0, 0x7f0a003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    .line 125
    const v0, 0x7f0a003f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_emptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 128
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_titleView:Landroid/widget/TextView;

    .line 129
    const v0, 0x7f0a00b5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectAllPanel:Landroid/view/ViewGroup;

    .line 131
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_header:Landroid/widget/TextView;

    .line 132
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_header:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 136
    const v0, 0x7f0a00bb

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    .line 137
    const v0, 0x7f0a003a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewWrapper:Landroid/view/View;

    .line 138
    const v0, 0x7f0a00c3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_listViewPlaceholder:Landroid/widget/TextView;

    .line 140
    const v0, 0x7f0a00be

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    .line 141
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButtonLabel:Ljava/lang/String;

    .line 147
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->showProgressView(Z)V

    .line 149
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 150
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 152
    :cond_1
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter

    .prologue
    .line 1063
    packed-switch p1, :pswitch_data_0

    .line 1076
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1065
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1066
    const v1, 0x7f090092

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/16 v4, 0x1f4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/InviteSelectionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1068
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/InviteSelectionActivity$3;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/InviteSelectionActivity$3;-><init>(Lcom/sgiggle/production/InviteSelectionActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 1063
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 163
    :goto_0
    return v0

    .line 162
    :cond_0
    const/4 v0, 0x4

    const v1, 0x7f090041

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 163
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    .line 1026
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 1032
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1034
    return-void
.end method

.method public onEmailClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 1163
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_EMAIL:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    .line 1164
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 893
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 894
    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;

    .line 895
    iget-boolean v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-nez v1, :cond_2

    move v1, v3

    :goto_0
    iput-boolean v1, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    .line 896
    iget v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    iget-boolean v2, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_1
    add-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_selectedCount:I

    .line 897
    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v2, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-eq v1, v2, :cond_0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->reachEmailSelectedLimit(Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 898
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->onCheckedItemChanged()V

    .line 900
    const v1, 0x7f0a00b4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 901
    if-eqz v1, :cond_1

    .line 902
    iget-boolean v0, v0, Lcom/sgiggle/production/InviteSelectionActivity$ContactItem;->m_selected:Z

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 905
    :cond_1
    return-void

    .line 895
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 896
    :cond_3
    const/4 v2, -0x1

    goto :goto_1
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 212
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onNewIntent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_contactItems:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 218
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_query:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->handleSearch(Ljava/lang/String;)V

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 202
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 207
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 204
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->onSearchRequested()Z

    .line 205
    const/4 v0, 0x1

    goto :goto_0

    .line 202
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1014
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 1016
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 169
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->isSelectedTypeActive()Z

    move-result v0

    return v0
.end method

.method public onRecommendedClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 1168
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    .line 1169
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 1008
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1009
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 1010
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/sgiggle/production/InviteSelectionActivity;->isSelectedTypeActive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x0

    .line 234
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public onSmsClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 1158
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_SMS:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    .line 1159
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 1002
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 1004
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 1020
    const-string v0, "Tango.InviteSelectionActivity"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStop()V

    .line 1022
    return-void
.end method

.method public onWeiboClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_typeRadioGroup:Landroid/widget/RadioGroup;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 1173
    sget-object v0, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_WEIBO:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/InviteSelectionActivity;->switchToType(Lcom/sgiggle/production/InviteSelectionActivity$ViewType;)V

    .line 1174
    return-void
.end method

.method public setRecommendedBadgeCount(I)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1182
    const-string v0, "Tango.InviteSelectionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRecommendedBadgeCount to count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1184
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_viewType:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    sget-object v1, Lcom/sgiggle/production/InviteSelectionActivity$ViewType;->VIEW_TYPE_INVITE_RECOMMENDED:Lcom/sgiggle/production/InviteSelectionActivity$ViewType;

    if-eq v0, v1, :cond_2

    .line 1185
    if-gtz p1, :cond_0

    .line 1186
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1187
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButtonLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1200
    :goto_0
    return-void

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1190
    const/16 v0, 0x63

    if-le p1, v0, :cond_1

    .line 1191
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButtonLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f090006

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1193
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButtonLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1197
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    invoke-virtual {v0, v3, v4}, Landroid/widget/RadioButton;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1198
    iget-object v0, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButton:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/sgiggle/production/InviteSelectionActivity;->m_recommendedButtonLabel:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
