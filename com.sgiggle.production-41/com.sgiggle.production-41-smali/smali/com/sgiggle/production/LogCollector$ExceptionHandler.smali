.class Lcom/sgiggle/production/LogCollector$ExceptionHandler;
.super Ljava/lang/Object;
.source "LogCollector.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/LogCollector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ExceptionHandler"
.end annotation


# instance fields
.field private m_defaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/LogCollector$ExceptionHandler;->m_defaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 104
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 108
    instance-of v0, p2, Lcom/sgiggle/exception/TangoException;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Lcom/sgiggle/exception/TangoException;

    invoke-direct {v0, p2}, Lcom/sgiggle/exception/TangoException;-><init>(Ljava/lang/Throwable;)V

    .line 111
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/LogCollector$ExceptionHandler;->m_defaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v1, p1, v0}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 112
    return-void

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method
