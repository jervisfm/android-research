.class public Lcom/sgiggle/production/LogCollector;
.super Ljava/lang/Object;
.source "LogCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/LogCollector$ExceptionHandler;,
        Lcom/sgiggle/production/LogCollector$TangoAppData;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    return-void
.end method

.method protected static checkWriteOk(Ljava/io/File;)Z
    .locals 3
    .parameter

    .prologue
    .line 59
    .line 61
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v1, "tangolog.txt"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 63
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v2, Ljava/io/FileWriter;

    invoke-direct {v2, v0}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 64
    const-string v2, "test"

    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 65
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->newLine()V

    .line 66
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 67
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V

    .line 69
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 75
    :goto_0
    return v0

    .line 71
    :catch_0
    move-exception v0

    .line 73
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getStorageDirSafe(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 30
    invoke-static {v0}, Lcom/sgiggle/production/LogCollector;->checkWriteOk(Ljava/io/File;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
