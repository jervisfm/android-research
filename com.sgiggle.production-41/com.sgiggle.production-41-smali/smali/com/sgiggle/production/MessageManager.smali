.class public final Lcom/sgiggle/production/MessageManager;
.super Ljava/lang/Object;
.source "MessageManager.java"


# static fields
.field private static final DBG:Z = true

.field private static final INSTANCE_ID_DEFAULT:I = 0x1

.field private static final INSTANCE_ID_INVALID:I = 0x0

.field private static final MESSAGE_ID_INVALID:I = 0x0

.field private static final PREF_INSTANCE_ID:Ljava/lang/String; = "instance_id"

.field private static final TAG:Ljava/lang/String; = "Tango.MessageManager"

.field private static final TANGO_EXTRA_INSTANCE_ID:Ljava/lang/String; = "me.tango.extra_instance_id"

.field private static final TANGO_EXTRA_MESSAGE_ID:Ljava/lang/String; = "me.tango.extra_message_id"

.field private static final VDBG:Z = true

.field private static s_me:Lcom/sgiggle/production/MessageManager;


# instance fields
.field private m_application:Lcom/sgiggle/production/TangoApp;

.field private final m_instanceId:I

.field private final m_messageMap:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sgiggle/messaging/Message;",
            ">;"
        }
    .end annotation
.end field

.field private m_nextMessageId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    .line 35
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_nextMessageId:Ljava/lang/Integer;

    .line 38
    iput-object p1, p0, Lcom/sgiggle/production/MessageManager;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_application:Lcom/sgiggle/production/TangoApp;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/MessageManager;->generateInstanceId(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/MessageManager;->m_instanceId:I

    .line 40
    return-void
.end method

.method private generateInstanceId(Landroid/content/Context;)I
    .locals 4
    .parameter

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sgiggle/production/MessageManager;->getInstanceId(Landroid/content/Context;)I

    move-result v0

    .line 106
    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 109
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/MessageManager;->setInstanceId(Landroid/content/Context;I)V

    .line 110
    const-string v1, "Tango.MessageManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "generateInstanceId(): new instanceId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    return v0

    .line 106
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getDefault()Lcom/sgiggle/production/MessageManager;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sgiggle/production/MessageManager;->s_me:Lcom/sgiggle/production/MessageManager;

    return-object v0
.end method

.method private getInstanceId(Landroid/content/Context;)I
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 115
    const-string v0, "Tango.MessageManager"

    invoke-virtual {p1, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 116
    const-string v1, "instance_id"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static init(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 43
    new-instance v0, Lcom/sgiggle/production/MessageManager;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/MessageManager;-><init>(Lcom/sgiggle/production/TangoApp;)V

    sput-object v0, Lcom/sgiggle/production/MessageManager;->s_me:Lcom/sgiggle/production/MessageManager;

    .line 44
    return-void
.end method

.method private setInstanceId(Landroid/content/Context;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 120
    const-string v0, "Tango.MessageManager"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 121
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "instance_id"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 122
    return-void
.end method


# virtual methods
.method public clearAllPendingMessages()V
    .locals 4

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    monitor-enter v0

    .line 96
    :try_start_0
    const-string v1, "Tango.MessageManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "clearAllPendingMessages(): Clear all events: count:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    iget-object v1, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->clear()V

    .line 98
    monitor-exit v0

    .line 99
    return-void

    .line 98
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;
    .locals 6
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 72
    const-string v0, "me.tango.extra_instance_id"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 73
    if-nez v0, :cond_0

    move-object v0, v4

    .line 90
    :goto_0
    return-object v0

    .line 75
    :cond_0
    iget v1, p0, Lcom/sgiggle/production/MessageManager;->m_instanceId:I

    if-eq v0, v1, :cond_1

    .line 76
    const-string v1, "Tango.MessageManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMessageFromIntent: The intent is from an old instance="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Current="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/sgiggle/production/MessageManager;->m_instanceId:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v4

    .line 78
    goto :goto_0

    .line 81
    :cond_1
    const-string v0, "me.tango.extra_message_id"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v4

    .line 83
    goto :goto_0

    .line 86
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    monitor-enter v2

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/messaging/Message;

    .line 88
    const-string v3, "Tango.MessageManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMessageFromIntent: {#"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sgiggle/production/MessageManager;->m_instanceId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "}"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    monitor-exit v2

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public storeMessageInIntent(Lcom/sgiggle/messaging/Message;Landroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 57
    const-string v0, "me.tango.extra_instance_id"

    iget v1, p0, Lcom/sgiggle/production/MessageManager;->m_instanceId:I

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 58
    iget-object v0, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    monitor-enter v0

    .line 59
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/MessageManager;->m_nextMessageId:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/MessageManager;->m_nextMessageId:Ljava/lang/Integer;

    .line 60
    iget-object v2, p0, Lcom/sgiggle/production/MessageManager;->m_messageMap:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v2, "me.tango.extra_message_id"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 62
    monitor-exit v0

    .line 63
    return-void

    .line 62
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
