.class Lcom/sgiggle/production/MissedCallNotifier;
.super Ljava/lang/Object;
.source "MissedCallNotifier.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.MissedCallNotifier"

.field private static s_me:Lcom/sgiggle/production/MissedCallNotifier;


# instance fields
.field private m_application:Lcom/sgiggle/production/TangoApp;

.field private m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

.field private m_missedCallAlert:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/TangoApp;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/MissedCallNotifier;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sgiggle/production/MissedCallNotifier;->makeCallback()V

    return-void
.end method

.method static synthetic access$102(Lcom/sgiggle/production/MissedCallNotifier;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-object p1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/MissedCallNotifier;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sgiggle/production/MissedCallNotifier;->clearMissedCall()V

    return-void
.end method

.method private clearMissedCall()V
    .locals 5

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedJid()Ljava/lang/String;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v1}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedName()Ljava/lang/String;

    move-result-object v1

    .line 222
    const-string v2, "Tango.MissedCallNotifier"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearMissedCall(): Dismiss "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ["

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "]"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    .line 227
    :cond_0
    return-void
.end method

.method private dismissPendingMissedCallAlert()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 166
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "Dismiss the existing Missed-Call alert..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    .line 170
    :cond_0
    return-void
.end method

.method static getDefault()Lcom/sgiggle/production/MissedCallNotifier;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sgiggle/production/MissedCallNotifier;->s_me:Lcom/sgiggle/production/MissedCallNotifier;

    return-object v0
.end method

.method static init(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 41
    new-instance v0, Lcom/sgiggle/production/MissedCallNotifier;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/MissedCallNotifier;-><init>(Lcom/sgiggle/production/TangoApp;)V

    sput-object v0, Lcom/sgiggle/production/MissedCallNotifier;->s_me:Lcom/sgiggle/production/MissedCallNotifier;

    .line 42
    return-void
.end method

.method private makeCallback()V
    .locals 5

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedJid()Ljava/lang/String;

    move-result-object v0

    .line 208
    iget-object v1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v1}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedName()Ljava/lang/String;

    move-result-object v1

    .line 210
    const-string v2, "Tango.MissedCallNotifier"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "makeCallback(): to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v2

    const-string v3, "jingle"

    new-instance v4, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;

    invoke-direct {v4, v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    .line 215
    :cond_0
    return-void
.end method

.method private showMissedCallAlert(Landroid/content/Context;Lcom/sgiggle/production/Utils$UIMissedCall;)V
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 127
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "showMissedCallAlert()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    invoke-direct {p0}, Lcom/sgiggle/production/MissedCallNotifier;->dismissPendingMissedCallAlert()V

    .line 131
    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedName()Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedWhen()J

    move-result-wide v1

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 135
    const-string v4, "%s %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const v6, 0x7f090039

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    const-string v6, "h:mm aa"

    invoke-static {v6, v1, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v1

    aput-object v1, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 137
    const v2, 0x7f09003a

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 140
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 141
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 142
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 143
    const v0, 0x7f09003b

    new-instance v1, Lcom/sgiggle/production/MissedCallNotifier$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/MissedCallNotifier$1;-><init>(Lcom/sgiggle/production/MissedCallNotifier;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 149
    const v0, 0x7f09001f

    new-instance v1, Lcom/sgiggle/production/MissedCallNotifier$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/MissedCallNotifier$2;-><init>(Lcom/sgiggle/production/MissedCallNotifier;)V

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 159
    return-void
.end method


# virtual methods
.method public cancelLastAlert()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "cancelLastAlert()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-direct {p0}, Lcom/sgiggle/production/MissedCallNotifier;->dismissPendingMissedCallAlert()V

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    .line 92
    :cond_0
    return-void
.end method

.method public dismissMissedCallNotification()V
    .locals 2

    .prologue
    .line 118
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 119
    return-void
.end method

.method public displayPendingMissedCallAlert(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "displayPendingMissedCallAlert()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 98
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/MissedCallNotifier;->showMissedCallAlert(Landroid/content/Context;Lcom/sgiggle/production/Utils$UIMissedCall;)V

    .line 100
    :cond_0
    return-void
.end method

.method public hidePendingMissedCallAlert()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "Hide the existing Missed-Call alert..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 109
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 110
    iput-object v2, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    .line 112
    :cond_0
    return-void
.end method

.method public isLastEventEqualToMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 8
    .parameter

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x8929

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    if-nez v0, :cond_1

    .line 74
    :cond_0
    const/4 v0, 0x0

    .line 80
    :goto_0
    return v0

    .line 76
    :cond_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;

    .line 77
    new-instance v1, Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getTimestamp()I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sgiggle/production/Utils$UIMissedCall;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 80
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/Utils$UIMissedCall;->isSame(Lcom/sgiggle/production/Utils$UIMissedCall;)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyMissedCallInStatusBar()V
    .locals 12

    .prologue
    const v8, 0x7f090039

    const/4 v11, 0x0

    .line 177
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "notifyMissedCallInStatusBar()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedJid()Ljava/lang/String;

    move-result-object v0

    .line 179
    iget-object v1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v1}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedName()Ljava/lang/String;

    move-result-object v1

    .line 180
    iget-object v2, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {v2}, Lcom/sgiggle/production/Utils$UIMissedCall;->getMissedWhen()J

    move-result-wide v2

    .line 182
    const v4, 0x7f0200ab

    .line 183
    iget-object v5, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 184
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 185
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 186
    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 187
    const v9, 0x7f09003a

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    aput-object v1, v9, v11

    invoke-static {v6, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 190
    new-instance v9, Landroid/content/Intent;

    const-class v10, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {v9, v5, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    const-string v10, "missedJid"

    invoke-virtual {v9, v10, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v0, "missedDisplayname"

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    const-string v0, "missedWhen"

    invoke-virtual {v9, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 194
    const/high16 v0, 0x3000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 195
    const/high16 v0, 0x800

    invoke-static {v5, v11, v9, v0}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 198
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1, v4, v7, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 199
    invoke-virtual {v1, v5, v8, v6, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 200
    iget v0, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v1, Landroid/app/Notification;->flags:I

    .line 202
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 203
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .parameter

    .prologue
    .line 231
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "onDismiss() [DialogInterface]"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-object v0, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_missedCallAlert:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_0

    .line 233
    invoke-direct {p0}, Lcom/sgiggle/production/MissedCallNotifier;->clearMissedCall()V

    .line 235
    :cond_0
    return-void
.end method

.method public saveMissedCallMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 8
    .parameter

    .prologue
    .line 49
    const-string v0, "Tango.MissedCallNotifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveMissedCallMessage("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x8929

    if-eq v0, v1, :cond_0

    .line 51
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "Logic error: Message is NOT a missed-call."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    const/4 v0, 0x0

    .line 58
    :goto_0
    return v0

    .line 54
    :cond_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;

    .line 55
    new-instance v1, Lcom/sgiggle/production/Utils$UIMissedCall;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getDisplayname()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CalleeMissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getTimestamp()I

    move-result v0

    int-to-long v4, v0

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/sgiggle/production/Utils$UIMissedCall;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iput-object v1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    .line 58
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public saveMissedCallNotification(Lcom/sgiggle/production/Utils$UIMissedCall;)V
    .locals 2
    .parameter

    .prologue
    .line 62
    const-string v0, "Tango.MissedCallNotifier"

    const-string v1, "saveMissedCallNotification()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iput-object p1, p0, Lcom/sgiggle/production/MissedCallNotifier;->m_lastMissedCall:Lcom/sgiggle/production/Utils$UIMissedCall;

    .line 64
    return-void
.end method
