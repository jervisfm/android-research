.class final Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "OAuthRequestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/OAuthRequestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/OAuthRequestActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/OAuthRequestActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/OAuthRequestActivity;Lcom/sgiggle/production/OAuthRequestActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;-><init>(Lcom/sgiggle/production/OAuthRequestActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 59
    const-string v0, "Tango.OAuthRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finished loading URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #getter for: Lcom/sgiggle/production/OAuthRequestActivity;->m_isFirstRun:Z
    invoke-static {v0}, Lcom/sgiggle/production/OAuthRequestActivity;->access$100(Lcom/sgiggle/production/OAuthRequestActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #getter for: Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/sgiggle/production/OAuthRequestActivity;->access$200(Lcom/sgiggle/production/OAuthRequestActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #getter for: Lcom/sgiggle/production/OAuthRequestActivity;->m_progressView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sgiggle/production/OAuthRequestActivity;->access$300(Lcom/sgiggle/production/OAuthRequestActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #getter for: Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/sgiggle/production/OAuthRequestActivity;->access$200(Lcom/sgiggle/production/OAuthRequestActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #setter for: Lcom/sgiggle/production/OAuthRequestActivity;->m_isFirstRun:Z
    invoke-static {v0, v3}, Lcom/sgiggle/production/OAuthRequestActivity;->access$102(Lcom/sgiggle/production/OAuthRequestActivity;Z)Z

    .line 67
    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    const-string v0, "Tango.OAuthRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPageStarted URL="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    #calls: Lcom/sgiggle/production/OAuthRequestActivity;->parseOAuth(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/sgiggle/production/OAuthRequestActivity;->access$400(Lcom/sgiggle/production/OAuthRequestActivity;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/OAuthRequestActivity;

    .line 77
    const-string v1, "Tango.OAuthRequestActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceivedError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 79
    return-void
.end method

.method public onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 83
    return-void
.end method
