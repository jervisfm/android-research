.class public Lcom/sgiggle/production/OAuthRequestActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "OAuthRequestActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/OAuthRequestActivity$1;,
        Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;
    }
.end annotation


# static fields
.field private static final ACCESSDENIED:Ljava/lang/String; = "access_denied"

.field private static final ACCESSTOKEN:Ljava/lang/String; = "access_token"

.field private static DEFAUTL_EXPIRETIME:I = 0x0

.field private static final EXPIRETIME:Ljava/lang/String; = "expires_in"

.field private static final REFRESHTOKEN:Ljava/lang/String; = "refresh_token"

.field private static final TAG:Ljava/lang/String; = "Tango.OAuthRequestActivity"


# instance fields
.field private m_isFirstRun:Z

.field private m_progressView:Landroid/view/ViewGroup;

.field private m_webView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const v0, 0x15180

    sput v0, Lcom/sgiggle/production/OAuthRequestActivity;->DEFAUTL_EXPIRETIME:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_isFirstRun:Z

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/OAuthRequestActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_isFirstRun:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sgiggle/production/OAuthRequestActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_isFirstRun:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/OAuthRequestActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/OAuthRequestActivity;)Landroid/view/ViewGroup;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_progressView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/OAuthRequestActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sgiggle/production/OAuthRequestActivity;->parseOAuth(Ljava/lang/String;)V

    return-void
.end method

.method private parseOAuth(Ljava/lang/String;)V
    .locals 9
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 88
    const-string v0, "access_token"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 89
    const-string v0, "access_token"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 90
    const-string v1, "&"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 91
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 92
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    .line 93
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4}, Ljava/lang/String;-><init>()V

    .line 94
    :goto_0
    array-length v5, v0

    if-ge v2, v5, :cond_2

    .line 95
    aget-object v5, v0, v2

    const-string v6, "access_token"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 96
    aget-object v1, v0, v2

    aget-object v5, v0, v2

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v8, v4

    move-object v4, v1

    move-object v1, v8

    .line 94
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move-object v8, v1

    move-object v1, v4

    move-object v4, v8

    goto :goto_0

    .line 97
    :cond_0
    aget-object v5, v0, v2

    const-string v6, "expires_in"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 98
    aget-object v3, v0, v2

    aget-object v5, v0, v2

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    move-object v8, v4

    move-object v4, v1

    move-object v1, v8

    goto :goto_1

    .line 99
    :cond_1
    aget-object v5, v0, v2

    const-string v6, "refresh_token"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 100
    aget-object v4, v0, v2

    aget-object v5, v0, v2

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    move-object v8, v4

    move-object v4, v1

    move-object v1, v8

    goto :goto_1

    .line 103
    :cond_2
    sget v0, Lcom/sgiggle/production/OAuthRequestActivity;->DEFAUTL_EXPIRETIME:I

    .line 105
    :try_start_0
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    sget v0, Lcom/sgiggle/production/OAuthRequestActivity;->DEFAUTL_EXPIRETIME:I

    move v3, v0

    .line 113
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 114
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v6

    const-string v7, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;

    const/4 v2, 0x1

    const-string v5, ""

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;-><init>(Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 123
    :cond_3
    :goto_3
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    :try_start_1
    const-string v0, "Tango.OAuthRequestActivity"

    const-string v2, "Parse int exception"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    sget v0, Lcom/sgiggle/production/OAuthRequestActivity;->DEFAUTL_EXPIRETIME:I

    move v3, v0

    .line 112
    goto :goto_2

    .line 111
    :catchall_0
    move-exception v0

    sget v1, Lcom/sgiggle/production/OAuthRequestActivity;->DEFAUTL_EXPIRETIME:I

    throw v0

    .line 117
    :cond_4
    const-string v0, "access_denied"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 119
    const-string v0, "Tango.OAuthRequestActivity"

    const-string v1, "User press the cancel button"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v6

    const-string v7, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;

    const-string v1, ""

    const-string v4, ""

    const-string v5, ""

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;-><init>(Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_3

    :cond_5
    move-object v8, v4

    move-object v4, v1

    move-object v1, v8

    goto/16 :goto_1
.end method


# virtual methods
.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 146
    const-string v0, "Tango.OAuthRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x8993

    if-eq v0, v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 149
    :cond_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsRequestAuthEvent;

    .line 150
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsRequestAuthEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;

    .line 151
    iget-object v1, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 127
    const-string v0, "Tango.OAuthRequestActivity"

    const-string v1, "onBackPressed() "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 129
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v6

    const-string v7, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;

    const-string v1, ""

    const-string v4, ""

    const-string v5, ""

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsAuthResultMessage;-><init>(Ljava/lang/String;ZILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 131
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 39
    const-string v0, "Tango.OAuthRequestActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v0, 0x7f030050

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/OAuthRequestActivity;->setContentView(I)V

    .line 43
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/OAuthRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_progressView:Landroid/view/ViewGroup;

    .line 44
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/OAuthRequestActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    .line 46
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 47
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/OAuthRequestActivity;->m_webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/OAuthRequestActivity$MyWebViewClient;-><init>(Lcom/sgiggle/production/OAuthRequestActivity;Lcom/sgiggle/production/OAuthRequestActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/production/OAuthRequestActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/OAuthRequestActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 52
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 135
    const-string v0, "Tango.OAuthRequestActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 137
    return-void
.end method
