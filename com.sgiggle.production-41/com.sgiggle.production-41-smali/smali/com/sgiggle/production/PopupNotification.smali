.class public Lcom/sgiggle/production/PopupNotification;
.super Landroid/app/Activity;
.source "PopupNotification.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.PushMsgNotifier"


# instance fields
.field private m_peerAccountId:Ljava/lang/String;

.field private m_peerFirstname:Ljava/lang/String;

.field private m_peerLastname:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/PopupNotification;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sgiggle/production/PopupNotification;->m_peerAccountId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/PopupNotification;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sgiggle/production/PopupNotification;->m_peerFirstname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/PopupNotification;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sgiggle/production/PopupNotification;->m_peerLastname:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/PopupNotification;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/PopupNotification;->makeCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private createCallDialog()V
    .locals 2

    .prologue
    .line 90
    const v0, 0x7f0a00ea

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 91
    const v1, 0x7f090095

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/PopupNotification;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 92
    new-instance v1, Lcom/sgiggle/production/PopupNotification$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PopupNotification$3;-><init>(Lcom/sgiggle/production/PopupNotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v0, 0x7f0a00e9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 99
    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/PopupNotification;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 100
    new-instance v1, Lcom/sgiggle/production/PopupNotification$4;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PopupNotification$4;-><init>(Lcom/sgiggle/production/PopupNotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    return-void
.end method

.method private createInformationalDialog()V
    .locals 2

    .prologue
    .line 75
    const v0, 0x7f0a00e9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/PopupNotification$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PopupNotification$1;-><init>(Lcom/sgiggle/production/PopupNotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    const v0, 0x7f0a00ea

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/PopupNotification$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PopupNotification$2;-><init>(Lcom/sgiggle/production/PopupNotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method

.method private goodToCall()Z
    .locals 3

    .prologue
    .line 65
    const/4 v0, 0x1

    .line 66
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 68
    const-string v1, "Tango.PushMsgNotifier"

    const-string v2, "goodToCall() in call"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    return v0
.end method

.method private makeCall(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    :goto_0
    return-void

    .line 114
    :cond_0
    const-string v0, "Tango.PushMsgNotifier"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "YFJ: makeCallback(): to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->sendLoginRequestIfNeeded()V

    .line 116
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    .line 117
    if-eqz v0, :cond_1

    const-string v2, "contacts"

    invoke-virtual {v0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    :cond_1
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;

    invoke-direct {v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;-><init>()V

    invoke-virtual {v0, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 121
    const-string v0, "Tango.PushMsgNotifier"

    const-string v2, "Send ContactsDisplayMainMessage"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_1
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v0

    .line 131
    sget-object v2, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne v0, v2, :cond_3

    move-object v0, p3

    move-object v2, p2

    .line 142
    :goto_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    move-object v3, v2

    .line 147
    :goto_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 149
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object v6, v0

    .line 159
    :goto_4
    new-instance v0, Lcom/sgiggle/production/Utils$UIContact;

    move-object v2, p2

    move-object v3, v1

    move-object v4, p3

    move-object v5, v1

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/Utils$UIContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iput-object p1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    .line 161
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIContact;->convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    sget-object v4, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_YFJ_ACTION:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    invoke-direct {v3, v0, v4}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 164
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    goto/16 :goto_0

    .line 124
    :cond_2
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    goto :goto_1

    :cond_3
    move-object v0, p2

    move-object v2, p3

    .line 139
    goto :goto_2

    .line 155
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    goto :goto_4

    :cond_5
    move-object v6, v3

    goto :goto_4

    :cond_6
    move-object v3, v1

    goto :goto_3
.end method


# virtual methods
.method public initPopup(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 168
    const v0, 0x7f0a00e7

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    const v0, 0x7f0a00e8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->requestWindowFeature(I)Z

    .line 38
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PopupNotification;->setContentView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/sgiggle/production/PopupNotification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/PopupNotification;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "body"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/PopupNotification;->initPopup(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    iput-object v4, p0, Lcom/sgiggle/production/PopupNotification;->m_peerFirstname:Ljava/lang/String;

    .line 43
    iput-object v4, p0, Lcom/sgiggle/production/PopupNotification;->m_peerLastname:Ljava/lang/String;

    .line 44
    iput-object v4, p0, Lcom/sgiggle/production/PopupNotification;->m_peerAccountId:Ljava/lang/String;

    .line 45
    invoke-virtual {p0}, Lcom/sgiggle/production/PopupNotification;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "actioninfo"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    const-string v1, "offer-call"

    const-string v2, "actiontype"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/production/PopupNotification;->goodToCall()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "accountid"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/PopupNotification;->m_peerAccountId:Ljava/lang/String;

    .line 49
    const-string v1, "lastname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/PopupNotification;->m_peerLastname:Ljava/lang/String;

    .line 50
    const-string v1, "firstname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/PopupNotification;->m_peerFirstname:Ljava/lang/String;

    .line 51
    const-string v0, "Tango.PushMsgNotifier"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "should display call dialog to  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/PopupNotification;->m_peerFirstname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/PopupNotification;->m_peerLastname:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/PopupNotification;->m_peerAccountId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/PopupNotification;->m_peerAccountId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-direct {p0}, Lcom/sgiggle/production/PopupNotification;->createCallDialog()V

    .line 58
    :goto_1
    return-void

    .line 31
    :catch_0
    move-exception v0

    .line 33
    const-string v1, "Tango.PushMsgNotifier"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 56
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/PopupNotification;->createInformationalDialog()V

    goto :goto_1
.end method
