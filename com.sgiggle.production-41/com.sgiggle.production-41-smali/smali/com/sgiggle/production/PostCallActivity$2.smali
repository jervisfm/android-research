.class Lcom/sgiggle/production/PostCallActivity$2;
.super Ljava/lang/Object;
.source "PostCallActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/PostCallActivity;->onDisplayPostCallEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/PostCallActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/PostCallActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    #getter for: Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z
    invoke-static {v0}, Lcom/sgiggle/production/PostCallActivity;->access$000(Lcom/sgiggle/production/PostCallActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    const/4 v1, 0x1

    #setter for: Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z
    invoke-static {v0, v1}, Lcom/sgiggle/production/PostCallActivity;->access$002(Lcom/sgiggle/production/PostCallActivity;Z)Z

    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    #getter for: Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    invoke-static {v0}, Lcom/sgiggle/production/PostCallActivity;->access$100(Lcom/sgiggle/production/PostCallActivity;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    iget-object v1, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    #getter for: Lcom/sgiggle/production/PostCallActivity;->upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v1}, Lcom/sgiggle/production/PostCallActivity;->access$200(Lcom/sgiggle/production/PostCallActivity;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/PostCallActivity;->purchase(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$2;->this$0:Lcom/sgiggle/production/PostCallActivity;

    #calls: Lcom/sgiggle/production/PostCallActivity;->navigateToContentDetailPage()V
    invoke-static {v0}, Lcom/sgiggle/production/PostCallActivity;->access$300(Lcom/sgiggle/production/PostCallActivity;)V

    .line 235
    :cond_1
    return-void
.end method
