.class Lcom/sgiggle/production/PostCallActivity$3;
.super Ljava/lang/Object;
.source "PostCallActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/PostCallActivity;->onDisplayPostCallEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/PostCallActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/PostCallActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 239
    iput-object p1, p0, Lcom/sgiggle/production/PostCallActivity$3;->this$0:Lcom/sgiggle/production/PostCallActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 242
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisablePostCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/PostCallActivity$3;->this$0:Lcom/sgiggle/production/PostCallActivity;

    #getter for: Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    invoke-static {v3}, Lcom/sgiggle/production/PostCallActivity;->access$100(Lcom/sgiggle/production/PostCallActivity;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisablePostCallMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 244
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity$3;->this$0:Lcom/sgiggle/production/PostCallActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/PostCallActivity;->finish()V

    .line 245
    return-void
.end method
