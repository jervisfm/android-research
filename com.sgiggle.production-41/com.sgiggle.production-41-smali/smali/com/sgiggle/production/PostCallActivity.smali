.class public Lcom/sgiggle/production/PostCallActivity;
.super Lcom/sgiggle/production/BillingSupportBaseActivity;
.source "PostCallActivity.java"


# static fields
.field private static final APPSTORE_DETAIL_REQUEST:I = 0x0

.field private static final FACEBOOK_LIKE_BROWSER_REQUEST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.PostCallActivity"


# instance fields
.field description:Landroid/widget/TextView;

.field detailButton:Landroid/widget/Button;

.field detailIcon:Landroid/widget/ImageView;

.field detailText:Landroid/widget/TextView;

.field icon:Landroid/widget/ImageView;

.field private mDeviceContactId:J

.field private mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

.field private m_btnClicked:Z

.field title:Landroid/widget/TextView;

.field private upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/PostCallActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sgiggle/production/PostCallActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sgiggle/production/PostCallActivity;)Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/PostCallActivity;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/PostCallActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sgiggle/production/PostCallActivity;->navigateToContentDetailPage()V

    return-void
.end method

.method private getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 251
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 252
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 265
    :goto_0
    return-object v0

    .line 256
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 257
    const/16 v1, 0xf0

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 258
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 259
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 260
    if-nez v0, :cond_1

    move-object v0, v2

    .line 261
    goto :goto_0

    .line 263
    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 264
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    move-object v0, v1

    .line 265
    goto :goto_0
.end method

.method private navigateToContentDetailPage()V
    .locals 4

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-eqz v0, :cond_1

    .line 149
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    .line 150
    if-eqz v0, :cond_0

    .line 151
    invoke-virtual {v0}, Lcom/sgiggle/production/TabActivityBase;->setForceSwitchTab()V

    .line 153
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardToPostCallContentMessage;

    iget-object v3, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardToPostCallContentMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 156
    :cond_1
    return-void
.end method

.method private onDisplayPostCallEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f0200ea

    const/16 v7, 0x8

    const/high16 v6, 0x3f00

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 159
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "on display postcall event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getContentType()Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    .line 161
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getCallEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDeviceContactId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/PostCallActivity;->mDeviceContactId:J

    .line 162
    iget-wide v0, p0, Lcom/sgiggle/production/PostCallActivity;->mDeviceContactId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 163
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/PostCallActivity;->mDeviceContactId:J

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_4

    .line 166
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->setContentView(I)V

    .line 174
    :goto_0
    const v0, 0x7f0a00ec

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    .line 175
    const v0, 0x7f0a00ed

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    .line 176
    const v0, 0x7f0a00ee

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->description:Landroid/widget/TextView;

    .line 177
    const v0, 0x7f0a00ef

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailButton:Landroid/widget/Button;

    .line 178
    const v0, 0x7f0a00f1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailIcon:Landroid/widget/ImageView;

    .line 179
    const v0, 0x7f0a00f2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailText:Landroid/widget/TextView;

    .line 181
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_5

    .line 182
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 183
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/PostCallActivity;->upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40a0

    mul-float/2addr v1, v2

    add-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 185
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->upsellProduct:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v0

    .line 186
    invoke-direct {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 188
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 213
    :cond_1
    :goto_1
    const v0, 0x7f0a00f5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_2

    .line 215
    new-instance v1, Lcom/sgiggle/production/PostCallActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PostCallActivity$1;-><init>(Lcom/sgiggle/production/PostCallActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailButton:Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/PostCallActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PostCallActivity$2;-><init>(Lcom/sgiggle/production/PostCallActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 237
    const v0, 0x7f0a00f4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 238
    if-eqz v0, :cond_3

    .line 239
    new-instance v1, Lcom/sgiggle/production/PostCallActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/PostCallActivity$3;-><init>(Lcom/sgiggle/production/PostCallActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    :cond_3
    return-void

    .line 168
    :cond_4
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 190
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_APPSTORE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_6

    .line 191
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 193
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900de

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x4120

    mul-float/2addr v1, v2

    add-float/2addr v1, v6

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 195
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v4, v4, v8}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 196
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->description:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900df

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 197
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 198
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_FACEBOOK:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_7

    .line 199
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->description:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 204
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_INVITE:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_1

    .line 205
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->icon:Landroid/widget/ImageView;

    const v1, 0x7f0200e9

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 207
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->title:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->description:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public confirmPurchaseFailed()V
    .locals 0

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->onBackPressed()V

    .line 294
    return-void
.end method

.method public displayAppStoreDetailPage()V
    .locals 3

    .prologue
    .line 121
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "displayAppStoreDetailPage(): enter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "market://details?id=com.sgiggle.production"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 124
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/PostCallActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 125
    return-void
.end method

.method public displayFacebookLikePage()V
    .locals 3

    .prologue
    .line 128
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "displayFacebookLikePage(): enter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    const-string v0, "http://www.facebook.com/TangoMe"

    .line 130
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 131
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/PostCallActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 132
    return-void
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->onBackPressed()V

    .line 289
    return-void
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 113
    const-string v0, "Tango.PostCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x8977

    if-ne v0, v1, :cond_0

    .line 116
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/PostCallActivity;->onDisplayPostCallEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;)V

    .line 118
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 95
    const-string v0, "Tango.PostCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult(): requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-super {p0, p1, p2, p3}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 98
    if-nez p1, :cond_1

    .line 99
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelAppStoreMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelAppStoreMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 102
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelFacebookLikeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelFacebookLikeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 135
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 137
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->finish()V

    .line 138
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 51
    const-string v0, "Tango.PostCallActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    invoke-super {p0, p1}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->setRequestedOrientation(I)V

    .line 56
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/view/Window;->setGravity(I)V

    .line 57
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    .line 58
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-nez v0, :cond_0

    .line 61
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "PostCallActivity created without an event to init the layout"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PostCallActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onDestroy()V

    .line 90
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 91
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 78
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onPause()V

    .line 81
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 84
    :cond_0
    return-void
.end method

.method public onPurchaseCancelled()V
    .locals 0

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->onBackPressed()V

    .line 299
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/PostCallActivity;->m_btnClicked:Z

    .line 72
    const-string v0, "Tango.PostCallActivity"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-super {p0}, Lcom/sgiggle/production/BillingSupportBaseActivity;->onResume()V

    .line 74
    return-void
.end method

.method public purchaseProcessed()V
    .locals 0

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/sgiggle/production/PostCallActivity;->onBackPressed()V

    .line 277
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 2
    .parameter

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->mType:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    if-ne v0, v1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/PostCallActivity;->detailButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 284
    :cond_0
    return-void
.end method

.method public setPurchesedProduct(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 272
    return-void
.end method
