.class public final Lcom/sgiggle/production/ProximityListener;
.super Ljava/lang/Object;
.source "ProximityListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ProximityListener$EventListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ProximityListener"


# instance fields
.field private final NEAR_VALUE:F

.field private isSamsungMoment:Z

.field private m_farValue:F

.field private m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

.field private m_proximitySensor:Landroid/hardware/Sensor;

.field private m_sensorListener:Landroid/hardware/SensorEventListener;

.field private m_sensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sgiggle/production/ProximityListener$EventListener;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/high16 v0, 0x4040

    iput v0, p0, Lcom/sgiggle/production/ProximityListener;->NEAR_VALUE:F

    .line 37
    iput-object p2, p0, Lcom/sgiggle/production/ProximityListener;->m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

    .line 38
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorManager:Landroid/hardware/SensorManager;

    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_proximitySensor:Landroid/hardware/Sensor;

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_proximitySensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_proximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/ProximityListener;->m_farValue:F

    .line 45
    const-string v0, "Tango.ProximityListener"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MaximumRange "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/ProximityListener;->m_farValue:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v0, Lcom/sgiggle/production/ProximityListener$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ProximityListener$1;-><init>(Lcom/sgiggle/production/ProximityListener;)V

    iput-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorListener:Landroid/hardware/SensorEventListener;

    .line 56
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M900"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/ProximityListener;->isSamsungMoment:Z

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sgiggle/production/ProximityListener;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ProximityListener;->onSensorEvent(F)V

    return-void
.end method

.method private static isDeviceNexusOne2_2()Z
    .locals 2

    .prologue
    .line 126
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus One"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onSensorEvent(F)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 85
    iget-boolean v0, p0, Lcom/sgiggle/production/ProximityListener;->isSamsungMoment:Z

    if-eqz v0, :cond_2

    .line 87
    const/high16 v0, 0x3f80

    cmpl-float v0, p1, v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

    invoke-interface {v0, v4}, Lcom/sgiggle/production/ProximityListener$EventListener;->proximityChanged(Z)V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

    invoke-interface {v0, v1}, Lcom/sgiggle/production/ProximityListener$EventListener;->proximityChanged(Z)V

    goto :goto_0

    .line 102
    :cond_2
    iget v0, p0, Lcom/sgiggle/production/ProximityListener;->m_farValue:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_4

    const/high16 v0, 0x4040

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_4

    .line 110
    float-to-double v0, p1

    const-wide/high16 v2, 0x3ff0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_3

    invoke-static {}, Lcom/sgiggle/production/ProximityListener;->isDeviceNexusOne2_2()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

    invoke-interface {v0, v4}, Lcom/sgiggle/production/ProximityListener$EventListener;->proximityChanged(Z)V

    goto :goto_0

    .line 116
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_listener:Lcom/sgiggle/production/ProximityListener$EventListener;

    invoke-interface {v0, v1}, Lcom/sgiggle/production/ProximityListener$EventListener;->proximityChanged(Z)V

    goto :goto_0
.end method


# virtual methods
.method public enable(Z)V
    .locals 4
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_proximitySensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_0

    .line 74
    :goto_0
    return-void

    .line 68
    :cond_0
    if-eqz p1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/sgiggle/production/ProximityListener;->m_proximitySensor:Landroid/hardware/Sensor;

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    goto :goto_0

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sgiggle/production/ProximityListener;->m_sensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method
