.class Lcom/sgiggle/production/PurchaseProxyActivity$1;
.super Ljava/lang/Object;
.source "PurchaseProxyActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/PurchaseProxyActivity;->postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/PurchaseProxyActivity;

.field final synthetic val$developerPayload:Ljava/lang/String;

.field final synthetic val$orderId:Ljava/lang/String;

.field final synthetic val$productId:Ljava/lang/String;

.field final synthetic val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field final synthetic val$purchaseTime:J

.field final synthetic val$signature:Ljava/lang/String;

.field final synthetic val$signedData:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/PurchaseProxyActivity;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    iput-object p1, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->this$0:Lcom/sgiggle/production/PurchaseProxyActivity;

    iput-object p2, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    iput-object p3, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$productId:Ljava/lang/String;

    iput-object p4, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$orderId:Ljava/lang/String;

    iput-wide p5, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$purchaseTime:J

    iput-object p7, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$developerPayload:Ljava/lang/String;

    iput-object p8, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$signedData:Ljava/lang/String;

    iput-object p9, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$signature:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->this$0:Lcom/sgiggle/production/PurchaseProxyActivity;

    iget-object v1, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    iget-object v2, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$productId:Ljava/lang/String;

    iget-object v3, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$orderId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$purchaseTime:J

    iget-object v6, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$developerPayload:Ljava/lang/String;

    iget-object v7, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$signedData:Ljava/lang/String;

    iget-object v8, p0, Lcom/sgiggle/production/PurchaseProxyActivity$1;->val$signature:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/sgiggle/production/PurchaseProxyActivity;->postPurchaseStateChange2(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    return-void
.end method
