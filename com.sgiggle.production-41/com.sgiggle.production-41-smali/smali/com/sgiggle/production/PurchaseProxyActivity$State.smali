.class final enum Lcom/sgiggle/production/PurchaseProxyActivity$State;
.super Ljava/lang/Enum;
.source "PurchaseProxyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/PurchaseProxyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/PurchaseProxyActivity$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum CONNECTING:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum DONE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum INVALID:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum IN_STORE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum STORE_AVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum STORE_FINISHED:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum STORE_UNAVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

.field public static final enum WAITING_FOR_CONFIRMATION:Lcom/sgiggle/production/PurchaseProxyActivity$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 67
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->INVALID:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 68
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->CONNECTING:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 69
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "STORE_AVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_AVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 70
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "STORE_UNAVAILABLE"

    invoke-direct {v0, v1, v6}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_UNAVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 71
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "IN_STORE"

    invoke-direct {v0, v1, v7}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->IN_STORE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 72
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "STORE_FINISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_FINISHED:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 73
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "WAITING_FOR_CONFIRMATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->WAITING_FOR_CONFIRMATION:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 74
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    const-string v1, "DONE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/PurchaseProxyActivity$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->DONE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 66
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sgiggle/production/PurchaseProxyActivity$State;

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->INVALID:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->CONNECTING:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_AVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_UNAVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->IN_STORE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_FINISHED:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/production/PurchaseProxyActivity$State;->WAITING_FOR_CONFIRMATION:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sgiggle/production/PurchaseProxyActivity$State;->DONE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->$VALUES:[Lcom/sgiggle/production/PurchaseProxyActivity$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/PurchaseProxyActivity$State;
    .locals 1
    .parameter

    .prologue
    .line 66
    const-class v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/PurchaseProxyActivity$State;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->$VALUES:[Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {v0}, [Lcom/sgiggle/production/PurchaseProxyActivity$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/PurchaseProxyActivity$State;

    return-object v0
.end method
