.class public Lcom/sgiggle/production/PurchaseProxyActivity;
.super Landroid/app/Activity;
.source "PurchaseProxyActivity.java"

# interfaces
.implements Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/PurchaseProxyActivity$3;,
        Lcom/sgiggle/production/PurchaseProxyActivity$State;
    }
.end annotation


# static fields
.field private static final DIALOG_BILLING_NOT_SUPPORTED:I = 0x2

.field private static final DIALOG_SHOW_ERROR:I = 0x1

.field public static final EXTRA_EXTERNAL_MARKET_ID:Ljava/lang/String; = "PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID"

.field public static final EXTRA_PRODUCT_ID:Ljava/lang/String; = "PurchaseProxyActivity.EXTRA_PRODUCT_ID"

.field private static final MSG_BUY:I = 0x2

.field private static final MSG_SHOW_UI:I = 0x1

.field private static final SHOW_UI_DELAY_MS:I = 0x3e8

.field private static final TAG:Ljava/lang/String; = "PurchaseProxyActivity"


# instance fields
.field private mExternalMarketId:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field private mProductId:Ljava/lang/String;

.field private mResumed:Z

.field private mState:Lcom/sgiggle/production/PurchaseProxyActivity$State;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 77
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->INVALID:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    iput-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mState:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mResumed:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/PurchaseProxyActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    return-void
.end method

.method private buyProduct()V
    .locals 3

    .prologue
    .line 162
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mExternalMarketId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mProductId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/payments/BillingServiceManager;->requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z

    .line 163
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->IN_STORE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 164
    return-void
.end method

.method private getDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter

    .prologue
    .line 289
    packed-switch p1, :pswitch_data_0

    .line 299
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 291
    :pswitch_0
    invoke-static {p0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getPurchaseFailedDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    .line 292
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 295
    :pswitch_1
    invoke-static {p0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getBillingNotSupportedDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    .line 296
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getDismissListener()Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto :goto_0

    .line 289
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getDismissListener()Landroid/content/DialogInterface$OnDismissListener;
    .locals 1

    .prologue
    .line 312
    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/PurchaseProxyActivity$2;-><init>(Lcom/sgiggle/production/PurchaseProxyActivity;)V

    return-object v0
.end method

.method private hideUi()V
    .locals 2

    .prologue
    .line 158
    const v0, 0x7f0a0083

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    return-void
.end method

.method private onTransactionDone(Z)V
    .locals 1
    .parameter

    .prologue
    .line 167
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->DONE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 168
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setResult(I)V

    .line 169
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->finish()V

    .line 170
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showUi()V
    .locals 2

    .prologue
    .line 154
    const v0, 0x7f0a0083

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 132
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 141
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 134
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->showUi()V

    move v0, v1

    .line 135
    goto :goto_0

    .line 138
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->buyProduct()V

    move v0, v1

    .line 139
    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 147
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mState:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$State;->WAITING_FOR_CONFIRMATION:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    if-ne v0, v1, :cond_0

    .line 148
    const v0, 0x7f09018c

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 150
    :cond_0
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/PurchaseProxyActivity;->setResult(I)V

    .line 151
    return-void
.end method

.method public onBillingSupported(Z)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x2

    .line 203
    if-eqz p1, :cond_0

    .line 204
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_AVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 205
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 210
    :goto_0
    return-void

    .line 207
    :cond_0
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_UNAVAILABLE:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 208
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/PurchaseProxyActivity;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    const v0, 0x7f03003f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setContentView(I)V

    .line 91
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->hideUi()V

    .line 93
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PurchaseProxyActivity.EXTRA_PRODUCT_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mProductId:Ljava/lang/String;

    .line 94
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mExternalMarketId:Ljava/lang/String;

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mProductId:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mExternalMarketId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 97
    :cond_0
    const-string v0, "PurchaseProxyActivity"

    const-string v1, "Missing product id or external market id."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setResult(I)V

    .line 99
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->finish()V

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_1
    const-string v0, "PurchaseProxyActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Purchase:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mProductId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/payments/BillingServiceManager;->bindServiceAndSetContext(Landroid/content/Context;)V

    .line 104
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->CONNECTING:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 304
    invoke-direct {p0, p1}, Lcom/sgiggle/production/PurchaseProxyActivity;->getDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_0

    .line 308
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 280
    invoke-direct {p0, p1}, Lcom/sgiggle/production/PurchaseProxyActivity;->getDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 281
    if-eqz v0, :cond_0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 127
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->unbindService()V

    .line 128
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 119
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mResumed:Z

    .line 120
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/payments/ResponseHandler;->unregisterObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)Z

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 122
    return-void
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 249
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResponseCodeReceived, responseCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->STORE_FINISHED:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    .line 251
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$3;->$SwitchMap$com$sgiggle$production$payments$Constants$ResponseCode:[I

    invoke-virtual {p2}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 271
    :cond_0
    :goto_0
    return-void

    .line 253
    :pswitch_0
    sget-object v0, Lcom/sgiggle/production/PurchaseProxyActivity$State;->WAITING_FOR_CONFIRMATION:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V

    goto :goto_0

    .line 256
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    goto :goto_0

    .line 260
    :pswitch_2
    invoke-direct {p0, v3}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    goto :goto_0

    .line 266
    :pswitch_3
    iget-boolean v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mResumed:Z

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->showDialog(I)V

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 276
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 110
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 111
    iput-boolean v3, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mResumed:Z

    .line 112
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/payments/ResponseHandler;->registerObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)V

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 114
    return-void
.end method

.method public postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 227
    iget-object v10, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/sgiggle/production/PurchaseProxyActivity$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/sgiggle/production/PurchaseProxyActivity$1;-><init>(Lcom/sgiggle/production/PurchaseProxyActivity;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 234
    return-void
.end method

.method public postPurchaseStateChange2(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mProductId:Ljava/lang/String;

    if-ne p2, v0, :cond_0

    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne p1, v0, :cond_0

    .line 240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    .line 245
    :goto_0
    return-void

    .line 242
    :cond_0
    const-string v0, "PurchaseProxyActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got confirmation for different product:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    goto :goto_0
.end method

.method setState(Lcom/sgiggle/production/PurchaseProxyActivity$State;)V
    .locals 0
    .parameter

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mState:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    .line 174
    invoke-virtual {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->updateUI()V

    .line 175
    return-void
.end method

.method public startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 214
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->skipWelcomePageOnce()V

    .line 216
    :try_start_0
    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/PurchaseProxyActivity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 220
    :goto_0
    return-void

    .line 217
    :catch_0
    move-exception v0

    .line 218
    invoke-direct {p0, v6}, Lcom/sgiggle/production/PurchaseProxyActivity;->onTransactionDone(Z)V

    goto :goto_0
.end method

.method updateUI()V
    .locals 3

    .prologue
    .line 178
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/PurchaseProxyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 179
    sget-object v1, Lcom/sgiggle/production/PurchaseProxyActivity$3;->$SwitchMap$com$sgiggle$production$PurchaseProxyActivity$State:[I

    iget-object v2, p0, Lcom/sgiggle/production/PurchaseProxyActivity;->mState:Lcom/sgiggle/production/PurchaseProxyActivity$State;

    invoke-virtual {v2}, Lcom/sgiggle/production/PurchaseProxyActivity$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 199
    :goto_0
    :pswitch_0
    return-void

    .line 188
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->hideUi()V

    goto :goto_0

    .line 191
    :pswitch_2
    const v1, 0x7f09018b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 192
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->showUi()V

    goto :goto_0

    .line 195
    :pswitch_3
    invoke-direct {p0}, Lcom/sgiggle/production/PurchaseProxyActivity;->hideUi()V

    goto :goto_0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
