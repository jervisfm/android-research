.class public Lcom/sgiggle/production/PushMsgNotifier;
.super Ljava/lang/Object;
.source "PushMsgNotifier.java"


# static fields
.field public static final ACTION_CALL_ACCOUNT_ID:Ljava/lang/String; = "accountid"

.field public static final ACTION_CALL_FIRST_NAME:Ljava/lang/String; = "firstname"

.field public static final ACTION_CALL_LAST_NAME:Ljava/lang/String; = "lastname"

.field public static final ACTION_INFO:Ljava/lang/String; = "actioninfo"

.field public static final ACTION_PUSH_MESSAGE_CONVERSATION_ID:Ljava/lang/String; = "conversationid"

.field public static final ACTION_SHOW_CONVERSATION:Ljava/lang/String; = "tango.broadcast.show_conversation"

.field public static final ACTION_SHOW_CONVERSATION_LIST:Ljava/lang/String; = "tango.broadcast.show_conversation_list"

.field public static final ACTION_TYPE:Ljava/lang/String; = "actiontype"

.field private static final TAG:Ljava/lang/String; = "Tango.PushMsgNotifier"

.field private static s_me:Lcom/sgiggle/production/PushMsgNotifier;


# instance fields
.field private m_application:Lcom/sgiggle/production/TangoApp;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/TangoApp;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/sgiggle/production/PushMsgNotifier;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 46
    return-void
.end method

.method static getDefault()Lcom/sgiggle/production/PushMsgNotifier;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sgiggle/production/PushMsgNotifier;->s_me:Lcom/sgiggle/production/PushMsgNotifier;

    return-object v0
.end method

.method static init(Lcom/sgiggle/production/TangoApp;)V
    .locals 1
    .parameter

    .prologue
    .line 49
    new-instance v0, Lcom/sgiggle/production/PushMsgNotifier;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/PushMsgNotifier;-><init>(Lcom/sgiggle/production/TangoApp;)V

    sput-object v0, Lcom/sgiggle/production/PushMsgNotifier;->s_me:Lcom/sgiggle/production/PushMsgNotifier;

    .line 50
    return-void
.end method

.method public static updateConversationMessageNotificationInStatusBar(Landroid/content/Context;Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;)V
    .locals 15
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 99
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUpdateNotification()Z

    move-result v0

    if-nez v0, :cond_3

    .line 100
    const-string v0, "Tango.PushMsgNotifier"

    const-string v2, "updateConversationMessageNotificationInStatusBar() payload mentions not to update notification, skipping event"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getPlayAlertSound()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {}, Lcom/sgiggle/production/manager/MediaManager;->getInstance()Lcom/sgiggle/production/manager/MediaManager;

    move-result-object v0

    if-eqz v1, :cond_2

    const/high16 v1, 0x7f06

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v2

    invoke-virtual {v0, p0, v1, v2}, Lcom/sgiggle/production/manager/MediaManager;->playAudioResourceAsNotification(Landroid/content/Context;IZ)V

    .line 233
    :cond_0
    :goto_2
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 102
    :cond_2
    const v1, 0x7f060002

    goto :goto_1

    .line 109
    :cond_3
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 112
    if-eqz v1, :cond_5

    .line 114
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    const-string v2, "tango.broadcast.show_conversation_list"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 116
    const/high16 v2, 0x3000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    const/4 v2, 0x0

    const/high16 v3, 0x800

    invoke-static {p0, v2, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09015a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 120
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09015b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 122
    new-instance v4, Landroid/app/Notification;

    const v5, 0x7f0200ab

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-direct {v4, v5, v3, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 123
    iput-object v3, v4, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 124
    invoke-virtual {v4, p0, v2, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 125
    iget v1, v4, Landroid/app/Notification;->defaults:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v4, Landroid/app/Notification;->defaults:I

    .line 126
    iget v1, v4, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v4, Landroid/app/Notification;->flags:I

    .line 127
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getPlayAlertSound()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 128
    const/high16 v1, 0x7f06

    invoke-static {v1}, Lcom/sgiggle/production/Utils;->getResourceUri(I)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v4, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 130
    :cond_4
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_2

    .line 137
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUnreadMessageCount()I

    move-result v1

    .line 139
    const-string v2, "Tango.PushMsgNotifier"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateConversationMessageNotificationInStatusBar: unreadCount ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", conversation ID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    if-nez v1, :cond_6

    .line 142
    const-string v1, "Tango.PushMsgNotifier"

    const-string v2, "Removing Conversation push message from notification bar"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto/16 :goto_2

    .line 147
    :cond_6
    const-string v2, "Tango.PushMsgNotifier"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating Conversation push message to notification bar, #messages ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v2, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    .line 151
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/sgiggle/production/model/ConversationMessageFactory;->create(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v3

    .line 154
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    const-string v5, "tango.broadcast.show_conversation"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v5, "conversationid"

    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const/high16 v5, 0x3000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 159
    const/4 v5, 0x0

    const/high16 v6, 0x800

    invoke-static {p0, v5, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 162
    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    .line 163
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lcom/sgiggle/production/model/ConversationMessage;->getText(Z)Ljava/lang/String;

    move-result-object v6

    .line 164
    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationMessage;->getTimestampMs()J

    move-result-wide v7

    .line 165
    invoke-virtual/range {p1 .. p1}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getPlayAlertSound()Z

    move-result v3

    if-eqz v3, :cond_a

    const v3, 0x7f060002

    invoke-static {v3}, Lcom/sgiggle/production/Utils;->getResourceUri(I)Landroid/net/Uri;

    move-result-object v3

    .line 166
    :goto_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090159

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v6, v11, v12

    invoke-virtual {v9, v10, v11}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 171
    const/4 v10, 0x0

    .line 172
    sget v11, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xb

    if-lt v11, v12, :cond_d

    .line 173
    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 174
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v10, 0x7f020091

    invoke-static {v2, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 196
    :cond_7
    :goto_4
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x1050005

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 198
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x1050006

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 201
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    if-eq v12, v10, :cond_8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    if-eq v12, v11, :cond_8

    .line 202
    const/4 v12, 0x1

    invoke-static {v2, v10, v11, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 205
    :cond_8
    new-instance v10, Landroid/app/Notification$Builder;

    invoke-direct {v10, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 206
    invoke-virtual {v10, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v4

    const v11, 0x7f0200ab

    invoke-virtual {v4, v11}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setNumber(I)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v7, v8}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 217
    invoke-virtual {v10}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v1

    .line 229
    :goto_5
    if-eqz v3, :cond_9

    .line 230
    iput-object v3, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 232
    :cond_9
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_2

    .line 165
    :cond_a
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 177
    :cond_b
    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v11

    .line 178
    const-wide/16 v13, -0x1

    cmp-long v2, v11, v13

    if-eqz v2, :cond_c

    .line 180
    :try_start_0
    invoke-static {v11, v12}, Lcom/sgiggle/contacts/ContactStore;->getPhotoByContactId(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 190
    :goto_6
    if-nez v2, :cond_7

    .line 191
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v10, 0x7f020090

    invoke-static {v2, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    goto/16 :goto_4

    .line 181
    :catch_0
    move-exception v2

    .line 182
    const-string v2, "Tango.PushMsgNotifier"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updateConversationMessageNotificationInStatusBar(): cannot get photo for contactId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v10

    .line 184
    goto :goto_6

    .line 186
    :cond_c
    const-string v2, "Tango.PushMsgNotifier"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "updateConversationMessageNotificationInStatusBar(): cannot get photo for contactId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v2, v11}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v10

    goto :goto_6

    .line 220
    :cond_d
    new-instance v2, Landroid/app/Notification;

    const v10, 0x7f0200ab

    invoke-direct {v2, v10, v6, v7, v8}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 221
    iput-object v9, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 222
    invoke-virtual {v2, p0, v5, v6, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 223
    iget v4, v2, Landroid/app/Notification;->defaults:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/app/Notification;->defaults:I

    .line 224
    iput v1, v2, Landroid/app/Notification;->number:I

    .line 225
    iget v1, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v2, Landroid/app/Notification;->flags:I

    move-object v1, v2

    goto :goto_5
.end method


# virtual methods
.method public notifyPushMessageInStatusBar(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    const-string v0, "Tango.PushMsgNotifier"

    const-string v1, "notifyPushMessageInStatusBar()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/PushMsgNotifier;->m_application:Lcom/sgiggle/production/TangoApp;

    .line 65
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 66
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f09008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 72
    :goto_0
    new-instance v2, Landroid/content/Intent;

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v3

    const-class v4, Lcom/sgiggle/production/PopupNotification;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 73
    const-string v3, "title"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 74
    const-string v3, "body"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 75
    const-string v3, "actioninfo"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 76
    const/high16 v3, 0x1004

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 77
    const/4 v3, 0x0

    const/high16 v4, 0x800

    invoke-static {v0, v3, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 81
    new-instance v3, Landroid/app/Notification;

    const v4, 0x7f0200ab

    const-wide/16 v5, 0x0

    invoke-direct {v3, v4, v1, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 82
    invoke-virtual {v3, v0, v1, p2, v2}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 83
    iget v0, v3, Landroid/app/Notification;->flags:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v3, Landroid/app/Notification;->flags:I

    .line 85
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 86
    return-void

    :cond_0
    move-object v1, p1

    .line 66
    goto :goto_0
.end method
