.class public Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "QuerySendSMSDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/QuerySendSMSDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private activity:Lcom/sgiggle/production/QuerySendSMSDialogActivity;

.field private askToSendSms:Z

.field private payload:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 46
    check-cast p1, Lcom/sgiggle/production/QuerySendSMSDialogActivity;

    iput-object p1, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->activity:Lcom/sgiggle/production/QuerySendSMSDialogActivity;

    .line 47
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;)Z
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->askToSendSms:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->postStartSMSComposeMessage(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;)Lcom/sgiggle/production/QuerySendSMSDialogActivity;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->activity:Lcom/sgiggle/production/QuerySendSMSDialogActivity;

    return-object v0
.end method

.method private postStartSMSComposeMessage(Z)V
    .locals 4
    .parameter

    .prologue
    .line 93
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    iget-object v1, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->payload:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->payload:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceiversList()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->payload:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLjava/util/List;Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 98
    return-void
.end method


# virtual methods
.method public create(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Landroid/app/AlertDialog;
    .locals 3
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->payload:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    .line 51
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getAskToSendSms()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->askToSendSms:Z

    .line 52
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogTitle()Ljava/lang/String;

    move-result-object v0

    .line 53
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getDialogMessage()Ljava/lang/String;

    move-result-object v1

    .line 55
    const v2, 0x7f020092

    invoke-virtual {p0, v2}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09001f

    new-instance v2, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$1;-><init>(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    iget-boolean v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->askToSendSms:Z

    if-eqz v0, :cond_0

    .line 69
    const v0, 0x7f090020

    new-instance v1, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$2;-><init>(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 78
    :cond_0
    new-instance v0, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder$3;-><init>(Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 85
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
