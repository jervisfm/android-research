.class public Lcom/sgiggle/production/QuerySendSMSDialogActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "QuerySendSMSDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.QuerySendSMSDialogActivity"

.field static VALIDATION_FAILED_DIALOG:I


# instance fields
.field private message:Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    sput v0, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->VALIDATION_FAILED_DIALOG:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 38
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 23
    const-string v0, "Tango.QuerySendSMSDialogActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-virtual {p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    iput-object v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->message:Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    .line 27
    sget v0, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->VALIDATION_FAILED_DIALOG:I

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->showDialog(I)V

    .line 29
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter

    .prologue
    .line 33
    const-string v0, "Tango.QuerySendSMSDialogActivity"

    const-string v1, "onCreateDialog()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v1, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    iget-object v0, p0, Lcom/sgiggle/production/QuerySendSMSDialogActivity;->message:Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/QuerySendSMSDialogActivity$Builder;->create(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
