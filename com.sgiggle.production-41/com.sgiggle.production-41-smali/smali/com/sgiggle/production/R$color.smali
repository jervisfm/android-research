.class public final Lcom/sgiggle/production/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_background_color:I = 0x7f070014

.field public static final actionbar_title_color:I = 0x7f070015

.field public static final android_black:I = 0x7f07001d

.field public static final black:I = 0x7f070000

.field public static final border_gray:I = 0x7f070009

.field public static final button_textcolor:I = 0x7f070035

.field public static final call_log_all_color:I = 0x7f07001b

.field public static final call_log_missed_color:I = 0x7f07001c

.field public static final common_list_divider_color:I = 0x7f070026

.field public static final contact_footer:I = 0x7f070027

.field public static final contact_list_separator_background:I = 0x7f070028

.field public static final contact_list_separator_background_compact:I = 0x7f07002a

.field public static final contact_list_separator_text:I = 0x7f070029

.field public static final dark_bar_background:I = 0x7f070018

.field public static final email_verification_background:I = 0x7f07002b

.field public static final incall_textConnected:I = 0x7f070019

.field public static final incall_textEnded:I = 0x7f07001a

.field public static final light_bar_background:I = 0x7f070016

.field public static final light_bar_separator:I = 0x7f070017

.field public static final navy_blue:I = 0x7f070005

.field public static final post_call_button_text_color1:I = 0x7f070020

.field public static final red:I = 0x7f070002

.field public static final selection_background_color:I = 0x7f070012

.field public static final shade_layer:I = 0x7f07000a

.field public static final sliding_tab_text_color_active:I = 0x7f07001e

.field public static final sliding_tab_text_color_shadow:I = 0x7f07001f

.field public static final tab_bottom_line1:I = 0x7f07000e

.field public static final tab_bottom_line2:I = 0x7f07000f

.field public static final tab_pressed:I = 0x7f07000d

.field public static final tab_selected:I = 0x7f07000b

.field public static final tab_text:I = 0x7f070010

.field public static final tab_unselected:I = 0x7f07000c

.field public static final tango_gray:I = 0x7f070007

.field public static final tango_orange:I = 0x7f070006

.field public static final tc_conversation_detail_list_background:I = 0x7f07002d

.field public static final tc_conversation_list_background:I = 0x7f07002c

.field public static final tc_conversation_list_summary_read:I = 0x7f070033

.field public static final tc_conversation_list_summary_unread:I = 0x7f070032

.field public static final tc_link:I = 0x7f070031

.field public static final tc_text_dark_grey:I = 0x7f07002e

.field public static final tc_text_light_grey:I = 0x7f070030

.field public static final tc_text_medium_grey:I = 0x7f07002f

.field public static final tc_vgood_selector_background:I = 0x7f070034

.field public static final text_dark:I = 0x7f070008

.field public static final text_disabled:I = 0x7f070004

.field public static final toggle_background:I = 0x7f070023

.field public static final toggle_separator:I = 0x7f070025

.field public static final toggle_text:I = 0x7f070024

.field public static final transparent:I = 0x7f070003

.field public static final vgood_row_dark:I = 0x7f070021

.field public static final vgood_row_light:I = 0x7f070022

.field public static final view_background_color:I = 0x7f070011

.field public static final view_text_color:I = 0x7f070013

.field public static final white:I = 0x7f070001


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
