.class public final Lcom/sgiggle/production/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alert_icon_android:I = 0x7f020000

.field public static final alert_panel:I = 0x7f020001

.field public static final android120:I = 0x7f020002

.field public static final appwidget_inner_focus_c:I = 0x7f020003

.field public static final appwidget_inner_press_c:I = 0x7f020004

.field public static final appwidget_settings_divider:I = 0x7f020005

.field public static final appwidget_settings_ind_off_c:I = 0x7f020006

.field public static final appwidget_settings_ind_off_c_portrait:I = 0x7f020007

.field public static final appwidget_settings_ind_on_c:I = 0x7f020008

.field public static final appwidget_settings_ind_on_c_portrait:I = 0x7f020009

.field public static final audiocall_footer:I = 0x7f02000a

.field public static final audiocall_header:I = 0x7f02000b

.field public static final avatar_demo_background:I = 0x7f02000c

.field public static final avatar_focused_background:I = 0x7f02000d

.field public static final bg_in_call_gradient_unidentified:I = 0x7f02000e

.field public static final bg_tc_play_button:I = 0x7f02000f

.field public static final bluebutton_pressed:I = 0x7f020010

.field public static final bluebutton_static:I = 0x7f020011

.field public static final bt_dark_bg_normal:I = 0x7f020012

.field public static final bt_dark_bg_pressed:I = 0x7f020013

.field public static final bt_record_bg_normal:I = 0x7f020014

.field public static final bt_record_bg_normal_rotated:I = 0x7f020015

.field public static final bt_record_bg_pressed:I = 0x7f020016

.field public static final bt_record_bg_pressed_rotated:I = 0x7f020017

.field public static final bt_record_light_off:I = 0x7f020018

.field public static final bt_record_light_off_rotated:I = 0x7f020019

.field public static final bt_record_light_on:I = 0x7f02001a

.field public static final bt_record_light_on_rotated:I = 0x7f02001b

.field public static final bt_switch_camera_normal:I = 0x7f02001c

.field public static final bt_switch_camera_pressed:I = 0x7f02001d

.field public static final bt_videomail_thumbnail_play:I = 0x7f02001e

.field public static final btn_answer_call:I = 0x7f02001f

.field public static final btn_answer_normal:I = 0x7f020020

.field public static final btn_answer_pressed:I = 0x7f020021

.field public static final btn_close:I = 0x7f020022

.field public static final btn_close_normal:I = 0x7f020023

.field public static final btn_close_pressed:I = 0x7f020024

.field public static final btn_contact_detail:I = 0x7f020025

.field public static final btn_dark_bg:I = 0x7f020026

.field public static final btn_decline_call:I = 0x7f020027

.field public static final btn_decline_normal:I = 0x7f020028

.field public static final btn_decline_pressed:I = 0x7f020029

.field public static final btn_in_call_main_disable:I = 0x7f02002a

.field public static final btn_in_call_main_disable_focused:I = 0x7f02002b

.field public static final btn_in_call_main_normal:I = 0x7f02002c

.field public static final btn_in_call_main_pressed:I = 0x7f02002d

.field public static final btn_in_call_main_selected:I = 0x7f02002e

.field public static final btn_in_call_switch_off_disable:I = 0x7f02002f

.field public static final btn_in_call_switch_off_disable_focused:I = 0x7f020030

.field public static final btn_in_call_switch_off_normal:I = 0x7f020031

.field public static final btn_in_call_switch_off_pressed:I = 0x7f020032

.field public static final btn_in_call_switch_off_selected:I = 0x7f020033

.field public static final btn_in_call_switch_on_disable:I = 0x7f020034

.field public static final btn_in_call_switch_on_disable_focused:I = 0x7f020035

.field public static final btn_in_call_switch_on_normal:I = 0x7f020036

.field public static final btn_in_call_switch_on_pressed:I = 0x7f020037

.field public static final btn_in_call_switch_on_selected:I = 0x7f020038

.field public static final btn_pictureviewer_more_operations:I = 0x7f020039

.field public static final btn_postcall_blue:I = 0x7f02003a

.field public static final btn_postcall_grey:I = 0x7f02003b

.field public static final btn_sns_share:I = 0x7f02003c

.field public static final btn_switch_camera_dark_land:I = 0x7f02003d

.field public static final btn_switch_camera_white_land:I = 0x7f02003e

.field public static final btn_videomail_cancel:I = 0x7f02003f

.field public static final btn_videomail_cancel_disabled:I = 0x7f020040

.field public static final btn_videomail_cancel_disabled_portrait:I = 0x7f020041

.field public static final btn_videomail_cancel_normal:I = 0x7f020042

.field public static final btn_videomail_cancel_normal_portrait:I = 0x7f020043

.field public static final btn_videomail_cancel_portrait:I = 0x7f020044

.field public static final btn_videomail_cancel_pressed:I = 0x7f020045

.field public static final btn_videomail_cancel_pressed_portrait:I = 0x7f020046

.field public static final btn_videomail_delete:I = 0x7f020047

.field public static final btn_videomail_delete_disabled:I = 0x7f020048

.field public static final btn_videomail_delete_normal:I = 0x7f020049

.field public static final btn_videomail_delete_pressed:I = 0x7f02004a

.field public static final btn_videomail_pause:I = 0x7f02004b

.field public static final btn_videomail_pause_disabled:I = 0x7f02004c

.field public static final btn_videomail_pause_normal:I = 0x7f02004d

.field public static final btn_videomail_pause_pressed:I = 0x7f02004e

.field public static final btn_videomail_play:I = 0x7f02004f

.field public static final btn_videomail_play_disabled:I = 0x7f020050

.field public static final btn_videomail_play_normal:I = 0x7f020051

.field public static final btn_videomail_play_pressed:I = 0x7f020052

.field public static final btn_videomail_recording:I = 0x7f020053

.field public static final btn_videomail_recording_portrait:I = 0x7f020054

.field public static final btn_videomail_reply:I = 0x7f020055

.field public static final btn_videomail_reply_disabled:I = 0x7f020056

.field public static final btn_videomail_reply_normal:I = 0x7f020057

.field public static final btn_videomail_reply_pressed:I = 0x7f020058

.field public static final btn_videomail_retake:I = 0x7f020059

.field public static final btn_videomail_retake_disabled:I = 0x7f02005a

.field public static final btn_videomail_retake_normal:I = 0x7f02005b

.field public static final btn_videomail_retake_pressed:I = 0x7f02005c

.field public static final btn_videomail_send:I = 0x7f02005d

.field public static final btn_videomail_send_disabled:I = 0x7f02005e

.field public static final btn_videomail_send_disabled_portrait:I = 0x7f02005f

.field public static final btn_videomail_send_normal:I = 0x7f020060

.field public static final btn_videomail_send_normal_portrait:I = 0x7f020061

.field public static final btn_videomail_send_portrait:I = 0x7f020062

.field public static final btn_videomail_send_pressed:I = 0x7f020063

.field public static final btn_videomail_send_pressed_portrait:I = 0x7f020064

.field public static final btn_wand:I = 0x7f020065

.field public static final btn_wand_land:I = 0x7f020066

.field public static final bubble_body:I = 0x7f020067

.field public static final bubble_body_land:I = 0x7f020068

.field public static final bubble_bottom_anchor_right:I = 0x7f020069

.field public static final bubble_bottom_anchor_right_land:I = 0x7f02006a

.field public static final bubble_bottom_arrow:I = 0x7f02006b

.field public static final bubble_bottom_arrow_land:I = 0x7f02006c

.field public static final bubble_bottom_top:I = 0x7f02006d

.field public static final bubble_bottom_top_land:I = 0x7f02006e

.field public static final bubble_close_btn:I = 0x7f02006f

.field public static final bubble_top_arrow:I = 0x7f020070

.field public static final bubble_top_arrow_land:I = 0x7f020071

.field public static final bubble_top_bottom:I = 0x7f020072

.field public static final bubble_top_bottom_land:I = 0x7f020073

.field public static final button_facebook_normal:I = 0x7f020074

.field public static final button_facebook_pressed:I = 0x7f020075

.field public static final caret:I = 0x7f020076

.field public static final close:I = 0x7f020077

.field public static final conversation_list_divider:I = 0x7f020078

.field public static final debug_info_panel:I = 0x7f020079

.field public static final default_selector_transparent:I = 0x7f02007a

.field public static final demo_background:I = 0x7f02007b

.field public static final detail_audio_icon:I = 0x7f02007c

.field public static final detail_conversation_icon:I = 0x7f02007d

.field public static final detail_page_seperate_line:I = 0x7f02007e

.field public static final detail_staricon_favorite:I = 0x7f02007f

.field public static final detail_staricon_notfavorite:I = 0x7f020080

.field public static final detail_video_icon:I = 0x7f020081

.field public static final dialog_no_call_answer:I = 0x7f020082

.field public static final divider_vertical_bright:I = 0x7f020083

.field public static final exclamation_triangle_2:I = 0x7f020084

.field public static final facebook_icon:I = 0x7f020085

.field public static final fb_login_button:I = 0x7f020086

.field public static final fb_logout:I = 0x7f020087

.field public static final fb_logout_button:I = 0x7f020088

.field public static final fb_logout_down:I = 0x7f020089

.field public static final greybutton_pressed:I = 0x7f02008a

.field public static final greybutton_static:I = 0x7f02008b

.field public static final ic_actionbar_up_indicator:I = 0x7f02008c

.field public static final ic_alert:I = 0x7f02008d

.field public static final ic_btn_round_more_normal:I = 0x7f02008e

.field public static final ic_cancel_light:I = 0x7f02008f

.field public static final ic_contact_thumb_default:I = 0x7f020090

.field public static final ic_contact_thumb_system_account:I = 0x7f020091

.field public static final ic_dialog_alert:I = 0x7f020092

.field public static final ic_filter_comic:I = 0x7f020093

.field public static final ic_filter_holga:I = 0x7f020094

.field public static final ic_filter_instagram:I = 0x7f020095

.field public static final ic_filter_mafia:I = 0x7f020096

.field public static final ic_hang_up:I = 0x7f020097

.field public static final ic_in_call_horizontal_end:I = 0x7f020098

.field public static final ic_in_call_horizontal_mute:I = 0x7f020099

.field public static final ic_in_call_horizontal_send_video:I = 0x7f02009a

.field public static final ic_in_call_mute:I = 0x7f02009b

.field public static final ic_in_call_speaker:I = 0x7f02009c

.field public static final ic_in_call_touch_send_video:I = 0x7f02009d

.field public static final ic_in_call_vertical_end:I = 0x7f02009e

.field public static final ic_in_call_vertical_mute:I = 0x7f02009f

.field public static final ic_in_call_vertical_send_video:I = 0x7f0200a0

.field public static final ic_menu_add:I = 0x7f0200a1

.field public static final ic_menu_delete_all:I = 0x7f0200a2

.field public static final ic_menu_help:I = 0x7f0200a3

.field public static final ic_menu_refresh:I = 0x7f0200a4

.field public static final ic_menu_search:I = 0x7f0200a5

.field public static final ic_menu_settings:I = 0x7f0200a6

.field public static final ic_outgoing_call:I = 0x7f0200a7

.field public static final ic_recording:I = 0x7f0200a8

.field public static final ic_search:I = 0x7f0200a9

.field public static final ic_send_light:I = 0x7f0200aa

.field public static final ic_stat_notify_tango:I = 0x7f0200ab

.field public static final ic_store_avatars_glow:I = 0x7f0200ac

.field public static final ic_store_games_glow:I = 0x7f0200ad

.field public static final ic_store_surprises_glow:I = 0x7f0200ae

.field public static final ic_surprise_gift:I = 0x7f0200af

.field public static final ic_tab_badge:I = 0x7f0200b0

.field public static final ic_tab_call_log:I = 0x7f0200b1

.field public static final ic_tab_contacts:I = 0x7f0200b2

.field public static final ic_tab_invite:I = 0x7f0200b3

.field public static final ic_tab_store:I = 0x7f0200b4

.field public static final ic_tab_tc:I = 0x7f0200b5

.field public static final ic_tc_actionbar_logo:I = 0x7f0200b6

.field public static final ic_tc_add_media:I = 0x7f0200b7

.field public static final ic_tc_add_text_light:I = 0x7f0200b8

.field public static final ic_tc_add_vgood:I = 0x7f0200b9

.field public static final ic_tc_arrow_1:I = 0x7f0200ba

.field public static final ic_tc_arrow_2:I = 0x7f0200bb

.field public static final ic_tc_arrow_3:I = 0x7f0200bc

.field public static final ic_tc_empty_conversation:I = 0x7f0200bd

.field public static final ic_tc_error:I = 0x7f0200be

.field public static final ic_tc_media_make_audio_call:I = 0x7f0200bf

.field public static final ic_tc_media_make_video_call:I = 0x7f0200c0

.field public static final ic_tc_media_pick_photo:I = 0x7f0200c1

.field public static final ic_tc_media_take_photo:I = 0x7f0200c2

.field public static final ic_tc_media_take_video:I = 0x7f0200c3

.field public static final ic_tc_message_bg_left_normal:I = 0x7f0200c4

.field public static final ic_tc_message_bg_left_pressed:I = 0x7f0200c5

.field public static final ic_tc_message_bg_right_normal:I = 0x7f0200c6

.field public static final ic_tc_message_bg_right_pressed:I = 0x7f0200c7

.field public static final ic_tc_multimedia:I = 0x7f0200c8

.field public static final ic_tc_send_message:I = 0x7f0200c9

.field public static final ic_tc_status_error:I = 0x7f0200ca

.field public static final ic_tc_status_sending:I = 0x7f0200cb

.field public static final ic_tc_video_call:I = 0x7f0200cc

.field public static final ic_tip_bubble_down_normal:I = 0x7f0200cd

.field public static final ic_tip_bubble_down_pressed:I = 0x7f0200ce

.field public static final ic_video_call:I = 0x7f0200cf

.field public static final ic_videomail_large:I = 0x7f0200d0

.field public static final ic_videomail_thumbnail_background:I = 0x7f0200d1

.field public static final ic_videomail_thumbnail_default:I = 0x7f0200d2

.field public static final icon:I = 0x7f0200d3

.field public static final icon_38x38:I = 0x7f0200d4

.field public static final icon_messages_settings:I = 0x7f0200d5

.field public static final icon_video_orange_24px:I = 0x7f0200d6

.field public static final icon_wand_settings:I = 0x7f0200d7

.field public static final incall_button:I = 0x7f0200d8

.field public static final incall_photo_border_lg:I = 0x7f0200d9

.field public static final incall_pip_small_border:I = 0x7f0200da

.field public static final incall_toggle_button:I = 0x7f0200db

.field public static final info_icon:I = 0x7f0200dc

.field public static final low_bandwidth_panel:I = 0x7f0200dd

.field public static final magic_wand_dark_land:I = 0x7f0200de

.field public static final magic_wand_dark_portrait:I = 0x7f0200df

.field public static final magic_wand_white_land:I = 0x7f0200e0

.field public static final magic_wand_white_portrait:I = 0x7f0200e1

.field public static final new_badge_bg:I = 0x7f0200e2

.field public static final orange_bar_bottom:I = 0x7f0200e3

.field public static final orange_bar_top:I = 0x7f0200e4

.field public static final participant_thumbnail_bg:I = 0x7f0200e5

.field public static final plus_icon:I = 0x7f0200e6

.field public static final postcall_bg:I = 0x7f0200e7

.field public static final postcall_facebook:I = 0x7f0200e8

.field public static final postcall_invite:I = 0x7f0200e9

.field public static final postcall_rate:I = 0x7f0200ea

.field public static final progress_bar_horizontal:I = 0x7f0200eb

.field public static final progress_bar_playback:I = 0x7f0200ec

.field public static final progress_bar_playback_bg:I = 0x7f0200ed

.field public static final progress_bar_playback_primary:I = 0x7f0200ee

.field public static final progress_bar_playback_secondary:I = 0x7f0200ef

.field public static final progress_bar_sending:I = 0x7f0200f0

.field public static final progress_bar_sending_bg:I = 0x7f0200f1

.field public static final progress_bar_sending_bg_portrait:I = 0x7f0200f2

.field public static final progress_bar_sending_portrait:I = 0x7f0200f3

.field public static final progress_bar_vertical:I = 0x7f0200f4

.field public static final radiogroup_down:I = 0x7f0200f5

.field public static final radiogroup_pressed:I = 0x7f0200f6

.field public static final radiogroup_toggle_indicator:I = 0x7f0200f7

.field public static final radiogroup_up:I = 0x7f0200f8

.field public static final rounded_corner:I = 0x7f0200f9

.field public static final selector_bg:I = 0x7f0200fa

.field public static final semi_transparent_color:I = 0x7f0200fb

.field public static final shadow:I = 0x7f0200fc

.field public static final sharebutton_pressed:I = 0x7f0200fd

.field public static final sharebutton_static:I = 0x7f0200fe

.field public static final sign_up_button:I = 0x7f0200ff

.field public static final switch_camera:I = 0x7f020100

.field public static final switch_camera_normal_portrait:I = 0x7f020101

.field public static final switch_camera_portrait:I = 0x7f020102

.field public static final switch_camera_pressed_portrait:I = 0x7f020103

.field public static final switch_camera_recorder:I = 0x7f020104

.field public static final tab_bg_selected:I = 0x7f020105

.field public static final tab_bg_selector:I = 0x7f020106

.field public static final tango_logotext_one:I = 0x7f020107

.field public static final tango_orange_background:I = 0x7f020108

.field public static final tango_splash:I = 0x7f020109

.field public static final tc_buy_separator:I = 0x7f02010a

.field public static final tc_message_bg_left:I = 0x7f02010b

.field public static final tc_message_bg_right:I = 0x7f02010c

.field public static final tc_vgood_bg:I = 0x7f02010d

.field public static final tip_bubble_down_bg:I = 0x7f02010e

.field public static final transparent_background:I = 0x7f020117

.field public static final vgood_empty_slot:I = 0x7f02010f

.field public static final vgood_placeholder:I = 0x7f020110

.field public static final video_button_states:I = 0x7f020111

.field public static final video_ringback_close:I = 0x7f020112

.field public static final videomail_player_scrubber:I = 0x7f020113

.field public static final videomail_ui_bar:I = 0x7f020114

.field public static final videomail_ui_bar_horizontal:I = 0x7f020115

.field public static final weiboicon:I = 0x7f020116


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
