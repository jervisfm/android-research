.class public final Lcom/sgiggle/production/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final actionbar_compat:I = 0x7f030000

.field public static final alert_bar:I = 0x7f030001

.field public static final applogin:I = 0x7f030002

.field public static final apploginerror:I = 0x7f030003

.field public static final audioinprogress:I = 0x7f030004

.field public static final avatar_bottom_bubble:I = 0x7f030005

.field public static final avatar_demo:I = 0x7f030006

.field public static final avatar_footer:I = 0x7f030007

.field public static final avatar_item:I = 0x7f030008

.field public static final avatar_subscription:I = 0x7f030009

.field public static final bottom_bubble:I = 0x7f03000a

.field public static final call_log_entry:I = 0x7f03000b

.field public static final call_log_list:I = 0x7f03000c

.field public static final call_quality_survey_detailed:I = 0x7f03000d

.field public static final call_quality_survey_quick:I = 0x7f03000e

.field public static final call_quality_survey_quick_item:I = 0x7f03000f

.field public static final call_quality_survey_quick_rating_item:I = 0x7f030010

.field public static final compose_conversation_message_fragment:I = 0x7f030011

.field public static final connection_status_fragment:I = 0x7f030012

.field public static final contact_detail:I = 0x7f030013

.field public static final contactlist:I = 0x7f030014

.field public static final contactlist_divider:I = 0x7f030015

.field public static final contactlist_divider_compact:I = 0x7f030016

.field public static final contactlist_footer:I = 0x7f030017

.field public static final contactlist_invite_item:I = 0x7f030018

.field public static final contactlistitem:I = 0x7f030019

.field public static final conversation_detail_activity:I = 0x7f03001a

.field public static final conversation_detail_empty:I = 0x7f03001b

.field public static final conversation_detail_fragment:I = 0x7f03001c

.field public static final conversation_detail_tip_layer:I = 0x7f03001d

.field public static final conversation_list_actions_fragment:I = 0x7f03001e

.field public static final conversation_list_fragment:I = 0x7f03001f

.field public static final conversation_list_item:I = 0x7f030020

.field public static final conversations_activity:I = 0x7f030021

.field public static final country_item:I = 0x7f030022

.field public static final country_list:I = 0x7f030023

.field public static final email_verification:I = 0x7f030024

.field public static final fb_abtesting_signup_bottom:I = 0x7f030025

.field public static final fb_abtesting_signup_orig:I = 0x7f030026

.field public static final fb_abtesting_signup_top:I = 0x7f030027

.field public static final game_demo_activity:I = 0x7f030028

.field public static final game_in_call_activity:I = 0x7f030029

.field public static final generic_product_demo_activity:I = 0x7f03002a

.field public static final icon_list_item:I = 0x7f03002b

.field public static final invite:I = 0x7f03002c

.field public static final invite_item:I = 0x7f03002d

.field public static final invite_list:I = 0x7f03002e

.field public static final invite_list_tabs:I = 0x7f03002f

.field public static final loading_list_spinner:I = 0x7f030030

.field public static final log_entry:I = 0x7f030031

.field public static final log_list:I = 0x7f030032

.field public static final message_list_compound_item:I = 0x7f030033

.field public static final message_list_compound_item_picture:I = 0x7f030034

.field public static final message_list_compound_item_text:I = 0x7f030035

.field public static final message_list_compound_item_vgood:I = 0x7f030036

.field public static final message_list_compound_item_videomail:I = 0x7f030037

.field public static final message_list_simple_item:I = 0x7f030038

.field public static final participant_list_item:I = 0x7f030039

.field public static final picture_preview_activity:I = 0x7f03003a

.field public static final picture_viewer:I = 0x7f03003b

.field public static final popup_notification:I = 0x7f03003c

.field public static final postcall:I = 0x7f03003d

.field public static final postcall_vgood:I = 0x7f03003e

.field public static final purchase_proxy:I = 0x7f03003f

.field public static final record_control_bar:I = 0x7f030040

.field public static final record_control_bar_portrait:I = 0x7f030041

.field public static final refresh_list_item:I = 0x7f030042

.field public static final registeruser:I = 0x7f030043

.field public static final select_contact_activity:I = 0x7f030044

.field public static final select_contact_footer_fragment:I = 0x7f030045

.field public static final select_contact_list_fragment:I = 0x7f030046

.field public static final select_contact_list_item:I = 0x7f030047

.field public static final settings:I = 0x7f030048

.field public static final settings_alert_list_row:I = 0x7f030049

.field public static final settings_copyright_version:I = 0x7f03004a

.field public static final settings_group_header:I = 0x7f03004b

.field public static final settings_tips:I = 0x7f03004c

.field public static final settings_vgoods:I = 0x7f03004d

.field public static final settings_videomail:I = 0x7f03004e

.field public static final sns_composer:I = 0x7f03004f

.field public static final sns_oauth:I = 0x7f030050

.field public static final store_activity:I = 0x7f030051

.field public static final store_category_list_fragment:I = 0x7f030052

.field public static final store_category_list_item:I = 0x7f030053

.field public static final sync_login:I = 0x7f030054

.field public static final tab:I = 0x7f030055

.field public static final tabs:I = 0x7f030056

.field public static final tango_dialog:I = 0x7f030057

.field public static final tips:I = 0x7f030058

.field public static final update_required:I = 0x7f030059

.field public static final upper_bubble:I = 0x7f03005a

.field public static final validationrequired:I = 0x7f03005b

.field public static final vgood_selector_item:I = 0x7f03005c

.field public static final vgood_selector_landscape:I = 0x7f03005d

.field public static final vgood_selector_portrait:I = 0x7f03005e

.field public static final vgoods_footer:I = 0x7f03005f

.field public static final vgoods_item:I = 0x7f030060

.field public static final vgoods_subscription:I = 0x7f030061

.field public static final videomail_alert_notification:I = 0x7f030062

.field public static final videomail_file_uploader:I = 0x7f030063

.field public static final videomail_file_uploader_rotated:I = 0x7f030064

.field public static final videomail_header_subscriptions:I = 0x7f030065

.field public static final videomail_header_trial:I = 0x7f030066

.field public static final videomail_list_fragment:I = 0x7f030067

.field public static final videomail_playback:I = 0x7f030068

.field public static final videomail_playback_controller:I = 0x7f030069

.field public static final videomail_record:I = 0x7f03006a

.field public static final videomail_record_landscape_natural:I = 0x7f03006b

.field public static final videomail_record_portrait:I = 0x7f03006c

.field public static final videomail_subscriptions:I = 0x7f03006d

.field public static final videomail_subscriptions_row:I = 0x7f03006e

.field public static final videotwoway_landscape:I = 0x7f03006f

.field public static final videotwoway_portrait:I = 0x7f030070

.field public static final videotwowaygl_landscape:I = 0x7f030071

.field public static final videotwowaygl_portrait:I = 0x7f030072

.field public static final welcome_screen:I = 0x7f030073


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 906
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
