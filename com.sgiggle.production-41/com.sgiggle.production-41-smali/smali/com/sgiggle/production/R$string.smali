.class public final Lcom/sgiggle/production/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Continue:I = 0x7f09005d

.field public static final SYNC_ACCOUNT_TYPE:I = 0x7f090087

.field public static final SYNC_ACCOUNT_USER:I = 0x7f090088

.field public static final Show:I = 0x7f0901da

.field public static final about:I = 0x7f0901b8

.field public static final accept:I = 0x7f0901ae

.field public static final account:I = 0x7f090078

.field public static final account_verification_title:I = 0x7f090170

.field public static final add_contact:I = 0x7f09003e

.field public static final add_email:I = 0x7f090185

.field public static final add_favorite:I = 0x7f0900f5

.field public static final alert_storage_low_message:I = 0x7f090120

.field public static final alert_storage_low_tittle:I = 0x7f09011f

.field public static final alert_storage_no_sdcard_message:I = 0x7f090121

.field public static final alert_title_validation_required:I = 0x7f0901b9

.field public static final alerts:I = 0x7f090079

.field public static final answer:I = 0x7f090023

.field public static final app_name:I = 0x7f090001

.field public static final app_name_in_market:I = 0x7f090002

.field public static final audio_call:I = 0x7f090091

.field public static final avatar_demo_bar_title:I = 0x7f09011c

.field public static final avatar_demo_downloading:I = 0x7f0900fc

.field public static final avatar_demo_wand_title:I = 0x7f09011b

.field public static final avatar_no_products_description:I = 0x7f0900fa

.field public static final avatar_purchased:I = 0x7f0900fb

.field public static final avatar_purchasing:I = 0x7f0900fe

.field public static final avatar_remote_unavailable:I = 0x7f09010a

.field public static final back:I = 0x7f090196

.field public static final badge_new:I = 0x7f0901d6

.field public static final bubble_video_text_off:I = 0x7f090086

.field public static final bubble_video_text_on:I = 0x7f090085

.field public static final call_log_call:I = 0x7f0901bb

.field public static final call_log_delete:I = 0x7f090084

.field public static final call_log_delete_all:I = 0x7f090082

.field public static final call_log_delete_all_alert:I = 0x7f090083

.field public static final call_log_duration_hour:I = 0x7f0901ba

.field public static final call_log_tab:I = 0x7f090009

.field public static final call_log_view_by:I = 0x7f0901db

.field public static final call_log_view_by_all:I = 0x7f090080

.field public static final call_log_view_by_missed:I = 0x7f090081

.field public static final call_log_when_today:I = 0x7f09007e

.field public static final call_log_when_yesterday:I = 0x7f09007f

.field public static final call_quality_bad_long:I = 0x7f0900d0

.field public static final call_quality_bad_short:I = 0x7f0900cd

.field public static final call_quality_details_avsync:I = 0x7f0900db

.field public static final call_quality_details_blurry:I = 0x7f0900d8

.field public static final call_quality_details_caption:I = 0x7f0900d4

.field public static final call_quality_details_delay:I = 0x7f0900d6

.field public static final call_quality_details_echo:I = 0x7f0900d5

.field public static final call_quality_details_freezing:I = 0x7f0900d9

.field public static final call_quality_details_more:I = 0x7f0900dc

.field public static final call_quality_details_rotated:I = 0x7f0900da

.field public static final call_quality_details_send:I = 0x7f0900dd

.field public static final call_quality_details_speech_unnatural:I = 0x7f0900d7

.field public static final call_quality_excellent_long:I = 0x7f0900ce

.field public static final call_quality_excellent_short:I = 0x7f0900cb

.field public static final call_quality_fair_long:I = 0x7f0900cf

.field public static final call_quality_fair_short:I = 0x7f0900cc

.field public static final call_quality_never_show_again:I = 0x7f0900d2

.field public static final call_quality_survey_details_title:I = 0x7f0900ca

.field public static final call_quality_survey_title:I = 0x7f0900c9

.field public static final call_quality_tell_more:I = 0x7f0900d1

.field public static final call_quality_thanks:I = 0x7f0900d3

.field public static final call_status_active:I = 0x7f090051

.field public static final call_status_connecting:I = 0x7f090050

.field public static final call_status_dialing:I = 0x7f09004e

.field public static final call_status_ended:I = 0x7f090052

.field public static final call_status_incoming:I = 0x7f09004f

.field public static final call_status_pre_dialing:I = 0x7f0901b4

.field public static final call_str_account_resolver_error:I = 0x7f0901c1

.field public static final call_str_audio_init_error:I = 0x7f0901be

.field public static final call_str_incomptiable_client_error:I = 0x7f0901c2

.field public static final call_str_incorrect_credential:I = 0x7f0901c3

.field public static final call_str_insufficient_network_error:I = 0x7f0901bd

.field public static final call_str_network_error:I = 0x7f0901bc

.field public static final call_str_no_premium_contact:I = 0x7f0901c4

.field public static final call_str_tango_server_error:I = 0x7f0901bf

.field public static final call_str_unknown_error:I = 0x7f0901c0

.field public static final callback:I = 0x7f09003b

.field public static final callfailure:I = 0x7f0901b0

.field public static final callfromlabel:I = 0x7f0901af

.field public static final callhandler_cannot_call_toast:I = 0x7f090118

.field public static final callmsg_name:I = 0x7f0901a8

.field public static final callnow:I = 0x7f09003c

.field public static final calls_free_for_members:I = 0x7f0900f9

.field public static final cancel:I = 0x7f090020

.field public static final choosing_existing_photo_failed:I = 0x7f090194

.field public static final code_emailed_prompt:I = 0x7f090174

.field public static final code_pushed_for_email_linking_prompt:I = 0x7f090177

.field public static final code_pushed_for_phone_linking_prompt:I = 0x7f090176

.field public static final code_smsed_prompt:I = 0x7f090181

.field public static final connection_status_connecting:I = 0x7f09016a

.field public static final connection_status_disconnected:I = 0x7f09016b

.field public static final contact_detail:I = 0x7f0900e9

.field public static final contact_details_last_call:I = 0x7f09011e

.field public static final contact_list_group_all:I = 0x7f090062

.field public static final contact_list_group_favorites:I = 0x7f090064

.field public static final contact_list_group_tango:I = 0x7f090063

.field public static final contact_list_invite_button:I = 0x7f090065

.field public static final contact_photo_description:I = 0x7f09004d

.field public static final contact_tango_support:I = 0x7f09017f

.field public static final contacts:I = 0x7f090029

.field public static final contacts_tab:I = 0x7f090008

.field public static final copyright_and_version:I = 0x7f090004

.field public static final country_code_label:I = 0x7f0901d8

.field public static final country_codes:I = 0x7f090028

.field public static final cta_background_data_message:I = 0x7f0900a9

.field public static final cta_background_data_title:I = 0x7f0900a8

.field public static final cta_google_account_missing_message:I = 0x7f0900a7

.field public static final cta_google_account_missing_title:I = 0x7f0900a6

.field public static final decline:I = 0x7f090024

.field public static final demo_bar_title:I = 0x7f09011a

.field public static final demo_wand_title:I = 0x7f090119

.field public static final dialog_call_on_hold_button_no:I = 0x7f09009a

.field public static final dialog_call_on_hold_button_yes:I = 0x7f090099

.field public static final dialog_call_on_hold_message:I = 0x7f090098

.field public static final dialog_delete_videomail_button_no:I = 0x7f09009e

.field public static final dialog_delete_videomail_button_yes:I = 0x7f09009d

.field public static final dialog_delete_videomail_message:I = 0x7f09009c

.field public static final dialog_delete_videomail_title:I = 0x7f09009b

.field public static final dialog_purchase_billing_not_supported_button_no:I = 0x7f0900a5

.field public static final dialog_purchase_billing_not_supported_button_yes:I = 0x7f0900a3

.field public static final dialog_purchase_billing_not_supported_message:I = 0x7f0900a2

.field public static final dialog_purchase_billing_not_supported_title:I = 0x7f0900a1

.field public static final dialog_purchase_failed_subscription_button_neutral:I = 0x7f0900a4

.field public static final dialog_purchase_failed_subscription_message:I = 0x7f0900a0

.field public static final dialog_purchase_failed_subscription_title:I = 0x7f09009f

.field public static final dismiss:I = 0x7f090027

.field public static final done:I = 0x7f090095

.field public static final done_button:I = 0x7f0901d0

.field public static final email_input_hint:I = 0x7f090016

.field public static final email_invite_reach_limit:I = 0x7f090092

.field public static final email_label:I = 0x7f090015

.field public static final email_request_msg:I = 0x7f090184

.field public static final email_request_title:I = 0x7f090183

.field public static final end_call_button_text:I = 0x7f090049

.field public static final enter_code_textfield_hint:I = 0x7f090179

.field public static final enter_email_code_prompt:I = 0x7f090175

.field public static final enter_push_code_prompt:I = 0x7f090178

.field public static final error62_help:I = 0x7f090188

.field public static final error62_msg:I = 0x7f090187

.field public static final error62_title:I = 0x7f090186

.field public static final facebook_app_id:I = 0x7f090000

.field public static final failed_under_low_storage_space_body:I = 0x7f090094

.field public static final failed_under_low_storage_space_header:I = 0x7f090093

.field public static final favorite:I = 0x7f0901ca

.field public static final first_name_input_hint:I = 0x7f090011

.field public static final forward_select_contact_any:I = 0x7f09016d

.field public static final forward_select_contact_zero:I = 0x7f09016c

.field public static final free:I = 0x7f0900fd

.field public static final game_demo_selector_bubble:I = 0x7f0901a4

.field public static final game_demo_wand_bubble:I = 0x7f0901a5

.field public static final game_no_products_description:I = 0x7f090104

.field public static final game_warning_local_failed:I = 0x7f090108

.field public static final game_warning_other_error:I = 0x7f090109

.field public static final game_warning_remote_failed:I = 0x7f090105

.field public static final game_warning_remote_timeout:I = 0x7f090107

.field public static final game_warning_remote_unavailable:I = 0x7f090106

.field public static final groups:I = 0x7f0901b1

.field public static final help:I = 0x7f090025

.field public static final ignore:I = 0x7f0901ad

.field public static final ignored_call_text:I = 0x7f090048

.field public static final ignored_call_title:I = 0x7f090047

.field public static final invite_contact_email:I = 0x7f09006d

.field public static final invite_contact_multiple_people:I = 0x7f09006f

.field public static final invite_contact_prompt:I = 0x7f090066

.field public static final invite_contact_send_videomail:I = 0x7f09006e

.field public static final invite_contact_text_home:I = 0x7f090069

.field public static final invite_contact_text_main:I = 0x7f09006b

.field public static final invite_contact_text_mobile:I = 0x7f090068

.field public static final invite_contact_text_other:I = 0x7f09006c

.field public static final invite_contact_text_work:I = 0x7f09006a

.field public static final invite_email:I = 0x7f09002a

.field public static final invite_email_body:I = 0x7f09002d

.field public static final invite_email_not_capable:I = 0x7f090075

.field public static final invite_email_title:I = 0x7f09002f

.field public static final invite_empty_email_list:I = 0x7f090030

.field public static final invite_empty_recommended_list:I = 0x7f090032

.field public static final invite_empty_sms_list:I = 0x7f090031

.field public static final invite_group_toggle_email:I = 0x7f090070

.field public static final invite_group_toggle_recommended:I = 0x7f090072

.field public static final invite_group_toggle_sms:I = 0x7f090071

.field public static final invite_group_toggle_weibo:I = 0x7f090073

.field public static final invite_main_text:I = 0x7f09002c

.field public static final invite_no_email_number_contact:I = 0x7f090067

.field public static final invite_recommended_not_capable:I = 0x7f090076

.field public static final invite_sms:I = 0x7f09002b

.field public static final invite_sms_body:I = 0x7f09002e

.field public static final invite_sms_not_capable:I = 0x7f090074

.field public static final invite_tab:I = 0x7f09000a

.field public static final invite_weibo:I = 0x7f0900ea

.field public static final invite_weibo_body:I = 0x7f0900f3

.field public static final invite_weibo_button:I = 0x7f0900eb

.field public static final invite_weibo_hint:I = 0x7f0900f2

.field public static final invite_weibo_sending:I = 0x7f0900ee

.field public static final invite_weibo_title:I = 0x7f0900ec

.field public static final last_name_input_hint:I = 0x7f090012

.field public static final later:I = 0x7f090096

.field public static final leave_message:I = 0x7f0901e7

.field public static final leave_video_message:I = 0x7f0901c6

.field public static final loading_text:I = 0x7f090005

.field public static final loginfailure:I = 0x7f090026

.field public static final low_bandwidth:I = 0x7f090058

.field public static final low_storage_space_ticker:I = 0x7f0900af

.field public static final merge_data_loss_prompt:I = 0x7f0901d2

.field public static final merge_data_loss_title:I = 0x7f0901d1

.field public static final missed_call_text:I = 0x7f09003a

.field public static final missed_call_title:I = 0x7f090039

.field public static final mobile_number_input_hint:I = 0x7f090010

.field public static final mobile_number_label:I = 0x7f09000e

.field public static final mobile_number_notice:I = 0x7f090013

.field public static final more_operations:I = 0x7f090197

.field public static final more_video_effects_desc:I = 0x7f090117

.field public static final more_video_effects_title:I = 0x7f090116

.field public static final mute_button_text:I = 0x7f09004b

.field public static final no:I = 0x7f090022

.field public static final no_call_log_msg:I = 0x7f09007d

.field public static final no_contact_found:I = 0x7f09016e

.field public static final no_contacts_msg:I = 0x7f090053

.field public static final no_email_code_title:I = 0x7f09017a

.field public static final no_favorite_msg:I = 0x7f090054

.field public static final no_matching_contacts:I = 0x7f090056

.field public static final no_matching_countries:I = 0x7f090057

.field public static final no_push_code_title:I = 0x7f09017c

.field public static final no_sms_title:I = 0x7f090180

.field public static final notification_videomail_title:I = 0x7f0901e0

.field public static final oauth_weibo_title:I = 0x7f0900ed

.field public static final ok:I = 0x7f09001f

.field public static final open_tips:I = 0x7f090077

.field public static final phone_type_home:I = 0x7f090036

.field public static final phone_type_main:I = 0x7f090034

.field public static final phone_type_mobile:I = 0x7f090033

.field public static final phone_type_work:I = 0x7f090035

.field public static final pic_viewer_action_forward:I = 0x7f090199

.field public static final pic_viewer_action_save_to_photo_gallery:I = 0x7f09019a

.field public static final pic_viewer_actions_title:I = 0x7f090198

.field public static final pic_viewer_downloading_failed:I = 0x7f09019d

.field public static final pic_viewer_hint_saving_to_photo_gallery_done:I = 0x7f09019b

.field public static final pic_viewer_hint_saving_to_photo_gallery_failed:I = 0x7f09019c

.field public static final postcall_calltype_incoming:I = 0x7f0901c9

.field public static final postcall_calltype_outgoing:I = 0x7f0901c8

.field public static final postcall_content_button_appstore:I = 0x7f0900e0

.field public static final postcall_content_button_facebook:I = 0x7f0900e3

.field public static final postcall_content_button_invite:I = 0x7f0900e8

.field public static final postcall_content_button_vgood:I = 0x7f0901e2

.field public static final postcall_content_desc_appstore:I = 0x7f0900df

.field public static final postcall_content_desc_facebook:I = 0x7f0900e2

.field public static final postcall_content_desc_invite:I = 0x7f0900e7

.field public static final postcall_content_desc_vgood:I = 0x7f0900c7

.field public static final postcall_content_title_appstore:I = 0x7f0900de

.field public static final postcall_content_title_facebook:I = 0x7f0900e1

.field public static final postcall_content_title_invite:I = 0x7f0900e6

.field public static final postcall_content_title_vgood:I = 0x7f0901e1

.field public static final postcall_later_button:I = 0x7f0900e5

.field public static final postcall_no_thanks_button:I = 0x7f0900e4

.field public static final postcall_vgood_getnow:I = 0x7f0900c8

.field public static final premium_features:I = 0x7f090189

.field public static final preview_invites_none:I = 0x7f090038

.field public static final privacy_label:I = 0x7f090019

.field public static final profile_label:I = 0x7f09000f

.field public static final profile_title:I = 0x7f09000d

.field public static final purchase_in_background:I = 0x7f09018c

.field public static final purchase_launching:I = 0x7f09018a

.field public static final purchase_waiting:I = 0x7f09018b

.field public static final push_msg_default_title:I = 0x7f09008c

.field public static final receiver_call_alert:I = 0x7f0901b3

.field public static final refresh:I = 0x7f0901ac

.field public static final refresh_contacts:I = 0x7f09003d

.field public static final refreshing_contacts:I = 0x7f090040

.field public static final register:I = 0x7f0901ab

.field public static final register_in_progress_text:I = 0x7f09003f

.field public static final register_title:I = 0x7f09000c

.field public static final registration_failed:I = 0x7f09005a

.field public static final registration_failed_due_to_network:I = 0x7f09005c

.field public static final registration_failed_message:I = 0x7f09005b

.field public static final remove_favorite:I = 0x7f0900f6

.field public static final save:I = 0x7f09001d

.field public static final save_failed_msg:I = 0x7f09005f

.field public static final save_failed_msg_email:I = 0x7f090061

.field public static final save_failed_msg_invalid_phonenumber:I = 0x7f090060

.field public static final save_failed_title:I = 0x7f09005e

.field public static final save_in_progress_text:I = 0x7f090042

.field public static final search:I = 0x7f090041

.field public static final search_results_for:I = 0x7f090055

.field public static final select_all:I = 0x7f090037

.field public static final send_sms:I = 0x7f0900aa

.field public static final send_video_button_text:I = 0x7f09004a

.field public static final sender_call_alert:I = 0x7f0901b2

.field public static final sender_msg_on_animation_upgrade:I = 0x7f09007c

.field public static final sending_photo_failed:I = 0x7f090195

.field public static final settings:I = 0x7f09010b

.field public static final settings_edit:I = 0x7f09010c

.field public static final settings_tab:I = 0x7f0901d7

.field public static final settings_vgoods_info:I = 0x7f090100

.field public static final settings_vgoods_select:I = 0x7f090101

.field public static final settings_vgoods_title:I = 0x7f0900ff

.field public static final settings_videomail_info:I = 0x7f09010e

.field public static final settings_videomail_select:I = 0x7f09010f

.field public static final settings_videomail_title:I = 0x7f09010d

.field public static final sign_up:I = 0x7f09001e

.field public static final sign_up_notice:I = 0x7f090017

.field public static final sign_up_or:I = 0x7f090018

.field public static final skip:I = 0x7f09001c

.field public static final slide_to_answer_hint:I = 0x7f0901b5

.field public static final slide_to_decline_hint:I = 0x7f0901b6

.field public static final speaker_button_text:I = 0x7f09004c

.field public static final splash_starting_text:I = 0x7f0901a9

.field public static final start_conversation:I = 0x7f0900f4

.field public static final store_avatars_description:I = 0x7f0901a7

.field public static final store_avatars_title:I = 0x7f0901a6

.field public static final store_contacts_label:I = 0x7f09001a

.field public static final store_contacts_notice:I = 0x7f09001b

.field public static final store_games_description:I = 0x7f0901a3

.field public static final store_games_title:I = 0x7f0901a2

.field public static final store_surprises_description:I = 0x7f0901a1

.field public static final store_surprises_title:I = 0x7f0901a0

.field public static final store_tab:I = 0x7f09000b

.field public static final submit_button:I = 0x7f09017e

.field public static final switch_camera:I = 0x7f0901b7

.field public static final switching_cameras:I = 0x7f090059

.field public static final sync_create_account_in_progress:I = 0x7f09008a

.field public static final sync_create_account_text:I = 0x7f090089

.field public static final sync_tango_contact_status:I = 0x7f090097

.field public static final sync_tango_video_call_title:I = 0x7f09008b

.field public static final tab_badge_100_plus:I = 0x7f090006

.field public static final tab_badge_error:I = 0x7f090007

.field public static final taking_photo_failed:I = 0x7f090193

.field public static final tc_alert_button_delete:I = 0x7f090131

.field public static final tc_buy:I = 0x7f09015f

.field public static final tc_can_not_forward_alert_message_generic:I = 0x7f090154

.field public static final tc_can_not_forward_alert_message_sms:I = 0x7f090153

.field public static final tc_can_not_forward_message_title:I = 0x7f090152

.field public static final tc_contact_name_unknown:I = 0x7f090132

.field public static final tc_conversation_action_delete:I = 0x7f090128

.field public static final tc_conversation_action_view:I = 0x7f090127

.field public static final tc_conversations_filter_all:I = 0x7f090125

.field public static final tc_conversations_filter_videomails:I = 0x7f090126

.field public static final tc_create_conversation:I = 0x7f090135

.field public static final tc_delete_conversation_alert_message:I = 0x7f090130

.field public static final tc_delete_conversation_alert_title:I = 0x7f09012f

.field public static final tc_delete_message_alert_message:I = 0x7f090151

.field public static final tc_delete_message_alert_title:I = 0x7f090150

.field public static final tc_duration_format:I = 0x7f09012c

.field public static final tc_empty_conversation_instruction:I = 0x7f090139

.field public static final tc_empty_conversation_instruction_main:I = 0x7f09013b

.field public static final tc_empty_conversation_instruction_media:I = 0x7f09013c

.field public static final tc_empty_conversation_instruction_no_videomail:I = 0x7f09013a

.field public static final tc_empty_conversation_instruction_text:I = 0x7f09013e

.field public static final tc_empty_conversation_instruction_vgood:I = 0x7f09013d

.field public static final tc_empty_conversation_title:I = 0x7f090138

.field public static final tc_group_conversation:I = 0x7f090140

.field public static final tc_just_now:I = 0x7f090133

.field public static final tc_leave_message_alert:I = 0x7f090155

.field public static final tc_media_make_audio_call:I = 0x7f090192

.field public static final tc_media_make_video_call:I = 0x7f090191

.field public static final tc_media_pick_photo:I = 0x7f09018f

.field public static final tc_media_take_photo:I = 0x7f09018e

.field public static final tc_media_take_video:I = 0x7f090190

.field public static final tc_media_title:I = 0x7f09018d

.field public static final tc_message_compose_hint:I = 0x7f09012d

.field public static final tc_message_not_sent_notification_text:I = 0x7f09015b

.field public static final tc_message_not_sent_notification_title:I = 0x7f09015a

.field public static final tc_message_not_sent_placeholder:I = 0x7f090134

.field public static final tc_message_option_buy:I = 0x7f090148

.field public static final tc_message_option_copy_text:I = 0x7f090147

.field public static final tc_message_option_delete:I = 0x7f090146

.field public static final tc_message_option_forward:I = 0x7f090144

.field public static final tc_message_option_resend:I = 0x7f090145

.field public static final tc_message_option_title:I = 0x7f090142

.field public static final tc_message_option_view:I = 0x7f090143

.field public static final tc_message_read:I = 0x7f090169

.field public static final tc_message_sending_error:I = 0x7f090141

.field public static final tc_missed_call_label:I = 0x7f090129

.field public static final tc_new_message_notification_ticker:I = 0x7f090159

.field public static final tc_not_supported_by_peer_alert_message_generic:I = 0x7f09014c

.field public static final tc_not_supported_by_peer_alert_message_sms:I = 0x7f09014a

.field public static final tc_not_supported_by_peer_alert_title:I = 0x7f090149

.field public static final tc_not_supported_peer_platform_alert_message_generic:I = 0x7f09014d

.field public static final tc_not_supported_peer_platform_alert_message_sms:I = 0x7f09014b

.field public static final tc_picture_choose_photo_no_sd:I = 0x7f090167

.field public static final tc_picture_default_title_read:I = 0x7f090162

.field public static final tc_picture_default_title_unread:I = 0x7f090161

.field public static final tc_picture_forwarded:I = 0x7f090164

.field public static final tc_picture_forwarding_failed:I = 0x7f090165

.field public static final tc_picture_message:I = 0x7f090160

.field public static final tc_picture_no_sd:I = 0x7f090168

.field public static final tc_picture_not_supported_message_by_text:I = 0x7f090163

.field public static final tc_picture_preview_cancel:I = 0x7f09019e

.field public static final tc_picture_preview_send:I = 0x7f09019f

.field public static final tc_picture_take_photo_no_sd:I = 0x7f090166

.field public static final tc_play:I = 0x7f09015e

.field public static final tc_select_contact_any:I = 0x7f090137

.field public static final tc_select_contact_zero:I = 0x7f090136

.field public static final tc_self_contact_name:I = 0x7f09012e

.field public static final tc_surprise_msg_text:I = 0x7f09015d

.field public static final tc_system_account_account_name:I = 0x7f090158

.field public static final tc_system_account_message_reply:I = 0x7f090156

.field public static final tc_system_account_message_welcome:I = 0x7f090157

.field public static final tc_tab:I = 0x7f090124

.field public static final tc_text_not_supported_by_peer_sms_body:I = 0x7f09014e

.field public static final tc_tip_did_not_answer:I = 0x7f09013f

.field public static final tc_vgood_not_downloaded:I = 0x7f09015c

.field public static final tc_vgood_not_supported_by_peer_sms_body:I = 0x7f09014f

.field public static final tc_videocall_label:I = 0x7f0901cf

.field public static final tc_videomail_default_title_read:I = 0x7f09012b

.field public static final tc_videomail_default_title_unread:I = 0x7f09012a

.field public static final terms_of_use:I = 0x7f0901d9

.field public static final text_profile:I = 0x7f090014

.field public static final timestamp_today:I = 0x7f0901de

.field public static final timestamp_yesterday:I = 0x7f0901df

.field public static final tip_prefix:I = 0x7f0900f8

.field public static final tips:I = 0x7f090043

.field public static final tips_url:I = 0x7f090044

.field public static final too_many_sms_requests_message:I = 0x7f090173

.field public static final too_many_sms_requests_title:I = 0x7f090172

.field public static final unknwon_caller:I = 0x7f0901e6

.field public static final update_required_message:I = 0x7f09007b

.field public static final update_required_title:I = 0x7f09007a

.field public static final update_tango_button:I = 0x7f090046

.field public static final update_tango_message:I = 0x7f090045

.field public static final validation_other_account_not_linked_notice:I = 0x7f09008f

.field public static final validation_other_device_text:I = 0x7f09008e

.field public static final validation_other_device_title:I = 0x7f09008d

.field public static final validation_url:I = 0x7f090003

.field public static final verification_failed:I = 0x7f0901d3

.field public static final verification_failed_due_to_network:I = 0x7f0901d5

.field public static final verification_failed_message:I = 0x7f0901d4

.field public static final verification_required_message:I = 0x7f090171

.field public static final verify:I = 0x7f0901c5

.field public static final verify_account:I = 0x7f09016f

.field public static final verify_via_email:I = 0x7f09017d

.field public static final verify_via_sms:I = 0x7f09017b

.field public static final vgood_demo_downloading:I = 0x7f090123

.field public static final vgood_remote_or_local_error:I = 0x7f090122

.field public static final vgood_try_me:I = 0x7f090103

.field public static final vgood_video_url:I = 0x7f0901cd

.field public static final vgoods_general_description:I = 0x7f0901e9

.field public static final vgoods_item_pending:I = 0x7f0901cb

.field public static final vgoods_no_products_description:I = 0x7f090102

.field public static final vgoods_no_products_title:I = 0x7f0901cc

.field public static final vgoods_purchased:I = 0x7f0901e8

.field public static final video_call:I = 0x7f090090

.field public static final video_effects_warning_avatar_downloading:I = 0x7f090114

.field public static final video_effects_warning_downloading:I = 0x7f090113

.field public static final video_effects_warning_incompatible_client:I = 0x7f090111

.field public static final video_effects_warning_incompatible_platform:I = 0x7f090112

.field public static final video_effects_warning_title:I = 0x7f090110

.field public static final video_effects_warning_unavailable:I = 0x7f090115

.field public static final video_messages_tos:I = 0x7f0900b0

.field public static final videomail_contact_selection_reached_limit:I = 0x7f0900c6

.field public static final videomail_contact_selection_select_none:I = 0x7f0900c5

.field public static final videomail_contact_selection_title:I = 0x7f0900c4

.field public static final videomail_context_menu_call:I = 0x7f0901dc

.field public static final videomail_context_menu_delete:I = 0x7f0901dd

.field public static final videomail_error_delivery_failed:I = 0x7f0900ba

.field public static final videomail_error_forward:I = 0x7f0900bb

.field public static final videomail_error_message_not_saved:I = 0x7f0900b9

.field public static final videomail_error_recording:I = 0x7f0900b8

.field public static final videomail_footer_button:I = 0x7f0901e4

.field public static final videomail_footer_description:I = 0x7f0901e3

.field public static final videomail_get_ready:I = 0x7f0901c7

.field public static final videomail_header_subscriptions:I = 0x7f0900b2

.field public static final videomail_header_trial:I = 0x7f0900b1

.field public static final videomail_max_duration_reached:I = 0x7f0900b6

.field public static final videomail_no_products_available:I = 0x7f0900f7

.field public static final videomail_playback_error_local:I = 0x7f0900bf

.field public static final videomail_playback_error_no_video:I = 0x7f0900be

.field public static final videomail_playback_error_streaming:I = 0x7f0900bd

.field public static final videomail_playback_warning_video_orientation:I = 0x7f0900c0

.field public static final videomail_reply_dialog_call:I = 0x7f0900ac

.field public static final videomail_reply_dialog_forward:I = 0x7f0900ad

.field public static final videomail_reply_dialog_title:I = 0x7f0900ab

.field public static final videomail_send_by_sms_warning_dialog_dont_show:I = 0x7f0900c3

.field public static final videomail_send_by_sms_warning_dialog_message:I = 0x7f0900c2

.field public static final videomail_send_by_sms_warning_dialog_title:I = 0x7f0900c1

.field public static final videomail_sending:I = 0x7f0900bc

.field public static final videomail_sent:I = 0x7f0900b7

.field public static final videomail_seperator_button:I = 0x7f0901e5

.field public static final videomail_sms_body:I = 0x7f0900ae

.field public static final videomail_subscriptions_tagline_bottom:I = 0x7f0900b4

.field public static final videomail_subscriptions_tagline_top:I = 0x7f0900b3

.field public static final videomail_subscriptions_title:I = 0x7f0900b5

.field public static final videomail_tab:I = 0x7f0901aa

.field public static final wait_for_sms_prompt:I = 0x7f090182

.field public static final weibo_sent_general_error:I = 0x7f0900f1

.field public static final weibo_sent_ok:I = 0x7f0900ef

.field public static final weibo_sent_repeat:I = 0x7f0900f0

.field public static final welcome_screen_button_default:I = 0x7f0901ce

.field public static final welcome_screen_title:I = 0x7f09011d

.field public static final yes:I = 0x7f090021


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
