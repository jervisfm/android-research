.class public final Lcom/sgiggle/production/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final ActionBar:I = 0x7f0d0057

.field public static final ActionBarCompat:I = 0x7f0d0007

.field public static final ActionBarCompatHomeItem:I = 0x7f0d000c

.field public static final ActionBarCompatItem:I = 0x7f0d000b

.field public static final ActionBarCompatItemBase:I = 0x7f0d0008

.field public static final ActionBarCompatTitle:I = 0x7f0d000a

.field public static final ActionBarCompatTitleBase:I = 0x7f0d0009

.field public static final ActionBarTitle:I = 0x7f0d0058

.field public static final Badge:I = 0x7f0d0056

.field public static final CallLogEmptyText:I = 0x7f0d0013

.field public static final CommonList:I = 0x7f0d0017

.field public static final ContactsEmptyText:I = 0x7f0d0012

.field public static final CopyrightAndVersionText:I = 0x7f0d0015

.field public static final DefaultText:I = 0x7f0d000e

.field public static final DialogNoTitle:I = 0x7f0d0029

.field public static final InCallTouchButton:I = 0x7f0d0020

.field public static final InCallTouchToggleButton:I = 0x7f0d0021

.field public static final InviteMainText:I = 0x7f0d0014

.field public static final ListItem:I = 0x7f0d0018

.field public static final ListItemLeftImage:I = 0x7f0d001b

.field public static final ListItemMainText:I = 0x7f0d001e

.field public static final ListItemRightImage:I = 0x7f0d001c

.field public static final ListItemSecondaryText:I = 0x7f0d001f

.field public static final ListItemTextOnly:I = 0x7f0d001a

.field public static final ListItemVerticalSeparator:I = 0x7f0d001d

.field public static final ListItemWithImages:I = 0x7f0d0019

.field public static final LoadingSpinnerText:I = 0x7f0d0024

.field public static final MainButton:I = 0x7f0d0016

.field public static final PlaybackProgressBar:I = 0x7f0d0035

.field public static final PremiumFeatureInfo:I = 0x7f0d0025

.field public static final PremiumFeatureTitle:I = 0x7f0d0026

.field public static final RegisterBlurb:I = 0x7f0d0010

.field public static final SecondaryText:I = 0x7f0d000f

.field public static final SelectContact:I = 0x7f0d0053

.field public static final SelectContact_List:I = 0x7f0d0054

.field public static final SelectContact_List_ContactName:I = 0x7f0d0055

.field public static final SettingsDivider:I = 0x7f0d0027

.field public static final Tango:I = 0x7f0d005a

.field public static final TangoWindowTitle:I = 0x7f0d000d

.field public static final TextAppearance_SlidingTabActive:I = 0x7f0d0023

.field public static final TextAppearance_SlidingTabNormal:I = 0x7f0d0022

.field public static final Theme:I = 0x7f0d0000

.field public static final Theme_Tango:I = 0x7f0d0004

.field public static final Theme_TangoBase:I = 0x7f0d0003

.field public static final Theme_TangoDefault:I = 0x7f0d0001

.field public static final Theme_TangoTabActivity:I = 0x7f0d0002

.field public static final Theme_Tango_ActionBar:I = 0x7f0d0005

.field public static final Theme_Tango_PurchaseProxy:I = 0x7f0d0006

.field public static final ThreadedConversations:I = 0x7f0d003b

.field public static final ThreadedConversations_ConversationList:I = 0x7f0d003c

.field public static final ThreadedConversations_ConversationList_ConversationTitle:I = 0x7f0d003f

.field public static final ThreadedConversations_ConversationList_Empty:I = 0x7f0d0043

.field public static final ThreadedConversations_ConversationList_Empty_Instruction:I = 0x7f0d0045

.field public static final ThreadedConversations_ConversationList_Empty_Title:I = 0x7f0d0044

.field public static final ThreadedConversations_ConversationList_List:I = 0x7f0d003d

.field public static final ThreadedConversations_ConversationList_Summary:I = 0x7f0d0041

.field public static final ThreadedConversations_ConversationList_Summary_Secondary:I = 0x7f0d0042

.field public static final ThreadedConversations_ConversationList_TimeStamp:I = 0x7f0d0040

.field public static final ThreadedConversations_MessageList:I = 0x7f0d0046

.field public static final ThreadedConversations_MessageList_ContactName:I = 0x7f0d004b

.field public static final ThreadedConversations_MessageList_Duration:I = 0x7f0d004e

.field public static final ThreadedConversations_MessageList_Empty:I = 0x7f0d0047

.field public static final ThreadedConversations_MessageList_Empty_Instruction:I = 0x7f0d0049

.field public static final ThreadedConversations_MessageList_Empty_Main:I = 0x7f0d0048

.field public static final ThreadedConversations_MessageList_Hint:I = 0x7f0d0051

.field public static final ThreadedConversations_MessageList_Link:I = 0x7f0d0052

.field public static final ThreadedConversations_MessageList_List:I = 0x7f0d0050

.field public static final ThreadedConversations_MessageList_StandaloneText:I = 0x7f0d004f

.field public static final ThreadedConversations_MessageList_Text:I = 0x7f0d004c

.field public static final ThreadedConversations_MessageList_TimeStamp:I = 0x7f0d004a

.field public static final ThreadedConversations_MessageList_Title:I = 0x7f0d004d

.field public static final ThreadedConversations_SummaryIcon:I = 0x7f0d003e

.field public static final Transparent:I = 0x7f0d002a

.field public static final ValidationRequiredText:I = 0x7f0d0011

.field public static final Widget:I = 0x7f0d002c

.field public static final Widget_Button:I = 0x7f0d002f

.field public static final Widget_Button_Transparent:I = 0x7f0d0030

.field public static final Widget_ImageButton:I = 0x7f0d002d

.field public static final Widget_ImageButton_Transparent:I = 0x7f0d002e

.field public static final Widget_ListView:I = 0x7f0d0031

.field public static final Widget_ProgressBar:I = 0x7f0d0032

.field public static final Widget_ProgressBar_Horizontal:I = 0x7f0d0033

.field public static final Widget_ProgressBar_Vertical:I = 0x7f0d0034

.field public static final Widget_TabButton:I = 0x7f0d0037

.field public static final Widget_TabButtonLayout:I = 0x7f0d0036

.field public static final Widget_ToggleRadioButton:I = 0x7f0d0039

.field public static final Widget_ToggleRadioGroup:I = 0x7f0d0038

.field public static final Widget_ToggleRadioSeparator:I = 0x7f0d003a

.field public static final call_quality_survey:I = 0x7f0d0059

.field public static final call_quality_survey_details:I = 0x7f0d002b

.field public static final popup_notification:I = 0x7f0d0028


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
