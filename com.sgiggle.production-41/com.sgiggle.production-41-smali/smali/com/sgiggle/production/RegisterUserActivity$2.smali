.class Lcom/sgiggle/production/RegisterUserActivity$2;
.super Ljava/lang/Object;
.source "RegisterUserActivity.java"

# interfaces
.implements Lcom/facebook/android/SessionEvents$AuthListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/RegisterUserActivity;->initializeFacebook(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/RegisterUserActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 374
    iput-object p1, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAuthFail(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    .line 401
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v1, v2, :cond_0

    const-string v1, "reg_"

    :goto_0
    const-string v2, "fb_me_query_failed"

    const-string v3, "fb_fail_reason"

    const-string v4, "fb_auth_failed"

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sgiggle/production/RegisterUserActivity;->access$400(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    return-void

    .line 401
    :cond_0
    const-string v1, "personal_info_"

    goto :goto_0
.end method

.method public onAuthSucceed()V
    .locals 5

    .prologue
    .line 379
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v1, v2, :cond_0

    const-string v1, "reg_"

    :goto_0
    const-string v2, "attempting_fb_me_query"

    const-string v3, "fb_init_reason"

    const-string v4, "fb_auth_success"

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sgiggle/production/RegisterUserActivity;->access$400(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->doFBLogin()V
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$500(Lcom/sgiggle/production/RegisterUserActivity;)V

    .line 397
    :goto_1
    return-void

    .line 381
    :cond_0
    const-string v1, "personal_info_"

    goto :goto_0

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$2;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v1, v2, :cond_2

    const-string v1, "reg_"

    :goto_2
    const-string v2, "fb_me_query_failed"

    const-string v3, "fb_fail_reason"

    const-string v4, "fb_auth_success_session_invalid"

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sgiggle/production/RegisterUserActivity;->access$400(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v1, "personal_info_"

    goto :goto_2
.end method
