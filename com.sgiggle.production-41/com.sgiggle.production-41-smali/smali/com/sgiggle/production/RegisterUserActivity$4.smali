.class Lcom/sgiggle/production/RegisterUserActivity$4;
.super Ljava/lang/Object;
.source "RegisterUserActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/RegisterUserActivity;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/RegisterUserActivity;

.field final synthetic val$email:Ljava/lang/String;

.field final synthetic val$firstName:Ljava/lang/String;

.field final synthetic val$lastName:Ljava/lang/String;

.field final synthetic val$phoneNumber:Ljava/lang/String;

.field final synthetic val$storeContacts:Z


# direct methods
.method constructor <init>(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iput-object p2, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$firstName:Ljava/lang/String;

    iput-object p3, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$lastName:Ljava/lang/String;

    iput-object p4, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$phoneNumber:Ljava/lang/String;

    iput-object p5, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$email:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$storeContacts:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 509
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v1, v2, :cond_0

    const-string v1, "reg_"

    :goto_0
    const-string v2, "optional_email_dialog"

    const-string v3, "optional_email_dialog_reason"

    const-string v4, "skip_email"

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/sgiggle/production/RegisterUserActivity;->access$400(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$firstName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$lastName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$phoneNumber:Ljava/lang/String;

    iget-object v5, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$email:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/sgiggle/production/RegisterUserActivity$4;->val$storeContacts:Z

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/RegisterUserActivity;->doRegisterUser(Lcom/sgiggle/production/RegisterUserActivity$ViewMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 515
    return-void

    .line 509
    :cond_0
    const-string v1, "personal_info_"

    goto :goto_0
.end method
