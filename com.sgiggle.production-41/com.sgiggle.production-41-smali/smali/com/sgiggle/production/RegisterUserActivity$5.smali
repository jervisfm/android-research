.class Lcom/sgiggle/production/RegisterUserActivity$5;
.super Ljava/lang/Object;
.source "RegisterUserActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/RegisterUserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/RegisterUserActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 703
    iput-object p1, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$100(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-ne p1, v0, :cond_1

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$000(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$000(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$000(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_SubscriberNumber:Ljava/lang/String;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$700(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$800(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    if-ne v0, v1, :cond_1

    .line 710
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->clearAndResetAutoFillData()V
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$900(Lcom/sgiggle/production/RegisterUserActivity;)V

    .line 711
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;
    invoke-static {v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$000(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;

    move-result-object v1

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->prepareAutoFillWithNumber(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->access$1000(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;)V

    .line 717
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$1100(Lcom/sgiggle/production/RegisterUserActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$800(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$1200(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$1300(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #getter for: Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$600(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 715
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity$5;->this$0:Lcom/sgiggle/production/RegisterUserActivity;

    #calls: Lcom/sgiggle/production/RegisterUserActivity;->doAutoFillData()V
    invoke-static {v0}, Lcom/sgiggle/production/RegisterUserActivity;->access$1400(Lcom/sgiggle/production/RegisterUserActivity;)V

    goto :goto_0
.end method
