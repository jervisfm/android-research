.class final enum Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
.super Ljava/lang/Enum;
.source "RegisterUserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/RegisterUserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ViewMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/RegisterUserActivity$ViewMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

.field public static final enum VIEW_MODE_PROFILE:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

.field public static final enum VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 113
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    const-string v1, "VIEW_MODE_REGISTER"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    .line 114
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    const-string v1, "VIEW_MODE_PROFILE"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_PROFILE:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    .line 112
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_PROFILE:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->$VALUES:[Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    .locals 1
    .parameter

    .prologue
    .line 112
    const-class v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->$VALUES:[Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    invoke-virtual {v0}, [Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    return-object v0
.end method
