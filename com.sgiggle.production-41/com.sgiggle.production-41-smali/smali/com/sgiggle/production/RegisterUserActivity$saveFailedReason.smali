.class final enum Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;
.super Ljava/lang/Enum;
.source "RegisterUserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/RegisterUserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "saveFailedReason"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

.field public static final enum EMAIL_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

.field public static final enum PHONE_EMPTY:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

.field public static final enum PHONE_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 175
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    const-string v1, "PHONE_EMPTY"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_EMPTY:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    .line 176
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    const-string v1, "PHONE_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    .line 177
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    const-string v1, "EMAIL_INVALID"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->EMAIL_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    .line 174
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_EMPTY:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->EMAIL_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->$VALUES:[Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 174
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;
    .locals 1
    .parameter

    .prologue
    .line 174
    const-class v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->$VALUES:[Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    invoke-virtual {v0}, [Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    return-object v0
.end method
