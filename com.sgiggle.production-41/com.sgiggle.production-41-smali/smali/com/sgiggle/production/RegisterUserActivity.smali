.class public Lcom/sgiggle/production/RegisterUserActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "RegisterUserActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/RegisterUserActivity$8;,
        Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;,
        Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;,
        Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;,
        Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    }
.end annotation


# static fields
.field private static final CHOOSE_COUNTRY_CODE_REQUEST:I = 0x0

.field private static final FB_DEFAULT_AUTH_ACTIVITY_CODE:I = 0x7f99

.field private static final FORCE_FIXED_SIGNUP_BUTTONS:Z = true

.field private static final KEY_ACCOUNT_INFO_VALIDATION_ERROR_REASON:Ljava/lang/String; = "account_info_validation_error_reason"

.field private static final KEY_ACCOUNT_SCREEN_UI:Ljava/lang/String; = "account_screen_ui"

.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "deviceid"

.field private static final KEY_FB_FAIL_REASON:Ljava/lang/String; = "fb_fail_reason"

.field private static final KEY_FB_INIT_REASON:Ljava/lang/String; = "fb_init_reason"

.field private static final KEY_FB_VALID_SESSION:Ljava/lang/String; = "fb_valid_session"

.field private static final KEY_NETWORK_ERROR_REASON:Ljava/lang/String; = "network_error_reason"

.field private static final KEY_OPTIONAL_EMAIL_DIALOG_REASON:Ljava/lang/String; = "optional_email_dialog_reason"

.field private static final MIN_PHONE_NUMBER_LENGTH:I = 0x5

.field private static final PERSONAL_INFO_PREFIX:Ljava/lang/String; = "personal_info_"

.field private static final REASON_ACCOUNT_INFO_VALIDATION_ERROR_EMAIL_INVALID:Ljava/lang/String; = "email_invalid"

.field private static final REASON_ACCOUNT_INFO_VALIDATION_ERROR_EMPTY_PHONE:Ljava/lang/String; = "empty_phone"

.field private static final REASON_ACCOUNT_INFO_VALIDATION_ERROR_PHONE_INVALID:Ljava/lang/String; = "phone_invalid"

.field private static final REASON_FB_FAIL_FB_AUTH_FAILED:Ljava/lang/String; = "fb_auth_failed"

.field private static final REASON_FB_FAIL_FB_AUTH_SUCCESS_SESSION_INVALID:Ljava/lang/String; = "fb_auth_success_session_invalid"

.field private static final REASON_FB_INIT_FB_AUTH_SUCCESS:Ljava/lang/String; = "fb_auth_success"

.field private static final REASON_FB_INIT_FB_SESSION_VALID_ON_INIT:Ljava/lang/String; = "fb_session_valid_on_init"

.field private static final REASON_NETWORK_ERROR_NETWORK_TIMEOUT:Ljava/lang/String; = "network_timeout"

.field private static final REASON_NETWORK_ERROR_NO_NETWORK:Ljava/lang/String; = "no_network"

.field private static final REASON_OPTIONAL_EMAIL_DIALOG_ADD_EMAIL:Ljava/lang/String; = "add_email"

.field private static final REASON_OPTIONAL_EMAIL_DIALOG_APPEARED:Ljava/lang/String; = "dialog_appeared"

.field private static final REASON_OPTIONAL_EMAIL_DIALOG_SKIP_EMAIL:Ljava/lang/String; = "skip_email"

.field private static final REGISTRATION_PREFIX:Ljava/lang/String; = "reg_"

.field private static final TAG:Ljava/lang/String; = "Tango.RegisterProfileUI"

.field private static final VALUE_ACCOUNT_INFO_VALIDATION_ERROR:Ljava/lang/String; = "account_info_validation_error"

.field private static final VALUE_ATTEMPTING_FB_ME_QUERY:Ljava/lang/String; = "attempting_fb_me_query"

.field private static final VALUE_ATTEMPTING_FB_ME_QUERY_FAILED:Ljava/lang/String; = "fb_me_query_failed"

.field private static final VALUE_FB_LOGIN_CLICKED:Ljava/lang/String; = "fb_login_clicked"

.field private static final VALUE_FB_ME_QUERY_COMPLETE_EVENT:Ljava/lang/String; = "fb_me_query_complete_event"

.field private static final VALUE_OPTIONAL_EMAIL_DIALOG:Ljava/lang/String; = "optional_email_dialog"

.field private static final VALUE_REGISTRATION_NETWORK_ERROR:Ljava/lang/String; = "registration_network_error"

.field private static final VALUE_SCREEN_APPEARED:Ljava/lang/String; = "screen_appeared"

.field private static final VALUE_SUBMIT_CLICKED:Ljava/lang/String; = "submit_clicked"

.field private static final VALUE_VALIDATION_OK_REGISTERING:Ljava/lang/String; = "validation_OK_registering"


# instance fields
.field private m_FBLoginButton:Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

.field private m_autoFill_Available:Z

.field private m_autoFill_Email:Ljava/lang/String;

.field private m_autoFill_Firstname:Ljava/lang/String;

.field private m_autoFill_Lastname:Ljava/lang/String;

.field private m_autoFill_SubscriberNumber:Ljava/lang/String;

.field private m_bottomSignUpButton:Landroid/widget/Button;

.field private m_cancelButton:Landroid/widget/Button;

.field private m_countryCode:Ljava/lang/String;

.field private m_countryCodeText:Landroid/widget/TextView;

.field private m_countryId:Ljava/lang/String;

.field private m_countryName:Ljava/lang/String;

.field private m_emailText:Landroid/widget/EditText;

.field private m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

.field private m_fb_permissions:[Ljava/lang/String;

.field private m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

.field private m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

.field private m_fb_signup_top_ll:Landroid/widget/LinearLayout;

.field private m_firstNameEditText:Landroid/widget/EditText;

.field private m_fixedSignUpButtonLayout:Landroid/widget/LinearLayout;

.field private m_isoCountryCode:Ljava/lang/String;

.field private m_lastNameEditText:Landroid/widget/EditText;

.field private m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private m_phoneNumberEditText:Landroid/widget/EditText;

.field private m_phoneTextWatcher:Landroid/text/TextWatcher;

.field private m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

.field private m_privacyLayout:Landroid/view/ViewGroup;

.field private m_profileBottomButtonsLayout:Landroid/widget/LinearLayout;

.field private m_progressBar:Landroid/widget/ProgressBar;

.field private m_progressDialog:Landroid/app/ProgressDialog;

.field private m_reg_linear_layout:Landroid/widget/LinearLayout;

.field private m_reg_root_layout:Landroid/widget/RelativeLayout;

.field private m_registerBottomButtonsLayout:Landroid/widget/LinearLayout;

.field private m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

.field private m_saveButton:Landroid/widget/Button;

.field private m_selectableCountries:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/CountryListActivity$CountryData;",
            ">;"
        }
    .end annotation
.end field

.field private m_signUpButton:Landroid/widget/Button;

.field private m_signUpButtonLayoutListener:Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;

.field private m_storeContacts:Landroid/widget/CheckBox;

.field private m_subscriberNumber:Ljava/lang/String;

.field private m_title:Landroid/widget/TextView;

.field private m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 101
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "email"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "publish_actions"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "user_birthday"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_permissions:[Ljava/lang/String;

    .line 119
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 120
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 122
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

    .line 166
    iput-boolean v3, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    .line 703
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$5;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/RegisterUserActivity$5;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    .line 1025
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/sgiggle/production/RegisterUserActivity;->prepareAutoFillWithNumber(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sgiggle/production/RegisterUserActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->doAutoFillData()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->formatPhoneNumber()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/production/RegisterUserActivity$ViewMode;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->doFBLogin()V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/RegisterUserActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/RegisterUserActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_SubscriberNumber:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sgiggle/production/RegisterUserActivity;)Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->clearAndResetAutoFillData()V

    return-void
.end method

.method private clearAndResetAutoFillData()V
    .locals 1

    .prologue
    .line 756
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    .line 757
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_SubscriberNumber:Ljava/lang/String;

    .line 758
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Firstname:Ljava/lang/String;

    .line 759
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Lastname:Ljava/lang/String;

    .line 760
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Email:Ljava/lang/String;

    .line 761
    return-void
.end method

.method private closeProgressDialog()V
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 778
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 779
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 781
    :cond_0
    return-void
.end method

.method private displayCountryCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 764
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private displayRegistrationNetworkFailure(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 831
    const-string v0, "Tango.RegisterProfileUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "displayRegistrationNetworkFailure(networkFailureCause == "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    const-string v2, "NO NETWORK)"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_4

    const-string v0, "reg_"

    :goto_1
    const-string v1, "registration_network_error"

    const-string v2, "network_error_reason"

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    :cond_1
    const-string v3, "no_network"

    :goto_2
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->closeProgressDialog()V

    .line 842
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 844
    const v1, 0x7f09005a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 845
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_6

    :cond_2
    const v1, 0x7f09005b

    :goto_3
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 848
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 849
    const v1, 0x7f09005d

    new-instance v2, Lcom/sgiggle/production/RegisterUserActivity$6;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/RegisterUserActivity$6;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 854
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 855
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 856
    return-void

    .line 831
    :cond_3
    const-string v2, "TIMED OUT DUE TO NETWORK)"

    goto :goto_0

    .line 834
    :cond_4
    const-string v0, "personal_info_"

    goto :goto_1

    :cond_5
    const-string v3, "network_timeout"

    goto :goto_2

    .line 845
    :cond_6
    const v1, 0x7f09005c

    goto :goto_3
.end method

.method private displaySaveFailed(Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;)V
    .locals 4
    .parameter

    .prologue
    .line 859
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 861
    const v1, 0x7f09005e

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 863
    const-string v1, ""

    .line 864
    sget-object v2, Lcom/sgiggle/production/RegisterUserActivity$8;->$SwitchMap$com$sgiggle$production$RegisterUserActivity$saveFailedReason:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 875
    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 876
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 877
    const v1, 0x7f09005d

    new-instance v2, Lcom/sgiggle/production/RegisterUserActivity$7;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/RegisterUserActivity$7;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 883
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 884
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 885
    return-void

    .line 866
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 869
    :pswitch_1
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 872
    :pswitch_2
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090061

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 864
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private doAutoFillData()V
    .locals 2

    .prologue
    .line 738
    iget-boolean v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    if-eqz v0, :cond_2

    .line 739
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "Pre-populate Auto-Fill data..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Firstname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 744
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Lastname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 751
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    .line 753
    :cond_2
    return-void
.end method

.method private doFBLogin()V
    .locals 8

    .prologue
    .line 935
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/android/Facebook;->getAccessToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/android/Facebook;->getAccessExpires()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-direct {v2, v3, v4, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginMessage;-><init>(Ljava/lang/String;J)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 937
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 938
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 939
    return-void
.end method

.method private formatPhoneNumber()V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sgiggle/corefacade/telephony/PhoneFormatter;->format(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 340
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 341
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 342
    const/4 v2, 0x0

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v1, v2, v3, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 343
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 344
    return-void
.end method

.method private initializeFacebook(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 348
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 349
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookUserAndFriendsPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 350
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookExtendedPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 351
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getFacebookOpenGraphPermissionsList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 352
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 354
    new-array v1, v3, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_permissions:[Ljava/lang/String;

    .line 357
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/facebook/android/SessionStore;->restore(Lcom/facebook/android/Facebook;Landroid/content/Context;)Z

    .line 358
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->setupFBABTesting()V

    .line 359
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_FBLoginButton:Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_FBLoginButton:Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_permissions:[Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;->init(Lcom/sgiggle/production/RegisterUserActivity;Lcom/facebook/android/Facebook;[Ljava/lang/String;Z)V

    .line 363
    :cond_1
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 365
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_2

    const-string v0, "reg_"

    :goto_0
    const-string v1, "attempting_fb_me_query"

    const-string v2, "fb_init_reason"

    const-string v3, "fb_session_valid_on_init"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->doFBLogin()V

    .line 409
    :goto_1
    return-void

    .line 365
    :cond_2
    const-string v0, "personal_info_"

    goto :goto_0

    .line 374
    :cond_3
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/RegisterUserActivity$2;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    invoke-static {v0}, Lcom/facebook/android/SessionEvents;->addAuthListener(Lcom/facebook/android/SessionEvents$AuthListener;)V

    goto :goto_1
.end method

.method private isFacebookSupported()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 980
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    if-nez v0, :cond_0

    move v0, v2

    .line 986
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_BOTTOM:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    if-ne v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private populateFieldsFromEvent(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 626
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    .line 628
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 631
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasFirstname()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 632
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 635
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasLastname()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 636
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 640
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 641
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 644
    :cond_2
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p2, :cond_4

    .line 645
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 647
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    .line 648
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryid()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    .line 649
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    .line 650
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryname()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryName:Ljava/lang/String;

    .line 651
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisocc()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    .line 652
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    iget-object v3, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->displayCountryCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 655
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 657
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    .line 658
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 661
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_SubscriberNumber:Ljava/lang/String;

    .line 665
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getCountryCodeList()Ljava/util/List;

    move-result-object v0

    .line 666
    invoke-static {v0}, Lcom/sgiggle/production/CountryListActivity;->buildSelectableCountriesFromList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_selectableCountries:Ljava/util/ArrayList;

    .line 668
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_storeContacts:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getStoreAddressBook()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 669
    return-void
.end method

.method private prePopulatePhoneNumber()V
    .locals 4

    .prologue
    .line 675
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 676
    if-eqz v0, :cond_1

    .line 677
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 680
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 681
    const-string v1, "Tango.RegisterProfileUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "... prePopulatePhoneNumber: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 684
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 687
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 689
    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    .line 690
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_subscriberNumber:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->prepareAutoFillWithNumber(Ljava/lang/String;)V

    .line 698
    :goto_0
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->doAutoFillData()V

    .line 701
    :cond_1
    return-void

    .line 695
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->prepareAutoFillWithNumber(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private prepareAutoFillWithNumber(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 721
    const-string v0, "Tango.RegisterProfileUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareAutoFillWithNumber("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    invoke-static {p1}, Lcom/sgiggle/contacts/ContactStore;->getContactByNumber(Ljava/lang/String;)Lcom/sgiggle/contacts/Contact;

    move-result-object v0

    .line 723
    if-eqz v0, :cond_0

    .line 724
    const-string v1, "Tango.RegisterProfileUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Prepare Auto-Fill: ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    .line 726
    iput-object p1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_SubscriberNumber:Ljava/lang/String;

    .line 727
    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->firstName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Firstname:Ljava/lang/String;

    .line 728
    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->lastName:Ljava/lang/String;

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Lastname:Ljava/lang/String;

    .line 729
    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 730
    iget-object v0, v0, Lcom/sgiggle/contacts/Contact;->emailAddresses:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Email:Ljava/lang/String;

    .line 735
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Email:Ljava/lang/String;

    goto :goto_0
.end method

.method private setupFBABTesting()V
    .locals 4

    .prologue
    const v3, 0x7f0a00a9

    .line 943
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 944
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 945
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 947
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$8;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$RegistrationOptions$RegistrationLayout:[I

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 968
    :goto_0
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fixedSignUpButtonLayout:Landroid/widget/LinearLayout;

    .line 969
    const v0, 0x7f0a00aa

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_signUpButton:Landroid/widget/Button;

    .line 970
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_signUpButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 973
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;-><init>(Lcom/sgiggle/production/RegisterUserActivity;Lcom/sgiggle/production/RegisterUserActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_signUpButtonLayoutListener:Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;

    .line 974
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_root_layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_signUpButtonLayoutListener:Lcom/sgiggle/production/RegisterUserActivity$SignUpButtonLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 976
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->isShowFixedSignUpButton()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->toggleSignUpButton(Z)V

    .line 977
    return-void

    .line 949
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 950
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 954
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 955
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 956
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_FBLoginButton:Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    .line 957
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 961
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 962
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 963
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_FBLoginButton:Lcom/sgiggle/production/RegisterUserActivity$FacebookLoginButton;

    .line 964
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_ll_remove_when_login:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 947
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showProgressDialog()V
    .locals 3

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_0

    const v0, 0x7f09003f

    .line 770
    :goto_0
    const-string v1, ""

    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {p0, v1, v0, v2}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)Landroid/app/ProgressDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressDialog:Landroid/app/ProgressDialog;

    .line 771
    return-void

    .line 768
    :cond_0
    const v0, 0x7f090042

    goto :goto_0
.end method

.method private startSMSListener()V
    .locals 2

    .prologue
    .line 413
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "Start SMS Listener"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->registerSMSReceiver()V

    .line 415
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->scheduleSMSReceiverTimer()V

    .line 416
    return-void
.end method

.method private statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 998
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 999
    return-void
.end method

.method private statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1002
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1003
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "account_screen_ui"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 1004
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1006
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/iphelper/IpHelper;->getDevIDBase64()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 1007
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1009
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "fb_valid_session"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "1"

    :goto_0
    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 1013
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1015
    if-eqz p3, :cond_0

    .line 1017
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 1018
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1021
    :cond_0
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;-><init>(Ljava/util/List;)V

    .line 1022
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1023
    return-void

    .line 1009
    :cond_1
    const-string v2, "0"

    goto :goto_0
.end method

.method private stopCurrentSMSListener()V
    .locals 2

    .prologue
    .line 420
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "Stop current SMS Listener"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->cancelSMSReceiverTimer()V

    .line 422
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->unregisterSMSReceiver()V

    .line 423
    return-void
.end method


# virtual methods
.method protected doRegisterUser(Lcom/sgiggle/production/RegisterUserActivity$ViewMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 547
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_0

    const-string v0, "reg_"

    :goto_0
    const-string v1, "validation_OK_registering"

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 553
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne p1, v0, :cond_1

    .line 554
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;

    iget-object v3, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    iget-object v6, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryName:Ljava/lang/String;

    move-object v1, p2

    move-object v2, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 576
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->stopCurrentSMSListener()V

    .line 577
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->startSMSListener()V

    .line 578
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->startWakeupAlarmService()V

    .line 579
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->showProgressDialog()V

    .line 580
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 581
    return-void

    .line 547
    :cond_0
    const-string v0, "personal_info_"

    goto :goto_0

    .line 565
    :cond_1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SavePersonalInfoMessage;

    iget-object v3, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    iget-object v4, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    iget-object v6, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryName:Ljava/lang/String;

    move-object v1, p2

    move-object v2, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/sgiggle/media_engine/MediaEngineMessage$SavePersonalInfoMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 243
    const-string v0, "Tango.RegisterProfileUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    if-nez p1, :cond_0

    .line 246
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 335
    :goto_0
    return-void

    .line 250
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 332
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 253
    :sswitch_0
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    .line 254
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_title:Landroid/widget/TextView;

    const v1, 0x7f09000c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 255
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_privacyLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 256
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_profileBottomButtonsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 257
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 259
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->closeProgressDialog()V

    .line 261
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x88d7

    if-ne v0, v1, :cond_3

    .line 263
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;

    .line 264
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    .line 265
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getRegistrationOptions()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;

    move-result-object v1

    .line 266
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getRegistrationLayout()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    .line 268
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->isFacebookSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 270
    invoke-direct {p0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->initializeFacebook(Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;)V

    .line 273
    :cond_1
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions;->getPrefillContactInfo()Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    .line 275
    invoke-direct {p0, v0, v5}, Lcom/sgiggle/production/RegisterUserActivity;->populateFieldsFromEvent(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)V

    .line 277
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_prefillContactInfo:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;->PREFILL_ENABLE:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$PrefillContactInfo;

    if-ne v0, v1, :cond_2

    .line 278
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->prePopulatePhoneNumber()V

    .line 281
    :cond_2
    const-string v0, "reg_"

    const-string v1, "screen_appeared"

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :goto_1
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$AllowAccessAddressBookMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AllowAccessAddressBookMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 285
    :cond_3
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginEvent;

    .line 286
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FBDidLoginEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    .line 287
    invoke-direct {p0, v0, v4}, Lcom/sgiggle/production/RegisterUserActivity;->populateFieldsFromEvent(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)V

    .line 288
    iput-boolean v4, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_autoFill_Available:Z

    .line 290
    const-string v0, "reg_"

    const-string v1, "fb_me_query_complete_event"

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 307
    :sswitch_1
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_PROFILE:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    .line 308
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 309
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 310
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 311
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_title:Landroid/widget/TextView;

    const v1, 0x7f09000d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 312
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_privacyLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fixedSignUpButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registerBottomButtonsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_profileBottomButtonsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 317
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->closeProgressDialog()V

    .line 318
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;

    .line 319
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayRegisterUserEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    invoke-direct {p0, v0, v5}, Lcom/sgiggle/production/RegisterUserActivity;->populateFieldsFromEvent(Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;Z)V

    .line 321
    const-string v0, "personal_info_"

    const-string v1, "screen_appeared"

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserNoNetworkEvent;

    .line 327
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$RegisterUserNoNetworkEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->displayRegistrationNetworkFailure(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 250
    nop

    :sswitch_data_0
    .sparse-switch
        0x88d3 -> :sswitch_1
        0x88d7 -> :sswitch_0
        0x8912 -> :sswitch_2
        0x89f8 -> :sswitch_0
    .end sparse-switch
.end method

.method public isInRegisterViewMode()Z
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isShowFixedSignUpButton()Z
    .locals 1

    .prologue
    .line 895
    const/4 v0, 0x1

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 601
    const-string v0, "Tango.RegisterProfileUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult(): requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    invoke-super {p0, p1, p2, p3}, Lcom/sgiggle/production/ActivityBase;->onActivityResult(IILandroid/content/Intent;)V

    .line 604
    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 607
    const-string v0, "selectedCountry"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CountryListActivity$CountryData;

    .line 608
    iget-object v1, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_cid:Ljava/lang/String;

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    .line 609
    iget-object v1, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_code:Ljava/lang/String;

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    .line 610
    iget-object v1, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_name:Ljava/lang/String;

    iput-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryName:Ljava/lang/String;

    .line 611
    iget-object v0, v0, Lcom/sgiggle/production/CountryListActivity$CountryData;->m_isocc:Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    .line 612
    const-string v0, "Tango.RegisterProfileUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "... Selected: Id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Country = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCode:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_isoCountryCode:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/sgiggle/production/RegisterUserActivity;->displayCountryCode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 615
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->formatPhoneNumber()V

    .line 618
    :cond_0
    const/16 v0, 0x7f99

    if-ne p1, v0, :cond_1

    .line 620
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/android/Facebook;->authorizeCallback(IILandroid/content/Intent;)V

    .line 622
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 785
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_0

    .line 788
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 789
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    .line 795
    :goto_0
    return-void

    .line 791
    :cond_0
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "onBackPressed(). Send \'No-State-Change\' message..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 436
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 437
    const-string v1, "Tango.RegisterProfileUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick(View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    sparse-switch v0, :sswitch_data_0

    .line 538
    const-string v1, "Tango.RegisterProfileUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onClick: unexpected click: View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :goto_0
    :sswitch_0
    return-void

    .line 446
    :sswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_0

    const-string v0, "reg_"

    :goto_1
    const-string v1, "submit_clicked"

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 450
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 451
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 452
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 453
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_storeContacts:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v6

    .line 455
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 456
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_1

    const-string v0, "reg_"

    :goto_2
    const-string v1, "account_info_validation_error"

    const-string v2, "account_info_validation_error_reason"

    const-string v3, "empty_phone"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_EMPTY:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->displaySaveFailed(Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;)V

    goto :goto_0

    .line 446
    :cond_0
    const-string v0, "personal_info_"

    goto :goto_1

    .line 456
    :cond_1
    const-string v0, "personal_info_"

    goto :goto_2

    :cond_2
    move v0, v7

    move v1, v7

    .line 465
    :goto_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 466
    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v7

    invoke-static {v7}, Ljava/lang/Character;->isDigit(C)Z

    move-result v7

    if-eqz v7, :cond_3

    add-int/lit8 v1, v1, 0x1

    .line 465
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 468
    :cond_4
    const/4 v0, 0x5

    if-ge v1, v0, :cond_6

    .line 469
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_5

    const-string v0, "reg_"

    :goto_4
    const-string v1, "account_info_validation_error"

    const-string v2, "account_info_validation_error_reason"

    const-string v3, "phone_invalid"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->PHONE_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->displaySaveFailed(Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;)V

    goto/16 :goto_0

    .line 469
    :cond_5
    const-string v0, "personal_info_"

    goto :goto_4

    .line 478
    :cond_6
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    invoke-static {v5}, Lcom/sgiggle/production/Utils;->isEmailStringValid(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 479
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_7

    const-string v0, "reg_"

    :goto_5
    const-string v1, "account_info_validation_error"

    const-string v2, "account_info_validation_error_reason"

    const-string v3, "email_invalid"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    sget-object v0, Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;->EMAIL_INVALID:Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->displaySaveFailed(Lcom/sgiggle/production/RegisterUserActivity$saveFailedReason;)V

    goto/16 :goto_0

    .line 479
    :cond_7
    const-string v0, "personal_info_"

    goto :goto_5

    .line 487
    :cond_8
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_a

    .line 488
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 489
    const v0, 0x7f090183

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 490
    const v0, 0x7f090184

    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 492
    const v0, 0x7f090185

    new-instance v1, Lcom/sgiggle/production/RegisterUserActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/RegisterUserActivity$3;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    invoke-virtual {v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 505
    const v8, 0x7f09001c

    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$4;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/RegisterUserActivity$4;-><init>(Lcom/sgiggle/production/RegisterUserActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v7, v8, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 518
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    sget-object v1, Lcom/sgiggle/production/RegisterUserActivity$ViewMode;->VIEW_MODE_REGISTER:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    if-ne v0, v1, :cond_9

    const-string v0, "reg_"

    :goto_6
    const-string v1, "optional_email_dialog"

    const-string v2, "optional_email_dialog_reason"

    const-string v3, "dialog_appeared"

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sgiggle/production/RegisterUserActivity;->statsCollectorLog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 524
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 518
    :cond_9
    const-string v0, "personal_info_"

    goto :goto_6

    .line 529
    :cond_a
    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_viewMode:Lcom/sgiggle/production/RegisterUserActivity$ViewMode;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/RegisterUserActivity;->doRegisterUser(Lcom/sgiggle/production/RegisterUserActivity$ViewMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 533
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$EndStateNoChangeMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 439
    :sswitch_data_0
    .sparse-switch
        0x7f0a00aa -> :sswitch_1
        0x7f0a00fd -> :sswitch_0
        0x7f0a010d -> :sswitch_1
        0x7f0a010f -> :sswitch_1
        0x7f0a0110 -> :sswitch_2
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 889
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 890
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 182
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 184
    const v0, 0x7f030043

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->setContentView(I)V

    .line 185
    const v0, 0x7f0a00f9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_root_layout:Landroid/widget/RelativeLayout;

    .line 186
    const v0, 0x7f0a00fb

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_reg_linear_layout:Landroid/widget/LinearLayout;

    .line 187
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030026

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_orig_ll:Landroid/widget/LinearLayout;

    .line 188
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030025

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_bottom_ll:Landroid/widget/LinearLayout;

    .line 189
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030027

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    .line 191
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_title:Landroid/widget/TextView;

    .line 192
    const v0, 0x7f0a00fd

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    .line 193
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryCodeText:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 195
    const v0, 0x7f0a00fe

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    .line 196
    const v0, 0x7f0a0102

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    .line 197
    const v0, 0x7f0a0104

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    .line 198
    const v0, 0x7f0a0100

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    .line 200
    const v0, 0x7f0a0106

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_privacyLayout:Landroid/view/ViewGroup;

    .line 201
    const v0, 0x7f0a010a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_storeContacts:Landroid/widget/CheckBox;

    .line 203
    const v0, 0x7f0a010c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registerBottomButtonsLayout:Landroid/widget/LinearLayout;

    .line 204
    const v0, 0x7f0a010d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_bottomSignUpButton:Landroid/widget/Button;

    .line 205
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_bottomSignUpButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    const v0, 0x7f0a010e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_profileBottomButtonsLayout:Landroid/widget/LinearLayout;

    .line 208
    const v0, 0x7f0a010f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_saveButton:Landroid/widget/Button;

    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_saveButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    const v0, 0x7f0a0110

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_cancelButton:Landroid/widget/Button;

    .line 211
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_cancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 212
    const v0, 0x7f0a0111

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_progressBar:Landroid/widget/ProgressBar;

    .line 214
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->setupFBABTesting()V

    .line 216
    new-instance v0, Lcom/sgiggle/production/RegisterUserActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/RegisterUserActivity$1;-><init>(Lcom/sgiggle/production/RegisterUserActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneTextWatcher:Landroid/text/TextWatcher;

    .line 225
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 227
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_phoneNumberEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 228
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_firstNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 229
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_lastNameEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_emailText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 232
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setRegisterActivityInstance(Lcom/sgiggle/production/RegisterUserActivity;)V

    .line 233
    invoke-virtual {p0}, Lcom/sgiggle/production/RegisterUserActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/RegisterUserActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 234
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 805
    const-string v0, "Tango.RegisterProfileUI"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 807
    invoke-direct {p0}, Lcom/sgiggle/production/RegisterUserActivity;->closeProgressDialog()V

    .line 808
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setRegisterActivityInstance(Lcom/sgiggle/production/RegisterUserActivity;)V

    .line 809
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 585
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_1

    .line 586
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a00fd

    if-ne v0, v1, :cond_0

    .line 587
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/CountryListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 588
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 589
    const-string v1, "countries"

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_selectableCountries:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 590
    const-string v1, "selectedCountryId"

    iget-object v2, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_countryId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 591
    invoke-virtual {p0, v0, v3}, Lcom/sgiggle/production/RegisterUserActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    move v0, v4

    .line 595
    :goto_0
    return v0

    :cond_1
    move v0, v3

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 799
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onUserInteraction()V

    .line 800
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 801
    return-void
.end method

.method protected toggleSignUpButton(Z)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 920
    if-eqz p1, :cond_1

    .line 921
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fixedSignUpButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 922
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registerBottomButtonsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 923
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 932
    :cond_0
    :goto_0
    return-void

    .line 927
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fixedSignUpButtonLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 928
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registerBottomButtonsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 929
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_registrationLayout:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;->TANGO_BOTTOM_AND_FB_TOP:Lcom/sgiggle/xmpp/SessionMessages$RegistrationOptions$RegistrationLayout;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/sgiggle/production/RegisterUserActivity;->m_fb_signup_top_ll:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
