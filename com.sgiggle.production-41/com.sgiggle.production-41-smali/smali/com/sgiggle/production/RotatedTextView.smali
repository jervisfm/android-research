.class public Lcom/sgiggle/production/RotatedTextView;
.super Landroid/widget/TextView;
.source "RotatedTextView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "RotatedTextView"


# instance fields
.field private isBold:Z

.field private m_displaymetrics:Landroid/util/DisplayMetrics;

.field private m_textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_displaymetrics:Landroid/util/DisplayMetrics;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/RotatedTextView;->isBold:Z

    .line 29
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_displaymetrics:Landroid/util/DisplayMetrics;

    .line 24
    iput-boolean v3, p0, Lcom/sgiggle/production/RotatedTextView;->isBold:Z

    .line 35
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    move v0, v3

    .line 37
    :goto_0
    invoke-interface {p2}, Landroid/util/AttributeSet;->getAttributeCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 38
    invoke-interface {p2, v0}, Landroid/util/AttributeSet;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "textStyle"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    invoke-interface {p2, v0, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(II)I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 40
    iput-boolean v4, p0, Lcom/sgiggle/production/RotatedTextView;->isBold:Z

    .line 37
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_displaymetrics:Landroid/util/DisplayMetrics;

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/RotatedTextView;->isBold:Z

    .line 49
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    .line 50
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    .line 149
    iget-object v1, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    .line 152
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 153
    if-nez v1, :cond_0

    .line 155
    const-string v0, "RotatedTextView"

    const-string v1, "can\'t create bitmap2"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :goto_0
    return-void

    .line 159
    :cond_0
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 160
    iget-object v3, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 162
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 163
    const/high16 v3, 0x4387

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 164
    const/4 v3, 0x0

    int-to-float v0, v0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 165
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 129
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 130
    iget-object v1, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 133
    iget-object v2, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 135
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    .line 136
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .parameter
    .parameter

    .prologue
    const/4 v10, 0x0

    .line 80
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0, p2, p1}, Landroid/widget/TextView;->measure(II)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 83
    iget-object v1, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 86
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v2

    .line 87
    if-eqz v2, :cond_6

    .line 88
    invoke-virtual {v2}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/RotatedTextView;->m_displaymetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 89
    iget-object v2, p0, Lcom/sgiggle/production/RotatedTextView;->m_displaymetrics:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 90
    new-instance v3, Landroid/text/TextPaint;

    invoke-direct {v3}, Landroid/text/TextPaint;-><init>()V

    .line 91
    iget-object v4, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 92
    invoke-virtual {p0}, Lcom/sgiggle/production/RotatedTextView;->getTextSize()F

    move-result v5

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 93
    iget-boolean v5, p0, Lcom/sgiggle/production/RotatedTextView;->isBold:Z

    if-eqz v5, :cond_0

    .line 94
    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v5}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 96
    :cond_0
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    new-array v5, v5, [F

    .line 97
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v3, v4, v10, v6, v5}, Landroid/text/TextPaint;->getTextWidths(Ljava/lang/String;II[F)I

    .line 98
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 99
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v3, v4, v10, v7, v6}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    move v3, v10

    move v4, v10

    .line 102
    :goto_0
    array-length v7, v5

    if-ge v3, v7, :cond_1

    .line 103
    int-to-float v4, v4

    aget v7, v5, v3

    add-float/2addr v4, v7

    float-to-int v4, v4

    .line 102
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 111
    :cond_1
    iget v3, v6, Landroid/graphics/Rect;->bottom:I

    iget v5, v6, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v5

    .line 112
    if-le v4, v2, :cond_2

    mul-int/lit8 v5, v3, 0x2

    if-lt v1, v5, :cond_3

    :cond_2
    if-gt v0, v2, :cond_3

    int-to-double v4, v4

    int-to-double v6, v2

    const-wide v8, 0x3feb333333333333L

    mul-double/2addr v6, v8

    cmpl-double v2, v4, v6

    if-lez v2, :cond_5

    mul-int/lit8 v2, v3, 0x2

    if-ge v1, v2, :cond_5

    :cond_3
    const/4 v2, 0x1

    .line 116
    :goto_1
    if-eqz v2, :cond_4

    mul-int/lit8 v1, v1, 0x2

    :cond_4
    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/RotatedTextView;->setMeasuredDimension(II)V

    .line 121
    :goto_2
    return-void

    :cond_5
    move v2, v10

    .line 112
    goto :goto_1

    .line 119
    :cond_6
    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/RotatedTextView;->setMeasuredDimension(II)V

    goto :goto_2
.end method

.method public setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1
    .parameter

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 70
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/RotatedTextView;->m_textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {p0}, Lcom/sgiggle/production/RotatedTextView;->requestLayout()V

    .line 59
    invoke-virtual {p0}, Lcom/sgiggle/production/RotatedTextView;->invalidate()V

    .line 61
    :cond_0
    return-void
.end method
