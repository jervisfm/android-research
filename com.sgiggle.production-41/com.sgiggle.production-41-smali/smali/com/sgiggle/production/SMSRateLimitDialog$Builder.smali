.class public Lcom/sgiggle/production/SMSRateLimitDialog$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "SMSRateLimitDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/SMSRateLimitDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/SMSRateLimitDialog$Builder;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->statsCollectorLog(Ljava/lang/String;)V

    return-void
.end method

.method private statsCollectorLog(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 73
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "sms_verification_dialog_ui"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 74
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/iphelper/IpHelper;->getDevIDBase64()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 77
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "fb_valid_session"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "1"

    :goto_0
    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 83
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;-><init>(Ljava/util/List;)V

    .line 86
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 87
    return-void

    .line 79
    :cond_0
    const-string v2, "0"

    goto :goto_0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 40
    const v0, 0x7f090172

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 41
    const v0, 0x7f090173

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 43
    new-instance v0, Lcom/sgiggle/production/SMSRateLimitDialog$Builder$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder$1;-><init>(Lcom/sgiggle/production/SMSRateLimitDialog$Builder;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 55
    const v0, 0x7f09001f

    new-instance v1, Lcom/sgiggle/production/SMSRateLimitDialog$Builder$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder$2;-><init>(Lcom/sgiggle/production/SMSRateLimitDialog$Builder;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 66
    const-string v0, "rate_limited_dialog_appeared"

    invoke-direct {p0, v0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;->statsCollectorLog(Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
