.class public Lcom/sgiggle/production/SMSRateLimitDialog;
.super Lcom/sgiggle/production/dialog/TangoAlertDialog;
.source "SMSRateLimitDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/SMSRateLimitDialog$Builder;
    }
.end annotation


# static fields
.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "deviceid"

.field private static final KEY_FB_VALID_SESSION:Ljava/lang/String; = "fb_valid_session"

.field private static final KEY_SMS_VERIFICATION_DIALOG_UI:Ljava/lang/String; = "sms_verification_dialog_ui"

.field private static final VALUE_SMS_VERIFICATION_DIALOG_UI_RATE_LIMITED_DIALOG_APPEARED:Ljava/lang/String; = "rate_limited_dialog_appeared"

.field private static final VALUE_SMS_VERIFICATION_DIALOG_UI_RATE_LIMITED_OK:Ljava/lang/String; = "rate_limited_OK"


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method
