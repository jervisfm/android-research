.class synthetic Lcom/sgiggle/production/SelectContactActivity$3;
.super Ljava/lang/Object;
.source "SelectContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/SelectContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

.field static final synthetic $SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 376
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->values()[Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I

    :try_start_0
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 117
    :goto_1
    invoke-static {}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->values()[Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

    :try_start_2
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    .line 376
    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method
