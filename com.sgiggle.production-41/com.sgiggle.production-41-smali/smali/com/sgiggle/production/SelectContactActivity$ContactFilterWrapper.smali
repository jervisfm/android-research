.class Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;
.super Ljava/lang/Object;
.source "SelectContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/SelectContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactFilterWrapper"
.end annotation


# instance fields
.field private m_available:Z

.field private m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field private m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

.field private m_contacts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_filterId:I

.field private m_filterSeparatorId:I

.field private m_selectContactFragment:Lcom/sgiggle/production/fragment/SelectContactFragment;

.field final synthetic this$0:Lcom/sgiggle/production/SelectContactActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/SelectContactActivity;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;IIZLcom/sgiggle/contacts/ContactStore$ContactOrderPair;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 87
    iput-object p1, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->this$0:Lcom/sgiggle/production/SelectContactActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_filterSeparatorId:I

    .line 71
    iput-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_available:Z

    .line 83
    invoke-static {}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 88
    iput-object p2, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 89
    iput p3, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_filterId:I

    .line 90
    iput p4, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_filterSeparatorId:I

    .line 91
    iput-boolean p5, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_available:Z

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    .line 93
    iput-object p6, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 94
    return-void
.end method


# virtual methods
.method public addContact(Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 1
    .parameter

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    return-void
.end method

.method public getContactListSelection()Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    return-object v0
.end method

.method public getContacts()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFilterId()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_filterId:I

    return v0
.end method

.method public getFilterSeparatorId()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_filterSeparatorId:I

    return v0
.end method

.method public getSelectContactFragment(Z)Lcom/sgiggle/production/fragment/SelectContactFragment;
    .locals 4
    .parameter

    .prologue
    .line 114
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSelectContactFragment: createIfNull="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_selectContactFragment:Lcom/sgiggle/production/fragment/SelectContactFragment;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 117
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$production$adapter$ContactListAdapter$ContactListSelection:[I

    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 127
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_selectContactFragment:Lcom/sgiggle/production/fragment/SelectContactFragment;

    return-object v0

    .line 122
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    iget-object v3, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->this$0:Lcom/sgiggle/production/SelectContactActivity;

    #getter for: Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I
    invoke-static {v3}, Lcom/sgiggle/production/SelectContactActivity;->access$000(Lcom/sgiggle/production/SelectContactActivity;)I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->newInstance(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)Lcom/sgiggle/production/fragment/SelectContactListFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_selectContactFragment:Lcom/sgiggle/production/fragment/SelectContactFragment;

    goto :goto_0

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_available:Z

    return v0
.end method

.method public sortContacts()V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contacts:Ljava/util/ArrayList;

    new-instance v1, Lcom/sgiggle/production/Utils$ContactComparator;

    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    invoke-virtual {v2}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sgiggle/production/Utils$ContactComparator;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 149
    :cond_0
    return-void
.end method
