.class Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "SelectContactActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/SelectContactActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectContactFragmentAdapter"
.end annotation


# instance fields
.field private m_numPages:I

.field final synthetic this$0:Lcom/sgiggle/production/SelectContactActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/SelectContactActivity;Landroid/support/v4/app/FragmentManager;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 506
    iput-object p1, p0, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;->this$0:Lcom/sgiggle/production/SelectContactActivity;

    .line 507
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 508
    iput p3, p0, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;->m_numPages:I

    .line 509
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 513
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;->m_numPages:I

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3
    .parameter

    .prologue
    .line 518
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SelectContactFragmentAdapter.getItem: position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;->this$0:Lcom/sgiggle/production/SelectContactActivity;

    #getter for: Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sgiggle/production/SelectContactActivity;->access$200(Lcom/sgiggle/production/SelectContactActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 520
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getSelectContactFragment(Z)Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v0

    return-object v0
.end method
