.class public Lcom/sgiggle/production/SelectContactActivity;
.super Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;
.source "SelectContactActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;
.implements Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/SelectContactActivity$3;,
        Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;,
        Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;
    }
.end annotation


# static fields
.field private static final DIALOG_MAX_SELECTION_REACHED:I = 0x0

.field public static final FORWARD_MAX_SELECTION:I = 0xc8

.field public static final MAX_SELECTION_INFINITE:I = -0x1

.field private static final MSG_SHOW_FOOTER_FRAGMENT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.SelectContactActivity"

.field private static s_instance:Lcom/sgiggle/production/SelectContactActivity;


# instance fields
.field private m_availableFilterWrappers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private m_contactFilterWrappers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private m_filterWrappersBySelection:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
            "Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

.field private m_fragmentAdapter:Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;

.field private m_handler:Landroid/os/Handler;

.field private m_maxSelection:I

.field private m_selectContactEventReceived:Z

.field private m_selectContactFilterGroup:Landroid/widget/RadioGroup;

.field private m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

.field private m_selectedContacts:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_selectionDone:Z

.field private m_viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;-><init>()V

    .line 191
    iput-boolean v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactEventReceived:Z

    .line 197
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    .line 201
    iput-boolean v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectionDone:Z

    .line 204
    new-instance v0, Lcom/sgiggle/production/SelectContactActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/SelectContactActivity$1;-><init>(Lcom/sgiggle/production/SelectContactActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_handler:Landroid/os/Handler;

    .line 503
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/SelectContactActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 55
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/SelectContactActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sgiggle/production/SelectContactActivity;->showFooterFragment(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/SelectContactActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    return-object v0
.end method

.method private cancelSelection()V
    .locals 2

    .prologue
    .line 616
    iget-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectionDone:Z

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "cancelSelection: selection already done or canceled, ignoring call."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :goto_0
    return-void

    .line 623
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectionDone:Z

    .line 624
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->sendCancelSelectContactMessage()V

    goto :goto_0
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/SelectContactActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 221
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity;->s_instance:Lcom/sgiggle/production/SelectContactActivity;

    if-ne v0, p0, :cond_0

    .line 222
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/SelectContactActivity;->s_instance:Lcom/sgiggle/production/SelectContactActivity;

    .line 223
    :cond_0
    return-void
.end method

.method private getCurrentFragment()Lcom/sgiggle/production/fragment/SelectContactFragment;
    .locals 2

    .prologue
    .line 722
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 723
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 724
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getSelectContactFragment(Z)Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v0

    .line 728
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/SelectContactActivity;
    .locals 1

    .prologue
    .line 213
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity;->s_instance:Lcom/sgiggle/production/SelectContactActivity;

    return-object v0
.end method

.method private handleSelectContactEvent(Lcom/sgiggle/messaging/Message;)V
    .locals 13
    .parameter

    .prologue
    const v5, 0x7f0b0007

    const/16 v3, 0xc8

    const/16 v12, 0x8

    const/4 v4, 0x1

    const/4 v11, 0x0

    .line 357
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    .line 358
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getFromPhone(Landroid/content/Context;)Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v6

    .line 361
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->hasMaxSelection()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getMaxSelection()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    .line 363
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSelectContactEvent: maxSelection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 373
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    move-result-object v7

    .line 374
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleSelectContactEvent: selectContactType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    sget-object v0, Lcom/sgiggle/production/SelectContactActivity$3;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I

    invoke-virtual {v7}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v8, v11

    move v9, v11

    move v5, v11

    .line 403
    :goto_1
    iget-object v10, p0, Lcom/sgiggle/production/SelectContactActivity;->m_contactFilterWrappers:Ljava/util/ArrayList;

    new-instance v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    sget-object v2, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const v3, 0x7f0a0113

    const v4, 0x7f0a0114

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;-><init>(Lcom/sgiggle/production/SelectContactActivity;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;IIZLcom/sgiggle/contacts/ContactStore$ContactOrderPair;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    iget-object v10, p0, Lcom/sgiggle/production/SelectContactActivity;->m_contactFilterWrappers:Ljava/util/ArrayList;

    new-instance v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    sget-object v2, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const v3, 0x7f0a0115

    const v4, 0x7f0a0116

    move-object v1, p0

    move v5, v9

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;-><init>(Lcom/sgiggle/production/SelectContactActivity;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;IIZLcom/sgiggle/contacts/ContactStore$ContactOrderPair;)V

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    iget-object v9, p0, Lcom/sgiggle/production/SelectContactActivity;->m_contactFilterWrappers:Ljava/util/ArrayList;

    new-instance v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    sget-object v2, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const v3, 0x7f0a0117

    move-object v1, p0

    move v4, v11

    move v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;-><init>(Lcom/sgiggle/production/SelectContactActivity;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;IIZLcom/sgiggle/contacts/ContactStore$ContactOrderPair;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_contactFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 424
    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_filterWrappersBySelection:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getContactListSelection()Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->isAvailable()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 427
    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 365
    :cond_1
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "handleSelectContactEvent: no max selection, consider infinite."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 382
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->setTitle(Ljava/lang/CharSequence;)V

    move v8, v4

    move v9, v4

    move v5, v11

    .line 384
    goto/16 :goto_1

    .line 392
    :pswitch_1
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    if-le v0, v3, :cond_3

    .line 394
    :cond_2
    iput v3, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    .line 398
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    new-array v2, v4, [Ljava/lang/Object;

    iget v3, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    invoke-virtual {v0, v5, v1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->setTitle(Ljava/lang/CharSequence;)V

    move v8, v4

    move v9, v4

    move v5, v4

    goto/16 :goto_1

    .line 430
    :cond_4
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterId()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/sgiggle/production/SelectContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/view/View;->setVisibility(I)V

    .line 431
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterSeparatorId()I

    move-result v2

    if-eqz v2, :cond_0

    .line 432
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterSeparatorId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 439
    if-lez v1, :cond_6

    .line 440
    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactFilterGroup:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterId()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 445
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 448
    new-instance v0, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-direct {v0, p0, v2, v1}, Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;-><init>(Lcom/sgiggle/production/SelectContactActivity;Landroid/support/v4/app/FragmentManager;I)V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_fragmentAdapter:Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;

    .line 450
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_fragmentAdapter:Lcom/sgiggle/production/SelectContactActivity$SelectContactFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 454
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getContactsList()Ljava/util/List;

    move-result-object v3

    .line 455
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_filterWrappersBySelection:Ljava/util/HashMap;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 456
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_filterWrappersBySelection:Ljava/util/HashMap;

    sget-object v2, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 457
    iget-object v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_filterWrappersBySelection:Ljava/util/HashMap;

    sget-object v4, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 459
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 460
    invoke-static {v3}, Lcom/sgiggle/production/Utils$UIContact;->convertFromMessageContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/production/Utils$UIContact;

    move-result-object v5

    .line 463
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 464
    invoke-virtual {v0, v5}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->addContact(Lcom/sgiggle/production/Utils$UIContact;)V

    .line 468
    :cond_8
    invoke-virtual {v1}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    .line 469
    invoke-virtual {v1, v5}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->addContact(Lcom/sgiggle/production/Utils$UIContact;)V

    .line 473
    :cond_9
    invoke-virtual {v2}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->isAvailable()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFavorite()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 474
    invoke-virtual {v2, v5}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->addContact(Lcom/sgiggle/production/Utils$UIContact;)V

    goto :goto_3

    .line 479
    :cond_a
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_b
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 481
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->sortContacts()V

    .line 483
    invoke-virtual {v0, v11}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getSelectContactFragment(Z)Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v2

    .line 484
    if-eqz v2, :cond_b

    .line 485
    const-string v3, "Tango.SelectContactActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSelectContactEvent: give contacts for selection="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getContactListSelection()Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getContacts()Ljava/util/ArrayList;

    move-result-object v0

    iget v3, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    invoke-virtual {v2, v0, v3}, Lcom/sgiggle/production/fragment/SelectContactFragment;->setContacts(Ljava/util/ArrayList;I)V

    goto :goto_4

    .line 492
    :cond_c
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->isMultiSelection()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 493
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    invoke-virtual {v0, v7}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->setSelectContactType(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)V

    .line 494
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 497
    :cond_d
    return-void

    .line 376
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isMultiSelection()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 742
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    if-gt v0, v2, :cond_0

    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private sendCancelSelectContactMessage()V
    .locals 3

    .prologue
    .line 631
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "sendCancelSelectContactMessage"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSelectContactMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelSelectContactMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 634
    return-void
.end method

.method private sendSelectContactResultMessage()V
    .locals 5

    .prologue
    .line 640
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectionDone:Z

    .line 643
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 644
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 645
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 646
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIContact;->convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 649
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;

    iget-object v4, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactPayload:Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;

    invoke-virtual {v4}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    move-result-object v4

    invoke-direct {v3, v4, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactResultMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;Ljava/util/List;)V

    invoke-virtual {v0, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 652
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/SelectContactActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 217
    sput-object p0, Lcom/sgiggle/production/SelectContactActivity;->s_instance:Lcom/sgiggle/production/SelectContactActivity;

    .line 218
    return-void
.end method

.method private showFooterFragment(Z)V
    .locals 3
    .parameter

    .prologue
    .line 529
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showFooterFragment: show="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 531
    const v1, 0x7f040009

    const v2, 0x7f04000d

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 533
    if-eqz p1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 540
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 541
    return-void

    .line 538
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method


# virtual methods
.method public getActionBarHomeIconResId()I
    .locals 1

    .prologue
    .line 733
    const v0, 0x7f0200b6

    return v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 336
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 350
    :goto_0
    return-void

    .line 338
    :pswitch_0
    iget-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactEventReceived:Z

    if-eqz v0, :cond_0

    .line 340
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "Ignoring SELECT_CONTACT_EVENT, it was already received."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 342
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactEventReceived:Z

    .line 343
    invoke-direct {p0, p1}, Lcom/sgiggle/production/SelectContactActivity;->handleSelectContactEvent(Lcom/sgiggle/messaging/Message;)V

    .line 346
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 336
    nop

    :pswitch_data_0
    .packed-switch 0x89d2
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 600
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->getCurrentFragment()Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v0

    .line 601
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->handleBackPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "BACK event handled by current fragment, catching event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    :goto_0
    return-void

    .line 609
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->cancelSelection()V

    goto :goto_0
.end method

.method public onCancelClicked()V
    .locals 0

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->cancelSelection()V

    .line 595
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 545
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 546
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 552
    :cond_1
    const/4 v0, -0x1

    .line 553
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v2, v0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 554
    add-int/lit8 v2, v2, 0x1

    .line 555
    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterId()I

    move-result v0

    if-ne v0, p2, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 556
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public onContactSelectionChanged(Lcom/sgiggle/production/Utils$UIContact;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 657
    invoke-virtual {p0, v1, p2}, Lcom/sgiggle/production/SelectContactActivity;->trySelectContacts(IZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    if-eqz p2, :cond_2

    .line 663
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 670
    :goto_1
    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-eqz v0, :cond_3

    .line 671
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "onContactSelectionChanged: sending result."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->sendSelectContactResultMessage()V

    goto :goto_0

    .line 665
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 677
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->isMultiSelection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->onSelectedCountChanged(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 227
    const-string v0, "Tango.SelectContactActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 232
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->setContentView(I)V

    .line 235
    const v0, 0x7f0a0112

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactFilterGroup:Landroid/widget/RadioGroup;

    .line 236
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactFilterGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 239
    const v0, 0x7f0a0118

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    .line 240
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 243
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    .line 244
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    .line 245
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_filterWrappersBySelection:Ljava/util/HashMap;

    .line 246
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_contactFilterWrappers:Ljava/util/ArrayList;

    .line 249
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0119

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    iput-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_footerFragment:Lcom/sgiggle/production/fragment/SelectContactFooterFragment;

    .line 251
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->showFooterFragment(Z)V

    .line 254
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 255
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SelectContactActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 258
    :cond_0
    invoke-static {p0}, Lcom/sgiggle/production/SelectContactActivity;->setRunningInstance(Lcom/sgiggle/production/SelectContactActivity;)V

    .line 259
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 288
    packed-switch p1, :pswitch_data_0

    .line 300
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 290
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 291
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 292
    const v1, 0x7f09001f

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/SelectContactActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/SelectContactActivity$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/SelectContactActivity$2;-><init>(Lcom/sgiggle/production/SelectContactActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 263
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onDestroy()V

    .line 264
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->cancelSelection()V

    .line 265
    invoke-static {p0}, Lcom/sgiggle/production/SelectContactActivity;->clearRunningInstance(Lcom/sgiggle/production/SelectContactActivity;)V

    .line 266
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 278
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 283
    :goto_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 280
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->cancelSelection()V

    goto :goto_0

    .line 278
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .parameter

    .prologue
    .line 565
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 570
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .parameter

    .prologue
    .line 575
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactFilterGroup:Landroid/widget/RadioGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_availableFilterWrappers:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;

    .line 577
    iget-object v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectContactFilterGroup:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getFilterId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioGroup;->check(I)V

    .line 580
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/SelectContactActivity$ContactFilterWrapper;->getSelectContactFragment(Z)Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v0

    .line 581
    if-eqz v0, :cond_0

    .line 582
    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onSelected()V

    .line 585
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 328
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPause()V

    .line 331
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->stop()V

    .line 332
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 270
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 273
    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getActionBarHelper()Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->setDisplayHomeAsUpEnabled(Z)V

    .line 274
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 306
    packed-switch p1, :pswitch_data_0

    .line 314
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 316
    :goto_0
    return-void

    .line 308
    :pswitch_0
    check-cast p2, Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/sgiggle/production/SelectContactActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0009

    iget v2, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onResume()V

    .line 323
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->start()V

    .line 324
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 709
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->getCurrentFragment()Lcom/sgiggle/production/fragment/SelectContactFragment;

    move-result-object v0

    .line 710
    if-eqz v0, :cond_0

    .line 711
    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onSearchRequested()Z

    move-result v0

    .line 713
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onSearchRequested()Z

    move-result v0

    goto :goto_0
.end method

.method public onSelectClicked()V
    .locals 0

    .prologue
    .line 589
    invoke-direct {p0}, Lcom/sgiggle/production/SelectContactActivity;->sendSelectContactResultMessage()V

    .line 590
    return-void
.end method

.method public trySelectContacts(IZ)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 685
    iget-boolean v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectionDone:Z

    if-eqz v0, :cond_0

    .line 686
    const-string v0, "Tango.SelectContactActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "trySelectContacts with nbContacts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selected="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Selection was refused since user already validated a selection."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 703
    :goto_0
    return v0

    .line 691
    :cond_0
    if-eqz p2, :cond_1

    iget v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    :cond_1
    move v0, v2

    .line 693
    goto :goto_0

    .line 697
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/SelectContactActivity;->m_selectedContacts:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    add-int/2addr v0, p1

    iget v1, p0, Lcom/sgiggle/production/SelectContactActivity;->m_maxSelection:I

    if-gt v0, v1, :cond_3

    move v0, v2

    .line 698
    goto :goto_0

    .line 702
    :cond_3
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/SelectContactActivity;->showDialog(I)V

    move v0, v3

    .line 703
    goto :goto_0
.end method
