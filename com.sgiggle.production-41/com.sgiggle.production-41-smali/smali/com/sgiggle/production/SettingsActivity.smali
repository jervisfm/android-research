.class public Lcom/sgiggle/production/SettingsActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.SettingsActivity"

.field private static m_postponedForResume:Lcom/sgiggle/messaging/Message;

.field private static s_instance:Lcom/sgiggle/production/SettingsActivity;


# instance fields
.field private m_accountEdit:Landroid/widget/Button;

.field private m_accountEmail:Landroid/widget/TextView;

.field private m_accountName:Landroid/widget/TextView;

.field private m_accountPhoneNumber:Landroid/widget/TextView;

.field private m_alertContainer:Landroid/view/View;

.field private m_alertDivider:Landroid/view/View;

.field private m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

.field private m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

.field private m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

.field private m_copyrightVersion:Landroid/widget/TextView;

.field private m_helpBtn:Landroid/widget/Button;

.field private m_tips_url:Ljava/lang/String;

.field private m_underHoodHeader:Landroid/widget/TextView;

.field private m_viewLog:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    return-void
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/SettingsActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 95
    sget-object v0, Lcom/sgiggle/production/SettingsActivity;->s_instance:Lcom/sgiggle/production/SettingsActivity;

    if-ne v0, p0, :cond_0

    .line 96
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/SettingsActivity;->s_instance:Lcom/sgiggle/production/SettingsActivity;

    .line 97
    :cond_0
    return-void
.end method

.method private createAlertAdapterAndShowView(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 559
    new-instance v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/adapter/AlertListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 560
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/widget/FixedListView;->setAdapter(Landroid/widget/Adapter;)V

    .line 563
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 564
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertDivider:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 565
    return-void
.end method

.method private createCTAforBackgroundDataAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 3

    .prologue
    .line 545
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setSeverity(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 551
    return-object v0
.end method

.method private createCTAforC2dmAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 3

    .prologue
    .line 533
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setTitle(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;->AS_OK:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->setSeverity(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$AlertSeverity;)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    .line 538
    return-object v0
.end method

.method private getAppVersion()Ljava/lang/String;
    .locals 4

    .prologue
    .line 597
    const-string v0, ""

    .line 600
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v0, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 606
    :cond_0
    const-string v0, "1.0.NA"

    .line 609
    :cond_1
    return-object v0

    .line 601
    :catch_0
    move-exception v1

    .line 602
    const-string v2, "Tango.SettingsActivity"

    const-string v3, "getAppVersion(): Tango package not found"

    invoke-static {v2, v3, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getCopyrightAndVersionInfo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 583
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->getAppVersion()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 585
    invoke-static {}, Lcom/sgiggle/util/TangoEnvironment;->getEnvironmentName()Ljava/lang/String;

    move-result-object v1

    .line 586
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 589
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/SettingsActivity;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sgiggle/production/SettingsActivity;->s_instance:Lcom/sgiggle/production/SettingsActivity;

    return-object v0
.end method

.method private hideAlertsViewIfNoAlert()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 571
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 572
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 573
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertDivider:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 575
    :cond_0
    return-void
.end method

.method private launchAccountsAndSyncActivity()V
    .locals 2

    .prologue
    .line 225
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.SYNC_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 227
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 228
    return-void
.end method

.method private launchRegistrationActivity()V
    .locals 3

    .prologue
    .line 236
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPersonalInfoMessage;-><init>()V

    .line 237
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 238
    return-void
.end method

.method private removeRegistrationAndValidationCTAs()V
    .locals 5

    .prologue
    .line 412
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 413
    const/4 v1, 0x0

    .line 416
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/AlertListAdapter;->getDataSet()Ljava/util/List;

    move-result-object v0

    .line 420
    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 421
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 422
    new-instance v3, Lcom/sgiggle/production/CTANotifier$AppAlert;

    invoke-direct {v3, v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 424
    invoke-virtual {v3}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isRegistrationRequired()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v3}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isValidationRequired()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 425
    :cond_1
    const-string v1, "Tango.SettingsActivity"

    const-string v2, "Remove Alert"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/adapter/AlertListAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/AlertListAdapter;->remove(Ljava/lang/Object;)V

    .line 428
    const/4 v0, 0x1

    .line 433
    :goto_0
    if-eqz v0, :cond_2

    .line 437
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->refresh()V

    .line 441
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->hideAlertsViewIfNoAlert()V

    .line 443
    :cond_3
    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static setMessage4resume(Lcom/sgiggle/messaging/Message;)V
    .locals 0
    .parameter

    .prologue
    .line 288
    sput-object p0, Lcom/sgiggle/production/SettingsActivity;->m_postponedForResume:Lcom/sgiggle/messaging/Message;

    .line 289
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/SettingsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    sput-object p0, Lcom/sgiggle/production/SettingsActivity;->s_instance:Lcom/sgiggle/production/SettingsActivity;

    .line 92
    return-void
.end method

.method private showHidePremiumSection(Z)V
    .locals 0
    .parameter

    .prologue
    .line 624
    return-void
.end method

.method private startAppLogActivity()V
    .locals 2

    .prologue
    .line 276
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/AppLogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 277
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 278
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 279
    return-void
.end method

.method private startAppLogActivityViaStateMachine()V
    .locals 3

    .prologue
    .line 268
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestAppLogMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestAppLogMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 270
    return-void
.end method

.method private startTipsActivityViaStateMachine()V
    .locals 3

    .prologue
    .line 255
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFAQMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestFAQMessage;-><init>()V

    .line 256
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 257
    return-void
.end method

.method private startTipsScreenViaBrowser()V
    .locals 3

    .prologue
    .line 260
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_tips_url:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 261
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 262
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 263
    return-void
.end method

.method private startVideomailSubscriptionActivity()V
    .locals 3

    .prologue
    .line 192
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryProductCatalogMessage;

    const/4 v1, 0x1

    const-string v2, "product.category.videomail"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryProductCatalogMessage;-><init>(ILjava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 194
    return-void
.end method

.method private udpateBackgroundDataAlertInfo()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 485
    const-string v0, "Tango.SettingsActivity"

    const-string v1, "Check if we need to enable background data alert CTA"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 488
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    .line 490
    :goto_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 498
    :cond_0
    const-string v1, "Tango.SettingsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Show background data alert="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    if-eqz v0, :cond_6

    .line 501
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 502
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 503
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->createAlertAdapterAndShowView(Ljava/util/List;)V

    .line 506
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    .line 510
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    if-eqz v1, :cond_3

    .line 511
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->remove(Ljava/lang/Object;)V

    .line 512
    iput-object v4, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 515
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->createCTAforBackgroundDataAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 516
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->add(Ljava/lang/Object;)V

    .line 524
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->refresh()V

    .line 526
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->hideAlertsViewIfNoAlert()V

    .line 527
    return-void

    .line 488
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    if-eqz v0, :cond_4

    .line 519
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->remove(Ljava/lang/Object;)V

    .line 520
    iput-object v4, p0, Lcom/sgiggle/production/SettingsActivity;->m_backgroundDataAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    goto :goto_1
.end method

.method private updateAlertInfo(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 389
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 390
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 394
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 395
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->createAlertAdapterAndShowView(Ljava/util/List;)V

    .line 399
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->updateC2dmAlertInfo()V

    .line 402
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->udpateBackgroundDataAlertInfo()V

    .line 405
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->hideAlertsViewIfNoAlert()V

    .line 406
    return-void
.end method

.method private updateC2dmAlertInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 449
    invoke-static {}, Lcom/sgiggle/production/Utils;->isDeviceAndroid22Plus()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/sgiggle/production/Utils;->getGoogleAccounts(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 452
    :goto_0
    if-eqz v0, :cond_5

    .line 453
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 454
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 455
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->createAlertAdapterAndShowView(Ljava/util/List;)V

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    .line 462
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    if-eqz v1, :cond_2

    .line 463
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->remove(Ljava/lang/Object;)V

    .line 464
    iput-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 467
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->createCTAforC2dmAlert()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 468
    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->add(Ljava/lang/Object;)V

    .line 476
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->refresh()V

    .line 478
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->hideAlertsViewIfNoAlert()V

    .line 479
    return-void

    .line 449
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 470
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    if-eqz v0, :cond_3

    .line 471
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/FixedListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AlertListAdapter;

    iget-object v1, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->remove(Ljava/lang/Object;)V

    .line 472
    iput-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_c2dmAlert:Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    goto :goto_1
.end method

.method private updatePersonalInfo(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;)V
    .locals 6
    .parameter

    .prologue
    .line 358
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;

    .line 359
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    .line 362
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasDisplayname()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v2

    .line 363
    :goto_0
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v3

    .line 364
    :goto_1
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v4

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuffer;

    const-string v5, "+"

    invoke-direct {v4, v5}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getCountryCode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 369
    :goto_2
    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 370
    iget-object v4, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountName:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    :goto_3
    iget-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountEmail:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 376
    iget-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountPhoneNumber:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RegisterUserPayload;->getRegistered()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->showHidePremiumSection(Z)V

    .line 378
    return-void

    .line 362
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 363
    :cond_1
    const-string v3, ""

    goto :goto_1

    .line 364
    :cond_2
    const-string v1, ""

    goto :goto_2

    .line 373
    :cond_3
    iget-object v2, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountName:Landroid/widget/TextView;

    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method


# virtual methods
.method public applyPostponedEvent()V
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/sgiggle/production/SettingsActivity;->m_postponedForResume:Lcom/sgiggle/messaging/Message;

    if-eqz v0, :cond_0

    .line 297
    sget-object v0, Lcom/sgiggle/production/SettingsActivity;->m_postponedForResume:Lcom/sgiggle/messaging/Message;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 298
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/SettingsActivity;->m_postponedForResume:Lcom/sgiggle/messaging/Message;

    .line 300
    :cond_0
    return-void
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 308
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 349
    :goto_0
    return-void

    .line 310
    :sswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;

    move-object v1, v0

    .line 311
    invoke-direct {p0, v1}, Lcom/sgiggle/production/SettingsActivity;->updatePersonalInfo(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsEvent;)V

    .line 312
    invoke-static {p1}, Lcom/sgiggle/production/CTANotifier;->extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/SettingsActivity;->updateAlertInfo(Ljava/util/List;)V

    goto :goto_0

    .line 318
    :sswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->removeRegistrationAndValidationCTAs()V

    .line 319
    invoke-static {p1}, Lcom/sgiggle/production/CTANotifier;->extractAlertList(Lcom/sgiggle/messaging/Message;)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/SettingsActivity;->updateAlertInfo(Ljava/util/List;)V

    goto :goto_0

    .line 324
    :sswitch_2
    const-string v1, "Tango.SettingsActivity"

    const-string v2, "Handle Validation-Result: clear registration/validation CTAs..."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->removeRegistrationAndValidationCTAs()V

    goto :goto_0

    .line 330
    :sswitch_3
    invoke-static {p1}, Lcom/sgiggle/production/AppLogActivity;->storeAppLogEntries(Lcom/sgiggle/messaging/Message;)V

    .line 331
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->startAppLogActivity()V

    goto :goto_0

    .line 335
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;

    .line 336
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 337
    new-instance v2, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 338
    invoke-virtual {v2, v1}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->create(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v1

    .line 339
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 343
    :sswitch_5
    new-instance v1, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 344
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 345
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 308
    :sswitch_data_0
    .sparse-switch
        0x88de -> :sswitch_1
        0x88e9 -> :sswitch_0
        0x88eb -> :sswitch_4
        0x8910 -> :sswitch_2
        0x892a -> :sswitch_5
        0x892c -> :sswitch_3
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 171
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$NavigateBackMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$NavigateBackMessage;-><init>()V

    .line 172
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 173
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_helpBtn:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 179
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->startTipsScreenViaBrowser()V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_viewLog:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 182
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->startAppLogActivityViaStateMachine()V

    goto :goto_0

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountEdit:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 185
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->launchRegistrationActivity()V

    goto :goto_0

    .line 186
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a013e

    if-ne v0, v1, :cond_0

    .line 187
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->startVideomailSubscriptionActivity()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 103
    const-string v0, "Tango.SettingsActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v0, 0x7f030048

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->setContentView(I)V

    .line 108
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_tips_url:Ljava/lang/String;

    .line 111
    const v0, 0x7f0a0126

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountName:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0a0128

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountPhoneNumber:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0a0129

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountEmail:Landroid/widget/TextView;

    .line 114
    const v0, 0x7f0a0134

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_copyrightVersion:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0a0131

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_helpBtn:Landroid/widget/Button;

    .line 116
    const v0, 0x7f0a0132

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_underHoodHeader:Landroid/widget/TextView;

    .line 117
    const v0, 0x7f0a0133

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_viewLog:Landroid/widget/Button;

    .line 118
    const v0, 0x7f0a0127

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountEdit:Landroid/widget/Button;

    .line 119
    const v0, 0x7f0a0130

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/FixedListView;

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    .line 120
    const v0, 0x7f0a012e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertContainer:Landroid/view/View;

    .line 121
    const v0, 0x7f0a012d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertDivider:Landroid/view/View;

    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_helpBtn:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_viewLog:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_accountEdit:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_alertsList:Lcom/sgiggle/production/widget/FixedListView;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/FixedListView;->setOnItemClickListener(Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;)V

    .line 131
    const v0, 0x7f0a013e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 133
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->videomailSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 134
    const v0, 0x7f0a013d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_copyrightVersion:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->getCopyrightAndVersionInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 141
    if-eqz v0, :cond_1

    .line 142
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 145
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->applyPostponedEvent()V

    .line 146
    invoke-static {p0}, Lcom/sgiggle/production/SettingsActivity;->setRunningInstance(Lcom/sgiggle/production/SettingsActivity;)V

    .line 147
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 152
    invoke-static {p0}, Lcom/sgiggle/production/SettingsActivity;->clearRunningInstance(Lcom/sgiggle/production/SettingsActivity;)V

    .line 153
    return-void
.end method

.method public onItemClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 208
    instance-of v1, v0, Lcom/sgiggle/production/CTANotifier$AppAlert;

    if-eqz v1, :cond_0

    .line 209
    check-cast v0, Lcom/sgiggle/production/CTANotifier$AppAlert;

    .line 212
    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isUpgradeRequired()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    invoke-static {p0}, Lcom/sgiggle/production/UpdateRequiredActivity;->forceTangoUpdate(Landroid/content/Context;)V

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isRegistrationRequired()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isValidationRequired()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 215
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->launchRegistrationActivity()V

    goto :goto_0

    .line 216
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 217
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->launchAccountsAndSyncActivity()V

    goto :goto_0

    .line 218
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900a8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->launchAccountsAndSyncActivity()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 157
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 158
    const-string v0, "Tango.SettingsActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p0}, Lcom/sgiggle/production/SettingsActivity;->applyPostponedEvent()V

    .line 162
    invoke-direct {p0}, Lcom/sgiggle/production/SettingsActivity;->updateC2dmAlertInfo()V

    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_underHoodHeader:Landroid/widget/TextView;

    sget-boolean v1, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/SettingsActivity;->m_viewLog:Landroid/widget/Button;

    sget-boolean v1, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 167
    return-void

    :cond_0
    move v1, v3

    .line 165
    goto :goto_0

    :cond_1
    move v1, v3

    .line 166
    goto :goto_1
.end method

.method public startTipsActivity()V
    .locals 2

    .prologue
    .line 245
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/TipsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 246
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 247
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 248
    return-void
.end method
