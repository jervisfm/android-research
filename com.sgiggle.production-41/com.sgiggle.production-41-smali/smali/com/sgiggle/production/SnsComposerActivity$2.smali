.class Lcom/sgiggle/production/SnsComposerActivity$2;
.super Ljava/lang/Object;
.source "SnsComposerActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/SnsComposerActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/SnsComposerActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/SnsComposerActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sgiggle/production/SnsComposerActivity$2;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v4, 0x118

    const/16 v3, 0x8c

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity$2;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    #getter for: Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/SnsComposerActivity;->access$100(Lcom/sgiggle/production/SnsComposerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GBK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    .line 116
    if-lt v0, v4, :cond_0

    .line 117
    const-string v0, ""

    .line 136
    :goto_0
    return-object v0

    .line 119
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GBK"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v1, v1

    add-int/2addr v1, v0

    if-le v1, v4, :cond_1

    .line 120
    iget-object v1, p0, Lcom/sgiggle/production/SnsComposerActivity$2;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    #calls: Lcom/sgiggle/production/SnsComposerActivity;->trimGBK(ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    invoke-static {v1, v0, p1, p2}, Lcom/sgiggle/production/SnsComposerActivity;->access$000(Lcom/sgiggle/production/SnsComposerActivity;ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 122
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    const-string v0, "Tango.SnsComposerActivity"

    const-string v1, "filter(): can NOT support GBK encoding!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity$2;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    #getter for: Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sgiggle/production/SnsComposerActivity;->access$100(Lcom/sgiggle/production/SnsComposerActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 129
    if-lt v0, v3, :cond_2

    .line 130
    const-string v0, ""

    goto :goto_0

    .line 131
    :cond_2
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v1, v0

    if-le v1, v3, :cond_3

    .line 132
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/2addr v0, v1

    sub-int/2addr v0, v3

    .line 133
    sub-int v0, p3, v0

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 136
    goto :goto_0
.end method
