.class Lcom/sgiggle/production/SnsComposerActivity$MyEditText;
.super Landroid/widget/EditText;
.source "SnsComposerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/SnsComposerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyEditText"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/SnsComposerActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/SnsComposerActivity;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    .line 38
    invoke-direct {p0, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/production/SnsComposerActivity;Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    .line 35
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public onTextContextMenuItem(I)Z
    .locals 10
    .parameter

    .prologue
    const/16 v6, 0x118

    const/16 v9, 0x8c

    const/4 v8, 0x0

    .line 42
    packed-switch p1, :pswitch_data_0

    .line 69
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 45
    invoke-virtual {v0}, Landroid/text/ClipboardManager;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    .line 47
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GBK"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v2, v2

    .line 48
    if-lt v2, v6, :cond_0

    move v0, v8

    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const-string v3, "Tango.SnsComposerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTextContextMenuItem() paste event src="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GBK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3

    add-int/2addr v3, v2

    if-le v3, v6, :cond_1

    .line 52
    iget-object v3, p0, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    const/4 v4, 0x0

    #calls: Lcom/sgiggle/production/SnsComposerActivity;->trimGBK(ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    invoke-static {v3, v2, v1, v4}, Lcom/sgiggle/production/SnsComposerActivity;->access$000(Lcom/sgiggle/production/SnsComposerActivity;ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 53
    const-string v3, "Tango.SnsComposerActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "final len="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->this$0:Lcom/sgiggle/production/SnsComposerActivity;

    const/4 v7, 0x0

    #calls: Lcom/sgiggle/production/SnsComposerActivity;->trimGBK(ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    invoke-static {v6, v2, v1, v7}, Lcom/sgiggle/production/SnsComposerActivity;->access$000(Lcom/sgiggle/production/SnsComposerActivity;ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "GBK"

    invoke-virtual {v2, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto/16 :goto_0

    .line 57
    :catch_0
    move-exception v2

    .line 58
    const-string v2, "Tango.SnsComposerActivity"

    const-string v3, "filter(): can NOT support GBK encoding!"

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-virtual {p0}, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 61
    if-lt v2, v9, :cond_2

    move v0, v8

    .line 62
    goto/16 :goto_0

    .line 63
    :cond_2
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v3, v2

    if-le v3, v9, :cond_3

    .line 64
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v9

    .line 65
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sub-int v2, v3, v2

    invoke-interface {v1, v8, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/EditText;->onTextContextMenuItem(I)Z

    move-result v0

    goto/16 :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1020022
        :pswitch_0
    .end packed-switch
.end method
