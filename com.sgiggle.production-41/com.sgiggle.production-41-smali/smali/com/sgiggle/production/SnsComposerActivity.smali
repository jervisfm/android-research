.class public Lcom/sgiggle/production/SnsComposerActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "SnsComposerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/SnsComposerActivity$MyEditText;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.SnsComposerActivity"


# instance fields
.field private final WEIBO_LIMITATION:I

.field private isProcessing:Z

.field private m_editText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->isProcessing:Z

    .line 31
    const/16 v0, 0x8c

    iput v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->WEIBO_LIMITATION:I

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/SnsComposerActivity;ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/SnsComposerActivity;->trimGBK(ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/SnsComposerActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sgiggle/production/SnsComposerActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/sgiggle/production/SnsComposerActivity;->isProcessing:Z

    return p1
.end method

.method private cancelProcessing()V
    .locals 3

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->isProcessing:Z

    if-eqz v0, :cond_0

    .line 153
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsCancelProcessMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$SnsCancelProcessMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 155
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->isProcessing:Z

    .line 161
    :goto_0
    return-void

    .line 158
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private trimGBK(ILjava/lang/CharSequence;I)Ljava/lang/CharSequence;
    .locals 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GBK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    array-length v0, v0

    add-int/2addr v0, p1

    const/16 v1, 0x118

    sub-int/2addr v0, v1

    .line 76
    const-string v1, "Tango.SnsComposerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extra = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    .line 78
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    .line 79
    :goto_0
    if-lez v1, :cond_0

    .line 80
    invoke-interface {p2, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "GBK"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    array-length v3, v3

    if-ne v3, v0, :cond_1

    .line 84
    :cond_0
    invoke-interface {p2, p3, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    .line 82
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0a00c4

    const/4 v3, 0x1

    .line 177
    const-string v0, "Tango.SnsComposerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 204
    :goto_0
    :pswitch_0
    return-void

    .line 180
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/SnsComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsPublishResultEvent;

    .line 182
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteSnsPublishResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload;

    .line 184
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload;->getSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 185
    const v0, 0x7f0900ef

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 191
    :goto_1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    .line 192
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 186
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload;->getErrorType()Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload$Error;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload$Error;->REPEAT_TEXT:Lcom/sgiggle/xmpp/SessionMessages$SnsProcessResultPayload$Error;

    if-ne v0, v1, :cond_1

    .line 187
    const v0, 0x7f0900f0

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 189
    :cond_1
    const v0, 0x7f0900f1

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 196
    :pswitch_2
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/SnsComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 178
    nop

    :pswitch_data_0
    .packed-switch 0x8992
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/sgiggle/production/SnsComposerActivity;->cancelProcessing()V

    .line 149
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/16 v3, 0x21

    .line 89
    const-string v0, "Tango.SnsComposerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 91
    const v0, 0x7f03004f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SnsComposerActivity;->setContentView(I)V

    .line 93
    new-instance v0, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;

    invoke-direct {v0, p0, p0}, Lcom/sgiggle/production/SnsComposerActivity$MyEditText;-><init>(Lcom/sgiggle/production/SnsComposerActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    .line 94
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    invoke-static {v3, v3, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 95
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    const v1, 0x7f0900f3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(I)V

    .line 97
    const v0, 0x7f0a0142

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SnsComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    .line 98
    iget-object v1, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 99
    const v0, 0x7f0a0143

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SnsComposerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 100
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    .line 101
    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 102
    new-instance v1, Lcom/sgiggle/production/SnsComposerActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/SnsComposerActivity$1;-><init>(Lcom/sgiggle/production/SnsComposerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v0, p0, Lcom/sgiggle/production/SnsComposerActivity;->m_editText:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/sgiggle/production/SnsComposerActivity$2;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/SnsComposerActivity$2;-><init>(Lcom/sgiggle/production/SnsComposerActivity;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 141
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setSnsActivityInstance(Lcom/sgiggle/production/SnsComposerActivity;)V

    .line 142
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 165
    const-string v0, "Tango.SnsComposerActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setSnsActivityInstance(Lcom/sgiggle/production/SnsComposerActivity;)V

    .line 167
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 168
    return-void
.end method
