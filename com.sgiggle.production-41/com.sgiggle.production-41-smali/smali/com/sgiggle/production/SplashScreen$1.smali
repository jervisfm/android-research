.class Lcom/sgiggle/production/SplashScreen$1;
.super Ljava/lang/Object;
.source "SplashScreen.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/SplashScreen;->getDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/SplashScreen;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/SplashScreen;)V
    .locals 0
    .parameter

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sgiggle/production/SplashScreen$1;->this$0:Lcom/sgiggle/production/SplashScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 197
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 198
    const-string v1, "http://www.tango.me/error62"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 199
    iget-object v1, p0, Lcom/sgiggle/production/SplashScreen$1;->this$0:Lcom/sgiggle/production/SplashScreen;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/SplashScreen;->startActivity(Landroid/content/Intent;)V

    .line 200
    iget-object v0, p0, Lcom/sgiggle/production/SplashScreen$1;->this$0:Lcom/sgiggle/production/SplashScreen;

    invoke-virtual {v0}, Lcom/sgiggle/production/SplashScreen;->finish()V

    .line 202
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Lcom/sgiggle/production/SplashScreen$1;->this$0:Lcom/sgiggle/production/SplashScreen;

    #getter for: Lcom/sgiggle/production/SplashScreen;->m_initFailingException:Ljava/lang/Exception;
    invoke-static {v1}, Lcom/sgiggle/production/SplashScreen;->access$000(Lcom/sgiggle/production/SplashScreen;)Ljava/lang/Exception;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/SplashScreen$1;->this$0:Lcom/sgiggle/production/SplashScreen;

    #getter for: Lcom/sgiggle/production/SplashScreen;->m_initFailingException:Ljava/lang/Exception;
    invoke-static {v2}, Lcom/sgiggle/production/SplashScreen;->access$000(Lcom/sgiggle/production/SplashScreen;)Ljava/lang/Exception;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method
