.class public Lcom/sgiggle/production/SplashScreen;
.super Landroid/app/Activity;
.source "SplashScreen.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static final DIALOG_INSTALL_FAILED:I = 0x0

.field private static final ERROR_62_URL:Ljava/lang/String; = "http://www.tango.me/error62"

.field private static final LOG_AUTHORITY_PREFIX:Ljava/lang/String; = "log"

.field private static final LOG_ENABLED_AUTHORITY:Ljava/lang/String; = "logenable"

.field private static final MSG_INIT:I = 0x0

.field private static final SETCONFIGVAL_AUTHORITY_PREFIX:Ljava/lang/String; = "setconfigval"

.field private static final TAG:Ljava/lang/String; = "Tango.SplashScreen"

.field private static final TANGO_SCHEMA:Ljava/lang/String; = "tango"

.field private static final WELCOME_SCREEN_AUTHORITY:Ljava/lang/String; = "welcome_screen"


# instance fields
.field m_handler:Landroid/os/Handler;

.field private m_initFailingException:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/SplashScreen;)Ljava/lang/Exception;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/SplashScreen;->m_initFailingException:Ljava/lang/Exception;

    return-object v0
.end method

.method private getDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 188
    if-nez p1, :cond_0

    .line 189
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 190
    const v1, 0x7f090186

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 191
    const v1, 0x7f090187

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 192
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 193
    const v1, 0x7f090188

    new-instance v2, Lcom/sgiggle/production/SplashScreen$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/SplashScreen$1;-><init>(Lcom/sgiggle/production/SplashScreen;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 205
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 207
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private handleCall(Landroid/net/Uri;)V
    .locals 12
    .parameter

    .prologue
    .line 129
    const/4 v6, 0x0

    .line 130
    const-string v7, ""

    .line 131
    const-wide/16 v8, -0x1

    .line 133
    invoke-virtual {p0}, Lcom/sgiggle/production/SplashScreen;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "data1"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "data4"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "data5"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 139
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 140
    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v3, v3

    .line 141
    const-string v5, "Tango.SplashScreen"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate(): ... found rawContact: peerJid = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], peerName = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], deviceContactId = ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "], _ID=["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "]"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v10, v3

    move-object v3, v1

    move-wide v0, v10

    .line 146
    :goto_0
    if-eqz v3, :cond_1

    .line 147
    invoke-static {v3}, Lcom/sgiggle/production/ContactListActivity;->findContact(Ljava/lang/String;)Lcom/sgiggle/production/Utils$UIContact;

    move-result-object v4

    .line 148
    if-eqz v4, :cond_0

    .line 149
    invoke-virtual {v4}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v0

    .line 150
    iget-wide v1, v4, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    .line 151
    const-string v4, "Tango.SplashScreen"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate(): ... received: foundContact = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v10, v1

    move-object v2, v0

    move-wide v0, v10

    .line 154
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v4

    invoke-virtual {v4, v3, v2, v0, v1}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;J)V

    .line 156
    invoke-virtual {p0}, Lcom/sgiggle/production/SplashScreen;->finish()V

    .line 158
    :cond_1
    return-void

    :cond_2
    move-wide v0, v8

    move-object v2, v7

    move-object v3, v6

    goto :goto_0
.end method

.method private handleIntent(Landroid/content/Intent;Z)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 99
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 100
    if-eqz v0, :cond_4

    .line 101
    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 102
    if-eqz v1, :cond_3

    const-string v2, "tango"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 103
    invoke-virtual {v0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    .line 104
    if-eqz v1, :cond_2

    const-string v2, "log"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "setconfigval"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 105
    :cond_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SplashScreen;->handleLog(Landroid/net/Uri;)V

    .line 106
    const-string v0, "logenable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/Startup;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 111
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 112
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SplashScreen;->startActivity(Landroid/content/Intent;)V

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/SplashScreen;->finish()V

    .line 115
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    .line 116
    :cond_2
    const-string v2, "welcome_screen"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SplashScreen;->handleWelcomeUri(Landroid/net/Uri;)V

    move v0, v3

    .line 118
    goto :goto_0

    .line 120
    :cond_3
    if-nez p2, :cond_4

    .line 121
    invoke-direct {p0, v0}, Lcom/sgiggle/production/SplashScreen;->handleCall(Landroid/net/Uri;)V

    move v0, v3

    .line 122
    goto :goto_0

    :cond_4
    move v0, v3

    .line 125
    goto :goto_0
.end method

.method private handleLog(Landroid/net/Uri;)V
    .locals 4
    .parameter

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    if-eqz p1, :cond_0

    .line 163
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 164
    if-eqz v1, :cond_0

    const-string v2, "tango"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 166
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/util/LogReporter;->enableUri(Ljava/lang/String;)Z

    move-result v0

    .line 169
    :cond_0
    const-string v1, "Tango.SplashScreen"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LogReporter.enableUri() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v0, :cond_1

    const-string v0, "successful"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-void

    .line 169
    :cond_1
    const-string v0, "failed"

    goto :goto_0
.end method

.method private handleWelcomeUri(Landroid/net/Uri;)V
    .locals 1
    .parameter

    .prologue
    .line 174
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->handleWelcomeUrl(Ljava/lang/String;)Z

    .line 175
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    invoke-virtual {p0}, Lcom/sgiggle/production/SplashScreen;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/SplashScreen;->handleIntent(Landroid/content/Intent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    iput-object v0, p0, Lcom/sgiggle/production/SplashScreen;->m_initFailingException:Ljava/lang/Exception;

    .line 79
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/SplashScreen;->showDialog(I)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .parameter

    .prologue
    .line 66
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 71
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 68
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/SplashScreen;->init()V

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/SplashScreen;->setContentView(I)V

    .line 59
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/sgiggle/production/SplashScreen;->m_handler:Landroid/os/Handler;

    .line 60
    const-wide/16 v0, 0xc8

    .line 61
    iget-object v2, p0, Lcom/sgiggle/production/SplashScreen;->m_handler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 62
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/sgiggle/production/SplashScreen;->getDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 216
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/sgiggle/production/SplashScreen;->getDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .parameter

    .prologue
    .line 90
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/SplashScreen;->handleIntent(Landroid/content/Intent;Z)Z

    .line 91
    return-void
.end method
