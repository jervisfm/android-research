.class public Lcom/sgiggle/production/Startup;
.super Landroid/app/Activity;
.source "Startup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Startup"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->isInstallationOk()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "Startup"

    const-string v1, "Second instance started, skipping..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-virtual {p0}, Lcom/sgiggle/production/Startup;->finish()V

    .line 57
    :goto_0
    return-void

    .line 30
    :cond_0
    const-string v0, "Startup"

    const-string v1, "Initialized. Asking to resume."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    goto :goto_0

    .line 33
    :cond_1
    const-string v0, "Startup"

    const-string v1, "NOT initialized. Starting splash screen."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/SplashScreen;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 36
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/Startup;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 63
    const-string v0, "Startup"

    const-string v1, "We should never RE-ENTER this screen. Calling finish()."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-virtual {p0}, Lcom/sgiggle/production/Startup;->finish()V

    .line 67
    return-void
.end method
