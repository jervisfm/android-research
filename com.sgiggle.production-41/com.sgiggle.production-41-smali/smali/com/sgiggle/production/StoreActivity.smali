.class public Lcom/sgiggle/production/StoreActivity;
.super Lcom/sgiggle/production/FragmentActivityBase;
.source "StoreActivity.java"


# instance fields
.field private m_fragment:Lcom/sgiggle/production/fragment/StoreCategoryListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sgiggle/production/FragmentActivityBase;-><init>()V

    return-void
.end method

.method private handleDisplayEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;)V
    .locals 1
    .parameter

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    invoke-direct {p0}, Lcom/sgiggle/production/StoreActivity;->showErrorMessage()V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/StoreActivity;->showCatalog()V

    goto :goto_0
.end method

.method private showCatalog()V
    .locals 2

    .prologue
    .line 51
    const v0, 0x7f0a0145

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 52
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    return-void
.end method

.method private showErrorMessage()V
    .locals 2

    .prologue
    .line 46
    const v0, 0x7f0a0034

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 47
    const v0, 0x7f0a0145

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    return-void
.end method


# virtual methods
.method protected finishIfResumedAfterKilled()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 35
    :goto_0
    return-void

    .line 30
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;

    move-object v1, v0

    invoke-direct {p0, v1}, Lcom/sgiggle/production/StoreActivity;->handleDisplayEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;)V

    .line 31
    iget-object v1, p0, Lcom/sgiggle/production/StoreActivity;->m_fragment:Lcom/sgiggle/production/fragment/StoreCategoryListFragment;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x89c0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 17
    const v0, 0x7f030051

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->setContentView(I)V

    .line 19
    invoke-virtual {p0}, Lcom/sgiggle/production/StoreActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0145

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;

    iput-object v0, p0, Lcom/sgiggle/production/StoreActivity;->m_fragment:Lcom/sgiggle/production/fragment/StoreCategoryListFragment;

    .line 22
    invoke-virtual {p0}, Lcom/sgiggle/production/StoreActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 23
    invoke-virtual {p0}, Lcom/sgiggle/production/StoreActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/StoreActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 24
    :cond_0
    return-void
.end method
