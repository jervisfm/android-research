.class public Lcom/sgiggle/production/SwipeDetector;
.super Ljava/lang/Object;
.source "SwipeDetector.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/SwipeDetector$GestureHandler;
    }
.end annotation


# static fields
.field static final BottomToTop:I = 0x3

.field static final LeftToRight:I = 0x1

.field static final RightToLeft:I = 0x0

.field static final SWIPE_MIN_DISTANCE:I = 0x64

.field static final TAG:Ljava/lang/String; = "SwipeDetector"

.field static final TopToBottom:I = 0x2


# instance fields
.field private REL_SWIPE_MIN_DISTANCE:I

.field private downX:F

.field private downY:F

.field private gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

.field private m_Orientation:I

.field private upX:F

.field private upY:F


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/SwipeDetector$GestureHandler;ILandroid/util/DisplayMetrics;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/sgiggle/production/SwipeDetector;->gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

    .line 29
    iput p2, p0, Lcom/sgiggle/production/SwipeDetector;->m_Orientation:I

    .line 31
    iget v0, p3, Landroid/util/DisplayMetrics;->densityDpi:I

    mul-int/lit8 v0, v0, 0x64

    int-to-float v0, v0

    const/high16 v1, 0x4320

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sgiggle/production/SwipeDetector;->REL_SWIPE_MIN_DISTANCE:I

    .line 32
    return-void
.end method


# virtual methods
.method public onBottomToTopSwipe()V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/SwipeDetector;->gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/sgiggle/production/SwipeDetector$GestureHandler;->handleSwipe(I)V

    .line 52
    return-void
.end method

.method public onLeftToRightSwipe()V
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/SwipeDetector;->gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sgiggle/production/SwipeDetector$GestureHandler;->handleSwipe(I)V

    .line 42
    return-void
.end method

.method public onRightToLeftSwipe()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sgiggle/production/SwipeDetector;->gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sgiggle/production/SwipeDetector$GestureHandler;->handleSwipe(I)V

    .line 37
    return-void
.end method

.method public onTopToBottomSwipe()V
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sgiggle/production/SwipeDetector;->gestureHandler:Lcom/sgiggle/production/SwipeDetector$GestureHandler;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/sgiggle/production/SwipeDetector$GestureHandler;->handleSwipe(I)V

    .line 47
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 55
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 119
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 57
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/SwipeDetector;->downX:F

    .line 58
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/SwipeDetector;->downY:F

    move v0, v4

    .line 60
    goto :goto_0

    .line 64
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/SwipeDetector;->upX:F

    .line 65
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/SwipeDetector;->upY:F

    .line 67
    iget v0, p0, Lcom/sgiggle/production/SwipeDetector;->downX:F

    iget v1, p0, Lcom/sgiggle/production/SwipeDetector;->upX:F

    sub-float/2addr v0, v1

    .line 68
    iget v1, p0, Lcom/sgiggle/production/SwipeDetector;->downY:F

    iget v2, p0, Lcom/sgiggle/production/SwipeDetector;->upY:F

    sub-float/2addr v1, v2

    .line 71
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/sgiggle/production/SwipeDetector;->REL_SWIPE_MIN_DISTANCE:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    .line 73
    cmpg-float v2, v0, v5

    if-gez v2, :cond_1

    .line 74
    iget v0, p0, Lcom/sgiggle/production/SwipeDetector;->m_Orientation:I

    if-nez v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onTopToBottomSwipe()V

    :goto_1
    move v0, v4

    .line 79
    goto :goto_0

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onLeftToRightSwipe()V

    goto :goto_1

    .line 81
    :cond_1
    cmpl-float v0, v0, v5

    if-lez v0, :cond_3

    .line 82
    iget v0, p0, Lcom/sgiggle/production/SwipeDetector;->m_Orientation:I

    if-nez v0, :cond_2

    .line 83
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onBottomToTopSwipe()V

    :goto_2
    move v0, v4

    .line 87
    goto :goto_0

    .line 85
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onRightToLeftSwipe()V

    goto :goto_2

    .line 94
    :cond_3
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/sgiggle/production/SwipeDetector;->REL_SWIPE_MIN_DISTANCE:I

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_7

    .line 96
    cmpg-float v0, v1, v5

    if-gez v0, :cond_5

    .line 97
    iget v0, p0, Lcom/sgiggle/production/SwipeDetector;->m_Orientation:I

    if-nez v0, :cond_4

    .line 98
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onRightToLeftSwipe()V

    :goto_3
    move v0, v4

    .line 102
    goto :goto_0

    .line 100
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onTopToBottomSwipe()V

    goto :goto_3

    .line 104
    :cond_5
    cmpl-float v0, v1, v5

    if-lez v0, :cond_7

    .line 105
    iget v0, p0, Lcom/sgiggle/production/SwipeDetector;->m_Orientation:I

    if-nez v0, :cond_6

    .line 106
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onLeftToRightSwipe()V

    :goto_4
    move v0, v4

    .line 110
    goto :goto_0

    .line 108
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/production/SwipeDetector;->onBottomToTopSwipe()V

    goto :goto_4

    :cond_7
    move v0, v4

    .line 116
    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
