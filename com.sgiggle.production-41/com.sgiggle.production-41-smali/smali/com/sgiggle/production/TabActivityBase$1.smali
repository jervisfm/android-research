.class Lcom/sgiggle/production/TabActivityBase$1;
.super Ljava/lang/Object;
.source "TabActivityBase.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/TabActivityBase;->setupTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/TabActivityBase;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/TabActivityBase;)V
    .locals 0
    .parameter

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sgiggle/production/TabActivityBase$1;->this$0:Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 241
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 242
    const-string v0, "threaded_conversations"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase$1;->this$0:Lcom/sgiggle/production/TabActivityBase;

    #getter for: Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;
    invoke-static {v1}, Lcom/sgiggle/production/TabActivityBase;->access$100(Lcom/sgiggle/production/TabActivityBase;)Landroid/widget/TabHost;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase$1;->this$0:Lcom/sgiggle/production/TabActivityBase;

    #calls: Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;
    invoke-static {v0}, Lcom/sgiggle/production/TabActivityBase;->access$200(Lcom/sgiggle/production/TabActivityBase;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    .line 244
    invoke-virtual {v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->scrollToRelevantItem()V

    .line 248
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
