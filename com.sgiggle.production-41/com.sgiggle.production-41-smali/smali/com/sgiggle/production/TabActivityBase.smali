.class public Lcom/sgiggle/production/TabActivityBase;
.super Landroid/app/TabActivity;
.source "TabActivityBase.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;
    }
.end annotation


# static fields
.field public static final CALL_LOG_TAB:Ljava/lang/String; = "call_log"

.field public static final CONTACTS_TAB:Ljava/lang/String; = "contacts"

.field private static final HELP_MENU_ID:I = 0x3

.field public static final INVITE_TAB:Ljava/lang/String; = "invite"

.field private static final SETTINGS_MENU_ID:I = 0x2

.field public static final STORE_TAB:Ljava/lang/String; = "store"

.field public static final TAB_ACTIVITY_FIRST_MENU_ID:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Tango.TabsUI"

.field public static final THREADED_CONVERSATIONS_TAB:Ljava/lang/String; = "threaded_conversations"


# instance fields
.field private forceSwtichTab:Z

.field private m_contactSearchActivity:Lcom/sgiggle/production/ContactListActivity;

.field private m_firstMessage:Lcom/sgiggle/messaging/Message;

.field private m_inviteBadgeCount:I

.field private m_isForegroundActivity:Z

.field private m_isInSearchMode:Z

.field private m_smsCapable:Z

.field private m_tabFooter:Landroid/view/View;

.field private m_tabHost:Landroid/widget/TabHost;

.field public m_tabIndexByTag:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Landroid/app/TabActivity;-><init>()V

    .line 70
    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isForegroundActivity:Z

    .line 71
    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isInSearchMode:Z

    .line 73
    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 78
    iput v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    .line 79
    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_smsCapable:Z

    .line 87
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/TabActivityBase;)Landroid/widget/TabHost;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/TabActivityBase;)Landroid/app/Activity;
    .locals 1
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private static createTabView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 291
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030055

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 292
    const v0, 0x7f0a014c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    const v0, 0x7f0a014a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 294
    return-object v1
.end method

.method private getCurrentTabActivity()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 545
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;
    .locals 2

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    .line 553
    check-cast v0, Lcom/sgiggle/production/ActivityStack;

    return-object v0
.end method

.method private handleMissedCallIntent(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 699
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMissedCallIntent(): missedJid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    const-string v0, "missedJid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 703
    new-instance v0, Lcom/sgiggle/production/Utils$UIMissedCall;

    const-string v1, "missedDisplayname"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "missedWhen"

    const-wide/16 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/sgiggle/production/Utils$UIMissedCall;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 706
    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/MissedCallNotifier;->saveMissedCallNotification(Lcom/sgiggle/production/Utils$UIMissedCall;)V

    .line 708
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    .line 709
    if-eqz v0, :cond_0

    .line 710
    const-string v0, "Tango.TabsUI"

    const-string v1, "handleMissedCallIntent(): A Tango call is in progresss. Finish this activity."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->finish()V

    .line 717
    :goto_0
    return-void

    .line 713
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 714
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private handleNotificationBar(Ljava/lang/String;Landroid/content/Intent;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 612
    const-string v0, "OPEN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 626
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 644
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    const-string v0, "tango.broadcast.show_conversation"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 630
    const-string v0, "conversationid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 631
    const-string v1, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Switching to Conversation detail page with conversation id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/TangoApp;->switchToConversationDetailIfPossible(Ljava/lang/String;)V

    goto :goto_0

    .line 633
    :cond_2
    const-string v0, "tango.broadcast.show_conversation_list"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 634
    const-string v0, "Tango.TabsUI"

    const-string v1, "Switching to Conversation List"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 635
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private onHelpMenuSelected()V
    .locals 3

    .prologue
    .line 794
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 795
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 796
    const/high16 v0, 0x1000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 797
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/TabActivityBase;->startActivity(Landroid/content/Intent;)V

    .line 798
    return-void
.end method

.method private onSettingsMenuSelected()V
    .locals 3

    .prologue
    .line 789
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>()V

    .line 790
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 791
    return-void
.end method

.method private setBadgeCount(Ljava/lang/String;IZ)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 830
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBadgeCount for tab="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 833
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabIndexByTag:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 834
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    .line 836
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 837
    :cond_0
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBadgeCount: could not find tab with tag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :goto_0
    return-void

    .line 841
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 842
    if-lt v0, v1, :cond_2

    .line 843
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setBadgeCount: tab position is invalid for tab="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 847
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 848
    const v1, 0x7f0a014b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 850
    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 851
    :goto_1
    invoke-static {v0, p2, v1, p0}, Lcom/sgiggle/production/Utils;->setBadgeCount(Landroid/widget/TextView;IZLandroid/content/Context;)V

    goto :goto_0

    .line 850
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private setBadgeToError(Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 860
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/sgiggle/production/TabActivityBase;->setBadgeCount(Ljava/lang/String;IZ)V

    .line 861
    return-void
.end method

.method private setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Landroid/view/View$OnTouchListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v0

    .line 274
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabIndexByTag:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p2, p4}, Lcom/sgiggle/production/TabActivityBase;->createTabView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    .line 277
    invoke-virtual {v0, p5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 278
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, p3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, p0, p1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/content/Intent;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    .line 280
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, v0}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 281
    return-void
.end method

.method private setupTabs()V
    .locals 13

    .prologue
    const/4 v5, 0x0

    .line 217
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabIndexByTag:Ljava/util/HashMap;

    .line 219
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 221
    const-class v1, Lcom/sgiggle/production/ContactListActivityStack;

    const v0, 0x7f090008

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "contacts"

    const v4, 0x7f0200b2

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/TabActivityBase;->setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V

    .line 227
    const-class v1, Lcom/sgiggle/production/CallLogActivityStack;

    const v0, 0x7f090009

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "call_log"

    const v4, 0x7f0200b1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/TabActivityBase;->setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V

    .line 233
    const-class v7, Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    const v0, 0x7f090124

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v9, "threaded_conversations"

    const v10, 0x7f0200b5

    new-instance v11, Lcom/sgiggle/production/TabActivityBase$1;

    invoke-direct {v11, p0}, Lcom/sgiggle/production/TabActivityBase$1;-><init>(Lcom/sgiggle/production/TabActivityBase;)V

    move-object v6, p0

    invoke-direct/range {v6 .. v11}, Lcom/sgiggle/production/TabActivityBase;->setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V

    .line 252
    const-class v1, Lcom/sgiggle/production/InviteSelectionActivity;

    const v0, 0x7f09000a

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "invite"

    const v4, 0x7f0200b3

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/TabActivityBase;->setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V

    .line 258
    const-class v1, Lcom/sgiggle/production/StoreActivity;

    const v0, 0x7f09000b

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "store"

    const v4, 0x7f0200b4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/production/TabActivityBase;->setupTab(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;ILandroid/view/View$OnTouchListener;)V

    .line 263
    return-void
.end method


# virtual methods
.method bringToForeground()V
    .locals 3

    .prologue
    .line 720
    const-string v0, "Tango.TabsUI"

    const-string v1, "bringToForeground()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    const-class v2, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 722
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 723
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->startActivity(Landroid/content/Intent;)V

    .line 724
    return-void
.end method

.method public getCurrentTabTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 303
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-nez p1, :cond_1

    .line 306
    const-string v0, "Tango.TabsUI"

    const-string v1, "handleNewMessage(): Message is null. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 534
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 526
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Unsupported message:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    :sswitch_0
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 314
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/ContactListActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    .line 315
    invoke-virtual {v0}, Lcom/sgiggle/production/ContactListActivity;->updateContacts()V

    goto :goto_0

    .line 320
    :sswitch_1
    const-string v0, "call_log"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivityStack;

    .line 322
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/CallLogActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity;

    .line 323
    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivity;->updateLogEntries()V

    goto :goto_0

    .line 328
    :sswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_contactSearchActivity:Lcom/sgiggle/production/ContactListActivity;

    if-eqz v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_contactSearchActivity:Lcom/sgiggle/production/ContactListActivity;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 331
    :cond_2
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 333
    invoke-virtual {v0}, Lcom/sgiggle/production/ContactListActivityStack;->getCurrentActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ActivityBase;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 338
    :sswitch_3
    const-string v0, "Tango.TabsUI"

    const-string v1, "LOGIN_COMPLETED_EVENT"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getDefault()Lcom/sgiggle/production/CTANotifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/CTANotifier;->publishCTA2NotificationBar(Lcom/sgiggle/messaging/Message;)V

    .line 340
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 341
    const-string v0, "contacts"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    .line 342
    :cond_3
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 343
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 344
    invoke-virtual {v0}, Lcom/sgiggle/production/ContactListActivityStack;->popToRoot()V

    .line 345
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/ContactListActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    .line 346
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 530
    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getLocalActivityManager()Landroid/app/LocalActivityManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/LocalActivityManager;->getActivity(Ljava/lang/String;)Landroid/app/Activity;

    move-result-object v0

    .line 531
    if-eqz v0, :cond_0

    .line 532
    invoke-virtual {v0}, Landroid/app/Activity;->closeOptionsMenu()V

    goto/16 :goto_0

    .line 351
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;

    .line 352
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 353
    new-instance v1, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 354
    invoke-virtual {v1, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->create(Ljava/lang/String;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 355
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 360
    :sswitch_5
    new-instance v0, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/SMSRateLimitDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 361
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 362
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_1

    .line 366
    :sswitch_6
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    if-eqz v0, :cond_5

    .line 367
    const-string v0, "call_log"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    .line 368
    iput-boolean v3, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 370
    :cond_5
    const-string v0, "call_log"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 371
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivityStack;

    .line 372
    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivityStack;->popToRoot()V

    .line 373
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/CallLogActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity;

    .line 374
    invoke-virtual {v0}, Lcom/sgiggle/production/CallLogActivity;->updateLogEntries()V

    goto :goto_1

    .line 379
    :sswitch_7
    const-string v0, "threaded_conversations"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    .line 380
    const-string v0, "threaded_conversations"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 381
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    .line 382
    if-eqz v0, :cond_4

    .line 383
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 389
    :sswitch_8
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    .line 392
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->hasLastUnreadMsgStatus()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getLastUnreadMsgStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v4

    .line 394
    :goto_2
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUpdateNotification()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getPlayAlertSound()Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUpdateNotification()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUnreadMessageCount()I

    move-result v2

    if-lez v2, :cond_9

    :cond_7
    move v2, v4

    .line 398
    :goto_3
    if-eqz v1, :cond_a

    .line 399
    const-string v0, "threaded_conversations"

    invoke-direct {p0, v0, v2}, Lcom/sgiggle/production/TabActivityBase;->setBadgeToError(Ljava/lang/String;Z)V

    goto/16 :goto_1

    :cond_8
    move v1, v3

    .line 392
    goto :goto_2

    :cond_9
    move v2, v3

    .line 394
    goto :goto_3

    .line 401
    :cond_a
    const-string v1, "threaded_conversations"

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;->getUnreadConversationCount()I

    move-result v0

    invoke-direct {p0, v1, v0, v2}, Lcom/sgiggle/production/TabActivityBase;->setBadgeCount(Ljava/lang/String;IZ)V

    goto/16 :goto_1

    .line 408
    :sswitch_9
    const-string v0, "threaded_conversations"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    .line 410
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 416
    :sswitch_a
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    if-eqz v0, :cond_b

    .line 417
    const-string v0, "invite"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    .line 418
    iput-boolean v3, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 420
    :cond_b
    const-string v0, "invite"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 421
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity;

    .line 422
    if-eqz v0, :cond_4

    .line 423
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 424
    iget v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->setRecommendedBadgeCount(I)V

    goto/16 :goto_1

    .line 435
    :sswitch_b
    const-string v0, "invite"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 436
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity;

    .line 437
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 438
    iget v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->setRecommendedBadgeCount(I)V

    goto/16 :goto_1

    .line 445
    :sswitch_c
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_smsCapable:Z

    if-eqz v0, :cond_4

    .line 449
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeInviteEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeInviteEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BadgeInviteCountPayload;

    .line 450
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BadgeInviteCountPayload;->getBadgeCount()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    .line 451
    const-string v0, "invite"

    iget v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    iget v2, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    if-lez v2, :cond_c

    move v2, v4

    :goto_4
    invoke-direct {p0, v0, v1, v2}, Lcom/sgiggle/production/TabActivityBase;->setBadgeCount(Ljava/lang/String;IZ)V

    .line 453
    const-string v0, "invite"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 454
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity;

    .line 455
    if-eqz v0, :cond_4

    .line 456
    iget v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_inviteBadgeCount:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/InviteSelectionActivity;->setRecommendedBadgeCount(I)V

    goto/16 :goto_1

    :cond_c
    move v2, v3

    .line 451
    goto :goto_4

    .line 463
    :sswitch_d
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getDefault()Lcom/sgiggle/production/CTANotifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier;->cancelValidationAndRegistrationAlerts()V

    .line 464
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 465
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 466
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/ContactListActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    .line 467
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 471
    :sswitch_e
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 473
    const-string v0, "Tango.TabsUI"

    const-string v1, "DISPLAY_CONTACT_DETAIL CONTACTS_TAB"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 475
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/ContactListActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivity;

    .line 476
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/ContactListActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 478
    :cond_d
    const-string v0, "call_log"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 479
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivityStack;

    .line 480
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/CallLogActivityStack;->getActivity(I)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/CallLogActivity;

    .line 481
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/CallLogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 486
    :sswitch_f
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    if-eqz v0, :cond_e

    .line 487
    const-string v0, "store"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->switchTabSafe(Ljava/lang/String;)Z

    .line 488
    iput-boolean v3, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 490
    :cond_e
    const-string v0, "store"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 491
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/StoreActivity;

    .line 492
    if-eqz v0, :cond_f

    .line 493
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/StoreActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 496
    :cond_f
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    .line 498
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getVgoodBadgeCount()I

    move-result v1

    if-lez v1, :cond_15

    .line 499
    add-int/lit8 v1, v3, 0x1

    .line 500
    :goto_5
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getGameBadgeCount()I

    move-result v2

    if-lez v2, :cond_10

    .line 501
    add-int/lit8 v1, v1, 0x1

    .line 502
    :cond_10
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getAvatarBadgeCount()I

    move-result v0

    if-lez v0, :cond_14

    .line 503
    add-int/lit8 v0, v1, 0x1

    .line 504
    :goto_6
    const-string v1, "store"

    invoke-direct {p0, v1, v0, v3}, Lcom/sgiggle/production/TabActivityBase;->setBadgeCount(Ljava/lang/String;IZ)V

    goto/16 :goto_1

    .line 509
    :sswitch_10
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->updateTangoAlertFooter()V

    goto/16 :goto_0

    .line 514
    :sswitch_11
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeStoreEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$BadgeStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;

    .line 516
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getVgoodBadgeCount()I

    move-result v1

    if-lez v1, :cond_13

    .line 517
    add-int/lit8 v1, v3, 0x1

    .line 518
    :goto_7
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getGameBadgeCount()I

    move-result v2

    if-lez v2, :cond_11

    .line 519
    add-int/lit8 v1, v1, 0x1

    .line 520
    :cond_11
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StoreBadgeCountPayload;->getAvatarBadgeCount()I

    move-result v0

    if-lez v0, :cond_12

    .line 521
    add-int/lit8 v0, v1, 0x1

    .line 522
    :goto_8
    const-string v1, "store"

    invoke-direct {p0, v1, v0, v4}, Lcom/sgiggle/production/TabActivityBase;->setBadgeCount(Ljava/lang/String;IZ)V

    goto/16 :goto_0

    :cond_12
    move v0, v1

    goto :goto_8

    :cond_13
    move v1, v3

    goto :goto_7

    :cond_14
    move v0, v1

    goto :goto_6

    :cond_15
    move v1, v3

    goto :goto_5

    .line 310
    :sswitch_data_0
    .sparse-switch
        0x88c3 -> :sswitch_3
        0x88dd -> :sswitch_0
        0x88de -> :sswitch_10
        0x88df -> :sswitch_a
        0x88e1 -> :sswitch_b
        0x88eb -> :sswitch_4
        0x88f1 -> :sswitch_b
        0x88f3 -> :sswitch_b
        0x88f4 -> :sswitch_b
        0x88f5 -> :sswitch_b
        0x8910 -> :sswitch_d
        0x8913 -> :sswitch_2
        0x8914 -> :sswitch_6
        0x8915 -> :sswitch_1
        0x892a -> :sswitch_5
        0x8990 -> :sswitch_e
        0x89c0 -> :sswitch_f
        0x89c1 -> :sswitch_11
        0x89c3 -> :sswitch_c
        0x89c8 -> :sswitch_7
        0x89c9 -> :sswitch_9
        0x89ca -> :sswitch_9
        0x89d3 -> :sswitch_8
        0x89d4 -> :sswitch_9
    .end sparse-switch
.end method

.method isForegroundActivity()Z
    .locals 1

    .prologue
    .line 727
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isForegroundActivity:Z

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 160
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreate(Landroid/os/Bundle;)V

    .line 163
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 166
    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v2, 0x10

    and-int/2addr v0, v2

    if-eqz v0, :cond_0

    .line 167
    const-string v0, "Tango.TabsUI"

    const-string v2, "onCreate(): The intent was re-launched from history."

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->startInitScreenIfResumeFromKilled()V

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    .line 174
    if-nez v0, :cond_1

    .line 175
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 176
    const-string v0, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate(): First time created: message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :goto_0
    const v0, 0x7f030056

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->setContentView(I)V

    .line 185
    const v0, 0x7f0a014d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    .line 187
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getTabHost()Landroid/widget/TabHost;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    .line 188
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->setupTabs()V

    .line 190
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setTabsActivityInstance(Lcom/sgiggle/production/TabActivityBase;)V

    .line 192
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    if-eqz v0, :cond_2

    .line 193
    iput-boolean v5, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 194
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 214
    :goto_1
    return-void

    .line 178
    :cond_1
    check-cast v0, Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;

    .line 179
    iget-object v0, v0, Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    .line 180
    const-string v0, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate(): Restore activity: message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 197
    :cond_2
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_3

    .line 199
    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/TabActivityBase;->handleNotificationBar(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_1

    .line 202
    :cond_3
    const-string v0, "missedJid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_4

    .line 204
    const-string v2, "Tango.TabsUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate(): missedJid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iput-boolean v5, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 206
    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/TabActivityBase;->handleMissedCallIntent(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_1

    .line 209
    :cond_4
    const-string v0, "Tango.TabsUI"

    const-string v1, "onCreate(): Query UI State Machine for the latest UI..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->requestServiceForLatestUI()V

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 770
    const/4 v0, 0x2

    const v1, 0x7f09010b

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a6

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 771
    const/4 v0, 0x3

    const v1, 0x7f090025

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0200a3

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 772
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 815
    const-string v0, "Tango.TabsUI"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    invoke-super {p0}, Landroid/app/TabActivity;->onDestroy()V

    .line 817
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setTabsActivityInstance(Lcom/sgiggle/production/TabActivityBase;)V

    .line 818
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 819
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 109
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNewIntent(intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const-string v0, "android.intent.action.SEARCH"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivityStack()Lcom/sgiggle/production/ActivityStack;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/ContactListActivityStack;

    .line 117
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 118
    const-class v2, Lcom/sgiggle/production/ContactListActivity;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 119
    const-string v2, "android.intent.action.SEARCH"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    const-string v2, "query"

    const-string v3, "query"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 121
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/production/ContactListActivityStack;->push(Ljava/lang/String;Landroid/content/Intent;)V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    const-string v0, "invite"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->getCurrentTabActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/InviteSelectionActivity;

    .line 125
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/InviteSelectionActivity;->onNewIntent(Landroid/content/Intent;)V

    goto :goto_0

    .line 131
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 132
    if-eqz v0, :cond_3

    .line 133
    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNotificationBar(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 136
    :cond_3
    const-string v0, "missedJid"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_4

    .line 138
    const-string v1, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent(): missedJid="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iput-boolean v4, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 140
    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/TabActivityBase;->handleMissedCallIntent(Ljava/lang/String;Landroid/content/Intent;)V

    goto :goto_0

    .line 145
    :cond_4
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 146
    if-nez v0, :cond_5

    .line 149
    const-string v0, "Tango.TabsUI"

    const-string v1, "onNewIntent(): Simply bring this Tabs activity to foreground."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 151
    :cond_5
    const-string v1, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNewIntent(): Message = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iput-boolean v4, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 153
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 777
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 785
    invoke-super {p0, p1}, Landroid/app/TabActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 779
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->onSettingsMenuSelected()V

    move v0, v1

    .line 780
    goto :goto_0

    .line 782
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/TabActivityBase;->onHelpMenuSelected()V

    move v0, v1

    .line 783
    goto :goto_0

    .line 777
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 802
    const-string v0, "Tango.TabsUI"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-super {p0}, Landroid/app/TabActivity;->onPause()V

    .line 804
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isForegroundActivity:Z

    .line 805
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 751
    const-string v0, "Tango.TabsUI"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    invoke-super {p0}, Landroid/app/TabActivity;->onResume()V

    .line 753
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_smsCapable:Z

    .line 754
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isForegroundActivity:Z

    .line 755
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 756
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 757
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 101
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRetainNonConfigurationInstance(): message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    new-instance v0, Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;-><init>(Lcom/sgiggle/production/TabActivityBase;Lcom/sgiggle/production/TabActivityBase$1;)V

    .line 103
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    iput-object v1, v0, Lcom/sgiggle/production/TabActivityBase$ActivityConfiguration;->firstMessage:Lcom/sgiggle/messaging/Message;

    .line 104
    return-object v0
.end method

.method onSearchModeEntered(Z)V
    .locals 3
    .parameter

    .prologue
    .line 731
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSearchModeEntered(): inSearch="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 732
    iput-boolean p1, p0, Lcom/sgiggle/production/TabActivityBase;->m_isInSearchMode:Z

    .line 733
    return-void
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 745
    const-string v0, "Tango.TabsUI"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    invoke-super {p0}, Landroid/app/TabActivity;->onStart()V

    .line 747
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 809
    const-string v0, "Tango.TabsUI"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    invoke-super {p0}, Landroid/app/TabActivity;->onStop()V

    .line 811
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 561
    const-string v0, "Tango.TabsUI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTabChanged(tabId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 564
    const/4 v0, 0x0

    .line 565
    const-string v1, "contacts"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 566
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ContactsDisplayMainMessage;-><init>()V

    .line 578
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->updateTangoAlertFooter()V

    .line 580
    if-eqz v0, :cond_1

    .line 581
    const-string v1, "Tango.TabsUI"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onTabChanged: Posting message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 584
    :cond_1
    return-void

    .line 567
    :cond_2
    const-string v1, "call_log"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568
    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/MissedCallNotifier;->dismissMissedCallNotification()V

    .line 569
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RequestCallLogMessage;-><init>()V

    goto :goto_0

    .line 570
    :cond_3
    const-string v1, "invite"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 571
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$InviteDisplayMainMessage;-><init>()V

    goto :goto_0

    .line 572
    :cond_4
    const-string v1, "threaded_conversations"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 573
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListMessage;-><init>()V

    goto :goto_0

    .line 574
    :cond_5
    const-string v1, "store"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 575
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreMessage;-><init>()V

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 761
    const-string v0, "Tango.TabsUI"

    const-string v1, "onUserLeaveHint()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    invoke-super {p0}, Landroid/app/TabActivity;->onUserLeaveHint()V

    .line 763
    iget-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isInSearchMode:Z

    if-nez v0, :cond_0

    .line 764
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 766
    :cond_0
    return-void
.end method

.method setContactSearchActivity(Lcom/sgiggle/production/ContactListActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 736
    iput-object p1, p0, Lcom/sgiggle/production/TabActivityBase;->m_contactSearchActivity:Lcom/sgiggle/production/ContactListActivity;

    .line 737
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_isInSearchMode:Z

    .line 738
    return-void
.end method

.method public setForceSwitchTab()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/TabActivityBase;->forceSwtichTab:Z

    .line 97
    return-void
.end method

.method switchTabSafe(Ljava/lang/String;)Z
    .locals 2
    .parameter

    .prologue
    .line 595
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 597
    :try_start_0
    const-string v0, "Tango.TabsUI"

    const-string v1, "switchTabSafe !!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    .line 599
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->updateTangoAlertFooter()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 603
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 605
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 603
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    throw v0
.end method

.method public updateTangoAlertFooter()V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 652
    const-string v0, "contacts"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "invite"

    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 654
    :cond_0
    const-string v0, "Tango.TabsUI"

    const-string v1, "updateTangoAlertFooter: hiding footer, handled by screen already or should not be shown."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 693
    :goto_0
    return-void

    .line 660
    :cond_1
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getDefault()Lcom/sgiggle/production/CTANotifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CTANotifier;->getLastCtaAlertList()Ljava/util/List;

    move-result-object v0

    .line 661
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    .line 662
    :cond_2
    const-string v0, "Tango.TabsUI"

    const-string v1, "updateTangoAlertFooter: hiding footer, no alert to show."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 668
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 669
    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 670
    const-string v1, "Tango.TabsUI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateTangoAlertFooter: updating footer, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " alerts to show."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    new-instance v3, Lcom/sgiggle/production/TabActivityBase$2;

    invoke-direct {v3, p0, v0}, Lcom/sgiggle/production/TabActivityBase$2;-><init>(Lcom/sgiggle/production/TabActivityBase;Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 680
    iget-object v1, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    const v3, 0x7f0a0005

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 683
    if-ne v2, v5, :cond_4

    .line 684
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 688
    :goto_1
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 689
    new-instance v0, Landroid/text/style/UnderlineSpan;

    invoke-direct {v0}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v2, v0, v6, v3, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 690
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 692
    iget-object v0, p0, Lcom/sgiggle/production/TabActivityBase;->m_tabFooter:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 686
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/TabActivityBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0008

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
