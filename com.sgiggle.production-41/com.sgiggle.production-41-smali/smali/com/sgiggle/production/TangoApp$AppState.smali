.class public final enum Lcom/sgiggle/production/TangoApp$AppState;
.super Ljava/lang/Enum;
.source "TangoApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/TangoApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AppState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/TangoApp$AppState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/TangoApp$AppState;

.field public static final enum APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

.field public static final enum APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

.field public static final enum APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

.field public static final enum APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 169
    new-instance v0, Lcom/sgiggle/production/TangoApp$AppState;

    const-string v1, "APP_STATE_INIT"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/TangoApp$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    .line 170
    new-instance v0, Lcom/sgiggle/production/TangoApp$AppState;

    const-string v1, "APP_STATE_BACKGROUND"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/TangoApp$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    .line 171
    new-instance v0, Lcom/sgiggle/production/TangoApp$AppState;

    const-string v1, "APP_STATE_RESUMING"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/TangoApp$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    .line 172
    new-instance v0, Lcom/sgiggle/production/TangoApp$AppState;

    const-string v1, "APP_STATE_FOREGROUND"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/TangoApp$AppState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    .line 168
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/production/TangoApp$AppState;->$VALUES:[Lcom/sgiggle/production/TangoApp$AppState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/TangoApp$AppState;
    .locals 1
    .parameter

    .prologue
    .line 168
    const-class v0, Lcom/sgiggle/production/TangoApp$AppState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp$AppState;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/TangoApp$AppState;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/sgiggle/production/TangoApp$AppState;->$VALUES:[Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0}, [Lcom/sgiggle/production/TangoApp$AppState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/TangoApp$AppState;

    return-object v0
.end method
