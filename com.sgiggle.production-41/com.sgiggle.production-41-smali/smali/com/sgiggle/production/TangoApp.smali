.class public Lcom/sgiggle/production/TangoApp;
.super Landroid/app/Application;
.source "TangoApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;,
        Lcom/sgiggle/production/TangoApp$MediaEngineMessageReceiver;,
        Lcom/sgiggle/production/TangoApp$AppState;
    }
.end annotation

.annotation runtime Lorg/acra/annotation/ReportsCrashes;
    formKey = ""
    formUri = ""
    logcatArguments = {
        "-t",
        "200",
        "-v",
        "threadtime"
    }
    mode = .enum Lorg/acra/ReportingInteractionMode;->SILENT:Lorg/acra/ReportingInteractionMode;
.end annotation


# static fields
.field private static final ACK_TIME_OUT_MS_FOR_DEBUG:J = 0x3e80L

.field private static final ACK_TIME_OUT_MS_FOR_RELEASE:J = 0x1f40L

.field public static final C2DM_SENDER_ID:Ljava/lang/String; = "tangodeveloper@gmail.com"

.field private static final C2DM_WAKE_LOCK_TIMEOUT:J = 0xea60L

.field private static final CAPABILITY_FEATURE_VIDEOMAIL:Ljava/lang/String; = "videomail"

.field private static final CONNECTIVITY_CHANGE_ACTION:Ljava/lang/String; = "android.net.conn.CONNECTIVITY_CHANGE"

.field private static final DBG:Z = true

.field static final DBG_LEVEL:I = 0x2

.field public static final EXTRA_APP_STATE:Ljava/lang/String; = "com.sgiggle.production.tangoapp.EXTRA_APP_STATE"

.field private static final KEY_INSTALLED:Ljava/lang/String; = "installed"

.field private static final MESSAGE_FROM_STATE_MACHINE:I = 0x1

.field private static final PREF_AVATAR_RESTORE_TRANSACTION:Ljava/lang/String; = "avatar_restore_transaction"

.field private static final PREF_FRONT_CAMERA_ROTATION_OPTION:Ljava/lang/String; = "front_camera_rotation"

.field private static final PREF_SCREEN_LOGGER_ENABLED:Ljava/lang/String; = "screen_logger_enabled"

.field private static final PREF_VGOOD_RESTORE_TRANSACTION:Ljava/lang/String; = "vgood_restore_transaction"

.field private static final SMS_RECEIVED_ACTION:Ljava/lang/String; = "android.provider.Telephony.SMS_RECEIVED"

.field private static final TAG:Ljava/lang/String; = "Tango.App"

.field private static final VDBG:Z = true

.field private static final VERIFICATION_SMS_WINDOW:I = 0x493e0

.field public static g_frontCameraRotation:I

.field public static g_screenLoggerEnabled:Z

.field private static m_facebook:Lcom/facebook/android/Facebook;

.field private static m_isInitialized:Z

.field private static m_prefs:Landroid/content/SharedPreferences;

.field private static m_upgradeable:Z

.field private static s_failedToLoadLibrary:Z

.field private static s_isInstallationOk:Z

.field private static s_me:Lcom/sgiggle/production/TangoApp;


# instance fields
.field private m_appState:Lcom/sgiggle/production/TangoApp$AppState;

.field private m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

.field private m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

.field private m_beforeBackgroundObservers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;",
            ">;"
        }
    .end annotation
.end field

.field private m_currActivty:Landroid/app/Activity;

.field private m_currentClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

.field private m_explicitLoginSent:Z

.field private m_handler:Landroid/os/Handler;

.field private m_hideUiAfterEndCall:Z

.field private m_htcLockScreenSupported:Z

.field private m_keyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private m_keyguardManager:Landroid/app/KeyguardManager;

.field private m_launchFromLockScreen:Z

.field private m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

.field private final m_messageReceiver:Lcom/sgiggle/messaging/MessageReceiver;

.field private m_networkReceiver:Lcom/sgiggle/network/Network;

.field private m_postCallActivity:Landroid/app/Activity;

.field private m_productPurchaseMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private m_recordVideomailActivity:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

.field private m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

.field private m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

.field private m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

.field private m_skipWelcomPageCounter:I

.field private m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

.field private m_snsActivity:Lcom/sgiggle/production/SnsComposerActivity;

.field private m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

.field private m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

.field private m_videoActivityPendingRequest:Lcom/sgiggle/messaging/Message;

.field private m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

.field private m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 148
    sput-boolean v1, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    .line 149
    const/4 v0, -0x1

    sput v0, Lcom/sgiggle/production/TangoApp;->g_frontCameraRotation:I

    .line 150
    sput-boolean v1, Lcom/sgiggle/production/TangoApp;->s_failedToLoadLibrary:Z

    .line 227
    sput-boolean v2, Lcom/sgiggle/production/TangoApp;->m_upgradeable:Z

    .line 251
    sput-boolean v2, Lcom/sgiggle/production/TangoApp;->s_isInstallationOk:Z

    .line 2294
    sput-boolean v1, Lcom/sgiggle/production/TangoApp;->m_isInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 256
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 165
    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 174
    sget-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 219
    new-instance v0, Lcom/sgiggle/production/TangoApp$MediaEngineMessageReceiver;

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/TangoApp$MediaEngineMessageReceiver;-><init>(Lcom/sgiggle/production/TangoApp;Lcom/sgiggle/production/TangoApp$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_messageReceiver:Lcom/sgiggle/messaging/MessageReceiver;

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_beforeBackgroundObservers:Ljava/util/List;

    .line 235
    new-instance v0, Lcom/sgiggle/production/TangoApp$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/TangoApp$1;-><init>(Lcom/sgiggle/production/TangoApp;)V

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_handler:Landroid/os/Handler;

    .line 247
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    .line 262
    new-instance v0, Lcom/sgiggle/production/LogCollector$ExceptionHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/LogCollector$ExceptionHandler;-><init>()V

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 257
    sput-object p0, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    .line 259
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/TangoApp;Lcom/sgiggle/messaging/Message;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->handleStateMachineMessage(Lcom/sgiggle/messaging/Message;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/TangoApp;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method public static areStoreTransactionRestored()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2168
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "areStoreTransactionRestored:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    const-string v3, "vgood_restore_transaction"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2169
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "vgood_restore_transaction"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private composeAndSendEmail()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2354
    const-string v0, "Tango.App"

    const-string v1, "ComposerAndSendEmail()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 2357
    invoke-static {}, Lcom/sgiggle/util/LogReporter;->getLogEmail()Ljava/lang/String;

    move-result-object v0

    .line 2367
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Thank you for helping Tango collect this information!  We will investigate the logs and get back to you as soon as possible with our conclusions.\nPlease hit \'Send\' to forward the logs to our team."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2370
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2371
    const-string v3, "message/rfc822"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2401
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 2402
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "content://"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tangocontentprovider/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/sgiggle/util/LogReporter;->outFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2405
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2407
    const-string v3, "android.intent.extra.EMAIL"

    new-array v4, v6, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2408
    const-string v0, "android.intent.extra.SUBJECT"

    const-string v3, "Sending Tango log file"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2409
    const-string v0, "android.intent.extra.TEXT"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2411
    invoke-virtual {v2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2412
    const/high16 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2413
    const/high16 v0, 0x1000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2417
    :try_start_0
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/TangoApp;->startActivity(Landroid/content/Intent;)V

    .line 2418
    const-string v0, "Tango.App"

    const-string v1, "Invites have been sent out."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2422
    :goto_0
    return-void

    .line 2419
    :catch_0
    move-exception v0

    .line 2420
    const-string v0, "Tango.App"

    const-string v1, "Not activity was found for ACTION_SEND (for sending Invites)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private dismissEmailVerificationScreen()V
    .locals 2

    .prologue
    .line 2092
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/EmailVerificationActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2093
    const-string v0, "Tango.App"

    const-string v1, "dismissEmailVerificationScreen(): Close the existing Email verification activity..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2094
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/EmailVerificationActivity;->finish()V

    .line 2096
    :cond_0
    return-void
.end method

.method private dismissInCallScreens()V
    .locals 2

    .prologue
    .line 2058
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v0, :cond_0

    .line 2059
    const-string v0, "Tango.App"

    const-string v1, "dismissInCallScreens(): Close the existing Video activity..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2060
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 2063
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v0, :cond_1

    .line 2064
    const-string v0, "Tango.App"

    const-string v1, "dismissInCallScreens(): Close the existing Audio activity..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2065
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    .line 2067
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    if-ne v0, v1, :cond_3

    .line 2068
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 2070
    :cond_3
    return-void
.end method

.method private dismissRegisterScreen()V
    .locals 2

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    if-eqz v0, :cond_0

    .line 2077
    const-string v0, "Tango.App"

    const-string v1, "dismissRegisterScreen(): Close the existing Register activity..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2078
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/RegisterUserActivity;->finish()V

    .line 2080
    :cond_0
    return-void
.end method

.method private dismissSettingsActivity()V
    .locals 1

    .prologue
    .line 2083
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2084
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/SettingsActivity;->finish()V

    .line 2086
    :cond_0
    return-void
.end method

.method public static ensureInitialized()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sgiggle/production/WrongTangoRuntimeVersionException;
        }
    .end annotation

    .prologue
    const/high16 v4, 0x7f09

    const/4 v3, 0x1

    .line 2312
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->m_isInitialized:Z

    if-nez v0, :cond_0

    .line 2313
    const-string v0, "Tango.App"

    const-string v1, "Initialization..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2315
    sget-object v0, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/iphelper/IpHelper;->updateContext(Landroid/content/Context;)V

    .line 2316
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->loadNativeLibrary()V

    .line 2317
    invoke-static {v3}, Lcom/sgiggle/util/Log;->setUseTangoLogs(Z)V

    .line 2319
    sget-object v0, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-direct {v0}, Lcom/sgiggle/production/TangoApp;->setup()V

    .line 2320
    new-instance v0, Lcom/facebook/android/Facebook;

    sget-object v1, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/android/Facebook;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sgiggle/production/TangoApp;->m_facebook:Lcom/facebook/android/Facebook;

    .line 2321
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FB_APP_ID is:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2322
    const-string v0, "Tango.App"

    const-string v1, "Initialization done."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2323
    sput-boolean v3, Lcom/sgiggle/production/TangoApp;->m_isInitialized:Z

    .line 2325
    :cond_0
    return-void
.end method

.method private failedToLoadNativeLibrary()V
    .locals 6

    .prologue
    .line 291
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f0200ab

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 294
    const/4 v1, 0x0

    invoke-static {p0}, Lcom/sgiggle/production/TangoApp;->getOpenLowMemoryTipsPageIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x1000

    invoke-static {p0, v1, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 296
    sget-object v2, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090093

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090094

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 299
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 300
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getNotificationManager()Landroid/app/NotificationManager;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 302
    const-string v0, "Tango.App"

    const-string v1, "send notification and kill the app"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    .line 304
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/TangoApp;
    .locals 4

    .prologue
    .line 381
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 387
    :goto_0
    sget-object v0, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    return-object v0

    .line 382
    :catch_0
    move-exception v0

    .line 384
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getNotificationManager()Landroid/app/NotificationManager;
    .locals 2

    .prologue
    .line 277
    sget-object v0, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    return-object v0
.end method

.method public static getOpenLowMemoryTipsPageIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .parameter

    .prologue
    .line 271
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://www.tango.me/android/lowmem"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 272
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 273
    return-object v0
.end method

.method private handleCalleeMissedCallEvent(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 2127
    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/MissedCallNotifier;->saveMissedCallMessage(Lcom/sgiggle/messaging/Message;)Z

    .line 2128
    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/MissedCallNotifier;->notifyMissedCallInStatusBar()V

    .line 2129
    return-void
.end method

.method private handleHTCLockScreen(Lcom/sgiggle/production/CallSession;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 2274
    new-instance v0, Lcom/htc/lockscreen/telephony/PhoneState;

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/htc/lockscreen/telephony/PhoneState;-><init>(Landroid/content/Context;)V

    .line 2275
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/htc/lockscreen/telephony/PhoneState;->setDisplayNumber(Ljava/lang/String;)V

    .line 2276
    if-eqz p1, :cond_0

    .line 2277
    iget-object v1, p1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/htc/lockscreen/telephony/PhoneState;->setName(Ljava/lang/String;)V

    .line 2278
    invoke-virtual {p1}, Lcom/sgiggle/production/CallSession;->getPeerPhoto()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/htc/lockscreen/telephony/PhoneState;->setPhoto(Landroid/graphics/Bitmap;)V

    .line 2280
    :cond_0
    invoke-virtual {v0, v4}, Lcom/htc/lockscreen/telephony/PhoneState;->setCallState(I)V

    .line 2281
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".service.HTCPhoneCallService"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/htc/lockscreen/telephony/PhoneState;->setPhoneComponent(Ljava/lang/String;Ljava/lang/String;)V

    .line 2284
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/htc/lockscreen/idlescreen/phonecall/IdlePhoneCallService;->startPhoneUI(Landroid/content/Context;Lcom/htc/lockscreen/telephony/PhoneState;)V

    .line 2288
    iput-boolean v4, p0, Lcom/sgiggle/production/TangoApp;->m_launchFromLockScreen:Z

    .line 2289
    return-void
.end method

.method private handleRegistrationChange(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 1498
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->readRegistrationFlag(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1511
    :cond_0
    :goto_0
    return-void

    .line 1501
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    const v1, 0x88c3

    if-ne v0, v1, :cond_0

    .line 1506
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;

    .line 1507
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->hasRegistrationSubmitted()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginCompletedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$LoginCompletedPayload;->getRegistrationSubmitted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1509
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->startWakeupAlarmService()V

    goto :goto_0
.end method

.method private handleServerPurchaseResponse(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 1488
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;

    .line 1489
    const-string v0, "Tango.App"

    const-string v1, "Received Report Purchase Result Event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1490
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getRecorded()Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload$RecordType;

    if-ne v0, v1, :cond_0

    .line 1491
    const-string v0, "Tango.App"

    const-string v1, "Update purchase state=true"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1492
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/TangoApp;->updatePurchaseState(Ljava/lang/String;Z)V

    .line 1494
    :cond_0
    return-void
.end method

.method private handleStateMachineMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 408
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->handleStateMachineMessageWithAckFlagReturn(Lcom/sgiggle/messaging/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->sendMessageAck()V

    .line 410
    :cond_0
    return-void
.end method

.method private handleStateMachineMessageWithAckFlagReturn(Lcom/sgiggle/messaging/Message;)Z
    .locals 10
    .parameter

    .prologue
    const v9, 0x88cb

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x400

    const/4 v5, 0x0

    .line 413
    .line 414
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->isInstallationOk()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v8

    .line 1477
    :cond_0
    :goto_0
    return v1

    .line 417
    :cond_1
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    .line 420
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v2

    .line 422
    const v3, 0x8910

    if-ne v1, v3, :cond_4

    .line 423
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationResultEvent;

    move-object v1, v0

    .line 424
    const-string v3, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Validation-result = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload;->getResult()Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    move-result-object v1

    .line 426
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;->ACCEPTED:Lcom/sgiggle/xmpp/SessionMessages$ValidationResultPayload$Result;

    if-ne v1, v2, :cond_3

    .line 427
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->cancelSMSReceiverTimer()V

    .line 428
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->unregisterSMSReceiver()V

    .line 430
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_2

    .line 431
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 433
    :cond_2
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 434
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/SettingsActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    :cond_3
    move v1, v8

    .line 437
    goto/16 :goto_0

    .line 440
    :cond_4
    const v3, 0x8924

    if-ne v1, v3, :cond_8

    .line 441
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;

    .line 442
    const-string v2, ""

    .line 443
    const-string v3, ""

    .line 444
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasTitle()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 445
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getTitle()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 446
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasMessage()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 447
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getMessage()Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 449
    :cond_6
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 450
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->hasActionClass()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClass()Ljava/lang/String;

    move-result-object v1

    const-string v5, "offer-call"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 452
    const-string v5, "actiontype"

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getActionClass()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v5, "firstname"

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getFirstname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v5, "lastname"

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getLastname()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    const-string v5, "accountid"

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$NotificationMessagePayload;->getAccountid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_7
    invoke-static {}, Lcom/sgiggle/production/PushMsgNotifier;->getDefault()Lcom/sgiggle/production/PushMsgNotifier;

    move-result-object v1

    invoke-virtual {v1, v2, v3, v4}, Lcom/sgiggle/production/PushMsgNotifier;->notifyPushMessageInStatusBar(Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    move v1, v8

    .line 458
    goto/16 :goto_0

    .line 461
    :cond_8
    const v3, 0x892e

    if-ne v1, v3, :cond_9

    .line 463
    const-string v2, "Tango.App"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Ignore the state machine event: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 464
    goto/16 :goto_0

    .line 467
    :cond_9
    const v3, 0x88dd

    if-ne v1, v3, :cond_f

    .line 468
    invoke-static {p1, p0}, Lcom/sgiggle/production/ContactListActivity;->storeContacts(Lcom/sgiggle/messaging/Message;Landroid/content/Context;)V

    .line 476
    :cond_a
    :goto_1
    const v3, 0x88c3

    if-eq v1, v3, :cond_b

    const v3, 0x8990

    if-eq v1, v3, :cond_b

    const v3, 0x8914

    if-ne v1, v3, :cond_d

    .line 478
    :cond_b
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissInCallScreens()V

    .line 479
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissRegisterScreen()V

    .line 480
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissEmailVerificationScreen()V

    .line 481
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissSettingsActivity()V

    .line 482
    if-eqz v2, :cond_c

    .line 483
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 484
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v2

    .line 486
    :cond_c
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->handleRegistrationChange(Lcom/sgiggle/messaging/Message;)V

    :cond_d
    move-object v3, v2

    .line 491
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v4, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    if-eq v2, v4, :cond_e

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v4, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v2, v4, :cond_11

    .line 493
    :cond_e
    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->shouldMessageBeIgnoredInBackground(I)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 494
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): App is in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 495
    goto/16 :goto_0

    .line 469
    :cond_f
    const v3, 0x8914

    if-eq v1, v3, :cond_10

    const v3, 0x8915

    if-ne v1, v3, :cond_a

    .line 470
    :cond_10
    invoke-static {p1, p0}, Lcom/sgiggle/production/CallLogActivity;->storeLogEntries(Lcom/sgiggle/messaging/Message;Landroid/content/Context;)V

    goto :goto_1

    .line 498
    :cond_11
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v4, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v2, v4, :cond_12

    const v2, 0x8929

    if-ne v1, v2, :cond_12

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-nez v2, :cond_12

    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sgiggle/production/MissedCallNotifier;->isLastEventEqualToMessage(Lcom/sgiggle/messaging/Message;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 502
    const-string v1, "Tango.App"

    const-string v2, "handleStateMachineMessage(): Replay the last missed-call event..."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    invoke-direct {p0, v1, v7}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 504
    goto/16 :goto_0

    .line 508
    :cond_12
    invoke-static {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailMessageHandler;->handleMessage(Landroid/content/Context;Lcom/sgiggle/messaging/Message;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 509
    const-string v1, "Tango.App"

    const-string v2, "handleStateMachineMessage(): Message handled by VideomailMessageHandler."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 510
    goto/16 :goto_0

    .line 514
    :cond_13
    sparse-switch v1, :sswitch_data_0

    :cond_14
    :goto_2
    move v1, v8

    goto/16 :goto_0

    .line 524
    :sswitch_0
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_15

    .line 525
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 531
    :cond_15
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_16

    .line 532
    const-string v1, "Tango.App"

    const-string v2, "Cleaning ConversationDetailActivity instance."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 533
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->finish()V

    .line 534
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->clearRunningInstance()V

    .line 537
    :cond_16
    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 540
    goto/16 :goto_0

    .line 543
    :sswitch_1
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_14

    .line 544
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 549
    :sswitch_2
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RetrieveOfflineMessageResultEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$RetrieveOfflineMessageResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatusPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatusPayload;->getStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    move-result-object v1

    invoke-static {v1}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setConnectionStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)V

    .line 550
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 551
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 552
    :cond_17
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_18

    .line 554
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 556
    :cond_18
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event RETRIEVE_OFFLINE_MESSAGE_RESULT_EVENT."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 558
    goto/16 :goto_0

    .line 563
    :sswitch_3
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_19

    .line 564
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 567
    :cond_19
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationMessageNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;

    invoke-static {v2, v1}, Lcom/sgiggle/production/PushMsgNotifier;->updateConversationMessageNotificationInStatusBar(Landroid/content/Context;Lcom/sgiggle/xmpp/SessionMessages$UpdateConversationMessageNotificationPayload;)V

    move v1, v8

    .line 570
    goto/16 :goto_0

    .line 573
    :sswitch_4
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissRegisterScreen()V

    .line 574
    const-class v1, Lcom/sgiggle/production/SettingsActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 575
    goto/16 :goto_0

    .line 578
    :sswitch_5
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getDefault()Lcom/sgiggle/production/CTANotifier;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CTANotifier;->saveLastCtaAlertList(Lcom/sgiggle/messaging/Message;)V

    .line 579
    invoke-static {}, Lcom/sgiggle/production/CTANotifier;->getDefault()Lcom/sgiggle/production/CTANotifier;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CTANotifier;->publishCTA2NotificationBar(Lcom/sgiggle/messaging/Message;)V

    .line 580
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_1a

    .line 581
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 583
    :cond_1a
    invoke-static {p1}, Lcom/sgiggle/production/SettingsActivity;->setMessage4resume(Lcom/sgiggle/messaging/Message;)V

    .line 584
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 585
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/SettingsActivity;->applyPostponedEvent()V

    move v1, v8

    goto/16 :goto_0

    .line 591
    :sswitch_6
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    if-eqz v1, :cond_1b

    .line 592
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/SettingsActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 600
    :cond_1b
    :sswitch_7
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_14

    .line 601
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 605
    :sswitch_8
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_1c

    .line 606
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 615
    :cond_1c
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not find m_tabsActivity when handling(message="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), recreate it"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 616
    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 619
    goto/16 :goto_0

    .line 622
    :sswitch_9
    const-class v1, Lcom/sgiggle/production/OAuthRequestActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 623
    goto/16 :goto_0

    .line 625
    :sswitch_a
    const-class v1, Lcom/sgiggle/production/SnsComposerActivity;

    const/high16 v2, 0x2

    invoke-virtual {p0, v1, p1, v2}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 626
    goto/16 :goto_0

    .line 630
    :sswitch_b
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_snsActivity:Lcom/sgiggle/production/SnsComposerActivity;

    if-eqz v1, :cond_14

    .line 631
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_snsActivity:Lcom/sgiggle/production/SnsComposerActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/SnsComposerActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 635
    :sswitch_c
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;

    move-object v1, v0

    .line 637
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    .line 638
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    if-eqz v2, :cond_1d

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    if-ne v1, v2, :cond_1d

    .line 639
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 640
    :cond_1d
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 642
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 644
    :cond_1e
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event FORWARD_VIDEO_MAIL_FINISHED_EVENT. Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 646
    goto/16 :goto_0

    .line 649
    :sswitch_d
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    if-eqz v1, :cond_1f

    .line 650
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 651
    :cond_1f
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v1

    if-eqz v1, :cond_20

    .line 652
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 653
    :cond_20
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_21

    .line 655
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 657
    :cond_21
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event START_SMS_COMPOSE_EVENT. Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 659
    goto/16 :goto_0

    .line 662
    :sswitch_e
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v1, :cond_22

    .line 663
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 665
    :cond_22
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event DISPLAY_VIDEO_MAIL_RECEIVERS_EVENT.  Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 667
    goto/16 :goto_0

    .line 670
    :sswitch_f
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v1, :cond_23

    .line 671
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 673
    :cond_23
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event DISPLAY_VIDEO_MAIL_NON_TANGO_NOTIFICATION_EVENT. Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 675
    goto/16 :goto_0

    .line 678
    :sswitch_10
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v1, :cond_24

    .line 679
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 681
    :cond_24
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event UPLOAD_VIDEO_MAIL_FINISHED_EVENT. Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 683
    goto/16 :goto_0

    .line 689
    :sswitch_11
    invoke-static {}, Lcom/sgiggle/production/SelectContactActivity;->getRunningInstance()Lcom/sgiggle/production/SelectContactActivity;

    move-result-object v1

    if-eqz v1, :cond_25

    .line 690
    invoke-static {}, Lcom/sgiggle/production/SelectContactActivity;->getRunningInstance()Lcom/sgiggle/production/SelectContactActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/SelectContactActivity;->finish()V

    .line 694
    :cond_25
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    .line 695
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasOpenConversationContext()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->FAILED_OUTGOING_CALL:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    if-ne v1, v2, :cond_26

    .line 697
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissInCallScreens()V

    .line 698
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 699
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    .line 702
    :cond_26
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_27

    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_27

    .line 705
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 706
    const-class v1, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    invoke-virtual {p0, v1, v7, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v5

    goto/16 :goto_0

    .line 708
    :cond_27
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->isAppOnForeground()Z

    move-result v1

    if-eqz v1, :cond_14

    .line 710
    const-class v1, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v5

    goto/16 :goto_0

    .line 715
    :sswitch_12
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 717
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 722
    :sswitch_13
    const-class v1, Lcom/sgiggle/production/SelectContactActivity;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 723
    goto/16 :goto_0

    .line 726
    :sswitch_14
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_28

    .line 728
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    .line 733
    :goto_3
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 734
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_0

    .line 730
    :cond_28
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event for Conversation Detail, Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto :goto_3

    .line 748
    :sswitch_15
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_29

    .line 750
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 752
    :cond_29
    const-string v1, "Tango.App"

    const-string v2, "Ignoring event for Conversation Detail, Logic error! Contact a developer."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 754
    goto/16 :goto_0

    .line 759
    :sswitch_16
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_2b

    .line 761
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    move v2, v5

    .line 766
    :goto_4
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v3

    if-eqz v3, :cond_2a

    .line 767
    invoke-static {}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 770
    :cond_2a
    if-nez v1, :cond_7d

    .line 771
    const-string v1, "Tango.App"

    const-string v3, "DISPLAY_CONVERSATION_MESSAGE_EVENT not handled"

    invoke-static {v1, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto/16 :goto_0

    :cond_2b
    move v1, v5

    move v2, v5

    .line 764
    goto :goto_4

    .line 778
    :sswitch_17
    invoke-static {}, Lcom/sgiggle/production/SelectContactActivity;->getRunningInstance()Lcom/sgiggle/production/SelectContactActivity;

    move-result-object v1

    if-eqz v1, :cond_2c

    .line 779
    invoke-static {}, Lcom/sgiggle/production/SelectContactActivity;->getRunningInstance()Lcom/sgiggle/production/SelectContactActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/SelectContactActivity;->finish()V

    .line 781
    :cond_2c
    const-class v1, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    .line 783
    goto/16 :goto_0

    .line 790
    :sswitch_18
    const-class v1, Lcom/sgiggle/production/RegisterUserActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 791
    goto/16 :goto_0

    .line 795
    :sswitch_19
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    if-eqz v1, :cond_14

    .line 796
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/RegisterUserActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 802
    :sswitch_1a
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    if-eqz v1, :cond_2d

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/EmailVerificationActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_2d

    .line 803
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/EmailVerificationActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 805
    :cond_2d
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    if-eqz v1, :cond_2e

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/RegisterUserActivity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_2e

    .line 806
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/RegisterUserActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 808
    :cond_2e
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 809
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/SettingsActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 815
    :sswitch_1b
    const-class v1, Lcom/sgiggle/production/ValidationFailedDialogActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 817
    goto/16 :goto_0

    .line 823
    :sswitch_1c
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    if-eqz v1, :cond_2f

    .line 824
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/EmailVerificationActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 827
    :cond_2f
    const-class v1, Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 829
    goto/16 :goto_0

    .line 832
    :sswitch_1d
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySupportWebsiteEvent;

    .line 833
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySupportWebsiteEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$WebNavigationPayload;

    .line 834
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$WebNavigationPayload;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 835
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 836
    const/high16 v1, 0x1000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 837
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/TangoApp;->startActivity(Landroid/content/Intent;)V

    move v1, v8

    .line 838
    goto/16 :goto_0

    .line 842
    :sswitch_1e
    invoke-static {}, Lcom/sgiggle/production/SettingsActivity;->getRunningInstance()Lcom/sgiggle/production/SettingsActivity;

    move-result-object v1

    .line 843
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v2, :cond_30

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v2}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v2

    if-eqz v2, :cond_30

    .line 844
    invoke-static {}, Lcom/sgiggle/production/AccountVerifier;->getDefault()Lcom/sgiggle/production/AccountVerifier;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1, v2}, Lcom/sgiggle/production/AccountVerifier;->handleNewMessage(Lcom/sgiggle/messaging/Message;Landroid/app/Activity;)V

    move v1, v8

    goto/16 :goto_0

    .line 845
    :cond_30
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    if-eqz v2, :cond_31

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v2}, Lcom/sgiggle/production/EmailVerificationActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_31

    .line 846
    invoke-static {}, Lcom/sgiggle/production/AccountVerifier;->getDefault()Lcom/sgiggle/production/AccountVerifier;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    invoke-virtual {v1, p1, v2}, Lcom/sgiggle/production/AccountVerifier;->handleNewMessage(Lcom/sgiggle/messaging/Message;Landroid/app/Activity;)V

    move v1, v8

    goto/16 :goto_0

    .line 847
    :cond_31
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    if-eqz v2, :cond_32

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v2}, Lcom/sgiggle/production/RegisterUserActivity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_32

    .line 848
    invoke-static {}, Lcom/sgiggle/production/AccountVerifier;->getDefault()Lcom/sgiggle/production/AccountVerifier;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    invoke-virtual {v1, p1, v2}, Lcom/sgiggle/production/AccountVerifier;->handleNewMessage(Lcom/sgiggle/messaging/Message;Landroid/app/Activity;)V

    move v1, v8

    goto/16 :goto_0

    .line 849
    :cond_32
    if-eqz v1, :cond_14

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_14

    .line 850
    invoke-static {}, Lcom/sgiggle/production/AccountVerifier;->getDefault()Lcom/sgiggle/production/AccountVerifier;

    move-result-object v2

    invoke-virtual {v2, p1, v1}, Lcom/sgiggle/production/AccountVerifier;->handleNewMessage(Lcom/sgiggle/messaging/Message;Landroid/app/Activity;)V

    move v1, v8

    goto/16 :goto_0

    .line 855
    :sswitch_1f
    const-class v1, Lcom/sgiggle/production/UpdateRequiredActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 856
    goto/16 :goto_0

    .line 859
    :sswitch_20
    const-class v1, Lcom/sgiggle/production/AppLoginErrorActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 860
    goto/16 :goto_0

    .line 867
    :sswitch_21
    iput-boolean v5, p0, Lcom/sgiggle/production/TangoApp;->m_launchFromLockScreen:Z

    .line 868
    invoke-static {}, Lcom/sgiggle/production/MissedCallNotifier;->getDefault()Lcom/sgiggle/production/MissedCallNotifier;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/MissedCallNotifier;->cancelLastAlert()V

    .line 869
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    if-eq v1, v2, :cond_33

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_38

    :cond_33
    move v1, v8

    :goto_5
    iput-boolean v1, p0, Lcom/sgiggle/production/TangoApp;->m_hideUiAfterEndCall:Z

    .line 871
    iget-boolean v1, p0, Lcom/sgiggle/production/TangoApp;->m_hideUiAfterEndCall:Z

    if-eqz v1, :cond_34

    .line 872
    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 874
    :cond_34
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_35

    .line 876
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingIgnoredCallAlert()V

    .line 877
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingRecordVideoMessageAlert()V

    .line 878
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingCallErrorAlert()V

    .line 879
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->dismissPendingCallOnHoldAlert()V

    .line 881
    :cond_35
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->dismissInCallScreens()V

    .line 885
    :sswitch_22
    if-nez v3, :cond_7c

    .line 886
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->startCallSession(Lcom/sgiggle/messaging/Message;)Lcom/sgiggle/production/CallSession;

    move-result-object v1

    .line 890
    :goto_6
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    .line 893
    iget-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    if-eqz v3, :cond_36

    .line 894
    iget-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    .line 895
    iput-object v7, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    .line 898
    :cond_36
    iget-boolean v3, p0, Lcom/sgiggle/production/TangoApp;->m_htcLockScreenSupported:Z

    if-eqz v3, :cond_37

    if-eqz v2, :cond_37

    .line 899
    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->handleHTCLockScreen(Lcom/sgiggle/production/CallSession;)V

    .line 902
    :cond_37
    iget-boolean v1, v1, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_3a

    .line 903
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-nez v1, :cond_39

    .line 904
    invoke-virtual {p0, p1, v6}, Lcom/sgiggle/production/TangoApp;->startVideoActivity(Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    goto/16 :goto_0

    :cond_38
    move v1, v5

    .line 869
    goto :goto_5

    .line 906
    :cond_39
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 909
    :cond_3a
    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 911
    goto/16 :goto_0

    .line 914
    :sswitch_23
    if-nez v3, :cond_3b

    .line 915
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$MissedCallEvent;

    .line 917
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v2

    const-string v3, "jingle"

    new-instance v4, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MissedCallEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getAccountId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 920
    goto/16 :goto_0

    .line 921
    :cond_3b
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_3c

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_3c

    .line 922
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 928
    :goto_7
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 929
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move v1, v8

    .line 931
    goto/16 :goto_0

    .line 923
    :cond_3c
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_3d

    .line 924
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_7

    .line 926
    :cond_3d
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not handled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 934
    :sswitch_24
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->handleCalleeMissedCallEvent(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 935
    goto/16 :goto_0

    .line 939
    :sswitch_25
    if-nez v3, :cond_3f

    .line 940
    if-ne v1, v9, :cond_3e

    .line 941
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_14

    .line 942
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 946
    :cond_3e
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 947
    goto/16 :goto_0

    .line 949
    :cond_3f
    iget-boolean v2, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    .line 950
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 951
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 952
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    .line 953
    iget-boolean v3, p0, Lcom/sgiggle/production/TangoApp;->m_hideUiAfterEndCall:Z

    if-eqz v3, :cond_40

    .line 954
    iput-boolean v5, p0, Lcom/sgiggle/production/TangoApp;->m_hideUiAfterEndCall:Z

    .line 955
    sget-object v3, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    iput-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 959
    :cond_40
    iget-boolean v3, p0, Lcom/sgiggle/production/TangoApp;->m_htcLockScreenSupported:Z

    if-eqz v3, :cond_41

    .line 960
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/htc/lockscreen/idlescreen/phonecall/IdlePhoneCallService;->endPhoneUI(Landroid/content/Context;)V

    .line 963
    :cond_41
    iget-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v3, :cond_42

    if-eqz v2, :cond_42

    .line 964
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 965
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_14

    .line 966
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    move v1, v8

    goto/16 :goto_0

    .line 968
    :cond_42
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v2, :cond_43

    .line 969
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 970
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 971
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    move v1, v8

    goto/16 :goto_0

    .line 973
    :cond_43
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    if-eqz v2, :cond_44

    if-ne v1, v9, :cond_44

    .line 976
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " handle call error in ViewVideomailActivity"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 977
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 978
    :cond_44
    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v2, :cond_45

    if-ne v1, v9, :cond_45

    .line 986
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " hanlde call error outside of call page"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    .line 988
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-static {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    move v1, v8

    .line 989
    goto/16 :goto_0

    .line 990
    :cond_45
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not handled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 992
    goto/16 :goto_0

    .line 994
    :sswitch_26
    if-nez v3, :cond_46

    .line 995
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 996
    goto/16 :goto_0

    .line 998
    :cond_46
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 999
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "here again "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v3, :cond_47

    move v4, v8

    :goto_8
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_48

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-nez v1, :cond_48

    .line 1004
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    :cond_47
    move v4, v5

    .line 999
    goto :goto_8

    .line 1005
    :cond_48
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_49

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_49

    .line 1007
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1009
    :cond_49
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not handled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 1011
    goto/16 :goto_0

    .line 1013
    :sswitch_27
    if-nez v3, :cond_4a

    .line 1014
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 1015
    goto/16 :goto_0

    .line 1018
    :cond_4a
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;

    move-object v1, v0

    .line 1019
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodSupport()Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioInInitializationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getVgoodsPurchased()Z

    move-result v1

    invoke-static {v2, v1}, Lcom/sgiggle/cafe/vgood/CafeMgr;->initVGoodStatus(Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;Z)V

    .line 1020
    const-string v1, "VGOOD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "-- X -- support:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v4, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", purchased:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v4, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodPurchased:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1023
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_4b

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-nez v1, :cond_4b

    .line 1024
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 1027
    :cond_4b
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_4c

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_4c

    .line 1028
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1029
    :cond_4c
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_4d

    .line 1030
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1032
    :cond_4d
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not handled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 1034
    goto/16 :goto_0

    .line 1037
    :sswitch_28
    if-nez v3, :cond_4e

    .line 1038
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1040
    :cond_4e
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1041
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v3, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 1042
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_4f

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-nez v1, :cond_4f

    .line 1043
    const-string v1, "Tango.App"

    const-string v2, "handleStatMachineMessage(): AUDIO_IN_PROGRESS_EVENT - video screen dismissed going back to audio screen"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1048
    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1049
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 1051
    :cond_4f
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    if-eqz v1, :cond_50

    .line 1052
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/GameInCallActivity;->finish()V

    .line 1054
    :cond_50
    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-nez v1, :cond_53

    .line 1055
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_51

    .line 1056
    const-string v1, "Tango.App"

    const-string v2, "handleStatMachineMessage(): AUDIO_IN_PROGRESS_EVENT - AudioInProgressActivity handle message"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1057
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1059
    :cond_51
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_52

    .line 1061
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): App is in background. won\'t open activity for message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1063
    :cond_52
    const-string v1, "Tango.App"

    const-string v2, "handleStatMachineMessage(): AUDIO_IN_PROGRESS_EVENT - create AudioInProgressActivity"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1067
    :cond_53
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_54

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_54

    .line 1068
    const-string v1, "Tango.App"

    const-string v2, "handleStatMachineMessage(): AUDIO_IN_PROGRESS_EVENT - video activity handle message"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1071
    :cond_54
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not handled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 1074
    goto/16 :goto_0

    .line 1078
    :sswitch_29
    if-nez v3, :cond_55

    .line 1079
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1082
    :cond_55
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1083
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    if-eqz v1, :cond_56

    .line 1084
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/GameInCallActivity;->finish()V

    .line 1086
    :cond_56
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_57

    .line 1087
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): App is in background. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1091
    :cond_57
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-nez v1, :cond_58

    .line 1092
    const-string v1, "Tango.App"

    const-string v2, "handleStateMachineMessage(): calling startVideoActivity() in response to video event"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1093
    invoke-virtual {p0, p1, v5}, Lcom/sgiggle/production/TangoApp;->startVideoActivity(Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    goto/16 :goto_0

    .line 1094
    :cond_58
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_59

    .line 1095
    const-string v1, "Tango.App"

    const-string v2, "handleStateMachineMessage(): current video activity is finishing, postponing startVideoActivity()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivityPendingRequest:Lcom/sgiggle/messaging/Message;

    move v1, v5

    .line 1097
    goto/16 :goto_0

    .line 1099
    :cond_59
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1102
    goto/16 :goto_0

    .line 1110
    :sswitch_2a
    if-nez v3, :cond_5a

    .line 1111
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1113
    :cond_5a
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1114
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5b

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_5b

    .line 1116
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 1118
    :cond_5b
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1119
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GameInCallActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1125
    :sswitch_2b
    invoke-static {}, Lcom/sgiggle/production/VGoodDemo;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    if-eqz v1, :cond_5c

    .line 1126
    invoke-static {}, Lcom/sgiggle/production/VGoodDemo;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 1128
    :cond_5c
    invoke-static {}, Lcom/sgiggle/production/GameDemoActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    if-eqz v1, :cond_5d

    .line 1129
    invoke-static {}, Lcom/sgiggle/production/GameDemoActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 1131
    :cond_5d
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1133
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v5

    goto/16 :goto_0

    .line 1139
    :sswitch_2c
    if-eqz v3, :cond_5e

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-eq v1, v2, :cond_5e

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_5e

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_5e

    iget-object v1, v3, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v1, v2, :cond_5f

    .line 1144
    :cond_5e
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): Ignore Display Animation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1145
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteMessage;

    invoke-direct {v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodAnimationCompleteMessage;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1148
    :cond_5f
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1149
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1151
    goto/16 :goto_0

    .line 1156
    :sswitch_2d
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1157
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_60

    .line 1158
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): App is in background. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1162
    :cond_60
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_61

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_61

    .line 1163
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1165
    :cond_61
    invoke-virtual {p0, p1, v5}, Lcom/sgiggle/production/TangoApp;->startVideoActivity(Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 1167
    goto/16 :goto_0

    .line 1170
    :sswitch_2e
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;

    move-object v1, v0

    .line 1171
    const-string v3, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleStateMachineMessage(): AVATAR_RENDER_REQUEST_TYPE: demo = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDemo()Z

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1172
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDemo()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 1173
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

    if-eqz v1, :cond_14

    .line 1174
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarDemo;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1176
    :cond_62
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 1177
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1184
    :sswitch_2f
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): Display demo avatar: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1185
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

    if-eqz v1, :cond_14

    .line 1186
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarDemo;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1191
    :sswitch_30
    if-nez v3, :cond_63

    .line 1192
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleStateMachineMessage(): No call session. Ignore: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    .line 1193
    goto/16 :goto_0

    .line 1195
    :cond_63
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1197
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    const-class v2, Lcom/sgiggle/production/AudioInProgressActivity;

    if-ne v1, v2, :cond_64

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_64

    .line 1199
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->onAudioModeUpdated()V

    move v1, v8

    goto/16 :goto_0

    .line 1201
    :cond_64
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 1203
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onAudioModeUpdated()V

    move v1, v8

    goto/16 :goto_0

    :sswitch_31
    move v1, v8

    .line 1209
    goto/16 :goto_0

    .line 1212
    :sswitch_32
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->composeAndSendEmail()V

    move v1, v8

    .line 1213
    goto/16 :goto_0

    .line 1216
    :sswitch_33
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

    if-eqz v1, :cond_14

    .line 1217
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

    invoke-interface {v1, p1}, Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;->onUploadTokenReceived(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1222
    :sswitch_34
    if-eqz v3, :cond_67

    .line 1223
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "QUERY_LEAVE_MESSAGE_EVENT m_audioActivity:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " callSession.m_callerInitVideoCall:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v4, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " m_videoActivity:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1224
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_66

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-nez v1, :cond_66

    .line 1225
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 1229
    :cond_65
    :goto_9
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->endCallSession()V

    .line 1246
    :goto_a
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move v1, v8

    .line 1247
    goto/16 :goto_0

    .line 1226
    :cond_66
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_65

    iget-boolean v1, v3, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    if-eqz v1, :cond_65

    .line 1227
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_9

    .line 1232
    :cond_67
    const-string v1, "Tango.App"

    const-string v2, "QUERY_LEAVE_MESSAGE_EVENT callsession is NULL"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1233
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_68

    .line 1234
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AudioInProgressActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_a

    .line 1236
    :cond_68
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_69

    .line 1237
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_a

    .line 1240
    :cond_69
    const-string v1, "Tango.App"

    const-string v2, "QUERY_LEAVE_MESSAGE_EVENT Wrong state: both m_audioActivity and m_videoActivity are NULL !!!"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageResultMessage;

    invoke-direct {v3, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageResultMessage;-><init>(Z)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_a

    .line 1250
    :sswitch_35
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_recordVideomailActivity:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    if-eqz v1, :cond_6a

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_recordVideomailActivity:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isReviewInProgress()Z

    move-result v1

    if-eqz v1, :cond_6a

    .line 1252
    const-string v1, "Tango.App"

    const-string v2, "Ignoring RECORD event while in Review, going back to current activity."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v8

    goto/16 :goto_0

    .line 1254
    :cond_6a
    const-class v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1256
    goto/16 :goto_0

    .line 1260
    :sswitch_36
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    if-nez v1, :cond_6b

    .line 1261
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->clearOldPlaybackData()V

    .line 1262
    const-class v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1268
    :cond_6b
    const-string v1, "Tango.App"

    const-string v2, "handleStateMachineMessage(): Activity already exists, re-starting it to call onNewIntent()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1269
    const-class v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1271
    goto/16 :goto_0

    .line 1274
    :sswitch_37
    invoke-static {p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->startPlayback(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1275
    goto/16 :goto_0

    .line 1279
    :sswitch_38
    invoke-static {p1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->updateProductCatalog(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1280
    goto/16 :goto_0

    .line 1284
    :sswitch_39
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_6c

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v1

    if-eqz v1, :cond_6c

    .line 1285
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/TabActivityBase;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1287
    :cond_6c
    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 1289
    goto/16 :goto_0

    .line 1292
    :sswitch_3a
    invoke-static {}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    if-nez v1, :cond_6d

    .line 1293
    const-class v1, Lcom/sgiggle/production/VGoodsSubscriptionActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1295
    :cond_6d
    invoke-static {}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1297
    goto/16 :goto_0

    .line 1301
    :sswitch_3b
    invoke-static {}, Lcom/sgiggle/production/VGoodDemo;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    if-nez v1, :cond_6e

    .line 1302
    const-class v1, Lcom/sgiggle/production/VGoodDemo;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1304
    :cond_6e
    invoke-static {}, Lcom/sgiggle/production/VGoodDemo;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1306
    goto/16 :goto_0

    .line 1310
    :sswitch_3c
    invoke-static {}, Lcom/sgiggle/production/GameCatalogActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    if-nez v1, :cond_6f

    .line 1311
    const-class v1, Lcom/sgiggle/production/GameCatalogActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1313
    :cond_6f
    invoke-static {}, Lcom/sgiggle/production/GameCatalogActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1315
    goto/16 :goto_0

    .line 1321
    :sswitch_3d
    invoke-static {}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/AvatarSubscriptionActivity;

    move-result-object v1

    if-nez v1, :cond_70

    .line 1322
    const-string v1, "Tango.App"

    const-string v2, "DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE: startExplicitActivity"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1323
    const-class v1, Lcom/sgiggle/production/AvatarSubscriptionActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1325
    :cond_70
    const-string v1, "Tango.App"

    const-string v2, "DISPLAY_AVATAR_PRODUCT_CATALOG_TYPE: AvatarSubscriptionActivity.getRunningInstance().handleNewMessage"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1326
    invoke-static {}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/AvatarSubscriptionActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1328
    goto/16 :goto_0

    .line 1332
    :sswitch_3e
    const-class v1, Lcom/sgiggle/production/AvatarDemo;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1333
    goto/16 :goto_0

    .line 1337
    :sswitch_3f
    invoke-static {}, Lcom/sgiggle/production/GameDemoActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    if-nez v1, :cond_71

    .line 1338
    const-class v1, Lcom/sgiggle/production/GameDemoActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1340
    :cond_71
    invoke-static {}, Lcom/sgiggle/production/GameDemoActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductDemoActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1342
    goto/16 :goto_0

    .line 1346
    :sswitch_40
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/CallHandler;->updateCallSession(Lcom/sgiggle/messaging/Message;)V

    .line 1347
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v3, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 1348
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_72

    .line 1349
    iput-object v7, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1350
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onGameInCallStart()V

    .line 1351
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 1352
    iput-object v7, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    .line 1355
    :cond_72
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-eq v1, v2, :cond_14

    .line 1356
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    if-nez v1, :cond_73

    .line 1357
    const-class v1, Lcom/sgiggle/production/GameInCallActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1359
    :cond_73
    invoke-static {}, Lcom/sgiggle/production/GameInCallActivity;->getRunningInstance()Lcom/sgiggle/production/GameInCallActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GameInCallActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1366
    :sswitch_41
    const-class v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1367
    goto/16 :goto_0

    .line 1370
    :sswitch_42
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->handleServerPurchaseResponse(Lcom/sgiggle/messaging/Message;)V

    .line 1371
    invoke-static {}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    if-eqz v1, :cond_74

    .line 1372
    invoke-static {}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/GenericProductCatalogActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 1375
    :cond_74
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_14

    .line 1376
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sgiggle/production/AvatarDemo;

    if-eqz v1, :cond_75

    .line 1377
    const-string v1, "Tango.App"

    const-string v2, "AvatarDemo handles the REPORT_PURCHASE_RESULT_EVENT"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1378
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/AvatarDemo;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarDemo;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1380
    :cond_75
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/sgiggle/production/AvatarSubscriptionActivity;

    if-eqz v1, :cond_14

    .line 1381
    const-string v1, "Tango.App"

    const-string v2, "AvatarSubscriptionActivity handles the REPORT_PURCHASE_RESULT_EVENT"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getCurrentActivityInstance()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/AvatarSubscriptionActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1387
    :sswitch_43
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_76

    .line 1388
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->finish()V

    .line 1389
    :cond_76
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;

    move-object v1, v0

    .line 1391
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getContentType()Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    move-result-object v2

    if-ne v3, v2, :cond_77

    .line 1392
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    if-nez v1, :cond_14

    .line 1393
    const-class v1, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1398
    :cond_77
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_VGOOD:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getContentType()Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    move-result-object v2

    if-ne v3, v2, :cond_79

    .line 1399
    const-string v3, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "upsell pid:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/payments/BillingServiceManager;->isBillingSupported()Z

    move-result v2

    if-eqz v2, :cond_78

    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v2

    iget-object v2, v2, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayPostCallScreenEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentPayload;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_79

    .line 1403
    :cond_78
    const-string v1, "BillingService"

    const-string v2, "Not showing postcall UI. Either billing not supported or this product is purchased"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1404
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;

    invoke-direct {v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1406
    goto/16 :goto_0

    .line 1409
    :cond_79
    const-class v1, Lcom/sgiggle/production/PostCallActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1410
    goto/16 :goto_0

    .line 1413
    :sswitch_44
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

    if-eqz v1, :cond_14

    .line 1414
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

    invoke-interface {v1, p1}, Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;->onVideomailErrorEvent(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1418
    :sswitch_45
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    if-eqz v1, :cond_7a

    .line 1419
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sgiggle/production/PostCallActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/PostCallActivity;->displayAppStoreDetailPage()V

    move v1, v8

    goto/16 :goto_0

    .line 1421
    :cond_7a
    const-string v1, "Tango.App"

    const-string v2, "DISPLAY_APPSTORE_LIKE_UI_TYPE received but post call activity not found, start app store here"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    const-string v3, "market://details?id=com.sgiggle.production"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1423
    const/high16 v2, 0x1000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1424
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/TangoApp;->startActivity(Landroid/content/Intent;)V

    .line 1425
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelAppStoreMessage;

    invoke-direct {v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelAppStoreMessage;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1428
    goto/16 :goto_0

    .line 1433
    :sswitch_46
    const-class v1, Lcom/sgiggle/production/FacebookLikePageActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1434
    goto/16 :goto_0

    .line 1436
    :sswitch_47
    const-class v1, Lcom/sgiggle/production/ContactDetailActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1437
    goto/16 :goto_0

    .line 1441
    :sswitch_48
    invoke-static {}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/AvatarSubscriptionActivity;

    move-result-object v1

    if-nez v1, :cond_7b

    .line 1442
    const-class v1, Lcom/sgiggle/production/AvatarSubscriptionActivity;

    invoke-direct {p0, v1, p1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1444
    :cond_7b
    invoke-static {}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->getRunningInstance()Lcom/sgiggle/production/AvatarSubscriptionActivity;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/AvatarSubscriptionActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    .line 1446
    goto/16 :goto_0

    .line 1448
    :sswitch_49
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    if-eqz v1, :cond_14

    .line 1449
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/AudioInProgressActivity;->stopVideoRingback()V

    move v1, v8

    goto/16 :goto_0

    .line 1453
    :sswitch_4a
    const-class v1, Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-virtual {p0, v1, p1, v6}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    move v1, v8

    .line 1454
    goto/16 :goto_0

    .line 1458
    :sswitch_4b
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 1459
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1464
    :sswitch_4c
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 1465
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    move v1, v8

    goto/16 :goto_0

    .line 1470
    :sswitch_4d
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v1, :cond_14

    .line 1471
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_2

    :cond_7c
    move-object v1, v3

    goto/16 :goto_6

    :cond_7d
    move v1, v2

    goto/16 :goto_0

    .line 514
    nop

    :sswitch_data_0
    .sparse-switch
        0x125 -> :sswitch_15
        0x88b9 -> :sswitch_20
        0x88c3 -> :sswitch_0
        0x88c7 -> :sswitch_22
        0x88c9 -> :sswitch_21
        0x88cb -> :sswitch_25
        0x88cd -> :sswitch_26
        0x88cf -> :sswitch_28
        0x88d1 -> :sswitch_29
        0x88d3 -> :sswitch_18
        0x88d7 -> :sswitch_18
        0x88d9 -> :sswitch_2c
        0x88dc -> :sswitch_32
        0x88dd -> :sswitch_7
        0x88de -> :sswitch_5
        0x88df -> :sswitch_0
        0x88e1 -> :sswitch_0
        0x88e9 -> :sswitch_4
        0x88eb -> :sswitch_1a
        0x88ec -> :sswitch_1c
        0x88ee -> :sswitch_1c
        0x88f0 -> :sswitch_1c
        0x88f1 -> :sswitch_0
        0x88f3 -> :sswitch_0
        0x88f4 -> :sswitch_0
        0x88f5 -> :sswitch_0
        0x88f9 -> :sswitch_1b
        0x88fb -> :sswitch_6
        0x88fd -> :sswitch_29
        0x88ff -> :sswitch_27
        0x8901 -> :sswitch_23
        0x8903 -> :sswitch_1f
        0x8905 -> :sswitch_2a
        0x8907 -> :sswitch_2a
        0x8908 -> :sswitch_2a
        0x890b -> :sswitch_30
        0x890d -> :sswitch_25
        0x890f -> :sswitch_19
        0x8911 -> :sswitch_2a
        0x8912 -> :sswitch_19
        0x8913 -> :sswitch_7
        0x8914 -> :sswitch_0
        0x8915 -> :sswitch_7
        0x891c -> :sswitch_1e
        0x891d -> :sswitch_1e
        0x8928 -> :sswitch_31
        0x8929 -> :sswitch_24
        0x892a -> :sswitch_1a
        0x892c -> :sswitch_6
        0x892f -> :sswitch_1c
        0x8934 -> :sswitch_1d
        0x8953 -> :sswitch_44
        0x8959 -> :sswitch_e
        0x895a -> :sswitch_f
        0x895e -> :sswitch_35
        0x8962 -> :sswitch_33
        0x8965 -> :sswitch_10
        0x896c -> :sswitch_36
        0x896d -> :sswitch_37
        0x8973 -> :sswitch_2a
        0x8977 -> :sswitch_43
        0x8979 -> :sswitch_4a
        0x897a -> :sswitch_2a
        0x8980 -> :sswitch_42
        0x8981 -> :sswitch_41
        0x8982 -> :sswitch_38
        0x898e -> :sswitch_45
        0x898f -> :sswitch_46
        0x8990 -> :sswitch_47
        0x8991 -> :sswitch_a
        0x8992 -> :sswitch_b
        0x8993 -> :sswitch_9
        0x8994 -> :sswitch_b
        0x8995 -> :sswitch_b
        0x8996 -> :sswitch_49
        0x8997 -> :sswitch_3a
        0x8998 -> :sswitch_3b
        0x8999 -> :sswitch_4b
        0x899a -> :sswitch_4b
        0x899d -> :sswitch_2b
        0x89aa -> :sswitch_2d
        0x89ab -> :sswitch_2d
        0x89ac -> :sswitch_2d
        0x89ad -> :sswitch_2e
        0x89ae -> :sswitch_3d
        0x89af -> :sswitch_3e
        0x89b0 -> :sswitch_2f
        0x89b1 -> :sswitch_48
        0x89b2 -> :sswitch_4c
        0x89b3 -> :sswitch_4d
        0x89c0 -> :sswitch_39
        0x89c1 -> :sswitch_7
        0x89c3 -> :sswitch_1
        0x89c6 -> :sswitch_11
        0x89c7 -> :sswitch_16
        0x89c8 -> :sswitch_0
        0x89c9 -> :sswitch_8
        0x89ca -> :sswitch_7
        0x89cb -> :sswitch_36
        0x89cc -> :sswitch_14
        0x89cd -> :sswitch_15
        0x89ce -> :sswitch_15
        0x89cf -> :sswitch_34
        0x89d0 -> :sswitch_c
        0x89d1 -> :sswitch_d
        0x89d2 -> :sswitch_13
        0x89d3 -> :sswitch_3
        0x89d4 -> :sswitch_2
        0x89d6 -> :sswitch_15
        0x89d8 -> :sswitch_12
        0x89da -> :sswitch_15
        0x89db -> :sswitch_15
        0x89dc -> :sswitch_15
        0x89de -> :sswitch_15
        0x89df -> :sswitch_15
        0x89e0 -> :sswitch_15
        0x89e1 -> :sswitch_17
        0x89e4 -> :sswitch_3c
        0x89e5 -> :sswitch_3f
        0x89e6 -> :sswitch_40
        0x89f8 -> :sswitch_18
        0x89f9 -> :sswitch_15
    .end sparse-switch
.end method

.method private initializeUIStateMachine()V
    .locals 4

    .prologue
    .line 1787
    const-string v0, "Tango.App"

    const-string v1, "initializeUIStateMachine()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1788
    invoke-static {p0}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->updateContext(Landroid/content/Context;)V

    .line 1789
    const v0, 0x7f060004

    const v1, 0x7f060003

    const v2, 0x7f060002

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/pjmedia/SoundEffWrapper;->setRingResID(IIII)V

    .line 1791
    invoke-static {p0}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->updateContext(Landroid/content/Context;)V

    .line 1793
    invoke-static {p0}, Lcom/sgiggle/pjmedia/AudioWebRTC;->updateContext(Landroid/content/Context;)V

    .line 1794
    invoke-static {p0}, Lcom/sgiggle/contacts/ContactStore;->updateContext(Landroid/content/Context;)V

    .line 1795
    invoke-static {p0}, Lcom/sgiggle/nativecalllog/NativeCallLogStore;->updateContext(Landroid/content/Context;)V

    .line 1796
    invoke-static {p0}, Lcom/sgiggle/network/Network;->updateContext(Landroid/content/Context;)V

    .line 1797
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/VSoftCodec;->updateContext(Landroid/content/Context;)V

    .line 1798
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/OpenmaxBinder;->updateContext(Landroid/content/Context;)V

    .line 1799
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->updateContext(Landroid/content/Context;)V

    .line 1800
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/VideoView;->updateContext(Landroid/content/Context;)V

    .line 1801
    invoke-static {p0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->updateContext(Landroid/content/Context;)V

    .line 1802
    invoke-static {p0}, Lcom/sgiggle/GLES20/GLRenderer;->updateContext(Landroid/content/Context;)V

    .line 1803
    invoke-static {p0}, Lcom/sgiggle/screen/ScreenManager;->updateContext(Landroid/content/Context;)V

    .line 1804
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->initialize()V

    .line 1805
    invoke-static {}, Lcom/sgiggle/messaging/MessageFactoryRegistry;->getInstance()Lcom/sgiggle/messaging/MessageFactoryRegistry;

    move-result-object v0

    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$MediaEngineMessageFactory;

    invoke-direct {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MediaEngineMessageFactory;-><init>()V

    invoke-virtual {v0, v1}, Lcom/sgiggle/messaging/MessageFactoryRegistry;->registerFactory(Lcom/sgiggle/messaging/MessageFactory;)I

    .line 1808
    invoke-static {}, Lcom/sgiggle/media_engine/ClientInit;->start()V

    .line 1810
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "ui"

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_messageReceiver:Lcom/sgiggle/messaging/MessageReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->registerReceiver(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V

    .line 1811
    return-void
.end method

.method public static isAccountUpgradeable()Z
    .locals 1

    .prologue
    .line 2260
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->m_upgradeable:Z

    return v0
.end method

.method private isAppOnForeground()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2476
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2477
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 2479
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 2480
    if-nez v0, :cond_0

    move v0, v4

    .line 2496
    :goto_0
    return v0

    .line 2481
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 2491
    iget-object v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v3, 0x64

    if-ne v0, v3, :cond_1

    .line 2493
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v4

    .line 2496
    goto :goto_0
.end method

.method public static isInitialized()Z
    .locals 1

    .prologue
    .line 2297
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->m_isInitialized:Z

    return v0
.end method

.method public static isInstallationOk()Z
    .locals 1

    .prologue
    .line 2461
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->s_isInstallationOk:Z

    return v0
.end method

.method private isVideoActivityClass(Ljava/lang/Class;)Z
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2049
    const-class v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    if-eq p1, v0, :cond_0

    const-class v0, Lcom/sgiggle/production/VideoTwoWayGLActivity;

    if-ne p1, v0, :cond_1

    .line 2050
    :cond_0
    const/4 v0, 0x1

    .line 2051
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static loadNativeLibrary()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sgiggle/production/WrongTangoRuntimeVersionException;
        }
    .end annotation

    .prologue
    .line 2329
    :try_start_0
    const-string v0, "Tango"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 2331
    invoke-static {}, Lcom/sgiggle/corefacade/coremanagement/CoreInfo;->getVersionString()Ljava/lang/String;

    move-result-object v0

    .line 2333
    sget-object v1, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 2334
    sget-object v2, Lcom/sgiggle/production/TangoApp;->s_me:Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 2336
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    .line 2339
    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2340
    new-instance v3, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;

    invoke-direct {v3, v1, v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_1

    .line 2342
    :catch_0
    move-exception v1

    .line 2343
    :try_start_2
    const-string v3, "Tango.App"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Package name "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 2344
    new-instance v2, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;

    const-string v3, "unknown"

    invoke-direct {v2, v3, v0, v1}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_2 .. :try_end_2} :catch_1

    .line 2346
    :catch_1
    move-exception v0

    .line 2347
    const-string v1, "Tango.App"

    invoke-virtual {v0}, Ljava/lang/UnsatisfiedLinkError;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2348
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sgiggle/production/TangoApp;->s_failedToLoadLibrary:Z

    .line 2349
    new-instance v1, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;

    const-string v2, "unknown"

    const-string v3, "unknown"

    invoke-direct {v1, v2, v3, v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2351
    :cond_0
    return-void
.end method

.method private notifyServiceOfBackgroundState()V
    .locals 3

    .prologue
    .line 2149
    const-string v0, "Tango.App"

    const-string v1, "notifyServiceOfBackgroundState()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->unregisterScreenLockReceiver()V

    .line 2152
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInBackgroundMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInBackgroundMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 2154
    return-void
.end method

.method private onCallActivityDestroyed()V
    .locals 2

    .prologue
    .line 1991
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    .line 1992
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v0}, Lcom/sgiggle/production/TabActivityBase;->isForegroundActivity()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1993
    const-string v0, "Tango.App"

    const-string v1, "AudioInProgressActivity got destroyed, but should not, asking for latest UI."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1994
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->requestServiceForLatestUI()V

    .line 1996
    :cond_0
    return-void
.end method

.method private registerLockScreenReceiver()V
    .locals 2

    .prologue
    .line 2500
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2501
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2503
    new-instance v1, Lcom/sgiggle/production/LockScreenReceiver;

    invoke-direct {v1}, Lcom/sgiggle/production/LockScreenReceiver;-><init>()V

    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

    .line 2504
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/TangoApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2505
    return-void
.end method

.method private registerNetworkReceiver()V
    .locals 2

    .prologue
    .line 1853
    const-string v0, "Tango.App"

    const-string v1, "registerNetworkReceiver()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1854
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1855
    new-instance v1, Lcom/sgiggle/network/Network;

    invoke-direct {v1}, Lcom/sgiggle/network/Network;-><init>()V

    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_networkReceiver:Lcom/sgiggle/network/Network;

    .line 1856
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_networkReceiver:Lcom/sgiggle/network/Network;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/TangoApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1857
    return-void
.end method

.method private static restoreFrontCameraRotationOption()V
    .locals 3

    .prologue
    .line 2180
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "front_camera_rotation"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sgiggle/production/TangoApp;->g_frontCameraRotation:I

    .line 2181
    sget v0, Lcom/sgiggle/production/TangoApp;->g_frontCameraRotation:I

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setUserFrontCamRotation(I)V

    .line 2182
    return-void
.end method

.method private static restoreScreenLoggerOption()V
    .locals 3

    .prologue
    .line 2157
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "screen_logger_enabled"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    .line 2158
    return-void
.end method

.method public static saveFrontCameraRotationOption(I)V
    .locals 2
    .parameter

    .prologue
    .line 2186
    sput p0, Lcom/sgiggle/production/TangoApp;->g_frontCameraRotation:I

    .line 2187
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2188
    const-string v1, "front_camera_rotation"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2189
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2190
    sget v0, Lcom/sgiggle/production/TangoApp;->g_frontCameraRotation:I

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setUserFrontCamRotation(I)V

    .line 2191
    return-void
.end method

.method public static saveScreenLoggerOption(Z)V
    .locals 2
    .parameter

    .prologue
    .line 2161
    sput-boolean p0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    .line 2162
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2163
    const-string v1, "screen_logger_enabled"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2164
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2165
    return-void
.end method

.method public static setAccountUpradeable(Z)V
    .locals 0
    .parameter

    .prologue
    .line 2264
    sput-boolean p0, Lcom/sgiggle/production/TangoApp;->m_upgradeable:Z

    .line 2265
    return-void
.end method

.method private static setInstallationStatus(Z)V
    .locals 0
    .parameter

    .prologue
    .line 2457
    sput-boolean p0, Lcom/sgiggle/production/TangoApp;->s_isInstallationOk:Z

    .line 2458
    return-void
.end method

.method public static setStoreTransactionRestored()V
    .locals 3

    .prologue
    .line 2173
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2174
    const-string v1, "vgood_restore_transaction"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 2175
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2176
    const-string v0, "Tango.App"

    const-string v1, "setStoreTransactionRestored"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2177
    return-void
.end method

.method private setup()V
    .locals 4

    .prologue
    .line 307
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->s_failedToLoadLibrary:Z

    if-eqz v0, :cond_0

    .line 308
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->failedToLoadNativeLibrary()V

    .line 374
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-static {p0}, Lcom/sgiggle/localstorage/LocalStorage;->updateContext(Landroid/content/Context;)Z

    move-result v0

    .line 316
    invoke-static {v0}, Lcom/sgiggle/production/TangoApp;->setInstallationStatus(Z)V

    .line 318
    invoke-static {}, Lcom/sgiggle/util/ClientCrashReporter;->getInstance()Lcom/sgiggle/util/ClientCrashReporter;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/util/ClientCrashReporter;->init(Landroid/app/Application;)V

    .line 320
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MODEL=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' PRODUCT=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' MANUFACTURER=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' DEVICE="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    invoke-static {p0}, Lcom/sgiggle/production/MessageManager;->init(Lcom/sgiggle/production/TangoApp;)V

    .line 322
    invoke-static {p0}, Lcom/sgiggle/production/CallHandler;->init(Lcom/sgiggle/production/TangoApp;)V

    .line 323
    invoke-static {p0}, Lcom/sgiggle/production/MissedCallNotifier;->init(Lcom/sgiggle/production/TangoApp;)V

    .line 324
    invoke-static {p0}, Lcom/sgiggle/production/CTANotifier;->init(Landroid/content/Context;)V

    .line 325
    invoke-static {p0}, Lcom/sgiggle/production/PushMsgNotifier;->init(Lcom/sgiggle/production/TangoApp;)V

    .line 326
    invoke-static {p0}, Lcom/sgiggle/production/AccountVerifier;->init(Lcom/sgiggle/production/TangoApp;)V

    .line 328
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->initializeUIStateMachine()V

    .line 330
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->startEmptyService()V

    .line 331
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->registerNetworkReceiver()V

    .line 332
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->registerLockScreenReceiver()V

    .line 334
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->supportsHTCLockScreen()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/TangoApp;->m_htcLockScreenSupported:Z

    .line 336
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardManager:Landroid/app/KeyguardManager;

    .line 337
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardManager:Landroid/app/KeyguardManager;

    const-string v1, "Tango.App"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 339
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isDeviceCapableOfC2dm(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    const-string v0, "Tango.App"

    const-string v1, "onCreate: register for C2DM..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const-string v0, "tangodeveloper@gmail.com"

    invoke-static {p0, v0}, Lcom/google/android/c2dm/C2DMessaging;->register(Landroid/content/Context;Ljava/lang/String;)V

    .line 349
    :goto_1
    invoke-static {p0}, Lcom/google/android/c2dm/C2DMessaging;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 350
    if-eqz v0, :cond_1

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 351
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate: Send C2DM registrationId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 357
    :cond_1
    const-string v0, "Tango.App"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/TangoApp;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/TangoApp;->m_prefs:Landroid/content/SharedPreferences;

    .line 358
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->restoreScreenLoggerOption()V

    .line 359
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->restoreFrontCameraRotationOption()V

    .line 362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_productPurchaseMap:Ljava/util/HashMap;

    .line 364
    new-instance v0, Lcom/sgiggle/production/payments/ResponseHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/payments/ResponseHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    .line 366
    const-class v0, Lcom/sgiggle/production/LogCollector$TangoAppData;

    invoke-static {v0}, Lcom/sgiggle/util/LogReporter;->setAppDataProvider(Ljava/lang/Class;)V

    .line 367
    invoke-static {}, Lcom/sgiggle/util/LogReporter;->restore()Z

    .line 370
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-wide/16 v1, 0x3e80

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->setAckTimeout(J)V

    goto/16 :goto_0

    .line 345
    :cond_2
    const-string v0, "Tango.App"

    const-string v1, "onCreate: Incapable of C2DM"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private shouldMessageBeIgnoredInBackground(I)Z
    .locals 1
    .parameter

    .prologue
    .line 2105
    sparse-switch p1, :sswitch_data_0

    .line 2120
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2117
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2105
    nop

    :sswitch_data_0
    .sparse-switch
        0x88c3 -> :sswitch_0
        0x88cf -> :sswitch_0
        0x88d1 -> :sswitch_0
        0x88d7 -> :sswitch_0
        0x88dd -> :sswitch_0
        0x88df -> :sswitch_0
        0x88e9 -> :sswitch_0
        0x88fd -> :sswitch_0
        0x890b -> :sswitch_0
        0x8914 -> :sswitch_0
        0x8990 -> :sswitch_0
    .end sparse-switch
.end method

.method private shutdownUIStateMachine()V
    .locals 3

    .prologue
    .line 1814
    const-string v0, "Tango.App"

    const-string v1, "shutdownUIStateMachine()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1815
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "ui"

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_messageReceiver:Lcom/sgiggle/messaging/MessageReceiver;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->unregisterReceiver(Ljava/lang/String;Lcom/sgiggle/messaging/MessageReceiver;)V

    .line 1816
    invoke-static {}, Lcom/sgiggle/pjmedia/AudioModeWrapper;->removeFromPreviousContext()V

    .line 1817
    invoke-static {}, Lcom/sgiggle/iphelper/IpHelper;->removeFromPreviousContext()V

    .line 1818
    return-void
.end method

.method private startEmptyService()V
    .locals 3

    .prologue
    .line 1828
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/service/MessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1832
    :goto_0
    return-void

    .line 1829
    :catch_0
    move-exception v0

    .line 1830
    const-string v1, "Tango.App"

    const-string v2, "Caught Security exception on starting service."

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/sgiggle/messaging/Message;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1513
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    .line 1514
    return-void
.end method

.method private stopEmptyService()V
    .locals 3

    .prologue
    .line 1846
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/service/MessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->stopService(Landroid/content/Intent;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1850
    :goto_0
    return-void

    .line 1847
    :catch_0
    move-exception v0

    .line 1848
    const-string v1, "Tango.App"

    const-string v2, "Caught Security exception on stopping service."

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private stopWakeupAlarmService()V
    .locals 3

    .prologue
    .line 1836
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/service/MessageService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1837
    const-string v1, "tango.service.STOP"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1838
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1842
    :goto_0
    return-void

    .line 1839
    :catch_0
    move-exception v0

    .line 1840
    const-string v1, "Tango.App"

    const-string v2, "Caught Security exception on starting service."

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private supportsHTCLockScreen()Z
    .locals 1

    .prologue
    .line 2202
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/htc/lockscreen/HtcLSUtility;->supportRemotePhoneService(Landroid/content/Context;)Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2204
    :goto_0
    return v0

    .line 2203
    :catch_0
    move-exception v0

    .line 2204
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static videomailSupported()Z
    .locals 2

    .prologue
    .line 267
    const/4 v0, 0x0

    const-string v1, "videomail"

    invoke-static {v0, v1}, Lcom/sgiggle/capability/Capability;->getBool(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public addBeforeBackgroundObserver(Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;)V
    .locals 1
    .parameter

    .prologue
    .line 2425
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_beforeBackgroundObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2426
    return-void
.end method

.method public cancelSMSReceiverTimer()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1897
    const-string v0, "Tango.App"

    const-string v1, "Cancel SMS Receiver Timer"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1898
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1899
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".receiver.TimerReceiver"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 1900
    const-string v1, "tango.service.STOP.SMS.LISTENER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1902
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1903
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1904
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1905
    return-void
.end method

.method public clearVideoActivityInstance(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 1966
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-ne v0, p1, :cond_2

    .line 1967
    iput-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    .line 1968
    iput-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 1969
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1970
    iput-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1973
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivityPendingRequest:Lcom/sgiggle/messaging/Message;

    if-eqz v0, :cond_1

    .line 1976
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivityPendingRequest:Lcom/sgiggle/messaging/Message;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1977
    iput-object v4, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivityPendingRequest:Lcom/sgiggle/messaging/Message;

    .line 1980
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->onCallActivityDestroyed()V

    .line 1982
    :cond_2
    return-void
.end method

.method public disableKeyguard()V
    .locals 1

    .prologue
    .line 2448
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V

    .line 2449
    return-void
.end method

.method public enableKeyguard()V
    .locals 1

    .prologue
    .line 2453
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_keyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 2454
    return-void
.end method

.method public getAppRunningState()Lcom/sgiggle/production/TangoApp$AppState;
    .locals 1

    .prologue
    .line 1697
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    return-object v0
.end method

.method public getAudioActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    return-object v0
.end method

.method public getCurrentActivityInstance()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1999
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    return-object v0
.end method

.method public getFacebook()Lcom/facebook/android/Facebook;
    .locals 1

    .prologue
    .line 2508
    sget-object v0, Lcom/sgiggle/production/TangoApp;->m_facebook:Lcom/facebook/android/Facebook;

    return-object v0
.end method

.method public getPostCallActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1957
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getPurchaseState(Ljava/lang/String;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 2232
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_productPurchaseMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2233
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2234
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2237
    :goto_0
    return v0

    .line 2236
    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    .line 2237
    goto :goto_0
.end method

.method public getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;
    .locals 1

    .prologue
    .line 2227
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    return-object v0
.end method

.method public getTabsActivityInstance()Lcom/sgiggle/production/TabActivityBase;
    .locals 1

    .prologue
    .line 1916
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    return-object v0
.end method

.method public getVideoActivity()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1949
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    return-object v0
.end method

.method public hasPurchaseState(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 2243
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_productPurchaseMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2244
    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasVideoActivity()Z
    .locals 1

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLaunchFromLockScreen()Z
    .locals 1

    .prologue
    .line 2214
    iget-boolean v0, p0, Lcom/sgiggle/production/TangoApp;->m_launchFromLockScreen:Z

    return v0
.end method

.method public isTabCurrentClass()Z
    .locals 2

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResumeAfterKilled(Landroid/app/Activity;)V
    .locals 4
    .parameter

    .prologue
    .line 1644
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1645
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResumeAfterKilled(): intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 1652
    sget-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 1659
    :goto_0
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 1660
    return-void

    .line 1654
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->requestServiceForLatestUI()V

    goto :goto_0
.end method

.method public onC2dmCallMessageReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1601
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v7

    const-string v8, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1603
    return-void
.end method

.method public onC2dmCallReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1576
    const-wide/32 v0, 0xea60

    const-string v2, "Tango.App"

    invoke-static {p0, v0, v1, v2}, Lcom/sgiggle/production/Utils;->acquirePartialWakeLock(Landroid/content/Context;JLjava/lang/String;)V

    .line 1577
    if-nez p2, :cond_1

    .line 1578
    const-string v0, "unknown"

    move-object v2, v0

    .line 1582
    :goto_0
    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    .line 1584
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v7

    const-string v8, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v7, v8, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1592
    :goto_1
    return-void

    .line 1589
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;

    invoke-direct {v3, p1, v2, p3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceivePushNotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_1

    :cond_1
    move-object v2, p2

    goto :goto_0
.end method

.method public onC2dmConversationMessageReceived(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1612
    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 1614
    :cond_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1621
    :goto_0
    return-void

    .line 1617
    :cond_1
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v6

    const-string v7, "jingle"

    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageNotificationReceivedMessage;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method public onC2dmMessageReceived(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1595
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;

    invoke-direct {v2, p1, p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReceiveMessageNotificationMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1597
    return-void
.end method

.method public onC2dmRegistrationIdReceived(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1566
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$TangoDeviceTokenMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1569
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->stopWakeupAlarmService()V

    .line 1570
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .parameter

    .prologue
    .line 1749
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConfigurationChanged("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Application;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1751
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 286
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 287
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(): App-state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return-void
.end method

.method public onLowMemory()V
    .locals 4

    .prologue
    .line 1756
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1762
    :goto_0
    const-string v0, "Tango.App"

    const-string v1, "onLowMemory()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1763
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/MessageManager;->clearAllPendingMessages()V

    .line 1764
    return-void

    .line 1757
    :catch_0
    move-exception v0

    .line 1759
    const-string v1, "Tango.App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onTerminate()V
    .locals 2

    .prologue
    .line 1774
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1775
    const-string v0, "Tango.App"

    const-string v1, "onTerminate()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1776
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->shutdownUIStateMachine()V

    .line 1777
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->stopEmptyService()V

    .line 1780
    :cond_0
    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    .line 1781
    return-void
.end method

.method public registerSMSReceiver()V
    .locals 2

    .prologue
    .line 1860
    const-string v0, "Tango.App"

    const-string v1, "registerSMSReceiver()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1861
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.provider.Telephony.SMS_RECEIVED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1862
    new-instance v1, Lcom/sgiggle/production/ValidationSMSReceiver;

    invoke-direct {v1}, Lcom/sgiggle/production/ValidationSMSReceiver;-><init>()V

    iput-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

    .line 1863
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/TangoApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1864
    return-void
.end method

.method public removeBeforeBackgroundObserver(Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;)V
    .locals 1
    .parameter

    .prologue
    .line 2429
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_beforeBackgroundObservers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2430
    return-void
.end method

.method public removePurchaseState(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 2249
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_productPurchaseMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2250
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2251
    return-void
.end method

.method public requestServiceForLatestUI()V
    .locals 3

    .prologue
    .line 2137
    const-string v0, "Tango.App"

    const-string v1, "requestServiceForLatestUI()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2138
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$PutAppInForegroundMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 2140
    return-void
.end method

.method public scheduleSMSReceiverTimer()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1868
    const-string v0, "Tango.App"

    const-string v1, "schedule SMS Receiver Timer"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1869
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1870
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".receiver.TimerReceiver"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 1871
    const-string v1, "tango.service.STOP.SMS.LISTENER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1873
    invoke-static {p0, v6, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1874
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1875
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    add-long/2addr v2, v4

    invoke-virtual {v0, v6, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1878
    return-void
.end method

.method public sendLoginRequestIfNeeded()V
    .locals 3

    .prologue
    .line 1666
    iget-boolean v0, p0, Lcom/sgiggle/production/TangoApp;->m_explicitLoginSent:Z

    if-nez v0, :cond_0

    .line 1667
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/TangoApp;->m_explicitLoginSent:Z

    .line 1668
    const-string v0, "Tango.App"

    const-string v1, "sendLoginRequestIfNeeded(): Send LOGIN..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1669
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$LoginMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1672
    :cond_0
    return-void
.end method

.method public sendMessageAck()V
    .locals 1

    .prologue
    .line 1484
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter;->ack()V

    .line 1485
    return-void
.end method

.method public setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V
    .locals 3
    .parameter

    .prologue
    .line 1706
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAppRunningState(): Changing App-state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " => "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1707
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 1708
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 1710
    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne p1, v1, :cond_2

    .line 1716
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executing "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_beforeBackgroundObservers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " callback before going to background"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1717
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_beforeBackgroundObservers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;

    .line 1718
    invoke-interface {v0}, Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;->beforeGoingToBackground()V

    goto :goto_0

    .line 1720
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->notifyServiceOfBackgroundState()V

    .line 1721
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/messaging/MessageRouter;->enterBackground()V

    .line 1744
    :cond_1
    :goto_1
    return-void

    .line 1723
    :cond_2
    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne p1, v1, :cond_3

    .line 1724
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->requestServiceForLatestUI()V

    goto :goto_1

    .line 1726
    :cond_3
    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_FOREGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne p1, v1, :cond_1

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v0, v1, :cond_1

    .line 1728
    iget v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    if-lez v0, :cond_4

    .line 1729
    iget v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    .line 1733
    :goto_2
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->registerLockScreenReceiver()V

    .line 1734
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->requestServiceForLatestUI()V

    .line 1736
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    .line 1737
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-nez v0, :cond_1

    .line 1740
    const-string v0, "Tango.App"

    const-string v1, "setAppRunningState(): Resume Video screen..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1741
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/TangoApp;->startVideoActivity(Lcom/sgiggle/messaging/Message;I)V

    goto :goto_1

    .line 1731
    :cond_4
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayWelcomeScreenMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayWelcomeScreenMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_2
.end method

.method public setAudioActivityInstance(Lcom/sgiggle/production/AudioInProgressActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1936
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_audioActivity:Lcom/sgiggle/production/AudioInProgressActivity;

    .line 1937
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 1939
    if-nez p1, :cond_0

    .line 1940
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->onCallActivityDestroyed()V

    .line 1942
    :cond_0
    return-void
.end method

.method public setAvatarDemoActivity(Lcom/sgiggle/production/AvatarDemo;)V
    .locals 0
    .parameter

    .prologue
    .line 2021
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_avatarDemoActivity:Lcom/sgiggle/production/AvatarDemo;

    .line 2022
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 2023
    return-void
.end method

.method public setCurrentActivityInstance(Landroid/app/Activity;)V
    .locals 0
    .parameter

    .prologue
    .line 2003
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 2004
    return-void
.end method

.method public setEmailVerificationActivityInstance(Lcom/sgiggle/production/EmailVerificationActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 1928
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_emailVerificationActivity:Lcom/sgiggle/production/EmailVerificationActivity;

    .line 1929
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    const-class v1, Lcom/sgiggle/production/EmailVerificationActivity;

    if-ne v0, v1, :cond_0

    .line 1931
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1933
    :cond_0
    return-void
.end method

.method public setLaunchFromLockScreen(Z)V
    .locals 0
    .parameter

    .prologue
    .line 2223
    iput-boolean p1, p0, Lcom/sgiggle/production/TangoApp;->m_launchFromLockScreen:Z

    .line 2224
    return-void
.end method

.method public setPostCallActivityInstance(Landroid/app/Activity;)V
    .locals 0
    .parameter

    .prologue
    .line 1953
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_postCallActivity:Landroid/app/Activity;

    .line 1954
    return-void
.end method

.method public setRecordVideomailActivityInstance(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 2007
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 2008
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_recordVideomailActivity:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    .line 2009
    return-void
.end method

.method public setRegisterActivityInstance(Lcom/sgiggle/production/RegisterUserActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 1920
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_registerActivity:Lcom/sgiggle/production/RegisterUserActivity;

    .line 1921
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    const-class v1, Lcom/sgiggle/production/RegisterUserActivity;

    if-ne v0, v1, :cond_0

    .line 1923
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1925
    :cond_0
    return-void
.end method

.method public setSendVideomailActivityHelper(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)V
    .locals 0
    .parameter

    .prologue
    .line 2026
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_sendVideomailActivityHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 2027
    return-void
.end method

.method public setSnsActivityInstance(Lcom/sgiggle/production/SnsComposerActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1912
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_snsActivity:Lcom/sgiggle/production/SnsComposerActivity;

    .line 1913
    return-void
.end method

.method public setTabsActivityInstance(Lcom/sgiggle/production/TabActivityBase;)V
    .locals 0
    .parameter

    .prologue
    .line 1908
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    .line 1909
    return-void
.end method

.method public setVideoActivityInstance(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1961
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    .line 1962
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 1963
    return-void
.end method

.method public setVideoPlayerActivityInstance(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 2016
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailViewActivity:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    .line 2017
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currActivty:Landroid/app/Activity;

    .line 2018
    return-void
.end method

.method public setVideomailStateMachineUploadListener(Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;)V
    .locals 0
    .parameter

    .prologue
    .line 2012
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_videomailStateMachineUploadListener:Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;

    .line 2013
    return-void
.end method

.method public showQuerySendSMSDialog(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Ljava/util/List;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2466
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    const/4 v2, 0x0

    move-object v1, p5

    move-object v3, p6

    move-object v4, p4

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLjava/util/List;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 2470
    const-class v1, Lcom/sgiggle/production/QuerySendSMSDialogActivity;

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    .line 2471
    return-void
.end method

.method public skipWelcomePageOnce()V
    .locals 1

    .prologue
    .line 2437
    iget v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/TangoApp;->m_skipWelcomPageCounter:I

    .line 2438
    return-void
.end method

.method public startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/sgiggle/messaging/Message;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1520
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startExplicitActivity(message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1521
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1522
    const-string v1, "com.sgiggle.production.tangoapp.EXTRA_APP_STATE"

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp$AppState;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1523
    invoke-virtual {v0, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1524
    if-eqz p2, :cond_0

    .line 1525
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/sgiggle/production/MessageManager;->storeMessageInIntent(Lcom/sgiggle/messaging/Message;Landroid/content/Intent;)V

    .line 1533
    :cond_0
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1535
    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    if-eq p1, v1, :cond_1

    const-class v1, Lcom/sgiggle/production/VideoTwoWayGLActivity;

    if-eq p1, v1, :cond_1

    const-class v1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    if-eq p1, v1, :cond_1

    const-class v1, Lcom/sgiggle/production/GameInCallActivity;

    if-ne p1, v1, :cond_2

    .line 1537
    :cond_1
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v1

    .line 1538
    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v1, v2, :cond_2

    .line 1540
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->disableKeyguard()V

    .line 1544
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    if-eqz v1, :cond_3

    const-class v1, Lcom/sgiggle/production/AudioInProgressActivity;

    if-eq p1, v1, :cond_3

    invoke-direct {p0, p1}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_3

    const-class v1, Lcom/sgiggle/production/TabActivityBase;

    if-eq p1, v1, :cond_3

    const-class v1, Lcom/sgiggle/production/ContactDetailActivity;

    if-eq p1, v1, :cond_3

    .line 1550
    const-string v1, "Tango.App"

    const-string v2, "tabs.startActivity()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    iget-object v1, p0, Lcom/sgiggle/production/TangoApp;->m_tabsActivity:Lcom/sgiggle/production/TabActivityBase;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/TabActivityBase;->startActivity(Landroid/content/Intent;)V

    .line 1559
    :goto_0
    iput-object p1, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    .line 1560
    return-void

    .line 1553
    :cond_3
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1554
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->startActivity(Landroid/content/Intent;)V

    .line 1556
    const-string v0, "Tango.App"

    const-string v1, "this.startActivity"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startInitScreenIfResumeFromKilled()V
    .locals 3

    .prologue
    .line 1626
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_INIT:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v0, v1, :cond_0

    .line 1627
    sget-object v0, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    .line 1628
    const-string v0, "Tango.App"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startInitScreenIfResumeFromKilled(): Put App-state to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/TangoApp;->m_appState:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    const-class v0, Lcom/sgiggle/production/SplashScreen;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;)V

    .line 1631
    :cond_0
    return-void
.end method

.method public startVideoActivity(Lcom/sgiggle/messaging/Message;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 2030
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_videoActivity:Lcom/sgiggle/production/VideoTwoWayActivity;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_currentClass:Ljava/lang/Class;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/TangoApp;->isVideoActivityClass(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2031
    const-string v0, "Tango.App"

    const-string v1, "Skipping starting video activity, one is already starting."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2042
    :goto_0
    return-void

    .line 2037
    :cond_0
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2038
    const-class v0, Lcom/sgiggle/production/VideoTwoWayGLActivity;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    goto :goto_0

    .line 2040
    :cond_1
    const-class v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    invoke-virtual {p0, v0, p1, p2}, Lcom/sgiggle/production/TangoApp;->startExplicitActivity(Ljava/lang/Class;Lcom/sgiggle/messaging/Message;I)V

    goto :goto_0
.end method

.method public startWakeupAlarmService()V
    .locals 2

    .prologue
    .line 1821
    const-string v0, "Tango.App"

    const-string v1, "Start Wakeup AlarmService"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1822
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->markRegistrationFlag(Landroid/content/Context;)V

    .line 1823
    invoke-direct {p0}, Lcom/sgiggle/production/TangoApp;->startEmptyService()V

    .line 1824
    return-void
.end method

.method public switchToConversationDetailIfPossible(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 1680
    invoke-virtual {p0}, Lcom/sgiggle/production/TangoApp;->skipWelcomePageOnce()V

    .line 1687
    invoke-static {}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->clearRunningInstance()V

    .line 1689
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;

    invoke-direct {v2, p1, v3, v3, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;-><init>(Ljava/lang/String;ZZZ)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1691
    return-void
.end method

.method public unregisterSMSReceiver()V
    .locals 2

    .prologue
    .line 1881
    const-string v0, "Tango.App"

    const-string v1, "unregisterSMSReceiver()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1882
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

    if-eqz v0, :cond_0

    .line 1883
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1884
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_smsReceiver:Lcom/sgiggle/production/ValidationSMSReceiver;

    .line 1886
    :cond_0
    return-void
.end method

.method public unregisterScreenLockReceiver()V
    .locals 2

    .prologue
    .line 1889
    const-string v0, "Tango.App"

    const-string v1, "unregisterScreenLockReceiver()..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1890
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

    if-eqz v0, :cond_0

    .line 1891
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TangoApp;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1892
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_lockSreenReceiver:Lcom/sgiggle/production/LockScreenReceiver;

    .line 1894
    :cond_0
    return-void
.end method

.method public updatePurchaseState(Ljava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 2255
    iget-object v0, p0, Lcom/sgiggle/production/TangoApp;->m_productPurchaseMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2256
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2257
    return-void
.end method
