.class final Lcom/sgiggle/production/TipsActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "TipsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/TipsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyWebViewClient"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/TipsActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/TipsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/TipsActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/TipsActivity;Lcom/sgiggle/production/TipsActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;-><init>(Lcom/sgiggle/production/TipsActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 47
    const-string v0, "Tango.TipsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Finished loading URL: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/TipsActivity;

    #getter for: Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/sgiggle/production/TipsActivity;->access$100(Lcom/sgiggle/production/TipsActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/TipsActivity;

    #getter for: Lcom/sgiggle/production/TipsActivity;->m_progressView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sgiggle/production/TipsActivity;->access$200(Lcom/sgiggle/production/TipsActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/TipsActivity;

    #getter for: Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/sgiggle/production/TipsActivity;->access$100(Lcom/sgiggle/production/TipsActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->requestFocus(I)Z

    .line 51
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;->this$0:Lcom/sgiggle/production/TipsActivity;

    .line 56
    const-string v1, "Tango.TipsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceivedError: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 58
    return-void
.end method
