.class public Lcom/sgiggle/production/TipsActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "TipsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/TipsActivity$1;,
        Lcom/sgiggle/production/TipsActivity$MyWebViewClient;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.TipsActivity"


# instance fields
.field private m_progressView:Landroid/view/ViewGroup;

.field private m_webView:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 45
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/TipsActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/TipsActivity;)Landroid/view/ViewGroup;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_progressView:Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 63
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplaySettingsMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 65
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 27
    const-string v0, "Tango.TipsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate(savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const v0, 0x7f030058

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TipsActivity;->setContentView(I)V

    .line 31
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TipsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_progressView:Landroid/view/ViewGroup;

    .line 32
    const v0, 0x7f0a0144

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/TipsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;

    .line 34
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 35
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 37
    iget-object v0, p0, Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;

    new-instance v1, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/TipsActivity$MyWebViewClient;-><init>(Lcom/sgiggle/production/TipsActivity;Lcom/sgiggle/production/TipsActivity$1;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 38
    invoke-virtual {p0}, Lcom/sgiggle/production/TipsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/sgiggle/production/TipsActivity;->m_webView:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 69
    const-string v0, "Tango.TipsActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 71
    return-void
.end method
