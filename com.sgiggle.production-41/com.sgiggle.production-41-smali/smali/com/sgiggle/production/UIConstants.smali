.class public final Lcom/sgiggle/production/UIConstants;
.super Ljava/lang/Object;
.source "UIConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/UIConstants$CTA;
    }
.end annotation


# static fields
.field public static final CALL_RECEIVED_NOTIFICATION_ID:I = 0x1

.field public static final CONVERSATION_MESSAGE_ERROR_NOTIFICATION_ID:I = 0x7

.field public static final CONVERSATION_MESSAGE_NOTIFICATION_ID:I = 0x5

.field public static final CTA_ALERT_BASE_NOTIFICATION_ID:I = 0x3e8

.field public static final DEVICE_CONTACT_ID_UNKNOWN:J = -0x1L

.field public static final EMAIL_INVITE_LIMIT:I = 0x1f4

.field public static final INVISIBLE_NOTIFICATION_ID:I = 0x6

.field public static final LOW_ON_SPACE_NOTIFICATION_ID:I = 0x4

.field public static final MISSED_CALL_JID:Ljava/lang/String; = "missedJid"

.field public static final MISSED_CALL_NAME:Ljava/lang/String; = "missedDisplayname"

.field public static final MISSED_CALL_NOTIFICATION_ID:I = 0x2

.field public static final MISSED_CALL_WHEN:Ljava/lang/String; = "missedWhen"

.field public static final PUSH_MESSAGE_NOTIFICATION_ID:I = 0x3

.field public static final TIPS_URL_FOR_LOW_MEMORY:Ljava/lang/String; = "http://www.tango.me/android/lowmem"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    return-void
.end method
