.class public Lcom/sgiggle/production/UpdateRequiredActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "UpdateRequiredActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UpdateRequiredActivity"

.field private static final s_updateURI:Ljava/lang/String; = "market://details?id=com.sgiggle.production"


# instance fields
.field private m_action:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    return-void
.end method

.method public static forceTangoUpdate(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 101
    :try_start_0
    const-string v0, "market://details?id=com.sgiggle.production"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 102
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 103
    const/high16 v0, 0x1000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 104
    const/high16 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 105
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 109
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 110
    const v1, 0x7f09007a

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 111
    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 112
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 113
    const v1, 0x7f09005d

    new-instance v2, Lcom/sgiggle/production/UpdateRequiredActivity$1;

    invoke-direct {v2}, Lcom/sgiggle/production/UpdateRequiredActivity$1;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 118
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public static getUpdateURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    const-string v0, "market://details?id=com.sgiggle.production"

    return-object v0
.end method


# virtual methods
.method protected handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 48
    const-string v0, "UpdateRequiredActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleNewMessage(): Message = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 52
    packed-switch v0, :pswitch_data_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 55
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateRequiredEvent;

    .line 56
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateRequiredEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UpdateRequiredPayload;->getAction()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/UpdateRequiredActivity;->m_action:Ljava/lang/String;

    .line 59
    const-string v0, "UpdateRequiredActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleNewMessage(): message = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "], action = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/UpdateRequiredActivity;->m_action:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    const v0, 0x7f0a0055

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/UpdateRequiredActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x8903
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a0153

    if-ne v0, v1, :cond_0

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/UpdateRequiredActivity;->m_action:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 86
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 87
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/UpdateRequiredActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    invoke-static {p0}, Lcom/sgiggle/production/UpdateRequiredActivity;->forceTangoUpdate(Landroid/content/Context;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 30
    const-string v0, "UpdateRequiredActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const v0, 0x7f030059

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/UpdateRequiredActivity;->setContentView(I)V

    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/UpdateRequiredActivity;->setRequestedOrientation(I)V

    .line 35
    const v0, 0x7f0a0153

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/UpdateRequiredActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 36
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    invoke-virtual {p0}, Lcom/sgiggle/production/UpdateRequiredActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/UpdateRequiredActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 39
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 71
    const-string v0, "UpdateRequiredActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 73
    return-void
.end method
