.class public Lcom/sgiggle/production/Utils$ContactComparator;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ContactComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sgiggle/production/Utils$IContactComparable;",
        ">;"
    }
.end annotation


# instance fields
.field private final m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    sget-object v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    iput-object v0, p0, Lcom/sgiggle/production/Utils$ContactComparator;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/sgiggle/production/Utils$ContactComparator;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    .line 72
    return-void
.end method


# virtual methods
.method public compare(Lcom/sgiggle/production/Utils$IContactComparable;Lcom/sgiggle/production/Utils$IContactComparable;)I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/Utils$ContactComparator;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-interface {p1, v0}, Lcom/sgiggle/production/Utils$IContactComparable;->compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$ContactComparator;->m_sortOrder:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    invoke-interface {p2, v1}, Lcom/sgiggle/production/Utils$IContactComparable;->compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 63
    check-cast p1, Lcom/sgiggle/production/Utils$IContactComparable;

    check-cast p2, Lcom/sgiggle/production/Utils$IContactComparable;

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/Utils$ContactComparator;->compare(Lcom/sgiggle/production/Utils$IContactComparable;Lcom/sgiggle/production/Utils$IContactComparable;)I

    move-result v0

    return v0
.end method
