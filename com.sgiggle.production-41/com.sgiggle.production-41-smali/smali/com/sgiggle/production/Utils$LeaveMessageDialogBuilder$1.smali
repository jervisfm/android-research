.class final Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->create(Landroid/app/Activity;Ljava/lang/String;Z)Lcom/sgiggle/production/dialog/TangoAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$activity:Landroid/app/Activity;

.field final synthetic val$finishActivityOnCancel:Z


# direct methods
.method constructor <init>(ZLandroid/app/Activity;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 651
    iput-boolean p1, p0, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;->val$finishActivityOnCancel:Z

    iput-object p2, p0, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;->val$activity:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 654
    const/4 v0, 0x0

    #calls: Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->postQueryLeaveMessageResultMessage(Z)V
    invoke-static {v0}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->access$000(Z)V

    .line 655
    iget-boolean v0, p0, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;->val$finishActivityOnCancel:Z

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;->val$activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 658
    :cond_0
    return-void
.end method
