.class public Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LeaveMessageDialogBuilder"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Z)V
    .locals 0
    .parameter

    .prologue
    .line 626
    invoke-static {p0}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->postQueryLeaveMessageResultMessage(Z)V

    return-void
.end method

.method public static create(Landroid/app/Activity;Ljava/lang/String;Z)Lcom/sgiggle/production/dialog/TangoAlertDialog;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 639
    new-instance v0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 640
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090155

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->setCancelable(Z)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->setDismissBeforeGoingToBackground(Z)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090021

    new-instance v3, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$2;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$2;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090022

    new-instance v3, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;

    invoke-direct {v3, p2, p0}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder$1;-><init>(ZLandroid/app/Activity;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    .line 661
    invoke-virtual {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->create()Lcom/sgiggle/production/dialog/TangoAlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private static postQueryLeaveMessageResultMessage(Z)V
    .locals 3
    .parameter

    .prologue
    .line 669
    const-string v0, "Tango.Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " sending QueryLeaveMessageResultMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageResultMessage;

    invoke-direct {v0, p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$QueryLeaveMessageResultMessage;-><init>(Z)V

    .line 671
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 672
    return-void
.end method
