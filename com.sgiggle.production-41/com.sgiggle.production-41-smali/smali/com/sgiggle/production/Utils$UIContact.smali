.class public Lcom/sgiggle/production/Utils$UIContact;
.super Ljava/lang/Object;
.source "Utils.java"

# interfaces
.implements Lcom/sgiggle/production/Utils$IContactComparable;
.implements Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UIContact"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public transient hasPhoto:Z

.field public m_accountId:Ljava/lang/String;

.field public m_countryCode:Ljava/lang/String;

.field public m_countryId:Ljava/lang/String;

.field public m_countryName:Ljava/lang/String;

.field public m_deviceContactId:J

.field public m_displayName:Ljava/lang/String;

.field public m_email:Ljava/lang/String;

.field public m_favorite:Z

.field public m_firstName:Ljava/lang/String;

.field public m_lastName:Ljava/lang/String;

.field public m_middleName:Ljava/lang/String;

.field public m_namePrefix:Ljava/lang/String;

.field public m_nameSuffix:Ljava/lang/String;

.field public m_phoneNumber:Ljava/lang/String;

.field public m_phoneType:I

.field public m_selected:Z


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_namePrefix:Ljava/lang/String;

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_middleName:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_nameSuffix:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    .line 98
    const/4 v0, 0x7

    iput v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneType:I

    .line 102
    iput-boolean v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    .line 103
    iput-boolean v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    .line 117
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_namePrefix:Ljava/lang/String;

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_middleName:Ljava/lang/String;

    .line 89
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_nameSuffix:Ljava/lang/String;

    .line 91
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    .line 97
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    .line 98
    const/4 v0, 0x7

    iput v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneType:I

    .line 102
    iput-boolean v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    .line 103
    iput-boolean v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    .line 109
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_namePrefix:Ljava/lang/String;

    .line 110
    if-nez p2, :cond_1

    const-string v0, ""

    :goto_1
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    .line 111
    if-nez p3, :cond_2

    const-string v0, ""

    :goto_2
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_middleName:Ljava/lang/String;

    .line 112
    if-nez p4, :cond_3

    const-string v0, ""

    :goto_3
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    .line 113
    if-nez p5, :cond_4

    const-string v0, ""

    :goto_4
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_nameSuffix:Ljava/lang/String;

    .line 114
    if-nez p6, :cond_5

    const-string v0, ""

    :goto_5
    iput-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    .line 115
    return-void

    :cond_0
    move-object v0, p1

    .line 109
    goto :goto_0

    :cond_1
    move-object v0, p2

    .line 110
    goto :goto_1

    :cond_2
    move-object v0, p3

    .line 111
    goto :goto_2

    :cond_3
    move-object v0, p4

    .line 112
    goto :goto_3

    :cond_4
    move-object v0, p5

    .line 113
    goto :goto_4

    :cond_5
    move-object v0, p6

    .line 114
    goto :goto_5
.end method

.method public static convertFromMessageContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/production/Utils$UIContact;
    .locals 7
    .parameter

    .prologue
    .line 219
    new-instance v0, Lcom/sgiggle/production/Utils$UIContact;

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/sgiggle/production/Utils$UIContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    .line 223
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    .line 225
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    .line 227
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v1

    invoke-static {v1}, Lcom/sgiggle/production/Utils$UIContact;->convertPhoneTypeToNative(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)I

    move-result v1

    iput v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneType:I

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 232
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    .line 235
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasFavorite()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 236
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFavorite()Z

    move-result v1

    iput-boolean v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    .line 238
    :cond_2
    return-object v0
.end method

.method public static convertPhoneTypeToNative(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)I
    .locals 2
    .parameter

    .prologue
    .line 188
    sget-object v0, Lcom/sgiggle/production/Utils$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$PhoneType:[I

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 193
    const/4 v0, 0x7

    :goto_0
    return v0

    .line 189
    :pswitch_0
    const/4 v0, 0x2

    goto :goto_0

    .line 190
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 191
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 192
    :pswitch_3
    const/16 v0, 0xc

    goto :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static convertPhoneTypeToProto(I)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1
    .parameter

    .prologue
    .line 201
    sparse-switch p0, :sswitch_data_0

    .line 206
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    :goto_0
    return-object v0

    .line 202
    :sswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MOBILE:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 203
    :sswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_HOME:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 204
    :sswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_WORK:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 205
    :sswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_MAIN:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 201
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x3 -> :sswitch_2
        0xc -> :sswitch_3
    .end sparse-switch
.end method

.method public static getDummyContact()Lcom/sgiggle/production/Utils$UIContact;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/sgiggle/production/Utils$UIContact;

    invoke-direct {v0}, Lcom/sgiggle/production/Utils$UIContact;-><init>()V

    return-object v0
.end method


# virtual methods
.method public compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 135
    const/4 v0, 0x0

    .line 136
    sget-object v1, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;->PRIMARY:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    if-ne p1, v1, :cond_3

    .line 137
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    .line 153
    :cond_0
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    .line 157
    :cond_1
    return-object v0

    .line 140
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    goto :goto_0

    .line 145
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    goto :goto_0

    .line 148
    :cond_4
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    goto :goto_0
.end method

.method public convertToMessageContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 3

    .prologue
    .line 242
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_namePrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_middleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_nameSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 252
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 254
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 255
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneType:I

    invoke-static {v2}, Lcom/sgiggle/production/Utils$UIContact;->convertPhoneTypeToProto(I)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 256
    :cond_1
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public dispNameFirstChar(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)C
    .locals 2
    .parameter

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/Utils$UIContact;->compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/Utils$UIContact;->compareName(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 167
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x20

    goto :goto_0
.end method

.method public displayName()Ljava/lang/String;
    .locals 5

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_displayName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    iget-object v4, p0, Lcom/sgiggle/production/Utils$UIContact;->m_email:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sgiggle/production/Utils;->getDisplayName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public firstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    return-object v0
.end method

.method public getDataToLoadImage()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 266
    iget-wide v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getImageId()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getViewTag()I
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/sgiggle/production/Utils$UIContact;->hashCode()I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 215
    iget-wide v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    long-to-int v0, v0

    return v0
.end method

.method public lastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    return-object v0
.end method

.method public middleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_middleName:Ljava/lang/String;

    return-object v0
.end method

.method public namePrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_namePrefix:Ljava/lang/String;

    return-object v0
.end method

.method public namesuffix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_nameSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public onLoadFailed()V
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    .line 272
    return-void
.end method

.method public phoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIContact;->m_phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public shouldHaveImage()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    return v0
.end method
