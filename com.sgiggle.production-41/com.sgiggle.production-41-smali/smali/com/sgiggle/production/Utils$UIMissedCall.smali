.class public Lcom/sgiggle/production/Utils$UIMissedCall;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UIMissedCall"
.end annotation


# instance fields
.field private m_missedJid:Ljava/lang/String;

.field private m_missedName:Ljava/lang/String;

.field private m_missedWhen:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 303
    iput-object p1, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedJid:Ljava/lang/String;

    .line 304
    iput-object p2, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedName:Ljava/lang/String;

    .line 305
    iput-wide p3, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedWhen:J

    .line 306
    return-void
.end method


# virtual methods
.method public getMissedJid()Ljava/lang/String;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedJid:Ljava/lang/String;

    return-object v0
.end method

.method public getMissedName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedName:Ljava/lang/String;

    return-object v0
.end method

.method public getMissedWhen()J
    .locals 2

    .prologue
    .line 310
    iget-wide v0, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedWhen:J

    return-wide v0
.end method

.method public isSame(Lcom/sgiggle/production/Utils$UIMissedCall;)Z
    .locals 4
    .parameter

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedJid:Ljava/lang/String;

    iget-object v1, p1, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedJid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedWhen:J

    iget-wide v2, p1, Lcom/sgiggle/production/Utils$UIMissedCall;->m_missedWhen:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
