.class public Lcom/sgiggle/production/VGoodDemo;
.super Lcom/sgiggle/production/GenericProductDemoActivity;
.source "VGoodDemo.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;-><init>()V

    return-void
.end method


# virtual methods
.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 2
    .parameter

    .prologue
    .line 12
    if-nez p1, :cond_0

    .line 27
    :goto_0
    return-void

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    .line 17
    packed-switch v1, :pswitch_data_0

    .line 26
    :goto_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/GenericProductDemoActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 19
    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailEvent;

    move-object v1, v0

    .line 20
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/VGoodDemo;->onNewProductDetailPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductDetailsPayload;)V

    goto :goto_1

    .line 17
    :pswitch_data_0
    .packed-switch 0x8998
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 31
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToVGoodProductCatalogMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$BackToVGoodProductCatalogMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 33
    invoke-super {p0}, Lcom/sgiggle/production/GenericProductDemoActivity;->onBackPressed()V

    .line 34
    return-void
.end method
