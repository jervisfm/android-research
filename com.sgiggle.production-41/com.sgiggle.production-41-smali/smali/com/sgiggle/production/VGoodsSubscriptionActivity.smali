.class public Lcom/sgiggle/production/VGoodsSubscriptionActivity;
.super Lcom/sgiggle/production/GenericProductCatalogActivity;
.source "VGoodsSubscriptionActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/sgiggle/production/GenericProductCatalogActivity;-><init>()V

    return-void
.end method

.method private dismissNewBadge()V
    .locals 3

    .prologue
    .line 53
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;

    const-string v1, "product.category.vgood"

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissStoreBadgeMessage;-><init>(Ljava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 56
    return-void
.end method


# virtual methods
.method handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 15
    if-nez p1, :cond_0

    .line 39
    :goto_0
    return-void

    .line 18
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 38
    :cond_1
    :goto_1
    invoke-super {p0, p1}, Lcom/sgiggle/production/GenericProductCatalogActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 20
    :sswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    .line 21
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->onNewProductCatalogPayload(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;)V

    .line 22
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getAllCached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->dismissNewBadge()V

    goto :goto_1

    .line 28
    :sswitch_1
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;

    move-object v1, v0

    .line 29
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->hasPriceLabel()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 31
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getProductMarketId()Ljava/lang/String;

    move-result-object v2

    .line 32
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ReportPurchaseResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PurchaseResultPayload;->getPriceLabel()Ljava/lang/String;

    move-result-object v1

    .line 33
    iget-object v3, p0, Lcom/sgiggle/production/VGoodsSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;

    invoke-virtual {v3, v2, v1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->markPurchased(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 18
    nop

    :sswitch_data_0
    .sparse-switch
        0x8980 -> :sswitch_1
        0x8997 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$NavigateBackMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$NavigateBackMessage;-><init>()V

    .line 49
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 50
    return-void
.end method

.method protected showDemo(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 3
    .parameter

    .prologue
    .line 42
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailMessage;

    invoke-direct {v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 44
    return-void
.end method
