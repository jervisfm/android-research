.class Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$2;
.super Ljava/lang/Object;
.source "ValidationFailedDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->create()Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$2;->this$0:Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 70
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeRegistrationErrorMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeRegistrationErrorMessage;-><init>()V

    .line 71
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$2;->this$0:Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;

    iget-object v0, v0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->activity:Lcom/sgiggle/production/ValidationFailedDialogActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity;->finish()V

    .line 74
    return-void
.end method
