.class public Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "ValidationFailedDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ValidationFailedDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected activity:Lcom/sgiggle/production/ValidationFailedDialogActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    check-cast p1, Lcom/sgiggle/production/ValidationFailedDialogActivity;

    iput-object p1, p0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->activity:Lcom/sgiggle/production/ValidationFailedDialogActivity;

    .line 43
    return-void
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->activity:Lcom/sgiggle/production/ValidationFailedDialogActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 48
    invoke-static {}, Lcom/sgiggle/production/MessageManager;->getDefault()Lcom/sgiggle/production/MessageManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/MessageManager;->getMessageFromIntent(Landroid/content/Intent;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationFailedEvent;

    .line 49
    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ValidationFailedEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 51
    const v1, 0x7f09005a

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 52
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 54
    new-instance v0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$1;-><init>(Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    const v0, 0x7f09005d

    new-instance v1, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder$2;-><init>(Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
