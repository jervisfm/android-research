.class public Lcom/sgiggle/production/ValidationFailedDialogActivity;
.super Landroid/app/Activity;
.source "ValidationFailedDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ValidationFailedDialogActivity"

.field static VALIDATION_FAILED_DIALOG:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput v0, Lcom/sgiggle/production/ValidationFailedDialogActivity;->VALIDATION_FAILED_DIALOG:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 25
    const-string v0, "Tango.ValidationFailedDialogActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    sget v0, Lcom/sgiggle/production/ValidationFailedDialogActivity;->VALIDATION_FAILED_DIALOG:I

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity;->showDialog(I)V

    .line 28
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter

    .prologue
    .line 32
    const-string v0, "Tango.ValidationFailedDialogActivity"

    const-string v1, "onCreateDialog()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;-><init>(Landroid/content/Context;)V

    .line 34
    invoke-virtual {v0}, Lcom/sgiggle/production/ValidationFailedDialogActivity$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
