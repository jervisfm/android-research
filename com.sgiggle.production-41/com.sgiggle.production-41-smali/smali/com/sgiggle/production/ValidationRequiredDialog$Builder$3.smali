.class Lcom/sgiggle/production/ValidationRequiredDialog$Builder$3;
.super Ljava/lang/Object;
.source "ValidationRequiredDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->create(Ljava/lang/String;)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/ValidationRequiredDialog$Builder;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$3;->this$0:Lcom/sgiggle/production/ValidationRequiredDialog$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 78
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;->SMS:Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;

    .line 79
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$SendValidationCodeRequestMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendValidationCodeRequestMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ValidationCodeDeliveryPayload$ValidationCodeDeliveryType;)V

    .line 80
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$3;->this$0:Lcom/sgiggle/production/ValidationRequiredDialog$Builder;

    const-string v1, "verification_requested"

    #calls: Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->statsCollectorLog(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->access$000(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;Ljava/lang/String;)V

    .line 83
    return-void
.end method
