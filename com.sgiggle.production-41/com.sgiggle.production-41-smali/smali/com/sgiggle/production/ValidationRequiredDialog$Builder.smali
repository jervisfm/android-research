.class public Lcom/sgiggle/production/ValidationRequiredDialog$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "ValidationRequiredDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/ValidationRequiredDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected m_context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->m_context:Landroid/content/Context;

    .line 38
    iput-object p1, p0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->m_context:Landroid/content/Context;

    .line 39
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->statsCollectorLog(Ljava/lang/String;)V

    return-void
.end method

.method private statsCollectorLog(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "sms_verification_dialog_ui"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 94
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "deviceid"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/iphelper/IpHelper;->getDevIDBase64()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 97
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    const-string v2, "fb_valid_session"

    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setKey(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/TangoApp;->getFacebook()Lcom/facebook/android/Facebook;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/android/Facebook;->isSessionValid()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "1"

    :goto_0
    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->setValue(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$KeyValuePair;

    move-result-object v1

    .line 103
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StatsCollectorLogMessage;-><init>(Ljava/util/List;)V

    .line 106
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 107
    return-void

    .line 99
    :cond_0
    const-string v2, "0"

    goto :goto_0
.end method


# virtual methods
.method public create(Ljava/lang/String;)Landroid/app/AlertDialog;
    .locals 3
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 47
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 48
    const v0, 0x7f090170

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 49
    new-instance v0, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$1;-><init>(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 61
    const v0, 0x7f090020

    new-instance v1, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$2;-><init>(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 72
    const v0, 0x7f0900aa

    new-instance v1, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder$3;-><init>(Lcom/sgiggle/production/ValidationRequiredDialog$Builder;)V

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 86
    const-string v0, "verification_dialog_appeared"

    invoke-direct {p0, v0}, Lcom/sgiggle/production/ValidationRequiredDialog$Builder;->statsCollectorLog(Ljava/lang/String;)V

    .line 88
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
