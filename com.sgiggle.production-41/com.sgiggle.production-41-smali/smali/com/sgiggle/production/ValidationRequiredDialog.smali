.class public Lcom/sgiggle/production/ValidationRequiredDialog;
.super Lcom/sgiggle/production/dialog/TangoAlertDialog;
.source "ValidationRequiredDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/ValidationRequiredDialog$Builder;
    }
.end annotation


# static fields
.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "deviceid"

.field private static final KEY_FB_VALID_SESSION:Ljava/lang/String; = "fb_valid_session"

.field private static final KEY_SMS_VERIFICATION_DIALOG_UI:Ljava/lang/String; = "sms_verification_dialog_ui"

.field private static final VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_DIALOG_APPEARED:Ljava/lang/String; = "verification_dialog_appeared"

.field private static final VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_REQUESTED:Ljava/lang/String; = "verification_requested"

.field private static final VALUE_SMS_VERIFICATION_DIALOG_UI_VERIFICATION_SKIPPED:Ljava/lang/String; = "verification_skipped"


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method
