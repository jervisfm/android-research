.class Lcom/sgiggle/production/VideoTwoWayActivity$1;
.super Landroid/os/Handler;
.source "VideoTwoWayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/VideoTwoWayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/VideoTwoWayActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 190
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    #getter for: Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->access$000(Lcom/sgiggle/production/VideoTwoWayActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 191
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Handler: ignoring message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; we\'re destroyed!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " call state = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v2, v2, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 198
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->updateLayout()V

    goto :goto_0

    .line 201
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->updateRendererSize(II)V

    goto :goto_0

    .line 204
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v1, v1, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    #calls: Lcom/sgiggle/production/VideoTwoWayActivity;->showIgnoredCallAlert(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->access$100(Lcom/sgiggle/production/VideoTwoWayActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 207
    :pswitch_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    #calls: Lcom/sgiggle/production/VideoTwoWayActivity;->hasSwitchCameraTipBubbleDismissed()Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->access$200(Lcom/sgiggle/production/VideoTwoWayActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    const v2, 0x7f090119

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
