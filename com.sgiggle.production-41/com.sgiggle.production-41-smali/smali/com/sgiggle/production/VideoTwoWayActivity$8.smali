.class final Lcom/sgiggle/production/VideoTwoWayActivity$8;
.super Ljava/lang/Object;
.source "VideoTwoWayActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/VideoTwoWayActivity;->showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$evt:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;


# direct methods
.method constructor <init>(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 0
    .parameter

    .prologue
    .line 1046
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$8;->val$evt:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 1048
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$8;->val$evt:Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1050
    return-void
.end method
