.class Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;
.super Ljava/util/TimerTask;
.source "VideoTwoWayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/VideoTwoWayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DebugInfoTimerTask"
.end annotation


# instance fields
.field private m_debugInfoText:Ljava/lang/StringBuilder;

.field private m_screenLogger:Lcom/sgiggle/screen/ScreenLogger;

.field final synthetic this$0:Lcom/sgiggle/production/VideoTwoWayActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 381
    new-instance v0, Lcom/sgiggle/screen/ScreenLogger;

    invoke-direct {v0}, Lcom/sgiggle/screen/ScreenLogger;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_screenLogger:Lcom/sgiggle/screen/ScreenLogger;

    .line 382
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/VideoTwoWayActivity;Lcom/sgiggle/production/VideoTwoWayActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 380
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;)Ljava/lang/StringBuilder;
    .locals 1
    .parameter

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 389
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_screenLogger:Lcom/sgiggle/screen/ScreenLogger;

    invoke-virtual {v0}, Lcom/sgiggle/screen/ScreenLogger;->getAllParameters()Ljava/util/LinkedHashMap;

    move-result-object v0

    .line 390
    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 391
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    const-string v3, "\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->m_debugInfoText:Ljava/lang/StringBuilder;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 398
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;->this$0:Lcom/sgiggle/production/VideoTwoWayActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask$1;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 405
    :cond_2
    return-void
.end method
