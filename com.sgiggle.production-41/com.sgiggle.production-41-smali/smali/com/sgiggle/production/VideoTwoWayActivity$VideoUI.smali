.class public interface abstract Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;
.super Ljava/lang/Object;
.source "VideoTwoWayActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/VideoTwoWayActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideoUI"
.end annotation


# virtual methods
.method public abstract bringToFront()V
.end method

.method public abstract changeViews()Z
.end method

.method public abstract hideCafe()V
.end method

.method public abstract initVideoViews()Z
.end method

.method public abstract onAvatarChanged()V
.end method

.method public abstract onAvatarPaused()V
.end method

.method public abstract onAvatarResumed()V
.end method

.method public abstract onCleanUpAvatar()V
.end method

.method public abstract onVideoModeChanged()V
.end method

.method public abstract pipSwapSupported()Z
.end method

.method public abstract showCafe()V
.end method

.method public abstract updateLayout()V
.end method

.method public abstract updateRendererSize(II)V
.end method
