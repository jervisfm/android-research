.class public Lcom/sgiggle/production/VideoTwoWayActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "VideoTwoWayActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/VideoTwoWayActivity$11;,
        Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;,
        Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;
    }
.end annotation


# static fields
.field private static final DEBUG_INFO_UPDATE_INTERVAL:I = 0x1f4

.field private static final DIALOG_CALL_ON_HOLD:I = 0x0

.field private static final DIALOG_LEAVE_MESSAGE:I = 0x1

.field private static final PREF_SWITCH_CAMERA_BUBBLE_DISMISSED:Ljava/lang/String; = "switch_camera_bubble_dismissed"

.field private static final SHOW_IGNORED_CALL:I = 0x7

.field private static final SHOW_IGNORED_CALL_DELAY:I = 0x1f4

.field private static final SHOW_WAND_BUBBLE:I = 0x8

.field protected static final TAG:Ljava/lang/String; = "Tango.Video"

.field private static final THRESHOLD_SWITCH_CAMERA:J = 0x3e8L

.field private static s_showWand:Z


# instance fields
.field private final DISMISS_BUBBLE_DELAY:I

.field private final DISMISS_BUBBLE_MSG:I

.field private final TOGGLE_BACK_CONTROL_BAR_DELAY:I

.field private final TOGGLE_BACK_CONTROL_BAR_MSG:I

.field private animRestart:Z

.field animationListener:Landroid/view/animation/Animation$AnimationListener;

.field controlBarTimer:Ljava/util/Timer;

.field private hasControlBarAnimation:Z

.field private hasVGoodAnimation:Z

.field private mAnimationSlideInRight:Landroid/view/animation/Animation;

.field private mAnimationSlideInTop:Landroid/view/animation/Animation;

.field private mAnimationSlideOutRight:Landroid/view/animation/Animation;

.field private mAnimationSlideOutTop:Landroid/view/animation/Animation;

.field protected mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

.field protected mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

.field private mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

.field private mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

.field private m_alertView:Landroid/widget/TextView;

.field private m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

.field private m_bubbleLayout:Landroid/widget/RelativeLayout;

.field private m_cafeInitialised:Z

.field private m_callStatusTextView:Landroid/widget/TextView;

.field private m_camera_count:I

.field protected m_camera_type:I

.field private m_canCleanCafeOnPause:Z

.field protected m_controlsLayout:Landroid/widget/RelativeLayout;

.field private m_debugInfoTimer:Ljava/util/Timer;

.field protected m_debugInfoView:Landroid/widget/TextView;

.field private m_endButton:Landroid/view/View;

.field private m_endImage:Landroid/widget/ImageView;

.field private m_endIndicator:Landroid/widget/ImageView;

.field private m_endResId:I

.field private m_firstVideoEvent:Z

.field protected m_frameLayout:Landroid/widget/FrameLayout;

.field protected m_handler:Landroid/os/Handler;

.field private m_ignoredCallAlert:Landroid/app/AlertDialog;

.field private m_isDestroyed:Z

.field private m_isInPortrait:Z

.field private m_isViewInitialized:Z

.field private m_lowBandwidthView:Landroid/widget/TextView;

.field private m_muteButton:Landroid/view/View;

.field private m_muteImage:Landroid/widget/ImageView;

.field private m_muteIndicator:Landroid/widget/ImageView;

.field private m_muteResId:I

.field private m_nameTextView:Landroid/widget/TextView;

.field protected m_orientation:I

.field private m_prefs:Landroid/content/SharedPreferences;

.field private m_resumeFromAvatar:Z

.field private m_resumed:Z

.field protected m_session:Lcom/sgiggle/production/CallSession;

.field protected m_showVgoodBar:Z

.field private m_switchCameraButton:Landroid/widget/ImageButton;

.field protected m_switchCameraLayout:Landroid/widget/RelativeLayout;

.field private m_timestampOld:J

.field private m_userNameTxt:Landroid/widget/TextView;

.field protected m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

.field private m_videoButton:Landroid/view/View;

.field private m_videoImage:Landroid/widget/ImageView;

.field private m_videoIndicator:Landroid/widget/ImageView;

.field private m_videoResId:I

.field protected m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

.field private m_wandButton:Landroid/widget/ImageButton;

.field private m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

.field toggleControlBarTask:Ljava/util/TimerTask;

.field private uihandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/VideoTwoWayActivity;->s_showWand:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 89
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    .line 98
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_timestampOld:J

    .line 100
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteButton:Landroid/view/View;

    .line 101
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteImage:Landroid/widget/ImageView;

    .line 102
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    .line 106
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endButton:Landroid/view/View;

    .line 107
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endImage:Landroid/widget/ImageView;

    .line 108
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endIndicator:Landroid/widget/ImageView;

    .line 111
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    .line 112
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoImage:Landroid/widget/ImageView;

    .line 113
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    .line 116
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    .line 117
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    .line 119
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    .line 128
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isViewInitialized:Z

    .line 136
    new-instance v0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-direct {v0}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    .line 144
    const/16 v0, 0x1388

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->DISMISS_BUBBLE_DELAY:I

    .line 145
    const/16 v0, 0x3a98

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->TOGGLE_BACK_CONTROL_BAR_DELAY:I

    .line 146
    iput v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->DISMISS_BUBBLE_MSG:I

    .line 147
    iput v4, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->TOGGLE_BACK_CONTROL_BAR_MSG:I

    .line 148
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->controlBarTimer:Ljava/util/Timer;

    .line 151
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 159
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->getCameraCount()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_count:I

    .line 160
    iput v5, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_type:I

    .line 161
    iput v5, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    .line 163
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumeFromAvatar:Z

    .line 165
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z

    .line 171
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    .line 172
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    .line 173
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    .line 175
    iput-boolean v4, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_firstVideoEvent:Z

    .line 176
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isInPortrait:Z

    .line 178
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    .line 179
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    .line 180
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    .line 181
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_cafeInitialised:Z

    .line 182
    iput-boolean v4, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_canCleanCafeOnPause:Z

    .line 187
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$1;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_handler:Landroid/os/Handler;

    .line 339
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayActivity$4;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$4;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 358
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayActivity$5;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$5;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    .line 1208
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animRestart:Z

    .line 1721
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/VideoTwoWayActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/VideoTwoWayActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->showIgnoredCallAlert(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/VideoTwoWayActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->hasSwitchCameraTipBubbleDismissed()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->clearIgnoredCall()V

    return-void
.end method

.method static synthetic access$402(Lcom/sgiggle/production/VideoTwoWayActivity;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$500(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->hangUpCall()V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/VideoTwoWayActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->setControlBarAnimation(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sgiggle/production/VideoTwoWayActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->toggleWandBtn()V

    return-void
.end method

.method static synthetic access$800(Lcom/sgiggle/production/VideoTwoWayActivity;ZZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissBubbleView(ZZ)V

    return-void
.end method

.method private addVGoodViewAndBringToFront()V
    .locals 4

    .prologue
    .line 1249
    const-string v0, "Tango.Video"

    const-string v1, "addGLViewAndBringToFront"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1250
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1251
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->showCafe()V

    .line 1279
    :cond_0
    :goto_0
    return-void

    .line 1254
    :cond_1
    const v0, 0x7f0a017f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/AbsoluteLayout;

    .line 1255
    invoke-virtual {v0}, Lcom/sgiggle/production/AbsoluteLayout;->getMeasuredWidth()I

    move-result v1

    .line 1256
    invoke-virtual {v0}, Lcom/sgiggle/production/AbsoluteLayout;->getMeasuredHeight()I

    move-result v0

    .line 1259
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v2

    .line 1260
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Display;->getHeight()I

    move-result v3

    .line 1261
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 1262
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1264
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    if-nez v2, :cond_2

    .line 1265
    const-string v0, "Tango.Video"

    const-string v1, "add surface view called before initialization"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1268
    :cond_2
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1269
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1270
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1271
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setZOrderMediaOverlay(Z)V

    .line 1272
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 1273
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->bringToFront()V

    .line 1274
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    if-nez v0, :cond_3

    .line 1275
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 1276
    :cond_3
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    goto :goto_0
.end method

.method private animationOnPause()V
    .locals 2

    .prologue
    .line 648
    const-string v0, "VGOOD"

    const-string v1, "animationOnPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->stopAndRemoveVGoodView()V

    .line 650
    return-void
.end method

.method private animationOnResume()V
    .locals 0

    .prologue
    .line 645
    return-void
.end method

.method private avatarOnPause()V
    .locals 2

    .prologue
    .line 653
    const-string v0, "Avatar"

    const-string v1, "avatarOnPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->onAvatarPaused()V

    .line 655
    return-void
.end method

.method private avatarOnResume()V
    .locals 2

    .prologue
    .line 658
    const-string v0, "Tango.Video"

    const-string v1, "avatarOnResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_0

    .line 664
    :goto_0
    return-void

    .line 662
    :cond_0
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 663
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->onAvatarResumed()V

    goto :goto_0
.end method

.method private cleanUpAvatar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1800
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_1

    .line 1801
    const-string v0, "Tango.Video"

    const-string v1, "avatar direction is not NONE, do not clean avatar here!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1816
    :cond_0
    :goto_0
    return-void

    .line 1804
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1805
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1806
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->onCleanUpAvatar()V

    .line 1809
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Reset()V

    .line 1810
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    if-eqz v0, :cond_0

    .line 1811
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->hideCurrentPlayingAnimation()V

    .line 1812
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v1, v2, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->reenableAllButtons(Z)V

    .line 1813
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->setFilterHightlightWithBackground()V

    goto :goto_0

    .line 1812
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private clearIgnoredCall()V
    .locals 4

    .prologue
    .line 256
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ClearMissedCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 258
    return-void
.end method

.method private createCallOnHoldDialog()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 746
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 747
    const v1, 0x7f090098

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f090099

    new-instance v3, Lcom/sgiggle/production/VideoTwoWayActivity$7;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$7;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09009a

    new-instance v3, Lcom/sgiggle/production/VideoTwoWayActivity$6;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$6;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 771
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private createLeaveMessageDialog()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 780
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 782
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/sgiggle/production/Utils$LeaveMessageDialogBuilder;->create(Landroid/app/Activity;Ljava/lang/String;Z)Lcom/sgiggle/production/dialog/TangoAlertDialog;

    move-result-object v0

    .line 786
    return-object v0
.end method

.method private createVGoodViewAndRenderer()V
    .locals 2

    .prologue
    .line 1830
    new-instance v0, Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodRenderer;-><init>(Z)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    .line 1831
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1832
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 1833
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onResume()V

    .line 1834
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 1836
    :cond_0
    return-void
.end method

.method private dismissBubbleView(ZZ)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/16 v3, 0x8

    .line 1593
    if-eqz p2, :cond_0

    .line 1594
    const v0, 0x7f0a0198

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    .line 1595
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1596
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1598
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1599
    const-string v1, "switch_camera_bubble_dismissed"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1600
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1603
    :cond_0
    if-eqz p1, :cond_2

    .line 1604
    const v0, 0x7f0a0154

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    .line 1605
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1606
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1608
    :cond_1
    sget-boolean v0, Lcom/sgiggle/production/VideoTwoWayActivity;->s_showWand:Z

    if-eqz v0, :cond_2

    .line 1609
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;->LOCATION_IN_CALL:Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$WandPressedMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$WandLocationType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1611
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/VideoTwoWayActivity;->s_showWand:Z

    .line 1614
    :cond_2
    return-void
.end method

.method private dismissKeyguard(Z)V
    .locals 3
    .parameter

    .prologue
    const/high16 v2, 0x40

    const/high16 v1, 0x8

    .line 540
    if-eqz p1, :cond_0

    .line 541
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->addFlags(I)V

    .line 542
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 547
    :goto_0
    return-void

    .line 544
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 545
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0
.end method

.method private dismissVideoBubbleViewByAvatar()V
    .locals 2

    .prologue
    .line 1583
    const-string v0, "Tango.Video"

    const-string v1, "dismissVideoBubbleViewByAvatar"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1584
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1585
    const v0, 0x7f0a0198

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    .line 1586
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1587
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1588
    const-string v0, "Tango.Video"

    const-string v1, "dismissVideoBubbleViewByAvatar() m_bubbleLayout.setVisibility(View.GONE) "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1590
    :cond_0
    return-void
.end method

.method private displayVGoodWarning()V
    .locals 6

    .prologue
    const v2, 0x7f090110

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1373
    sget-object v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_CLIENT:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    if-ne v0, v1, :cond_1

    .line 1374
    const-string v0, "Tango.Video"

    const-string v1, "VGOOD_UNSUPPORTED_CLIENT"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1375
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090111

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 1390
    :cond_0
    :goto_0
    return-void

    .line 1377
    :cond_1
    sget-object v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_UNSUPPORTED_PLATFORM:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    if-ne v0, v1, :cond_2

    .line 1378
    const-string v0, "Tango.Video"

    const-string v1, "VGOOD_UNSUPPORTED_PLATFORM"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1379
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090112

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0

    .line 1381
    :cond_2
    sget-object v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_DOWNLOADING:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    if-ne v0, v1, :cond_3

    .line 1382
    const-string v0, "Tango.Video"

    const-string v1, "VGOOD_DOWNLOADING"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090113

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0

    .line 1385
    :cond_3
    sget-object v0, Lcom/sgiggle/cafe/vgood/CafeMgr;->vgoodSupport:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;->VGOOD_INVALID_STATE:Lcom/sgiggle/xmpp/SessionMessages$VGoodSupportType;

    if-ne v0, v1, :cond_0

    .line 1386
    const-string v0, "Tango.Video"

    const-string v1, "VGOOD_INVALID_STATE"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090115

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto :goto_0
.end method

.method private handleAnimationEvent(Lcom/sgiggle/messaging/Message;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 1210
    const-string v0, "Tango.Video"

    const-string v1, "handleAnimationEvent"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;

    .line 1212
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetId()J

    move-result-wide v1

    .line 1213
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getRestart()Z

    move-result v0

    .line 1214
    const-string v3, "VGOOD"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "restart? "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animRestart:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1215
    if-ne v0, v6, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    if-eqz v0, :cond_1

    .line 1217
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->animationOnPause()V

    .line 1218
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->animationOnResume()V

    .line 1219
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;

    long-to-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$InitiateVGoodMessage;-><init>(Ljava/lang/Integer;)V

    .line 1220
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1221
    iput-boolean v6, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animRestart:Z

    .line 1246
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    if-nez v0, :cond_0

    .line 1231
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->createVGoodViewAndRenderer()V

    .line 1232
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->addVGoodViewAndBringToFront()V

    .line 1234
    iput-boolean v6, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    .line 1235
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->handleDisplayAnimationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAnimationEvent;)V

    .line 1237
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v6}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->updateButtons(I)V

    .line 1238
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 1239
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->bringToFront()V

    .line 1240
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 1241
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bringToFront()V

    .line 1242
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1243
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 1245
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->restartControlBarTimer()V

    goto :goto_0
.end method

.method private handleAudioInInitializationEvent()V
    .locals 0

    .prologue
    .line 1119
    return-void
.end method

.method private handleAudioInProgressEvent()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 1123
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cleanUpAvatar()V

    .line 1124
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1125
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_nameTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1126
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->setVolumeControlStream(I)V

    .line 1127
    return-void
.end method

.method private handleAvatarEvent(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 1738
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->CameraToType(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_type:I

    .line 1739
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    .line 1744
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getAppRunningState()Lcom/sgiggle/production/TangoApp$AppState;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v1, v2, :cond_1

    .line 1745
    const-string v1, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleAvatarEvent  in background, avatar state shoulb be changed to : ,dir:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1779
    :cond_0
    :goto_0
    return-void

    .line 1749
    :cond_1
    const-string v1, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleAvatarEvent avatar state will be changed to : ,dir:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1750
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iput-object v0, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 1752
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v1, v2, :cond_2

    .line 1753
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissVideoBubbleViewByAvatar()V

    .line 1755
    :cond_2
    if-nez p2, :cond_4

    .line 1756
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    .line 1766
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1767
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->resetPipSwapped()V

    .line 1770
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->stopAndRemoveVGoodView()V

    .line 1771
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1772
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1773
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->onAvatarChanged()V

    .line 1774
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    .line 1777
    if-nez p2, :cond_0

    .line 1778
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumeFromAvatar:Z

    goto :goto_0

    .line 1758
    :cond_4
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_5

    .line 1759
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_1

    .line 1760
    :cond_5
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_6

    .line 1761
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_1

    .line 1763
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    goto :goto_1
.end method

.method private handleAvatarRenderRequest(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 1819
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;

    .line 1820
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getIsLocal()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    if-eqz v0, :cond_0

    .line 1822
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->handleRenderRequest(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;)V

    .line 1827
    :cond_0
    :goto_0
    return-void

    .line 1824
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    if-eqz v0, :cond_0

    .line 1825
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->handleRenderRequest(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;)V

    goto :goto_0
.end method

.method private handleBandwidthEvent()V
    .locals 2

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v1, v1, Lcom/sgiggle/production/CallSession;->m_showLowBandwidth:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1132
    return-void

    .line 1130
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private handleCallDisconnectingEvent()V
    .locals 3

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 1010
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onSurfaceDestroyed()V

    .line 1011
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onSurfaceDestroyed()V

    .line 1012
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_canCleanCafeOnPause:Z

    .line 1013
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleCallDisconnectingEvent m_session.m_callState ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 1025
    return-void
.end method

.method private handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 1035
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1036
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 1037
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->switchToPortrait()V

    .line 1038
    invoke-static {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    .line 1040
    :cond_0
    return-void
.end method

.method private handleInCallAlertEvent()V
    .locals 3

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_showInCallAlert:Z

    if-eqz v0, :cond_0

    .line 1136
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_inCallAlertText:Ljava/lang/String;

    sget-object v2, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 1137
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1142
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private handleMissedCallEvent()V
    .locals 4

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DIALING:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1080
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTED:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 1081
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x7

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1091
    :goto_0
    return-void

    .line 1088
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->clearIgnoredCall()V

    .line 1089
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    goto :goto_0
.end method

.method private handleSendCallAcceptedEvent()V
    .locals 3

    .prologue
    .line 1098
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 1113
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1115
    return-void
.end method

.method private handleSendCallInvitationEvent()V
    .locals 3

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissPendingIgnoredCallAlert()V

    .line 1071
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissBubbleView(ZZ)V

    .line 1072
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleVideoEvent()V

    .line 1073
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1074
    return-void
.end method

.method private handleVideoEvent()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1146
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cleanUpAvatar()V

    .line 1147
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleVideoEvent: m_cameraPosition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", direction="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", m_callerInitVideoCall="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v2, v2, Lcom/sgiggle/production/CallSession;->m_callerInitVideoCall:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", m_callState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",m_firstVideoEvent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_firstVideoEvent:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1152
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1153
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->resetPipSwapped()V

    .line 1156
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->CameraToType(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_type:I

    .line 1157
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initViews()V

    .line 1158
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    .line 1159
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_1

    .line 1160
    invoke-virtual {p0, v3, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->setFilter(II)V

    .line 1162
    :cond_1
    return-void
.end method

.method private handleVideoModeChangedEvent()V
    .locals 3

    .prologue
    .line 1169
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleVideoModeChangedEvent() cameraPos="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->pipSwapSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    if-ne v0, v1, :cond_1

    .line 1174
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/CallSession;->setPipSwapped(Z)V

    .line 1180
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->CameraToType(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_type:I

    .line 1181
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->onVideoModeChanged()V

    .line 1182
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    .line 1183
    return-void

    .line 1175
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    if-ne v0, v1, :cond_0

    .line 1176
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/CallSession;->setPipSwapped(Z)V

    goto :goto_0
.end method

.method private hangUpCall()V
    .locals 4

    .prologue
    .line 1393
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Terminate call..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1395
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_DISCONNECTING:Lcom/sgiggle/production/CallSession$CallState;

    iput-object v1, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    .line 1398
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$TerminateCallMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1400
    return-void
.end method

.method private hasSwitchCameraTipBubbleDismissed()Z
    .locals 3

    .prologue
    .line 1617
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "switch_camera_bubble_dismissed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private initButtons()V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v5, -0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x4

    .line 419
    const-string v0, "Tango.Video"

    const-string v1, "initButtons"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const v0, 0x7f0a017e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    .line 422
    const v0, 0x7f0a0191

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraLayout:Landroid/widget/RelativeLayout;

    .line 423
    const v0, 0x7f0a019a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    .line 425
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->stopAndRemoveVGoodView()V

    .line 427
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 428
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->bringToFront()V

    .line 429
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 431
    new-instance v0, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;I)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    .line 432
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getVGoodData()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v2, v3, :cond_6

    :cond_0
    move v2, v7

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setData(Ljava/util/List;Z)V

    .line 434
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getEmptySlotCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setEmptySlots(I)V

    .line 436
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setVisibility(I)V

    .line 440
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-ne v0, v7, :cond_7

    .line 441
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x50

    invoke-direct {v0, v8, v5, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 445
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 448
    const v0, 0x7f0a019b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    .line 449
    const v0, 0x7f0a019c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoImage:Landroid/widget/ImageView;

    .line 450
    const v0, 0x7f0a019d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    .line 451
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 453
    const v0, 0x7f0a019e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endButton:Landroid/view/View;

    .line 454
    const v0, 0x7f0a019f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endImage:Landroid/widget/ImageView;

    .line 455
    const v0, 0x7f0a01a0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endIndicator:Landroid/widget/ImageView;

    .line 456
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 458
    const v0, 0x7f0a001a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteButton:Landroid/view/View;

    .line 459
    const v0, 0x7f0a01a1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteImage:Landroid/widget/ImageView;

    .line 460
    const v0, 0x7f0a01a2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    .line 461
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 464
    const v0, 0x7f0a000f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    .line 465
    const v0, 0x7f0a000e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_nameTextView:Landroid/widget/TextView;

    .line 466
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "call state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_8

    move v0, v4

    .line 468
    :goto_2
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 469
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 470
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_nameTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 471
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_nameTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    sget-object v0, Lcom/sgiggle/production/VideoTwoWayActivity$11;->$SwitchMap$com$sgiggle$production$CallSession$CallState:[I

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession$CallState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 486
    :cond_2
    :goto_3
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_9

    .line 487
    const v0, 0x7f02009f

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteResId:I

    .line 488
    const v0, 0x7f02009e

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endResId:I

    .line 489
    const v0, 0x7f0200a0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoResId:I

    .line 490
    const v0, 0x7f0a0195

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    .line 491
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 492
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 494
    const v0, 0x7f0a0196

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    .line 495
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 509
    :goto_4
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_3

    .line 510
    const-string v0, "Tango.Video"

    const-string v1, "initializeView(): Setup debug-info timer..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    const v0, 0x7f0a0197

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    .line 513
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 515
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    if-nez v0, :cond_3

    .line 516
    new-instance v0, Ljava/util/Timer;

    const-string v1, "debugInfo"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    .line 517
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity$DebugInfoTimerTask;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;Lcom/sgiggle/production/VideoTwoWayActivity$1;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 521
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_4

    .line 522
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupSwitchCameraTipBubble()V

    .line 525
    :cond_4
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_5

    .line 526
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 528
    :cond_5
    iput-boolean v7, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isViewInitialized:Z

    .line 529
    iput-boolean v6, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    .line 530
    return-void

    :cond_6
    move v2, v6

    .line 433
    goto/16 :goto_0

    .line 443
    :cond_7
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, 0x5

    invoke-direct {v0, v5, v8, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    goto/16 :goto_1

    :cond_8
    move v0, v6

    .line 467
    goto/16 :goto_2

    .line 474
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 477
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090050

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 480
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_callStatusTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 497
    :cond_9
    const v0, 0x7f020099

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteResId:I

    .line 498
    const v0, 0x7f020098

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endResId:I

    .line 499
    const v0, 0x7f02009a

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoResId:I

    .line 500
    const v0, 0x7f0a01a4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    .line 501
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    const v1, 0x7f090058

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 502
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_lowBandwidthView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 504
    const v0, 0x7f0a01a5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    .line 505
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_alertView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initOnScreenButtons()V
    .locals 2

    .prologue
    .line 1416
    const-string v0, "Tango.Video"

    const-string v1, "initOnScreenButtons"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1417
    const v0, 0x7f0a0170

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    .line 1418
    const v0, 0x7f0a0192

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    .line 1419
    const v0, 0x7f0a005c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    .line 1420
    return-void
.end method

.method private setControlBarAnimation(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1711
    if-eqz p1, :cond_1

    .line 1712
    iput-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    .line 1713
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->updateButtons(I)V

    .line 1719
    :cond_0
    :goto_0
    return-void

    .line 1715
    :cond_1
    iput-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    .line 1716
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    if-nez v0, :cond_0

    .line 1717
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->updateButtons(I)V

    goto :goto_0
.end method

.method private setupSwitchCameraTipBubble()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1574
    const-string v0, "Tango.Video"

    const-string v1, "setupSwitchCameraTipBubble"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1575
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->hasSwitchCameraTipBubbleDismissed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1576
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_1

    move v0, v4

    .line 1577
    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const v0, 0x7f090085

    :goto_1
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    .line 1580
    :cond_0
    return-void

    :cond_1
    move v0, v3

    .line 1576
    goto :goto_0

    .line 1577
    :cond_2
    const v0, 0x7f090086

    goto :goto_1
.end method

.method public static showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1043
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sgiggle/production/Utils;->getStringFromResource(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1044
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090027

    new-instance v2, Lcom/sgiggle/production/VideoTwoWayActivity$8;

    invoke-direct {v2, p1}, Lcom/sgiggle/production/VideoTwoWayActivity$8;-><init>(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 1054
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 1055
    return-void
.end method

.method private showIgnoredCallAlert(Ljava/lang/String;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 223
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090047

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 224
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090048

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 227
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09001f

    new-instance v2, Lcom/sgiggle/production/VideoTwoWayActivity$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$2;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    .line 240
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 241
    return-void
.end method

.method private stopAndRemoveVGoodView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 667
    const-string v0, "VGOOD"

    const-string v1, "stopAndRemoveVGoodView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v0

    if-nez v0, :cond_2

    .line 669
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    if-eqz v0, :cond_0

    .line 670
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setZOrderMediaOverlay(Z)V

    .line 671
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onPause()V

    .line 672
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setVisibility(I)V

    .line 673
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 674
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 680
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    if-eqz v0, :cond_1

    .line 681
    iput-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    .line 682
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mVGoodRenderer:Lcom/sgiggle/cafe/vgood/VGoodRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodRenderer;->stopAnimation()V

    .line 684
    :cond_1
    return-void

    .line 678
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->hideCafe()V

    goto :goto_0
.end method

.method private switchAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V
    .locals 3
    .parameter

    .prologue
    .line 1782
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->switchAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1783
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->switchAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    .line 1784
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1785
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->refreshCurrentPlayingAnimation(J)V

    .line 1787
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1788
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1789
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v1, v2, :cond_3

    :cond_1
    const/4 v1, 0x1

    .line 1790
    :goto_0
    if-eqz v1, :cond_2

    .line 1791
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1793
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v2, v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->enableButtonByTypes(Ljava/util/List;Z)V

    .line 1794
    return-void

    .line 1789
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private switchCamera()V
    .locals 5

    .prologue
    .line 1409
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    if-nez v0, :cond_0

    .line 1410
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchCameraMessage;

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchCameraMessage;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1413
    :cond_0
    return-void
.end method

.method private switchToPortrait()V
    .locals 2

    .prologue
    .line 1028
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isInPortrait:Z

    .line 1029
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_type:I

    .line 1030
    const-string v0, "Tango.Video"

    const-string v1, "change orientation to portrait before show error message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1031
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initViews()V

    .line 1032
    return-void
.end method

.method private throttleSwitchCamera()Z
    .locals 4

    .prologue
    .line 1685
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_timestampOld:J

    sub-long/2addr v0, v2

    .line 1686
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1687
    const/4 v0, 0x1

    .line 1690
    :goto_0
    return v0

    .line 1689
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_timestampOld:J

    .line 1690
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private toggleWandBtn()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1330
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Magic WAND..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1331
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cancelControlBarTimer()V

    .line 1332
    invoke-direct {p0, v2, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissBubbleView(ZZ)V

    .line 1336
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    if-eqz v0, :cond_1

    .line 1370
    :cond_0
    :goto_0
    return-void

    .line 1339
    :cond_1
    invoke-direct {p0, v2}, Lcom/sgiggle/production/VideoTwoWayActivity;->setControlBarAnimation(Z)V

    .line 1342
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 1343
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    .line 1344
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_2

    .line 1345
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutTop:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1346
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    const v1, 0x7f020066

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1363
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bringToFront()V

    .line 1364
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1365
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    goto :goto_0

    .line 1348
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutRight:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1349
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    const v1, 0x7f020065

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 1353
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v3}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setVisibility(I)V

    .line 1354
    iput-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    .line 1355
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_4

    .line 1356
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInTop:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1357
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200de

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1

    .line 1359
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInRight:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1360
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    const v1, 0x7f0200df

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1
.end method

.method private updateButtons()V
    .locals 6

    .prologue
    const v5, 0x7f020009

    const v4, 0x7f020008

    const v3, 0x7f020007

    const v2, 0x7f020006

    .line 1495
    const-string v0, "Tango.Video"

    const-string v1, "updateButtons"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1498
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isViewInitialized:Z

    if-nez v0, :cond_1

    .line 1545
    :cond_0
    :goto_0
    return-void

    .line 1501
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1502
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_muted:Z

    if-eqz v0, :cond_4

    .line 1503
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_3

    .line 1504
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1516
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1517
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_6

    .line 1518
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1523
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoImage:Landroid/widget/ImageView;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoResId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1524
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_8

    .line 1527
    :cond_2
    const-string v0, "Tango.Video"

    const-string v1, "updateButtons, video on"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1528
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_7

    .line 1529
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1542
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_0

    .line 1543
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupSwitchCameraTipBubble()V

    goto :goto_0

    .line 1506
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1509
    :cond_4
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_5

    .line 1510
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1512
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1520
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 1531
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 1534
    :cond_8
    const-string v0, "Tango.Video"

    const-string v1, "updateButtons, video off"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1535
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v0, :cond_9

    .line 1536
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    .line 1538
    :cond_9
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3
.end method

.method private updateOnScreenButtons()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1426
    const-string v0, "Tango.Video"

    const-string v1, "updateOnScreenButtons"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1428
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v1, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v0, v1, :cond_3

    move v0, v3

    .line 1429
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_1

    .line 1430
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_camera_count:I

    if-le v1, v4, :cond_4

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v1, v2, :cond_4

    .line 1433
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1434
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1439
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_callState:Lcom/sgiggle/production/CallSession$CallState;

    sget-object v2, Lcom/sgiggle/production/CallSession$CallState;->CALL_STATE_ACTIVE:Lcom/sgiggle/production/CallSession$CallState;

    if-ne v1, v2, :cond_5

    .line 1440
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 1444
    :goto_2
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_2

    .line 1445
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1446
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1449
    :cond_2
    return-void

    .line 1428
    :cond_3
    const/4 v0, 0x4

    goto :goto_0

    .line 1436
    :cond_4
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 1442
    :cond_5
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_2
.end method

.method private updateVgoodSelectorData()V
    .locals 4

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    if-eqz v0, :cond_1

    .line 1003
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getVGoodData()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v2, v3, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setData(Ljava/util/List;Z)V

    .line 1004
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getEmptySlotCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setEmptySlots(I)V

    .line 1006
    :cond_1
    return-void

    .line 1003
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected CameraToType(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)I
    .locals 1
    .parameter

    .prologue
    .line 1481
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    if-ne p1, v0, :cond_0

    .line 1482
    const/4 v0, 0x1

    .line 1487
    :goto_0
    return v0

    .line 1483
    :cond_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    if-ne p1, v0, :cond_1

    .line 1484
    const/4 v0, 0x2

    goto :goto_0

    .line 1486
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cancelControlBarTimer()V
    .locals 2

    .prologue
    .line 1700
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1701
    return-void
.end method

.method protected cleanUpCafe()V
    .locals 1

    .prologue
    .line 1843
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_cafeInitialised:Z

    if-eqz v0, :cond_0

    .line 1844
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 1845
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 1846
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeEngine()V

    .line 1847
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_cafeInitialised:Z

    .line 1848
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onCafeFreed()V

    .line 1849
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onCafeFreed()V

    .line 1851
    :cond_0
    return-void
.end method

.method public dismissPendingIgnoredCallAlert()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 248
    const-string v0, "Tango.Video"

    const-string v1, "Dismiss the existing Ignored-Call alert..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->clearIgnoredCall()V

    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    .line 253
    :cond_0
    return-void
.end method

.method public handleNewMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 7
    .parameter

    .prologue
    const v6, 0x7f090111

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 796
    const-string v0, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleNewMessage(): Message = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    if-nez p1, :cond_0

    .line 806
    const-string v0, "Tango.Video"

    const-string v1, "handleNewMessage(): message = null. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 999
    :goto_0
    return-void

    .line 810
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 998
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateButtons()V

    goto :goto_0

    .line 813
    :sswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleSendCallInvitationEvent()V

    goto :goto_1

    .line 818
    :sswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleSendCallAcceptedEvent()V

    goto :goto_1

    .line 823
    :sswitch_2
    const-string v0, "Tango.Video"

    const-string v1, "handleNewMessage(): Audio-In-Initialization: Reset call-timer."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 824
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAudioInInitializationEvent()V

    goto :goto_1

    .line 829
    :sswitch_3
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleMissedCallEvent()V

    goto :goto_1

    .line 834
    :sswitch_4
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAudioInProgressEvent()V

    .line 837
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isViewInitialized:Z

    if-eqz v0, :cond_1

    .line 838
    const-string v0, "Tango.Video"

    const-string v1, "ignore updateButton for AUDIO_IN_PROGRESS_EVENT"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 844
    :sswitch_5
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->toggleWandBtn()V

    goto :goto_1

    .line 848
    :sswitch_6
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;

    .line 849
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVgoodSelectorData()V

    .line 851
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioVideoInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload;->getShowWand()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 852
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_handler:Landroid/os/Handler;

    const/16 v1, 0x8

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 853
    sput-boolean v5, Lcom/sgiggle/production/VideoTwoWayActivity;->s_showWand:Z

    .line 855
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleVideoEvent()V

    goto :goto_1

    .line 860
    :sswitch_7
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVgoodSelectorData()V

    .line 861
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleVideoEvent()V

    goto :goto_1

    .line 866
    :sswitch_8
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleBandwidthEvent()V

    goto :goto_1

    .line 870
    :sswitch_9
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleInCallAlertEvent()V

    goto :goto_1

    .line 874
    :sswitch_a
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleVideoModeChangedEvent()V

    goto :goto_1

    .line 879
    :sswitch_b
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleCallDisconnectingEvent()V

    goto :goto_1

    .line 884
    :sswitch_c
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    .line 885
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    goto :goto_1

    .line 890
    :sswitch_d
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 891
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->switchToPortrait()V

    .line 892
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->showDialog(I)V

    .line 897
    invoke-direct {p0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    goto :goto_1

    .line 902
    :sswitch_e
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAnimationEvent(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 906
    :sswitch_f
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;

    .line 907
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-direct {p0, v0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAvatarEvent(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Z)V

    goto/16 :goto_1

    .line 911
    :sswitch_10
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;

    .line 912
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-direct {p0, v0, v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAvatarEvent(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Z)V

    goto/16 :goto_1

    .line 916
    :sswitch_11
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;

    .line 917
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$Audio2WayAvatarInProgressEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-direct {p0, v0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAvatarEvent(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Z)V

    goto/16 :goto_1

    .line 921
    :sswitch_12
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleAvatarRenderRequest(Lcom/sgiggle/messaging/Message;)V

    goto/16 :goto_1

    .line 925
    :sswitch_13
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "VGOOD_ANIMATION_COMPLETE_TYPE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animRestart:Z

    if-eqz v0, :cond_3

    .line 927
    const-string v0, "Tango.Video"

    const-string v1, "skip this animation complete event becuase of restart"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animRestart:Z

    goto/16 :goto_0

    .line 932
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v3}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->updateButtons(I)V

    .line 933
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->restartControlBarTimer()V

    .line 935
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasVGoodAnimation:Z

    if-nez v0, :cond_4

    .line 936
    const-string v0, "Tango.Video"

    const-string v1, "skip this animation complete event. Animation interrupted and stopped already"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 940
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->stopAndRemoveVGoodView()V

    goto/16 :goto_1

    .line 945
    :sswitch_14
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodErrorEvent;

    .line 946
    const-string v2, "Tango.Video"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VGOOD_ERROR_EVENT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VGoodErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$VGoodErrorPayload$Error;

    if-ne v0, v2, :cond_5

    .line 949
    invoke-virtual {p0, v6}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_2
    move-object v0, p0

    move v4, v3

    .line 954
    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto/16 :goto_1

    .line 952
    :cond_5
    const v0, 0x7f090122

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 958
    :sswitch_15
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;

    .line 959
    const-string v2, "Tango.Video"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AVATAR_ERROR_TYPE "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 961
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    if-ne v0, v2, :cond_6

    .line 962
    invoke-virtual {p0, v6}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_3
    move-object v0, p0

    move v4, v3

    .line 969
    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto/16 :goto_1

    .line 963
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;->UNCACHED_BY_LOCAL:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    if-ne v0, v2, :cond_7

    .line 964
    const v0, 0x7f090114

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_3

    .line 967
    :cond_7
    const v0, 0x7f09010a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_3

    .line 973
    :sswitch_16
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;

    .line 974
    const-string v2, "Tango.Video"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GAME_ERROR_EVENT "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 976
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;->UNSUPPORDED_BY_REMOTE:Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    if-ne v0, v2, :cond_8

    .line 977
    const v0, 0x7f090106

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_4
    move-object v0, p0

    move v4, v3

    .line 989
    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V

    goto/16 :goto_1

    .line 978
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;->REMOTE_FAILED:Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    if-ne v0, v2, :cond_9

    .line 979
    const v0, 0x7f090105

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    .line 980
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;->LOCAL_FAILED:Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    if-ne v0, v2, :cond_a

    .line 981
    const v0, 0x7f090108

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    .line 982
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;->REMOTE_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    if-ne v0, v2, :cond_b

    .line 983
    const v0, 0x7f090107

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    .line 986
    :cond_b
    const-string v2, "Tango.Video"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected GAME_ERROR_EVENT error type"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$GameErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$GameErrorPayload$Error;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    const v0, 0x7f090109

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_4

    .line 993
    :sswitch_17
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchAvatarEvent;

    .line 994
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$SwitchAvatarEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->switchAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V

    goto/16 :goto_1

    .line 810
    nop

    :sswitch_data_0
    .sparse-switch
        0x88c7 -> :sswitch_0
        0x88cb -> :sswitch_c
        0x88cd -> :sswitch_1
        0x88cf -> :sswitch_4
        0x88d1 -> :sswitch_6
        0x88d9 -> :sswitch_e
        0x88fd -> :sswitch_7
        0x88ff -> :sswitch_2
        0x8901 -> :sswitch_3
        0x8905 -> :sswitch_8
        0x8907 -> :sswitch_8
        0x8908 -> :sswitch_9
        0x890d -> :sswitch_b
        0x8911 -> :sswitch_a
        0x8973 -> :sswitch_5
        0x897a -> :sswitch_13
        0x8999 -> :sswitch_14
        0x899a -> :sswitch_16
        0x89aa -> :sswitch_f
        0x89ab -> :sswitch_10
        0x89ac -> :sswitch_11
        0x89ad -> :sswitch_12
        0x89b2 -> :sswitch_15
        0x89b3 -> :sswitch_17
        0x89cf -> :sswitch_d
    .end sparse-switch
.end method

.method public hasControlBarAnimation()Z
    .locals 1

    .prologue
    .line 1707
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->hasControlBarAnimation:Z

    return v0
.end method

.method protected initViews()V
    .locals 1

    .prologue
    .line 1186
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initViews(Z)V

    .line 1187
    return-void
.end method

.method protected initViews(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1189
    const-string v0, "Tango.Video"

    const-string v1, "initViews"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isViewInitialized:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumed:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumeFromAvatar:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_2

    .line 1193
    :cond_0
    iput-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumed:Z

    .line 1194
    iput-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumeFromAvatar:Z

    .line 1195
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->initVideoViews()Z

    move-result v0

    .line 1199
    :goto_0
    if-eqz v0, :cond_1

    .line 1200
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initButtons()V

    .line 1201
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initOnScreenButtons()V

    .line 1203
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateButtons()V

    .line 1204
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateOnScreenButtons()V

    .line 1205
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateUserName()V

    .line 1206
    return-void

    .line 1197
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->changeViews()Z

    move-result v0

    goto :goto_0
.end method

.method public onAudioModeUpdated()V
    .locals 0

    .prologue
    .line 1165
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateButtons()V

    .line 1166
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 551
    const-string v0, "Tango.Video"

    const-string v1, "onBackPressed() Ignore the BACK key."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    if-eqz v0, :cond_0

    .line 553
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->toggleWandBtn()V

    .line 554
    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .parameter

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    if-ne p1, v0, :cond_0

    .line 263
    const-string v0, "Tango.Video"

    const-string v1, "onCancel(DialogInterface) Cancel the Ignored-Call dialog..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->clearIgnoredCall()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_ignoredCallAlert:Landroid/app/AlertDialog;

    .line 267
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1283
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_muteButton:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 1285
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Toggle mute button..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v3, v3, Lcom/sgiggle/production/CallSession;->m_muted:Z

    if-nez v3, :cond_1

    move v3, v5

    :goto_0
    invoke-static {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1326
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateButtons()V

    .line 1327
    return-void

    :cond_1
    move v3, v4

    .line 1286
    goto :goto_0

    .line 1290
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_videoButton:Landroid/view/View;

    if-ne p1, v0, :cond_5

    .line 1292
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onClick(): On video-button: direction="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1294
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_4

    .line 1297
    :cond_3
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Remove video..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveVideoMessage;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$RemoveVideoMessage;-><init>(Ljava/lang/String;)V

    .line 1303
    :goto_2
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1304
    invoke-direct {p0, v4, v5}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissBubbleView(ZZ)V

    goto :goto_1

    .line 1300
    :cond_4
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Send video..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1301
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v1}, Lcom/sgiggle/production/CallSession;->getPeerAccountId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AddVideoMessage;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1306
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_endButton:Landroid/view/View;

    if-ne p1, v0, :cond_6

    .line 1308
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->hangUpCall()V

    goto :goto_1

    .line 1310
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_switchCameraButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_8

    .line 1312
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Switch camera..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1315
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->throttleSwitchCamera()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1316
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->switchCamera()V

    goto/16 :goto_1

    .line 1318
    :cond_7
    const-string v0, "Tango.Video"

    const-string v1, "Throttling switch camera action..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1321
    :cond_8
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wandButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 1323
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->toggleWandBtn()V

    goto/16 :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 272
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() savedInstanceState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x200480

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 281
    invoke-direct {p0, v4}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 283
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 285
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayActivity$3;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$3;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->setButtonHandler(Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;)V

    .line 300
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/CallHandler;->getCallSession()Lcom/sgiggle/production/CallSession;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    .line 301
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    .line 302
    const-string v0, "Tango.Video"

    const-string v1, "onCreate(): Call session is null. End this screen."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    invoke-direct {p0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 304
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->finish()V

    .line 306
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_RESUMING:Lcom/sgiggle/production/TangoApp$AppState;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setAppRunningState(Lcom/sgiggle/production/TangoApp$AppState;)V

    .line 337
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setVideoActivityInstance(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    .line 312
    new-instance v0, Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/receiver/WifiLockReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    .line 314
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->setVolumeControlStream(I)V

    .line 317
    const-string v0, "Tango.Video"

    invoke-virtual {p0, v0, v3}, Lcom/sgiggle/production/VideoTwoWayActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_prefs:Landroid/content/SharedPreferences;

    .line 319
    new-instance v0, Lcom/sgiggle/production/avatar/AvatarRenderer;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;-><init>(IZLcom/sgiggle/production/CallSession;Z)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    .line 320
    new-instance v0, Lcom/sgiggle/production/avatar/AvatarRenderer;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v2

    invoke-direct {v0, v4, v4, v1, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;-><init>(IZLcom/sgiggle/production/CallSession;Z)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    .line 322
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleNewMessage(Lcom/sgiggle/messaging/Message;)V

    .line 324
    const v0, 0x7f04000a

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInRight:Landroid/view/animation/Animation;

    .line 325
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutRight:Landroid/view/animation/Animation;

    .line 326
    const v0, 0x7f04000b

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInTop:Landroid/view/animation/Animation;

    .line 327
    const v0, 0x7f040010

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutTop:Landroid/view/animation/Animation;

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInRight:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 330
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutTop:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 331
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideInTop:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 332
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mAnimationSlideOutRight:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->animationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 334
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/payments/BillingServiceManager;->checkBillingSupport(Landroid/content/Context;)V

    .line 336
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->enableKeyguard()V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 724
    .line 726
    packed-switch p1, :pswitch_data_0

    .line 734
    const/4 v0, 0x0

    .line 737
    :goto_0
    return-object v0

    .line 728
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->createCallOnHoldDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 731
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->createLeaveMessageDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 726
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 719
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 703
    const-string v0, "Tango.Video"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 707
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cleanUpCafe()V

    .line 708
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setActivityHandler(Landroid/os/Handler;)V

    .line 709
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissPendingIgnoredCallAlert()V

    .line 710
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->clearVideoActivityInstance(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    .line 713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isDestroyed:Z

    .line 714
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 715
    return-void
.end method

.method public onGameInCallStart()V
    .locals 1

    .prologue
    .line 1839
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_canCleanCafeOnPause:Z

    .line 1840
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 623
    const-string v0, "Tango.Video"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 625
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 626
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->animationOnPause()V

    .line 627
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->avatarOnPause()V

    .line 634
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_canCleanCafeOnPause:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 635
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cleanUpCafe()V

    .line 637
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    if-eqz v0, :cond_2

    .line 638
    const-string v0, "Tango.Video"

    const-string v1, "onPause(): Cancel debug-info timer...."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 640
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoTimer:Ljava/util/Timer;

    .line 642
    :cond_2
    return-void
.end method

.method protected onRestart()V
    .locals 4

    .prologue
    .line 571
    const-string v0, "Tango.Video"

    const-string v1, "onRestart()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onRestart()V

    .line 573
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v3, v3, Lcom/sgiggle/production/CallSession;->m_speakerOn:Z

    invoke-static {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;->createMessage(IZ)Lcom/sgiggle/media_engine/MediaEngineMessage$AudioControlMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 577
    return-void
.end method

.method protected onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 581
    const-string v0, "Tango.Video"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getAppRunningState()Lcom/sgiggle/production/TangoApp$AppState;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v0, v1, :cond_4

    move v0, v3

    .line 583
    :goto_0
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 584
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->animationOnResume()V

    .line 585
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->isSupported()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_canCleanCafeOnPause:Z

    .line 586
    iget-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_cafeInitialised:Z

    if-nez v1, :cond_0

    .line 587
    invoke-static {p0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->InitEngine(Landroid/content/Context;)V

    .line 588
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetCallbacks()V

    .line 589
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 590
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_cafeInitialised:Z

    .line 591
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onCafeInitialized()V

    .line 592
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onCafeInitialized()V

    .line 595
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.VoIP_BLUETOOTH"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 596
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_btBtnReceiver:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/sgiggle/production/VideoTwoWayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 598
    iput-boolean v3, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_resumed:Z

    .line 599
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v1, v2, :cond_6

    .line 600
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->handleVideoEvent()V

    .line 601
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    .line 612
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-boolean v0, v0, Lcom/sgiggle/production/CallSession;->m_callOnHold:Z

    if-eqz v0, :cond_2

    .line 613
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/VideoTwoWayActivity;->showDialog(I)V

    .line 616
    :cond_2
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_isInPortrait:Z

    if-eqz v0, :cond_3

    .line 617
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->switchToPortrait()V

    .line 619
    :cond_3
    return-void

    :cond_4
    move v0, v4

    .line 582
    goto :goto_0

    :cond_5
    move v1, v4

    .line 585
    goto :goto_1

    .line 605
    :cond_6
    if-nez v0, :cond_1

    .line 606
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->avatarOnResume()V

    .line 607
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    goto :goto_2
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 558
    const-string v0, "Tango.Video"

    const-string v1, "onStart()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 560
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 561
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 562
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 564
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 565
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 567
    return-void
.end method

.method protected onStop()V
    .locals 3

    .prologue
    .line 688
    const-string v0, "Tango.Video"

    const-string v1, "onStop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStop()V

    .line 690
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->dismissKeyguard(Z)V

    .line 692
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/receiver/WifiLockReceiver;->releaseWifiLock(Landroid/content/Context;)V

    .line 694
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_wifiLockReceiver:Lcom/sgiggle/production/receiver/WifiLockReceiver;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 699
    :goto_0
    return-void

    .line 695
    :catch_0
    move-exception v0

    .line 696
    const-string v1, "Tango.Video"

    const-string v2, "got exception for unregisterReceiver()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public restartControlBarTimer()V
    .locals 0

    .prologue
    .line 1695
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->cancelControlBarTimer()V

    .line 1696
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->startControlBarTimer()V

    .line 1697
    return-void
.end method

.method public setFilter(II)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1871
    return-void
.end method

.method protected setFilterHightlightWithBackground()V
    .locals 0

    .prologue
    .line 1874
    return-void
.end method

.method public setupWarningBubble(Ljava/lang/String;Ljava/lang/String;ZZZ)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const v6, 0x7f0a0199

    const v5, 0x7f0a0036

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1621
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setupBubble() m_orientation == Orientation.LANDSCAPE? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_orientation:I

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1622
    if-eqz p3, :cond_4

    .line 1623
    const v0, 0x7f0a0154

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    .line 1624
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayActivity$9;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$9;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1637
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0022

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1638
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1640
    if-nez p3, :cond_0

    .line 1641
    if-eqz p4, :cond_5

    .line 1642
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1643
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1650
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0a0023

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1651
    if-eqz p1, :cond_6

    .line 1652
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1653
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1654
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1655
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setupBubble() bubbleTextTitle.setVisibility(View.VISIBLE) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1661
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0a0024

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1662
    if-eqz p2, :cond_7

    .line 1663
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1664
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1665
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setupBubble() bubbleTextBody.setVisibility(View.VISIBLE) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 1671
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1672
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    .line 1675
    :cond_1
    if-eqz p5, :cond_2

    .line 1676
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1677
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    const-wide/16 v1, 0x1388

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1679
    :cond_2
    return-void

    :cond_3
    move v2, v3

    .line 1621
    goto/16 :goto_0

    .line 1630
    :cond_4
    const v0, 0x7f0a0198

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    .line 1631
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayActivity$10;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/VideoTwoWayActivity$10;-><init>(Lcom/sgiggle/production/VideoTwoWayActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 1645
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1646
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_bubbleLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 1657
    :cond_6
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1658
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 1667
    :cond_7
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method public startControlBarTimer()V
    .locals 4

    .prologue
    .line 1703
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->uihandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1704
    return-void
.end method

.method protected updateUserName()V
    .locals 2

    .prologue
    .line 1855
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1856
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 1857
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1858
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1859
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_peerName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1868
    :cond_0
    :goto_0
    return-void

    .line 1860
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1861
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_localName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1865
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_userNameTxt:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateVGoodBar()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1550
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1551
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v1}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->showCurrentPlayingAnimation(J)V

    .line 1556
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_2

    .line 1557
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1558
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->AVATAR:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1559
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v1, v2, :cond_5

    :cond_0
    const/4 v1, 0x1

    .line 1560
    :goto_1
    if-eqz v1, :cond_1

    .line 1561
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1563
    :cond_1
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v2, v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->enableButtonByTypes(Ljava/util/List;Z)V

    .line 1566
    :cond_2
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_showVgoodBar:Z

    if-eqz v0, :cond_3

    .line 1567
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bringToFront()V

    .line 1568
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0, v3}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setVisibility(I)V

    .line 1569
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->restartControlBarTimer()V

    .line 1571
    :cond_3
    return-void

    .line 1554
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->hideCurrentPlayingAnimation()V

    goto :goto_0

    :cond_5
    move v1, v3

    .line 1559
    goto :goto_1
.end method
