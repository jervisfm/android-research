.class Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;
.super Ljava/lang/Object;
.source "VideoTwoWayCanvasActivity.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->createAvatarGLView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 925
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 928
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAvatarSurfaceView on touch "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v5, :cond_1

    .line 930
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 931
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 932
    const-string v2, "Tango.Video"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "touch on "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 933
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    iget v2, v2, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v2

    .line 934
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 935
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    iget-object v0, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->pipSwapSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 936
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    #calls: Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapAvatar()V
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->access$800(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V

    :cond_0
    move v0, v5

    .line 941
    :goto_0
    return v0

    :cond_1
    move v0, v5

    goto :goto_0
.end method
