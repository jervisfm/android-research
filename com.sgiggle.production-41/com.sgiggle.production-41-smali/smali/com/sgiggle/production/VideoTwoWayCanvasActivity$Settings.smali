.class Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;
.super Ljava/lang/Object;
.source "VideoTwoWayCanvasActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/VideoTwoWayCanvasActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Settings"
.end annotation


# instance fields
.field private m_borderRectIndex:I

.field private m_camera_size:I

.field private m_camera_type:I

.field private m_camera_view_index:I

.field private m_isH264Renderer:Z

.field private m_renderer_size:I

.field private m_renderer_view_index:I

.field private m_requestedOrientation:I

.field final synthetic this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 63
    iput-object p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    .line 65
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    .line 66
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    .line 67
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    .line 68
    const/4 v0, 0x6

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    .line 69
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    .line 70
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    return v0
.end method

.method static synthetic access$102(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    return v0
.end method

.method static synthetic access$202(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    return p1
.end method

.method static synthetic access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    return v0
.end method

.method static synthetic access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    return p1
.end method

.method static synthetic access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    return v0
.end method

.method static synthetic access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    return p1
.end method

.method static synthetic access$500(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    return v0
.end method

.method static synthetic access$502(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    return p1
.end method

.method static synthetic access$602(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    return p1
.end method

.method static synthetic access$700(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    return v0
.end method

.method static synthetic access$702(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53
    iput p1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    return p1
.end method


# virtual methods
.method public clone()Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->this$0:Lcom/sgiggle/production/VideoTwoWayCanvasActivity;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;-><init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V

    .line 76
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    .line 77
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    .line 78
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    .line 79
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    .line 80
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    .line 81
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    .line 82
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    iput v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    .line 83
    iget-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    iput-boolean v1, v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    .line 84
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->clone()Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    move-result-object v0

    return-object v0
.end method

.method public equals(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 88
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    if-eq v0, v1, :cond_0

    move v0, v2

    .line 97
    :goto_0
    return v0

    .line 89
    :cond_0
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    if-eq v0, v1, :cond_1

    move v0, v2

    goto :goto_0

    .line 90
    :cond_1
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    if-eq v0, v1, :cond_2

    move v0, v2

    goto :goto_0

    .line 91
    :cond_2
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    if-eq v0, v1, :cond_3

    move v0, v2

    goto :goto_0

    .line 92
    :cond_3
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    if-eq v0, v1, :cond_4

    move v0, v2

    goto :goto_0

    .line 93
    :cond_4
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    if-eq v0, v1, :cond_5

    move v0, v2

    goto :goto_0

    .line 94
    :cond_5
    iget v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    if-eq v0, v1, :cond_6

    move v0, v2

    goto :goto_0

    .line 95
    :cond_6
    iget-boolean v0, p1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    iget-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    if-eq v0, v1, :cond_7

    move v0, v2

    goto :goto_0

    .line 97
    :cond_7
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public print()V
    .locals 3

    .prologue
    .line 101
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "equals "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void
.end method
