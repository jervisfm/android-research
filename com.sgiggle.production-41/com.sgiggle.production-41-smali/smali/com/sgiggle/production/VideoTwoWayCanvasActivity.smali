.class public Lcom/sgiggle/production/VideoTwoWayCanvasActivity;
.super Lcom/sgiggle/production/VideoTwoWayActivity;
.source "VideoTwoWayCanvasActivity.java"

# interfaces
.implements Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;
    }
.end annotation


# instance fields
.field private final BUTTON_BACKGROUND:I

.field private final CAMERA_BACK_BIG:I

.field private final CAMERA_BACK_SMALL:I

.field private final CAMERA_FRONT_BIG:I

.field private final CAMERA_FRONT_SMALL:I

.field private final RENDERER_BIG:I

.field private final RENDERER_SMALL:I

.field private mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

.field private m_backgrounds:[Landroid/widget/ImageView;

.field private m_border:Landroid/widget/RelativeLayout;

.field private m_borderLayout:Landroid/widget/RelativeLayout;

.field private m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

.field private m_holders:[Landroid/view/SurfaceHolder;

.field private m_htcNoSwap:Z

.field private m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

.field private m_rendererRect:Landroid/graphics/Rect;

.field private m_transparentRect:Landroid/graphics/Rect;

.field private m_viewIndex:[I

.field private m_views:[Landroid/view/SurfaceView;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x6

    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->CAMERA_BACK_BIG:I

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->CAMERA_FRONT_BIG:I

    .line 35
    const/4 v0, 0x2

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->CAMERA_BACK_SMALL:I

    .line 36
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->CAMERA_FRONT_SMALL:I

    .line 37
    const/4 v0, 0x4

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->RENDERER_BIG:I

    .line 38
    const/4 v0, 0x5

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->RENDERER_SMALL:I

    .line 41
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->BUTTON_BACKGROUND:I

    .line 43
    new-array v0, v1, [Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    .line 44
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    .line 45
    new-array v0, v1, [Landroid/view/SurfaceHolder;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    .line 47
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    .line 113
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;-><init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    .line 114
    new-instance v0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;-><init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    .line 119
    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    return-void
.end method

.method static synthetic access$800(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapAvatar()V

    return-void
.end method

.method private addAvatarViewAndBringToFront()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 948
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_1

    .line 988
    :cond_0
    :goto_0
    return-void

    .line 951
    :cond_1
    const-string v0, "Tango.Video"

    const-string v1, "addAvatarViewAndBringToFront"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 952
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    if-nez v0, :cond_2

    .line 953
    const-string v0, "Tango.Video"

    const-string v1, "add surface view called before initialization"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 959
    :cond_2
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 960
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 961
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout;->measure(II)V

    .line 963
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    if-nez v1, :cond_4

    .line 964
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 965
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v0, v2

    move v6, v1

    move v1, v0

    move v0, v6

    .line 974
    :goto_1
    const-string v2, "Tango.Video"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "avatar surface view x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " y "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " w "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " h "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 975
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 976
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 977
    iput v5, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 978
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 979
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 980
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setZOrderMediaOverlay(Z)V

    .line 981
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_switchCameraLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 982
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 983
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_showVgoodBar:Z

    if-eqz v0, :cond_3

    .line 984
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->bringToFront()V

    .line 985
    :cond_3
    sget-boolean v0, Lcom/sgiggle/production/TangoApp;->g_screenLoggerEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_debugInfoView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 986
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_debugInfoView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->bringToFront()V

    goto/16 :goto_0

    .line 967
    :cond_4
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_controlsLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 968
    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move v6, v1

    move v1, v0

    move v0, v6

    goto/16 :goto_1
.end method

.method private bringPIPVideoToFront()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 215
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bringPIPToFront "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/SurfaceView;->bringToFront()V

    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 224
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 227
    :cond_1
    return-void

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/SurfaceView;->bringToFront()V

    .line 222
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0, v3}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    goto :goto_0
.end method

.method private createAvatarGLView()V
    .locals 2

    .prologue
    .line 923
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Reset()V

    .line 924
    new-instance v0, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 925
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    new-instance v1, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$1;-><init>(Lcom/sgiggle/production/VideoTwoWayCanvasActivity;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 944
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onResume()V

    .line 945
    return-void
.end method

.method private getCameraViewIndex(II)I
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 744
    const/4 v0, -0x1

    .line 745
    if-ne p2, v1, :cond_2

    .line 746
    if-nez p1, :cond_1

    .line 747
    const/4 v0, 0x0

    .line 756
    :cond_0
    :goto_0
    return v0

    .line 748
    :cond_1
    if-ne p1, v1, :cond_0

    move v0, v2

    .line 749
    goto :goto_0

    .line 750
    :cond_2
    if-ne p2, v2, :cond_0

    .line 751
    if-nez p1, :cond_3

    move v0, v1

    .line 752
    goto :goto_0

    .line 753
    :cond_3
    if-ne p1, v1, :cond_0

    .line 754
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private getRectangleIndex(I)I
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 760
    const/4 v0, -0x1

    .line 764
    if-nez p1, :cond_1

    move v0, v2

    .line 778
    :cond_0
    :goto_0
    return v0

    .line 766
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    move v0, v3

    .line 767
    goto :goto_0

    .line 768
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 769
    const/4 v0, 0x7

    goto :goto_0

    .line 770
    :cond_3
    if-ne p1, v2, :cond_4

    .line 771
    const/16 v0, 0x8

    goto :goto_0

    .line 772
    :cond_4
    if-ne p1, v3, :cond_5

    move v0, v4

    .line 773
    goto :goto_0

    .line 774
    :cond_5
    if-ne p1, v4, :cond_0

    .line 775
    const/16 v0, 0x9

    goto :goto_0
.end method

.method private getRendererViewIndex(I)I
    .locals 2
    .parameter

    .prologue
    .line 787
    const/4 v0, -0x1

    .line 788
    if-nez p1, :cond_1

    .line 789
    const/4 v0, 0x4

    .line 792
    :cond_0
    :goto_0
    return v0

    .line 790
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 791
    const/4 v0, 0x5

    goto :goto_0
.end method

.method private getVideoViewIndex(I)I
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 801
    const/4 v0, -0x1

    .line 802
    if-nez p1, :cond_0

    move v0, v2

    .line 816
    :goto_0
    return v0

    .line 804
    :cond_0
    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    move v0, v3

    .line 805
    goto :goto_0

    .line 806
    :cond_1
    const/4 v1, 0x2

    if-ne p1, v1, :cond_2

    .line 807
    const/4 v0, 0x7

    goto :goto_0

    .line 808
    :cond_2
    if-ne p1, v2, :cond_3

    .line 809
    const/16 v0, 0x8

    goto :goto_0

    .line 810
    :cond_3
    if-ne p1, v3, :cond_4

    move v0, v4

    .line 811
    goto :goto_0

    .line 812
    :cond_4
    if-ne p1, v4, :cond_5

    .line 813
    const/16 v0, 0x9

    goto :goto_0

    .line 815
    :cond_5
    const-string v1, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVideoViewIndex wrong index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private hideView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/16 v5, 0x8

    .line 449
    const-string v0, "Tango.Video"

    const-string v1, "hideView"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_2

    .line 454
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    .line 457
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    if-ne v1, v7, :cond_3

    .line 458
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    .line 462
    :goto_1
    const-string v2, "Tango.Video"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hideView camera_view_ind="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " renderer_view_ind="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " big="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " small="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    if-eq v1, v6, :cond_0

    .line 467
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v2, v1

    invoke-virtual {v1, v5}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 468
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 471
    :cond_0
    if-eq v0, v6, :cond_1

    .line 472
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v1, v0

    invoke-virtual {v0, v5}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 474
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 475
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 489
    :cond_1
    return-void

    .line 455
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_5

    .line 456
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    goto/16 :goto_0

    .line 459
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    if-ne v1, v7, :cond_4

    .line 460
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    goto/16 :goto_1

    :cond_4
    move v1, v6

    goto/16 :goto_1

    :cond_5
    move v0, v6

    goto/16 :goto_0
.end method

.method private initBackground()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 678
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_1

    .line 679
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    if-ne v0, v1, :cond_0

    .line 680
    const/16 v0, 0xf

    move v1, v0

    .line 692
    :goto_0
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    const/4 v0, 0x6

    if-ge v2, v0, :cond_3

    .line 694
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    add-int v3, v1, v2

    invoke-static {v0, v3}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v3

    .line 695
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    const/high16 v4, -0x100

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 696
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 697
    iget v4, v3, Landroid/graphics/Rect;->left:I

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 698
    iget v4, v3, Landroid/graphics/Rect;->top:I

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 699
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    iput v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 700
    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 701
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 692
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 682
    :cond_0
    const/16 v0, 0x15

    move v1, v0

    goto :goto_0

    .line 683
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_3

    .line 684
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    if-ne v0, v1, :cond_2

    .line 685
    const/16 v0, 0x1b

    move v1, v0

    goto :goto_0

    .line 687
    :cond_2
    const/16 v0, 0x21

    move v1, v0

    goto :goto_0

    .line 703
    :cond_3
    return-void
.end method

.method private initBorder()V
    .locals 4

    .prologue
    .line 664
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$700(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->getBorderRect(II)Landroid/graphics/Rect;

    move-result-object v1

    .line 666
    const-string v0, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initBorder: borderRect="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 669
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 670
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 671
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 672
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 673
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 674
    return-void
.end method

.method private initButtonBackground()V
    .locals 0

    .prologue
    .line 735
    return-void
.end method

.method private initCameraView(I)V
    .locals 3
    .parameter

    .prologue
    .line 624
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getVideoViewIndex(I)I

    move-result v0

    .line 628
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v1, v1, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v1, v2, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 631
    :cond_0
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    const/16 v1, 0xa

    invoke-static {v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v0

    move-object v1, v0

    .line 635
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 636
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 637
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 638
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 639
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 640
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 641
    return-void

    .line 633
    :cond_1
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private initRendererView(I)V
    .locals 4
    .parameter

    .prologue
    .line 644
    invoke-direct {p0, p1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getVideoViewIndex(I)I

    move-result v0

    .line 645
    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v1

    .line 647
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 648
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 649
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 650
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 651
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 653
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    .line 655
    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/sgiggle/VideoCapture/VideoView;->getRect(II)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_transparentRect:Landroid/graphics/Rect;

    .line 657
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_rendererRect:Landroid/graphics/Rect;

    .line 660
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, p1

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 661
    return-void
.end method

.method private initViewId()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 496
    const-string v0, "Tango.Video"

    const-string v1, "initViewId"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0180

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v6

    .line 498
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0181

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v9

    .line 499
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v2, 0x2

    const v0, 0x7f0a0183

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v2

    .line 500
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0184

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v5

    .line 501
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0182

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v7

    .line 502
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0185

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v8

    .line 503
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    if-nez v0, :cond_0

    .line 504
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    .line 505
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 506
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v2, v2, v6

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    aput v2, v1, v6

    .line 507
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v2, v2, v9

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    aput v2, v1, v9

    .line 508
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    aput v3, v1, v2

    .line 509
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v2, v2, v5

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    aput v2, v1, v5

    .line 510
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v2, v2, v7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    aput v2, v1, v7

    .line 511
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v2, v2, v8

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    aput v0, v1, v8

    .line 513
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const v0, 0x7f0a0189

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v6

    .line 514
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const v0, 0x7f0a018a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v9

    .line 515
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v2, 0x2

    const v0, 0x7f0a018b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 516
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const v0, 0x7f0a018c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v5

    .line 517
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const v0, 0x7f0a018d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v7

    .line 518
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const v0, 0x7f0a018e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v8

    .line 519
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v2, 0x6

    const v0, 0x7f0a018f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    aput-object v0, v1, v2

    .line 520
    const v0, 0x7f0a0186

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    .line 521
    const v0, 0x7f0a0187

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    .line 528
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v6

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v6

    .line 529
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v6

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 530
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v6

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 532
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v9

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v9

    .line 533
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v9

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 534
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v9

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 536
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    aput-object v2, v0, v1

    .line 537
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 538
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 540
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v5

    .line 541
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v5

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 542
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v5

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 544
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v7

    .line 545
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v7

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 547
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v7

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 557
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v8

    .line 558
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v8

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 559
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 561
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v8

    invoke-interface {v0, v5}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 567
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v8

    invoke-virtual {v0, v9}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 569
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 573
    return-void

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v7

    invoke-interface {v0, v6}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 554
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v7

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0

    .line 565
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v8

    invoke-interface {v0, v6}, Landroid/view/SurfaceHolder;->setType(I)V

    goto :goto_1
.end method

.method private initViewSize()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 231
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-static {}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->isH264Renderer()Z

    move-result v1

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$002(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;Z)Z

    .line 232
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoView;->setIsH264Renderer(Z)V

    .line 233
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 234
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 236
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initViewSize vide "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " avatar direction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_3

    .line 238
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 239
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 249
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->isPipSwapped()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 250
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapViewSize()V

    .line 252
    :cond_1
    return-void

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    goto :goto_0

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_4

    .line 244
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0, v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    goto :goto_0

    .line 245
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_0

    .line 246
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 247
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0, v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    goto :goto_0
.end method

.method private pauseAndRemoveAvatarView()V
    .locals 2

    .prologue
    .line 910
    const-string v0, "Tango.Video"

    const-string v1, "pauseAndRemoveAvatarView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setZOrderMediaOverlay(Z)V

    .line 913
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->onPause()V

    .line 914
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->setVisibility(I)V

    .line 915
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_frameLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 916
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    .line 917
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onSurfaceDestroyed()V

    .line 918
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onSurfaceDestroyed()V

    .line 920
    :cond_0
    return-void
.end method

.method private reinitVideoAvatarViews()V
    .locals 2

    .prologue
    .line 1041
    const-string v0, "Tango.Video"

    const-string v1, "reinitVideoAvatarViews"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1042
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->pauseAndRemoveAvatarView()V

    .line 1043
    const/4 v0, 0x1

    invoke-super {p0, v0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initViews(Z)V

    .line 1044
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->createAvatarGLView()V

    .line 1045
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->addAvatarViewAndBringToFront()V

    .line 1046
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->resetAvatarRenderersList()V

    .line 1048
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1050
    const-string v0, "Tango.Video"

    const-string v1, "bring pip video to front"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->bringPIPVideoToFront()V

    .line 1053
    :cond_0
    return-void
.end method

.method private resetAvatarRenderersList()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 992
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->clearCafeViewRenderers()V

    .line 994
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needDrawPIPVideoBg()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 996
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 997
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    .line 998
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 999
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 1000
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    .line 1024
    :goto_0
    return-void

    .line 1002
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1005
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 1006
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 1007
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    .line 1008
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 1009
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 1010
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    goto :goto_0

    .line 1012
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1015
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 1016
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 1017
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    .line 1018
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mAvatarSurfaceView:Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeViewForCanvasRenderer;->getRenderer()Lcom/sgiggle/cafe/vgood/CafeRenderer;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeRenderer;->addCafeViewRenderer(Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;)V

    .line 1019
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setUpdate(Z)V

    .line 1020
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setClear(Z)V

    goto/16 :goto_0

    .line 1022
    :cond_4
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "render status are error, avatar direction = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private screenOrientationToViewOrientation(I)I
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 857
    if-nez p1, :cond_1

    .line 858
    const/4 v0, 0x0

    .line 862
    :cond_0
    :goto_0
    return v0

    .line 859
    :cond_1
    if-eq p1, v0, :cond_0

    .line 861
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "screenOrientationToViewOrientation unexpected orientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private setOrientation()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 271
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    if-ne v0, v2, :cond_0

    .line 272
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoView;->getOrientation(I)I

    move-result v0

    .line 277
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->viewOrientationToScreenOrientation(I)I

    move-result v2

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I
    invoke-static {v1, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$502(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 278
    const-string v1, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setOrientation() m_camera_type is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " orientation is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " m_requestedOrientation is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I
    invoke-static {v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$500(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "LANDSCAPE"

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_type:I
    invoke-static {v0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$602(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 281
    return-void

    .line 273
    :cond_0
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_camera_type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 274
    invoke-static {v2}, Lcom/sgiggle/VideoCapture/VideoView;->getOrientation(I)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 276
    goto :goto_0

    .line 278
    :cond_2
    const-string v2, "PORTRAIT"

    goto :goto_1
.end method

.method private setViews(Z)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 289
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setViews "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v2, v2, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setOrientation()V

    .line 292
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->equals(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 297
    :cond_0
    const-string v0, "Tango.Video"

    const-string v1, "setViews: no configuration change, returning"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    .line 322
    :goto_0
    return v0

    .line 300
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->hideView()V

    .line 306
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$500(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$500(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_requestedOrientation:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$500(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setRequestedOrientation(I)V

    .line 308
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getRequestedOrientation()I

    move-result v0

    .line 309
    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->screenOrientationToViewOrientation(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    .line 310
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "orientation changed to  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    if-nez v2, :cond_3

    const-string v2, "LANDSCAPE"

    :goto_1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_orientation:I

    if-nez v0, :cond_4

    const v0, 0x7f03006f

    :goto_2
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setContentView(I)V

    .line 313
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initViewId()V

    move v0, v3

    .line 318
    :goto_3
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->updateLayout()V

    .line 319
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->showView()V

    .line 320
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    invoke-virtual {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->clone()Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    goto :goto_0

    .line 310
    :cond_3
    const-string v2, "PORTRAIT"

    goto :goto_1

    .line 311
    :cond_4
    const v0, 0x7f030070

    goto :goto_2

    :cond_5
    move v0, v4

    .line 316
    goto :goto_3
.end method

.method private showView()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 361
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_cameraPosition:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->CameraToType(Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;)I

    move-result v0

    .line 362
    const-string v1, "Tango.Video"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showView camera_size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " renderer_size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " camera_type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v2

    invoke-direct {p0, v2, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getCameraViewIndex(II)I

    move-result v0

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v1, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$102(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 365
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getRendererViewIndex(I)I

    move-result v1

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v0, v1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$202(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 369
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_5

    .line 370
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    move v1, v0

    .line 373
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-ne v0, v7, :cond_6

    .line 374
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    move v2, v0

    .line 378
    :goto_1
    const-string v0, "Tango.Video"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "showView camera_view_ind="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_view_index:I
    invoke-static {v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$100(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " renderer_view_ind="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " big="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " small="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    if-eq v1, v6, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 388
    :cond_0
    if-eq v2, v6, :cond_8

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 392
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    aget v4, v4, v2

    if-eq v3, v4, :cond_1

    .line 393
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 394
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_viewIndex:[I

    aget v4, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 395
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    invoke-virtual {v0, v5}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 403
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 404
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Landroid/view/SurfaceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 406
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-ne v2, v0, :cond_7

    :cond_3
    invoke-direct {p0, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->getRectangleIndex(I)I

    move-result v0

    :goto_2
    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->updateBorder(I)V

    .line 407
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 414
    :goto_3
    if-eq v1, v6, :cond_4

    .line 415
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBackground()V

    .line 416
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v0, v0, v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 417
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 419
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 421
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_backgrounds:[Landroid/widget/ImageView;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 442
    :cond_4
    return-void

    .line 371
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_a

    .line 372
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    move v1, v0

    goto/16 :goto_0

    .line 375
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-ne v0, v7, :cond_9

    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_view_index:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$200(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    move v2, v0

    goto/16 :goto_1

    .line 406
    :cond_7
    const/16 v0, 0xa

    goto :goto_2

    .line 410
    :cond_8
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_border:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_3

    :cond_9
    move v2, v6

    goto/16 :goto_1

    :cond_a
    move v1, v6

    goto/16 :goto_0
.end method

.method private swapAvatar()V
    .locals 2

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->swapPip()V

    .line 1029
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_0

    .line 1030
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->resetAvatarRenderersList()V

    .line 1031
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setRenderView()V

    .line 1032
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setRenderView()V

    .line 1036
    :goto_0
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateVGoodBar()V

    .line 1037
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateUserName()V

    .line 1038
    return-void

    .line 1034
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->reinitVideoAvatarViews()V

    goto :goto_0
.end method

.method private swapView()V
    .locals 2

    .prologue
    .line 351
    const-string v0, "Tango.Video"

    const-string v1, "swapView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->swapPip()V

    .line 353
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapViewSize()V

    .line 354
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setViews(Z)Z

    .line 355
    return-void
.end method

.method private swapViewSize()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 255
    const-string v0, "Tango.Video"

    const-string v1, "swapViewSize"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_2

    .line 257
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 262
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-nez v0, :cond_3

    .line 263
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0, v2}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 267
    :cond_1
    :goto_1
    return-void

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$300(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_camera_size:I
    invoke-static {v0, v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$302(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    goto :goto_0

    .line 264
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$400(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 265
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_renderer_size:I
    invoke-static {v0, v3}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$402(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    goto :goto_1
.end method

.method private updateBorder(I)V
    .locals 1
    .parameter

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #setter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_borderRectIndex:I
    invoke-static {v0, p1}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$702(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;I)I

    .line 602
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBorder()V

    .line 603
    return-void
.end method

.method private viewOrientationToScreenOrientation(I)I
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 843
    if-nez p1, :cond_1

    .line 844
    const/4 v0, 0x0

    .line 848
    :cond_0
    :goto_0
    return v0

    .line 845
    :cond_1
    if-eq p1, v0, :cond_0

    .line 847
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "viewOrientationToScreenOrientation unexpected orientation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 848
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public bringToFront()V
    .locals 2

    .prologue
    .line 208
    const-string v0, "Tango.Video"

    const-string v1, "bringToFront"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_borderLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->bringToFront()V

    .line 212
    :cond_0
    return-void
.end method

.method public changeViews()Z
    .locals 2

    .prologue
    .line 341
    const-string v0, "Tango.Video"

    const-string v1, "changeViews"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initViewSize()V

    .line 343
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setViews(Z)Z

    move-result v0

    return v0
.end method

.method public hideCafe()V
    .locals 0

    .prologue
    .line 873
    return-void
.end method

.method public initVideoViews()Z
    .locals 2

    .prologue
    .line 330
    const-string v0, "Tango.Video"

    const-string v1, "VideoTwoWayCanvasActivity.initViews()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initViewSize()V

    .line 332
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->setViews(Z)Z

    move-result v0

    return v0
.end method

.method public onAvatarChanged()V
    .locals 0

    .prologue
    .line 882
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->reinitVideoAvatarViews()V

    .line 883
    return-void
.end method

.method public onAvatarPaused()V
    .locals 0

    .prologue
    .line 887
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->pauseAndRemoveAvatarView()V

    .line 888
    return-void
.end method

.method public onAvatarResumed()V
    .locals 0

    .prologue
    .line 892
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->reinitVideoAvatarViews()V

    .line 893
    return-void
.end method

.method public onCleanUpAvatar()V
    .locals 0

    .prologue
    .line 897
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->pauseAndRemoveAvatarView()V

    .line 898
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 140
    const-string v0, "Tango.Video"

    const-string v1, "VideoTwoWayCanvasActivity.onClick"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    .line 144
    :cond_0
    const-string v0, "Tango.Video"

    const-string v1, "onClick(): Swap view..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_htcNoSwap:Z

    if-nez v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_2

    .line 152
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapAvatar()V

    .line 158
    :cond_1
    :goto_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onClick(Landroid/view/View;)V

    .line 159
    return-void

    .line 154
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->swapView()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 123
    const-string v0, "Tango.Video"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    iput-object p0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    .line 125
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setActivityHandler(Landroid/os/Handler;)V

    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setActivityHandler(Landroid/os/Handler;)V

    .line 127
    invoke-static {}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->hasH264Renderer()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_htcNoSwap:Z

    .line 128
    invoke-super {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onCreate(Landroid/os/Bundle;)V

    .line 129
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "Tango.Video"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setActivityHandler(Landroid/os/Handler;)V

    .line 135
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->onDestroy()V

    .line 136
    return-void
.end method

.method public onVideoModeChanged()V
    .locals 2

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_0

    .line 903
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->reinitVideoAvatarViews()V

    .line 907
    :goto_0
    return-void

    .line 905
    :cond_0
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->initViews()V

    goto :goto_0
.end method

.method public pipSwapSupported()Z
    .locals 1

    .prologue
    .line 877
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_htcNoSwap:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showCafe()V
    .locals 0

    .prologue
    .line 868
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x4

    .line 163
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " surface: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v3

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    .line 167
    :cond_0
    invoke-static {p1}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 168
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_current:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v3

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 171
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_rendererRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_transparentRect:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setBitmapConfig(Landroid/graphics/Bitmap$Config;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 176
    :goto_0
    const/high16 v0, -0x100

    invoke-static {v0}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->clear(I)V

    .line 179
    :cond_1
    return-void

    .line 173
    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_rendererRect:Landroid/graphics/Rect;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setBitmapConfig(Landroid/graphics/Bitmap$Config;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter

    .prologue
    .line 183
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceCreated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " surface: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    .line 186
    :cond_0
    invoke-static {p1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 187
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->staticResumeRecording()V

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_3

    .line 190
    :cond_2
    invoke-static {}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->staticStartRenderer()V

    .line 192
    :cond_3
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 196
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceDestroyed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " surface: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_1

    .line 198
    :cond_0
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->staticSuspendRecording()V

    .line 199
    invoke-static {v3}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    if-ne p1, v0, :cond_3

    .line 202
    :cond_2
    invoke-static {}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->staticStopRenderer()V

    .line 203
    invoke-static {v3}, Lcom/sgiggle/VideoRenderer/VideoRenderer;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 205
    :cond_3
    return-void
.end method

.method public updateLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 576
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateLayout tid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v4

    if-nez v0, :cond_0

    .line 578
    const-string v0, "Tango.Video"

    const-string v1, "updateLayout: no views, returning"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :goto_0
    return-void

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_next:Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;

    #getter for: Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->m_isH264Renderer:Z
    invoke-static {v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;->access$000(Lcom/sgiggle/production/VideoTwoWayCanvasActivity$Settings;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 586
    const/16 v0, 0xc0

    const/16 v1, 0x80

    invoke-static {v0, v1}, Lcom/sgiggle/VideoCapture/VideoView;->setRenderSize(II)Z

    .line 589
    :cond_1
    invoke-direct {p0, v4}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 590
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 591
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initRendererView(I)V

    .line 592
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 593
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 594
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initRendererView(I)V

    .line 595
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBorder()V

    .line 596
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBackground()V

    .line 597
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initButtonBackground()V

    goto :goto_0
.end method

.method public updateRendererSize(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 606
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateRendererSize("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->m_views:[Landroid/view/SurfaceView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    .line 608
    const-string v0, "Tango.Video"

    const-string v1, "updateRendererSize: no views, returning"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    invoke-static {p1, p2}, Lcom/sgiggle/VideoCapture/VideoView;->setRenderSize(II)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 613
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initRendererView(I)V

    .line 614
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 615
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initCameraView(I)V

    .line 616
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initRendererView(I)V

    .line 617
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBorder()V

    .line 618
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initBackground()V

    .line 619
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayCanvasActivity;->initButtonBackground()V

    goto :goto_0
.end method
