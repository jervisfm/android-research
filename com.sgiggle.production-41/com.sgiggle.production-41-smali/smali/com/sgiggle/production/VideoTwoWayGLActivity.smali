.class public Lcom/sgiggle/production/VideoTwoWayGLActivity;
.super Lcom/sgiggle/production/VideoTwoWayActivity;
.source "VideoTwoWayGLActivity.java"

# interfaces
.implements Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final SURFACE_CAMERA:I

.field private final SURFACE_GL:I

.field private currentFilter:I

.field private filterCount:I

.field private glCapture:Lcom/sgiggle/GLES20/GLCapture;

.field private glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

.field private mPaused:Z

.field private m_holders:[Landroid/view/SurfaceHolder;

.field private m_views:[Landroid/view/SurfaceView;

.field private videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->SURFACE_CAMERA:I

    .line 28
    const/4 v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->SURFACE_GL:I

    .line 29
    new-array v0, v1, [Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    .line 30
    new-array v0, v1, [Landroid/view/SurfaceHolder;

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    return-void
.end method


# virtual methods
.method public bringToFront()V
    .locals 0

    .prologue
    .line 256
    return-void
.end method

.method public changeViews()Z
    .locals 1

    .prologue
    .line 189
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->changeViews(Z)Z

    move-result v0

    return v0
.end method

.method public changeViews(Z)Z
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v3, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x3

    const/4 v4, -0x1

    .line 193
    .line 197
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 198
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_1

    move v0, v3

    move v1, v4

    .line 238
    :goto_0
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->isPipSwapped()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v3}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 239
    if-nez p1, :cond_0

    move v2, v4

    .line 242
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-virtual {v3, v0, v1, v2}, Lcom/sgiggle/GLES20/VideoTwoWay;->setView(III)V

    .line 249
    :goto_1
    return v5

    :cond_1
    move v0, v4

    move v1, v2

    move v2, v3

    .line 205
    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->RECEIVE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_4

    .line 208
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_3

    move v0, v6

    move v1, v4

    move v2, v5

    .line 211
    goto :goto_0

    :cond_3
    move v0, v4

    move v1, v2

    move v2, v5

    .line 215
    goto :goto_0

    .line 217
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_5

    move v0, v3

    move v1, v2

    move v2, v5

    .line 220
    goto :goto_0

    .line 221
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_a

    .line 222
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_6

    move v0, v6

    move v1, v4

    .line 225
    goto :goto_0

    .line 226
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_7

    move v0, v4

    move v1, v4

    move v2, v6

    .line 227
    goto :goto_0

    .line 228
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_a

    move v0, v4

    move v1, v4

    .line 229
    goto :goto_0

    .line 244
    :cond_8
    if-nez p1, :cond_9

    move v0, v4

    .line 247
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-virtual {v3, v2, v1, v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->setView(III)V

    goto :goto_1

    :cond_a
    move v0, v4

    move v1, v4

    move v2, v4

    goto :goto_0
.end method

.method public hideCafe()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/GLES20/GLRenderer;->setRenderMode(I)V

    .line 282
    :cond_0
    return-void
.end method

.method public initVideoViews()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 135
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    if-eqz v0, :cond_1

    .line 136
    iget-boolean v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->mPaused:Z

    if-nez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->changeViews()Z

    move-result v0

    .line 184
    :goto_0
    return v0

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    check-cast v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLSurfaceViewEx;->onDetachedFromWindow()V

    .line 140
    iput-boolean v4, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->mPaused:Z

    .line 143
    :cond_1
    iput v3, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_orientation:I

    .line 144
    const v0, 0x7f030072

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->setContentView(I)V

    .line 147
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-nez v0, :cond_2

    .line 148
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a0180

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v4

    .line 149
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v4

    invoke-virtual {v0, v4}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v4

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v4

    .line 151
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v4

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 152
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v4

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    const v0, 0x7f0a01a6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    aput-object v0, v1, v3

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    invoke-virtual {v0, p0}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    aput-object v1, v0, v3

    .line 159
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    aget-object v0, v0, v3

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    invoke-virtual {v0, v3}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 163
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 164
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v2

    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 176
    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 177
    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 178
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 179
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 180
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v1, v1, v3

    invoke-virtual {v1, v0}, Landroid/view/SurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 182
    iget-object v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v3

    check-cast v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx;

    invoke-virtual {v1, v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->setSurface(Lcom/sgiggle/GLES20/GLSurfaceViewEx;)V

    .line 183
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->changeViews()Z

    move v0, v3

    .line 184
    goto/16 :goto_0
.end method

.method public onAvatarChanged()V
    .locals 1

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->initViews()V

    .line 292
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    invoke-interface {v0}, Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;->showCafe()V

    .line 293
    return-void
.end method

.method public onAvatarPaused()V
    .locals 0

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->hideCafe()V

    .line 298
    return-void
.end method

.method public onAvatarResumed()V
    .locals 0

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->showCafe()V

    .line 303
    return-void
.end method

.method public onCleanUpAvatar()V
    .locals 0

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->hideCafe()V

    .line 308
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 40
    iput-object p0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_video_ui:Lcom/sgiggle/production/VideoTwoWayActivity$VideoUI;

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setActivityHandler(Landroid/os/Handler;)V

    .line 42
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->getInstance()Lcom/sgiggle/GLES20/GLRenderer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    .line 43
    invoke-static {}, Lcom/sgiggle/GLES20/GLRenderer;->hasGLCapture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Lcom/sgiggle/GLES20/GLCapture;->getInstance()Lcom/sgiggle/GLES20/GLCapture;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    .line 45
    :cond_0
    new-instance v0, Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-direct {v0}, Lcom/sgiggle/GLES20/VideoTwoWay;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    .line 46
    const-string v0, "filters"

    invoke-static {p0, v0}, Lcom/sgiggle/GLES20/FilterManager;->install(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->filterCount:I

    .line 47
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    .line 48
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->setFilter(II)V

    .line 49
    invoke-super {p0, p1}, Lcom/sgiggle/production/VideoTwoWayActivity;->onCreate(Landroid/os/Bundle;)V

    .line 50
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "Tango.Video"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setActivityHandler(Landroid/os/Handler;)V

    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->release()V

    .line 91
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->onDestroy()V

    .line 92
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 67
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v1

    check-cast v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLSurfaceViewEx;->onPause()V

    .line 69
    iput-boolean v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->mPaused:Z

    .line 71
    :cond_0
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->onPause()V

    .line 72
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getAppRunningState()Lcom/sgiggle/production/TangoApp$AppState;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/production/TangoApp$AppState;->APP_STATE_BACKGROUND:Lcom/sgiggle/production/TangoApp$AppState;

    if-ne v0, v1, :cond_0

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->changeViews(Z)Z

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_views:[Landroid/view/SurfaceView;

    aget-object v0, v0, v2

    check-cast v0, Lcom/sgiggle/GLES20/GLSurfaceViewEx;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/GLSurfaceViewEx;->onResume()V

    .line 62
    :cond_1
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->onResume()V

    .line 63
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 96
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 97
    if-eq v0, v3, :cond_0

    move v0, v3

    .line 107
    :goto_0
    return v0

    .line 99
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 100
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 101
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-virtual {v2, v0, v1}, Lcom/sgiggle/GLES20/VideoTwoWay;->isInside(FF)Z

    move-result v0

    .line 102
    if-ne v0, v3, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->videoTwoWay:Lcom/sgiggle/GLES20/VideoTwoWay;

    invoke-virtual {v0}, Lcom/sgiggle/GLES20/VideoTwoWay;->swapView()V

    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_session:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->swapPip()V

    .line 105
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->updateUserName()V

    :cond_1
    move v0, v3

    .line 107
    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 2

    .prologue
    .line 79
    const-string v0, "Tango.Video"

    const-string v1, "onUserLeaveHint()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->mLocalAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->mRemoteAvatarRenderer:Lcom/sgiggle/production/avatar/AvatarRenderer;

    invoke-virtual {v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 82
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->cleanUpCafe()V

    .line 83
    invoke-super {p0}, Lcom/sgiggle/production/VideoTwoWayActivity;->onUserLeaveHint()V

    .line 84
    return-void
.end method

.method public onVideoModeChanged()V
    .locals 0

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/sgiggle/production/VideoTwoWayGLActivity;->initViews()V

    .line 313
    return-void
.end method

.method public pipSwapSupported()Z
    .locals 1

    .prologue
    .line 286
    const/4 v0, 0x1

    return v0
.end method

.method public setFilter(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 316
    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    if-ne p1, v0, :cond_1

    move v0, v1

    .line 320
    :goto_0
    invoke-static {v1}, Lcom/sgiggle/GLES20/FilterManager;->setFilter(I)V

    .line 321
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    if-eqz v2, :cond_0

    .line 322
    iget-object v2, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    invoke-virtual {v2, v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setHightlightWithBackground(I)V

    .line 324
    :cond_0
    iput v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    .line 325
    return-void

    :cond_1
    move v0, p2

    move v1, p1

    goto :goto_0
.end method

.method protected setFilterHightlightWithBackground()V
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_vgoodBtnLayout:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;

    iget v1, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->currentFilter:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView;->setFilterHightlightWithBackground(I)V

    .line 331
    :cond_0
    return-void
.end method

.method public showCafe()V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glRenderer:Lcom/sgiggle/GLES20/GLRenderer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/GLES20/GLRenderer;->setRenderMode(I)V

    .line 275
    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 112
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter

    .prologue
    .line 117
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceCreated "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_1

    .line 119
    :cond_0
    invoke-static {p1}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 120
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->staticResumeRecording()V

    .line 122
    :cond_1
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter

    .prologue
    .line 126
    const-string v0, "Tango.Video"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "surfaceDestroyed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->m_holders:[Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/VideoTwoWayGLActivity;->glCapture:Lcom/sgiggle/GLES20/GLCapture;

    if-eqz v0, :cond_1

    .line 128
    :cond_0
    invoke-static {}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->staticSuspendRecording()V

    .line 129
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    .line 131
    :cond_1
    return-void
.end method

.method public updateLayout()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method public updateRendererSize(II)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 268
    return-void
.end method
