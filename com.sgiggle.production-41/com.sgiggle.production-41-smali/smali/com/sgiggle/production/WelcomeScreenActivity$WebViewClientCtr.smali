.class Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;
.super Landroid/webkit/WebViewClient;
.source "WelcomeScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/WelcomeScreenActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WebViewClientCtr"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/WelcomeScreenActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/WelcomeScreenActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/WelcomeScreenActivity;Lcom/sgiggle/production/WelcomeScreenActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;-><init>(Lcom/sgiggle/production/WelcomeScreenActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    const v1, 0x7f0a0006

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/WelcomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 111
    const-string v0, "Tango.UI"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR LOADING:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->onBackPressed()V

    .line 114
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 86
    invoke-static {p2}, Lcom/sgiggle/production/WelcomeScreenActivity;->handleWelcomeUrl(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->finish()V

    move v0, v4

    .line 101
    :goto_0
    return v0

    .line 91
    :cond_0
    const-string v0, "vnd.youtube:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/WelcomeScreenActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;->this$0:Lcom/sgiggle/production/WelcomeScreenActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->onBackPressed()V

    move v0, v4

    .line 98
    goto :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v0, "Tango.UI"

    const-string v1, "No youtube application installed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 101
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
