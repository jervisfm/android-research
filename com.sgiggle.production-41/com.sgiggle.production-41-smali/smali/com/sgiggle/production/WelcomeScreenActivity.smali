.class public Lcom/sgiggle/production/WelcomeScreenActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "WelcomeScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;
    }
.end annotation


# static fields
.field private static final LINK_PATTERN:Ljava/lang/String; = "tango.*?://welcome_screen/q\\?action=(.*)"

.field private static final TAG:Ljava/lang/String; = "Tango.UI"


# instance fields
.field private m_url:Ljava/lang/String;

.field private m_webview:Landroid/webkit/WebView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 83
    return-void
.end method

.method public static handleWelcomeUrl(Ljava/lang/String;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 72
    const-string v0, "tango.*?://welcome_screen/q\\?action=(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 73
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    invoke-virtual {v0, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$WelcomeGoToMessage;

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$WelcomeGoToMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    move v0, v4

    .line 80
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 67
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductInfoOkMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductInfoOkMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 68
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    .line 69
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 32
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 33
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/WelcomeScreenActivity;->requestWindowFeature(I)Z

    .line 34
    const v0, 0x7f030073

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->setContentView(I)V

    .line 36
    const v0, 0x7f0a0031

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/WelcomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 37
    const v1, 0x7f09011d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 39
    const v1, 0x7f0a01a7

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/WelcomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/WelcomeScreenActivity$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/WelcomeScreenActivity$1;-><init>(Lcom/sgiggle/production/WelcomeScreenActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    const v1, 0x7f0a0144

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/WelcomeScreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/webkit/WebView;

    iput-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    .line 47
    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 49
    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setPluginsEnabled(Z)V

    .line 50
    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    const/high16 v2, 0x200

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    .line 51
    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    new-instance v2, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sgiggle/production/WelcomeScreenActivity$WebViewClientCtr;-><init>(Lcom/sgiggle/production/WelcomeScreenActivity;Lcom/sgiggle/production/WelcomeScreenActivity$1;)V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 53
    invoke-virtual {p0}, Lcom/sgiggle/production/WelcomeScreenActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_0

    .line 55
    check-cast v1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductInfoEvent;

    .line 56
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductInfoEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/xmpp/SessionMessages$WelcomePayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$WelcomePayload;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 57
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductInfoEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$WelcomePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$WelcomePayload;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_url:Ljava/lang/String;

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?tango"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_url:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_webview:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/sgiggle/production/WelcomeScreenActivity;->m_url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method
