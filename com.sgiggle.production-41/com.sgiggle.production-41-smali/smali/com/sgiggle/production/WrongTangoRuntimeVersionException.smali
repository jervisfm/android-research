.class public Lcom/sgiggle/production/WrongTangoRuntimeVersionException;
.super Ljava/lang/Exception;
.source "WrongTangoRuntimeVersionException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private m_tangoCoreVersion:Ljava/lang/String;

.field private m_uiVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-static {p1, p2}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 16
    iput-object p1, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_uiVersion:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_tangoCoreVersion:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-static {p1, p2}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 22
    iput-object p1, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_uiVersion:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_tangoCoreVersion:Ljava/lang/String;

    .line 24
    return-void
.end method

.method private static createMessage(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Initialization failed due to UI/Runtime version mismatch: ui version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " core version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getTangoCoreVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_tangoCoreVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getUiVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->m_uiVersion:Ljava/lang/String;

    return-object v0
.end method
