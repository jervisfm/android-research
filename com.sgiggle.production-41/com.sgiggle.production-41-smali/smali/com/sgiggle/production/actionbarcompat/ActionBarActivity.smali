.class public abstract Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;
.super Lcom/sgiggle/production/FragmentActivityBase;
.source "ActionBarActivity.java"


# static fields
.field private static final MSG_REFRESH_ACTION_BAR:I


# instance fields
.field private mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

.field private m_handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sgiggle/production/FragmentActivityBase;-><init>()V

    .line 41
    invoke-static {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->createInstance(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    .line 47
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity$1;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->performRefreshActionBar()V

    return-void
.end method

.method private performRefreshActionBar()V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected getActionBarHelper()Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    return-object v0
.end method

.method public abstract getActionBarHomeIconResId()I
.end method

.method public getMenuInflater()Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    invoke-super {p0}, Lcom/sgiggle/production/FragmentActivityBase;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->onCreate(Landroid/os/Bundle;)V

    .line 76
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 95
    invoke-super {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 96
    return v0
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->onPostCreate(Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->onPostCreate(Landroid/os/Bundle;)V

    .line 83
    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->mActionBarHelper:Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 103
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/FragmentActivityBase;->onTitleChanged(Ljava/lang/CharSequence;I)V

    .line 104
    return-void
.end method

.method public refreshActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-eq v0, v1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 120
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
