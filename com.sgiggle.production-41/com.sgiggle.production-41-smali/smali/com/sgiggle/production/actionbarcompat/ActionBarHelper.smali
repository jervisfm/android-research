.class public abstract Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# instance fields
.field protected mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;


# direct methods
.method protected constructor <init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    .line 53
    return-void
.end method

.method public static createInstance(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;
    .locals 2
    .parameter

    .prologue
    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 45
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperHoneycomb;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperHoneycomb;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V

    .line 47
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 0
    .parameter

    .prologue
    .line 94
    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 77
    const/4 v0, 0x1

    return v0
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    return-void
.end method

.method public onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 85
    return-void
.end method

.method public abstract setDisplayHomeAsUpEnabled(Z)V
.end method
