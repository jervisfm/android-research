.class Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;
.super Ljava/lang/Object;
.source "ActionBarHelperBase.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

.field final synthetic val$item:Landroid/view/MenuItem;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Landroid/view/MenuItem;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    iput-object p2, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->val$item:Landroid/view/MenuItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 205
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->val$item:Landroid/view/MenuItem;

    invoke-interface {v0}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v3

    .line 212
    :goto_0
    return v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    iget-object v0, v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->val$item:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    iget-object v1, v1, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 210
    const/16 v2, 0x31

    invoke-virtual {v0, v2, v3, v1}, Landroid/widget/Toast;->setGravity(III)V

    .line 211
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 212
    const/4 v0, 0x1

    goto :goto_0
.end method
