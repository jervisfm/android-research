.class Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;
.super Landroid/view/MenuInflater;
.source "ActionBarHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WrappedMenuInflater"
.end annotation


# instance fields
.field mInflater:Landroid/view/MenuInflater;

.field final synthetic this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Landroid/content/Context;Landroid/view/MenuInflater;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    .line 229
    invoke-direct {p0, p2}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    .line 230
    iput-object p3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->mInflater:Landroid/view/MenuInflater;

    .line 231
    return-void
.end method

.method private loadActionBarMetadata(I)V
    .locals 8
    .parameter

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 247
    const/4 v0, 0x0

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    iget-object v1, v1, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 251
    :try_start_1
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    move v7, v2

    move v2, v1

    move v1, v7

    .line 256
    :goto_0
    if-nez v1, :cond_3

    .line 257
    packed-switch v2, :pswitch_data_0

    .line 282
    :cond_0
    :goto_1
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v2

    goto :goto_0

    .line 259
    :pswitch_0
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "item"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    const-string v2, "http://schemas.android.com/apk/res/android"

    const-string v3, "id"

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .line 265
    if-eqz v2, :cond_0

    .line 269
    const-string v3, "http://schemas.android.com/apk/res/android"

    const-string v4, "showAsAction"

    const/4 v5, -0x1

    invoke-interface {v0, v3, v4, v5}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v3

    .line 271
    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    if-ne v3, v6, :cond_0

    .line 273
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->this$0:Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;

    iget-object v3, v3, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 284
    :catch_0
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 285
    :goto_2
    :try_start_2
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 289
    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_2

    .line 290
    invoke-interface {v1}, Landroid/content/res/XmlResourceParser;->close()V

    :cond_2
    throw v0

    :pswitch_1
    move v1, v6

    .line 278
    goto :goto_1

    .line 289
    :cond_3
    if-eqz v0, :cond_4

    .line 290
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->close()V

    .line 293
    :cond_4
    return-void

    .line 286
    :catch_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 287
    :goto_4
    :try_start_3
    new-instance v2, Landroid/view/InflateException;

    const-string v3, "Error inflating menu XML"

    invoke-direct {v2, v3, v0}, Landroid/view/InflateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 289
    :catchall_1
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_3

    .line 286
    :catch_2
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_4

    .line 284
    :catch_3
    move-exception v1

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_2

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public inflate(ILandroid/view/Menu;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 235
    invoke-direct {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->loadActionBarMetadata(I)V

    .line 236
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;->mInflater:Landroid/view/MenuInflater;

    invoke-virtual {v0, p1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 237
    return-void
.end method
