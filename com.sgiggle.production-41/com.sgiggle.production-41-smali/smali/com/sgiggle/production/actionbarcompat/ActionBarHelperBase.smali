.class public Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;
.super Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;
.source "ActionBarHelperBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;
    }
.end annotation


# static fields
.field private static final MENU_ATTR_ID:Ljava/lang/String; = "id"

.field private static final MENU_ATTR_SHOW_AS_ACTION:Ljava/lang/String; = "showAsAction"

.field private static final MENU_RES_NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field protected mActionItemIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected m_upView:Landroid/view/View;


# direct methods
.method protected constructor <init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;)V

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    .line 62
    return-void
.end method

.method private addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x0

    const v5, 0x102002c

    .line 174
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 176
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->getActionBarCompat()Landroid/view/ViewGroup;

    move-result-object v1

    .line 177
    if-nez v1, :cond_0

    move-object v0, v6

    .line 219
    :goto_0
    return-object v0

    .line 182
    :cond_0
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    if-ne v0, v5, :cond_1

    const v4, 0x7f010002

    :goto_1
    invoke-direct {v2, v3, v6, v4}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 185
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    iget-object v4, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v4}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-ne v0, v5, :cond_2

    const v0, 0x7f080007

    :goto_2
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    const/4 v4, -0x1

    invoke-direct {v3, v0, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 193
    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 195
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 196
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$2;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$2;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Landroid/view/MenuItem;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$3;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Landroid/view/MenuItem;)V

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 217
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v0, v2

    .line 219
    goto :goto_0

    .line 182
    :cond_1
    const v4, 0x7f010001

    goto :goto_1

    .line 185
    :cond_2
    const v0, 0x7f080006

    goto :goto_2
.end method

.method private getActionBarCompat()Landroid/view/ViewGroup;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    const/high16 v1, 0x7f0a

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method private setupActionBar()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 93
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->getActionBarCompat()Landroid/view/ViewGroup;

    move-result-object v0

    .line 94
    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v6, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 100
    const/high16 v2, 0x3f80

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 103
    new-instance v2, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    iget-object v3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-direct {v2, v3}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;-><init>(Landroid/content/Context;)V

    .line 104
    new-instance v3, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;

    const v4, 0x102002c

    iget-object v5, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v5}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-direct {v3, v2, v4, v6, v5}, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;-><init>(Lcom/sgiggle/production/actionbarcompat/SimpleMenu;IILjava/lang/CharSequence;)V

    .line 106
    iget-object v2, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v2}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getActionBarHomeIconResId()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 107
    invoke-direct {p0, v3}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;

    .line 110
    const v2, 0x7f0a0002

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->m_upView:Landroid/view/View;

    .line 111
    iget-object v2, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->m_upView:Landroid/view/View;

    new-instance v4, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$1;

    invoke-direct {v4, p0, v3}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$1;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;)V

    invoke-virtual {v2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    new-instance v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    const/4 v4, 0x0

    const/high16 v5, 0x7f01

    invoke-direct {v2, v3, v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method


# virtual methods
.method public getMenuInflater(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 2
    .parameter

    .prologue
    .line 157
    new-instance v0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;

    iget-object v1, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-direct {v0, p0, v1, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase$WrappedMenuInflater;-><init>(Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;Landroid/content/Context;Landroid/view/MenuInflater;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->requestWindowFeature(I)Z

    .line 68
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 137
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 139
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x7

    const/high16 v2, 0x7f03

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->setupActionBar()V

    .line 77
    new-instance v1, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-direct {v1, v0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;-><init>(Landroid/content/Context;)V

    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v0, v3, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 79
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move v2, v3

    .line 80
    :goto_0
    invoke-virtual {v1}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 81
    invoke-virtual {v1, v2}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;

    .line 82
    iget-object v3, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActionItemIds:Ljava/util/Set;

    invoke-virtual {v0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->getItemId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 83
    invoke-direct {p0, v0}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->addActionItemCompatFromMenuItem(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v3

    .line 84
    invoke-virtual {v0, v3}, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->setView(Landroid/view/View;)V

    .line 80
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 87
    :cond_1
    return-void
.end method

.method public onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->mActivity:Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 147
    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_0
    return-void
.end method

.method public setDisplayHomeAsUpEnabled(Z)V
    .locals 2
    .parameter

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/ActionBarHelperBase;->m_upView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 300
    return-void

    .line 299
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method
