.class public Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;
.super Ljava/lang/Object;
.source "SimpleMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field private mEnabled:Z

.field private mIconDrawable:Landroid/graphics/drawable/Drawable;

.field private mIconResId:I

.field private final mId:I

.field private mMenu:Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

.field private final mOrder:I

.field private mTitle:Ljava/lang/CharSequence;

.field private mTitleCondensed:Ljava/lang/CharSequence;

.field private mView:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mVisible:Z


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/actionbarcompat/SimpleMenu;IILjava/lang/CharSequence;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconResId:I

    .line 44
    iput-boolean v1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mEnabled:Z

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mView:Ljava/lang/ref/WeakReference;

    .line 46
    iput-boolean v1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mVisible:Z

    .line 49
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mMenu:Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    .line 50
    iput p2, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mId:I

    .line 51
    iput p3, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mOrder:I

    .line 52
    iput-object p4, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 53
    return-void
.end method


# virtual methods
.method public collapseActionView()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 114
    :goto_0
    return-object v0

    .line 110
    :cond_0
    iget v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconResId:I

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mMenu:Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    invoke-virtual {v0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconResId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mId:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mOrder:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mView:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mEnabled:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mVisible:Z

    return v0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 254
    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 249
    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 180
    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 190
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 200
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mEnabled:Z

    .line 119
    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1
    .parameter

    .prologue
    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 101
    iput p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconResId:I

    .line 102
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1
    .parameter

    .prologue
    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconResId:I

    .line 95
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mIconDrawable:Landroid/graphics/drawable/Drawable;

    .line 96
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 155
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 170
    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 230
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 165
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0
    .parameter

    .prologue
    .line 240
    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 1
    .parameter

    .prologue
    .line 244
    const/4 v0, 0x0

    return-object v0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mMenu:Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    invoke-virtual {v0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitle:Ljava/lang/CharSequence;

    .line 73
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mTitleCondensed:Ljava/lang/CharSequence;

    .line 86
    return-object p0
.end method

.method public setView(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 56
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mView:Ljava/lang/ref/WeakReference;

    .line 57
    return-void
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1
    .parameter

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mVisible:Z

    .line 210
    iget-object v0, p0, Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;->mMenu:Lcom/sgiggle/production/actionbarcompat/SimpleMenu;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/actionbarcompat/SimpleMenu;->onItemVisibleChanged(Lcom/sgiggle/production/actionbarcompat/SimpleMenuItem;)V

    .line 211
    return-object p0
.end method
