.class public Lcom/sgiggle/production/adapter/AlertListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AlertListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/AlertListAdapter$1;,
        Lcom/sgiggle/production/adapter/AlertListAdapter$OpenLinksTouchListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
        ">;"
    }
.end annotation


# static fields
.field public static final KEY_TAG_APP_ALERT:I = 0x1

.field public static final KEY_TAG_HOLDER:I


# instance fields
.field private final m_context:Landroid/content/Context;

.field private m_data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private final m_inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 35
    iput-object p1, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_context:Landroid/content/Context;

    .line 36
    iput-object p2, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    .line 37
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 38
    return-void
.end method

.method private getMessageWithLinks(Ljava/lang/String;)Landroid/text/Spanned;
    .locals 2
    .parameter

    .prologue
    .line 101
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 105
    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/text/Spannable;I)Z

    .line 108
    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDataSet()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    return-object v0
.end method

.method public getItem(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 55
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/AlertListAdapter;->getItem(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 60
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 69
    if-nez p2, :cond_2

    .line 70
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030049

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    .line 73
    :goto_0
    const v0, 0x7f0a0136

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 74
    const v1, 0x7f0a0137

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 75
    const v2, 0x7f0a0135

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 77
    iget-object v3, p0, Lcom/sgiggle/production/adapter/AlertListAdapter;->m_data:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    .line 78
    new-instance v5, Lcom/sgiggle/production/CTANotifier$AppAlert;

    invoke-direct {v5, v3}, Lcom/sgiggle/production/CTANotifier$AppAlert;-><init>(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)V

    .line 82
    invoke-virtual {v5}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {v5}, Lcom/sgiggle/production/CTANotifier$AppAlert;->CTA()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/AlertListAdapter;->getMessageWithLinks(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 86
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    new-instance v0, Lcom/sgiggle/production/adapter/AlertListAdapter$OpenLinksTouchListener;

    invoke-direct {v0, v6}, Lcom/sgiggle/production/adapter/AlertListAdapter$OpenLinksTouchListener;-><init>(Lcom/sgiggle/production/adapter/AlertListAdapter$1;)V

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 89
    invoke-virtual {v5}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isRegistrationRequired()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v5}, Lcom/sgiggle/production/CTANotifier$AppAlert;->isValidationRequired()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    :cond_0
    const v0, 0x7f020084

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 95
    :goto_1
    invoke-virtual {v4, v5}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 97
    return-object v4

    .line 92
    :cond_1
    const v0, 0x7f0200dc

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_2
    move-object v4, p2

    goto :goto_0
.end method
