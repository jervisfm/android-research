.class Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;
.super Landroid/os/Handler;
.source "AutoRefreshingArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;->this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 33
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 45
    :cond_0
    :goto_0
    return-void

    .line 35
    :pswitch_0
    const-string v0, "Tango.AutoRefreshingArrayAdapter"

    const-string v1, "Refreshing self."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;->this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;

    #calls: Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->refresh()V
    invoke-static {v0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->access$000(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)V

    .line 40
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;->this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;

    #getter for: Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z
    invoke-static {v0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->access$100(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;->this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;

    #getter for: Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->access$300(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;->this$0:Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;

    #getter for: Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_refreshIntervalMs:J
    invoke-static {v2}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->access$200(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
