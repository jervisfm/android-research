.class public abstract Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AutoRefreshingArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final MSG_REFRESH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.AutoRefreshingArrayAdapter"


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_isAutoRefreshing:Z

.field private m_refreshIntervalMs:J


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    .line 30
    new-instance v0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter$1;-><init>(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_handler:Landroid/os/Handler;

    .line 59
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->getRefreshIntervalMs()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_refreshIntervalMs:J

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->refresh()V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)Z
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)J
    .locals 2
    .parameter

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_refreshIntervalMs:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method private refresh()V
    .locals 0

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->notifyDataSetChanged()V

    .line 53
    return-void
.end method


# virtual methods
.method protected abstract getRefreshIntervalMs()J
.end method

.method public isAutoRefreshing()Z
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    return v0
.end method

.method public startAutoRefresh(Z)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 77
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    if-eqz v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->stopAutoRefresh()V

    .line 84
    iput-boolean v3, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    .line 87
    if-eqz p1, :cond_1

    .line 88
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->refresh()V

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_handler:Landroid/os/Handler;

    iget-wide v1, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_refreshIntervalMs:J

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopAutoRefresh()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->m_isAutoRefreshing:Z

    .line 101
    return-void
.end method
