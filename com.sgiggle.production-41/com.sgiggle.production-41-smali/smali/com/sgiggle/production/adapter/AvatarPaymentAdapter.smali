.class public Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "AvatarPaymentAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;,
        Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private iconsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private m_clickListener:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;

.field private m_inflater:Landroid/view/LayoutInflater;

.field private m_isBillingSupported:Z

.field private m_isPurchaseEnabled:Z

.field private m_rowColorDark:I

.field private m_rowColorLight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 50
    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 40
    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isBillingSupported:Z

    .line 42
    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isPurchaseEnabled:Z

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    .line 51
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_rowColorLight:I

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_rowColorDark:I

    .line 54
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isPurchaseEnabled:Z

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isBillingSupported:Z

    .line 55
    return-void
.end method

.method private getColorForPosition(I)I
    .locals 1
    .parameter

    .prologue
    .line 155
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    .line 156
    iget v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_rowColorLight:I

    .line 157
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_rowColorDark:I

    goto :goto_0
.end method

.method private getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 135
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 136
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 151
    :goto_0
    return-object v0

    .line 140
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 142
    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 143
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 144
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 146
    if-nez v0, :cond_1

    move-object v0, v2

    .line 147
    goto :goto_0

    .line 149
    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 150
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    move-object v0, v1

    .line 151
    goto :goto_0
.end method

.method private getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Check price"

    goto :goto_0
.end method

.method private notifyItemClicked(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;->onClick(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 181
    :cond_0
    return-void
.end method

.method private setPurchaseEnabled(Z)V
    .locals 0
    .parameter

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isPurchaseEnabled:Z

    .line 197
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->notifyDataSetChanged()V

    .line 198
    return-void
.end method


# virtual methods
.method public getOnClickListener()Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;

    return-object v0
.end method

.method public getProductItemFromMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 4
    .parameter

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getCount()I

    move-result v1

    .line 202
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 203
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 204
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 208
    :goto_1
    return-object v0

    .line 202
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const v9, 0x7f0200d3

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 67
    .line 68
    if-nez p2, :cond_b

    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030008

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 70
    new-instance v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;-><init>()V

    .line 71
    const v0, 0x7f0a0031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0a0032

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->desc:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0a0033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    .line 74
    const v0, 0x7f0a0029

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    .line 75
    const v0, 0x7f0a0030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    .line 76
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v4, v1

    .line 79
    :goto_0
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;

    .line 80
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 82
    invoke-direct {p0, p1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getColorForPosition(I)I

    move-result v2

    invoke-virtual {v4, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 84
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->desc:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 88
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v2, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 94
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v2

    iget-object v2, v2, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 96
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getValue()F

    move-result v3

    cmpl-float v3, v3, v8

    if-nez v3, :cond_3

    .line 97
    iget-object v3, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 104
    :goto_1
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 107
    iget-object v3, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/SoftReference;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    if-nez v3, :cond_1

    .line 108
    :cond_0
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 109
    if-eqz v3, :cond_1

    .line 110
    iget-object v5, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v7, Ljava/lang/ref/SoftReference;

    invoke-direct {v7, v3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    :cond_1
    if-eqz v3, :cond_7

    .line 114
    iget-object v5, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 122
    :goto_2
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v2, :cond_9

    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v2, v3, :cond_9

    .line 123
    :cond_2
    iget-object v0, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    const v1, 0x7f0900fb

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 131
    :goto_3
    return-object v4

    .line 98
    :cond_3
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v2, :cond_4

    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq v2, v3, :cond_5

    :cond_4
    iget-boolean v3, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isPurchaseEnabled:Z

    if-nez v3, :cond_6

    .line 99
    :cond_5
    iget-object v3, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 101
    :cond_6
    iget-object v3, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 116
    :cond_7
    iget-object v3, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 119
    :cond_8
    iget-object v3, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v3, v9}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 125
    :cond_9
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getValue()F

    move-result v2

    cmpl-float v2, v2, v8

    if-lez v2, :cond_a

    .line 126
    iget-object v0, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 128
    :cond_a
    iget-object v0, v0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    const v1, 0x7f0900fd

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_3

    :cond_b
    move-object v4, p2

    goto/16 :goto_0
.end method

.method public isBillingSupported()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isBillingSupported:Z

    return v0
.end method

.method public isPurchaseEnabled()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isPurchaseEnabled:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 170
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 171
    instance-of v1, v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    if-eqz v1, :cond_0

    .line 172
    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 173
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->notifyItemClicked(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 175
    :cond_0
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 0
    .parameter

    .prologue
    .line 216
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isBillingSupported:Z

    .line 217
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->updateUIState()V

    .line 218
    return-void
.end method

.method public setOnClickListener(Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;)V
    .locals 0
    .parameter

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$OnClickListener;

    .line 189
    return-void
.end method

.method public setProductPurchasedFlag(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 252
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 253
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 254
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setPurchased(Z)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    .line 256
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->remove(Ljava/lang/Object;)V

    .line 257
    invoke-virtual {p0, v2, v1}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->insert(Ljava/lang/Object;I)V

    .line 252
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 260
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->notifyDataSetChanged()V

    .line 261
    return-void
.end method

.method public setProducts(Ljava/util/List;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 230
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setNotifyOnChange(Z)V

    .line 231
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->clear()V

    .line 234
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 235
    new-instance v1, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter$1;-><init>(Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 242
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 243
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKey()Ljava/lang/String;

    move-result-object v2

    const-string v3, "product.category.avatar"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 246
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->updateUIState()V

    .line 247
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->notifyDataSetChanged()V

    .line 249
    :cond_2
    return-void
.end method

.method public updateUIState()V
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->m_isBillingSupported:Z

    if-nez v0, :cond_0

    .line 222
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setPurchaseEnabled(Z)V

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/AvatarPaymentAdapter;->setPurchaseEnabled(Z)V

    goto :goto_0
.end method
