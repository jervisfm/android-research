.class public final enum Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;
.super Ljava/lang/Enum;
.source "ContactListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/adapter/ContactListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContactListSelection"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field public static final enum ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field public static final enum FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field public static final enum TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field public static final enum UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 34
    new-instance v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const-string v1, "TANGO"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 35
    new-instance v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const-string v1, "FAVORITE"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 36
    new-instance v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->ALL:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->$VALUES:[Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;
    .locals 1
    .parameter

    .prologue
    .line 32
    const-class v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->$VALUES:[Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v0}, [Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    return-object v0
.end method
