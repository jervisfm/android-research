.class public abstract Lcom/sgiggle/production/adapter/ContactListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContactListAdapter.java"

# interfaces
.implements Landroid/widget/SectionIndexer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;
    }
.end annotation


# static fields
.field private static final COMPACT_LIST_LIMIT:I = 0xe

.field private static final VIEW_TYPE_CONTACT:I = 0x1

.field private static final VIEW_TYPE_DIVIDER:I


# instance fields
.field private alphaIndexer:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private indexerPos:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

.field protected final m_defaultContactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field protected m_inflater:Landroid/view/LayoutInflater;

.field private m_isSectionIndexerUpToDate:Z

.field protected final m_items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private sections:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    .line 46
    invoke-static {}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_isSectionIndexerUpToDate:Z

    .line 59
    iput-object p1, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 60
    iput-object p2, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_defaultContactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 61
    iput-object p3, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 62
    return-void
.end method

.method private getDividerView(Ljava/lang/String;)Landroid/view/View;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 200
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->isCompactMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030016

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 206
    :goto_0
    const v0, 0x7f0a0078

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    return-object v1

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030015

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method private isCompactMode()Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetGroupIdxs()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 67
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 68
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V
.end method

.method public getContactCount()I
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 224
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    array-length v1, v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method protected abstract getContactView(Lcom/sgiggle/production/Utils$UIContact;)Landroid/view/View;
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 127
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 128
    :cond_0
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 136
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 151
    const/4 v0, 0x0

    .line 153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 2
    .parameter

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1
    .parameter

    .prologue
    .line 249
    const/4 v0, 0x0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 173
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 174
    const v0, 0x7f0a0078

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, p2

    .line 194
    :goto_0
    return-object v0

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getDividerView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 187
    if-nez p2, :cond_2

    .line 188
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getContactView(Lcom/sgiggle/production/Utils$UIContact;)Landroid/view/View;

    move-result-object v1

    .line 192
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V

    move-object v0, v1

    .line 194
    goto :goto_0

    :cond_2
    move-object v1, p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 166
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ContactListAdapter;->getItemViewType(I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSectionIndexerUpToDate()Z
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_isSectionIndexerUpToDate:Z

    return v0
.end method

.method public loadGroups(Ljava/util/List;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;)V
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;",
            "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ContactListAdapter;->resetGroupIdxs()V

    .line 76
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne p2, v0, :cond_b

    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_defaultContactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    move-object v1, v0

    .line 81
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/16 v2, 0xe

    if-ge v0, v2, :cond_4

    .line 82
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 83
    sget-object v3, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v1, v3, :cond_1

    iget-object v3, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    :cond_1
    sget-object v3, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v1, v3, :cond_2

    iget-boolean v3, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    if-eqz v3, :cond_0

    .line 90
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    :cond_3
    iput-boolean v4, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_isSectionIndexerUpToDate:Z

    .line 123
    :goto_2
    return-void

    :cond_4
    move v2, v4

    .line 95
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 96
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 97
    sget-object v3, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->TANGO:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v1, v3, :cond_6

    iget-object v3, v0, Lcom/sgiggle/production/Utils$UIContact;->m_accountId:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_6

    .line 95
    :cond_5
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 100
    :cond_6
    sget-object v3, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->FAVORITE:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    if-ne v1, v3, :cond_7

    iget-boolean v3, v0, Lcom/sgiggle/production/Utils$UIContact;->m_favorite:Z

    if-eqz v3, :cond_5

    .line 104
    :cond_7
    iget-object v3, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    invoke-virtual {v3}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/Utils$UIContact;->dispNameFirstChar(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;)C

    move-result v3

    .line 106
    const/16 v4, 0x41

    if-ge v3, v4, :cond_9

    const-string v3, "#"

    .line 107
    :goto_5
    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    .line 108
    iget-object v4, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 109
    iget-object v4, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v4, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->indexerPos:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    iget-object v3, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-static {}, Lcom/sgiggle/production/Utils$UIContact;->getDummyContact()Lcom/sgiggle/production/Utils$UIContact;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_items:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 106
    :cond_9
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_5

    .line 116
    :cond_a
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->alphaIndexer:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 117
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 118
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 119
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    .line 120
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->sections:[Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/ContactListAdapter;->m_isSectionIndexerUpToDate:Z

    goto/16 :goto_2

    :cond_b
    move-object v1, p2

    goto/16 :goto_0
.end method
