.class Lcom/sgiggle/production/adapter/ConversationListAdapter$1;
.super Ljava/lang/Object;
.source "ConversationListAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/adapter/ConversationListAdapter;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sgiggle/production/model/ConversationSummary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/adapter/ConversationListAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/adapter/ConversationListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter$1;->this$0:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sgiggle/production/model/ConversationSummary;Lcom/sgiggle/production/model/ConversationSummary;)I
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationSummary;->getLastMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getTimestampMs()J

    move-result-wide v0

    .line 42
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getLastMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationMessage;->getTimestampMs()J

    move-result-wide v2

    .line 44
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 38
    check-cast p1, Lcom/sgiggle/production/model/ConversationSummary;

    check-cast p2, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/adapter/ConversationListAdapter$1;->compare(Lcom/sgiggle/production/model/ConversationSummary;Lcom/sgiggle/production/model/ConversationSummary;)I

    move-result v0

    return v0
.end method
