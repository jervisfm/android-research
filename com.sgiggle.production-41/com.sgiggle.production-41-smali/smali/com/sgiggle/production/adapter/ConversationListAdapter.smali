.class public Lcom/sgiggle/production/adapter/ConversationListAdapter;
.super Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;
.source "ConversationListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter",
        "<",
        "Lcom/sgiggle/production/model/ConversationSummary;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationListAdapter"


# instance fields
.field private m_context:Landroid/content/Context;

.field private m_conversationPerId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sgiggle/production/model/ConversationSummary;",
            ">;"
        }
    .end annotation
.end field

.field private final m_mostRecentComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sgiggle/production/model/ConversationSummary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 34
    const v0, 0x1090003

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 35
    iput-object p1, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_context:Landroid/content/Context;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    .line 38
    new-instance v0, Lcom/sgiggle/production/adapter/ConversationListAdapter$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter$1;-><init>(Lcom/sgiggle/production/adapter/ConversationListAdapter;)V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_mostRecentComparator:Ljava/util/Comparator;

    .line 47
    return-void
.end method

.method private getFirstMessageIdIfUnread()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getUnreadMessageCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 87
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getLastMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v0

    .line 89
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private sortByMostRecent()V
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_mostRecentComparator:Ljava/util/Comparator;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->sort(Ljava/util/Comparator;)V

    .line 176
    return-void
.end method


# virtual methods
.method public add(Lcom/sgiggle/production/model/ConversationSummary;)V
    .locals 2
    .parameter

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->add(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    return-void
.end method

.method public bridge synthetic add(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    check-cast p1, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->add(Lcom/sgiggle/production/model/ConversationSummary;)V

    return-void
.end method

.method public addOrUpdate(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    .line 141
    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-direct {v0, p1, p2}, Lcom/sgiggle/production/model/ConversationSummary;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->add(Lcom/sgiggle/production/model/ConversationSummary;)V

    .line 150
    :goto_0
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->sortByMostRecent()V

    .line 151
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->notifyDataSetChanged()V

    .line 152
    return-void

    .line 146
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/model/ConversationSummary;->setData(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->clear()V

    .line 127
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 128
    return-void
.end method

.method public getFirstConversationPosWithUnreadMessages()I
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getCount()I

    move-result v1

    .line 162
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 163
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getUnreadMessageCount()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v2

    .line 168
    :goto_1
    return v0

    .line 162
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method protected getRefreshIntervalMs()J
    .locals 2

    .prologue
    .line 206
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    if-nez p2, :cond_0

    .line 186
    new-instance v0, Lcom/sgiggle/production/widget/ConversationListItemView;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/widget/ConversationListItemView;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    .line 192
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    .line 193
    iget-object v2, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_context:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->fill(Landroid/content/Context;Lcom/sgiggle/production/model/ConversationSummary;)V

    .line 195
    return-object v1

    .line 188
    :cond_0
    check-cast p2, Lcom/sgiggle/production/widget/ConversationListItemView;

    move-object v1, p2

    goto :goto_0
.end method

.method public remove(Lcom/sgiggle/production/model/ConversationSummary;)V
    .locals 2
    .parameter

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->remove(Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-void
.end method

.method public bridge synthetic remove(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    check-cast p1, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->remove(Lcom/sgiggle/production/model/ConversationSummary;)V

    return-void
.end method

.method public remove(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sgiggle/production/adapter/ConversationListAdapter;->m_conversationPerId:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    .line 113
    if-nez v0, :cond_0

    .line 115
    const-string v0, "Tango.ConversationListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "remove(): no conversation with ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :goto_0
    return-void

    .line 117
    :cond_0
    const-string v1, "Tango.ConversationListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove(): removed conversation with ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->remove(Lcom/sgiggle/production/model/ConversationSummary;)V

    goto :goto_0
.end method

.method public setData(Ljava/util/List;)Z
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationSummary;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 55
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getFirstMessageIdIfUnread()I

    move-result v1

    .line 59
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->setNotifyOnChange(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->clear()V

    .line 62
    if-eqz p1, :cond_2

    .line 63
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    .line 64
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->add(Lcom/sgiggle/production/model/ConversationSummary;)V

    goto :goto_0

    .line 68
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->sortByMostRecent()V

    .line 70
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getFirstMessageIdIfUnread()I

    move-result v0

    .line 73
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->setNotifyOnChange(Z)V

    .line 74
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->notifyDataSetChanged()V

    .line 76
    if-eq v0, v3, :cond_1

    if-eq v1, v0, :cond_1

    move v0, v5

    :goto_2
    return v0

    :cond_1
    move v0, v4

    goto :goto_2

    :cond_2
    move v0, v3

    goto :goto_1
.end method
