.class public Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;
.super Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;
.source "ConversationMessageListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/ConversationMessageListAdapter$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter",
        "<",
        "Lcom/sgiggle/production/model/ConversationMessage;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationMessageListAdapter"

.field private static final TYPE_MAX_COUNT:I = 0x5

.field private static final TYPE_PICTURE:I = 0x4

.field private static final TYPE_SIMPLE:I = 0x2

.field private static final TYPE_TEXT:I = 0x0

.field private static final TYPE_VGOOD_ANIM:I = 0x3

.field private static final TYPE_VIDEOMAIL:I = 0x1


# instance fields
.field private m_context:Landroid/content/Context;

.field private m_isGroupConversation:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 39
    const v0, 0x1090003

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_isGroupConversation:Z

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setNotifyOnChange(Z)V

    .line 41
    iput-object p1, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_context:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public clearData()V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setData(Ljava/util/List;Z)V

    .line 60
    return-void
.end method

.method public createMessageListItemView(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/widget/MessageListItemView;
    .locals 3
    .parameter

    .prologue
    .line 139
    sget-object v0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType:[I

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 156
    const-string v0, "Tango.ConversationMessageListAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "itemViewFactory: unsupported type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 141
    :pswitch_0
    new-instance v0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 143
    :pswitch_1
    new-instance v0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 150
    :pswitch_2
    new-instance v0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 153
    :pswitch_3
    const-string v0, "Tango.ConversationMessageListAdapter"

    const-string v1, "createMessageListItemView IMAGE_MESSAGE"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    new-instance v0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 139
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 4
    .parameter

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    .line 87
    sget-object v1, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType:[I

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 103
    const-string v1, "Tango.ConversationMessageListAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getItemViewType: unsupported type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    invoke-super {p0, p1}, Lcom/sgiggle/production/adapter/AutoRefreshingArrayAdapter;->getItemViewType(I)I

    move-result v0

    :goto_0
    return v0

    .line 89
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    :pswitch_2
    const/4 v0, 0x4

    goto :goto_0

    .line 101
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected getRefreshIntervalMs()J
    .locals 2

    .prologue
    .line 52
    const-wide/32 v0, 0xea60

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    .line 121
    if-nez p2, :cond_0

    .line 122
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->createMessageListItemView(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/widget/MessageListItemView;

    move-result-object v1

    .line 128
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_isGroupConversation:Z

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/widget/MessageListItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 130
    return-object v1

    .line 124
    :cond_0
    check-cast p2, Lcom/sgiggle/production/widget/MessageListItemView;

    move-object v1, p2

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x5

    return v0
.end method

.method public setData(Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationMessage;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setNotifyOnChange(Z)V

    .line 71
    iput-boolean p2, p0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->m_isGroupConversation:Z

    .line 73
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->clear()V

    .line 74
    if-eqz p1, :cond_0

    .line 75
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    .line 76
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 80
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setNotifyOnChange(Z)V

    .line 81
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->notifyDataSetChanged()V

    .line 82
    return-void
.end method
