.class public Lcom/sgiggle/production/adapter/ParticipantListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ParticipantListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/production/model/ConversationContact;",
        ">;"
    }
.end annotation


# instance fields
.field private m_inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    const v0, 0x1090003

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 30
    return-void
.end method

.method private addData(Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 54
    if-eqz p2, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->clear()V

    .line 57
    :cond_0
    if-eqz p1, :cond_1

    .line 58
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 59
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 62
    :cond_1
    return-void
.end method


# virtual methods
.method public addData(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->addData(Ljava/util/List;Z)V

    .line 46
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 70
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 73
    if-nez p2, :cond_1

    .line 74
    iget-object v1, p0, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030039

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v2, v1

    .line 80
    :goto_0
    const v1, 0x7f0a00df

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 84
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 85
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v3, v3, v5

    if-eqz v3, :cond_4

    .line 86
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->shouldHaveImage()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 87
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 88
    if-nez v3, :cond_0

    .line 90
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 95
    :cond_0
    :goto_1
    if-eqz v3, :cond_2

    .line 96
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 103
    :goto_2
    if-nez p1, :cond_3

    .line 104
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f09012e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 109
    :goto_3
    const v0, 0x7f0a00e0

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    return-object v2

    :cond_1
    move-object v2, p2

    .line 76
    goto :goto_0

    .line 98
    :cond_2
    const v3, 0x7f020090

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 106
    :cond_3
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayNameShort()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    :cond_4
    move-object v3, v7

    goto :goto_1
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->addData(Ljava/util/List;Z)V

    .line 38
    return-void
.end method
