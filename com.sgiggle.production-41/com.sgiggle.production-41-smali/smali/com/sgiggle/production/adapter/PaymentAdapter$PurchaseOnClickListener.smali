.class Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;
.super Ljava/lang/Object;
.source "PaymentAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/adapter/PaymentAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PurchaseOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/adapter/PaymentAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/adapter/PaymentAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;->this$0:Lcom/sgiggle/production/adapter/PaymentAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 217
    const/4 v0, 0x0

    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lcom/sgiggle/production/model/Product;

    if-eqz v1, :cond_0

    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/Product;

    .line 222
    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;->this$0:Lcom/sgiggle/production/adapter/PaymentAdapter;

    #getter for: Lcom/sgiggle/production/adapter/PaymentAdapter;->m_listener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;
    invoke-static {v1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->access$000(Lcom/sgiggle/production/adapter/PaymentAdapter;)Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 223
    iget-object v1, p0, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;->this$0:Lcom/sgiggle/production/adapter/PaymentAdapter;

    #getter for: Lcom/sgiggle/production/adapter/PaymentAdapter;->m_listener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;
    invoke-static {v1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->access$000(Lcom/sgiggle/production/adapter/PaymentAdapter;)Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;->onPurchase(Lcom/sgiggle/production/model/Product;)V

    .line 225
    :cond_1
    return-void
.end method
