.class public Lcom/sgiggle/production/adapter/PaymentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "PaymentAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;,
        Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;,
        Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/production/model/Product;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.PaymentAdapter"

.field private static final TRIAL_SECTION_SUPPORTED:Z = false

.field private static final VIEW_TYPE_COUNT:I = 0x3

.field private static final VIEW_TYPE_HEADER_SUBSCRIPTIONS:I = 0x2

.field private static final VIEW_TYPE_HEADER_TRIAL:I = 0x1

.field private static final VIEW_TYPE_NORMAL:I


# instance fields
.field private m_data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/Product;",
            ">;"
        }
    .end annotation
.end field

.field private m_enableAllItems:Z

.field private m_inflater:Landroid/view/LayoutInflater;

.field private m_listener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;

.field private final m_purchaseClickListener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/Product;",
            ">;",
            "Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 23
    new-instance v0, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;-><init>(Lcom/sgiggle/production/adapter/PaymentAdapter;)V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_purchaseClickListener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_enableAllItems:Z

    .line 43
    iput-object p2, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    .line 44
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 45
    iput-object p3, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_listener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/adapter/PaymentAdapter;)Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;
    .locals 1
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_listener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;

    return-object v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public enableAllItems(Z)V
    .locals 3
    .parameter

    .prologue
    .line 229
    const-string v0, "Tango.PaymentAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Enable all items:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_enableAllItems:Z

    .line 233
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/PaymentAdapter;->notifyDataSetChanged()V

    .line 234
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 56
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Lcom/sgiggle/production/model/Product;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/Product;

    .line 75
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->getItem(I)Lcom/sgiggle/production/model/Product;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 80
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .parameter

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 149
    if-nez p2, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 152
    new-instance v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;-><init>()V

    .line 153
    const v0, 0x7f0a017c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentDescription:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f0a017b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentPlan:Landroid/widget/TextView;

    .line 155
    const v0, 0x7f0a0034

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_error:Landroid/widget/TextView;

    .line 156
    const v0, 0x7f0a017d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    .line 157
    iget-object v0, v2, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_purchaseClickListener:Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseOnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 159
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v2

    .line 164
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->getItem(I)Lcom/sgiggle/production/model/Product;

    move-result-object v2

    .line 166
    if-nez v2, :cond_2

    .line 167
    if-nez p1, :cond_0

    .line 168
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentDescription:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentPlan:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 170
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 172
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_error:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    iget-object v0, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_error:Landroid/widget/TextView;

    const v2, 0x7f0900f7

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    move-object v0, v1

    .line 204
    :goto_1
    return-object v0

    .line 161
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;

    move-object v1, p2

    goto :goto_0

    .line 178
    :cond_2
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentDescription:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getProductDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentPlan:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getProductName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 182
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentDescription:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_paymentPlan:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 184
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 185
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_error:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq v3, v4, :cond_3

    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq v3, v4, :cond_3

    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/production/payments/Constants$PurchaseState;->REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v3, v4, :cond_6

    .line 190
    :cond_3
    iget-object v3, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_enableAllItems:Z

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 191
    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 192
    iget-object v0, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getFormattedPrice()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_2
    move-object v0, v1

    .line 204
    goto :goto_1

    .line 194
    :cond_5
    iget-object v0, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 196
    :cond_6
    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v3

    sget-object v4, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PENDING:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v3, v4, :cond_7

    .line 197
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 198
    iget-object v0, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    const-string v2, "Pending"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 199
    :cond_7
    invoke-virtual {v2}, Lcom/sgiggle/production/model/Product;->getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v2, v3, :cond_4

    .line 200
    iget-object v2, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    invoke-virtual {v2, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 201
    iget-object v0, v0, Lcom/sgiggle/production/adapter/PaymentAdapter$ViewHolder;->m_purchase:Landroid/widget/Button;

    const-string v2, "Purchased"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 128
    if-nez p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->getItem(I)Lcom/sgiggle/production/model/Product;

    move-result-object v0

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public updateItemStatus(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)Lcom/sgiggle/production/model/Product;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sgiggle/production/adapter/PaymentAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/Product;

    .line 104
    invoke-virtual {v0}, Lcom/sgiggle/production/model/Product;->getProductMarketId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    invoke-virtual {v0, p2}, Lcom/sgiggle/production/model/Product;->setPurchaseState(Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 106
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/PaymentAdapter;->notifyDataSetChanged()V

    .line 112
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
