.class Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;
.super Ljava/lang/Object;
.source "SelectContactListAdapter.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/adapter/SelectContactListAdapter;->fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 178
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 179
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    if-eq v1, p2, :cond_0

    .line 180
    iget-object v1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
    invoke-static {v1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$200(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;->trySelectContacts(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    iput-boolean p2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    .line 182
    iget-object v1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
    invoke-static {v1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$200(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;->onContactSelectionChanged(Lcom/sgiggle/production/Utils$UIContact;Z)V

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 186
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 187
    invoke-virtual {p1, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0
.end method
