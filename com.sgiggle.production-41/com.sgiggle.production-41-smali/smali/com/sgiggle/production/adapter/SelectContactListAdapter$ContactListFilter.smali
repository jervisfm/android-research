.class Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;
.super Landroid/widget/Filter;
.source "SelectContactListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/adapter/SelectContactListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContactListFilter"
.end annotation


# instance fields
.field private m_listener:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;

.field final synthetic this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/adapter/SelectContactListAdapter;Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    .line 50
    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 51
    iput-object p2, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->m_listener:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;

    .line 52
    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7
    .parameter

    .prologue
    .line 56
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 58
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 62
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filterResultLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$000(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/lang/Object;

    move-result-object v0

    monitor-enter v0

    .line 64
    :try_start_0
    iget-object v2, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;
    invoke-static {v2}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$100(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 65
    iget-object v2, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;
    invoke-static {v2}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$100(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 66
    monitor-exit v0

    .line 85
    :goto_0
    return-object v1

    .line 66
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filterResultLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$000(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 69
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;
    invoke-static {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$100(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 72
    iget-object v6, v0, Lcom/sgiggle/production/Utils$UIContact;->m_firstName:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, v0, Lcom/sgiggle/production/Utils$UIContact;->m_lastName:Ljava/lang/String;

    invoke-static {v6, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, Lcom/sgiggle/production/Utils;->startWithUpperCase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 75
    :cond_3
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 82
    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 80
    :cond_4
    :try_start_2
    iput-object v4, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 81
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 82
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    #getter for: Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filterResultLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->access$000(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 93
    iget-object v2, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    iget-object v3, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    iget-object v3, v3, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_defaultContactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {v2, v0, v3}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->loadGroups(Ljava/util/List;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;)V

    .line 94
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->this$0:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->notifyDataSetChanged()V

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->m_listener:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;->m_listener:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;

    invoke-interface {v0, p1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;->onFilterDone(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    monitor-exit v1

    .line 100
    return-void

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
