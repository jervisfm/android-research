.class public Lcom/sgiggle/production/adapter/SelectContactListAdapter;
.super Lcom/sgiggle/production/adapter/ContactListAdapter;
.source "SelectContactListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;,
        Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;,
        Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private m_allContacts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_filter:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;

.field private final m_filterResultLock:Ljava/lang/Object;

.field private m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

.field private m_showCheckbox:Z


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 134
    invoke-direct {p0, p3, p1, p2}, Lcom/sgiggle/production/adapter/ContactListAdapter;-><init>(Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;)V

    .line 125
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filterResultLock:Ljava/lang/Object;

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_showCheckbox:Z

    .line 135
    iput-object p4, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filterResultLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    return-object v0
.end method


# virtual methods
.method protected fillContactView(Landroid/view/View;Lcom/sgiggle/production/Utils$UIContact;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 145
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;

    .line 148
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_name:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIContact;->displayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p2}, Lcom/sgiggle/production/Utils$UIContact;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 153
    iget-wide v1, p2, Lcom/sgiggle/production/Utils$UIContact;->m_deviceContactId:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_3

    .line 154
    iget-boolean v1, p2, Lcom/sgiggle/production/Utils$UIContact;->hasPhoto:Z

    if-eqz v1, :cond_3

    .line 155
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 157
    if-nez v1, :cond_0

    .line 159
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v2

    iget-object v3, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, p2, v3}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 165
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 166
    iget-object v2, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 172
    :goto_1
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 173
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    iget-boolean v2, p2, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 174
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p2}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 175
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter$1;-><init>(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 193
    iget-boolean v1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_showCheckbox:Z

    if-eqz v1, :cond_2

    .line 194
    iget-object v0, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 198
    :goto_2
    return-void

    .line 168
    :cond_1
    iget-object v1, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_thumbnail:Landroid/widget/ImageView;

    const v2, 0x7f020090

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 196
    :cond_2
    iget-object v0, v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_2

    :cond_3
    move-object v1, v5

    goto :goto_0
.end method

.method protected getContactView(Lcom/sgiggle/production/Utils$UIContact;)Landroid/view/View;
    .locals 3
    .parameter

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 203
    new-instance v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;-><init>(Lcom/sgiggle/production/adapter/SelectContactListAdapter;)V

    .line 204
    const v0, 0x7f0a0120

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_thumbnail:Landroid/widget/ImageView;

    .line 205
    const v0, 0x7f0a0121

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_name:Landroid/widget/TextView;

    .line 206
    const v0, 0x7f0a0123

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ViewHolder;->m_checkbox:Landroid/widget/CheckBox;

    .line 207
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 208
    return-object v1
.end method

.method public getFilter(Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;)Landroid/widget/Filter;
    .locals 1
    .parameter

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filter:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;-><init>(Lcom/sgiggle/production/adapter/SelectContactListAdapter;Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;)V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filter:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_filter:Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilter;

    return-object v0
.end method

.method public onItemClicked(Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    const v0, 0x7f0a0123

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 220
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 223
    :cond_0
    return-void

    .line 221
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setContacts(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;

    .line 140
    iget-object v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_allContacts:Ljava/util/List;

    iget-object v1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_defaultContactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->loadGroups(Ljava/util/List;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;)V

    .line 141
    return-void
.end method

.method public setShowCheckbox(Z)V
    .locals 1
    .parameter

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_showCheckbox:Z

    if-eq v0, p1, :cond_0

    .line 243
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->m_showCheckbox:Z

    .line 244
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->notifyDataSetChanged()V

    .line 246
    :cond_0
    return-void
.end method
