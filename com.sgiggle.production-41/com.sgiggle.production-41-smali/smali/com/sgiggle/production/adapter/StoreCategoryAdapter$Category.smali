.class public Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;
.super Ljava/lang/Object;
.source "StoreCategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/adapter/StoreCategoryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Category"
.end annotation


# static fields
.field public static final AVATARS:I = 0x2

.field public static final GAMES:I = 0x1

.field public static final SURPRISES:I


# instance fields
.field m_isNewBadgeVisible:Z

.field m_resDesc:I

.field m_resIcon:I

.field m_resTitle:I

.field m_type:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_type:I

    .line 29
    iput p2, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resTitle:I

    .line 30
    iput p3, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resDesc:I

    .line 31
    iput p4, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resIcon:I

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_isNewBadgeVisible:Z

    .line 33
    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_type:I

    return v0
.end method

.method public setNewBadge(Z)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_isNewBadgeVisible:Z

    .line 41
    return-void
.end method
