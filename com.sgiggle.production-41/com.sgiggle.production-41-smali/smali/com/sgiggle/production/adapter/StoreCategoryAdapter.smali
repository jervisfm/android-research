.class public Lcom/sgiggle/production/adapter/StoreCategoryAdapter;
.super Landroid/widget/BaseAdapter;
.source "StoreCategoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;
    }
.end annotation


# instance fields
.field private m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

.field private m_colorDark:I

.field private m_colorLight:I

.field private m_inflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 49
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    new-instance v1, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    const v2, 0x7f0901a0

    const v3, 0x7f0901a1

    const v4, 0x7f0200ae

    invoke-direct {v1, v5, v2, v3, v4}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;-><init>(IIII)V

    aput-object v1, v0, v5

    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    new-instance v1, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    const v2, 0x7f0901a2

    const v3, 0x7f0901a3

    const v4, 0x7f0200ad

    invoke-direct {v1, v6, v2, v3, v4}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;-><init>(IIII)V

    aput-object v1, v0, v6

    .line 55
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    new-instance v1, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    const v2, 0x7f0901a6

    const v3, 0x7f0901a7

    const v4, 0x7f0200ac

    invoke-direct {v1, v7, v2, v3, v4}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;-><init>(IIII)V

    aput-object v1, v0, v7

    .line 57
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_colorLight:I

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_colorDark:I

    .line 60
    return-void
.end method

.method private getColorForPosition(I)I
    .locals 1
    .parameter

    .prologue
    .line 94
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    .line 95
    iget v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_colorLight:I

    .line 96
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_colorDark:I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 74
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 79
    .line 80
    if-nez p2, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030053

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    .line 83
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    .line 84
    const v1, 0x7f0a0031

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v3, v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resTitle:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 85
    const v1, 0x7f0a0032

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v3, v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resDesc:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 86
    const v1, 0x7f0a0030

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v3, v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_resIcon:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 87
    const v1, 0x7f0a0148

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->m_isNewBadgeVisible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 89
    invoke-direct {p0, p1}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->getColorForPosition(I)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 90
    return-object v2

    .line 87
    :cond_0
    const/16 v0, 0x8

    goto :goto_1

    :cond_1
    move-object v2, p2

    goto :goto_0
.end method

.method public setNewBadge(IZ)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->m_categories:[Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->setNewBadge(Z)V

    .line 101
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->notifyDataSetChanged()V

    .line 102
    return-void
.end method
