.class public Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;
.super Lcom/sgiggle/production/adapter/IconListAdapter;
.source "TcMediaSelectorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter$MediaListItem;
    }
.end annotation


# static fields
.field public static final MAKE_AUDIO_CALL:I = 0x4

.field public static final MAKE_VIDEO_CALL:I = 0x3

.field public static final PICK_PHOTO:I = 0x1

.field public static final TAKE_PHOTO:I = 0x0

.field public static final TAKE_VIDEO:I = 0x2


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-static {p1, p2}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->getData(Landroid/content/Context;Z)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/adapter/IconListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 41
    return-void
.end method

.method protected static addItem(Ljava/util/List;Ljava/lang/String;II)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/adapter/IconListAdapter$IconListItem;",
            ">;",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 73
    new-instance v0, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter$MediaListItem;

    invoke-direct {v0, p1, p2, p3}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter$MediaListItem;-><init>(Ljava/lang/String;II)V

    .line 74
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    return-void
.end method

.method protected static getData(Landroid/content/Context;Z)Ljava/util/List;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/adapter/IconListAdapter$IconListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x4

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 50
    const v1, 0x7f09018e

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 53
    const v1, 0x7f09018f

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c1

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 56
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->videomailSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    const v1, 0x7f090190

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c3

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 61
    :cond_0
    if-eqz p1, :cond_1

    .line 62
    const v1, 0x7f090191

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200c0

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 65
    const v1, 0x7f090192

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0200bf

    invoke-static {v0, v1, v2, v4}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->addItem(Ljava/util/List;Ljava/lang/String;II)V

    .line 69
    :cond_1
    return-object v0
.end method


# virtual methods
.method public buttonToCommand(I)I
    .locals 1
    .parameter

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter$MediaListItem;

    .line 45
    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter$MediaListItem;->getCommand()I

    move-result v0

    return v0
.end method
