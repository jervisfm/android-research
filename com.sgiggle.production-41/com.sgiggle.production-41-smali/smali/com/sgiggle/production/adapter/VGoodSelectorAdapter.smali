.class public Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;
.super Landroid/widget/BaseAdapter;
.source "VGoodSelectorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/VGoodSelectorAdapter$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VGoodSelectorAdapter"

.field public static final filterImageResId:[I


# instance fields
.field mEmptySlotDrawable:Landroid/graphics/drawable/Drawable;

.field private mHighlightBackground:Landroid/graphics/drawable/Drawable;

.field private mIsReversed:Z

.field private mIsRotated:Z

.field private mShowEmptySlots:Z

.field m_buttonEnabled:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field m_cache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field m_context:Landroid/content/Context;

.field m_data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field

.field m_emptyCount:I

.field private m_enabled:Z

.field private m_highlighted:I

.field private m_highlightedWithBackground:I

.field m_inflater:Landroid/view/LayoutInflater;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->filterImageResId:[I

    return-void

    nop

    :array_0
    .array-data 0x4
        0x95t 0x0t 0x2t 0x7ft
        0x94t 0x0t 0x2t 0x7ft
        0x96t 0x0t 0x2t 0x7ft
        0x93t 0x0t 0x2t 0x7ft
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_buttonEnabled:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_cache:Ljava/util/HashMap;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_enabled:Z

    .line 49
    iput-boolean v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mShowEmptySlots:Z

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlightedWithBackground:I

    .line 57
    iput-boolean v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsReversed:Z

    .line 59
    iput-boolean v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsRotated:Z

    .line 105
    iput-object p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_context:Landroid/content/Context;

    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 107
    return-void
.end method

.method private getDataCount()I
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 163
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 271
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 285
    :goto_0
    return-object v0

    .line 276
    :cond_0
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsRotated:Z

    if-eqz v1, :cond_1

    .line 278
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 281
    :cond_1
    if-nez v0, :cond_2

    move-object v0, v2

    .line 282
    goto :goto_0

    .line 284
    :cond_2
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object v0, v1

    .line 285
    goto :goto_0
.end method

.method private getDrawableForPosition(I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_cache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 219
    if-nez v0, :cond_0

    .line 220
    invoke-direct {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getEmptySlotDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 225
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_cache:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    :cond_0
    return-object v0

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getImageList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDrawable(Ljava/util/List;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getEmptySlotDrawable()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mEmptySlotDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 237
    const v1, 0x7f02010f

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 238
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsRotated:Z

    if-eqz v2, :cond_0

    .line 239
    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 240
    :cond_0
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mEmptySlotDrawable:Landroid/graphics/drawable/Drawable;

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mEmptySlotDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getHighlightBackgroundDrawable()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mHighlightBackground:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 248
    const v1, 0x7f02000d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 249
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsRotated:Z

    if-eqz v2, :cond_0

    .line 250
    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 251
    :cond_0
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v0, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mHighlightBackground:Landroid/graphics/drawable/Drawable;

    .line 253
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mHighlightBackground:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private getStateSet(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)[I
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 309
    new-array v0, v3, [I

    .line 310
    sget-object v1, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$VGoodSelectorImage$ImageState:[I

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 324
    :goto_0
    return-object v0

    .line 312
    :pswitch_0
    new-array v0, v4, [I

    const v1, -0x101009e

    aput v1, v0, v3

    goto :goto_0

    .line 315
    :pswitch_1
    new-array v0, v5, [I

    fill-array-data v0, :array_0

    goto :goto_0

    .line 318
    :pswitch_2
    new-array v0, v5, [I

    fill-array-data v0, :array_1

    goto :goto_0

    .line 321
    :pswitch_3
    new-array v0, v4, [I

    const v1, 0x10100a0

    aput v1, v0, v3

    goto :goto_0

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 315
    :array_0
    .array-data 0x4
        0x9et 0x0t 0x1t 0x1t
        0x59t 0xfft 0xfet 0xfet
    .end array-data

    .line 318
    :array_1
    .array-data 0x4
        0x9et 0x0t 0x1t 0x1t
        0xa7t 0x0t 0x1t 0x1t
    .end array-data
.end method

.method private isEmptySlot(I)Z
    .locals 1
    .parameter

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDataCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .parameter

    .prologue
    .line 293
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    .line 294
    if-nez v0, :cond_0

    .line 295
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 296
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 297
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 298
    invoke-static {v1, v2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 299
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getDensity()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    .line 300
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 301
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 302
    neg-int v2, v2

    int-to-float v2, v2

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 303
    const/high16 v2, -0x3d4c

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 304
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 305
    return-object v0
.end method


# virtual methods
.method public clearHighlight()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlighted:I

    .line 137
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 138
    return-void
.end method

.method public clearHightlightWithBackground()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlightedWithBackground:I

    .line 147
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 148
    return-void
.end method

.method public enableItem(IZ)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 115
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getPos(I)I

    move-result v0

    .line 116
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_buttonEnabled:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_cache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 120
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mShowEmptySlots:Z

    if-eqz v0, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDataCount()I

    move-result v0

    iget v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_emptyCount:I

    add-int/2addr v0, v1

    .line 155
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDataCount()I

    move-result v0

    goto :goto_0
.end method

.method public getDrawable(Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 257
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    .line 259
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 260
    invoke-direct {p0, v3}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 261
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getImageType()Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;

    move-result-object v0

    .line 262
    if-eqz v3, :cond_0

    .line 263
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getStateSet(Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage$ImageState;)[I

    move-result-object v0

    .line 264
    invoke-virtual {v1, v0, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 267
    :cond_1
    return-object v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 168
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getPos(I)I

    move-result v0

    .line 169
    const/4 v1, 0x0

    .line 170
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 172
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getPos(I)I

    move-result v0

    .line 178
    int-to-long v0, v0

    return-wide v0
.end method

.method public getPos(I)I
    .locals 2
    .parameter

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsReversed:Z

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getCount()I

    move-result v0

    sub-int/2addr v0, p1

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    .line 96
    :goto_0
    return v0

    :cond_0
    move v0, p1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f0a0004

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 183
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getPos(I)I

    move-result v1

    .line 186
    if-nez p2, :cond_3

    .line 187
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03005c

    invoke-virtual {v0, v2, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 188
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    move-object v3, v2

    move-object v2, v0

    .line 194
    :goto_0
    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v0

    sget-object v4, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-ne v0, v4, :cond_4

    .line 195
    sget-object v4, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->filterImageResId:[I

    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v5

    long-to-int v0, v5

    aget v0, v4, v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 201
    :goto_1
    iget v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlighted:I

    if-ne v1, v0, :cond_5

    move v4, v8

    .line 202
    :goto_2
    iget v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlightedWithBackground:I

    if-ne v1, v0, :cond_6

    move v5, v8

    .line 203
    :goto_3
    if-nez v4, :cond_0

    if-eqz v5, :cond_7

    :cond_0
    move v0, v8

    :goto_4
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setPressed(Z)V

    .line 205
    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 206
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_buttonEnabled:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 207
    :goto_5
    iget-boolean v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_enabled:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_2

    :cond_1
    if-eqz v4, :cond_8

    :cond_2
    move v0, v8

    :goto_6
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 208
    if-eqz v5, :cond_9

    .line 209
    invoke-direct {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getHighlightBackgroundDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 213
    :goto_7
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 214
    return-object v3

    .line 191
    :cond_3
    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    move-object v2, v0

    move-object v3, p2

    goto :goto_0

    .line 197
    :cond_4
    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getDrawableForPosition(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 198
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_5
    move v4, v7

    .line 201
    goto :goto_2

    :cond_6
    move v5, v7

    .line 202
    goto :goto_3

    :cond_7
    move v0, v7

    .line 203
    goto :goto_4

    :cond_8
    move v0, v7

    .line 207
    goto :goto_6

    .line 211
    :cond_9
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_7

    :cond_a
    move v0, v7

    goto :goto_5
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_enabled:Z

    return v0
.end method

.method public isItemEnabled(I)Z
    .locals 2
    .parameter

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->getPos(I)I

    move-result v0

    .line 124
    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->isEmptySlot(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_buttonEnabled:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 127
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setData(Ljava/util/List;Z)V
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 63
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_cache:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlighted:I

    .line 65
    iput-boolean v4, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mShowEmptySlots:Z

    .line 66
    if-eqz p1, :cond_2

    .line 67
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 68
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getImageCount()I

    move-result v2

    if-lez v2, :cond_0

    if-nez p2, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getType()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;->FILTER:Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic$Type;

    if-eq v2, v3, :cond_0

    .line 69
    :cond_1
    iget-object v2, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_data:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_buttonEnabled:Ljava/util/List;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEnabled(Z)V

    .line 75
    return-void
.end method

.method public setEmptyCount(I)V
    .locals 1
    .parameter

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_emptyCount:I

    .line 101
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 102
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .parameter

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_enabled:Z

    .line 111
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 112
    return-void
.end method

.method public setHighlight(I)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlighted:I

    .line 132
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 133
    return-void
.end method

.method public setHightlightWithBackground(I)V
    .locals 0
    .parameter

    .prologue
    .line 141
    iput p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->m_highlightedWithBackground:I

    .line 142
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 143
    return-void
.end method

.method public setReversed(Z)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsReversed:Z

    .line 84
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 85
    return-void
.end method

.method public setRotated(Z)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mIsRotated:Z

    .line 89
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 90
    return-void
.end method

.method public setShowEmptySlots(Z)V
    .locals 0
    .parameter

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->mShowEmptySlots:Z

    .line 79
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->notifyDataSetChanged()V

    .line 80
    return-void
.end method
