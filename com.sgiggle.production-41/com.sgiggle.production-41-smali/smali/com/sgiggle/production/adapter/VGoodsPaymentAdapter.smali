.class public Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;
.super Landroid/widget/ArrayAdapter;
.source "VGoodsPaymentAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;,
        Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private iconsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private m_clickListener:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;

.field private m_inflater:Landroid/view/LayoutInflater;

.field private m_isBillingSupported:Z

.field private m_isPurchaseEnabled:Z

.field private m_rowColorDark:I

.field private m_rowColorLight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 41
    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isBillingSupported:Z

    .line 43
    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isPurchaseEnabled:Z

    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    .line 52
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_rowColorLight:I

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070021

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_rowColorDark:I

    .line 55
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isPurchaseEnabled:Z

    iput-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isBillingSupported:Z

    .line 56
    return-void
.end method

.method private getColorForPosition(I)I
    .locals 1
    .parameter

    .prologue
    .line 146
    rem-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    .line 147
    iget v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_rowColorLight:I

    .line 148
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_rowColorDark:I

    goto :goto_0
.end method

.method private getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 126
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 127
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v2

    .line 142
    :goto_0
    return-object v0

    .line 131
    :cond_0
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 133
    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 134
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 135
    invoke-static {p1, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 137
    if-nez v0, :cond_1

    move-object v0, v2

    .line 138
    goto :goto_0

    .line 140
    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 141
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setTargetDensity(I)V

    move-object v0, v1

    .line 142
    goto :goto_0
.end method

.method private getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 152
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasPrice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 155
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Check price"

    goto :goto_0
.end method

.method private notifyItemClicked(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;->onClick(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 172
    :cond_0
    return-void
.end method

.method private setPurchaseEnabled(Z)V
    .locals 0
    .parameter

    .prologue
    .line 187
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isPurchaseEnabled:Z

    .line 188
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->notifyDataSetChanged()V

    .line 189
    return-void
.end method


# virtual methods
.method public getOnClickListener()Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;

    return-object v0
.end method

.method public getProductItemFromMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 4
    .parameter

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getCount()I

    move-result v1

    .line 193
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v1, :cond_1

    .line 194
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 195
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 199
    :goto_1
    return-object v0

    .line 193
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const v7, 0x7f0200d3

    .line 68
    .line 69
    if-nez p2, :cond_7

    .line 70
    iget-object v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030060

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 71
    new-instance v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;

    invoke-direct {v2}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;-><init>()V

    .line 72
    const v0, 0x7f0a0031

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f0a0032

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->desc:Landroid/widget/TextView;

    .line 74
    const v0, 0x7f0a0033

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    .line 75
    const v0, 0x7f0a0029

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    .line 76
    const v0, 0x7f0a0030

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    .line 77
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v3, v1

    .line 80
    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;

    .line 81
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 83
    invoke-direct {p0, p1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getColorForPosition(I)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 85
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->desc:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 89
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->demoBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 95
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v2

    iget-object v2, v2, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 96
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v2, :cond_0

    sget-object v4, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq v2, v4, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isPurchaseEnabled:Z

    if-nez v2, :cond_4

    .line 97
    :cond_1
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 102
    :goto_1
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 105
    iget-object v2, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/SoftReference;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_3

    .line 106
    :cond_2
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getImagePath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 107
    if-eqz v2, :cond_3

    .line 108
    iget-object v4, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->iconsMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v6, Ljava/lang/ref/SoftReference;

    invoke-direct {v6, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    :cond_3
    if-eqz v2, :cond_5

    .line 112
    iget-object v4, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 120
    :goto_2
    iget-object v0, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    invoke-direct {p0, v1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getPriceText(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-object v3

    .line 99
    :cond_4
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->buyBtn:Landroid/widget/Button;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_1

    .line 114
    :cond_5
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 117
    :cond_6
    iget-object v2, v0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$ViewHolder;->marketing_icon:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    :cond_7
    move-object v3, p2

    goto/16 :goto_0
.end method

.method public isBillingSupported()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isBillingSupported:Z

    return v0
.end method

.method public isPurchaseEnabled()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isPurchaseEnabled:Z

    return v0
.end method

.method public markPurchased(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 242
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getProductItemFromMarketId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    .line 245
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v1

    .line 246
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v2

    invoke-static {v2}, Lcom/sgiggle/xmpp/SessionMessages$Price;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Price;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    move-result-object v2

    .line 247
    invoke-virtual {v2, p2}, Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;->setLabel(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;

    .line 248
    invoke-virtual {v1, v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    .line 249
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    .line 251
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v2

    .line 252
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->remove(Ljava/lang/Object;)V

    .line 253
    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->insert(Ljava/lang/Object;I)V

    .line 255
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 162
    instance-of v1, v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    if-eqz v1, :cond_0

    .line 163
    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 164
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->notifyItemClicked(ILcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)V

    .line 166
    :cond_0
    return-void
.end method

.method public setBillingSupported(Z)V
    .locals 0
    .parameter

    .prologue
    .line 207
    iput-boolean p1, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isBillingSupported:Z

    .line 208
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->updateUIState()V

    .line 209
    return-void
.end method

.method public setOnClickListener(Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;)V
    .locals 0
    .parameter

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_clickListener:Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$OnClickListener;

    .line 180
    return-void
.end method

.method public setProducts(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setNotifyOnChange(Z)V

    .line 222
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->clear()V

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 226
    new-instance v1, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter$1;-><init>(Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 233
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 234
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 236
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->updateUIState()V

    .line 237
    invoke-virtual {p0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->notifyDataSetChanged()V

    .line 239
    :cond_1
    return-void
.end method

.method public updateUIState()V
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->m_isBillingSupported:Z

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setPurchaseEnabled(Z)V

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/adapter/VGoodsPaymentAdapter;->setPurchaseEnabled(Z)V

    goto :goto_0
.end method
