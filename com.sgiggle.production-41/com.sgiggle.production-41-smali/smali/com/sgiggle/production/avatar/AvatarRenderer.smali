.class public Lcom/sgiggle/production/avatar/AvatarRenderer;
.super Ljava/lang/Object;
.source "AvatarRenderer.java"

# interfaces
.implements Lcom/sgiggle/cafe/vgood/CafeRenderer$CafeViewRenderer;


# static fields
.field private static final TAG:Ljava/lang/String; = "AVATAR_RENDERER"


# instance fields
.field private mAutoAdjustRotation:Z

.field private mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

.field private mCallSession:Lcom/sgiggle/production/CallSession;

.field private mCurrentClip:Ljava/lang/String;

.field private mGLRendererMode:Z

.field private mIsCafeReady:Z

.field private mIsLocal:Z

.field private volatile mLastSurpriseId:I

.field private mRequireClear:Z

.field private mRequireUpdate:Z

.field private mRotate:Z

.field private mSurfaceRect:Landroid/graphics/Rect;

.field private mViewId:I


# direct methods
.method public constructor <init>(IZLcom/sgiggle/production/CallSession;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;-><init>(IZLcom/sgiggle/production/CallSession;Z)V

    .line 39
    return-void
.end method

.method public constructor <init>(IZLcom/sgiggle/production/CallSession;Z)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    .line 17
    iput-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    .line 19
    iput v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    .line 27
    iput-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    .line 28
    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRotate:Z

    .line 29
    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAutoAdjustRotation:Z

    .line 31
    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireUpdate:Z

    .line 32
    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireClear:Z

    .line 33
    iput-boolean v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    .line 42
    iput v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    .line 43
    iput p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    .line 44
    iput-boolean p2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    .line 45
    iput-object p3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    .line 46
    iput-boolean p4, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    .line 47
    iput-boolean v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsCafeReady:Z

    .line 48
    return-void
.end method

.method private hasValidAvatarInfo()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 176
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    if-nez v0, :cond_0

    move v0, v1

    .line 180
    :goto_0
    return v0

    .line 178
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    .line 180
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private isCafeReady()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-nez v0, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 162
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsCafeReady:Z

    goto :goto_0
.end method

.method private isRendererAvailable()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_0

    .line 151
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isCafeReady()Z

    move-result v0

    .line 153
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isSurfaceReady()Z

    move-result v0

    goto :goto_0
.end method

.method private isSurfaceReady()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 166
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_0

    move v0, v1

    .line 168
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needRotate()Z
    .locals 2

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAutoAdjustRotation:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gt v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRotate:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needStartAvatar()Z
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isRendererAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->hasValidAvatarInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private surfaceToCafe(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6
    .parameter

    .prologue
    .line 196
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v3, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 197
    return-object v0
.end method


# virtual methods
.method public getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    if-nez v0, :cond_0

    .line 300
    const/4 v0, 0x0

    .line 305
    :goto_0
    return-object v0

    .line 301
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-eqz v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    goto :goto_0

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public handleRenderRequest(Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;)V
    .locals 5
    .parameter

    .prologue
    .line 245
    if-nez p1, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 247
    :cond_1
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    if-gez v0, :cond_2

    .line 248
    const-string v0, "AVATAR_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ignore render request while surprise not started "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 251
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTrack()Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$AvatarRenderRequestEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimation()Ljava/lang/String;

    move-result-object v0

    .line 253
    const-string v2, "AVATAR_RENDERER"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HANDLE RENDER REQUEST ,track:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ,clip:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCurrentClip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 255
    iput-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCurrentClip:Ljava/lang/String;

    .line 256
    iget v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/sgiggle/cafe/vgood/CafeMgr;->PlayClip(ILjava/lang/String;Z)V

    .line 257
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->forceUpdateAvatarTrack(Z)V

    goto :goto_0
.end method

.method public isAvatarActived()Z
    .locals 2

    .prologue
    .line 138
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 139
    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    if-eqz v1, :cond_3

    .line 140
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_avatarDirection:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    .line 144
    :cond_0
    :goto_0
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_2

    iget-boolean v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-nez v1, :cond_4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_4

    :cond_2
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 141
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    if-eqz v1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isInPIPMode()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    move v0, v1

    .line 118
    :goto_0
    return v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->pipSwappable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 112
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->isPipSwapped()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->isPipSwapped()Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 118
    goto :goto_0
.end method

.method public needDrawPIPVideoBg()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 123
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    if-nez v0, :cond_0

    move v0, v2

    .line 125
    :goto_0
    return v0

    .line 124
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 125
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    iget-object v0, v0, Lcom/sgiggle/production/CallSession;->m_videoDirection:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;->SEND:Lcom/sgiggle/xmpp/SessionMessages$MediaSessionPayload$Direction;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mCallSession:Lcom/sgiggle/production/CallSession;

    invoke-virtual {v0}, Lcom/sgiggle/production/CallSession;->isPipSwapped()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public onAvatarChanged(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V
    .locals 2
    .parameter

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    .line 264
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v0

    if-nez v0, :cond_1

    .line 265
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 270
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    iget v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    invoke-static {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeMgr;->setAvatarSurpriseId(ZI)V

    .line 271
    return-void

    .line 266
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needStartAvatar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    goto :goto_0
.end method

.method public onCafeFreed()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsCafeReady:Z

    .line 99
    return-void
.end method

.method public onCafeInitialized()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsCafeReady:Z

    .line 93
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needStartAvatar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    .line 95
    :cond_0
    return-void
.end method

.method public onSurfaceChanged(IIZZ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 54
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "GL2.0 should not call onSurfaceChanged "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    const-string v0, "AVATAR_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " w "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " rotate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " autoAdjustRotation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iput-boolean p3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRotate:Z

    .line 60
    iput-boolean p4, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAutoAdjustRotation:Z

    .line 61
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    .line 62
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setRenderView()V

    .line 63
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needStartAvatar()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    goto :goto_0
.end method

.method public onSurfaceCreated()V
    .locals 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_0

    .line 71
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "GL2.0 should not call onSurfaceCreated "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->onSurfaceDestroyed()V

    goto :goto_0
.end method

.method public onSurfaceDestroyed()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "onSurfaceDestroyed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    .line 135
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 136
    return-void
.end method

.method public render()V
    .locals 4

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v0

    .line 85
    iget-boolean v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireUpdate:Z

    if-eqz v1, :cond_0

    .line 86
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->updateAvatarTracksVisibility()V

    .line 88
    :cond_0
    iget v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    iget-boolean v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireUpdate:Z

    iget-boolean v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireClear:Z

    invoke-static {v1, v2, v3, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->RenderView(IZZZ)V

    .line 89
    return-void
.end method

.method public setClear(Z)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireClear:Z

    .line 107
    return-void
.end method

.method public setRenderView()V
    .locals 9

    .prologue
    const/16 v3, 0xa

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/high16 v7, 0x3f80

    const/4 v6, 0x0

    .line 202
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mGLRendererMode:Z

    if-eqz v0, :cond_1

    .line 203
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "GL2.0 should not call onSurfaceChanged "

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isSurfaceReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needRotate()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 211
    const/16 v0, 0x5a

    move v5, v0

    .line 213
    :goto_1
    const/4 v0, 0x1

    .line 214
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needRotate()Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v2

    .line 218
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isAvatarActived()Z

    move-result v1

    if-nez v1, :cond_4

    .line 220
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needDrawPIPVideoBg()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 221
    invoke-static {v0, v3}, Lcom/sgiggle/VideoCapture/VideoView;->getBorderRect(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 222
    invoke-direct {p0, v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->surfaceToCafe(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 223
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    iget v1, v4, Landroid/graphics/Rect;->left:I

    add-int/lit8 v1, v1, 0x1

    iget v2, v4, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v3, v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v8

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderView(IIIIII)V

    .line 224
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    invoke-static {v0, v6, v6, v6, v7}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderClearColor(IFFFF)F

    goto :goto_0

    .line 226
    :cond_3
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderView(IIIIII)V

    .line 227
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    invoke-static {v0, v6, v6, v6, v6}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderClearColor(IFFFF)F

    goto :goto_0

    .line 232
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->isInPIPMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 233
    invoke-static {v0, v3}, Lcom/sgiggle/VideoCapture/VideoView;->getBorderRect(II)Landroid/graphics/Rect;

    move-result-object v0

    .line 234
    invoke-direct {p0, v0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->surfaceToCafe(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    .line 235
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    iget v1, v4, Landroid/graphics/Rect;->left:I

    iget v2, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v3

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderView(IIIIII)V

    .line 241
    :goto_2
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    invoke-static {v0, v6, v6, v6, v7}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderClearColor(IFFFF)F

    goto/16 :goto_0

    .line 238
    :cond_5
    const-string v0, "AVATAR_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set render view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " w "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    iget-object v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mSurfaceRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetRenderView(IIIIII)V

    goto :goto_2

    :cond_6
    move v5, v2

    goto/16 :goto_1
.end method

.method public setUpdate(Z)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-boolean p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mRequireUpdate:Z

    .line 103
    return-void
.end method

.method public startAvatar()V
    .locals 8

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->needStartAvatar()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "start null avatar!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 330
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 316
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->setRenderView()V

    .line 318
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    .line 320
    const-string v1, "AVATAR_RENDERER"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startAvatar animation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mediaDir = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDir()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mediaFile="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFile()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDir()Ljava/lang/String;

    move-result-object v1

    .line 324
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFile()Ljava/lang/String;

    move-result-object v2

    .line 325
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimation()Ljava/lang/String;

    move-result-object v3

    .line 326
    invoke-static {v1, v2}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mViewId:I

    const/4 v4, 0x1

    const-wide/16 v5, 0x0

    iget-boolean v7, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    invoke-static/range {v0 .. v7}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartAvatarSurpriseForClip(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJZ)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    .line 328
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    iget v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    invoke-static {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeMgr;->setAvatarSurpriseId(ZI)V

    .line 329
    const-string v0, "AVATAR_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SET surprise  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " clip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " started."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public stopAvatarAnimation()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 335
    const-string v0, "AVATAR_RENDERER"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop surprise  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    if-lez v0, :cond_0

    .line 337
    iget-boolean v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    invoke-static {v0, v3}, Lcom/sgiggle/cafe/vgood/CafeMgr;->setAvatarSurpriseId(ZI)V

    .line 338
    iget v0, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    invoke-static {v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopSurprise(I)V

    .line 339
    iput v3, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mLastSurpriseId:I

    .line 341
    :cond_0
    return-void
.end method

.method public switchAvatar(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)V
    .locals 4
    .parameter

    .prologue
    .line 274
    const-string v0, "AVATAR_RENDERER"

    const-string v1, "switchAvatar"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    .line 276
    iget-boolean v1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mIsLocal:Z

    if-eqz v1, :cond_3

    .line 277
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 278
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 280
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 281
    iput-object p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    .line 282
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    .line 296
    :cond_2
    :goto_0
    return-void

    .line 287
    :cond_3
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-eq v0, v1, :cond_4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    if-ne v0, v1, :cond_2

    .line 288
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    .line 289
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->getAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 290
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->stopAvatarAnimation()V

    .line 291
    iput-object p1, p0, Lcom/sgiggle/production/avatar/AvatarRenderer;->mAvatarControlPayload:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    .line 292
    invoke-virtual {p0}, Lcom/sgiggle/production/avatar/AvatarRenderer;->startAvatar()V

    goto :goto_0
.end method
