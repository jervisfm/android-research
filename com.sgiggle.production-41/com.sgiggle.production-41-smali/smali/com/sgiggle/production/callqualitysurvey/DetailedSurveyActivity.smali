.class public Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "DetailedSurveyActivity.java"


# instance fields
.field final TAG:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 23
    const/16 v0, 0x56

    iput v0, p0, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->TAG:I

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->send()V

    return-void
.end method

.method private getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;
    .locals 1
    .parameter

    .prologue
    .line 80
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 81
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    goto :goto_0
.end method

.method private send()V
    .locals 12

    .prologue
    .line 56
    const v0, 0x7f0a004d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    .line 59
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;

    const/4 v1, -0x1

    const v2, 0x7f0a0045

    invoke-direct {p0, v2}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const v4, 0x7f0a0046

    invoke-direct {p0, v4}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v4

    const v5, 0x7f0a0047

    invoke-direct {p0, v5}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v5

    const v6, 0x7f0a0049

    invoke-direct {p0, v6}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v6

    const v7, 0x7f0a0048

    invoke-direct {p0, v7}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v7

    const v8, 0x7f0a004a

    invoke-direct {p0, v8}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v8

    sget-object v9, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const v10, 0x7f0a004b

    invoke-direct {p0, v10}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getAnswer(I)Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    move-result-object v10

    invoke-direct/range {v0 .. v11}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;-><init>(ILcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 72
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 76
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->finish()V

    .line 77
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 3

    .prologue
    .line 87
    const/16 v0, 0x56

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    .line 88
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 90
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->finish()V

    .line 91
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f03000d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->setContentView(I)V

    .line 30
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 32
    const v0, 0x7f0a0042

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity$1;-><init>(Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 52
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 53
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 44
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 47
    :cond_0
    return-void
.end method
