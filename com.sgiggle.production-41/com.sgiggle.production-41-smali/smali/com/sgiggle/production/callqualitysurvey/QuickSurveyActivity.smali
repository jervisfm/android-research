.class public Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "QuickSurveyActivity.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final TAG:I

.field private m_spannedSurveyDetailsChosen:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 26
    const/16 v0, 0x56

    iput v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->TAG:I

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->onSelectionMade(I)V

    return-void
.end method

.method private onDontShowAgain()V
    .locals 4

    .prologue
    .line 109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    .line 110
    const/16 v0, 0x56

    const-string v1, "onDontShowAgain"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 111
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$DisablePostCallMessage;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;->POSTCALL_CALLQUALITY:Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisablePostCallMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$PostCallContentType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 116
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->finish()V

    .line 117
    return-void
.end method

.method private onMoreDetails()V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    .line 121
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/callqualitysurvey/DetailedSurveyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->startActivity(Landroid/content/Intent;)V

    .line 122
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->finish()V

    .line 123
    return-void
.end method

.method private onRating(I)V
    .locals 12
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 73
    iput-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    .line 76
    packed-switch p1, :pswitch_data_0

    .line 87
    const/16 v0, 0x56

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid rating value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    .line 88
    const/4 v0, -0x2

    move v1, v0

    .line 92
    :goto_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;

    sget-object v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v3, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v4, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v5, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v6, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v7, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v8, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v9, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    sget-object v10, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;

    const-string v11, ""

    invoke-direct/range {v0 .. v11}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage;-><init>(ILcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Lcom/sgiggle/media_engine/MediaEngineMessage$CallQualitySurveyDataMessage$AgreeAnswerOptions;Ljava/lang/String;)V

    .line 99
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 101
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 105
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->finish()V

    .line 106
    return-void

    :pswitch_0
    move v1, v0

    .line 79
    goto :goto_0

    .line 81
    :pswitch_1
    const/4 v0, 0x2

    move v1, v0

    .line 82
    goto :goto_0

    .line 84
    :pswitch_2
    const/4 v0, 0x4

    move v1, v0

    .line 85
    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private onSelectionMade(I)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x4

    .line 165
    sget-boolean v0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    if-le p1, v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 167
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 168
    invoke-direct {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->onMoreDetails()V

    .line 174
    :goto_0
    return-void

    .line 169
    :cond_2
    if-ne p1, v1, :cond_3

    .line 170
    invoke-direct {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->onDontShowAgain()V

    goto :goto_0

    .line 172
    :cond_3
    invoke-direct {p0, p1}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->onRating(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .line 128
    const/16 v0, 0x56

    const-string v1, "onCancel"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    .line 129
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostCallMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 131
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->finish()V

    .line 132
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 32
    iput-boolean v1, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    .line 34
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->setContentView(I)V

    .line 36
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 42
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->showDialog(I)V

    .line 43
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 49
    new-instance v0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;-><init>(Landroid/content/Context;)V

    .line 50
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900c9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 54
    new-instance v2, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity$1;-><init>(Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 62
    new-instance v0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity$2;-><init>(Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 69
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 148
    const/16 v0, 0x56

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    .line 149
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 158
    iget-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    if-nez v0, :cond_0

    .line 159
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 161
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 137
    const/16 v0, 0x56

    const-string v1, "onPause"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    .line 138
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 140
    invoke-virtual {p0}, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/callqualitysurvey/QuickSurveyActivity;->m_spannedSurveyDetailsChosen:Z

    if-nez v0, :cond_0

    .line 141
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setPostCallActivityInstance(Landroid/app/Activity;)V

    .line 143
    :cond_0
    return-void
.end method
