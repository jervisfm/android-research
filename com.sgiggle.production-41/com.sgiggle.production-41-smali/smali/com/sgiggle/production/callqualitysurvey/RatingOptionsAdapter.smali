.class public Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;
.super Landroid/widget/BaseAdapter;
.source "RatingOptionsAdapter.java"


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field static final BAD:I = 0x2

.field static final DONT_SHOW_AGAIN:I = 0x4

.field static final EXCELLENT:I = 0x0

.field static final FAIR:I = 0x1

.field private static final MENU_ITEMS:[I = null

.field static final MORE_DETAILS:I = 0x3


# instance fields
.field private m_context:Landroid/content/Context;

.field private final m_longDescription:[Ljava/lang/String;

.field private final m_shortDescription:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->$assertionsDisabled:Z

    .line 22
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->MENU_ITEMS:[I

    return-void

    .line 14
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 22
    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
        0x4t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_context:Landroid/content/Context;

    .line 30
    new-array v0, v5, [Ljava/lang/String;

    const v1, 0x7f0900cb

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f0900cc

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0900cd

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_shortDescription:[Ljava/lang/String;

    .line 35
    new-array v0, v5, [Ljava/lang/String;

    const v1, 0x7f0900ce

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const v1, 0x7f0900cf

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0900d0

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_longDescription:[Ljava/lang/String;

    .line 40
    return-void
.end method

.method private getMaxShortDescriptionWidth(Landroid/widget/TextView;)I
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 94
    move v0, v6

    move v1, v6

    .line 98
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_shortDescription:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 99
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 100
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_shortDescription:[Ljava/lang/String;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_shortDescription:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v6, v5, v2}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 102
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_0
    return v1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x5

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 115
    sget-object v0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->MENU_ITEMS:[I

    aget v0, v0, p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 120
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 51
    sget-boolean v1, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    const/4 v1, 0x5

    if-lt p1, v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 55
    :cond_0
    if-ge p1, v3, :cond_1

    .line 58
    const v1, 0x7f030010

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 61
    const v0, 0x7f0a004f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 65
    invoke-direct {p0, v0}, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->getMaxShortDescriptionWidth(Landroid/widget/TextView;)I

    move-result v2

    .line 67
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 68
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    .line 70
    add-int/2addr v2, v4

    add-int/lit8 v2, v2, 0xa

    iput v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 72
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    iget-object v2, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_shortDescription:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const v0, 0x7f0a0050

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_longDescription:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    .line 90
    :goto_0
    return-object v0

    .line 79
    :cond_1
    const v1, 0x7f03000f

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 86
    const v0, 0x7f0a004e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 87
    iget-object v2, p0, Lcom/sgiggle/production/callqualitysurvey/RatingOptionsAdapter;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-ne p1, v3, :cond_2

    const v3, 0x7f0900d1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v1

    goto :goto_0

    .line 87
    :cond_2
    const v3, 0x7f0900d2

    goto :goto_1
.end method
