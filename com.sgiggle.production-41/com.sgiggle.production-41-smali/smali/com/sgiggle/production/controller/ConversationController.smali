.class public Lcom/sgiggle/production/controller/ConversationController;
.super Ljava/lang/Object;
.source "ConversationController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationController"


# instance fields
.field private m_msgControllersByType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;",
            "Lcom/sgiggle/production/controller/ConversationMessageController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 3
    .parameter

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    new-instance v2, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;

    invoke-direct {v2, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    new-instance v2, Lcom/sgiggle/production/controller/ConversationMessageControllerText;

    invoke-direct {v2, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    new-instance v2, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;

    invoke-direct {v2, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    new-instance v2, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;

    invoke-direct {v2, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method


# virtual methods
.method public broadcastMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 2
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/controller/ConversationMessageController;

    .line 67
    invoke-virtual {v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;->handleMessage(Lcom/sgiggle/messaging/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x1

    .line 70
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;
    .locals 3
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/controller/ConversationMessageController;

    .line 52
    if-nez v0, :cond_0

    .line 53
    const-string v0, "Tango.ConversationController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMessageController: cannot get controller of type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not implemented! Cannot get controller of type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/controller/ConversationMessageController;

    .line 78
    invoke-virtual {v0, p1, p2, p3}, Lcom/sgiggle/production/controller/ConversationMessageController;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationController;->m_msgControllersByType:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/controller/ConversationMessageController;

    .line 84
    invoke-virtual {v0}, Lcom/sgiggle/production/controller/ConversationMessageController;->onResume()V

    goto :goto_0

    .line 86
    :cond_0
    return-void
.end method
