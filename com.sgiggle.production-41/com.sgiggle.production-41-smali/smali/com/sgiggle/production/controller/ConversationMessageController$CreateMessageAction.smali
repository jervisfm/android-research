.class public final enum Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;
.super Ljava/lang/Enum;
.source "ConversationMessageController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/controller/ConversationMessageController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CreateMessageAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

.field public static final enum ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

.field public static final enum ACTION_PICK:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45
    new-instance v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    const-string v1, "ACTION_NEW"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    .line 46
    new-instance v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    const-string v1, "ACTION_PICK"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_PICK:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    sget-object v1, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_PICK:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->$VALUES:[Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;
    .locals 1
    .parameter

    .prologue
    .line 44
    const-class v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->$VALUES:[Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    invoke-virtual {v0}, [Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    return-object v0
.end method
