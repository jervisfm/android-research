.class public abstract Lcom/sgiggle/production/controller/ConversationMessageController;
.super Ljava/lang/Object;
.source "ConversationMessageController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;
    }
.end annotation


# static fields
.field protected static final MENU_ID_BUY:I = 0x4

.field protected static final MENU_ID_COPY_TEXT:I = 0x2

.field protected static final MENU_ID_DELETE:I = 0x3

.field protected static final MENU_ID_FORWARD:I = 0x5

.field protected static final MENU_ID_RESEND:I = 0x0

.field protected static final MENU_ID_VIEW:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.ConversationMessageController"


# instance fields
.field protected m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    .line 62
    return-void
.end method

.method private doActionDeleteMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->newInstance(Lcom/sgiggle/production/model/ConversationMessage;Ljava/lang/String;)Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;

    move-result-object v0

    .line 372
    iget-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 373
    return-void
.end method

.method private doActionForward(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 342
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/sgiggle/production/model/ConversationMessage;->convertToSessionConversationMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 345
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 346
    return-void
.end method

.method private doActionResend(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 4
    .parameter

    .prologue
    .line 354
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v2

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)V

    .line 359
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 361
    return-void
.end method


# virtual methods
.method public varargs abstract createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
.end method

.method public doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 334
    const-string v0, "Tango.ConversationMessageController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doActionViewMessage: cannot view message of type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return-void
.end method

.method protected getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    return-object v0
.end method

.method protected getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    return-object v0
.end method

.method public getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    return-object v0
.end method

.method protected getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 1
    .parameter

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 119
    return-void
.end method

.method public final onContextItemSelected(ILcom/sgiggle/production/model/ConversationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 294
    packed-switch p1, :pswitch_data_0

    .line 308
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/controller/ConversationMessageController;->onContextItemSelectedExtra(ILcom/sgiggle/production/model/ConversationMessage;)V

    .line 311
    :goto_0
    return-void

    .line 296
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageController;->doActionResend(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 299
    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageController;->doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 302
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageController;->doActionForward(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 305
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageController;->doActionDeleteMessage(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 294
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onContextItemSelectedExtra(ILcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 320
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not implemented! menuItem not handled="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 4
    .parameter

    .prologue
    .line 180
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    invoke-static {}, Lcom/sgiggle/production/manager/MediaManager;->getInstance()Lcom/sgiggle/production/manager/MediaManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    const/high16 v2, 0x7f06

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/manager/MediaManager;->playAudioResourceAsNotification(Landroid/content/Context;IZ)V

    .line 187
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusErrorPeerNotSupported()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusErrorPeerPlatformNotSupported()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;->onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageController;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->showDeliveryError()V

    .line 197
    :cond_2
    :goto_0
    return-void

    .line 191
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    invoke-static {}, Lcom/sgiggle/production/manager/MediaManager;->getInstance()Lcom/sgiggle/production/manager/MediaManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    const v2, 0x7f060001

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/manager/MediaManager;->playAudioResourceAsNotification(Landroid/content/Context;IZ)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method protected onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 1
    .parameter

    .prologue
    .line 207
    const/4 v0, 0x0

    return v0
.end method

.method public final populateContextMenu(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 136
    .line 139
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 140
    add-int/lit8 v0, v3, 0x1

    const v1, 0x7f090145

    invoke-interface {p1, v3, v3, v0, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 144
    :goto_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/production/controller/ConversationMessageController;->populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I

    move-result v0

    .line 147
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->supportsForward()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 148
    :cond_0
    const/4 v1, 0x5

    add-int/lit8 v0, v0, 0x1

    const v2, 0x7f090144

    invoke-interface {p1, v3, v1, v0, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 152
    :cond_1
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSending()Z

    move-result v1

    if-nez v1, :cond_3

    .line 153
    :cond_2
    const/4 v1, 0x3

    add-int/lit8 v0, v0, 0x1

    const v2, 0x7f090146

    invoke-interface {p1, v3, v1, v0, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 155
    :cond_3
    return-void

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method protected populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 172
    return p3
.end method

.method protected showGenericSendSmsDialog(Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Lcom/sgiggle/production/model/ConversationContact;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 249
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 250
    invoke-virtual {p4}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    const/4 v1, 0x0

    const-string v4, ""

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/TangoApp;->showQuerySendSMSDialog(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Ljava/util/List;)V

    .line 254
    return-void
.end method

.method protected showNotSupportedByPeerGenericSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 280
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusErrorPeerNotSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09014c

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;->showGenericSendSmsDialog(Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Lcom/sgiggle/production/model/ConversationContact;)V

    .line 286
    return-void

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09014d

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected showNotSupportedByPeerSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 266
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusErrorPeerNotSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09014a

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    :goto_0
    sget-object v4, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_TEXT_MESSAGE_FOR_NON_TANGO_USER:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    move-object v0, p0

    move-object v3, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/controller/ConversationMessageController;->showSendSmsDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Lcom/sgiggle/production/model/ConversationContact;)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09014b

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0
.end method

.method protected showSendSmsDialog(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Lcom/sgiggle/production/model/ConversationContact;)Z
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 224
    invoke-virtual {p5}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    .line 225
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 226
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageController;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    :cond_0
    const/4 v0, 0x0

    .line 236
    :goto_1
    return v0

    .line 225
    :cond_1
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 230
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 231
    invoke-virtual {p5}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/TangoApp;->showQuerySendSMSDialog(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Ljava/util/List;)V

    move v0, v1

    .line 236
    goto :goto_1
.end method

.method protected abstract supportsForward()Z
.end method
