.class public Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;
.super Lcom/sgiggle/production/controller/ConversationMessageController;
.source "ConversationMessageControllerPicture.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/controller/ConversationMessageControllerPicture$1;
    }
.end annotation


# static fields
.field private static final CHOOSE_EXISTING_PHOTO_ACTIVITY_REQUEST_CODE:I = 0xc8

.field private static final PHOTO_PREVIEW_ACTIVITY_REQUEST_CODE:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "Tango.ConversationMessageControllerPicture"

.field private static final TAKE_PHOTO_ACTIVITY_REQUEST_CODE:I = 0x64


# instance fields
.field private m_isChoosePictureInProgress:Z

.field private m_isPreviewInProcess:Z

.field private m_isTakePictureInProgress:Z

.field private m_oriPictureUri:Landroid/net/Uri;

.field private m_processedPicturePath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 52
    invoke-direct {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    .line 43
    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isPreviewInProcess:Z

    .line 44
    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isTakePictureInProgress:Z

    .line 45
    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z

    .line 48
    iput-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    .line 49
    iput-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 53
    return-void
.end method

.method private cancelChoosePicture()V
    .locals 3

    .prologue
    .line 379
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelChoosePictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 381
    return-void
.end method

.method private cancelPostProcessPicture()V
    .locals 3

    .prologue
    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sgiggle/production/util/FileOperator;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 392
    :goto_0
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostProcessPictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelPostProcessPictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 394
    return-void

    .line 390
    :cond_0
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to delete image : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private cancelTakePicture()V
    .locals 3

    .prologue
    .line 360
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelTakePictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 362
    return-void
.end method

.method private handleErrorInChoosingExistingPhoto()V
    .locals 0

    .prologue
    .line 374
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelChoosePicture()V

    .line 375
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showErrorInChoosingExistingPhoto()V

    .line 376
    return-void
.end method

.method private handleErrorInTakingPhoto()V
    .locals 0

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelTakePicture()V

    .line 356
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showErrorInTakingPhoto()V

    .line 357
    return-void
.end method

.method private handleResultFromCaptureImageActivity(Ljava/lang/String;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 232
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 235
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->shrinkAndRotateBitmap(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 236
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    .line 237
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->sendPhoto(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    :goto_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    .line 239
    const-string v1, "Tango.ConversationMessageControllerPicture"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleResultFromCaptureImageActivity got:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 241
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInTakingPhoto()V

    goto :goto_0

    .line 243
    :cond_0
    if-nez p2, :cond_1

    .line 244
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelTakePicture()V

    .line 245
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "The camera activity is canceled by user or crashed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 247
    :cond_1
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "in handleResultFromCaptureImageActivity() gets unexpected resultCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInTakingPhoto()V

    goto :goto_0
.end method

.method private handleResultFromChooseExistingPhoto(ILandroid/content/Intent;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 263
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 266
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->shrinkAndRotateBitmap(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    .line 267
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->processPickupPicture()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v0

    .line 269
    const-string v1, "Tango.ConversationMessageControllerPicture"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleResultFromChooseExistingPhoto() got exception:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInChoosingExistingPhoto()V

    goto :goto_0

    .line 272
    :cond_0
    if-nez p1, :cond_1

    .line 273
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelChoosePicture()V

    .line 274
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "The gallery activity is canceled by user or crashed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 276
    :cond_1
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleResultFromChooseExistingPhoto() gets unexpected code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInChoosingExistingPhoto()V

    goto :goto_0
.end method

.method private handleResultFromPhotoPreviewActivity(Ljava/lang/String;ILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 322
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isPreviewInProcess:Z

    .line 324
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 325
    const-string v0, "result_uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 326
    if-nez v0, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showErrorInChoosingExistingPhoto()V

    .line 328
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelPostProcessPicture()V

    .line 344
    :goto_0
    return-void

    .line 332
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->sendPhoto(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    .line 334
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showErrorInChoosingExistingPhoto()V

    .line 335
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelPostProcessPicture()V

    goto :goto_0

    .line 338
    :cond_1
    if-nez p2, :cond_2

    .line 339
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelPostProcessPicture()V

    goto :goto_0

    .line 341
    :cond_2
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleResultFromPhotoPreviewActivity unknown resultCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelPostProcessPicture()V

    goto :goto_0
.end method

.method private hasSDMounted()Z
    .locals 2

    .prologue
    .line 211
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    .line 212
    const-string v1, "mounted"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "hasSDMounted() return true"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x1

    .line 217
    :goto_0
    return v0

    .line 216
    :cond_0
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "hasSDMounted() return false"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onChooseExistingPhotoClicked()V
    .locals 3

    .prologue
    .line 227
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ChoosePictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 229
    return-void
.end method

.method private onTakePhotoClicked()V
    .locals 3

    .prologue
    .line 221
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "onTakePhotoClicked()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$TakePictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 224
    return-void
.end method

.method private onViewPicture(Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 448
    check-cast p2, Lcom/sgiggle/production/model/ConversationMessagePicture;

    .line 449
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->startPictureViewer(Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessagePicture;)V

    .line 450
    return-void
.end method

.method private processPickupPicture()V
    .locals 3

    .prologue
    .line 365
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$PostProcessPictureMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 367
    return-void
.end method

.method private static renameFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 397
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 398
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 399
    invoke-virtual {v0, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 400
    return-void
.end method

.method private sendPhoto(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 403
    if-nez p1, :cond_0

    .line 404
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showErrorInTakingPhoto()V

    .line 405
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "conversation id should be valid"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    const-string v1, "_sent"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sgiggle/production/screens/picture/PictureStorage;->getTmpPicPath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v3

    .line 410
    invoke-static {p2, v3}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->renameFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    const-string v1, "_thumb"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sgiggle/production/screens/picture/PictureStorage;->getTmpPicPath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v4

    .line 414
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 415
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 416
    invoke-static {v3}, Lcom/sgiggle/production/util/BitmapTransformer;->loadFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 417
    const/4 v5, 0x1

    invoke-static {v2, v0, v1, v5}, Lcom/sgiggle/production/util/BitmapTransformer;->downscale(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 419
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    .line 421
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 422
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 425
    :cond_1
    invoke-static {v0, v4}, Lcom/sgiggle/production/util/BitmapTransformer;->saveFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 426
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_2

    .line 427
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 431
    :cond_2
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendPhoto, path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Tpath:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v7, 0x7f090163

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 443
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 445
    return-void
.end method

.method private showError(I)V
    .locals 3
    .parameter

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 352
    return-void
.end method

.method private showErrorInChoosingExistingPhoto()V
    .locals 1

    .prologue
    .line 347
    const v0, 0x7f090194

    invoke-direct {p0, v0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showError(I)V

    .line 348
    return-void
.end method

.method private showErrorInTakingPhoto()V
    .locals 1

    .prologue
    .line 370
    const v0, 0x7f090193

    invoke-direct {p0, v0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showError(I)V

    .line 371
    return-void
.end method

.method private shrinkAndRotateBitmap(Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v3, 0x500

    const/4 v5, 0x0

    .line 282
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/sgiggle/production/util/BitmapOrientationDetector;->getOrientationOfImage(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    .line 283
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/sgiggle/production/util/FilePathResolver;->translateUriToFilePath(Landroid/app/Activity;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 285
    invoke-static {v1}, Lcom/sgiggle/production/util/BitmapTransformer;->loadFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 286
    if-nez v2, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/sgiggle/production/util/BitmapTransformer;->loadFile(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 290
    :cond_0
    if-nez v2, :cond_1

    .line 291
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0

    .line 294
    :cond_1
    invoke-static {v2, v3, v3, v5}, Lcom/sgiggle/production/util/BitmapTransformer;->downscale(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 295
    if-eq v2, v3, :cond_2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_2

    .line 296
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 300
    :cond_2
    invoke-static {v3, v0}, Lcom/sgiggle/production/util/BitmapTransformer;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 301
    if-eq v3, v0, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_3

    .line 302
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 306
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v2

    const-string v3, "_ro"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/sgiggle/production/screens/picture/PictureStorage;->getTmpPicPath(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v2

    .line 307
    invoke-static {v0, v2}, Lcom/sgiggle/production/util/BitmapTransformer;->saveFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 308
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_4

    .line 309
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 312
    :cond_4
    if-eqz p2, :cond_5

    .line 313
    invoke-static {v1}, Lcom/sgiggle/production/util/FileOperator;->deleteFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 314
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unable to remove file : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    :cond_5
    return-object v2
.end method

.method private startChooseExistingPhoto()V
    .locals 4

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z

    if-nez v0, :cond_0

    .line 196
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 197
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 198
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    iget-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    const/16 v2, 0xc8

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    const-string v1, "Tango.ConversationMessageControllerPicture"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in startChooseExistingPhoto() exception is caught:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInChoosingExistingPhoto()V

    goto :goto_0
.end method

.method private startPhotoPreview(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 253
    if-nez p1, :cond_0

    .line 254
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "path is null, unable to start photo preview"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :goto_0
    return-void

    .line 257
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    const-class v2, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 258
    const-string v1, "path"

    invoke-static {p1}, Lcom/sgiggle/production/util/FileOperator;->getUriFromPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    iget-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    const/16 v2, 0x12c

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private startPictureViewer(Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessagePicture;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 453
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getMessageId()I

    move-result v1

    invoke-direct {v0, p1, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureMessage;-><init>(Ljava/lang/String;I)V

    .line 454
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 455
    return-void
.end method

.method private startTakePhoto()V
    .locals 4

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isTakePictureInProgress:Z

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 180
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 182
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/production/screens/picture/PictureStorage;->getTmpPicUri(Landroid/content/Context;Ljava/lang/Boolean;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    .line 183
    const-string v1, "output"

    iget-object v2, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_oriPictureUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 184
    iget-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    const/16 v2, 0x64

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 185
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isTakePictureInProgress:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 186
    :catch_0
    move-exception v0

    .line 187
    const-string v1, "Tango.ConversationMessageControllerPicture"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in startTakePhoto() exception is caught:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleErrorInTakingPhoto()V

    goto :goto_0
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 92
    sget-object v0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture$1;->$SwitchMap$com$sgiggle$production$controller$ConversationMessageController$CreateMessageAction:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "createNewMessage: action not handled."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :goto_0
    return-void

    .line 94
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->hasSDMounted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->onTakePhotoClicked()V

    goto :goto_0

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    const v1, 0x7f090166

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 102
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->hasSDMounted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->onChooseExistingPhotoClicked()V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    const v1, 0x7f090167

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 92
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 160
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->onViewPicture(Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    const v1, 0x7f090168

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 83
    const/4 v0, 0x0

    .line 87
    :goto_0
    return v0

    .line 60
    :sswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->startTakePhoto()V

    move v0, v1

    .line 61
    goto :goto_0

    :sswitch_1
    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :sswitch_2
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->startChooseExistingPhoto()V

    move v0, v1

    .line 69
    goto :goto_0

    :sswitch_3
    move v0, v1

    .line 73
    goto :goto_0

    .line 76
    :sswitch_4
    iget-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isPreviewInProcess:Z

    if-nez v0, :cond_0

    .line 77
    iput-boolean v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isPreviewInProcess:Z

    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_processedPicturePath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->startPhotoPreview(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x125 -> :sswitch_3
        0x89da -> :sswitch_0
        0x89db -> :sswitch_1
        0x89dc -> :sswitch_2
        0x89de -> :sswitch_4
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 117
    sparse-switch p1, :sswitch_data_0

    .line 133
    :goto_0
    return-void

    .line 119
    :sswitch_0
    iput-boolean v3, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isTakePictureInProgress:Z

    .line 120
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleResultFromCaptureImageActivity(Ljava/lang/String;I)V

    goto :goto_0

    .line 124
    :sswitch_1
    const-string v0, "Tango.ConversationMessageControllerPicture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult case CHOOSE_EXISTING_PHOTO_ACTIVITY_REQUEST_CODE :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    iput-boolean v3, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z

    .line 126
    invoke-direct {p0, p2, p3}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleResultFromChooseExistingPhoto(ILandroid/content/Intent;)V

    goto :goto_0

    .line 130
    :sswitch_2
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->handleResultFromPhotoPreviewActivity(Ljava/lang/String;ILandroid/content/Intent;)V

    goto :goto_0

    .line 117
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_2
    .end sparse-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "Tango.ConversationMessageControllerPicture"

    const-string v1, "onResume() should be called after m_isChoosePictureInProgress is cleared, the choosing picture app crashed possibly"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    invoke-direct {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->cancelChoosePicture()V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->m_isChoosePictureInProgress:Z

    .line 144
    :cond_0
    return-void
.end method

.method protected onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 2
    .parameter

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v0

    .line 171
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getTextInSms()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showNotSupportedByPeerSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    invoke-virtual {p0, v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerPicture;->showNotSupportedByPeerGenericSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 174
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    const/4 v0, 0x0

    const/4 v1, 0x1

    add-int/lit8 v2, p3, 0x1

    const v3, 0x7f090143

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 150
    return v2
.end method

.method protected supportsForward()Z
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x1

    return v0
.end method
