.class public Lcom/sgiggle/production/controller/ConversationMessageControllerText;
.super Lcom/sgiggle/production/controller/ConversationMessageController;
.source "ConversationMessageControllerText.java"


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    .line 20
    return-void
.end method

.method private doActionCopyText(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/production/Utils;->copyTextToClipboard(Ljava/lang/String;Landroid/content/Context;)V

    .line 48
    return-void
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 64
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->getConversationId()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const/4 v0, 0x0

    aget-object v0, p2, v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;)V

    .line 70
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 72
    return-void
.end method

.method public onContextItemSelectedExtra(ILcom/sgiggle/production/model/ConversationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    packed-switch p1, :pswitch_data_0

    .line 35
    :goto_0
    return-void

    .line 33
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->doActionCopyText(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 31
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 52
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v0

    .line 53
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09014e

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->showNotSupportedByPeerSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 56
    invoke-virtual {p0, v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerText;->showNotSupportedByPeerGenericSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 58
    :cond_0
    return v6
.end method

.method protected populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    const/4 v0, 0x0

    const/4 v1, 0x2

    add-int/lit8 v2, p3, 0x1

    const v3, 0x7f090147

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 26
    return v2
.end method

.method protected supportsForward()Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method
