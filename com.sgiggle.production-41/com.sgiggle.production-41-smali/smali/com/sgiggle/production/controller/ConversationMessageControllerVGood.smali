.class public Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;
.super Lcom/sgiggle/production/controller/ConversationMessageController;
.source "ConversationMessageControllerVGood.java"


# instance fields
.field private m_random:Ljava/util/Random;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 1
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    .line 24
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->m_random:Ljava/util/Random;

    .line 28
    return-void
.end method

.method private doActionBuy(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 57
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessageVGood;

    .line 58
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sgiggle/production/PurchaseProxyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    const-string v1, "PurchaseProxyActivity.EXTRA_PRODUCT_ID"

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getProductId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 60
    const-string v1, "PurchaseProxyActivity.EXTRA_EXTERNAL_MARKET_ID"

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getExternalMarketId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 95
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->m_random:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v0

    const-wide v2, 0xffffffffL

    and-long/2addr v0, v2

    .line 96
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "%d:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aget-object v5, p2, v6

    aput-object v5, v4, v6

    const/4 v5, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 97
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getConversationId()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v4, ""

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 105
    return-void
.end method

.method public doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 6
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideIme()V

    .line 67
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessageVGood;

    .line 69
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getVGoodPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getVGoodId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getSeed()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->startVGoodAnimation(Ljava/lang/String;JJ)Z

    .line 70
    return-void
.end method

.method public onContextItemSelectedExtra(ILcom/sgiggle/production/model/ConversationMessage;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    packed-switch p1, :pswitch_data_0

    .line 54
    :goto_0
    return-void

    .line 51
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->doActionBuy(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 6
    .parameter

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;->onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 77
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessageVGood;

    .line 79
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getActivity()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getVGoodPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getVGoodId()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getSeed()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->startVGoodAnimation(Ljava/lang/String;JJ)Z

    .line 81
    :cond_1
    return-void
.end method

.method protected onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 3
    .parameter

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v0

    .line 86
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09014f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->showNotSupportedByPeerSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Ljava/lang/String;Lcom/sgiggle/production/model/ConversationMessage;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 88
    invoke-virtual {p0, v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerVGood;->showNotSupportedByPeerGenericSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 90
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 33
    const/4 v0, 0x1

    add-int/lit8 v1, p3, 0x1

    const v2, 0x7f090143

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 34
    check-cast p2, Lcom/sgiggle/production/model/ConversationMessageVGood;

    .line 35
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationMessageVGood;->canBePurchased()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x4

    add-int/lit8 v1, v1, 0x1

    const v2, 0x7f090148

    invoke-interface {p1, v3, v0, v1, v2}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    :cond_0
    move v0, v1

    .line 38
    return v0
.end method

.method protected supportsForward()Z
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method
