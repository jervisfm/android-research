.class public Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;
.super Lcom/sgiggle/production/controller/ConversationMessageController;
.source "ConversationMessageControllerVideo.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationMessageControllerVideo"


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    .line 23
    return-void
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 63
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;->m_conversationDetailFragment:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationDetail;->convertPeersToContactList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$LeaveVideoMailMessage;-><init>(Ljava/util/List;Z)V

    .line 67
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 68
    return-void
.end method

.method public doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 6
    .parameter

    .prologue
    .line 39
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessageVideo;

    move-object v1, v0

    .line 40
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isLocalPlaybackAvailable()Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    const-string v1, "Tango.ConversationMessageControllerVideo"

    const-string v2, "Video play back not available"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    const v2, 0x7f0900bd

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 51
    :goto_0
    return-void

    .line 46
    :cond_0
    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageMessage;

    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;->getConversationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getMediaId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v4

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v5

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageMessage;-><init>(Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 50
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v3, "jingle"

    invoke-virtual {v1, v3, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method protected onStatusErrorPeerNotSupported(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 1
    .parameter

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;->getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageControllerVideo;->showNotSupportedByPeerGenericSendSmsDialog(Lcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 57
    const/4 v0, 0x1

    return v0
.end method

.method protected populateContextMenuExtra(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;I)I
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x1

    add-int/lit8 v2, p3, 0x1

    const v3, 0x7f090143

    invoke-interface {p1, v0, v1, v2, v3}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 29
    return v2
.end method

.method protected supportsForward()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method
