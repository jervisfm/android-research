.class public Lcom/sgiggle/production/database/PurchaseDatabase;
.super Ljava/lang/Object;
.source "PurchaseDatabase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "purchase.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final HISTORY_COLUMNS:[Ljava/lang/String; = null

.field public static final HISTORY_DEVELOPER_PAYLOAD_COL:Ljava/lang/String; = "developerPayload"

.field public static final HISTORY_ORDER_ID_COL:Ljava/lang/String; = "_id"

.field public static final HISTORY_PRODUCT_ID_COL:Ljava/lang/String; = "productId"

.field public static final HISTORY_PURCHASE_TIME_COL:Ljava/lang/String; = "purchaseTime"

.field public static final HISTORY_STATE_COL:Ljava/lang/String; = "state"

.field private static final PURCHASED_COLUMNS:[Ljava/lang/String; = null

.field private static final PURCHASED_ITEMS_TABLE_NAME:Ljava/lang/String; = "purchased"

.field public static final PURCHASED_PRODUCT_ID_COL:Ljava/lang/String; = "_id"

.field public static final PURCHASED_QUANTITY_COL:Ljava/lang/String; = "quantity"

.field private static final PURCHASE_HISTORY_TABLE_NAME:Ljava/lang/String; = "history"

.field private static final TAG:Ljava/lang/String; = "PurchaseDatabase"


# instance fields
.field private mDatabaseHelper:Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "productId"

    aput-object v1, v0, v4

    const-string v1, "state"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "purchaseTime"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "developerPayload"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/production/database/PurchaseDatabase;->HISTORY_COLUMNS:[Ljava/lang/String;

    .line 59
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "quantity"

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/production/database/PurchaseDatabase;->PURCHASED_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;-><init>(Lcom/sgiggle/production/database/PurchaseDatabase;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDatabaseHelper:Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;

    .line 67
    iget-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDatabaseHelper:Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    .line 68
    return-void
.end method

.method private insertOrder(Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;JLjava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 93
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v1, "productId"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    const-string v1, "state"

    invoke-virtual {p3}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->ordinal()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 96
    const-string v1, "purchaseTime"

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 97
    const-string v1, "developerPayload"

    invoke-virtual {v0, v1, p6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "history"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 99
    return-void
.end method

.method private updatePurchasedItem(Ljava/lang/String;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 111
    if-nez p2, :cond_0

    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "purchased"

    const-string v2, "_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 120
    :goto_0
    return-void

    .line 116
    :cond_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 117
    const-string v1, "_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v1, "quantity"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 119
    iget-object v1, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "purchased"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDatabaseHelper:Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;

    invoke-virtual {v0}, Lcom/sgiggle/production/database/PurchaseDatabase$DatabaseHelper;->close()V

    .line 72
    return-void
.end method

.method public queryAllPurchasedItems()Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "purchased"

    sget-object v2, Lcom/sgiggle/production/database/PurchaseDatabase;->PURCHASED_COLUMNS:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized updatePurchase(Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;JLjava/lang/String;)I
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/sgiggle/production/database/PurchaseDatabase;->insertOrder(Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;JLjava/lang/String;)V

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/database/PurchaseDatabase;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "history"

    sget-object v2, Lcom/sgiggle/production/database/PurchaseDatabase;->HISTORY_COLUMNS:[Ljava/lang/String;

    const-string v3, "productId=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 144
    if-nez v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 167
    :goto_0
    monitor-exit p0

    return v0

    .line 147
    :cond_0
    const/4 v1, 0x0

    .line 150
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 151
    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 152
    invoke-static {v2}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->valueOf(I)Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v2

    .line 155
    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v2, v3, :cond_1

    .line 156
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 161
    :cond_3
    invoke-direct {p0, p2, v1}, Lcom/sgiggle/production/database/PurchaseDatabase;->updatePurchasedItem(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    if-eqz v0, :cond_4

    .line 164
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_4
    move v0, v1

    .line 167
    goto :goto_0

    .line 163
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_5

    .line 164
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 141
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
