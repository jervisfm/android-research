.class Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;
.super Ljava/lang/Object;
.source "TangoAlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->create()Lcom/sgiggle/production/dialog/TangoAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

.field final synthetic val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;Lcom/sgiggle/production/dialog/TangoAlertDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 132
    const-string v0, "Tango.TangoAlertDialog"

    const-string v1, "execute button click listener"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setButtonClicked(Z)V

    .line 134
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$000(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    if-ne p2, v3, :cond_1

    .line 135
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$000(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-interface {v0, v1, v3}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 139
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-virtual {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->dismiss()V

    .line 140
    return-void

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$100(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    if-ne p2, v2, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$100(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_0
.end method
