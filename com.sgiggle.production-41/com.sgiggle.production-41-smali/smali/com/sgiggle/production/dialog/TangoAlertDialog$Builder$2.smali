.class Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;
.super Ljava/lang/Object;
.source "TangoAlertDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->create()Lcom/sgiggle/production/dialog/TangoAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

.field final synthetic val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;Lcom/sgiggle/production/dialog/TangoAlertDialog;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .parameter

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-virtual {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->isButtonClicked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    const-string v0, "Tango.TangoAlertDialog"

    const-string v1, "call extra dismiss listener since dialog is not dismissed by clicking button"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$100(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$100(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    const/4 v2, -0x2

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 167
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setButtonClicked(Z)V

    .line 171
    :cond_1
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->removeBeforeBackgroundObserver(Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;)V

    .line 174
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog;->m_dismissListener:Landroid/content/DialogInterface$OnDismissListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->access$200(Lcom/sgiggle/production/dialog/TangoAlertDialog;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog;->m_dismissListener:Landroid/content/DialogInterface$OnDismissListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->access$200(Lcom/sgiggle/production/dialog/TangoAlertDialog;)Landroid/content/DialogInterface$OnDismissListener;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 177
    :cond_2
    return-void

    .line 164
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$000(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->this$0:Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;

    #getter for: Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->access$000(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;->val$tdialog:Lcom/sgiggle/production/dialog/TangoAlertDialog;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_0
.end method
