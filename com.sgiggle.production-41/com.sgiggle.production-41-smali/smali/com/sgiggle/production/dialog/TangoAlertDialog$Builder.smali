.class public Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
.super Ljava/lang/Object;
.source "TangoAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/dialog/TangoAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private m_cancelable:Z

.field private m_context:Landroid/content/Context;

.field private m_dismissBeforeGoingToBackground:Z

.field private m_message:Ljava/lang/CharSequence;

.field private m_negativeButton:Ljava/lang/String;

.field private m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private m_positiveButton:Ljava/lang/String;

.field private m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-boolean v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_cancelable:Z

    .line 67
    iput-boolean v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_dismissBeforeGoingToBackground:Z

    .line 72
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_context:Landroid/content/Context;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public create()Lcom/sgiggle/production/dialog/TangoAlertDialog;
    .locals 4

    .prologue
    .line 125
    new-instance v0, Lcom/sgiggle/production/dialog/TangoAlertDialog;

    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;-><init>(Landroid/content/Context;)V

    .line 126
    iget-object v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_message:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 127
    iget-boolean v1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_cancelable:Z

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setCancelable(Z)V

    .line 129
    new-instance v1, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;

    invoke-direct {v1, p0, v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$1;-><init>(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;Lcom/sgiggle/production/dialog/TangoAlertDialog;)V

    .line 144
    iget-object v2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButton:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 145
    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButton:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 148
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButton:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 149
    const/4 v2, -0x2

    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButton:Ljava/lang/String;

    invoke-virtual {v0, v2, v3, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 152
    :cond_1
    new-instance v1, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;

    invoke-direct {v1, p0, v0}, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder$2;-><init>(Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;Lcom/sgiggle/production/dialog/TangoAlertDialog;)V

    .line 181
    iget-boolean v2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_dismissBeforeGoingToBackground:Z

    if-eqz v2, :cond_2

    .line 182
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setBackgroundDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 183
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sgiggle/production/TangoApp;->addBeforeBackgroundObserver(Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;)V

    .line 186
    :cond_2
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 187
    return-object v0
.end method

.method public setCancelable(Z)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-boolean p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_cancelable:Z

    .line 116
    return-object p0
.end method

.method public setDismissBeforeGoingToBackground(Z)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_dismissBeforeGoingToBackground:Z

    .line 121
    return-object p0
.end method

.method public setMessage(I)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 87
    return-object p0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 82
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 77
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButton:Ljava/lang/String;

    .line 104
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 105
    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButton:Ljava/lang/String;

    .line 110
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 111
    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButton:Ljava/lang/String;

    .line 92
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 93
    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButton:Ljava/lang/String;

    .line 98
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 99
    return-object p0
.end method
