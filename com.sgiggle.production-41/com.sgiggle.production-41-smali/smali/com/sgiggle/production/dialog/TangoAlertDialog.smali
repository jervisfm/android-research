.class public Lcom/sgiggle/production/dialog/TangoAlertDialog;
.super Landroid/app/AlertDialog;
.source "TangoAlertDialog.java"

# interfaces
.implements Lcom/sgiggle/production/TangoApp$BeforeGoingToBackgroundCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/dialog/TangoAlertDialog$Builder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.TangoAlertDialog"


# instance fields
.field private backgroundDismissListener:Landroid/content/DialogInterface$OnDismissListener;

.field private buttonClicked:Z

.field private m_dismissListener:Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->buttonClicked:Z

    .line 25
    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/dialog/TangoAlertDialog;)Landroid/content/DialogInterface$OnDismissListener;
    .locals 1
    .parameter

    .prologue
    .line 10
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->m_dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    return-object v0
.end method


# virtual methods
.method public beforeGoingToBackground()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->backgroundDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->backgroundDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p0}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 56
    invoke-virtual {p0}, Lcom/sgiggle/production/dialog/TangoAlertDialog;->dismiss()V

    .line 58
    :cond_0
    return-void
.end method

.method public isButtonClicked()Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->buttonClicked:Z

    return v0
.end method

.method public setBackgroundDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->backgroundDismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 38
    return-void
.end method

.method public setButtonClicked(Z)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->buttonClicked:Z

    .line 33
    return-void
.end method

.method public setExtraOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V
    .locals 0
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoAlertDialog;->m_dismissListener:Landroid/content/DialogInterface$OnDismissListener;

    .line 198
    return-void
.end method
