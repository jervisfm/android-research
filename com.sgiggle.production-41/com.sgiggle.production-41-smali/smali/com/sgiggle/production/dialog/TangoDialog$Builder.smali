.class public Lcom/sgiggle/production/dialog/TangoDialog$Builder;
.super Ljava/lang/Object;
.source "TangoDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/dialog/TangoDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private m_cancelable:Z

.field private m_contentView:Landroid/view/View;

.field private m_context:Landroid/content/Context;

.field private m_inflater:Landroid/view/LayoutInflater;

.field private m_message:Ljava/lang/CharSequence;

.field private m_negativeButton:Ljava/lang/String;

.field private m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private m_positiveButton:Ljava/lang/String;

.field private m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

.field private m_title:Ljava/lang/String;

.field private m_titleView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/dialog/TangoDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/dialog/TangoDialog$Builder;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    return-object v0
.end method


# virtual methods
.method public create()Lcom/sgiggle/production/dialog/TangoDialog;
    .locals 10

    .prologue
    const v9, 0x7f0a0058

    const v4, 0x7f0a0031

    const/16 v8, 0x8

    const/4 v7, -0x1

    const/4 v6, -0x2

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_inflater:Landroid/view/LayoutInflater;

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_inflater:Landroid/view/LayoutInflater;

    .line 117
    :cond_0
    new-instance v1, Lcom/sgiggle/production/dialog/TangoDialog;

    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    const v2, 0x7f0d005a

    invoke-direct {v1, v0, v2}, Lcom/sgiggle/production/dialog/TangoDialog;-><init>(Landroid/content/Context;I)V

    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f030057

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 119
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/production/dialog/TangoDialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_title:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 122
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 123
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_title:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_message:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 131
    const v0, 0x7f0a014f

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_message:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButton:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 139
    const v0, 0x7f0a0150

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 140
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButton:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v3, :cond_3

    .line 143
    new-instance v3, Lcom/sgiggle/production/dialog/TangoDialog$Builder$1;

    invoke-direct {v3, p0, v1}, Lcom/sgiggle/production/dialog/TangoDialog$Builder$1;-><init>(Lcom/sgiggle/production/dialog/TangoDialog$Builder;Lcom/sgiggle/production/dialog/TangoDialog;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 156
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButton:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 157
    const v0, 0x7f0a0151

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 158
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButton:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v3, :cond_4

    .line 161
    new-instance v3, Lcom/sgiggle/production/dialog/TangoDialog$Builder$2;

    invoke-direct {v3, p0, v1}, Lcom/sgiggle/production/dialog/TangoDialog$Builder$2;-><init>(Lcom/sgiggle/production/dialog/TangoDialog$Builder;Lcom/sgiggle/production/dialog/TangoDialog;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    :cond_4
    :goto_3
    iget-boolean v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_cancelable:Z

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/dialog/TangoDialog;->setCancelable(Z)V

    .line 175
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/production/dialog/TangoDialog;->setContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    invoke-virtual {v1}, Lcom/sgiggle/production/dialog/TangoDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/view/Window;->setLayout(II)V

    .line 178
    return-object v1

    .line 124
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_titleView:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 125
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 126
    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 127
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_titleView:Landroid/view/View;

    const/4 v4, 0x0

    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v7, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3, v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 132
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_contentView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 133
    invoke-virtual {v2, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 134
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 135
    iget-object v3, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_contentView:Landroid/view/View;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v7, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    .line 153
    :cond_7
    const v0, 0x7f0a0150

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 171
    :cond_8
    const v0, 0x7f0a0151

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3
.end method

.method public setCancelable(Z)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 108
    iput-boolean p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_cancelable:Z

    .line 109
    return-object p0
.end method

.method public setContentView(Landroid/view/View;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_contentView:Landroid/view/View;

    .line 75
    return-object p0
.end method

.method public setCustomeTitleView(Landroid/view/View;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_titleView:Landroid/view/View;

    .line 104
    return-object p0
.end method

.method public setMessage(I)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 60
    return-object p0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 55
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_message:Ljava/lang/CharSequence;

    .line 50
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButton:Ljava/lang/String;

    .line 92
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 93
    return-object p0
.end method

.method public setNegativeButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButton:Ljava/lang/String;

    .line 98
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_negativeButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 99
    return-object p0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButton:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 81
    return-object p0
.end method

.method public setPositiveButton(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButton:Ljava/lang/String;

    .line 86
    iput-object p2, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_positiveButtonClickListener:Landroid/content/DialogInterface$OnClickListener;

    .line 87
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_title:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public setTtile(I)Lcom/sgiggle/production/dialog/TangoDialog$Builder;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/dialog/TangoDialog$Builder;->m_title:Ljava/lang/String;

    .line 65
    return-object p0
.end method
