.class public abstract Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
.super Ljava/lang/Object;
.source "AbstractProximityFactory.java"


# static fields
.field private static s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    if-nez v0, :cond_1

    .line 30
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 32
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 34
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    .line 46
    :cond_1
    :goto_0
    sget-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    return-object v0

    .line 35
    :cond_2
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_3

    .line 37
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    goto :goto_0

    .line 38
    :cond_3
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->getManufacturer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_4

    .line 40
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    goto :goto_0

    .line 42
    :cond_4
    invoke-static {}, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;->s_factory:Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;

    goto :goto_0
.end method

.method public static getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method public abstract getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
.end method
