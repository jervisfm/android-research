.class public Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;
.super Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
.source "GenericProximityFactory.java"


# static fields
.field private static m_proximityFactory:Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;


# instance fields
.field private m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;-><init>()V

    .line 13
    new-instance v0, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    .line 14
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;

    if-nez v0, :cond_0

    .line 18
    new-instance v0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;-><init>()V

    sput-object v0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;

    .line 21
    :cond_0
    sget-object v0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;

    return-object v0
.end method

.method public static getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string v0, ""

    return-object v0
.end method


# virtual methods
.method public getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/generic/GenericProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    return-object v0
.end method
