.class public abstract Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
.super Ljava/lang/Object;
.source "AbstractProximityHandler.java"


# static fields
.field private static final THRESHOLD:J = 0x1f4L


# instance fields
.field private m_timestampCurrent:J

.field private m_timestampOld:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampOld:J

    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampCurrent:J

    .line 14
    return-void
.end method


# virtual methods
.method public abstract handleProximityFar(Landroid/app/Activity;)V
.end method

.method public abstract handleProximityNear(Landroid/app/Activity;)V
.end method

.method public throttle()Z
    .locals 4

    .prologue
    .line 34
    iget-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampCurrent:J

    iget-wide v2, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampOld:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1f4

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateTimestamp()V
    .locals 2

    .prologue
    .line 21
    iget-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampCurrent:J

    iput-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampOld:J

    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;->m_timestampCurrent:J

    .line 23
    return-void
.end method
