.class public Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;
.super Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
.source "HTCProximityFactory.java"


# static fields
.field private static final HTC_BLISS:Ljava/lang/String; = "HTC Bliss"

.field private static final HTC_NEXUS_ONE:Ljava/lang/String; = "Nexus One"

.field private static final HTC_RHYME:Ljava/lang/String; = "HTC Rhyme"

.field private static final HTC_RUBY:Ljava/lang/String; = "HTC Ruby"

.field private static final HTC_RUNNYMEDE:Ljava/lang/String; = "HTC Runnymede"

.field private static final HTC_SENSATION:Ljava/lang/String; = "HTC Sensation"

.field private static final HTC_SENSATION_XL:Ljava/lang/String; = "HTC Sensation XL"

.field private static m_proximityFactory:Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;


# instance fields
.field private m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;-><init>()V

    .line 30
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;-><init>()V

    sput-object v0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;

    .line 37
    :cond_0
    sget-object v0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;

    return-object v0
.end method

.method public static getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "HTC"

    return-object v0
.end method


# virtual methods
.method public getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
    .locals 2

    .prologue
    .line 42
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 44
    iget-object v1, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    .line 66
    :goto_0
    return-object v0

    .line 46
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    .line 47
    :cond_1
    new-instance v0, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    .line 66
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_0

    .line 48
    :cond_2
    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 49
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 50
    :cond_3
    const-string v1, "HTC Ruby"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 51
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 52
    :cond_4
    const-string v1, "Nexus One"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 53
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 54
    :cond_5
    const-string v1, "HTC Bliss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 55
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 56
    :cond_6
    const-string v1, "HTC Rhyme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 57
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 58
    :cond_7
    const-string v1, "HTC Sensation XL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 59
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 60
    :cond_8
    const-string v1, "HTC Runnymede"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 61
    new-instance v0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 63
    :cond_9
    new-instance v0, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/HTCProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1
.end method
