.class public Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;
.super Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
.source "HTCProximityHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.HTCProximityHandler"


# instance fields
.field private m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;-><init>()V

    .line 16
    return-void
.end method

.method private createNewWakeLock(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 49
    const-string v0, "Tango.HTCProximityHandler"

    const-string v1, "Creating new wake lock"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 51
    const/16 v1, 0x20

    const-string v2, "Tango.HTCProximityHandler"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 53
    :cond_0
    return-void
.end method


# virtual methods
.method public handleProximityFar(Landroid/app/Activity;)V
    .locals 2
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->createNewWakeLock(Landroid/app/Activity;)V

    .line 33
    invoke-virtual {p0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->updateTimestamp()V

    .line 36
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->throttle()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 38
    const-string v0, "Tango.HTCProximityHandler"

    const-string v1, "Released proximity wake lock"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    :cond_0
    return-void
.end method

.method public handleProximityNear(Landroid/app/Activity;)V
    .locals 2
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->createNewWakeLock(Landroid/app/Activity;)V

    .line 21
    invoke-virtual {p0}, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->updateTimestamp()V

    .line 24
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/htc/handler/HTCProximityHandler;->m_proximityWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 26
    const-string v0, "Tango.HTCProximityHandler"

    const-string v1, "Acquired proximity wake lock"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :cond_0
    return-void
.end method
