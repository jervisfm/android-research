.class public Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;
.super Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
.source "SamsungProximityFactory.java"


# static fields
.field private static final SAMSUNG_GT_I9103:Ljava/lang/String; = "GT-I9103"

.field private static final SAMSUNG_NEXUS_S:Ljava/lang/String; = "Nexus S"

.field private static final TAG:Ljava/lang/String; = "Tango.SamsungProximityFactory"

.field private static m_proximityFactory:Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;


# instance fields
.field private m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;-><init>()V

    .line 31
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/factory/proximity/AbstractProximityFactory;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;-><init>()V

    sput-object v0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;

    .line 38
    :cond_0
    sget-object v0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityFactory:Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;

    return-object v0
.end method

.method public static getManufacturer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    const-string v0, "Samsung"

    return-object v0
.end method


# virtual methods
.method public getProximityHandler()Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;
    .locals 4

    .prologue
    .line 43
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 44
    const-string v1, "Tango.SamsungProximityFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Model: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v1, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    .line 56
    :goto_0
    return-object v0

    .line 48
    :cond_0
    const-string v1, "Nexus S"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    .line 49
    new-instance v0, Lcom/sgiggle/production/factory/proximity/samsung/handler/SamsungProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/samsung/handler/SamsungProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    .line 56
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_0

    .line 50
    :cond_1
    const-string v1, "GT-I9103"

    invoke-virtual {v1, v0}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    .line 51
    new-instance v0, Lcom/sgiggle/production/factory/proximity/samsung/handler/SamsungProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/samsung/handler/SamsungProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1

    .line 53
    :cond_2
    new-instance v0, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;

    invoke-direct {v0}, Lcom/sgiggle/production/factory/proximity/generic/handler/GenericProximityHandler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/factory/proximity/samsung/SamsungProximityFactory;->m_proximityHandler:Lcom/sgiggle/production/factory/proximity/handler/AbstractProximityHandler;

    goto :goto_1
.end method
