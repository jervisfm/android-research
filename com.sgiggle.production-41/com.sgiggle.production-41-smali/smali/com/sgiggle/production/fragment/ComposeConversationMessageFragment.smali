.class public Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;
.super Landroid/support/v4/app/Fragment;
.source "ComposeConversationMessageFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ComposeConversationMessageFragment"


# instance fields
.field private m_addMediaButton:Landroid/widget/ImageButton;

.field m_inAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

.field private m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

.field private m_sendButton:Landroid/widget/ImageButton;

.field private m_switcher:Landroid/widget/ViewSwitcher;

.field private m_text:Landroid/widget/TextView;

.field private m_vgoodButton:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 62
    new-instance v0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$1;-><init>(Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_inAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;)Landroid/widget/ViewSwitcher;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    return-object v0
.end method

.method private handleOpenConversationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 209
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getEmptySlotCount()I

    move-result v1

    .line 210
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getVgoodBundleList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->setSelectorVGoods(Ljava/util/List;I)V

    .line 211
    return-void
.end method

.method private sendComposingConversationMessage()V
    .locals 3

    .prologue
    .line 266
    const-string v0, "Tango.ComposeConversationMessageFragment"

    const-string v1, "sendComposingConversationMessage"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ComposingConversationMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ComposingConversationMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 269
    return-void
.end method

.method private setSelectorVGoods(Ljava/util/List;I)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x4

    .line 217
    new-instance v0, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;-><init>(Landroid/content/Context;)V

    .line 218
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setData(Ljava/util/List;Z)V

    .line 219
    if-le p2, v2, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/VGoodSelectorAdapter;->setEmptyCount(I)V

    .line 220
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    return-void

    :cond_0
    move v1, p2

    .line 219
    goto :goto_0
.end method


# virtual methods
.method public focusText()V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/sgiggle/production/Utils;->focusTextViewAndShowKeyboard(Landroid/content/Context;Landroid/widget/TextView;)V

    .line 247
    return-void
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 201
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 206
    :goto_0
    return-void

    .line 203
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->handleOpenConversationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;)V

    goto :goto_0

    .line 201
    :pswitch_data_0
    .packed-switch 0x89c6
        :pswitch_0
    .end packed-switch
.end method

.method public hideIme()V
    .locals 2

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/sgiggle/production/Utils;->hideKeyboard(Landroid/content/Context;Landroid/widget/TextView;)V

    .line 239
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 134
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    return-void

    .line 135
    :catch_0
    move-exception v1

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement ComposeConversationMessageFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    const-string v2, "Tango.ComposeConversationMessageFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_sendButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onComposeActionStarted()V

    .line 152
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v2, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v0, v1, v2, v3}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    .line 156
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_vgoodButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 159
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 160
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->hideIme()V

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getHeight()I

    move-result v0

    .line 164
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getHeight()I

    move-result v1

    if-ge v1, v0, :cond_2

    .line 165
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 166
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 174
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onComposeActionStarted()V

    goto :goto_0

    .line 172
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f09015c

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 175
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_addMediaButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onComposeActionStarted()V

    .line 177
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onAddMediaClicked()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    const v0, 0x7f030011

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 107
    const v0, 0x7f0a0055

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    .line 108
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 109
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 111
    const v0, 0x7f0a0056

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_sendButton:Landroid/widget/ImageButton;

    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_sendButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    const v0, 0x7f0a0053

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_addMediaButton:Landroid/widget/ImageButton;

    .line 115
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_addMediaButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    const v0, 0x7f0a0054

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_vgoodButton:Landroid/widget/ImageButton;

    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_vgoodButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 120
    const v0, 0x7f0a0051

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_inAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 123
    const v0, 0x7f0a0057

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/HorizontalListView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    .line 124
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_selector:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 126
    return-object v1
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 185
    packed-switch p2, :pswitch_data_0

    .line 194
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 187
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_sendButton:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->performClick()Z

    .line 188
    const/4 v0, 0x1

    goto :goto_0

    .line 185
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_text:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 259
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->sendComposingConversationMessage()V

    .line 261
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onComposeActionStarted()V

    .line 263
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 225
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    .line 226
    if-eqz v0, :cond_0

    .line 227
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetId()J

    move-result-wide v0

    .line 228
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 229
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    invoke-interface {v2}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->onComposeActionStarted()V

    .line 230
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_listener:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v4, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-interface {v2, v3, v4, v5}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    .line 235
    :goto_0
    return-void

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090117

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 251
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 252
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->m_switcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->requestFocus()Z

    .line 253
    return-void
.end method
