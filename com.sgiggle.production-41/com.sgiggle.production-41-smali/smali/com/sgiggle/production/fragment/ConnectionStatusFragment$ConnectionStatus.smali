.class public final enum Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;
.super Ljava/lang/Enum;
.source "ConnectionStatusFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/ConnectionStatusFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

.field public static final enum CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

.field public static final enum CONNECTING:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

.field public static final enum DISCONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    const-string v1, "CONNECTING"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTING:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    new-instance v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    const-string v1, "CONNECTED"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    new-instance v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    const-string v1, "DISCONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->DISCONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    .line 43
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTING:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->DISCONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->$VALUES:[Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;
    .locals 1
    .parameter

    .prologue
    .line 43
    const-class v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->$VALUES:[Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    invoke-virtual {v0}, [Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    return-object v0
.end method
