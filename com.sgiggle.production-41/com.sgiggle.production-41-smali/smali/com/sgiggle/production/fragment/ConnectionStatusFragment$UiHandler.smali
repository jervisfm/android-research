.class Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;
.super Landroid/os/Handler;
.source "ConnectionStatusFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/ConnectionStatusFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UiHandler"
.end annotation


# instance fields
.field m_fragment:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sgiggle/production/fragment/ConnectionStatusFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/fragment/ConnectionStatusFragment;)V
    .locals 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;->m_fragment:Ljava/lang/ref/WeakReference;

    .line 56
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 60
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 74
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled message type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;->m_fragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;->m_fragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    const/4 v1, 0x1

    #calls: Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setVisible(Z)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->access$000(Lcom/sgiggle/production/fragment/ConnectionStatusFragment;Z)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 68
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;->m_fragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;->m_fragment:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    const/4 v1, 0x0

    #calls: Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setVisible(Z)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->access$000(Lcom/sgiggle/production/fragment/ConnectionStatusFragment;Z)V

    goto :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
