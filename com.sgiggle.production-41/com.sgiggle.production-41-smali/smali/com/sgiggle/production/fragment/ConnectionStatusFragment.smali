.class public Lcom/sgiggle/production/fragment/ConnectionStatusFragment;
.super Landroid/support/v4/app/Fragment;
.source "ConnectionStatusFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/ConnectionStatusFragment$1;,
        Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;,
        Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;
    }
.end annotation


# static fields
.field private static final FRAGMENT_VISIBILITY_CHANGE_DELAY_MS:I = 0x3e8

.field private static final MSG_HIDE_FRAGMENT:I = 0x1

.field private static final MSG_SHOW_FRAGMENT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.ConnectionStatusFragment"

.field private static s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_progressBar:Landroid/view/View;

.field private m_text:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    sput-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    new-instance v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$UiHandler;-><init>(Lcom/sgiggle/production/fragment/ConnectionStatusFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    .line 23
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/ConnectionStatusFragment;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setVisible(Z)V

    return-void
.end method

.method public static setConnectionStatus(Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;)V
    .locals 0
    .parameter

    .prologue
    .line 202
    sput-object p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    .line 203
    return-void
.end method

.method public static setConnectionStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)V
    .locals 3
    .parameter

    .prologue
    .line 210
    const-string v0, "Tango.ConnectionStatusFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setConnectionStatus: new status="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    .line 213
    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$RetrieveOfflineMessageStatus:[I

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 226
    :goto_0
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setConnectionStatus(Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;)V

    .line 227
    return-void

    .line 216
    :pswitch_0
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    goto :goto_0

    .line 219
    :pswitch_1
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->DISCONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    goto :goto_0

    .line 222
    :pswitch_2
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTING:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setVisible(Z)V
    .locals 3
    .parameter

    .prologue
    .line 175
    const-string v0, "Tango.ConnectionStatusFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisible: show="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    :cond_0
    const-string v0, "Tango.ConnectionStatusFragment"

    const-string v1, "setVisible: won\'t change visibility, activity is finishing or view was not created yet."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :goto_0
    return-void

    .line 182
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 183
    const v1, 0x7f04000c

    const v2, 0x7f040011

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 184
    if-eqz p1, :cond_2

    .line 185
    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 193
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 187
    :cond_2
    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method private setVisibleDelayed(Z)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 156
    const-string v0, "Tango.ConnectionStatusFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVisibleDelayed: show="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    if-eqz p1, :cond_1

    move v0, v4

    .line 159
    :goto_0
    if-eqz p1, :cond_2

    move v1, v3

    .line 162
    :goto_1
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 168
    :cond_0
    return-void

    :cond_1
    move v0, v3

    .line 158
    goto :goto_0

    :cond_2
    move v1, v4

    .line 159
    goto :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 82
    const-string v0, "Tango.ConnectionStatusFragment"

    const-string v1, "onCreateView"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const v0, 0x7f030012

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 89
    const v0, 0x7f0a005a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_text:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f0a0059

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_progressBar:Landroid/view/View;

    .line 92
    return-object v1
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 98
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 99
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setVisible(Z)V

    .line 108
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->updateConnectionStatus()V

    .line 109
    return-void
.end method

.method public updateConnectionStatus()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    const-string v0, "Tango.ConnectionStatusFragment"

    const-string v1, "setConnectionStatus: won\'t show fragment, activity is finishing or view was not created yet."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :goto_0
    return-void

    .line 120
    :cond_1
    const/4 v0, -0x1

    .line 123
    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$1;->$SwitchMap$com$sgiggle$production$fragment$ConnectionStatusFragment$ConnectionStatus:[I

    sget-object v2, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    invoke-virtual {v2}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection status not supported: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :pswitch_0
    const v0, 0x7f09016a

    move v1, v0

    move v0, v4

    .line 141
    :goto_1
    if-lez v1, :cond_2

    .line 142
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_text:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 143
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->m_progressBar:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v3

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 147
    :cond_2
    sget-object v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->s_connectionStatus:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    sget-object v1, Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;->CONNECTED:Lcom/sgiggle/production/fragment/ConnectionStatusFragment$ConnectionStatus;

    if-eq v0, v1, :cond_4

    move v0, v4

    :goto_3
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->setVisibleDelayed(Z)V

    goto :goto_0

    :pswitch_1
    move v1, v0

    move v0, v3

    .line 131
    goto :goto_1

    .line 134
    :pswitch_2
    const v0, 0x7f09016b

    move v1, v0

    move v0, v3

    .line 135
    goto :goto_1

    .line 143
    :cond_3
    const/16 v0, 0x8

    goto :goto_2

    :cond_4
    move v0, v3

    .line 147
    goto :goto_3

    .line 123
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
