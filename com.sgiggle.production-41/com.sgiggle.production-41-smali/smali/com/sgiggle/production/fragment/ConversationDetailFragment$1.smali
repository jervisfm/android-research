.class Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;
.super Landroid/os/Handler;
.source "ConversationDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/ConversationDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/fragment/ConversationDetailFragment;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 182
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 192
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: unsupported message="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    #calls: Lcom/sgiggle/production/fragment/ConversationDetailFragment;->postShowMoreMessageMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->access$000(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    .line 194
    :goto_0
    return-void

    .line 188
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    #calls: Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startContinuousAnimations()V
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->access$100(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
