.class public interface abstract Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;
.super Ljava/lang/Object;
.source "ConversationDetailFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/ConversationDetailFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationDetailFragmentListener"
.end annotation


# virtual methods
.method public abstract getConversationPayload(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
.end method

.method public abstract hideIme()V
.end method

.method public abstract hideMenuItems()Z
.end method

.method public abstract isComposeEnabled()Z
.end method

.method public abstract onLoadFinished(I)V
.end method

.method public abstract onTipClicked()V
.end method

.method public abstract updateMenuItemsVisibility(Z)Z
.end method
