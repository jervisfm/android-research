.class public Lcom/sgiggle/production/fragment/ConversationDetailFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ConversationDetailFragment.java"

# interfaces
.implements Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/ConversationDetailFragment$3;,
        Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final KEY_CONVERSATION_ID:Ljava/lang/String; = "KEY_CONVERSATION_ID"

.field private static final MSG_SHOW_MORE:I = 0x0

.field private static final MSG_SHOW_MORE_DELAY_MS:I = 0x3e8

.field private static final MSG_START_CONTINUOUS_ANIMATIONS:I = 0x1

.field private static final MSG_START_CONTINUOUS_ANIMATIONS_DELAY_MS:I = 0x1f4

.field private static final REQUEST_COMPOSE_SMS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.ConversationDetailFragment"


# instance fields
.field private m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

.field private m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

.field private m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

.field private m_conversationId:Ljava/lang/String;

.field private m_deliveryErrorToast:Landroid/widget/Toast;

.field private m_emptyViewArrowsWrapper:Landroid/view/View;

.field private m_emptyViewWrapper:Landroid/view/View;

.field private m_handler:Landroid/os/Handler;

.field private m_isStarted:Z

.field private m_latestConversationPayloadHashCode:I

.field private m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

.field private m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

.field private m_participantList:Lcom/sgiggle/production/widget/HorizontalListView;

.field private m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

.field private m_participantListBar:Landroid/view/View;

.field private m_progressView:Landroid/view/View;

.field private m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

.field private m_tipComposeBubble:Landroid/view/View;

.field private m_tipComposeText:Landroid/widget/TextView;

.field private m_tipWrapper:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    .line 123
    iput-boolean v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_isStarted:Z

    .line 126
    iput v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_latestConversationPayloadHashCode:I

    .line 180
    new-instance v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$1;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->postShowMoreMessageMessage()V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startContinuousAnimations()V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->stopContinuousAnimations()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    return-object v0
.end method

.method private fill(Lcom/sgiggle/production/model/ConversationDetail;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 941
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConversationDetailFragment::fill(): data = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 946
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 949
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_3

    .line 950
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getMostRecentMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 951
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->getMostRecentMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v1

    .line 952
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v0

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 954
    :cond_0
    const/4 v0, 0x1

    move v1, v0

    .line 958
    :goto_0
    iput-object p1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    .line 960
    if-nez p1, :cond_1

    .line 961
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->clearData()V

    .line 962
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->setData(Ljava/util/List;)V

    .line 985
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateParticipantListBarVisibility()V

    .line 986
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateMenuItemsVisibility()V

    .line 989
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getMoreMessageAvailable()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->resetRefreshState(Z)V

    .line 991
    return v1

    .line 965
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationMessages()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->isGroupConversation()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setData(Ljava/util/List;Z)V

    .line 967
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->clear()V

    .line 970
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->isGroupConversation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 972
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f090140

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentActivity;->setTitle(I)V

    .line 975
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->getMyself()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->add(Ljava/lang/Object;)V

    .line 977
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationPeers()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->addData(Ljava/util/List;)V

    goto :goto_1

    .line 980
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationPeers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 981
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_0
.end method

.method private handleConversationMessageSendStatusEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 1
    .parameter

    .prologue
    .line 532
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateExistingConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)V

    .line 533
    return-void
.end method

.method private handleDeleteSingleConversationMessageEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 3
    .parameter

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/model/ConversationDetail;->deleteConversationMessage(I)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 516
    if-nez v0, :cond_1

    .line 517
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleDeleteSingleConversationMessageEvent: could not delete message with ID"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->remove(Ljava/lang/Object;)V

    .line 521
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateEmptyView()V

    goto :goto_0
.end method

.method private handleDisplayConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 444
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsForUpdate()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 447
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateExistingConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)V

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "handleDisplayConversationMessage: new message"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/model/ConversationDetail;->addConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 452
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->add(Ljava/lang/Object;)V

    .line 455
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isTipLayerShown()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 456
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideTipLayer(Z)V

    .line 459
    :cond_2
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 462
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onNewMessageFromSelf()V

    goto :goto_0
.end method

.method private handleForwardMessageResultEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;)V
    .locals 3
    .parameter

    .prologue
    .line 1067
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    .line 1068
    sget-object v1, Lcom/sgiggle/production/fragment/ConversationDetailFragment$3;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ForwardMessageResultType:[I

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1082
    sget-boolean v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 1070
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onForwardError()V

    .line 1085
    :cond_0
    :goto_0
    return-void

    .line 1074
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onForwardSuccess()V

    goto :goto_0

    .line 1078
    :pswitch_2
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onForwardBySMSNeeded(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)V

    goto :goto_0

    .line 1068
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleOpenConversationPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 372
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleOpenConversationPayload(), conversationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    new-instance v0, Lcom/sgiggle/production/model/ConversationDetail;

    invoke-direct {v0, p1}, Lcom/sgiggle/production/model/ConversationDetail;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V

    .line 379
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->fill(Lcom/sgiggle/production/model/ConversationDetail;)Z

    move-result v0

    .line 382
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasOpenConversationContext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->FAILED_OUTGOING_CALL:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    if-ne v1, v2, :cond_1

    move v1, v3

    .line 385
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->makeEmptyViewTransparent(Z)V

    .line 386
    invoke-direct {p0, v1, v3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateTipLayer(ZZ)V

    .line 388
    if-eqz v0, :cond_0

    .line 389
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onNewMessageFromSelf()V

    .line 391
    :cond_0
    return-void

    .line 382
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleReadStatusUpdateEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 555
    invoke-direct {p0, p2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideExistingMessageStatus(I)V

    .line 556
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateExistingConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)V

    .line 557
    return-void
.end method

.method private handleShowMoreMessageEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V
    .locals 4
    .parameter

    .prologue
    .line 473
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "handleShowMoreMessageEvent()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMoreMessageAvailable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/model/ConversationDetail;->setMoreMessageAvailable(Z)V

    .line 479
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMessageList()Ljava/util/List;

    move-result-object v0

    .line 480
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 481
    if-lez v1, :cond_0

    .line 482
    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2, v0}, Lcom/sgiggle/production/model/ConversationDetail;->addConversationMessagesAtFront(Ljava/util/List;)V

    .line 483
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationMessages()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationDetail;->isGroupConversation()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->setData(Ljava/util/List;Z)V

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getMoreMessageAvailable()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->onRefreshDone(IZ)V

    .line 489
    return-void
.end method

.method private handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 497
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceiversList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/Utils;->getSmsAddressListFromContactList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 500
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v0

    .line 502
    invoke-static {v1, v0}, Lcom/sgiggle/production/Utils;->getSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 504
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 508
    :goto_0
    return-void

    .line 505
    :catch_0
    move-exception v0

    .line 506
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "No activity was found for ACTION_VIEW (for sending SMS)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private hasData()Z
    .locals 1

    .prologue
    .line 999
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideExistingMessageStatus(I)V
    .locals 3
    .parameter

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/model/ConversationDetail;->updateMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 562
    if-nez v0, :cond_0

    .line 564
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleConversationMessageStatusPayload: message not found for Conversation #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    :goto_0
    return-void

    .line 567
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->notifyDataSetChanged()V

    .line 568
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/sgiggle/production/fragment/ConversationDetailFragment;
    .locals 3
    .parameter

    .prologue
    .line 600
    new-instance v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    invoke-direct {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;-><init>()V

    .line 602
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 603
    const-string v2, "KEY_CONVERSATION_ID"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 605
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->setArguments(Landroid/os/Bundle;)V

    .line 607
    return-object v0
.end method

.method private onForwardBySMSNeeded(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)V
    .locals 7
    .parameter

    .prologue
    .line 1107
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/Utils;->isSmsIntentAvailable(Landroid/content/Context;)Z

    move-result v1

    .line 1108
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f090152

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1109
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090153

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1110
    :goto_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v0

    .line 1111
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextIfNotSupport()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 1112
    :goto_1
    sget-object v5, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    .line 1113
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getSmsContactsList()Ljava/util/List;

    move-result-object v6

    .line 1114
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual/range {v0 .. v6}, Lcom/sgiggle/production/TangoApp;->showQuerySendSMSDialog(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;Ljava/util/List;)V

    .line 1115
    return-void

    .line 1109
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f090154

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    move-object v4, v0

    .line 1111
    goto :goto_1
.end method

.method private onForwardError()V
    .locals 3

    .prologue
    .line 1100
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090165

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1101
    return-void
.end method

.method private onForwardSuccess()V
    .locals 3

    .prologue
    .line 1092
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090164

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1093
    return-void
.end method

.method private onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 2
    .parameter

    .prologue
    .line 577
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/controller/ConversationController;->getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/controller/ConversationMessageController;->onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 579
    return-void
.end method

.method private onNewMessageFromSelf()V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setSelectionToBottom()V

    .line 401
    :cond_0
    return-void
.end method

.method private postFinishSMSComposeMessage(Z)V
    .locals 3
    .parameter

    .prologue
    .line 1025
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;-><init>(Z)V

    .line 1027
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1028
    return-void
.end method

.method private postShowMoreMessageMessage()V
    .locals 4

    .prologue
    .line 1006
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getOldestMessageId()I

    move-result v0

    .line 1008
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1009
    const-string v1, "Tango.ConversationDetailFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot post ShowMoreMessageMessage, previous message ID is invalid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1018
    :goto_0
    return-void

    .line 1013
    :cond_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageMessage;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getOldestMessageId()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageMessage;-><init>(Ljava/lang/String;I)V

    .line 1017
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private showProgressView(Z)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 867
    if-eqz p1, :cond_0

    .line 868
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setVisibility(I)V

    .line 869
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 870
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateParticipantListBarVisibility()V

    .line 876
    :goto_0
    return-void

    .line 872
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 873
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 874
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setVisibility(I)V

    goto :goto_0
.end method

.method private startContinuousAnimations()V
    .locals 2

    .prologue
    .line 780
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 781
    :cond_0
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "startContinuousAnimations: won\'t start animations, activity is finishing or view was not created yet."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 790
    :cond_1
    :goto_0
    return-void

    .line 786
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 787
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040005

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 788
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private startContinuousAnimationsDelayed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 768
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->isComposeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 774
    :cond_0
    return-void
.end method

.method private stopContinuousAnimations()V
    .locals 2

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 797
    :cond_0
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "stopContinuousAnimations: won\'t stop animations, activity is finishing or view was not created yet."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    :goto_0
    return-void

    .line 801
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private updateExistingConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/model/ConversationDetail;->updateMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 538
    if-nez v0, :cond_1

    .line 540
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleConversationMessageStatusPayload: message not found for Conversation #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v2}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Message #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_0
    :goto_0
    return-void

    .line 543
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->notifyDataSetChanged()V

    .line 544
    if-eqz p2, :cond_0

    .line 545
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onMessageStatusChanged(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0
.end method

.method private updateMenuItemsVisibility()V
    .locals 2

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->isSystemAccountConversation()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 911
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->hideMenuItems()Z

    .line 915
    :goto_0
    return-void

    .line 913
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationDetail;->isGroupConversation()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->updateMenuItemsVisibility(Z)Z

    goto :goto_0
.end method

.method private updateParticipantListBarVisibility()V
    .locals 2

    .prologue
    .line 896
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 897
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 901
    :goto_0
    return-void

    .line 899
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateTipLayer(ZZ)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v5, 0x0

    .line 409
    if-eqz p1, :cond_1

    .line 411
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09013f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v4}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayNameShort()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 415
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startContinuousAnimationsDelayed()V

    .line 416
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 437
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 418
    if-eqz p2, :cond_2

    .line 420
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f04

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 421
    new-instance v1, Lcom/sgiggle/production/fragment/ConversationDetailFragment$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$2;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 430
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 432
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 433
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 434
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->stopContinuousAnimations()V

    goto :goto_0
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/controller/ConversationController;->getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/sgiggle/production/controller/ConversationMessageController;->createNewMessage(Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    .line 365
    return-void
.end method

.method public getConversationDetail()Lcom/sgiggle/production/model/ConversationDetail;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    return-object v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 6
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 252
    .line 255
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 346
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    if-eqz v1, :cond_5

    .line 347
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/controller/ConversationController;->broadcastMessage(Lcom/sgiggle/messaging/Message;)Z

    move-result v1

    .line 353
    :goto_0
    return v1

    .line 257
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    .line 258
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    .line 260
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 261
    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleDisplayConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    move v1, v4

    .line 262
    goto :goto_0

    .line 265
    :cond_0
    const-string v2, "Tango.ConversationDetailFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: Discarding DISPLAY_CONVERSATION_MESSAGE_EVENT, for conv #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", current is #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 270
    goto :goto_0

    .line 273
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    .line 275
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 276
    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleConversationMessageSendStatusEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    move v1, v4

    .line 277
    goto :goto_0

    .line 280
    :cond_1
    const-string v2, "Tango.ConversationDetailFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: Discarding CONVERSATION_MESSAGE_SEND_STATUS_EVENT, for conv #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", current is #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 285
    goto/16 :goto_0

    .line 289
    :sswitch_2
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$SwapReadStatusesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SwapReadStatusesPayload;->getShowReadMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v2

    .line 291
    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 292
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$MessageUpdateReadStatusEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$SwapReadStatusesPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$SwapReadStatusesPayload;->getHideReadMessageId()I

    move-result v1

    invoke-direct {p0, v2, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleReadStatusUpdateEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)V

    move v1, v4

    .line 293
    goto/16 :goto_0

    .line 296
    :cond_2
    const-string v1, "Tango.ConversationDetailFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: Discarding CONVERSATION_MESSAGE_SEND_STATUS_EVENT, for conv #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", current is #"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 301
    goto/16 :goto_0

    .line 304
    :sswitch_3
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowMoreMessageEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    .line 306
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 307
    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleShowMoreMessageEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V

    move v1, v4

    .line 308
    goto/16 :goto_0

    .line 311
    :cond_3
    const-string v2, "Tango.ConversationDetailFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: Discarding SHOW_MORE_MESSAGE_EVENT, for conv #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", current is #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 316
    goto/16 :goto_0

    .line 319
    :sswitch_4
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;

    .line 320
    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V

    move v1, v5

    .line 321
    goto/16 :goto_0

    .line 324
    :sswitch_5
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    .line 327
    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 328
    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleDeleteSingleConversationMessageEvent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    move v1, v4

    .line 329
    goto/16 :goto_0

    .line 332
    :cond_4
    const-string v2, "Tango.ConversationDetailFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: Discarding DELETE_SINGLE_CONVERSATION_MESSAGE_EVENT, for conv #"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", current is #"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v5

    .line 337
    goto/16 :goto_0

    .line 340
    :sswitch_6
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;

    .line 341
    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleForwardMessageResultEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;)V

    move v1, v4

    .line 343
    goto/16 :goto_0

    :cond_5
    move v1, v5

    goto/16 :goto_0

    .line 255
    nop

    :sswitch_data_0
    .sparse-switch
        0x89c7 -> :sswitch_0
        0x89cc -> :sswitch_1
        0x89cd -> :sswitch_3
        0x89d0 -> :sswitch_6
        0x89d1 -> :sswitch_4
        0x89d6 -> :sswitch_5
        0x89f9 -> :sswitch_2
    .end sparse-switch
.end method

.method public hideIme()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->hideIme()V

    .line 244
    return-void
.end method

.method public hideTipLayer(Z)V
    .locals 1
    .parameter

    .prologue
    .line 1122
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateTipLayer(ZZ)V

    .line 1123
    return-void
.end method

.method public isCallEnabled()Z
    .locals 1

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->isSystemAccountConversation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTipLayerShown()Z
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeEmptyViewTransparent(Z)V
    .locals 2
    .parameter

    .prologue
    .line 1130
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateEmptyView()V

    .line 1131
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewWrapper:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1132
    return-void

    .line 1131
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 707
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "onActivityCreated()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 712
    new-instance v0, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    .line 713
    new-instance v0, Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/ParticipantListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    .line 716
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 717
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantList:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListAdapter:Lcom/sgiggle/production/adapter/ParticipantListAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 718
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 832
    if-ne p1, v0, :cond_1

    .line 837
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->postFinishSMSComposeMessage(Z)V

    .line 845
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    if-eqz v0, :cond_0

    .line 843
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sgiggle/production/controller/ConversationController;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 616
    const-string v1, "Tango.ConversationDetailFragment"

    const-string v2, "onAttach()"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 621
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    return-void

    .line 622
    :catch_0
    move-exception v1

    .line 623
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement ConversationDetailFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 625
    const-string v2, "Tango.ConversationDetailFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onAudioCallClicked()V
    .locals 6

    .prologue
    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v3

    .line 232
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v3

    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_OFF:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    .line 236
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 1141
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 1142
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->onTipClicked()V

    .line 1147
    :cond_0
    :goto_0
    return-void

    .line 1144
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 1145
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideTipLayer(Z)V

    goto :goto_0
.end method

.method public onComposeEnabled()V
    .locals 0

    .prologue
    .line 921
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateEmptyView()V

    .line 924
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startContinuousAnimationsDelayed()V

    .line 925
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    .line 693
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/controller/ConversationController;->getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;

    move-result-object v0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/controller/ConversationMessageController;->onContextItemSelected(ILcom/sgiggle/production/model/ConversationMessage;)V

    .line 697
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    .line 698
    const/4 v0, 0x1

    return v0
.end method

.method public onConversationPayloadChanged()V
    .locals 3

    .prologue
    .line 1041
    const-string v0, "Tango.ConversationDetailFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onConversationPayloadChanged, conversationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    iget-boolean v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_isStarted:Z

    if-nez v0, :cond_1

    .line 1045
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "onConversationPayloadChanged, fragment is not started, ignoring event"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    :cond_0
    :goto_0
    return-void

    .line 1050
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->getConversationPayload(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    .line 1051
    if-eqz v0, :cond_0

    .line 1052
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    .line 1053
    iget v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_latestConversationPayloadHashCode:I

    if-eq v1, v2, :cond_2

    .line 1054
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleOpenConversationPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V

    .line 1058
    :goto_1
    iput v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_latestConversationPayloadHashCode:I

    goto :goto_0

    .line 1056
    :cond_2
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v2, "onConversationPayloadChanged, payload has not changed, skipping"

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 200
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->setHasOptionsMenu(Z)V

    .line 203
    new-instance v0, Lcom/sgiggle/production/controller/ConversationController;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/controller/ConversationController;-><init>(Lcom/sgiggle/production/fragment/ConversationDetailFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    .line 205
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 206
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    if-ne p2, v0, :cond_0

    .line 679
    const v0, 0x7f090142

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 682
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 683
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    iget v1, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    .line 686
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/controller/ConversationController;->getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_selectedConversationMessage:Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/controller/ConversationMessageController;->populateContextMenu(Landroid/view/ContextMenu;Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 689
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 639
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "onCreateView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONVERSATION_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationId:Ljava/lang/String;

    .line 645
    const v0, 0x7f03001c

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 649
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    .line 650
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setEmptyView(Landroid/view/View;)V

    .line 651
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setListener(Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;)V

    .line 653
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listView:Lcom/sgiggle/production/widget/ScrollToRefreshListView;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 655
    const v0, 0x7f0a00c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_progressView:Landroid/view/View;

    .line 656
    const v0, 0x7f0a0089

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/HorizontalListView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantList:Lcom/sgiggle/production/widget/HorizontalListView;

    .line 657
    const v0, 0x7f0a0088

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_participantListBar:Landroid/view/View;

    .line 659
    const v0, 0x7f0a0082

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewWrapper:Landroid/view/View;

    .line 660
    const v0, 0x7f0a0084

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewArrowsWrapper:Landroid/view/View;

    .line 663
    const v0, 0x7f0a008a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    .line 664
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    const v2, 0x7f0a008b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    .line 665
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    const v2, 0x7f0a008c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeText:Landroid/widget/TextView;

    .line 666
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipWrapper:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 667
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_tipComposeBubble:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 668
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideTipLayer(Z)V

    .line 670
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->makeEmptyViewTransparent(Z)V

    .line 672
    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 826
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    .line 828
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 820
    const-string v0, "Tango.ConversationDetailFragment"

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroyView()V

    .line 822
    return-void
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 850
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    .line 854
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 855
    invoke-virtual {p1, p2}, Landroid/widget/ListView;->showContextMenuForChild(Landroid/view/View;)Z

    .line 859
    :goto_0
    return-void

    .line 857
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/controller/ConversationController;->getMessageController(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/production/controller/ConversationMessageController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/controller/ConversationMessageController;->doActionViewMessage(Lcom/sgiggle/production/model/ConversationMessage;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 210
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 215
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 212
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onVideoCallClicked()V

    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x7f0a01a8
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 806
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 808
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->stopAutoRefresh()V

    .line 809
    return-void
.end method

.method public onRefreshRequested()V
    .locals 4

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1035
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 740
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 743
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hasData()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->showProgressView(Z)V

    .line 746
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateParticipantListBarVisibility()V

    .line 747
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->updateMenuItemsVisibility()V

    .line 750
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetailAdapter:Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/adapter/ConversationMessageListAdapter;->startAutoRefresh(Z)V

    .line 751
    const-string v0, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 752
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090168

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 756
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->startContinuousAnimationsDelayed()V

    .line 758
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    if-eqz v0, :cond_1

    .line 759
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationController:Lcom/sgiggle/production/controller/ConversationController;

    invoke-virtual {v0}, Lcom/sgiggle/production/controller/ConversationController;->onResume()V

    .line 761
    :cond_1
    return-void

    .line 743
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 722
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStart()V

    .line 724
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_isStarted:Z

    .line 727
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onConversationPayloadChanged()V

    .line 730
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hasData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationDetail;->getConversationMessageCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->onLoadFinished(I)V

    .line 733
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 813
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_isStarted:Z

    .line 814
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->stopContinuousAnimations()V

    .line 815
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onStop()V

    .line 816
    return-void
.end method

.method public onVideoCallClicked()V
    .locals 6

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_conversationDetail:Lcom/sgiggle/production/model/ConversationDetail;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationDetail;->getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v3

    .line 222
    invoke-static {}, Lcom/sgiggle/production/CallHandler;->getDefault()Lcom/sgiggle/production/CallHandler;

    move-result-object v0

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v3

    sget-object v5, Lcom/sgiggle/production/CallHandler$VideoMode;->VIDEO_ON:Lcom/sgiggle/production/CallHandler$VideoMode;

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/CallHandler;->sendCallMessage(Ljava/lang/String;Ljava/lang/String;JLcom/sgiggle/production/CallHandler$VideoMode;)V

    .line 226
    :cond_0
    return-void
.end method

.method public showDeliveryError()V
    .locals 3

    .prologue
    .line 585
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_deliveryErrorToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 586
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090141

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_deliveryErrorToast:Landroid/widget/Toast;

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_deliveryErrorToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 591
    return-void
.end method

.method public updateEmptyView()V
    .locals 3

    .prologue
    .line 882
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;->isComposeEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 883
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewArrowsWrapper:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 884
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewArrowsWrapper:Landroid/view/View;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f040001

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 885
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewArrowsWrapper:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 888
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->m_emptyViewArrowsWrapper:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
