.class public Lcom/sgiggle/production/fragment/ConversationListActionsFragment;
.super Landroid/support/v4/app/Fragment;
.source "ConversationListActionsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationListActionsFragment"


# instance fields
.field private m_addTextButton:Landroid/widget/Button;

.field private m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 60
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    return-void

    .line 61
    :catch_0
    move-exception v1

    .line 62
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement ConversationListActionsFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 64
    const-string v2, "Tango.ConversationListActionsFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment;->m_addTextButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;->onAddTextClicked()V

    .line 75
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    const v0, 0x7f03001e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 49
    const v0, 0x7f0a008d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment;->m_addTextButton:Landroid/widget/Button;

    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment;->m_addTextButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    return-object v1
.end method
