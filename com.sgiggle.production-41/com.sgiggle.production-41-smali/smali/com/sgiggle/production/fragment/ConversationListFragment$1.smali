.class Lcom/sgiggle/production/fragment/ConversationListFragment$1;
.super Ljava/lang/Object;
.source "ConversationListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/fragment/ConversationListFragment;->scrollToPosition(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/fragment/ConversationListFragment;

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/sgiggle/production/fragment/ConversationListFragment;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationListFragment;

    iput p2, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 431
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationListFragment;

    #getter for: Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->access$200(Lcom/sgiggle/production/fragment/ConversationListFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->val$position:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    .line 436
    :goto_0
    return-void

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->this$0:Lcom/sgiggle/production/fragment/ConversationListFragment;

    #getter for: Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->access$200(Lcom/sgiggle/production/fragment/ConversationListFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$1;->val$position:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0
.end method
