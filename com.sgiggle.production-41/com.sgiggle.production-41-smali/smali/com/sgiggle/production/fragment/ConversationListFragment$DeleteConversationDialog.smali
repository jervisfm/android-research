.class Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ConversationListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/ConversationListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeleteConversationDialog"
.end annotation


# instance fields
.field private m_parent:Lcom/sgiggle/production/fragment/ConversationListFragment;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/fragment/ConversationListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;->m_parent:Lcom/sgiggle/production/fragment/ConversationListFragment;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;)Lcom/sgiggle/production/fragment/ConversationListFragment;
    .locals 1
    .parameter

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;->m_parent:Lcom/sgiggle/production/fragment/ConversationListFragment;

    return-object v0
.end method

.method public static newInstance(Lcom/sgiggle/production/fragment/ConversationListFragment;)Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;
    .locals 1
    .parameter

    .prologue
    .line 86
    new-instance v0, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;-><init>(Lcom/sgiggle/production/fragment/ConversationListFragment;)V

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 91
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020092

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f09012f

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090130

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090131

    new-instance v2, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog$2;-><init>(Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090020

    new-instance v2, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog$1;-><init>(Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
