.class public Lcom/sgiggle/production/fragment/ConversationListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "ConversationListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;
    }
.end annotation


# static fields
.field private static final ACTION_INDEX_DELETE:I = 0x1

.field private static final ACTION_INDEX_OPEN:I = 0x0

.field private static final KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD:Ljava/lang/String; = "KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD"

.field private static final TAG:Ljava/lang/String; = "Tango.ConversationListFragment"


# instance fields
.field private m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

.field private m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

.field private m_listView:Landroid/widget/ListView;

.field private m_listViewEmpty:Landroid/view/View;

.field private m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;

.field private m_myself:Lcom/sgiggle/production/model/ConversationContact;

.field private m_progressView:Landroid/view/View;

.field private m_selectedConversationId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 78
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/fragment/ConversationListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->postDeleteConversationMessage()V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/fragment/ConversationListFragment;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    return-object v0
.end method

.method private buildConversationSummaryFromPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationSummary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 259
    new-instance v0, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    .line 262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 263
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getConversationSummaryList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    .line 265
    new-instance v3, Lcom/sgiggle/production/model/ConversationSummary;

    iget-object v4, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    invoke-direct {v3, v0, v4}, Lcom/sgiggle/production/model/ConversationSummary;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 268
    :cond_0
    return-object v1
.end method

.method private handleConversationSummaryListPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)V
    .locals 2
    .parameter

    .prologue
    .line 219
    const-string v0, "Tango.ConversationListFragment"

    const-string v1, "handleConversationSummaryListPayload()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->buildConversationSummaryFromPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->setData(Ljava/util/List;)Z

    move-result v0

    .line 226
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->showProgressView(Z)V

    .line 231
    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->scrollToRelevantItem()V

    .line 234
    :cond_0
    return-void
.end method

.method private handleDeleteConversationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;)V
    .locals 2
    .parameter

    .prologue
    .line 253
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$StringPayload;->getText()Ljava/lang/String;

    move-result-object v0

    .line 254
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->remove(Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method private handleUpdateConversationSummaryEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;)V
    .locals 3
    .parameter

    .prologue
    .line 243
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryItemPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryItemPayload;->getSummary()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    .line 244
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->addOrUpdate(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V

    .line 245
    return-void
.end method

.method private hasData()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/production/fragment/ConversationListFragment;
    .locals 3
    .parameter

    .prologue
    .line 119
    new-instance v0, Lcom/sgiggle/production/fragment/ConversationListFragment;

    invoke-direct {v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;-><init>()V

    .line 121
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 122
    const-string v2, "KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 125
    invoke-virtual {v0, v1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 127
    return-object v0
.end method

.method private postDeleteConversationMessage()V
    .locals 3

    .prologue
    .line 324
    const-string v0, "Tango.ConversationListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deleting conversation with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_selectedConversationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_selectedConversationId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationMessage;-><init>(Ljava/lang/String;)V

    .line 328
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 329
    return-void
.end method

.method private postOpenConversationMessage(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 314
    const-string v0, "Tango.ConversationListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Opening conversation with ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationMessage;-><init>(Ljava/lang/String;)V

    .line 317
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 318
    return-void
.end method

.method private scrollToPosition(I)V
    .locals 3
    .parameter

    .prologue
    .line 424
    const-string v0, "Tango.ConversationListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "scrollToPosition="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    .line 427
    iget-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    if-ge p1, v0, :cond_0

    .line 428
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    new-instance v1, Lcom/sgiggle/production/fragment/ConversationListFragment$1;

    invoke-direct {v1, p0, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment$1;-><init>(Lcom/sgiggle/production/fragment/ConversationListFragment;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 439
    :cond_0
    return-void
.end method

.method private showProgressView(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 378
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_progressView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 389
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    if-eqz p1, :cond_2

    .line 383
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 387
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Lcom/sgiggle/messaging/Message;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 188
    const/4 v0, 0x0

    .line 190
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 209
    :goto_0
    return v0

    .line 192
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    .line 194
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleConversationSummaryListPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)V

    move v0, v2

    .line 196
    goto :goto_0

    .line 199
    :pswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleUpdateConversationSummaryEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$UpdateConversationSummaryEvent;)V

    move v0, v2

    .line 201
    goto :goto_0

    .line 204
    :pswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleDeleteConversationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteConversationEvent;)V

    move v0, v2

    .line 205
    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x89c8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 284
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 287
    new-instance v0, Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    .line 288
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 291
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleConversationSummaryListPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)V

    .line 294
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 136
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 144
    return-void

    .line 137
    :catch_0
    move-exception v1

    .line 138
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement ConversationListActionsFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 140
    const-string v2, "Tango.ConversationListFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listViewEmpty:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listener:Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;->onAddTextClicked()V

    .line 406
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .parameter

    .prologue
    .line 352
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 354
    packed-switch v0, :pswitch_data_0

    .line 366
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 356
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_selectedConversationId:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->postOpenConversationMessage(Ljava/lang/String;)V

    goto :goto_0

    .line 361
    :pswitch_1
    invoke-static {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;->newInstance(Lcom/sgiggle/production/fragment/ConversationListFragment;)Lcom/sgiggle/production/fragment/ConversationListFragment$DeleteConversationDialog;

    move-result-object v0

    .line 362
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 354
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 334
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    if-ne p2, v0, :cond_0

    .line 335
    check-cast p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 337
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    iget v1, p3, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    .line 338
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_selectedConversationId:Ljava/lang/String;

    .line 340
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getDisplayString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    .line 342
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    move v1, v3

    .line 344
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 345
    aget-object v2, v0, v1

    invoke-interface {p1, v3, v1, v1, v2}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 348
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 149
    const-string v0, "Tango.ConversationListFragment"

    const-string v1, "onCreateView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONVERSATION_SUMMARY_LIST_PAYLOAD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    .line 156
    const v0, 0x7f03001f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 160
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    .line 161
    const v0, 0x1020004

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listViewEmpty:Landroid/view/View;

    .line 162
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listViewEmpty:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listViewEmpty:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 166
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->videomailSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    const v0, 0x7f0a0090

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f09013a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 171
    :cond_0
    const v0, 0x7f0a00c4

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_progressView:Landroid/view/View;

    .line 173
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 176
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->showProgressView(Z)V

    .line 178
    return-object v1
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {v0, p3}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationSummary;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->postOpenConversationMessage(Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 298
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onPause()V

    .line 301
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->stopAutoRefresh()V

    .line 302
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 273
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 276
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->hasData()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->showProgressView(Z)V

    .line 279
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->startAutoRefresh(Z)V

    .line 280
    return-void

    .line 276
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public scrollToRelevantItem()V
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_listView:Landroid/widget/ListView;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, -0x1

    .line 416
    :goto_0
    if-lez v0, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->scrollToPosition(I)V

    .line 417
    return-void

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/ConversationListFragment;->m_adapter:Lcom/sgiggle/production/adapter/ConversationListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/ConversationListAdapter;->getFirstConversationPosWithUnreadMessages()I

    move-result v0

    goto :goto_0

    .line 416
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
