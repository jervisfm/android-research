.class public Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "DeleteConversationMessageDialog.java"


# instance fields
.field private final m_conversationId:Ljava/lang/String;

.field private final m_message:Lcom/sgiggle/production/model/ConversationMessage;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/model/ConversationMessage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->m_message:Lcom/sgiggle/production/model/ConversationMessage;

    .line 23
    iput-object p2, p0, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->m_conversationId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->postDeleteSingleConversationMessageMessage()V

    return-void
.end method

.method public static newInstance(Lcom/sgiggle/production/model/ConversationMessage;Ljava/lang/String;)Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 28
    new-instance v0, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;-><init>(Lcom/sgiggle/production/model/ConversationMessage;Ljava/lang/String;)V

    return-object v0
.end method

.method private postDeleteSingleConversationMessageMessage()V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageMessage;

    iget-object v1, p0, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->m_message:Lcom/sgiggle/production/model/ConversationMessage;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/model/ConversationMessage;->convertToSessionConversationMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteSingleConversationMessageMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 58
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 59
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 33
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020092

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090150

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090151

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090131

    new-instance v2, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog$2;-><init>(Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090020

    new-instance v2, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog$1;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog$1;-><init>(Lcom/sgiggle/production/fragment/DeleteConversationMessageDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
