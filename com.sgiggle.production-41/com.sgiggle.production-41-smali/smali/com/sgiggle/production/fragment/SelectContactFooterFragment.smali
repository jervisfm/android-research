.class public Lcom/sgiggle/production/fragment/SelectContactFooterFragment;
.super Landroid/support/v4/app/Fragment;
.source "SelectContactFooterFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/SelectContactFooterFragment$1;,
        Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.SelectContactFooterFragment"


# instance fields
.field private m_cancelButton:Landroid/widget/Button;

.field private m_listener:Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;

.field private m_selectButton:Landroid/widget/Button;

.field private m_type:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public onAttach(Landroid/app/Activity;)V
    .locals 3
    .parameter

    .prologue
    .line 72
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 73
    const-string v1, "Tango.SelectContactFooterFragment"

    const-string v2, "onAttach"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :try_start_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    return-void

    .line 77
    :catch_0
    move-exception v1

    .line 78
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAttach: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SelectContactFooterFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 80
    const-string v2, "Tango.SelectContactFooterFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_selectButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;->onSelectClicked()V

    .line 93
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_cancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;

    invoke-interface {v0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment$SelectContactFooterFragmentListener;->onCancelClicked()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    const-string v0, "Tango.SelectContactFooterFragment"

    const-string v1, "onCreateView"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    const v0, 0x7f030045

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 61
    const v0, 0x7f0a011a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_selectButton:Landroid/widget/Button;

    .line 62
    const v0, 0x7f0a011b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_cancelButton:Landroid/widget/Button;

    .line 64
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_selectButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_cancelButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-object v1
.end method

.method public onSelectedCountChanged(I)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 111
    const-string v0, "Tango.SelectContactFooterFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSelectedCountChanged: count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_type:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v0, 0x0

    .line 115
    sget-object v1, Lcom/sgiggle/production/fragment/SelectContactFooterFragment$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$SelectContactType:[I

    iget-object v2, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_type:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 133
    const-string v1, "Tango.SelectContactFooterFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setType: unknown type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_type:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :goto_0
    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_selectButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_selectButton:Landroid/widget/Button;

    if-lez p1, :cond_3

    move v1, v5

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 143
    return-void

    .line 117
    :pswitch_0
    if-gtz p1, :cond_1

    .line 118
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090136

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090137

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 125
    :pswitch_1
    if-gtz p1, :cond_2

    .line 126
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09016d

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v1, v4

    .line 141
    goto :goto_1

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setSelectContactType(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)V
    .locals 3
    .parameter

    .prologue
    .line 100
    const-string v0, "Tango.SelectContactFooterFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSelectContactType: contactType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iput-object p1, p0, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->m_type:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    .line 103
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactFooterFragment;->onSelectedCountChanged(I)V

    .line 104
    return-void
.end method
