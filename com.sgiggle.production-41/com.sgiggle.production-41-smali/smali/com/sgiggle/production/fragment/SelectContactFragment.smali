.class public abstract Lcom/sgiggle/production/fragment/SelectContactFragment;
.super Landroid/support/v4/app/Fragment;
.source "SelectContactFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
    }
.end annotation


# static fields
.field private static final KEY_CONTACT_LIST:Ljava/lang/String; = "KEY_CONTACT_LIST"

.field private static final KEY_CONTACT_LIST_CONTACT_ORDER_PAIR:Ljava/lang/String; = "KEY_CONTACT_LIST_CONTACT_ORDER_PAIR"

.field private static final KEY_CONTACT_LIST_MAX_SELECTION:Ljava/lang/String; = "KEY_CONTACT_LIST_MAX_SELECTION"

.field private static final KEY_CONTACT_LIST_SELECTION:Ljava/lang/String; = "KEY_CONTACT_LIST_SELECTION"

.field private static final TAG:Ljava/lang/String; = "Tango.SelectContactFragment"


# instance fields
.field protected m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

.field protected m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

.field protected m_contacts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_dataView:Landroid/view/View;

.field protected m_maxSelection:I

.field private m_progressView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 58
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 61
    invoke-static {}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_maxSelection:I

    .line 114
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;",
            "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
            "Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 58
    sget-object v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;->UNDEFINED:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 61
    invoke-static {}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDefault()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_maxSelection:I

    .line 86
    const-string v0, "Tango.SelectContactFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SelectContactFragment: #contacts="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", sortOrder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getSortOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", displayOrder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;->getDisplayOrder()Lcom/sgiggle/contacts/ContactStore$ContactOrderPair$ContactOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", maxSelection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v1, "KEY_CONTACT_LIST"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 91
    const-string v1, "KEY_CONTACT_LIST_SELECTION"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 92
    const-string v1, "KEY_CONTACT_LIST_CONTACT_ORDER_PAIR"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 93
    const-string v1, "KEY_CONTACT_LIST_MAX_SELECTION"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 95
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->setArguments(Landroid/os/Bundle;)V

    .line 96
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_0
.end method

.method private hasData()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contacts:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showProgressView(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_dataView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_progressView:Landroid/view/View;

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    if-eqz p1, :cond_2

    .line 206
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_dataView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 210
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_dataView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method protected abstract getDataViewResId()I
.end method

.method protected abstract getProgressViewResId()I
.end method

.method public handleBackPressed()Z
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract onContactsSet(Ljava/util/ArrayList;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;I)V"
        }
    .end annotation
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 120
    const-string v0, "Tango.SelectContactFragment"

    const-string v1, "onCreateView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONTACT_LIST"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contacts:Ljava/util/ArrayList;

    .line 124
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONTACT_LIST_SELECTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    .line 125
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONTACT_LIST_CONTACT_ORDER_PAIR"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    .line 126
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "KEY_CONTACT_LIST_MAX_SELECTION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_maxSelection:I

    .line 128
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment;->createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 131
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getDataViewResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_dataView:Landroid/view/View;

    .line 132
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->getProgressViewResId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_progressView:Landroid/view/View;

    .line 135
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/SelectContactFragment;->showProgressView(Z)V

    .line 137
    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 189
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->hasData()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->showProgressView(Z)V

    .line 190
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x1

    return v0
.end method

.method public onSelected()V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public setContacts(Ljava/util/ArrayList;I)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 155
    const-string v0, "Tango.SelectContactFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setContacts: maxSelection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    iput-object p1, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_contacts:Ljava/util/ArrayList;

    .line 158
    iput p2, p0, Lcom/sgiggle/production/fragment/SelectContactFragment;->m_maxSelection:I

    .line 163
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onContactsSet(Ljava/util/ArrayList;I)V

    .line 166
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->showProgressView(Z)V

    .line 167
    return-void
.end method
