.class Lcom/sgiggle/production/fragment/SelectContactListFragment$1;
.super Ljava/lang/Object;
.source "SelectContactListFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/fragment/SelectContactListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/fragment/SelectContactListFragment;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;->this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;->this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;

    #getter for: Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->access$000(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;->this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;

    #getter for: Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->access$100(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 68
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;->this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;

    #getter for: Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;
    invoke-static {v0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->access$000(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;->this$0:Lcom/sgiggle/production/fragment/SelectContactListFragment;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->getFilter(Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;)Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 70
    :cond_0
    return-void
.end method
