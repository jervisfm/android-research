.class public Lcom/sgiggle/production/fragment/SelectContactListFragment;
.super Lcom/sgiggle/production/fragment/SelectContactFragment;
.source "SelectContactListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
.implements Lcom/sgiggle/production/adapter/SelectContactListAdapter$ContactListFilterListener;


# static fields
.field private static final FAST_SCROLL_ENABLED:Z = true

.field private static final TAG:Ljava/lang/String; = "Tango.SelectContactListFragment"


# instance fields
.field private m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

.field private m_headerSwitcher:Landroid/widget/ViewSwitcher;

.field private m_listView:Landroid/widget/ListView;

.field private m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

.field private m_searchButton:Landroid/widget/ImageButton;

.field private m_searchText:Landroid/widget/EditText;

.field private m_searchTextWatcher:Landroid/text/TextWatcher;

.field private m_selectAllCheckbox:Landroid/widget/CheckBox;

.field private m_selectAllCheckboxText:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;-><init>()V

    .line 50
    new-instance v0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;-><init>(Lcom/sgiggle/production/fragment/SelectContactListFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchTextWatcher:Landroid/text/TextWatcher;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    .line 139
    return-void
.end method

.method private constructor <init>(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;",
            "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
            "Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sgiggle/production/fragment/SelectContactFragment;-><init>(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)V

    .line 50
    new-instance v0, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment$1;-><init>(Lcom/sgiggle/production/fragment/SelectContactListFragment;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchTextWatcher:Landroid/text/TextWatcher;

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Lcom/sgiggle/production/adapter/SelectContactListAdapter;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Z
    .locals 1
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->searchShown()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/fragment/SelectContactListFragment;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    return-object v0
.end method

.method public static newInstance(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)Lcom/sgiggle/production/fragment/SelectContactListFragment;
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;",
            "Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;",
            "Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;",
            "I)",
            "Lcom/sgiggle/production/fragment/SelectContactListFragment;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Lcom/sgiggle/production/fragment/SelectContactListFragment;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sgiggle/production/fragment/SelectContactListFragment;-><init>(Ljava/util/ArrayList;Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;I)V

    .line 114
    return-object v0
.end method

.method private searchShown()Z
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0a011f

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showSearch(Z)V
    .locals 2
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 338
    if-eqz p1, :cond_0

    const v1, 0x7f0a011f

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    const v1, 0x7f0a011e

    if-ne v0, v1, :cond_2

    .line 344
    :cond_1
    :goto_0
    return-void

    .line 343
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->showNext()V

    goto :goto_0
.end method

.method private unselectAllCheckboxSafe()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 387
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 388
    return-void
.end method


# virtual methods
.method protected createView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 152
    const v0, 0x7f030046

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 155
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    .line 156
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    const v2, 0x1020004

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 159
    const v0, 0x7f0a011c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchButton:Landroid/widget/ImageButton;

    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    const v0, 0x7f0a00b6

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 164
    const v0, 0x7f0a00b7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckboxText:Landroid/view/View;

    .line 166
    const v0, 0x7f0a011f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 169
    const v0, 0x7f0a011d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewSwitcher;

    iput-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    .line 170
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_headerSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v0}, Landroid/widget/ViewSwitcher;->getInAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 171
    if-eqz v0, :cond_0

    .line 172
    new-instance v2, Lcom/sgiggle/production/fragment/SelectContactListFragment$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment$2;-><init>(Lcom/sgiggle/production/fragment/SelectContactListFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 196
    :cond_0
    return-object v1
.end method

.method protected getDataViewResId()I
    .locals 1

    .prologue
    .line 242
    const v0, 0x7f0a0058

    return v0
.end method

.method protected getProgressViewResId()I
    .locals 1

    .prologue
    .line 247
    const v0, 0x7f0a00c4

    return v0
.end method

.method public handleBackPressed()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 399
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->searchShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 400
    invoke-direct {p0, v1}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->showSearch(Z)V

    .line 401
    const/4 v0, 0x1

    .line 404
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 204
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 206
    :try_start_0
    move-object v0, v2

    check-cast v0, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    move-object v1, v0

    iput-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 217
    new-instance v2, Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    iget-object v3, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contactListSelection:Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;

    iget-object v4, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contactOrderPair:Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;

    invoke-direct {v2, v3, v1, v4, p0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;-><init>(Lcom/sgiggle/production/adapter/ContactListAdapter$ContactListSelection;Landroid/view/LayoutInflater;Lcom/sgiggle/contacts/ContactStore$ContactOrderPair;Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;)V

    iput-object v2, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    .line 219
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contacts:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 222
    const-string v1, "Tango.SelectContactListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityCreated: maxSelection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_maxSelection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contacts:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_maxSelection:I

    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->onContactsSet(Ljava/util/ArrayList;I)V

    .line 228
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v1

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onAttach: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SelectContactListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    const-string v2, "Tango.SelectContactListFragment"

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    new-instance v2, Ljava/lang/ClassCastException;

    invoke-direct {v2, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 225
    :cond_0
    const-string v1, "Tango.SelectContactListFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityCreated: no contacts set. onContactsSet() should be called shortly. m_maxSelection="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_maxSelection:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 299
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    if-ne p1, v0, :cond_3

    .line 304
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    move v3, v2

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 305
    add-int/lit8 v3, v3, 0x1

    .line 306
    iget-boolean v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    if-eq v0, p2, :cond_5

    .line 307
    add-int/lit8 v0, v2, 0x1

    :goto_1
    move v2, v0

    goto :goto_0

    .line 311
    :cond_0
    if-lez v3, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    invoke-interface {v0, v2, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;->trySelectContacts(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 312
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 313
    iget-boolean v2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    if-eq v2, p2, :cond_1

    .line 314
    iput-boolean p2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    .line 315
    iget-boolean v2, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    invoke-virtual {p0, v0, v2}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->onContactSelectionChanged(Lcom/sgiggle/production/Utils$UIContact;Z)V

    goto :goto_2

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->notifyDataSetChanged()V

    .line 323
    :cond_3
    :goto_3
    return-void

    .line 320
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->unselectAllCheckboxSafe()V

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_0

    .line 328
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->searchShown()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->showSearch(Z)V

    .line 330
    :cond_0
    return-void

    .line 328
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onContactSelectionChanged(Lcom/sgiggle/production/Utils$UIContact;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 358
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->unselectAllCheckboxSafe()V

    .line 363
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->onSearchDone()V

    .line 366
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;->onContactSelectionChanged(Lcom/sgiggle/production/Utils$UIContact;Z)V

    .line 369
    :cond_1
    return-void
.end method

.method protected onContactsSet(Ljava/util/ArrayList;I)V
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/Utils$UIContact;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 252
    if-ne p2, v4, :cond_0

    move v0, v4

    .line 254
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v1, p1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->setContacts(Ljava/util/List;)V

    .line 255
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    if-nez v0, :cond_1

    move v2, v4

    :goto_1
    invoke-virtual {v1, v2}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->setShowCheckbox(Z)V

    .line 256
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->notifyDataSetChanged()V

    .line 258
    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 261
    if-eqz v0, :cond_2

    .line 262
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 263
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckboxText:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 268
    :goto_2
    return-void

    :cond_0
    move v0, v3

    .line 252
    goto :goto_0

    :cond_1
    move v2, v3

    .line 255
    goto :goto_1

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckboxText:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 144
    const-string v0, "Tango.SelectContactListFragment"

    const-string v1, "onCreateView()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onFilterDone(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->isSectionIndexerUpToDate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listView:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    .line 413
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->onItemClicked(Landroid/view/View;IJ)V

    .line 295
    return-void
.end method

.method public onSearchDone()V
    .locals 2

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/sgiggle/production/Utils;->hideKeyboard(Landroid/content/Context;Landroid/widget/TextView;)V

    .line 421
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 392
    invoke-direct {p0, v0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->showSearch(Z)V

    .line 393
    return v0
.end method

.method public onSearchStarting()V
    .locals 2

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/sgiggle/production/Utils;->focusTextViewAndShowKeyboard(Landroid/content/Context;Landroid/widget/TextView;)V

    .line 417
    return-void
.end method

.method public onSelected()V
    .locals 2

    .prologue
    .line 272
    invoke-super {p0}, Lcom/sgiggle/production/fragment/SelectContactFragment;->onSelected()V

    .line 275
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_selectAllCheckbox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_contacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/Utils$UIContact;

    .line 277
    iget-boolean v0, v0, Lcom/sgiggle/production/Utils$UIContact;->m_selected:Z

    if-nez v0, :cond_0

    .line 278
    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->unselectAllCheckboxSafe()V

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    if-eqz v0, :cond_2

    .line 286
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_adapter:Lcom/sgiggle/production/adapter/SelectContactListAdapter;

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/SelectContactListAdapter;->notifyDataSetChanged()V

    .line 288
    :cond_2
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1
    .parameter

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/sgiggle/production/fragment/SelectContactFragment;->setUserVisibleHint(Z)V

    .line 235
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_searchText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->searchShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/SelectContactListFragment;->onSearchDone()V

    .line 238
    :cond_0
    return-void
.end method

.method public trySelectContacts(IZ)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/sgiggle/production/fragment/SelectContactListFragment;->m_listener:Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/fragment/SelectContactFragment$SelectContactListener;->trySelectContacts(IZ)Z

    move-result v0

    .line 377
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
