.class public Lcom/sgiggle/production/fragment/StoreCategoryListFragment;
.super Landroid/support/v4/app/ListFragment;
.source "StoreCategoryListFragment.java"


# instance fields
.field private m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private handleDisplayEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 65
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getVgoodBadgeCount()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v4

    :goto_1
    invoke-virtual {v1, v3, v0}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->setNewBadge(IZ)V

    .line 69
    iget-object v1, p0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getGameBadgeCount()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v4

    :goto_2
    invoke-virtual {v1, v4, v0}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->setNewBadge(IZ)V

    .line 70
    iget-object v1, p0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    const/4 v2, 0x2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayStorePayload;->getAvatarBadgeCount()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v4

    :goto_3
    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;->setNewBadge(IZ)V

    goto :goto_0

    :cond_1
    move v0, v3

    .line 68
    goto :goto_1

    :cond_2
    move v0, v3

    .line 69
    goto :goto_2

    :cond_3
    move v0, v3

    .line 70
    goto :goto_3
.end method


# virtual methods
.method getItemClickedMessage(Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;)Lcom/sgiggle/messaging/Message;
    .locals 2
    .parameter

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 51
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported type!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :pswitch_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;-><init>()V

    .line 49
    :goto_0
    return-object v0

    .line 47
    :pswitch_1
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayGameCatalogMessage;-><init>()V

    goto :goto_0

    .line 49
    :pswitch_2
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarPaymentMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayAvatarPaymentMessage;-><init>()V

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    :goto_0
    return-void

    .line 58
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->handleDisplayEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayStoreEvent;)V

    goto :goto_0

    .line 56
    :pswitch_data_0
    .packed-switch 0x89c0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    const v0, 0x7f030052

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 25
    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;

    .line 38
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->getItemClickedMessage(Lcom/sgiggle/production/adapter/StoreCategoryAdapter$Category;)Lcom/sgiggle/messaging/Message;

    move-result-object v0

    .line 39
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 40
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 31
    new-instance v0, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/adapter/StoreCategoryAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    .line 32
    iget-object v0, p0, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->m_adapter:Lcom/sgiggle/production/adapter/StoreCategoryAdapter;

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/fragment/StoreCategoryListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 33
    return-void
.end method
