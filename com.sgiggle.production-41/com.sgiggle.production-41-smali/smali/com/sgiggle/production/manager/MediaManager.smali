.class public Lcom/sgiggle/production/manager/MediaManager;
.super Ljava/lang/Object;
.source "MediaManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.AudioManager"

.field private static s_instance:Lcom/sgiggle/production/manager/MediaManager;


# instance fields
.field private m_mediaPlayer:Landroid/media/MediaPlayer;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/manager/MediaManager;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/sgiggle/production/manager/MediaManager;->s_instance:Lcom/sgiggle/production/manager/MediaManager;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/sgiggle/production/manager/MediaManager;

    invoke-direct {v0}, Lcom/sgiggle/production/manager/MediaManager;-><init>()V

    sput-object v0, Lcom/sgiggle/production/manager/MediaManager;->s_instance:Lcom/sgiggle/production/manager/MediaManager;

    .line 38
    :cond_0
    sget-object v0, Lcom/sgiggle/production/manager/MediaManager;->s_instance:Lcom/sgiggle/production/manager/MediaManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized playAudioResource(Landroid/content/Context;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;

    .line 55
    :cond_0
    const-string v0, "Tango.AudioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "playAudioResource "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :try_start_1
    invoke-static {p1, p2}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 64
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 68
    const-string v0, "Tango.AudioManager"

    const-string v1, "MediaPlayer.create failed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 73
    :goto_0
    monitor-exit p0

    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 60
    :try_start_3
    const-string v1, "Tango.AudioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "playAudioResource: cannot find file with ID="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 72
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/sgiggle/production/manager/MediaManager;->m_mediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public playAudioResourceAsNotification(Landroid/content/Context;IZ)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 85
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 87
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 88
    const/4 v2, 0x0

    iput v2, v1, Landroid/app/Notification;->defaults:I

    .line 89
    if-nez p3, :cond_0

    .line 90
    iget v2, v1, Landroid/app/Notification;->defaults:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/app/Notification;->defaults:I

    .line 92
    :cond_0
    iget v2, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v1, Landroid/app/Notification;->flags:I

    .line 93
    invoke-static {p2}, Lcom/sgiggle/production/Utils;->getResourceUri(I)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 94
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 95
    return-void
.end method
