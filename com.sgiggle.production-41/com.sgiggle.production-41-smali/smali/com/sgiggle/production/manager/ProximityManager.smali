.class public Lcom/sgiggle/production/manager/ProximityManager;
.super Ljava/lang/Object;
.source "ProximityManager.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/manager/ProximityManager$Listener;
    }
.end annotation


# static fields
.field private static final PROXIMITY_THRESHOLD:F = 5.0f

.field private static final TAG:Ljava/lang/String; = "Tango.ProximityManager"


# instance fields
.field private m_hasProximitySensor:Z

.field private m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

.field private m_proximityMaximumRange:F

.field private m_proximitySensor:Landroid/hardware/Sensor;

.field private m_sensorManager:Landroid/hardware/SensorManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sgiggle/production/manager/ProximityManager$Listener;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-boolean v2, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    .line 55
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_sensorManager:Landroid/hardware/SensorManager;

    .line 56
    iput-object p2, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

    .line 59
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_sensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 60
    iput-boolean v2, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    .line 75
    :goto_0
    return-void

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_sensorManager:Landroid/hardware/SensorManager;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximitySensor:Landroid/hardware/Sensor;

    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximitySensor:Landroid/hardware/Sensor;

    if-nez v0, :cond_1

    .line 67
    iput-boolean v2, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    .line 72
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximitySensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getMaximumRange()F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximityMaximumRange:F

    goto :goto_0
.end method


# virtual methods
.method public disable()V
    .locals 2

    .prologue
    .line 91
    const-string v0, "Tango.ProximityManager"

    const-string v1, "Unegistered the proximity listener."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-boolean v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_sensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 95
    :cond_0
    return-void
.end method

.method public enable()V
    .locals 3

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_hasProximitySensor:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

    if-eqz v0, :cond_0

    .line 82
    const-string v0, "Tango.ProximityManager"

    const-string v1, "Registered the proximity listener to receive events."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximitySensor:Landroid/hardware/Sensor;

    const/4 v2, 0x3

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 85
    :cond_0
    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 100
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v0, v2

    .line 106
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    const/high16 v1, 0x40a0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    iget v1, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_proximityMaximumRange:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sgiggle/production/manager/ProximityManager$Listener;->onProximityChanged(Z)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/manager/ProximityManager;->m_listener:Lcom/sgiggle/production/manager/ProximityManager$Listener;

    invoke-interface {v0, v2}, Lcom/sgiggle/production/manager/ProximityManager$Listener;->onProximityChanged(Z)V

    goto :goto_0
.end method
