.class public Lcom/sgiggle/production/model/ConversationContact;
.super Ljava/lang/Object;
.source "ConversationContact.java"

# interfaces
.implements Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;


# instance fields
.field private m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private m_displayName:Ljava/lang/String;

.field private m_displayNameShort:Ljava/lang/String;

.field private m_hasPhoto:Z


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_hasPhoto:Z

    .line 29
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/model/ConversationContact;->refreshDisplayNameShort()V

    .line 31
    return-void
.end method

.method private refreshDisplayName()V
    .locals 2

    .prologue
    .line 74
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    .line 81
    :cond_0
    if-nez v0, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayNameShort()Ljava/lang/String;

    move-result-object v0

    .line 85
    :cond_1
    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayName:Ljava/lang/String;

    .line 86
    return-void
.end method

.method private refreshDisplayNameShort()V
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x0

    .line 45
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 47
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 61
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 62
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090132

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    :cond_1
    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayNameShort:Ljava/lang/String;

    .line 67
    return-void

    .line 49
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 52
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 53
    :cond_4
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 54
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 55
    :cond_5
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getDataToLoadImage()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/sgiggle/production/model/ConversationContact;->refreshDisplayName()V

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayNameShort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayNameShort:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 94
    invoke-direct {p0}, Lcom/sgiggle/production/model/ConversationContact;->refreshDisplayNameShort()V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_displayNameShort:Ljava/lang/String;

    return-object v0
.end method

.method public getImageId()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getViewTag()I
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isSame(Lcom/sgiggle/production/model/ConversationContact;)Z
    .locals 2
    .parameter

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_contact:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onLoadFailed()V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_hasPhoto:Z

    .line 132
    return-void
.end method

.method public shouldHaveImage()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationContact;->m_hasPhoto:Z

    return v0
.end method
