.class public Lcom/sgiggle/production/model/ConversationDetail;
.super Ljava/lang/Object;
.source "ConversationDetail.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private m_contactsByAccountId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_conversationId:Ljava/lang/String;

.field private m_conversationMessages:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sgiggle/production/model/ConversationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private m_conversationPeers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;"
        }
    .end annotation
.end field

.field private m_isGroupConversation:Z

.field private m_moreMessageAvailable:Z

.field private m_myself:Lcom/sgiggle/production/model/ConversationContact;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "Tango.ConversationDetail"

    sput-object v0, Lcom/sgiggle/production/model/ConversationDetail;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_moreMessageAvailable:Z

    .line 49
    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_isGroupConversation:Z

    .line 51
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMoreMessageAvailable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_moreMessageAvailable:Z

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    .line 58
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationId:Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    new-instance v1, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 69
    iget-object v2, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 74
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    .line 75
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMessageList()Ljava/util/List;

    move-result-object v0

    .line 77
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    .line 78
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationDetail;->addConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;

    goto :goto_1

    .line 80
    :cond_1
    return-void
.end method

.method private addConversationMessage(Lcom/sgiggle/production/model/ConversationMessage;)V
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 92
    return-void
.end method

.method private buildConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 5
    .parameter

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    .line 162
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 163
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 164
    const-string v0, "Could not find accountId=\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\' (message ID=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\', conv ID=\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'). List of known peer account IDs for this conversation: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    .line 172
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 173
    const-string v4, "\'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\', "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 177
    :cond_0
    const-string v0, " self accountId=\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    sget-object v0, Lcom/sgiggle/production/model/ConversationDetail;->TAG:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    .line 183
    :goto_1
    invoke-static {p1, v0}, Lcom/sgiggle/production/model/ConversationMessageFactory;->create(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    return-object v0

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_contactsByAccountId:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    goto :goto_1
.end method

.method private getMessageById(I)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 3
    .parameter

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    .line 193
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 198
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 1
    .parameter

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/sgiggle/production/model/ConversationDetail;->buildConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 149
    invoke-direct {p0, v0}, Lcom/sgiggle/production/model/ConversationDetail;->addConversationMessage(Lcom/sgiggle/production/model/ConversationMessage;)V

    .line 151
    return-object v0
.end method

.method public addConversationMessagesAtFront(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 144
    :goto_0
    return-void

    .line 134
    :cond_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 135
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    .line 136
    invoke-direct {p0, v0}, Lcom/sgiggle/production/model/ConversationDetail;->buildConversationMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_2

    .line 141
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    .line 143
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/util/LinkedList;->addAll(ILjava/util/Collection;)Z

    goto :goto_0
.end method

.method public convertPeersToContactList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 316
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 318
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    :cond_0
    return-object v1
.end method

.method public deleteConversationMessage(I)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    move-object v0, v3

    .line 114
    :goto_0
    return-object v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    .line 107
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 109
    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getConversationMessageCount()I
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getConversationMessages()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationMessage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getConversationPeers()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/production/model/ConversationContact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    return-object v0
.end method

.method public getFirstPeer()Lcom/sgiggle/production/model/ConversationContact;
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 274
    :cond_0
    const/4 v0, 0x0

    .line 276
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    goto :goto_0
.end method

.method public getMoreMessageAvailable()Z
    .locals 1

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_moreMessageAvailable:Z

    return v0
.end method

.method public getMostRecentMessage()Lcom/sgiggle/production/model/ConversationMessage;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 339
    :cond_0
    const/4 v0, 0x0

    .line 341
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    goto :goto_0
.end method

.method public getMyself()Lcom/sgiggle/production/model/ConversationContact;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_myself:Lcom/sgiggle/production/model/ConversationContact;

    return-object v0
.end method

.method public getOldestMessageId()I
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 331
    :cond_0
    const/4 v0, -0x1

    .line 333
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationMessage;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getMessageId()I

    move-result v0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFromMySelf(Lcom/sgiggle/production/model/ConversationMessage;)Z
    .locals 1
    .parameter

    .prologue
    .line 306
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    return v0
.end method

.method public isGroupConversation()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_isGroupConversation:Z

    return v0
.end method

.method public isSystemAccountConversation()Z
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationPeers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/ConversationContact;

    .line 359
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    const/4 v0, 0x0

    .line 364
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setMoreMessageAvailable(Z)V
    .locals 0
    .parameter

    .prologue
    .line 349
    iput-boolean p1, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_moreMessageAvailable:Z

    .line 350
    return-void
.end method

.method public updateMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    .line 248
    const/4 v0, 0x0

    .line 257
    :cond_0
    :goto_0
    return-object v0

    .line 252
    :cond_1
    invoke-direct {p0, p1}, Lcom/sgiggle/production/model/ConversationDetail;->getMessageById(I)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_0

    .line 254
    invoke-static {v0, p2}, Lcom/sgiggle/production/model/ConversationMessageFactory;->updateStatus(Lcom/sgiggle/production/model/ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V

    goto :goto_0
.end method

.method public updateMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 1
    .parameter

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationDetail;->m_conversationMessages:Ljava/util/LinkedList;

    if-nez v0, :cond_1

    .line 228
    const/4 v0, 0x0

    .line 237
    :cond_0
    :goto_0
    return-object v0

    .line 232
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/model/ConversationDetail;->getMessageById(I)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    invoke-static {v0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->update(Lcom/sgiggle/production/model/ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    goto :goto_0
.end method
