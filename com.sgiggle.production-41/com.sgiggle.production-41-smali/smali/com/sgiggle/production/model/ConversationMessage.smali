.class public abstract Lcom/sgiggle/production/model/ConversationMessage;
.super Ljava/lang/Object;
.source "ConversationMessage.java"


# static fields
.field public static final MESSAGE_ID_UNKNOWN:I = -0x1


# instance fields
.field private m_fromContact:Lcom/sgiggle/production/model/ConversationContact;

.field private m_isFromSelf:Z

.field private m_loadingStatus:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

.field private m_messageId:I

.field private m_readStatusTimestampMs:J

.field private m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

.field protected m_text:Ljava/lang/String;

.field private m_textInSms:Ljava/lang/String;

.field private m_timestampMs:J

.field private m_type:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;


# direct methods
.method protected constructor <init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_messageId:I

    .line 34
    iput-object p2, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_type:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 35
    iput-object p3, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_text:Ljava/lang/String;

    .line 36
    iput-wide p4, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_timestampMs:J

    .line 37
    iput-boolean p6, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_isFromSelf:Z

    .line 38
    iput-object p7, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_fromContact:Lcom/sgiggle/production/model/ConversationContact;

    .line 39
    iput-object p8, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 40
    iput-object p9, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_loadingStatus:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 41
    iput-wide p10, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_readStatusTimestampMs:J

    .line 42
    return-void
.end method

.method public static isStatusError(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z
    .locals 1
    .parameter

    .prologue
    .line 164
    invoke-static {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSending(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isStatusSending(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z
    .locals 1
    .parameter

    .prologue
    .line 125
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_TRIMMING_VIDEO:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_UPLOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isStatusSent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z
    .locals 1
    .parameter

    .prologue
    .line 182
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public convertToSessionConversationMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 2
    .parameter

    .prologue
    .line 254
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_type:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_messageId:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getFromContact()Lcom/sgiggle/production/model/ConversationContact;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_fromContact:Lcom/sgiggle/production/model/ConversationContact;

    return-object v0
.end method

.method public getMessageId()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_messageId:I

    return v0
.end method

.method public getReadStatusTimestampMs()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_readStatusTimestampMs:J

    return-wide v0
.end method

.method protected getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method

.method public getSummaryPrimaryText(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 271
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090134

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/model/ConversationMessage;->getText(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getSummarySecondaryText(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 288
    const-string v0, ""

    return-object v0
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessage;->getText(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getText(Z)Ljava/lang/String;
.end method

.method public getTextInSms()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_textInSms:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestampMs()J
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_timestampMs:J

    return-wide v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_type:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public isFromSelf()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_isFromSelf:Z

    return v0
.end method

.method public isLoadingStatusLoaded()Z
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_loadingStatus:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLoadingStatusLoading()Z
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_loadingStatus:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusError()Z
    .locals 1

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-static {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-static {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSending(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusRead()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusReadShow()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusErrorPeerNotSupported()Z
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusErrorPeerPlatformNotSupported()Z
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusRead()Z
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusReadShow()Z
    .locals 2

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusSending()Z
    .locals 1

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-static {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSending(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusSent()Z
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-static {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSent(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Z

    move-result v0

    return v0
.end method

.method public isStatusUnknown()Z
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStatusUploaded()Z
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_loadingStatus:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 108
    return-void
.end method

.method public setReadStatusTimestampMs(J)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_readStatusTimestampMs:J

    .line 88
    return-void
.end method

.method public setStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_status:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 100
    return-void
.end method

.method public setTextInSms(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessage;->m_textInSms:Ljava/lang/String;

    .line 72
    return-void
.end method
