.class public Lcom/sgiggle/production/model/ConversationMessageFactory;
.super Ljava/lang/Object;
.source "ConversationMessageFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/model/ConversationMessageFactory$1;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    return-void
.end method

.method private static canBePurchased(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProduct()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    .line 193
    :goto_0
    return v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPurchased()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    .line 181
    goto :goto_0

    .line 184
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 185
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasBeginTime()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getBeginTime()J

    move-result-wide v2

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    move v0, v4

    .line 186
    goto :goto_0

    .line 189
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->hasEndTime()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getEndTime()J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-gez v0, :cond_3

    move v0, v4

    .line 190
    goto :goto_0

    .line 193
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static create(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 18
    sget-object v0, Lcom/sgiggle/production/model/ConversationMessageFactory$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType:[I

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 30
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not implemented! Cannot create message of type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :pswitch_0
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->createText(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 26
    :goto_0
    return-object v0

    .line 22
    :pswitch_1
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->createVideoMail(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    goto :goto_0

    .line 24
    :pswitch_2
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->createVGood(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    goto :goto_0

    .line 26
    :pswitch_3
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->createPicture(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    goto :goto_0

    .line 18
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static createPicture(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 15
    .parameter
    .parameter

    .prologue
    .line 130
    new-instance v0, Lcom/sgiggle/production/model/ConversationMessagePicture;

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeSend()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v6

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v10

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v12

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRobotMessageType()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v7

    move-object v14, v7

    :goto_1
    move-object/from16 v7, p1

    invoke-direct/range {v0 .. v14}, Lcom/sgiggle/production/model/ConversationMessagePicture;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JILjava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    return-object v0

    :cond_0
    const-wide/16 v10, -0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    move-object v14, v7

    goto :goto_1
.end method

.method private static createText(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 13
    .parameter
    .parameter

    .prologue
    .line 122
    new-instance v0, Lcom/sgiggle/production/model/ConversationMessageText;

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeSend()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v6

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v8

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v9

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v10

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRobotMessageType()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v7

    move-object v12, v7

    :goto_1
    move-object v7, p1

    invoke-direct/range {v0 .. v12}, Lcom/sgiggle/production/model/ConversationMessageText;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    return-object v0

    :cond_0
    const-wide/16 v10, -0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    move-object v12, v7

    goto :goto_1
.end method

.method private static createVGood(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 19
    .parameter
    .parameter

    .prologue
    .line 147
    new-instance v0, Lcom/sgiggle/production/model/ConversationMessageVGood;

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeSend()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v10

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaId()Ljava/lang/String;

    move-result-object v12

    invoke-static/range {p0 .. p0}, Lcom/sgiggle/production/model/ConversationMessageFactory;->getVgoodPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;

    move-result-object v13

    invoke-static/range {p0 .. p0}, Lcom/sgiggle/production/model/ConversationMessageFactory;->getVgoodIconPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRobotMessageType()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v7

    move-object v15, v7

    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/sgiggle/production/model/ConversationMessageFactory;->canBePurchased(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Z

    move-result v16

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProduct()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v17, v7

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProduct()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getExternalMarketId()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v18, v7

    :goto_3
    move-object/from16 v7, p1

    invoke-direct/range {v0 .. v18}, Lcom/sgiggle/production/model/ConversationMessageVGood;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;ZLjava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    const-wide/16 v10, -0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    move-object v15, v7

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    move-object/from16 v17, v7

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    move-object/from16 v18, v7

    goto :goto_3
.end method

.method private static createVideoMail(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;
    .locals 20
    .parameter
    .parameter

    .prologue
    .line 138
    new-instance v0, Lcom/sgiggle/production/model/ConversationMessageVideo;

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeSend()J

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v10

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaId()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDuration()I

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRobotMessageType()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v7

    move-object/from16 v18, v7

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v19

    move-object/from16 v7, p1

    invoke-direct/range {v0 .. v19}, Lcom/sgiggle/production/model/ConversationMessageVideo;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;I)V

    return-object v0

    :cond_0
    const-wide/16 v10, -0x1

    goto :goto_0

    :cond_1
    const/4 v7, 0x0

    move-object/from16 v18, v7

    goto :goto_1
.end method

.method private static getVgoodIconPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasVgoodBundle()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v2

    .line 172
    :goto_0
    return-object v0

    .line 169
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getImageCount()I

    move-result v1

    if-nez v1, :cond_1

    move-object v0, v2

    .line 171
    goto :goto_0

    .line 172
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getImage(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodSelectorImage;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static getVgoodPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 158
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    .line 159
    if-nez v0, :cond_0

    move-object v0, v2

    .line 163
    :goto_0
    return-object v0

    .line 161
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v1

    if-nez v1, :cond_1

    move-object v0, v2

    .line 162
    goto :goto_0

    .line 163
    :cond_1
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getCinematic()Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodCinematic;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static update(Lcom/sgiggle/production/model/ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasSendStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessage;->setStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V

    .line 46
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/model/ConversationMessage;->setReadStatusTimestampMs(J)V

    .line 50
    :cond_1
    sget-object v0, Lcom/sgiggle/production/model/ConversationMessageFactory$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType:[I

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :pswitch_0
    check-cast p0, Lcom/sgiggle/production/model/ConversationMessageVideo;

    .line 54
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)V

    .line 56
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 57
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->setThumbnailLocalPath(Ljava/lang/String;)V

    .line 59
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isStatusSending()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 60
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->setProgress(I)V

    .line 80
    :cond_3
    :goto_0
    :pswitch_1
    return-void

    .line 68
    :pswitch_2
    check-cast p0, Lcom/sgiggle/production/model/ConversationMessagePicture;

    .line 69
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 70
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->updateSentPictureMessage(Lcom/sgiggle/production/model/ConversationMessagePicture;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    goto :goto_0

    .line 72
    :cond_4
    invoke-static {p0, p1}, Lcom/sgiggle/production/model/ConversationMessageFactory;->updateReceivedPictureMessage(Lcom/sgiggle/production/model/ConversationMessagePicture;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static updateReceivedPictureMessage(Lcom/sgiggle/production/model/ConversationMessagePicture;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 107
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v0

    .line 108
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne v0, v1, :cond_1

    .line 112
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)V

    .line 113
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setProgress(I)V

    .line 114
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setThumbnailLocalPath(Ljava/lang/String;)V

    .line 118
    :cond_1
    return-void
.end method

.method private static updateSentPictureMessage(Lcom/sgiggle/production/model/ConversationMessagePicture;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 94
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)V

    .line 95
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V

    .line 96
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setProgress(I)V

    .line 98
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setTextInSms(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->setThumbnailLocalPath(Ljava/lang/String;)V

    .line 103
    :cond_0
    return-void
.end method

.method public static updateStatus(Lcom/sgiggle/production/model/ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/model/ConversationMessage;->setStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)V

    .line 90
    return-void
.end method
