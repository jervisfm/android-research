.class public Lcom/sgiggle/production/model/ConversationMessagePicture;
.super Lcom/sgiggle/production/model/ConversationMessage;
.source "ConversationMessagePicture.java"

# interfaces
.implements Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;


# instance fields
.field private m_progress:I

.field private m_thumbnailFailedToLoad:Z

.field private m_thumbnailLocalPath:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JILjava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V
    .locals 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    move-object v2, p0

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-wide/from16 v12, p10

    move-object/from16 v14, p14

    invoke-direct/range {v2 .. v14}, Lcom/sgiggle/production/model/ConversationMessage;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    .line 24
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailFailedToLoad:Z

    .line 32
    move/from16 v0, p12

    move-object v1, p0

    iput v0, v1, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_progress:I

    .line 33
    move-object/from16 v0, p13

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public convertToSessionConversationMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 3
    .parameter

    .prologue
    .line 106
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDataToLoadImage()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getImageId()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_progress:I

    return v0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 95
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_0

    const v1, 0x7f090161

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v1, 0x7f090162

    goto :goto_0
.end method

.method public getThumbnailLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getViewTag()I
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onLoadFailed()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailFailedToLoad:Z

    .line 84
    return-void
.end method

.method public setProgress(I)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput p1, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_progress:I

    .line 57
    return-void
.end method

.method public setThumbnailLocalPath(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 42
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 46
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 47
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailFailedToLoad:Z

    .line 49
    :cond_1
    return-void
.end method

.method public shouldHaveImage()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessagePicture;->m_thumbnailFailedToLoad:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
