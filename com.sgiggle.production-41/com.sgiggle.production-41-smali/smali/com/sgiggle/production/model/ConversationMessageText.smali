.class public Lcom/sgiggle/production/model/ConversationMessageText;
.super Lcom/sgiggle/production/model/ConversationMessage;
.source "ConversationMessageText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/model/ConversationMessageText$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationMessageText"


# direct methods
.method protected constructor <init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V
    .locals 16
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-virtual/range {p7 .. p7}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v3

    move-object/from16 v0, p3

    move-object/from16 v1, p12

    move v2, v3

    invoke-static {v0, v1, v2}, Lcom/sgiggle/production/model/ConversationMessageText;->buildMessageText(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;Z)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v7, p4

    move/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-wide/from16 v13, p10

    move-object/from16 v15, p12

    invoke-direct/range {v3 .. v15}, Lcom/sgiggle/production/model/ConversationMessage;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    .line 27
    return-void
.end method

.method protected static buildMessageText(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;Z)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 49
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 51
    sget-object v2, Lcom/sgiggle/production/model/ConversationMessageText$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$RobotMessageType:[I

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 61
    const-string v1, "Tango.ConversationMessageText"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "buildMessageText: unknown ROBOT message type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    move-object v0, p0

    .line 72
    :cond_1
    return-object v0

    .line 54
    :pswitch_0
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 58
    :pswitch_1
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090157

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getText(Z)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageText;->m_text:Ljava/lang/String;

    return-object v0
.end method
