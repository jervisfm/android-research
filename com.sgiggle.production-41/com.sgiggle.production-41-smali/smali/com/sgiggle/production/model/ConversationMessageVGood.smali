.class public Lcom/sgiggle/production/model/ConversationMessageVGood;
.super Lcom/sgiggle/production/model/ConversationMessage;
.source "ConversationMessageVGood.java"

# interfaces
.implements Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;


# instance fields
.field private m_canBePurchased:Z

.field private m_externalMarketId:Ljava/lang/String;

.field private m_iconFailedToLoad:Z

.field private m_mediaId:Ljava/lang/String;

.field private m_productId:Ljava/lang/String;

.field private m_seed:J

.field private m_vgoodIconPath:Ljava/lang/String;

.field private m_vgoodId:J

.field private m_vgoodPath:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    move-object v2, p0

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-wide/from16 v12, p10

    move-object/from16 v14, p15

    invoke-direct/range {v2 .. v14}, Lcom/sgiggle/production/model/ConversationMessage;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    .line 23
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_iconFailedToLoad:Z

    .line 24
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_canBePurchased:Z

    .line 35
    move-object/from16 v0, p12

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_mediaId:Ljava/lang/String;

    .line 36
    move-object/from16 v0, p13

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodPath:Ljava/lang/String;

    .line 37
    move-object/from16 v0, p14

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodIconPath:Ljava/lang/String;

    .line 38
    new-instance v2, Ljava/util/Scanner;

    move-object v0, v2

    move-object/from16 v1, p12

    invoke-direct {v0, v1}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 39
    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/util/Scanner;->useDelimiter(Ljava/lang/String;)Ljava/util/Scanner;

    .line 40
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextLong()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/Scanner;->nextLong()J

    move-result-wide v3

    :goto_0
    iput-wide v3, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodId:J

    .line 41
    invoke-virtual {v2}, Ljava/util/Scanner;->hasNextLong()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/util/Scanner;->nextLong()J

    move-result-wide v2

    :goto_1
    iput-wide v2, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_seed:J

    .line 42
    move/from16 v0, p16

    move-object v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_canBePurchased:Z

    .line 43
    move-object/from16 v0, p17

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_productId:Ljava/lang/String;

    .line 44
    move-object/from16 v0, p18

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_externalMarketId:Ljava/lang/String;

    .line 45
    return-void

    .line 40
    :cond_0
    const-wide/16 v3, 0x0

    goto :goto_0

    .line 41
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public canBePurchased()Z
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_canBePurchased:Z

    return v0
.end method

.method public getDataToLoadImage()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodIconPath:Ljava/lang/String;

    return-object v0
.end method

.method public getExternalMarketId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_externalMarketId:Ljava/lang/String;

    return-object v0
.end method

.method public getImageId()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodIconPath:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_mediaId:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_productId:Ljava/lang/String;

    return-object v0
.end method

.method public getSeed()J
    .locals 2

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_seed:J

    return-wide v0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 108
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f09015d

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVGoodIconPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodIconPath:Ljava/lang/String;

    return-object v0
.end method

.method public getVGoodId()J
    .locals 2

    .prologue
    .line 52
    iget-wide v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodId:J

    return-wide v0
.end method

.method public getVGoodPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_vgoodPath:Ljava/lang/String;

    return-object v0
.end method

.method public getViewTag()I
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onLoadFailed()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_iconFailedToLoad:Z

    .line 76
    return-void
.end method

.method public shouldHaveImage()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVGood;->m_iconFailedToLoad:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
