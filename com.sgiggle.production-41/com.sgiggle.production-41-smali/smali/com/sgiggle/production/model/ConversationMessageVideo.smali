.class public Lcom/sgiggle/production/model/ConversationMessageVideo;
.super Lcom/sgiggle/production/model/ConversationMessage;
.source "ConversationMessageVideo.java"

# interfaces
.implements Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;


# static fields
.field private static final TAG:Ljava/lang/String; = "ConversationMessageVideo"


# instance fields
.field private m_durationDisplayString:Ljava/lang/String;

.field private m_durationSec:I

.field private m_hasThumbnail:Z

.field private m_mediaId:Ljava/lang/String;

.field private m_mediaLocalPath:Ljava/lang/String;

.field private m_mediaUrl:Ljava/lang/String;

.field private m_progress:I

.field private m_thumbnailFailedToLoad:Z

.field private m_thumbnailLocalPath:Ljava/lang/String;

.field private m_thumbnailRemoteUrl:Ljava/lang/String;


# direct methods
.method protected constructor <init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;I)V
    .locals 15
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    move-object v2, p0

    move/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-wide/from16 v12, p10

    move-object/from16 v14, p18

    invoke-direct/range {v2 .. v14}, Lcom/sgiggle/production/model/ConversationMessage;-><init>(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;JZLcom/sgiggle/production/model/ConversationContact;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;JLcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)V

    .line 34
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_hasThumbnail:Z

    .line 37
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailFailedToLoad:Z

    .line 52
    move-object/from16 v0, p12

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaId:Ljava/lang/String;

    .line 53
    move-object/from16 v0, p13

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaUrl:Ljava/lang/String;

    .line 54
    move-object/from16 v0, p14

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaLocalPath:Ljava/lang/String;

    .line 55
    move/from16 v0, p15

    move-object v1, p0

    iput v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationSec:I

    .line 56
    move-object/from16 v0, p16

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 57
    move-object/from16 v0, p17

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailRemoteUrl:Ljava/lang/String;

    .line 58
    move/from16 v0, p19

    move-object v1, p0

    iput v0, v1, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_progress:I

    .line 59
    return-void
.end method

.method private formatDurationDisplayString()V
    .locals 5

    .prologue
    .line 149
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f09012c

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    iget v1, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationSec:I

    div-int/lit8 v1, v1, 0x3c

    .line 153
    iget v2, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationSec:I

    rem-int/lit8 v2, v2, 0x3c

    .line 155
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationDisplayString:Ljava/lang/String;

    .line 156
    return-void
.end method


# virtual methods
.method public convertToSessionConversationMessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 3
    .parameter

    .prologue
    .line 207
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900ae

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDataToLoadImage()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 70
    iget v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationSec:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getDurationDisplayString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationDisplayString:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->formatDurationDisplayString()V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_durationDisplayString:Ljava/lang/String;

    return-object v0
.end method

.method public getImageId()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getMessageId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMediaId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaId:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getMediaUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_mediaUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_progress:I

    return v0
.end method

.method public getSummarySecondaryText(Landroid/content/Context;Z)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 195
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/model/ConversationMessage;->getSummarySecondaryText(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getDurationDisplayString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 187
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz p1, :cond_0

    const v1, 0x7f09012a

    :goto_0
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const v1, 0x7f09012b

    goto :goto_0
.end method

.method public getThumbnailLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getThumbnailRemoteUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailRemoteUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getViewTag()I
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isLocalPlaybackAvailable()Z
    .locals 3

    .prologue
    .line 124
    const/4 v0, 0x1

    .line 125
    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isFromSelf()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isStatusUploaded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 127
    const-string v0, "ConversationMessageVideo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video Message can\'t be playbacked  playback "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " upload status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v0, 0x0

    .line 130
    :cond_0
    return v0
.end method

.method public onLoadFailed()V
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailFailedToLoad:Z

    .line 176
    return-void
.end method

.method public setProgress(I)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput p1, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_progress:I

    .line 83
    return-void
.end method

.method public setThumbnailLocalPath(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 106
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailLocalPath:Ljava/lang/String;

    .line 110
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailFailedToLoad:Z

    .line 113
    :cond_1
    return-void
.end method

.method public setThumbnailRemoteUrl(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailRemoteUrl:Ljava/lang/String;

    .line 95
    iput-object p1, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailRemoteUrl:Ljava/lang/String;

    .line 98
    iget-boolean v1, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_hasThumbnail:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_hasThumbnail:Z

    .line 102
    :cond_0
    return-void
.end method

.method public shouldHaveImage()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/sgiggle/production/model/ConversationMessageVideo;->m_thumbnailFailedToLoad:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
