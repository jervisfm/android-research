.class public Lcom/sgiggle/production/model/ConversationSummary;
.super Ljava/lang/Object;
.source "ConversationSummary.java"


# instance fields
.field private m_conversationId:Ljava/lang/String;

.field private m_displayString:Ljava/lang/String;

.field private m_lastMessage:Lcom/sgiggle/production/model/ConversationMessage;

.field private m_peer:Lcom/sgiggle/production/model/ConversationContact;

.field private m_unreadMessageCount:I


# direct methods
.method public constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/model/ConversationSummary;->setData(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getConversationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_conversationId:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_displayString:Ljava/lang/String;

    return-object v0
.end method

.method public getLastMessage()Lcom/sgiggle/production/model/ConversationMessage;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_lastMessage:Lcom/sgiggle/production/model/ConversationMessage;

    return-object v0
.end method

.method public getPeer()Lcom/sgiggle/production/model/ConversationContact;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_peer:Lcom/sgiggle/production/model/ConversationContact;

    return-object v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_unreadMessageCount:I

    return v0
.end method

.method public setData(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/production/model/ConversationContact;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 62
    new-instance v0, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_peer:Lcom/sgiggle/production/model/ConversationContact;

    .line 64
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getLast()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p2

    .line 67
    :goto_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getLast()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sgiggle/production/model/ConversationMessageFactory;->create(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/production/model/ConversationContact;)Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_lastMessage:Lcom/sgiggle/production/model/ConversationMessage;

    .line 69
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_conversationId:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getUnreadMessageCount()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_unreadMessageCount:I

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_peer:Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/model/ConversationSummary;->m_displayString:Ljava/lang/String;

    .line 74
    return-void

    .line 64
    :cond_0
    new-instance v0, Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/production/model/ConversationContact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    goto :goto_0
.end method
