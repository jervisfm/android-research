.class public Lcom/sgiggle/production/model/Product;
.super Ljava/lang/Object;
.source "Product.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/model/Product$Managed;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sgiggle/production/model/Product;",
        ">;"
    }
.end annotation


# instance fields
.field private m_beginTime:J

.field private m_category:Ljava/lang/String;

.field private m_category_key:Ljava/lang/String;

.field private m_endTime:J

.field private m_leaseDuration:I

.field private m_managed:Lcom/sgiggle/production/model/Product$Managed;

.field private m_marketId:I

.field private m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

.field private m_productDescription:Ljava/lang/String;

.field private m_productId:J

.field private m_productMarketId:Ljava/lang/String;

.field private m_productName:Ljava/lang/String;

.field private m_purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field private m_sku:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sgiggle/production/model/Product;)I
    .locals 2
    .parameter

    .prologue
    .line 163
    iget v0, p0, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    iget v1, p1, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    if-le v0, v1, :cond_0

    .line 164
    const/4 v0, 0x1

    .line 168
    :goto_0
    return v0

    .line 165
    :cond_0
    iget v0, p0, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    iget v1, p1, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    if-ge v0, v1, :cond_1

    .line 166
    const/4 v0, -0x1

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 5
    check-cast p1, Lcom/sgiggle/production/model/Product;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/model/Product;->compareTo(Lcom/sgiggle/production/model/Product;)I

    move-result v0

    return v0
.end method

.method public getBeginTime()J
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/sgiggle/production/model/Product;->m_beginTime:J

    return-wide v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_category:Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_category_key:Ljava/lang/String;

    return-object v0
.end method

.method public getEndTime()J
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/sgiggle/production/model/Product;->m_endTime:J

    return-wide v0
.end method

.method public getFormattedPrice()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    const-string v0, ""

    .line 50
    iget-object v1, p0, Lcom/sgiggle/production/model/Product;->m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/model/Product;->m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Price;->hasValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Price;->getLabel()Ljava/lang/String;

    move-result-object v0

    .line 54
    :cond_0
    return-object v0
.end method

.method public getLeaseDuration()I
    .locals 1

    .prologue
    .line 146
    iget v0, p0, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    return v0
.end method

.method public getManaged()Lcom/sgiggle/production/model/Product$Managed;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_managed:Lcom/sgiggle/production/model/Product$Managed;

    return-object v0
.end method

.method public getMarketId()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sgiggle/production/model/Product;->m_marketId:I

    return v0
.end method

.method public getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

    return-object v0
.end method

.method public getProductDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_productDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()J
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/sgiggle/production/model/Product;->m_productId:J

    return-wide v0
.end method

.method public getProductMarketId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_productMarketId:Ljava/lang/String;

    return-object v0
.end method

.method public getProductName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_productName:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseState()Lcom/sgiggle/production/payments/Constants$PurchaseState;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    return-object v0
.end method

.method public getSku()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/model/Product;->m_sku:Ljava/lang/String;

    return-object v0
.end method

.method public setBeginTime(J)V
    .locals 0
    .parameter

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/sgiggle/production/model/Product;->m_beginTime:J

    .line 135
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_category:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public setCategoryKey(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_category_key:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setEndTime(J)V
    .locals 0
    .parameter

    .prologue
    .line 142
    iput-wide p1, p0, Lcom/sgiggle/production/model/Product;->m_endTime:J

    .line 143
    return-void
.end method

.method public setLeaseDuration(I)V
    .locals 0
    .parameter

    .prologue
    .line 150
    iput p1, p0, Lcom/sgiggle/production/model/Product;->m_leaseDuration:I

    .line 151
    return-void
.end method

.method public setManaged(Lcom/sgiggle/production/model/Product$Managed;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_managed:Lcom/sgiggle/production/model/Product$Managed;

    .line 45
    return-void
.end method

.method public setMarketId(I)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput p1, p0, Lcom/sgiggle/production/model/Product;->m_marketId:I

    .line 127
    return-void
.end method

.method public setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_price:Lcom/sgiggle/xmpp/SessionMessages$Price;

    .line 63
    return-void
.end method

.method public setProductDescription(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_productDescription:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setProductId(J)V
    .locals 0
    .parameter

    .prologue
    .line 78
    iput-wide p1, p0, Lcom/sgiggle/production/model/Product;->m_productId:J

    .line 79
    return-void
.end method

.method public setProductMarketId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_productMarketId:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setProductName(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_productName:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setPurchaseState(Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 0
    .parameter

    .prologue
    .line 158
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 159
    return-void
.end method

.method public setSku(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sgiggle/production/model/Product;->m_sku:Ljava/lang/String;

    .line 87
    return-void
.end method
