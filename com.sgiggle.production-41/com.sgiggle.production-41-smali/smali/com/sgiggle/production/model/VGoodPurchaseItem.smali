.class public Lcom/sgiggle/production/model/VGoodPurchaseItem;
.super Ljava/lang/Object;
.source "VGoodPurchaseItem.java"


# static fields
.field public static final PRODUCT_1MONTH:Ljava/lang/String; = "me.tango.tangodev.moments.pass.1month"

.field public static final PRODUCT_7DAYS:Ljava/lang/String; = "me.tango.tangodev.moments.pass.7day"

.field public static final PRODUCT_FAKE:Ljava/lang/String; = "android.test.purchased"


# instance fields
.field private m_description:Ljava/lang/String;

.field private final m_name:Ljava/lang/String;

.field private final m_productId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/model/VGoodPurchaseItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_productId:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_name:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_description:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_description:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_name:Ljava/lang/String;

    return-object v0
.end method

.method public getProductId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_productId:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sgiggle/production/model/VGoodPurchaseItem;->m_description:Ljava/lang/String;

    .line 36
    return-void
.end method
