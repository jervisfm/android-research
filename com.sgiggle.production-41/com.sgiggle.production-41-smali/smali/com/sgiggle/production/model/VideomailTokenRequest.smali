.class public Lcom/sgiggle/production/model/VideomailTokenRequest;
.super Ljava/lang/Object;
.source "VideomailTokenRequest.java"

# interfaces
.implements Lcom/sgiggle/production/model/Model;


# static fields
.field private static final KEY_ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field private static final KEY_CALLEES:Ljava/lang/String; = "callees"

.field private static final KEY_DURATION:Ljava/lang/String; = "duration"

.field private static final KEY_MIME_TYPE:Ljava/lang/String; = "mime"

.field private static final KEY_SIZE:Ljava/lang/String; = "size"


# instance fields
.field private m_accountId:Ljava/lang/String;

.field private m_callees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_duration:J

.field private m_mimeType:Ljava/lang/String;

.field private m_size:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_callees:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public addCallee(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_callees:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public build(Ljava/io/InputStream;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method public fromJSON(Lorg/json/JSONObject;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 32
    return-void
.end method

.method public getAccountId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_accountId:Ljava/lang/String;

    return-object v0
.end method

.method public getCallees()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_callees:Ljava/util/List;

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_duration:J

    return-wide v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_size:J

    return-wide v0
.end method

.method public setAccountId(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_accountId:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-wide p1, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_duration:J

    .line 82
    return-void
.end method

.method public setMimeType(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_mimeType:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setSize(J)V
    .locals 0
    .parameter

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_size:J

    .line 74
    return-void
.end method

.method public toJSON()Lorg/json/JSONObject;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 37
    const-string v1, "account_id"

    iget-object v2, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_accountId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 38
    const-string v1, "mime"

    iget-object v2, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_mimeType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 39
    const-string v1, "size"

    iget-wide v2, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_size:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 40
    const-string v1, "duration"

    iget-wide v2, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_duration:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 41
    const-string v1, "size"

    iget-wide v2, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_size:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 43
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 44
    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_callees:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 45
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 46
    const-string v4, "account_id"

    iget-object v5, p0, Lcom/sgiggle/production/model/VideomailTokenRequest;->m_callees:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 48
    invoke-virtual {v1, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 51
    :cond_0
    const-string v2, "callees"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    return-object v0
.end method
