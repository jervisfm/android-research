.class Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;
.super Ljava/io/FilterOutputStream;
.source "CountingMultipartEntity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/network/CountingMultipartEntity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CountingOutputStream"
.end annotation


# instance fields
.field m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

.field m_transferred:J


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 32
    iput-object p2, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    .line 34
    return-void
.end method


# virtual methods
.method public write(I)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    .line 46
    iget-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    iget-wide v1, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    invoke-interface {v0, v1, v2}, Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;->transferred(J)V

    .line 48
    return-void
.end method

.method public write([B)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    .line 53
    iget-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    array-length v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    .line 54
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    iget-wide v1, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    invoke-interface {v0, v1, v2}, Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;->transferred(J)V

    .line 55
    return-void
.end method

.method public write([BII)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 39
    iget-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    .line 40
    iget-object v0, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    iget-wide v1, p0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;->m_transferred:J

    invoke-interface {v0, v1, v2}, Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;->transferred(J)V

    .line 41
    return-void
.end method
