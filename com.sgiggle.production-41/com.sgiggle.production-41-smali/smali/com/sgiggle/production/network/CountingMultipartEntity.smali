.class public Lcom/sgiggle/production/network/CountingMultipartEntity;
.super Lorg/apache/http/entity/mime/MultipartEntity;
.source "CountingMultipartEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;,
        Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;
    }
.end annotation


# instance fields
.field private m_listener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/sgiggle/production/network/CountingMultipartEntity;->m_listener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    .line 19
    return-void
.end method


# virtual methods
.method public writeTo(Ljava/io/OutputStream;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;

    iget-object v1, p0, Lcom/sgiggle/production/network/CountingMultipartEntity;->m_listener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    invoke-direct {v0, p1, v1}, Lcom/sgiggle/production/network/CountingMultipartEntity$CountingOutputStream;-><init>(Ljava/io/OutputStream;Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V

    invoke-super {p0, v0}, Lorg/apache/http/entity/mime/MultipartEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 24
    return-void
.end method
