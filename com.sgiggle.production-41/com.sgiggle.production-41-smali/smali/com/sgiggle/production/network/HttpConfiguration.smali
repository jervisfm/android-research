.class public interface abstract Lcom/sgiggle/production/network/HttpConfiguration;
.super Ljava/lang/Object;
.source "HttpConfiguration.java"


# static fields
.field public static final ENCODING_GZIP:Ljava/lang/String; = "gzip"

.field public static final HEADER_ACCEPT_ENCODING:Ljava/lang/String; = "Accept-Encoding"

.field public static final USER_AGENT_BASE:Ljava/lang/String; = "Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17"


# virtual methods
.method public abstract getAuthCredentials()Lorg/apache/http/auth/Credentials;
.end method

.method public abstract getHttpBufferSize()I
.end method

.method public abstract getHttpConnectionTimeout()I
.end method

.method public abstract getHttpDefaultMaxPerRoute()I
.end method

.method public abstract getHttpMaxTotalConnections()I
.end method

.method public abstract getHttpReadTimeout()I
.end method

.method public abstract getHttpRetryCount()I
.end method

.method public abstract getHttpRetryIntervalSeconds()I
.end method

.method public abstract getUserAgent(Landroid/content/Context;)Ljava/lang/String;
.end method

.method public abstract isGZIPEnabled()Z
.end method

.method public abstract isPrettyDebugEnabled()Z
.end method
