.class public Lcom/sgiggle/production/network/RequestFactory;
.super Ljava/lang/Object;
.source "RequestFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static composeUrl(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 76
    if-eqz p1, :cond_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {p1, v1}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method private parseRequest(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 84
    .line 85
    const-string v0, "GET"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 99
    :goto_0
    return-object v0

    .line 88
    :cond_0
    const-string v0, "POST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 91
    :cond_1
    const-string v0, "PUT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 92
    new-instance v0, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    const-string v0, "DELETE"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    new-instance v0, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :cond_3
    const-string v0, "HEAD"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 98
    new-instance v0, Lorg/apache/http/client/methods/HttpHead;

    invoke-direct {v0, p2}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_4
    new-instance v0, Lorg/apache/http/MethodNotSupportedException;

    const-string v1, "Invalid request"

    invoke-direct {v0, v1}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public newHttpRequest(Lcom/sgiggle/production/network/command/Command;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;,
            Ljava/io/UnsupportedEncodingException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getMethod()Ljava/lang/String;

    move-result-object v1

    .line 43
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getUrl()Ljava/lang/String;

    move-result-object v3

    .line 46
    const-string v2, "GET"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 47
    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/production/network/RequestFactory;->composeUrl(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 68
    :goto_0
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 69
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getHeaders()[Lorg/apache/http/Header;

    move-result-object v2

    invoke-interface {v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeaders([Lorg/apache/http/Header;)V

    .line 72
    :cond_0
    return-object v1

    .line 48
    :cond_1
    const-string v2, "POST"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 49
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 51
    move-object v0, v2

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v1, v0

    new-instance v3, Lcom/sgiggle/production/network/URLEncodedEntity;

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sgiggle/production/network/URLEncodedEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v1, v3}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 52
    const-string v1, "Content-Type"

    const-string v3, "application/x-www-form-urlencoded"

    invoke-interface {v2, v1, v3}, Lorg/apache/http/client/methods/HttpUriRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    :cond_2
    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 56
    move-object v0, v2

    check-cast v0, Lorg/apache/http/client/methods/HttpPost;

    move-object v3, v0

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-virtual {v3, v1}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    move-object v1, v2

    goto :goto_0

    .line 58
    :cond_3
    const-string v2, "PUT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 59
    new-instance v1, Lorg/apache/http/client/methods/HttpPut;

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/production/network/RequestFactory;->composeUrl(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_4
    const-string v2, "DELETE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 61
    new-instance v1, Lorg/apache/http/client/methods/HttpDelete;

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/production/network/RequestFactory;->composeUrl(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_5
    const-string v2, "HEAD"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 63
    new-instance v1, Lorg/apache/http/client/methods/HttpHead;

    invoke-interface {p1}, Lcom/sgiggle/production/network/command/Command;->getParameters()Ljava/util/List;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/sgiggle/production/network/RequestFactory;->composeUrl(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpHead;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 65
    :cond_6
    new-instance v1, Lorg/apache/http/MethodNotSupportedException;

    const-string v2, "Invalid request"

    invoke-direct {v1, v2}, Lorg/apache/http/MethodNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v1, v2

    goto/16 :goto_0
.end method

.method public newHttpRequest(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 33
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/network/RequestFactory;->parseRequest(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 37
    return-object v0
.end method

.method public newHttpRequest(Lorg/apache/http/RequestLine;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/MethodNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-interface {p1}, Lorg/apache/http/RequestLine;->getMethod()Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-interface {p1}, Lorg/apache/http/RequestLine;->getUri()Ljava/lang/String;

    move-result-object v1

    .line 26
    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/network/RequestFactory;->parseRequest(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 28
    return-object v0
.end method
