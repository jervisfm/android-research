.class public interface abstract Lcom/sgiggle/production/network/command/Command;
.super Ljava/lang/Object;
.source "Command.java"


# virtual methods
.method public abstract getEntity()Lorg/apache/http/HttpEntity;
.end method

.method public abstract getHeaders()[Lorg/apache/http/Header;
.end method

.method public abstract getMethod()Ljava/lang/String;
.end method

.method public abstract getParameters()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method
