.class public Lcom/sgiggle/production/network/command/CommandFactory;
.super Ljava/lang/Object;
.source "CommandFactory.java"


# static fields
.field public static final COMMAND_UPLOAD:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    return-void
.end method


# virtual methods
.method public getCommand(I)Lcom/sgiggle/production/network/command/Command;
    .locals 3
    .parameter

    .prologue
    .line 11
    packed-switch p1, :pswitch_data_0

    .line 15
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported command request for command: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 13
    :pswitch_0
    new-instance v0, Lcom/sgiggle/production/network/command/UploadCommand;

    const-string v1, "/sdcard/output.3gp"

    invoke-direct {v0, v1}, Lcom/sgiggle/production/network/command/UploadCommand;-><init>(Ljava/lang/String;)V

    return-object v0

    .line 11
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
