.class public Lcom/sgiggle/production/network/command/UploadCommand;
.super Ljava/lang/Object;
.source "UploadCommand.java"

# interfaces
.implements Lcom/sgiggle/production/network/command/Command;


# instance fields
.field private m_file:Ljava/io/File;

.field private m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

.field private m_url:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid filename"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 25
    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_file:Ljava/io/File;

    .line 27
    iget-object v0, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "File does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_2
    return-void
.end method


# virtual methods
.method public getEntity()Lorg/apache/http/HttpEntity;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/sgiggle/production/network/CountingMultipartEntity;

    iget-object v1, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    invoke-direct {v0, v1}, Lcom/sgiggle/production/network/CountingMultipartEntity;-><init>(Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V

    .line 65
    :try_start_0
    const-string v1, "type"

    new-instance v2, Lorg/apache/http/entity/mime/content/StringBody;

    const-string v3, "videomessage"

    invoke-direct {v2, v3}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/network/CountingMultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 66
    const-string v1, "file"

    new-instance v2, Lorg/apache/http/entity/mime/content/FileBody;

    iget-object v3, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_file:Ljava/io/File;

    invoke-direct {v2, v3}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/network/CountingMultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    return-object v0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getHeaders()[Lorg/apache/http/Header;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const-string v0, "POST"

    return-object v0
.end method

.method public getParameters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_url:Ljava/lang/String;

    return-object v0
.end method

.method public setProgressListener(Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_progressListener:Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;

    .line 34
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sgiggle/production/network/command/UploadCommand;->m_url:Ljava/lang/String;

    .line 43
    return-void
.end method
