.class Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;
.super Ljava/lang/Object;
.source "BillingServiceManager.java"

# interfaces
.implements Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/payments/BillingServiceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BillingPurchaseObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/payments/BillingServiceManager;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/payments/BillingServiceManager;)V
    .locals 0
    .parameter

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/payments/BillingServiceManager;Lcom/sgiggle/production/payments/BillingServiceManager$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/payments/BillingServiceManager;)V

    return-void
.end method


# virtual methods
.method public onBillingSupported(Z)V
    .locals 3
    .parameter

    .prologue
    .line 133
    const-string v0, "BillingServiceMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBillingSupported:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    #setter for: Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z
    invoke-static {v0, p1}, Lcom/sgiggle/production/payments/BillingServiceManager;->access$102(Lcom/sgiggle/production/payments/BillingServiceManager;Z)Z

    .line 136
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    #getter for: Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z
    invoke-static {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->access$100(Lcom/sgiggle/production/payments/BillingServiceManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->restoreTransactions()V

    .line 139
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    #getter for: Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z
    invoke-static {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->access$200(Lcom/sgiggle/production/payments/BillingServiceManager;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;->unbindService()V

    .line 141
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/payments/BillingServiceManager;

    const/4 v1, 0x0

    #setter for: Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z
    invoke-static {v0, v1}, Lcom/sgiggle/production/payments/BillingServiceManager;->access$202(Lcom/sgiggle/production/payments/BillingServiceManager;Z)Z

    .line 143
    :cond_1
    return-void
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 177
    return-void
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 181
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Transactions response received: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    sget-object v0, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne p2, v0, :cond_0

    .line 183
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->setStoreTransactionRestored()V

    .line 184
    const-string v0, "BillingService"

    const-string v1, "Completed restoring transactions"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_0
    return-void

    .line 186
    :cond_0
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Transactions Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 153
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "postPurchaseStateChange productId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    sget-object v0, Lcom/sgiggle/production/payments/BillingServiceManager$1;->$SwitchMap$com$sgiggle$production$payments$Constants$PurchaseState:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 173
    :goto_0
    :pswitch_0
    return-void

    .line 166
    :pswitch_1
    invoke-static {}, Lcom/sgiggle/production/payments/BillingServiceManager;->getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;

    move-result-object v0

    iget-object v0, v0, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    invoke-virtual {v0, p2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 147
    return-void
.end method
