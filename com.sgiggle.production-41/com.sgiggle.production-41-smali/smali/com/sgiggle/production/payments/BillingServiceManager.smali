.class public Lcom/sgiggle/production/payments/BillingServiceManager;
.super Ljava/lang/Object;
.source "BillingServiceManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/payments/BillingServiceManager$1;,
        Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BillingServiceMgr"

.field private static mgr:Lcom/sgiggle/production/payments/BillingServiceManager;


# instance fields
.field private isBinded:Z

.field private isCheckBillingServiceSupport:Z

.field private isSupported:Z

.field private mContext:Landroid/content/Context;

.field private m_billingService:Lcom/sgiggle/production/service/BillingService;

.field private m_purchaseObserver:Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;

.field public purchaseStateMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sgiggle/production/payments/Constants$PurchaseState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/payments/BillingServiceManager;->mgr:Lcom/sgiggle/production/payments/BillingServiceManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isBinded:Z

    .line 27
    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    .line 29
    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    .line 33
    new-instance v0, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/payments/BillingServiceManager;Lcom/sgiggle/production/payments/BillingServiceManager$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_purchaseObserver:Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->purchaseStateMap:Ljava/util/HashMap;

    .line 44
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/payments/BillingServiceManager;)Z
    .locals 1
    .parameter

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sgiggle/production/payments/BillingServiceManager;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/payments/BillingServiceManager;)Z
    .locals 1
    .parameter

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sgiggle/production/payments/BillingServiceManager;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    return p1
.end method

.method private bindService()V
    .locals 4

    .prologue
    .line 68
    const-string v0, "BillingServiceMgr"

    const-string v1, "bindService"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    if-eqz v0, :cond_0

    .line 70
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_purchaseObserver:Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->registerObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sgiggle/production/service/BillingService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isBinded:Z

    .line 76
    :cond_1
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/payments/BillingServiceManager;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/sgiggle/production/payments/BillingServiceManager;->mgr:Lcom/sgiggle/production/payments/BillingServiceManager;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sgiggle/production/payments/BillingServiceManager;

    invoke-direct {v0}, Lcom/sgiggle/production/payments/BillingServiceManager;-><init>()V

    sput-object v0, Lcom/sgiggle/production/payments/BillingServiceManager;->mgr:Lcom/sgiggle/production/payments/BillingServiceManager;

    .line 40
    :cond_0
    sget-object v0, Lcom/sgiggle/production/payments/BillingServiceManager;->mgr:Lcom/sgiggle/production/payments/BillingServiceManager;

    return-object v0
.end method


# virtual methods
.method public bindServiceAndSetContext(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    .line 64
    invoke-direct {p0}, Lcom/sgiggle/production/payments/BillingServiceManager;->bindService()V

    .line 65
    return-void
.end method

.method public checkBillingSupport(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    .line 49
    invoke-direct {p0}, Lcom/sgiggle/production/payments/BillingServiceManager;->bindService()V

    .line 50
    return-void
.end method

.method public isBillingSupported()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    return v0
.end method

.method public isSupported()Z
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    return v0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 110
    const-string v0, "BillingServiceMgr"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isBinded:Z

    if-eqz v0, :cond_0

    .line 112
    check-cast p2, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;

    invoke-virtual {p2}, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;->getService()Lcom/sgiggle/production/service/BillingService;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService;->checkBillingSupported()Z

    .line 115
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .parameter

    .prologue
    .line 118
    const-string v0, "BillingServiceMgr"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    .line 120
    return-void
.end method

.method public requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/service/BillingService;->requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 126
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public restoreTransactions()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "BillingServiceMgr"

    const-string v1, "RestoreTransactions!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->areStoreTransactionRestored()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService;->restoreTransactions()Z

    .line 106
    :goto_0
    return-void

    .line 105
    :cond_0
    const-string v0, "BillingServiceMgr"

    const-string v1, "No billing service!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setSupported(Z)V
    .locals 3
    .parameter

    .prologue
    .line 57
    const-string v0, "BillingServiceMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "service isSupported:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iput-boolean p1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isSupported:Z

    .line 59
    return-void
.end method

.method public unbindService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    const-string v0, "BillingServiceMgr"

    const-string v1, "unbindService"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isCheckBillingServiceSupport:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_purchaseObserver:Lcom/sgiggle/production/payments/BillingServiceManager$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->unregisterObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)Z

    .line 83
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isBinded:Z

    if-eqz v0, :cond_1

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->isBinded:Z

    .line 90
    iput-object v2, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    .line 91
    iput-object v2, p0, Lcom/sgiggle/production/payments/BillingServiceManager;->mContext:Landroid/content/Context;

    .line 93
    :cond_1
    return-void

    .line 86
    :catch_0
    move-exception v0

    goto :goto_0
.end method
