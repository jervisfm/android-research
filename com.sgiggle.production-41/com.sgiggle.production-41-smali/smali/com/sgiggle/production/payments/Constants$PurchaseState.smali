.class public final enum Lcom/sgiggle/production/payments/Constants$PurchaseState;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/payments/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PurchaseState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/payments/Constants$PurchaseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field public static final enum CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field public static final enum PENDING:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field public static final enum PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field public static final enum PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field public static final enum REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    const-string v1, "PURCHASED"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/payments/Constants$PurchaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 22
    new-instance v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/payments/Constants$PurchaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 23
    new-instance v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    const-string v1, "REFUNDED"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/payments/Constants$PurchaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 24
    new-instance v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    const-string v1, "PURCHASE"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/payments/Constants$PurchaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 25
    new-instance v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    const-string v1, "PENDING"

    invoke-direct {v0, v1, v6}, Lcom/sgiggle/production/payments/Constants$PurchaseState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PENDING:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 19
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/production/payments/Constants$PurchaseState;

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PENDING:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->$VALUES:[Lcom/sgiggle/production/payments/Constants$PurchaseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(I)Lcom/sgiggle/production/payments/Constants$PurchaseState;
    .locals 2
    .parameter

    .prologue
    .line 29
    invoke-static {}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->values()[Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v0

    .line 30
    if-ltz p0, :cond_0

    array-length v1, v0

    if-lt p0, v1, :cond_1

    .line 31
    :cond_0
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 33
    :goto_0
    return-object v0

    :cond_1
    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/payments/Constants$PurchaseState;
    .locals 1
    .parameter

    .prologue
    .line 19
    const-class v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/payments/Constants$PurchaseState;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->$VALUES:[Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0}, [Lcom/sgiggle/production/payments/Constants$PurchaseState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/payments/Constants$PurchaseState;

    return-object v0
.end method
