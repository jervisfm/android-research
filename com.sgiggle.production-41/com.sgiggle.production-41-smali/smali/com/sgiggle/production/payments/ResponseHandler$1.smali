.class Lcom/sgiggle/production/payments/ResponseHandler$1;
.super Ljava/lang/Object;
.source "ResponseHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/payments/ResponseHandler;->purchaseResponse(Landroid/content/Context;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/payments/ResponseHandler;

.field final synthetic val$developerPayload:Ljava/lang/String;

.field final synthetic val$observer:Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

.field final synthetic val$orderId:Ljava/lang/String;

.field final synthetic val$productId:Ljava/lang/String;

.field final synthetic val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field final synthetic val$purchaseTime:J

.field final synthetic val$signature:Ljava/lang/String;

.field final synthetic val$signedData:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/payments/ResponseHandler;Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->this$0:Lcom/sgiggle/production/payments/ResponseHandler;

    iput-object p2, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$observer:Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    iput-object p3, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    iput-object p4, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$productId:Ljava/lang/String;

    iput-object p5, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$orderId:Ljava/lang/String;

    iput-wide p6, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$purchaseTime:J

    iput-object p8, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$developerPayload:Ljava/lang/String;

    iput-object p9, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$signedData:Ljava/lang/String;

    iput-object p10, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$signature:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 128
    const-class v9, Lcom/sgiggle/production/payments/ResponseHandler;

    monitor-enter v9

    .line 129
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$observer:Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    iget-object v1, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$productId:Ljava/lang/String;

    iget-object v3, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$orderId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$purchaseTime:J

    iget-object v6, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$developerPayload:Ljava/lang/String;

    iget-object v7, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$signedData:Ljava/lang/String;

    iget-object v8, p0, Lcom/sgiggle/production/payments/ResponseHandler$1;->val$signature:Ljava/lang/String;

    invoke-interface/range {v0 .. v8}, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;->postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    monitor-exit v9

    .line 132
    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
