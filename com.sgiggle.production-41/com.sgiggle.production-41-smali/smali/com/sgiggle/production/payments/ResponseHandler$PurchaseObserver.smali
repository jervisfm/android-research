.class public interface abstract Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;
.super Ljava/lang/Object;
.source "ResponseHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/payments/ResponseHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PurchaseObserver"
.end annotation


# virtual methods
.method public abstract onBillingSupported(Z)V
.end method

.method public abstract onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
.end method

.method public abstract onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
.end method

.method public abstract postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V
.end method
