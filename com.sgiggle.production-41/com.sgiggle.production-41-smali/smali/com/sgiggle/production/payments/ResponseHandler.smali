.class public Lcom/sgiggle/production/payments/ResponseHandler;
.super Ljava/lang/Object;
.source "ResponseHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;
    }
.end annotation


# instance fields
.field private m_observers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method public buyPageIntentResponse(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 73
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 74
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 75
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;->startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V

    goto :goto_0

    .line 76
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 77
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 81
    :cond_3
    return-void
.end method

.method public checkBillingSupported(Z)V
    .locals 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 59
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 60
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 61
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    invoke-interface {v0, p1}, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;->onBillingSupported(Z)V

    goto :goto_0

    .line 63
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 64
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_3
    return-void
.end method

.method public purchaseResponse(Landroid/content/Context;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 13
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 122
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 123
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    .line 124
    new-instance v12, Ljava/lang/Thread;

    new-instance v0, Lcom/sgiggle/production/payments/ResponseHandler$1;

    move-object v1, p0

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/sgiggle/production/payments/ResponseHandler$1;-><init>(Lcom/sgiggle/production/payments/ResponseHandler;Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v12, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v12}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 134
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    .line 135
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_3
    return-void
.end method

.method public registerObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)V
    .locals 2
    .parameter

    .prologue
    .line 37
    if-eqz p1, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    :cond_0
    return-void
.end method

.method public responseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 87
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 88
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;->onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V

    goto :goto_0

    .line 89
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 90
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 94
    :cond_3
    return-void
.end method

.method public responseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 98
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 100
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 101
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;

    invoke-interface {v0, p1, p2}, Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;->onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V

    goto :goto_0

    .line 102
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 103
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    :cond_3
    return-void
.end method

.method public unregisterObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)Z
    .locals 3
    .parameter

    .prologue
    .line 43
    if-eqz p1, :cond_3

    .line 44
    iget-object v0, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 45
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 46
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_1

    .line 47
    iget-object v1, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 54
    :goto_1
    return v0

    .line 48
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 49
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/payments/ResponseHandler;->m_observers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 54
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
