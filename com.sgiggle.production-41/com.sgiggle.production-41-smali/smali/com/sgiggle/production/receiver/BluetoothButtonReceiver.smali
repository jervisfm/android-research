.class public Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothButtonReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;
    }
.end annotation


# instance fields
.field private m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 41
    const-string v1, "android.intent.action.VoIP_BLUETOOTH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "Event"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 44
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 46
    iget-object v0, p0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;

    invoke-interface {v0}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;->acceptCall()V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;

    invoke-interface {v0}, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;->rejectCall()V

    goto :goto_0
.end method

.method public setButtonHandler(Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sgiggle/production/receiver/BluetoothButtonReceiver;->m_handler:Lcom/sgiggle/production/receiver/BluetoothButtonReceiver$ButtonHandler;

    .line 35
    return-void
.end method
