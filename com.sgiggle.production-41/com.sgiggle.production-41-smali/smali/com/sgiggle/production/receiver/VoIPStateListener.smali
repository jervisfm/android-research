.class public Lcom/sgiggle/production/receiver/VoIPStateListener;
.super Landroid/content/BroadcastReceiver;
.source "VoIPStateListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;
    }
.end annotation


# instance fields
.field private m_listener:Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sgiggle/production/receiver/VoIPStateListener;->m_listener:Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;

    .line 35
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 41
    const-string v0, "android.intent.action.VoIP_RESUME_CALL"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "ResumeType"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 45
    const/4 v1, 0x2

    if-ne v1, v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/sgiggle/production/receiver/VoIPStateListener;->m_listener:Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;

    invoke-interface {v0}, Lcom/sgiggle/production/receiver/VoIPStateListener$Listener;->handleCSCallResume()V

    .line 49
    :cond_0
    return-void
.end method
