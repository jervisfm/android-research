.class Lcom/sgiggle/production/screens/picture/ImageViewControl$2;
.super Ljava/lang/Object;
.source "ImageViewControl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/screens/picture/ImageViewControl;->transformImageAnimated(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/picture/ImageViewControl;

.field final synthetic val$fromScaleX:F

.field final synthetic val$fromScaleY:F

.field final synthetic val$fromTransX:F

.field final synthetic val$fromTransY:F

.field final synthetic val$interpolator:Landroid/view/animation/Interpolator;

.field final synthetic val$matrix:Landroid/graphics/Matrix;

.field final synthetic val$startTime:J

.field final synthetic val$thisImageView:Landroid/widget/ImageView;

.field final synthetic val$toScaleX:F

.field final synthetic val$toScaleY:F

.field final synthetic val$toTransX:F

.field final synthetic val$toTransY:F


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/picture/ImageViewControl;JLandroid/view/animation/Interpolator;FFFFFFFFLandroid/graphics/Matrix;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->this$0:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    iput-wide p2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$startTime:J

    iput-object p4, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$interpolator:Landroid/view/animation/Interpolator;

    iput p5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleX:F

    iput p6, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toScaleX:F

    iput p7, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleY:F

    iput p8, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toScaleY:F

    iput p9, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransX:F

    iput p10, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toTransX:F

    iput p11, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransY:F

    iput p12, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toTransY:F

    iput-object p13, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$matrix:Landroid/graphics/Matrix;

    iput-object p14, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$thisImageView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/high16 v8, 0x3f80

    .line 340
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$startTime:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x4396

    div-float/2addr v0, v1

    .line 341
    cmpl-float v1, v0, v8

    if-lez v1, :cond_0

    move v0, v8

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$interpolator:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 343
    iget v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleX:F

    iget v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toScaleX:F

    iget v4, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleX:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    add-float/2addr v2, v3

    .line 344
    iget v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleY:F

    iget v4, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toScaleY:F

    iget v5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromScaleY:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    .line 345
    iget v4, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransX:F

    iget v5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toTransX:F

    iget v6, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransX:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    .line 346
    iget v5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransY:F

    iget v6, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$toTransY:F

    iget v7, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$fromTransY:F

    sub-float/2addr v6, v7

    mul-float/2addr v1, v6

    add-float/2addr v1, v5

    .line 348
    iget-object v5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$matrix:Landroid/graphics/Matrix;

    invoke-virtual {v5}, Landroid/graphics/Matrix;->reset()V

    .line 351
    iget-object v5, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$matrix:Landroid/graphics/Matrix;

    invoke-virtual {v5, v2, v3}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 352
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$matrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 353
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->this$0:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$matrix:Landroid/graphics/Matrix;

    #calls: Lcom/sgiggle/production/screens/picture/ImageViewControl;->setMatrix(Landroid/graphics/Matrix;)V
    invoke-static {v1, v2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->access$100(Lcom/sgiggle/production/screens/picture/ImageViewControl;Landroid/graphics/Matrix;)V

    .line 354
    cmpg-float v0, v0, v8

    if-gez v0, :cond_1

    .line 355
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;->val$thisImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 357
    :cond_1
    return-void
.end method
