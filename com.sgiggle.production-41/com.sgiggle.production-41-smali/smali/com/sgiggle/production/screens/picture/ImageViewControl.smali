.class public Lcom/sgiggle/production/screens/picture/ImageViewControl;
.super Landroid/widget/ImageView;
.source "ImageViewControl.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final DURATION_OF_DOUBLE_TAP_IN_MS:I = 0x1f4

.field private static final DURATION_OF_TRANSFORMING_ANIMATION_IN_MS:I = 0x12c

.field private static final MAX_SCALE:I = 0x4

.field private static final TAG:Ljava/lang/String; = "Tango.ImageViewControl"

.field static final TOUCH_MODE_DRAG:I = 0x1

.field static final TOUCH_MODE_NONE:I = 0x0

.field static final TOUCH_MODE_ZOOM:I = 0x2

.field private static final ZOOM_IN_RATIO:F = 3.0f


# instance fields
.field m_bubbleTapEventHandler:Landroid/os/Handler;

.field m_curMatrix:Landroid/graphics/Matrix;

.field m_distBeforeZooming:F

.field m_firstTapEvent:Landroid/view/MotionEvent;

.field private m_isUriChanged:Z

.field private m_mappedImageBound:Landroid/graphics/RectF;

.field m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

.field m_midPointBeforeZooming:Landroid/graphics/Point;

.field private m_minScale:F

.field private m_oriImageBound:Landroid/graphics/RectF;

.field m_pointBeforePanning:Landroid/graphics/Point;

.field private m_scaleFittingScreen:F

.field m_tapEventBubbler:Ljava/lang/Runnable;

.field m_touchMode:I

.field private m_uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    .line 36
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    .line 41
    iput v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    .line 43
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    .line 44
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_bubbleTapEventHandler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;-><init>(Lcom/sgiggle/production/screens/picture/ImageViewControl;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_tapEventBubbler:Ljava/lang/Runnable;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 61
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    .line 36
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    .line 41
    iput v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    .line 43
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    .line 44
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_bubbleTapEventHandler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;-><init>(Lcom/sgiggle/production/screens/picture/ImageViewControl;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_tapEventBubbler:Ljava/lang/Runnable;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 65
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    .line 34
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    .line 36
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    .line 41
    iput v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    .line 42
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    .line 43
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    .line 44
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    .line 46
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    .line 48
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_bubbleTapEventHandler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl$1;-><init>(Lcom/sgiggle/production/screens/picture/ImageViewControl;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_tapEventBubbler:Ljava/lang/Runnable;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 69
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/picture/ImageViewControl;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->bubbleTapEvent()V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/picture/ImageViewControl;Landroid/graphics/Matrix;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setMatrix(Landroid/graphics/Matrix;)V

    return-void
.end method

.method private adjustMatrix(Landroid/graphics/Matrix;)V
    .locals 5
    .parameter

    .prologue
    const/high16 v3, 0x4080

    const/high16 v2, 0x3f80

    const/4 v4, 0x0

    .line 187
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_oriImageBound:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 190
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_oriImageBound:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float/2addr v0, v1

    .line 192
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_minScale:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 193
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_minScale:F

    div-float v0, v1, v0

    .line 197
    :goto_0
    cmpl-float v1, v0, v2

    if-eqz v1, :cond_0

    .line 198
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 199
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_oriImageBound:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 203
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getWidth()I

    move-result v0

    .line 204
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getHeight()I

    move-result v1

    .line 207
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    int-to-float v3, v0

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    .line 208
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_2

    .line 209
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    .line 218
    :goto_1
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, v1

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_5

    .line 219
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    .line 220
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    neg-float v1, v1

    .line 228
    :goto_2
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 229
    return-void

    .line 194
    :cond_1
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    mul-float/2addr v1, v3

    cmpl-float v1, v0, v1

    if-lez v1, :cond_8

    .line 195
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    mul-float/2addr v1, v3

    div-float v0, v1, v0

    goto :goto_0

    .line 210
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    int-to-float v3, v0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    .line 211
    int-to-float v0, v0

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v2

    goto :goto_1

    .line 214
    :cond_3
    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    sub-float/2addr v0, v2

    goto :goto_1

    .line 221
    :cond_4
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_6

    .line 222
    int-to-float v1, v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v2

    goto :goto_2

    .line 225
    :cond_5
    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_mappedImageBound:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    sub-float/2addr v1, v2

    goto :goto_2

    :cond_6
    move v1, v4

    goto :goto_2

    :cond_7
    move v0, v4

    goto :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method private beginDetectingDoubleTapEvent(Landroid/view/MotionEvent;)V
    .locals 4
    .parameter

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_bubbleTapEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_tapEventBubbler:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 362
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 363
    return-void
.end method

.method private bubbleTapEvent()V
    .locals 2

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 372
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 373
    return-void
.end method

.method private cancelDetectingDoubleTapEvent()V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_bubbleTapEventHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_tapEventBubbler:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 367
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    .line 368
    return-void
.end method

.method private getCurScale()F
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-static {v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getMatrixValues(Landroid/graphics/Matrix;)[F

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private getDistBetweenTwoFingers(Landroid/view/MotionEvent;)F
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 232
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 233
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 234
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method private static getMatrixValues(Landroid/graphics/Matrix;)[F
    .locals 1
    .parameter

    .prologue
    .line 380
    const/16 v0, 0x9

    new-array v0, v0, [F

    .line 381
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 382
    return-object v0
.end method

.method private getMidPointOfTwoFingers(Landroid/graphics/Point;Landroid/view/MotionEvent;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 239
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 240
    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    float-to-int v1, v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 241
    return-void
.end method

.method private handleTapEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .parameter

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_firstTapEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 292
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->onDoubleTapEvent(Landroid/view/MotionEvent;)V

    .line 293
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->cancelDetectingDoubleTapEvent()V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->beginDetectingDoubleTapEvent(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method private initForZoomingAndPanning(II)V
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/high16 v6, 0x3f80

    const/4 v5, 0x0

    .line 256
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 288
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 262
    if-nez v0, :cond_2

    .line 263
    const-string v0, "Tango.ImageViewControl"

    const-string v1, "image was not loaded successfully"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 267
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 268
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 269
    new-instance v2, Landroid/graphics/RectF;

    sub-int v3, v1, v4

    int-to-float v3, v3

    sub-int v4, v0, v4

    int-to-float v4, v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_oriImageBound:Landroid/graphics/RectF;

    .line 272
    mul-int v2, v1, p2

    mul-int v3, v0, p1

    if-le v2, v3, :cond_4

    .line 273
    int-to-float v0, p1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    .line 277
    :goto_1
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    iput v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_minScale:F

    .line 278
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_minScale:F

    cmpl-float v0, v0, v6

    if-lez v0, :cond_3

    .line 279
    iput v6, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_minScale:F

    .line 281
    :cond_3
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    if-eqz v0, :cond_5

    .line 282
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    .line 283
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->scaleImageAndAdjust(F)V

    goto :goto_0

    .line 275
    :cond_4
    int-to-float v1, p2

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    goto :goto_1

    .line 285
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->adjustMatrix(Landroid/graphics/Matrix;)V

    .line 286
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method private onDoubleTapEvent(Landroid/view/MotionEvent;)V
    .locals 5
    .parameter

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getCurScale()F

    move-result v0

    .line 302
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v1, v1

    const-wide v3, 0x3f847ae147ae147bL

    cmpg-double v1, v1, v3

    if-gez v1, :cond_0

    .line 303
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    const/high16 v2, 0x4040

    mul-float/2addr v1, v2

    .line 308
    :goto_0
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 309
    iget-object v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 310
    div-float v0, v1, v0

    .line 311
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v2, v0, v0, v1, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 312
    invoke-direct {p0, v2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->adjustMatrix(Landroid/graphics/Matrix;)V

    .line 314
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0, v2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->transformImageAnimated(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V

    .line 315
    return-void

    .line 305
    :cond_0
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_scaleFittingScreen:F

    goto :goto_0
.end method

.method private scaleImageAndAdjust(F)V
    .locals 1
    .parameter

    .prologue
    .line 244
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 245
    invoke-virtual {v0, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 246
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->adjustMatrix(Landroid/graphics/Matrix;)V

    .line 247
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setMatrix(Landroid/graphics/Matrix;)V

    .line 248
    return-void
.end method

.method private setMatrix(Landroid/graphics/Matrix;)V
    .locals 0
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    .line 183
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 184
    return-void
.end method

.method private transformImageAnimated(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)V
    .locals 15
    .parameter
    .parameter

    .prologue
    .line 318
    invoke-static/range {p1 .. p1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getMatrixValues(Landroid/graphics/Matrix;)[F

    move-result-object v0

    .line 319
    invoke-static/range {p2 .. p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getMatrixValues(Landroid/graphics/Matrix;)[F

    move-result-object v1

    .line 321
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 322
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 324
    new-instance v13, Landroid/graphics/Matrix;

    invoke-direct {v13}, Landroid/graphics/Matrix;-><init>()V

    .line 327
    const/4 v5, 0x0

    aget v5, v0, v5

    .line 328
    const/4 v6, 0x4

    aget v7, v0, v6

    .line 329
    const/4 v6, 0x0

    aget v6, v1, v6

    .line 330
    const/4 v8, 0x4

    aget v8, v1, v8

    .line 332
    const/4 v9, 0x2

    aget v9, v0, v9

    .line 333
    const/4 v10, 0x5

    aget v11, v0, v10

    .line 334
    const/4 v0, 0x2

    aget v10, v1, v0

    .line 335
    const/4 v0, 0x5

    aget v12, v1, v0

    .line 337
    new-instance v0, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;

    move-object v1, p0

    move-object v14, p0

    invoke-direct/range {v0 .. v14}, Lcom/sgiggle/production/screens/picture/ImageViewControl$2;-><init>(Lcom/sgiggle/production/screens/picture/ImageViewControl;JLandroid/view/animation/Interpolator;FFFFFFFFLandroid/graphics/Matrix;Landroid/widget/ImageView;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->post(Ljava/lang/Runnable;)Z

    .line 358
    return-void
.end method


# virtual methods
.method public loadImageToMemory()V
    .locals 3

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 100
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 101
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 102
    const-string v0, "Tango.ImageViewControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadImageToMemory() success "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :goto_1
    return-void

    .line 104
    :cond_1
    const-string v0, "Tango.ImageViewControl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadImageToMemory() bitmap is empty for file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-nez v2, :cond_2

    const-string v2, " unkown uri"

    :goto_2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    goto :goto_2

    .line 97
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public onSizeChanged(IIII)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 252
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 253
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->initForZoomingAndPanning(II)V

    .line 254
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    const/high16 v2, 0x4120

    const-wide v5, 0x3fb999999999999aL

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 120
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-nez v0, :cond_0

    move v0, v1

    .line 178
    :goto_0
    return v0

    .line 123
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 177
    :cond_1
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setMatrix(Landroid/graphics/Matrix;)V

    move v0, v4

    .line 178
    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 127
    iput v4, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    goto :goto_1

    .line 130
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getDistBetweenTwoFingers(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    .line 131
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    invoke-direct {p0, v0, p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getMidPointOfTwoFingers(Landroid/graphics/Point;Landroid/view/MotionEvent;)V

    .line 134
    iput v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    goto :goto_1

    .line 138
    :pswitch_3
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    .line 139
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 143
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 144
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 145
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 147
    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 148
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/StrictMath;->abs(F)F

    move-result v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    .line 149
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/StrictMath;->abs(F)F

    move-result v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 152
    float-to-double v1, v1

    cmpg-double v1, v1, v5

    if-gez v1, :cond_1

    float-to-double v0, v0

    cmpg-double v0, v0, v5

    if-gez v0, :cond_1

    .line 153
    invoke-direct {p0, p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->handleTapEvent(Landroid/view/MotionEvent;)V

    move v0, v4

    .line 154
    goto/16 :goto_0

    .line 158
    :pswitch_4
    iput v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    goto/16 :goto_1

    .line 161
    :pswitch_5
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    if-ne v0, v4, :cond_2

    .line 162
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_pointBeforePanning:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 165
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->adjustMatrix(Landroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 166
    :cond_2
    iget v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_touchMode:I

    if-ne v0, v3, :cond_1

    .line 167
    invoke-direct {p0, p2}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getDistBetweenTwoFingers(Landroid/view/MotionEvent;)F

    move-result v0

    .line 168
    cmpl-float v1, v0, v2

    if-lez v1, :cond_1

    .line 169
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_matrixBeforeZoomingPanning:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 170
    iget v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_distBeforeZooming:F

    div-float/2addr v0, v1

    .line 171
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_midPointBeforeZooming:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 172
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_curMatrix:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->adjustMatrix(Landroid/graphics/Matrix;)V

    goto/16 :goto_1

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 74
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    if-nez p1, :cond_3

    .line 78
    :cond_2
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    .line 83
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->unloadImageFromMemory()V

    .line 84
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    .line 85
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->loadImageToMemory()V

    .line 87
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->initForZoomingAndPanning(II)V

    goto :goto_0

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_uri:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/ImageViewControl;->m_isUriChanged:Z

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public unloadImageFromMemory()V
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 109
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    .line 110
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 111
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 114
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 116
    :cond_1
    return-void
.end method
