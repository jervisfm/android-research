.class public Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "PicturePreviewActivity.java"


# static fields
.field public static final PATH:Ljava/lang/String; = "path"

.field public static final RESULT_TITLE:Ljava/lang/String; = "result_title"

.field public static final RESULT_URI:Ljava/lang/String; = "result_uri"


# instance fields
.field private m_photoPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    return-void
.end method

.method private doesFileExist(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 92
    if-nez p1, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 96
    :goto_0
    return v0

    .line 95
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->setResult(I)V

    .line 54
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->finish()V

    .line 55
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 26
    const v0, 0x7f03003a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->setContentView(I)V

    .line 28
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 30
    const-string v1, "path"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->doesFileExist(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 33
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->setResult(I)V

    .line 34
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->finish()V

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_1
    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->m_photoPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public onFilter(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 58
    const-string v0, "Filtering..."

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 60
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 44
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->onCancel(Landroid/view/View;)V

    .line 45
    const/4 v0, 0x1

    .line 48
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onSend(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->m_photoPath:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 83
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 84
    const-string v1, "result_uri"

    iget-object v2, p0, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->m_photoPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    const-string v1, "result_title"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 86
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->m_photoPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->doesFileExist(Ljava/lang/String;)Z

    move-result v1

    .line 87
    if-eqz v1, :cond_1

    const/4 v1, -0x1

    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 88
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->finish()V

    goto :goto_0

    .line 87
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 72
    const v0, 0x7f0a00e1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/picture/ImageViewControl;

    .line 73
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->m_photoPath:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageURI(Landroid/net/Uri;)V

    .line 74
    const v1, 0x7f0a00e2

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 75
    invoke-virtual {v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    sub-int v1, v2, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 77
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStop()V

    .line 65
    const v0, 0x7f0a00e1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PicturePreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/picture/ImageViewControl;

    .line 66
    invoke-virtual {v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->unloadImageFromMemory()V

    .line 67
    return-void
.end method
