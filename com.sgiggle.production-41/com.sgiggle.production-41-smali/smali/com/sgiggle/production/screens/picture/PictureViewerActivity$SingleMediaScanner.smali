.class public Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;
.super Ljava/lang/Object;
.source "PictureViewerActivity.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/picture/PictureViewerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SingleMediaScanner"
.end annotation


# instance fields
.field private m_fileName:Ljava/lang/String;

.field private m_mediaScannerConnection:Landroid/media/MediaScannerConnection;

.field final synthetic this$0:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 428
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->this$0:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    iput-object p3, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_fileName:Ljava/lang/String;

    .line 430
    new-instance v0, Landroid/media/MediaScannerConnection;

    invoke-direct {v0, p2, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_mediaScannerConnection:Landroid/media/MediaScannerConnection;

    .line 431
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_mediaScannerConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    .line 432
    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_mediaScannerConnection:Landroid/media/MediaScannerConnection;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_fileName:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 441
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;->m_mediaScannerConnection:Landroid/media/MediaScannerConnection;

    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->disconnect()V

    .line 442
    return-void
.end method
