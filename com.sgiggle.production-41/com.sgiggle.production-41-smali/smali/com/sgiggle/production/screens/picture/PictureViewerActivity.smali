.class public Lcom/sgiggle/production/screens/picture/PictureViewerActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "PictureViewerActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field static final synthetic $assertionsDisabled:Z = false

.field private static final DURATION_OF_BAR_BEING_VISIBLE:I = 0x1388

.field private static final MENU_ITEM_ID_FORWARD:I = 0x0

.field private static final MENU_ITEM_ID_SAVE_TO_PHONE:I = 0x1

.field private static final REQUEST_CODE_SMS_OLD_CLIENT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Tango.PictureViewerActivity"

.field private static s_instance:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;


# instance fields
.field m_FadeOutAnimation:Landroid/view/animation/Animation;

.field private m_actionBar:Landroid/view/View;

.field m_barHider:Ljava/lang/Runnable;

.field m_btnDoMoreOperations:Landroid/widget/Button;

.field private m_conversationId:Ljava/lang/String;

.field private m_imagePath:Ljava/lang/String;

.field m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

.field private m_isBarVisible:Z

.field m_isForwardEnabled:Z

.field private m_messageId:I

.field m_progressBar:Landroid/widget/ProgressBar;

.field m_timeoutHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isBarVisible:Z

    .line 69
    new-instance v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$1;-><init>(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_barHider:Ljava/lang/Runnable;

    .line 77
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_timeoutHandler:Landroid/os/Handler;

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isForwardEnabled:Z

    .line 424
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->hideBar(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    return-object v0
.end method

.method private back()V
    .locals 3

    .prologue
    .line 375
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelViewPictureMessage;-><init>()V

    .line 377
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 378
    return-void
.end method

.method private cancelTimerTriggerHidingBar()V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_timeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_barHider:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 361
    return-void
.end method

.method private changeImage(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    if-ne v0, p1, :cond_0

    .line 225
    :goto_0
    return-void

    .line 214
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 216
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageURI(Landroid/net/Uri;)V

    .line 219
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->startTimerTriggerHidingBar()V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 98
    sget-object v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->s_instance:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    if-ne v0, p0, :cond_0

    .line 99
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->s_instance:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    .line 101
    :cond_0
    return-void
.end method

.method private copyFile()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 260
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureStorage;->getTmpPicPath(Landroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imagePath:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/sgiggle/production/util/FileOperator;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    new-instance v1, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;

    invoke-direct {v1, p0, p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$SingleMediaScanner;-><init>(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;Landroid/content/Context;Ljava/lang/String;)V

    move v0, v2

    .line 266
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMessageMediaPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 197
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/screens/picture/PictureViewerActivity;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->s_instance:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    return-object v0
.end method

.method private handleDownloadingError()V
    .locals 2

    .prologue
    .line 192
    const v0, 0x7f09019d

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 193
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->back()V

    .line 194
    return-void
.end method

.method private handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V
    .locals 5
    .parameter

    .prologue
    .line 409
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceiversList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/Utils;->getSmsAddressListFromContactList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 412
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v0

    .line 414
    const-string v2, "Tango.PictureViewerActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleStartSMSComposeEvent() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-static {v1, v0}, Lcom/sgiggle/production/Utils;->getSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 418
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :goto_0
    return-void

    .line 419
    :catch_0
    move-exception v0

    .line 420
    const-string v0, "Tango.PictureViewerActivity"

    const-string v1, "No activity was found for ACTION_VIEW (for sending SMS)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private hideBar(Z)V
    .locals 2
    .parameter

    .prologue
    .line 340
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->cancelTimerTriggerHidingBar()V

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isBarVisible:Z

    .line 342
    if-eqz p1, :cond_0

    .line 343
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_FadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private postFinishSMSComposeMessage(Z)V
    .locals 3
    .parameter

    .prologue
    .line 385
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;-><init>(Z)V

    .line 387
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 388
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 94
    sput-object p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->s_instance:Lcom/sgiggle/production/screens/picture/PictureViewerActivity;

    .line 95
    return-void
.end method

.method private showBar()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 351
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isBarVisible:Z

    .line 352
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 353
    return-void
.end method

.method private startTimerTriggerHidingBar()V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_timeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_barHider:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 357
    return-void
.end method

.method private toggleBar()V
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isBarVisible:Z

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->hideBar(Z)V

    .line 337
    :goto_0
    return-void

    .line 335
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->showBar()V

    goto :goto_0
.end method

.method private updateImageAndStatusFromMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V
    .locals 4
    .parameter

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getMessageMediaPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v1

    .line 177
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v2

    .line 179
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne v2, v3, :cond_0

    .line 180
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->handleDownloadingError()V

    .line 189
    :goto_0
    return-void

    .line 182
    :cond_0
    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne v2, v3, :cond_2

    .line 184
    :cond_1
    invoke-direct {p0, v0, v2, v1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->updateUiWithImageAndStatus(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;I)V

    .line 187
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v0, v1, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isForwardEnabled:Z

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateUiWithImageAndStatus(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;I)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 202
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->changeImage(Ljava/lang/String;)V

    .line 204
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_btnDoMoreOperations:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 205
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_progressBar:Landroid/widget/ProgressBar;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    if-ne p2, v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 208
    return-void

    :cond_0
    move v1, v2

    .line 204
    goto :goto_0

    .line 205
    :cond_1
    const/4 v1, 0x4

    goto :goto_1
.end method


# virtual methods
.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 151
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 170
    sget-boolean v0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 153
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayConversationMessageEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    .line 154
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->updateImageAndStatusFromMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 157
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;

    .line 158
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V

    goto :goto_0

    .line 162
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConversationMessageSendStatusEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    .line 164
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    if-ne v1, v2, :cond_0

    .line 165
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getMessageMediaPath(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->changeImage(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    nop

    :sswitch_data_0
    .sparse-switch
        0x89c7 -> :sswitch_0
        0x89cc -> :sswitch_2
        0x89d1 -> :sswitch_1
    .end sparse-switch
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 392
    if-ne p1, v2, :cond_0

    .line 393
    const-string v0, "Tango.PictureViewerActivity"

    const-string v1, "onActivityResult() resultCode = REQUEST_CODE_SMS_OLD_CLIENT"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    invoke-direct {p0, v2}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->postFinishSMSComposeMessage(Z)V

    .line 400
    :cond_0
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 286
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->startTimerTriggerHidingBar()V

    .line 287
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 295
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 289
    :pswitch_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->onForwardClicked()V

    move v0, v1

    .line 290
    goto :goto_0

    .line 292
    :pswitch_1
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->onSaveToPhoneClicked()V

    move v0, v1

    .line 293
    goto :goto_0

    .line 287
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->setContentView(I)V

    .line 107
    const v0, 0x7f0a00e1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/picture/ImageViewControl;

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    .line 108
    const v0, 0x7f0a00e2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_actionBar:Landroid/view/View;

    .line 109
    const v0, 0x7f040003

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_FadeOutAnimation:Landroid/view/animation/Animation;

    .line 110
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_FadeOutAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity$2;-><init>(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 127
    const v0, 0x7f0a00e5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_btnDoMoreOperations:Landroid/widget/Button;

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_btnDoMoreOperations:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 130
    const v0, 0x7f0a0059

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_progressBar:Landroid/widget/ProgressBar;

    .line 132
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureEvent;

    .line 134
    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewPictureEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    .line 135
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    .line 136
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_conversationId:Ljava/lang/String;

    .line 137
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_messageId:I

    .line 138
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->updateImageAndStatusFromMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 139
    invoke-static {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->setRunningInstance(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V

    .line 141
    :cond_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->sendMessageAck()V

    .line 142
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 273
    invoke-super {p0, p1, p2, p3}, Lcom/sgiggle/production/ActivityBase;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 274
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_isForwardEnabled:Z

    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090199

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v3, v3, v3, v0}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 277
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 279
    const v0, 0x7f090198

    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(I)Landroid/view/ContextMenu;

    .line 281
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->cancelTimerTriggerHidingBar()V

    .line 282
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 316
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 317
    invoke-static {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->clearRunningInstance(Lcom/sgiggle/production/screens/picture/PictureViewerActivity;)V

    .line 318
    return-void
.end method

.method public onForwardClicked()V
    .locals 3

    .prologue
    .line 234
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_conversationId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    iget v1, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_messageId:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090163

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    .line 242
    new-instance v1, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;

    invoke-direct {v1, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 244
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v2, "jingle"

    invoke-virtual {v0, v2, v1}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 245
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 365
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 366
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->back()V

    .line 367
    const/4 v0, 0x1

    .line 370
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onMoreOperationsClicked(Landroid/view/View;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->registerForContextMenu(Landroid/view/View;)V

    .line 229
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->openContextMenu(Landroid/view/View;)V

    .line 230
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->unregisterForContextMenu(Landroid/view/View;)V

    .line 231
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .parameter

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onNewIntent(Landroid/content/Intent;)V

    .line 147
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->sendMessageAck()V

    .line 148
    return-void
.end method

.method public onSaveToPhoneClicked()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 249
    .line 251
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->copyFile()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 254
    :goto_0
    if-eqz v0, :cond_0

    const v0, 0x7f09019b

    :goto_1
    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 257
    return-void

    .line 252
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 254
    :cond_0
    const v0, 0x7f09019c

    goto :goto_1
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 301
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 302
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->loadImageToMemory()V

    .line 303
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->showBar()V

    .line 304
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStop()V

    .line 310
    iget-object v0, p0, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->m_imageView:Lcom/sgiggle/production/screens/picture/ImageViewControl;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/picture/ImageViewControl;->unloadImageFromMemory()V

    .line 311
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->cancelTimerTriggerHidingBar()V

    .line 312
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter

    .prologue
    .line 323
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/sgiggle/production/screens/picture/PictureViewerActivity;->toggleBar()V

    .line 327
    :cond_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
