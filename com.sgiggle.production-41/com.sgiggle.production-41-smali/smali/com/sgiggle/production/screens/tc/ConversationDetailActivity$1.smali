.class Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;
.super Landroid/os/Handler;
.source "ConversationDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 135
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 137
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 138
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #calls: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->showComposeFragment(ZI)V
    invoke-static {v1, v2, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$000(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;ZI)V

    goto :goto_0

    .line 142
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #setter for: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isComposeEnabled:Z
    invoke-static {v0, v2}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$102(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;Z)Z

    .line 145
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 147
    const/4 v1, 0x0

    .line 148
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #calls: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;
    invoke-static {v2}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$200(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v2

    .line 149
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_2

    .line 150
    :cond_1
    const-string v2, "Tango.ConversationDetailActivity"

    const-string v3, "Handler: dumping MSG_HANDLE_COMPOSE_FRAGMENT_SHOWN, there is no fragment yet, or it was detached from activity."

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :goto_1
    if-nez v1, :cond_0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #getter for: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;
    invoke-static {v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$300(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #getter for: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;
    invoke-static {v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$300(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    #getter for: Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;
    invoke-static {v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->access$300(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->focusText()V

    goto :goto_0

    .line 152
    :cond_2
    invoke-virtual {v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onComposeEnabled()V

    .line 153
    invoke-virtual {v2}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isTipLayerShown()Z

    move-result v1

    goto :goto_1

    .line 135
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
