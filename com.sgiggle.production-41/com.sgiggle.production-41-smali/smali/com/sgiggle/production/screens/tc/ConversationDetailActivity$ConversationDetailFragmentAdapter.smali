.class public Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "ConversationDetailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConversationDetailFragmentAdapter"
.end annotation


# instance fields
.field private m_conversationIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m_conversationPayloads:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 392
    iput-object p1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->this$0:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    .line 393
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    .line 397
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationPayloads:Ljava/util/HashMap;

    .line 398
    return-void
.end method


# virtual methods
.method public getConversationPayload(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 3
    .parameter

    .prologue
    .line 456
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getConversationPayload: conversationId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationPayloads:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .parameter

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 408
    const-string v1, "Tango.ConversationDetailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConversationDetailFragmentAdapter: position="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " conversationId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->newInstance(Ljava/lang/String;)Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 411
    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .parameter

    .prologue
    .line 416
    const/4 v0, -0x2

    return v0
.end method

.method public onNewConversationPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V
    .locals 5
    .parameter

    .prologue
    .line 424
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    .line 425
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    .line 427
    const-string v2, "Tango.ConversationDetailActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onNewConversationPayload: conversationId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isConversationIdKnown="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationPayloads:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 433
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 434
    if-eqz v1, :cond_0

    .line 435
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationPayloads:Ljava/util/HashMap;

    invoke-virtual {v2, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    if-nez v1, :cond_1

    .line 444
    const-string v1, "Tango.ConversationDetailActivity"

    const-string v2, "onNewConversationPayload: refreshing data"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->m_conversationIds:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->notifyDataSetChanged()V

    .line 448
    :cond_1
    return-void
.end method
