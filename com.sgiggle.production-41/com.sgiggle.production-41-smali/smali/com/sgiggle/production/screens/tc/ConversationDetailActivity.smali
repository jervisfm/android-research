.class public Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;
.super Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;
.source "ConversationDetailActivity.java"

# interfaces
.implements Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment$ComposeConversationMessageFragmentListener;
.implements Lcom/sgiggle/production/fragment/ConversationDetailFragment$ConversationDetailFragmentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;
    }
.end annotation


# static fields
.field private static final FOCUS_COMPOSE_FRAGMENT_TEXT_DELAY_MS:I = 0x64

.field private static final MSG_HANDLE_COMPOSE_FRAGMENT_SHOWN:I = 0x1

.field private static final MSG_SHOW_COMPOSE_FRAGMENT:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.ConversationDetailActivity"

.field private static s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;


# instance fields
.field private m_cafeAnimationInProgress:Z

.field private m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

.field private m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

.field private m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

.field private m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

.field private m_handler:Landroid/os/Handler;

.field private m_isCafePaused:Z

.field private m_isComposeEnabled:Z

.field private m_isConversationClosed:Z

.field private m_isForegroundActivity:Z

.field private m_menuVideoCall:Landroid/view/MenuItem;

.field private final m_supportsSwipingBetweenConversations:Z

.field private m_viewPager:Lcom/sgiggle/production/widget/BetterViewPager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;-><init>()V

    .line 111
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    .line 114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isCafePaused:Z

    .line 117
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isComposeEnabled:Z

    .line 120
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isConversationClosed:Z

    .line 123
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_supportsSwipingBetweenConversations:Z

    .line 128
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isForegroundActivity:Z

    .line 131
    new-instance v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$1;-><init>(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    .line 385
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;ZI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->showComposeFragment(ZI)V

    return-void
.end method

.method static synthetic access$102(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isComposeEnabled:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ConversationDetailFragment;
    .locals 1
    .parameter

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->onMediaSelected(I)V

    return-void
.end method

.method public static clearRunningInstance()V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    .line 83
    return-void
.end method

.method private static clearRunningInstance(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 90
    sget-object v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    if-ne v0, p0, :cond_0

    .line 91
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    .line 93
    :cond_0
    return-void
.end method

.method private closeConversation(Z)V
    .locals 3
    .parameter

    .prologue
    .line 359
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isConversationClosed:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 360
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeConversation: force="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Conversation already closed, ignoring."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :goto_0
    return-void

    .line 364
    :cond_0
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "closeConversation: force="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->sendCloseConversationMessage()V

    .line 366
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isConversationClosed:Z

    goto :goto_0
.end method

.method private getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;
    .locals 2

    .prologue
    .line 466
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0081

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    return-object v0
.end method

.method public static getRunningInstance()Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    return-object v0
.end method

.method private onMediaSelected(I)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 672
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 673
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-void

    .line 677
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 702
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onMediaSelected: unhandled type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 679
    :pswitch_0
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v2, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    goto :goto_0

    .line 684
    :pswitch_1
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v2, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_PICK:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    goto :goto_0

    .line 689
    :pswitch_2
    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v2, Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;->ACTION_NEW:Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    goto :goto_0

    .line 694
    :pswitch_3
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->onVideoCallClicked()V

    goto :goto_0

    .line 698
    :pswitch_4
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->onAudioCallClicked()V

    goto :goto_0

    .line 677
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private onPauseVGood()V
    .locals 1

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->stopVGoodAnimation()V

    .line 598
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onPause()V

    .line 599
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Pause()V

    .line 600
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isCafePaused:Z

    .line 601
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->FreeEngine()V

    .line 602
    return-void
.end method

.method private onResumeVGood()V
    .locals 1

    .prologue
    .line 589
    invoke-static {p0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->InitEngine(Landroid/content/Context;)V

    .line 590
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->SetCallbacks()V

    .line 591
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0}, Lcom/sgiggle/cafe/vgood/CafeView;->onResume()V

    .line 592
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Resume()V

    .line 593
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isCafePaused:Z

    .line 594
    return-void
.end method

.method private sendCloseConversationMessage()V
    .locals 3

    .prologue
    .line 375
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "sendCloseConversationMessage"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$CloseConversationMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CloseConversationMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 379
    return-void
.end method

.method private static setRunningInstance(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    sput-object p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    .line 87
    return-void
.end method

.method private showComposeFragment(Z)V
    .locals 1
    .parameter

    .prologue
    .line 551
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->showComposeFragment(ZI)V

    .line 552
    return-void
.end method

.method private showComposeFragment(ZI)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const v2, 0x7f040009

    const/4 v4, 0x1

    .line 555
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "showComposeFragment: won\'t show fragment, activity is finishing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 581
    :goto_0
    return-void

    .line 560
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 561
    const v1, 0x7f04000d

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 562
    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 564
    if-eqz p1, :cond_1

    .line 566
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 568
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 569
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    .line 570
    iget-object v3, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x64

    add-long/2addr v4, v6

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 580
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0

    .line 574
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_1
.end method

.method private showMediaSelectorDialog()V
    .locals 3

    .prologue
    .line 646
    const/4 v0, 0x0

    .line 647
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v1

    .line 648
    if-eqz v1, :cond_0

    .line 649
    invoke-virtual {v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->isCallEnabled()Z

    move-result v0

    .line 652
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 653
    const v2, 0x7f09018d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 655
    new-instance v2, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;

    invoke-direct {v2, p0, v0}, Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;-><init>(Landroid/content/Context;Z)V

    .line 657
    new-instance v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$2;

    invoke-direct {v0, p0, v2}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$2;-><init>(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;Lcom/sgiggle/production/adapter/TcMediaSelectorAdapter;)V

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 664
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 665
    return-void
.end method


# virtual methods
.method public varargs createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 472
    if-nez v0, :cond_0

    .line 473
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "createNewMessage asked, but no conversation is selected. Dumping message..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :goto_0
    return-void

    .line 475
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->createNewMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Lcom/sgiggle/production/controller/ConversationMessageController$CreateMessageAction;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getActionBarHomeIconResId()I
    .locals 1

    .prologue
    .line 585
    const v0, 0x7f0200b6

    return v0
.end method

.method public getConversationPayload(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 3
    .parameter

    .prologue
    .line 638
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getConversationPayload for conversation id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->getConversationPayload(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 6
    .parameter

    .prologue
    .line 169
    const/4 v0, 0x0

    .line 170
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v1

    .line 171
    if-eqz v1, :cond_0

    .line 172
    invoke-virtual {v1, p1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->handleMessage(Lcom/sgiggle/messaging/Message;)Z

    move-result v0

    .line 174
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    invoke-virtual {v2, p1}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 176
    if-nez v0, :cond_2

    .line 177
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 217
    const-string v0, "Tango.ConversationDetailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not handled by fragment or activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->sendMessageAck()V

    .line 231
    :goto_1
    return-void

    .line 180
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    .line 181
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

    invoke-virtual {v2, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;->onNewConversationPayload(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)V

    .line 184
    if-eqz v1, :cond_1

    .line 185
    invoke-virtual {v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onConversationPayloadChanged()V

    goto :goto_0

    .line 190
    :sswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->updateConnectionStatus()V

    goto :goto_0

    .line 196
    :sswitch_2
    const v0, 0x7f0900bd

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 200
    :sswitch_3
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->stopVGoodAnimation()V

    goto :goto_0

    .line 204
    :sswitch_4
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    if-nez v0, :cond_1

    .line 205
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowAdvertisementEvent;

    .line 206
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowAdvertisementEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShowAdvertisementPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShowAdvertisementPayload;->getId()J

    move-result-wide v2

    .line 207
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ShowAdvertisementEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ShowAdvertisementPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ShowAdvertisementPayload;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 208
    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->startVGoodAnimation(Ljava/lang/String;JJ)Z

    move-result v0

    .line 209
    if-eqz v0, :cond_1

    .line 210
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementShownMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$AdvertisementShownMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0

    .line 229
    :cond_2
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->sendMessageAck()V

    goto :goto_1

    .line 177
    :sswitch_data_0
    .sparse-switch
        0x899d -> :sswitch_3
        0x89c6 -> :sswitch_0
        0x89ce -> :sswitch_2
        0x89d4 -> :sswitch_1
        0x89d8 -> :sswitch_4
    .end sparse-switch
.end method

.method public hideIme()V
    .locals 1

    .prologue
    .line 713
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->hideIme()V

    .line 714
    return-void
.end method

.method public hideMenuItems()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_menuVideoCall:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    move v0, v1

    .line 546
    :goto_0
    return v0

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_menuVideoCall:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 546
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isComposeEnabled()Z
    .locals 1

    .prologue
    .line 526
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isComposeEnabled:Z

    return v0
.end method

.method public isForegroundActivity()Z
    .locals 1

    .prologue
    .line 717
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isForegroundActivity:Z

    return v0
.end method

.method public onAddMediaClicked()V
    .locals 0

    .prologue
    .line 501
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->showMediaSelectorDialog()V

    .line 502
    return-void
.end method

.method public onAudioCallClicked()V
    .locals 2

    .prologue
    .line 491
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 492
    if-nez v0, :cond_0

    .line 493
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "onVideoCallClicked clicked but no conversation is selected. Dumping message..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    :goto_0
    return-void

    .line 495
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onAudioCallClicked()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->closeConversation(Z)V

    .line 372
    return-void
.end method

.method public onComposeActionStarted()V
    .locals 2

    .prologue
    .line 507
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 508
    if-nez v0, :cond_0

    .line 509
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "Compose clicked, but no conversation is selected. Dumping message..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :goto_0
    return-void

    .line 512
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->hideTipLayer(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 235
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 240
    invoke-static {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->setRunningInstance(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V

    .line 243
    const v0, 0x7f03001a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->setContentView(I)V

    .line 247
    new-instance v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;-><init>(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;Landroid/support/v4/app/FragmentManager;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

    .line 248
    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/BetterViewPager;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_viewPager:Lcom/sgiggle/production/widget/BetterViewPager;

    .line 249
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_viewPager:Lcom/sgiggle/production/widget/BetterViewPager;

    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity$ConversationDetailFragmentAdapter;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_viewPager:Lcom/sgiggle/production/widget/BetterViewPager;

    const v1, 0x7f0a0089

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterViewPager;->setScrollableChildResId(I)V

    .line 253
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0080

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    .line 255
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->showComposeFragment(Z)V

    .line 257
    const v0, 0x7f0a0026

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/cafe/vgood/CafeView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    .line 258
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeView;->setZOrderOnTop(Z)V

    .line 260
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a007f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    .line 263
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 264
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->handleMessage(Lcom/sgiggle/messaging/Message;)V

    .line 266
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .parameter

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 298
    const/high16 v1, 0x7f0e

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 300
    const v0, 0x7f0a01a8

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_menuVideoCall:Landroid/view/MenuItem;

    .line 302
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 345
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onDestroy()V

    .line 346
    invoke-static {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->clearRunningInstance(Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;)V

    .line 349
    sget-object v0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->s_instance:Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;

    if-nez v0, :cond_0

    .line 350
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->closeConversation(Z)V

    .line 352
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 309
    const/16 v0, 0x52

    if-ne p1, v0, :cond_0

    .line 310
    const/4 v0, 0x1

    .line 312
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLoadFinished(I)V
    .locals 4
    .parameter

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 521
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->refreshActionBar()V

    .line 522
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 317
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 323
    :goto_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 319
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->closeConversation(Z)V

    goto :goto_0

    .line 317
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 328
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPause()V

    .line 331
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->stop()V

    .line 334
    invoke-static {}, Lcom/sgiggle/production/util/HttpImageLoader;->getInstance()Lcom/sgiggle/production/util/HttpImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/HttpImageLoader;->stop()V

    .line 337
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/FileImageLoader;->stop()V

    .line 339
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->onPauseVGood()V

    .line 340
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isForegroundActivity:Z

    .line 341
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 270
    invoke-super {p0, p1}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 273
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getActionBarHelper()Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/actionbarcompat/ActionBarHelper;->setDisplayHomeAsUpEnabled(Z)V

    .line 274
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isConversationClosed:Z

    .line 280
    invoke-super {p0}, Lcom/sgiggle/production/actionbarcompat/ActionBarActivity;->onResume()V

    .line 283
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->start()V

    .line 286
    invoke-static {}, Lcom/sgiggle/production/util/HttpImageLoader;->getInstance()Lcom/sgiggle/production/util/HttpImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/HttpImageLoader;->start()V

    .line 289
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/FileImageLoader;->start()V

    .line 291
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->onResumeVGood()V

    .line 292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isForegroundActivity:Z

    .line 293
    return-void
.end method

.method public onTipClicked()V
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_composeFragment:Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;

    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ComposeConversationMessageFragment;->focusText()V

    .line 710
    return-void
.end method

.method public onVideoCallClicked()V
    .locals 2

    .prologue
    .line 481
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getCurrentConversationFragment()Lcom/sgiggle/production/fragment/ConversationDetailFragment;

    move-result-object v0

    .line 482
    if-nez v0, :cond_0

    .line 483
    const-string v0, "Tango.ConversationDetailActivity"

    const-string v1, "onVideoCallClicked clicked but no conversation is selected. Dumping message..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    :goto_0
    return-void

    .line 485
    :cond_0
    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationDetailFragment;->onVideoCallClicked()V

    goto :goto_0
.end method

.method public startVGoodAnimation(Ljava/lang/String;JJ)Z
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 605
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_isCafePaused:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 624
    :goto_0
    return v0

    .line 608
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 609
    const v0, 0x7f09015c

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    .line 610
    goto :goto_0

    .line 613
    :cond_2
    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 614
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    .line 615
    if-eqz v0, :cond_3

    move v0, v1

    .line 617
    goto :goto_0

    .line 620
    :cond_3
    iput-boolean v5, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    .line 621
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeView;->setVisibility(I)V

    .line 622
    const-string v0, "Surprise.Cafe"

    invoke-static {p1, v0}, Lcom/sgiggle/cafe/vgood/CafeMgr;->LoadSurprise(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, p1

    move-wide v1, p4

    move-wide v3, p2

    .line 623
    invoke-static/range {v0 .. v5}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StartSurprise(Ljava/lang/String;JJZ)I

    move v0, v5

    .line 624
    goto :goto_0
.end method

.method public stopVGoodAnimation()V
    .locals 2

    .prologue
    .line 628
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    if-eqz v0, :cond_0

    .line 629
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeAnimationInProgress:Z

    .line 630
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_cafeView:Lcom/sgiggle/cafe/vgood/CafeView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sgiggle/cafe/vgood/CafeView;->setVisibility(I)V

    .line 631
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->StopAllSurprises()V

    .line 632
    invoke-static {}, Lcom/sgiggle/cafe/vgood/CafeMgr;->Reset()V

    .line 634
    :cond_0
    return-void
.end method

.method public updateMenuItemsVisibility(Z)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 532
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_menuVideoCall:Landroid/view/MenuItem;

    if-nez v0, :cond_0

    move v0, v1

    .line 536
    :goto_0
    return v0

    .line 535
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationDetailActivity;->m_menuVideoCall:Landroid/view/MenuItem;

    if-nez p1, :cond_1

    move v1, v2

    :cond_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    move v0, v2

    .line 536
    goto :goto_0
.end method
