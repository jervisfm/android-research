.class Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ConversationsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/tc/ConversationsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConversationFragmentAdapter"
.end annotation


# instance fields
.field private m_numPages:I

.field final synthetic this$0:Lcom/sgiggle/production/screens/tc/ConversationsActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/screens/tc/ConversationsActivity;Landroid/support/v4/app/FragmentManager;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;->this$0:Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    .line 176
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 177
    iput p3, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;->m_numPages:I

    .line 178
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;->m_numPages:I

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3
    .parameter

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;->this$0:Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    #getter for: Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I
    invoke-static {v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->access$000(Lcom/sgiggle/production/screens/tc/ConversationsActivity;)[I

    move-result-object v0

    aget v0, v0, p1

    .line 191
    packed-switch v0, :pswitch_data_0

    .line 203
    const-string v0, "Tango.ConversationsActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConversationFragmentAdapter.getItem(): invalid position #"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    .line 194
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;->this$0:Lcom/sgiggle/production/screens/tc/ConversationsActivity;

    #calls: Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getConversationSummaryListPayload()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->access$100(Lcom/sgiggle/production/screens/tc/ConversationsActivity;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->newInstance(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/production/fragment/ConversationListFragment;

    move-result-object v0

    goto :goto_0

    .line 199
    :pswitch_1
    new-instance v0, Lcom/sgiggle/production/fragment/VideomailListFragment;

    invoke-direct {v0}, Lcom/sgiggle/production/fragment/VideomailListFragment;-><init>()V

    goto :goto_0

    .line 191
    :pswitch_data_0
    .packed-switch 0x7f0a009b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
