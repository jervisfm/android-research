.class public Lcom/sgiggle/production/screens/tc/ConversationsActivity;
.super Lcom/sgiggle/production/FragmentActivityBase;
.source "ConversationsActivity.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/sgiggle/production/fragment/ConversationListActionsFragment$ConversationListActionsFragmentListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ConversationsActivity"


# instance fields
.field private final USE_CONVERSATION_TOGGLE:Z

.field private m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

.field private m_conversationFilterGroup:Landroid/widget/RadioGroup;

.field private final m_conversationFilterIds:[I

.field private m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

.field private m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;

.field private m_nbConversationFilters:I

.field private m_viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sgiggle/production/FragmentActivityBase;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->USE_CONVERSATION_TOGGLE:Z

    .line 172
    return-void

    .line 44
    :array_0
    .array-data 0x4
        0x9bt 0x0t 0xat 0x7ft
        0x9ct 0x0t 0xat 0x7ft
    .end array-data
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/tc/ConversationsActivity;)[I
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/tc/ConversationsActivity;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getConversationSummaryListPayload()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method private getConversationSummaryListPayload()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    .line 222
    :goto_0
    return-object v0

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    instance-of v0, v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;

    if-eqz v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_firstMessage:Lcom/sgiggle/messaging/Message;

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    goto :goto_0

    .line 222
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getCurrentFragment()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a0081

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private getPagePositionFromFilterId(I)I
    .locals 3
    .parameter

    .prologue
    .line 232
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    array-length v0, v0

    .line 233
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 234
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    aget v2, v2, v1

    if-ne v2, p1, :cond_0

    move v0, v1

    .line 240
    :goto_1
    return v0

    .line 233
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 240
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private sendSelectContactRequestMessage()V
    .locals 4

    .prologue
    .line 289
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;

    sget-object v3, Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;->SELECT_TO_START_CONVERSATION:Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$SelectContactRequestMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SelectContactType;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 292
    return-void
.end method


# virtual methods
.method protected finishIfResumedAfterKilled()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 117
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    .line 145
    instance-of v3, v2, Lcom/sgiggle/production/fragment/ConversationListFragment;

    if-eqz v3, :cond_0

    .line 146
    check-cast v2, Lcom/sgiggle/production/fragment/ConversationListFragment;

    .line 148
    invoke-virtual {v2, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleMessage(Lcom/sgiggle/messaging/Message;)Z

    move-result v1

    .line 155
    :cond_0
    :goto_0
    if-nez v1, :cond_1

    .line 156
    const-string v1, "Tango.ConversationsActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleMessage: message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " was not handled by fragment"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_1
    return-void

    .line 120
    :sswitch_0
    move-object v0, p1

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;

    move-object v1, v0

    invoke-virtual {v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$OpenConversationListEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    iput-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationSummaryListPayload:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    .line 122
    instance-of v1, v2, Lcom/sgiggle/production/fragment/ConversationListFragment;

    if-eqz v1, :cond_2

    .line 123
    move-object v0, v2

    check-cast v0, Lcom/sgiggle/production/fragment/ConversationListFragment;

    move-object v1, v0

    .line 125
    invoke-virtual {v1, p1}, Lcom/sgiggle/production/fragment/ConversationListFragment;->handleMessage(Lcom/sgiggle/messaging/Message;)Z

    .line 131
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    const v2, 0x7f0a009b

    invoke-direct {p0, v2}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getPagePositionFromFilterId(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 134
    const/4 v1, 0x1

    .line 135
    goto :goto_0

    .line 138
    :sswitch_1
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    if-eqz v2, :cond_0

    .line 139
    iget-object v2, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    invoke-virtual {v2}, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;->updateConnectionStatus()V

    goto :goto_0

    .line 117
    :sswitch_data_0
    .sparse-switch
        0x89c8 -> :sswitch_0
        0x89d4 -> :sswitch_1
    .end sparse-switch
.end method

.method public onAddTextClicked()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->sendSelectContactRequestMessage()V

    .line 283
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 245
    invoke-virtual {p1, p2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 246
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_nbConversationFilters:I

    if-ge v0, v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    aget v1, v1, v0

    if-ne v1, p2, :cond_2

    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_2

    .line 255
    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 252
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 57
    const-string v0, "Tango.ConversationsActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-super {p0, p1}, Lcom/sgiggle/production/FragmentActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 62
    const v0, 0x7f030021

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->setContentView(I)V

    .line 64
    const v0, 0x7f0a009a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterGroup:Landroid/widget/RadioGroup;

    .line 72
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterGroup:Landroid/widget/RadioGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    .line 75
    const/4 v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_nbConversationFilters:I

    .line 78
    new-instance v0, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_nbConversationFilters:I

    invoke-direct {v0, p0, v1, v2}, Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;-><init>(Lcom/sgiggle/production/screens/tc/ConversationsActivity;Landroid/support/v4/app/FragmentManager;I)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;

    .line 80
    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_fragmentAdapter:Lcom/sgiggle/production/screens/tc/ConversationsActivity$ConversationFragmentAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 84
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a007f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    iput-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_connectionStatusFragment:Lcom/sgiggle/production/fragment/ConnectionStatusFragment;

    .line 86
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .parameter

    .prologue
    .line 264
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 269
    return-void
.end method

.method public onPageSelected(I)V
    .locals 2
    .parameter

    .prologue
    .line 274
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterGroup:Landroid/widget/RadioGroup;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterGroup:Landroid/widget/RadioGroup;

    iget-object v1, p0, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->m_conversationFilterIds:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 277
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lcom/sgiggle/production/FragmentActivityBase;->onPause()V

    .line 107
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->stop()V

    .line 108
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Lcom/sgiggle/production/FragmentActivityBase;->onResume()V

    .line 99
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->start()V

    .line 100
    return-void
.end method

.method public scrollToRelevantItem()V
    .locals 2

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/sgiggle/production/screens/tc/ConversationsActivity;->getCurrentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 299
    instance-of v1, v0, Lcom/sgiggle/production/fragment/ConversationListFragment;

    if-eqz v1, :cond_0

    .line 300
    check-cast v0, Lcom/sgiggle/production/fragment/ConversationListFragment;

    .line 301
    invoke-virtual {v0}, Lcom/sgiggle/production/fragment/ConversationListFragment;->scrollToRelevantItem()V

    .line 303
    :cond_0
    return-void
.end method
