.class public Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;
.super Ljava/lang/Object;
.source "CamcorderProfileWrapper.java"


# instance fields
.field public audioBitRate:I

.field public audioChannels:I

.field public audioCodec:I

.field public audioSampleRate:I

.field public duration:I

.field public fileFormat:I

.field public quality:I

.field public videoBitRate:I

.field public videoCodec:I

.field public videoFrameHeight:I

.field public videoFrameRate:I

.field public videoFrameWidth:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setFromCamcorderProfile(Landroid/media/CamcorderProfile;)V
    .locals 1
    .parameter

    .prologue
    .line 21
    iget v0, p1, Landroid/media/CamcorderProfile;->audioBitRate:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    .line 22
    iget v0, p1, Landroid/media/CamcorderProfile;->audioChannels:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioChannels:I

    .line 23
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    .line 24
    iget v0, p1, Landroid/media/CamcorderProfile;->audioSampleRate:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioSampleRate:I

    .line 25
    iget v0, p1, Landroid/media/CamcorderProfile;->duration:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->duration:I

    .line 26
    iget v0, p1, Landroid/media/CamcorderProfile;->fileFormat:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    .line 27
    iget v0, p1, Landroid/media/CamcorderProfile;->quality:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->quality:I

    .line 28
    iget v0, p1, Landroid/media/CamcorderProfile;->videoBitRate:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 29
    iget v0, p1, Landroid/media/CamcorderProfile;->videoCodec:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    .line 30
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameRate:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 31
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 32
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 33
    return-void
.end method
