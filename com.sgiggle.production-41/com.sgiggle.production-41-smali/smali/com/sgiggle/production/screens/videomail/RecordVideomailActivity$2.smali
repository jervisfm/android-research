.class Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;
.super Ljava/lang/Object;
.source "RecordVideomailActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->restartPreview()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 624
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 627
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->stopPreview()V

    .line 628
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->startPreview()V

    .line 629
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 634
    :goto_0
    return-void

    .line 630
    :catch_0
    move-exception v0

    .line 631
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restartPreview(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
