.class Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;
.super Ljava/lang/Object;
.source "RecordVideomailActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraSizeComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/hardware/Camera$Size;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 1622
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1622
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    return-void
.end method


# virtual methods
.method public compare(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1625
    iget v0, p1, Landroid/hardware/Camera$Size;->width:I

    iget v1, p1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v0, v1

    iget v1, p2, Landroid/hardware/Camera$Size;->width:I

    iget v2, p2, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1622
    check-cast p1, Landroid/hardware/Camera$Size;

    check-cast p2, Landroid/hardware/Camera$Size;

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;->compare(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)I

    move-result v0

    return v0
.end method
