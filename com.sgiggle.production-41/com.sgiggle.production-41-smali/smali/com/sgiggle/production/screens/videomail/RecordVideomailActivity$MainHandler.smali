.class Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;
.super Landroid/os/Handler;
.source "RecordVideomailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 202
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 202
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 205
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 244
    :pswitch_0
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :goto_0
    return-void

    .line 207
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 211
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->prepareCamera()Z
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Z

    goto :goto_0

    .line 215
    :pswitch_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v0

    if-le v0, v3, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/production/widget/RotateButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    goto :goto_0

    .line 222
    :pswitch_4
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->keepScreenOnAwhile()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    goto :goto_0

    .line 226
    :pswitch_5
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_progressView:Landroid/view/View;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$500(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/SurfaceView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 228
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 229
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v0

    if-le v0, v3, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/production/widget/RotateButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopPrepareCameraThread()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    goto :goto_0

    .line 236
    :pswitch_6
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setRecButtonLightOn(Z)V
    invoke-static {v0, v3}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Z)V

    goto :goto_0

    .line 240
    :pswitch_7
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setRecButtonLightOn(Z)V
    invoke-static {v0, v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Z)V

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_5
    .end packed-switch
.end method
