.class Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;
.super Ljava/lang/Thread;
.source "RecordVideomailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PrepareCameraThread"
.end annotation


# instance fields
.field private m_stopPreviewThread:Z

.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 1890
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    .line 1891
    const-string v0, "PrepareCamera"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 1892
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->m_stopPreviewThread:Z

    .line 1893
    return-void
.end method


# virtual methods
.method public requestExitAndWait()V
    .locals 4

    .prologue
    .line 1933
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->m_stopPreviewThread:Z

    .line 1935
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1939
    :goto_0
    return-void

    .line 1936
    :catch_0
    move-exception v0

    .line 1937
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestExitAndWait() failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public run()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1897
    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->m_stopPreviewThread:Z

    if-nez v0, :cond_2

    .line 1898
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I
    invoke-static {v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v2

    aget v1, v1, v2

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->openCamera(I)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1900(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)V

    .line 1903
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->m_stopPreviewThread:Z

    if-nez v0, :cond_3

    .line 1904
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getIdealResolution()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 1905
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    new-instance v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread$1;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;)V

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1911
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/SurfaceHolder;

    move-result-object v1

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Landroid/view/SurfaceHolder;)V

    .line 1912
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_1

    .line 1913
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->calcCameraDisplayOrientation()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 1914
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2500()Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 1915
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setDisplayOrientation(I)V

    .line 1918
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setCameraParameters()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 1920
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->startPreview()V

    .line 1921
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1922
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Camera opened and preview started successfully"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1930
    :goto_1
    return-void

    .line 1900
    :cond_2
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "PrepareCameraThread is requested to stop running"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1926
    :catch_0
    move-exception v0

    .line 1927
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->closeCamera()V
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$2800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 1928
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "PrepareCameraThrea.run() failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1924
    :cond_3
    :try_start_1
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "PrepareCameraThread is requested to stop running"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
