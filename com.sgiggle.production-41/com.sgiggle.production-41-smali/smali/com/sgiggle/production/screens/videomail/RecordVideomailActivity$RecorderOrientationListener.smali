.class Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;
.super Landroid/view/OrientationEventListener;
.source "RecordVideomailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecorderOrientationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1582
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    .line 1583
    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    .line 1584
    return-void
.end method


# virtual methods
.method public enable()V
    .locals 1

    .prologue
    .line 1608
    invoke-super {p0}, Landroid/view/OrientationEventListener;->enable()V

    .line 1612
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->onOrientationChanged(I)V

    .line 1613
    return-void
.end method

.method public onOrientationChanged(I)V
    .locals 2
    .parameter

    .prologue
    .line 1588
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1604
    :cond_0
    :goto_0
    return-void

    .line 1592
    :cond_1
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 1596
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    add-int/lit8 v1, p1, 0x2d

    div-int/lit8 v1, v1, 0x5a

    mul-int/lit8 v1, v1, 0x5a

    rem-int/lit16 v1, v1, 0x168

    #setter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I
    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1402(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)I

    .line 1598
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getDisplayRotation()I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1500(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1599
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 1600
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #setter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I
    invoke-static {v1, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1602(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)I

    .line 1602
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/production/widget/RotateButton;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->this$0:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->access$1600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/RotateButton;->setDegree(I)V

    goto :goto_0
.end method
