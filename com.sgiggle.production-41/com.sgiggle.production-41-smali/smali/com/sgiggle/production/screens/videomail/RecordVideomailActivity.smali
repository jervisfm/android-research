.class public Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "RecordVideomailActivity.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;
.implements Landroid/media/MediaRecorder$OnErrorListener;
.implements Landroid/media/MediaRecorder$OnInfoListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/sgiggle/production/widget/Timer$Callback;
.implements Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;,
        Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;,
        Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;,
        Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;
    }
.end annotation


# static fields
#the value of this static final field might be set in the static constructor
.field private static final AUDIO_BIT_RATE:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final AUDIO_CODEC:I = 0x0

.field private static final CAMERA_ID_UNKNOWN:I = -0x1

.field private static final DEFAULT_CAMERA_BRIGHTNESS:F = 0.7f

.field private static final DEFAULT_MAX_RECORDING_DURATION:I = 0x7530

.field private static final FILE_EXTENSION:Ljava/lang/String; = ".mp4"

.field private static final FILE_FORMAT:I = 0x2

.field private static final FRAME_RATE:I = 0x1e

.field public static final KEY_CALLEE_ACCOUNT_ID:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.RecordVideomailNewActivity.KEY_ACCOUNT_ID"

.field private static final LOW_STORAGE_THRESHOLD:J = 0x300000L

.field private static final MIME_TYPE:Ljava/lang/String; = "video/mp4"

.field private static final MSG_CLEAR_SCREEN_DELAY:I = 0x0

.field private static final MSG_HIDE_PROGRESS_VIEW:I = 0x8

.field private static final MSG_KEEP_SCREEN_ON_A_WHILE:I = 0x5

.field private static final MSG_RECONNECT_CAMERA:I = 0x2

.field private static final MSG_SET_PREVIEW_UI:I = 0x4

.field private static final MSG_TURN_REC_BUTTON_LIGHT_OFF:I = 0x7

.field private static final MSG_TURN_REC_BUTTON_LIGHT_ON:I = 0x6

.field private static final RECONNECT_CAMERA_DELAY:I = 0x7d0

.field private static final REC_LIGHT_FIRST_TOGGLE_DELAY:I = 0x3e8

.field private static final REC_LIGHT_ON_DURATION:I = 0x1f4

.field private static final SCREEN_DELAY:I = 0x1d4c0

.field private static SHOW_TIMER_AUTO_DISMISS_ALERT:Z = false

.field private static final TAG:Ljava/lang/String; = "Tango.RecordVideomailActivity"

.field private static final THRESHOLD_RECORD:J = 0x3e8L

.field private static final THRESHOLD_SWITCH_CAMERA:J = 0x3e8L

.field private static final THUMBNAIL_FILE_EXTENSION:Ljava/lang/String; = ".jpeg"

.field private static final TIMER_AUTO_DISMISS_ALERT_DURATION:I = 0x1388

.field private static final TIMER_PERMANENT_ALERT_DURATION:I = 0x1388

#the value of this static final field might be set in the static constructor
.field private static final VIDEO_BIT_RATE:I = 0x0

#the value of this static final field might be set in the static constructor
.field private static final VIDEO_CODEC:I = 0x0

.field private static final VIDEO_FRAME_HEIGHT:I = 0xf0

.field private static final VIDEO_FRAME_WIDTH:I = 0x140

.field private static final isGingerbreadOrHigher:Z

.field private static final isIcsOrHigher:Z

.field private static final isSupportCamcorderProfile:Z


# instance fields
.field private m_alertMessage:Landroid/widget/TextView;

.field private m_alertNotificaiton:Landroid/widget/FrameLayout;

.field private m_alwaysShowTimerAlert:Z

.field private m_calleesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private m_camcorderProfile:Landroid/media/CamcorderProfile;

.field private m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

.field private m_cameraDisplayOrientation:I

.field private m_cameraId:I

.field private m_cameraMap:[I

.field private m_cameraParameters:Landroid/hardware/Camera$Parameters;

.field private m_cancel:Landroid/widget/Button;

.field private m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

.field private m_currentFilename:Ljava/lang/String;

.field private m_currentRequestedOrientation:I

.field private m_currentThumbnailFilename:Ljava/lang/String;

.field private m_fileUploadProgress:Landroid/widget/ProgressBar;

.field private m_fileUploader:Landroid/widget/LinearLayout;

.field private final m_handler:Landroid/os/Handler;

.field private m_hideAutoDismissTimerAlertAt:I

.field private m_isMaxDurationReached:Z

.field private m_isPausing:Z

.field private m_isRecording:Z

.field private m_isReviewInProgress:Z

.field private m_isStopping:Z

.field private m_isVideomailReplaySupported:Z

.field private m_layout_orientation:[I

.field private m_maxRawVideoSize:J

.field private m_maxRecordingDuration:I

.field private m_mediaRecorder:Landroid/media/MediaRecorder;

.field private m_numberOfCameras:I

.field private m_orientation:I

.field private m_orientationCompensation:I

.field private m_orientationListener:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;

.field private m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

.field private m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

.field private m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

.field private m_progressView:Landroid/view/View;

.field private m_recBtnOffDrawable:Landroid/graphics/drawable/Drawable;

.field private m_recBtnOnDrawable:Landroid/graphics/drawable/Drawable;

.field private m_record:Landroid/widget/Button;

.field private m_recordButtonTimer:Ljava/util/Timer;

.field private m_rotationHint:I

.field private m_send:Landroid/widget/Button;

.field private m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

.field private m_showAutoDismissTimerAlertAt:I

.field private m_showPermanentTimerAlertAt:I

.field private m_surfaceHolder:Landroid/view/SurfaceHolder;

.field private m_surfaceView:Landroid/view/SurfaceView;

.field private m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

.field private m_timestampLastCameraSwitch:J

.field private m_timestampLastRecord:J

.field private m_uploadingMessage:Landroid/widget/TextView;

.field private m_videomailSenderLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    move v0, v3

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    move v0, v3

    :goto_1
    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isIcsOrHigher:Z

    .line 87
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-MS910"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v3

    :goto_2
    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isSupportCamcorderProfile:Z

    .line 95
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x7d00

    :goto_3
    sput v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->AUDIO_BIT_RATE:I

    .line 96
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_4

    const v0, 0x7a120

    :goto_4
    sput v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->VIDEO_BIT_RATE:I

    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v5, :cond_5

    const/4 v0, 0x2

    :goto_5
    sput v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->VIDEO_CODEC:I

    .line 99
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_6

    move v0, v4

    :goto_6
    sput v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->AUDIO_CODEC:I

    .line 108
    sput-boolean v2, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->SHOW_TIMER_AUTO_DISMISS_ALERT:Z

    return-void

    :cond_0
    move v0, v2

    .line 85
    goto :goto_0

    :cond_1
    move v0, v2

    .line 86
    goto :goto_1

    :cond_2
    move v0, v2

    .line 87
    goto :goto_2

    .line 95
    :cond_3
    const/16 v0, 0x2fa8

    goto :goto_3

    .line 96
    :cond_4
    const v0, 0x5dc00

    goto :goto_4

    :cond_5
    move v0, v4

    .line 98
    goto :goto_5

    :cond_6
    move v0, v3

    .line 99
    goto :goto_6
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    const/16 v3, 0x7530

    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 125
    iput-wide v5, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastCameraSwitch:J

    .line 127
    iput-wide v5, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastRecord:J

    .line 134
    iput-object v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    .line 149
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isPausing:Z

    .line 150
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 151
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isStopping:Z

    .line 152
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isMaxDurationReached:Z

    .line 154
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I

    .line 155
    iput v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I

    .line 156
    iput v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    .line 158
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    .line 159
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    .line 160
    iput v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    .line 161
    iput v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    .line 166
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    .line 167
    invoke-static {v3}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->calculateMaxRawVideoSize(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRawVideoSize:J

    .line 169
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isVideomailReplaySupported:Z

    .line 173
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_videomailSenderLock:Ljava/lang/Object;

    .line 177
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alwaysShowTimerAlert:Z

    .line 184
    new-instance v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    invoke-direct {v0}, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    .line 190
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isReviewInProgress:Z

    .line 196
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recordButtonTimer:Ljava/util/Timer;

    .line 250
    new-instance v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;

    invoke-direct {v0, p0, v4}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$MainHandler;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    .line 1886
    return-void

    .line 158
    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data

    .line 159
    :array_1
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->prepareCamera()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/VideoCapture/CameraWrapper;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    return v0
.end method

.method static synthetic access$1400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    iput p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getDisplayRotation()I

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I

    return v0
.end method

.method static synthetic access$1602(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    iput p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I

    return p1
.end method

.method static synthetic access$1700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)[I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    return v0
.end method

.method static synthetic access$1900(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sgiggle/production/screens/videomail/CameraHardwareException;
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->openCamera(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Lcom/sgiggle/production/widget/RotateButton;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getIdealResolution()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resizeForPreviewAspectRatio()V

    return-void
.end method

.method static synthetic access$2200(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/SurfaceHolder;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Landroid/view/SurfaceHolder;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->calcCameraDisplayOrientation()V

    return-void
.end method

.method static synthetic access$2500()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    return v0
.end method

.method static synthetic access$2600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 71
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setCameraParameters()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->closeCamera()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->keepScreenOnAwhile()V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_progressView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/view/SurfaceView;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopPrepareCameraThread()V

    return-void
.end method

.method static synthetic access$800(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setRecButtonLightOn(Z)V

    return-void
.end method

.method private calcCameraDisplayOrientation()V
    .locals 3

    .prologue
    .line 714
    new-instance v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    .line 715
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v1, v1, v2

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 718
    iget v0, v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->getSurfaceRotation(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    .line 719
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set display orientation:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    return-void
.end method

.method private static calculateMaxRawVideoSize(I)J
    .locals 6
    .parameter

    .prologue
    .line 1726
    const-wide/32 v0, 0x4f1a00

    int-to-long v2, p0

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private cancelSend()V
    .locals 3

    .prologue
    .line 1784
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1785
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v0, :cond_0

    .line 1786
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->cancel()V

    .line 1789
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 1790
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/TangoApp;->setSendVideomailActivityHelper(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)V

    .line 1791
    monitor-exit v1

    .line 1792
    return-void

    .line 1791
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private cleanupFiles()V
    .locals 1

    .prologue
    .line 1541
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1542
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->deleteVideoFile(Ljava/lang/String;)V

    .line 1543
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    .line 1545
    :cond_0
    return-void
.end method

.method private closeCamera()V
    .locals 2

    .prologue
    .line 688
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "closeCamera"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 689
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_0

    .line 690
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "already stopped."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 697
    :goto_0
    return-void

    .line 694
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->lock()V

    .line 695
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->releaseCamera()V

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    goto :goto_0
.end method

.method private deleteVideoFile(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 1556
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1557
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1558
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File deleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    :goto_0
    return-void

    .line 1560
    :cond_0
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not delete (or already deleted): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static getAvailableStorage()J
    .locals 4

    .prologue
    .line 1501
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1502
    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1504
    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method private getDisplayRotation()I
    .locals 4

    .prologue
    .line 1480
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1482
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1487
    const/4 v0, 0x0

    .line 1490
    :goto_1
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Display Rotation Degree: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    return v0

    .line 1480
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getOrientation()I

    move-result v0

    goto :goto_0

    .line 1483
    :pswitch_0
    const/16 v0, 0x5a

    goto :goto_1

    .line 1484
    :pswitch_1
    const/16 v0, 0xb4

    goto :goto_1

    .line 1485
    :pswitch_2
    const/16 v0, 0x10e

    goto :goto_1

    .line 1482
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getDisplayRotation(Landroid/app/Activity;)I
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 811
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 812
    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 822
    :goto_0
    return v0

    :pswitch_0
    move v0, v1

    .line 814
    goto :goto_0

    .line 816
    :pswitch_1
    const/16 v0, 0x5a

    goto :goto_0

    .line 818
    :pswitch_2
    const/16 v0, 0xb4

    goto :goto_0

    .line 820
    :pswitch_3
    const/16 v0, 0x10e

    goto :goto_0

    .line 812
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getIdealResolution()V
    .locals 6

    .prologue
    .line 838
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setDefaultProfile()V

    .line 841
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_0

    .line 872
    :goto_0
    return-void

    .line 845
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2

    .line 849
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    .line 851
    if-eqz v0, :cond_1

    .line 852
    new-instance v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$CameraSizeComparator;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 854
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 855
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v2, v3

    mul-int/lit8 v2, v2, 0x1e

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    div-int/lit16 v3, v3, 0x3e8

    mul-int/2addr v2, v3

    int-to-long v2, v2

    .line 856
    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iget v5, v0, Landroid/hardware/Camera$Size;->height:I

    if-lt v4, v5, :cond_2

    iget-wide v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRawVideoSize:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 857
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iput v3, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 858
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto :goto_1

    .line 866
    :cond_1
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Device does not have any valid supported preview sizes. Defaulting to width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    :cond_2
    new-instance v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    .line 870
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v1, v1, v2

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 871
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v0, v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    invoke-static {v1, v0}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->getProfile(Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;I)V

    goto/16 :goto_0
.end method

.method private getRecordDurationInSeconds()I
    .locals 4

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->getDuration()J

    move-result-wide v0

    long-to-float v0, v0

    const/high16 v1, 0x447a

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1775
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Record duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1777
    return v0
.end method

.method private getStoragePath()Ljava/lang/String;
    .locals 7

    .prologue
    .line 1516
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1517
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "MMddyyyyHHmmss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1519
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "android_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".mp4"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1520
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "android_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".jpeg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1521
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v1

    .line 1523
    const-string v3, "mounted"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getAvailableStorage()J

    move-result-wide v3

    const-wide/32 v5, 0x300000

    cmp-long v1, v3, v5

    if-lez v1, :cond_0

    .line 1524
    new-instance v1, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1525
    new-instance v2, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v2

    .line 1531
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    .line 1532
    const-string v2, "Tango.RecordVideomailActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Local File Name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1533
    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentThumbnailFilename:Ljava/lang/String;

    .line 1535
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    .line 1536
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Local File Name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1527
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1528
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_0
.end method

.method private handleRecordingError(Ljava/lang/Exception;)V
    .locals 2
    .parameter

    .prologue
    .line 1084
    const-string v0, "handlerRecordingError: Serious error happened during recording."

    .line 1085
    if-nez p1, :cond_0

    .line 1086
    const-string v1, "Tango.RecordVideomailActivity"

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    :goto_0
    const v0, 0x7f0900b8

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1095
    return-void

    .line 1088
    :cond_0
    const-string v1, "Tango.RecordVideomailActivity"

    invoke-static {v1, v0, p1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private initVideoPreferences()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1027
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isSupportCamcorderProfile:Z

    if-eqz v0, :cond_0

    .line 1028
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v0, v0, v1

    invoke-static {v0, v2}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    .line 1029
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->setFromCamcorderProfile(Landroid/media/CamcorderProfile;)V

    .line 1031
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getIdealResolution()V

    .line 1032
    return-void

    .line 1028
    :cond_1
    invoke-static {v2}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    goto :goto_0
.end method

.method private initializeRecorder()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 890
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "initializeRecorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 892
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_0

    .line 1011
    :goto_0
    return-void

    .line 896
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_1

    .line 897
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Surface holder is null. Wait for surface change"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 901
    :cond_1
    iput-boolean v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isMaxDurationReached:Z

    .line 904
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I800"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D700"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I405"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 910
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->stopPreview()V

    .line 913
    :cond_3
    new-instance v0, Landroid/media/MediaRecorder;

    invoke-direct {v0}, Landroid/media/MediaRecorder;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    .line 916
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->unlock()V

    .line 917
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCamera()Landroid/hardware/Camera;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setCamera(Landroid/hardware/Camera;)V

    .line 919
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "SHARP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 921
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v5}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    .line 925
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v5}, Landroid/media/MediaRecorder;->setVideoSource(I)V

    .line 926
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isSupportCamcorderProfile:Z

    if-eqz v0, :cond_7

    .line 927
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setToCamcorderProfile()V

    .line 930
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setProfile(Landroid/media/CamcorderProfile;)V

    .line 941
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setMaxDuration(I)V

    .line 943
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getStoragePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFile(Ljava/lang/String;)V

    .line 944
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setPreviewDisplay(Landroid/view/Surface;)V

    .line 947
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_9

    .line 948
    new-instance v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    .line 949
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v1, v1, v2

    invoke-static {v1, v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 952
    iget v1, v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->orientation:I

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientation:I

    iget v0, v0, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    invoke-static {v1, v2, v0}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->getRecorderRotation(III)I

    move-result v0

    .line 955
    :goto_3
    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    .line 957
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    rem-int/lit8 v1, v1, 0x5a

    if-eqz v1, :cond_4

    .line 958
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rotation Hint = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is NOT a multiple of 90!!!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 959
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    add-int/lit8 v1, v1, 0x5a

    div-int/lit8 v1, v1, 0x5a

    mul-int/lit8 v1, v1, 0x5a

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    .line 960
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Normalized Rotation Hint to = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    :cond_4
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isRotated()Z

    move-result v1

    .line 970
    sget-boolean v2, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v2, :cond_8

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v3, "Samsung"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    if-nez v1, :cond_8

    .line 971
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Orientation Hint: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v0}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    move v1, v5

    .line 982
    :goto_4
    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isVideomailReplaySupported:Z

    if-eqz v2, :cond_5

    if-nez v1, :cond_5

    .line 983
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Orientation Hint (for replay): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v1, v0}, Landroid/media/MediaRecorder;->setOrientationHint(I)V

    .line 988
    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 997
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, p0}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 998
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, p0}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 1000
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder fileFormat: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  quality: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->quality:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->duration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1003
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder audioCodec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  audioChannels: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioChannels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  audioSampleRate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioSampleRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  audioBitRate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1007
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaRecorder videoCodec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  videoFrameSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  videoFrameRate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  videoBitRate: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 923
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioSource(I)V

    goto/16 :goto_1

    .line 933
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setOutputFormat(I)V

    .line 934
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setAudioEncoder(I)V

    .line 935
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoEncoder(I)V

    .line 936
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaRecorder;->setVideoSize(II)V

    .line 937
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v0, v1}, Landroid/media/MediaRecorder;->setVideoFrameRate(I)V

    goto/16 :goto_2

    .line 989
    :catch_0
    move-exception v0

    .line 990
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepare failed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 991
    iput-boolean v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 992
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 993
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->releaseMediaRecorder()V

    .line 994
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_8
    move v1, v4

    goto/16 :goto_4

    :cond_9
    move v0, v4

    goto/16 :goto_3
.end method

.method private initializeView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 357
    const v0, 0x7f0a016f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/SurfaceView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    .line 358
    const v0, 0x7f0a00f8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    .line 359
    const v0, 0x7f0a00f6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    .line 360
    const v0, 0x7f0a00f7

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    .line 361
    const v0, 0x7f0a0170

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/RotateButton;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    .line 362
    const v0, 0x7f0a0171

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/Timer;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    .line 363
    const v0, 0x7f0a015e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    .line 364
    const v0, 0x7f0a015f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_fileUploadProgress:Landroid/widget/ProgressBar;

    .line 365
    const v0, 0x7f0a015c

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    .line 366
    const v0, 0x7f0a015d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertMessage:Landroid/widget/TextView;

    .line 367
    const v0, 0x7f0a0160

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_uploadingMessage:Landroid/widget/TextView;

    .line 369
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_progressView:Landroid/view/View;

    .line 372
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 375
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 377
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/RotateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/Timer;->setTimerCallback(Lcom/sgiggle/production/widget/Timer$Callback;)V

    .line 381
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isVideomailReplaySupported:Z

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 392
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 393
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 394
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 397
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 398
    new-instance v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$1;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 410
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private isSending()Z
    .locals 1

    .prologue
    .line 1795
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private keepScreenOn()V
    .locals 2

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1577
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1578
    return-void
.end method

.method private keepScreenOnAwhile()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1570
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1571
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1572
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const-wide/32 v1, 0x1d4c0

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1573
    return-void
.end method

.method private onRecordFinished()V
    .locals 1

    .prologue
    .line 1394
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isVideomailReplaySupported:Z

    if-eqz v0, :cond_0

    .line 1395
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    .line 1396
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->reviewVideoMail()V

    .line 1401
    :goto_0
    return-void

    .line 1399
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    goto :goto_0
.end method

.method private openCamera(I)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sgiggle/production/screens/videomail/CameraHardwareException;
        }
    .end annotation

    .prologue
    .line 643
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    if-eq v0, p1, :cond_0

    .line 644
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 645
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    .line 646
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    .line 650
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_2

    .line 652
    :try_start_0
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "open camera "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    aget v0, v0, p1

    invoke-static {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->open(I)Lcom/sgiggle/VideoCapture/CameraWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    .line 655
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-nez v0, :cond_1

    .line 658
    new-instance v0, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "CameraWrapper swallowed our runtime exception and returned a null camera device!"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    :catch_0
    move-exception v0

    .line 663
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "fail to connect Camera"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 664
    new-instance v1, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;

    invoke-direct {v1, v0}, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 661
    :cond_1
    :try_start_1
    iput p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    .line 668
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    .line 685
    :goto_0
    return-void

    .line 671
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->reconnect()V

    .line 672
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reconnect camera "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    aget v2, v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 679
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-nez v0, :cond_3

    .line 680
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "recording-size-width"

    const/16 v2, 0x140

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 681
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "recording-size-height"

    const/16 v2, 0xf0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 683
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0

    .line 673
    :catch_1
    move-exception v0

    .line 674
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "reconnect failed"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    new-instance v1, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;

    invoke-direct {v1, v0}, Lcom/sgiggle/production/screens/videomail/CameraHardwareException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private prepareCamera()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 601
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 602
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 604
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 605
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 607
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->startPrepareCameraThread()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 613
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 608
    :catch_0
    move-exception v0

    .line 609
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "prepareCamera() failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    .line 610
    goto :goto_0
.end method

.method private releaseCamera()V
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->stopPreview()V

    .line 701
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->release()V

    .line 702
    return-void
.end method

.method private releaseMediaRecorder()V
    .locals 2

    .prologue
    .line 1014
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Releasing media recorder."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->reset()V

    .line 1017
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->release()V

    .line 1018
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    .line 1022
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_1

    .line 1023
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->lock()V

    .line 1024
    :cond_1
    return-void
.end method

.method private resetRecButtonLight()V
    .locals 2

    .prologue
    .line 1869
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1870
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1871
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setRecButtonLightOn(Z)V

    .line 1872
    return-void
.end method

.method private resetScreenOn()V
    .locals 2

    .prologue
    .line 1565
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1566
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1567
    return-void
.end method

.method private resizeForPreviewAspectRatio()V
    .locals 5

    .prologue
    .line 575
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 576
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->calcCameraDisplayOrientation()V

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    if-eqz v0, :cond_2

    .line 579
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    rem-int/lit16 v1, v1, 0xb4

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    int-to-double v1, v1

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v3, v3, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    :goto_0
    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->setAspectRatio(D)V

    .line 587
    :goto_1
    return-void

    .line 579
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    int-to-double v1, v1

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v3, v3, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    int-to-double v3, v3

    div-double/2addr v1, v3

    goto :goto_0

    .line 584
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDisplayOrientation:I

    rem-int/lit16 v1, v1, 0xb4

    if-nez v1, :cond_3

    const-wide v1, 0x3ff5555555555555L

    :goto_2
    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->setAspectRatio(D)V

    goto :goto_1

    :cond_3
    const-wide/high16 v1, 0x3fe8

    goto :goto_2
.end method

.method private restartPreview()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 621
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_progressView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 622
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 624
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;

    invoke-direct {v1, p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$2;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 636
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 638
    return-void
.end method

.method private sendStartRecordingMessage()V
    .locals 3

    .prologue
    .line 1427
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartRecordingVideoMailMessage;-><init>()V

    .line 1428
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1429
    return-void
.end method

.method private sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V
    .locals 3
    .parameter

    .prologue
    .line 1432
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StopRecordingVideoMailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1433
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 1434
    return-void
.end method

.method private setActivityLayout()V
    .locals 2

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v0, v0, v1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setActivityLayout(I)V

    .line 515
    return-void
.end method

.method private setActivityLayout(I)V
    .locals 6
    .parameter

    .prologue
    const v4, 0x7f02001a

    const v3, 0x7f020018

    .line 523
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "New layout orientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getRequestedOrientation()I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 525
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setRequestedOrientation(I)V

    .line 530
    :cond_0
    const/4 v0, 0x0

    .line 531
    if-nez p1, :cond_2

    .line 532
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->useFlippedLandscapeLayout()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 534
    const v1, 0x7f03006a

    .line 537
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOnDrawable:Landroid/graphics/drawable/Drawable;

    .line 538
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOffDrawable:Landroid/graphics/drawable/Drawable;

    move v5, v0

    move v0, v1

    move v1, v5

    .line 556
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setContentView(I)V

    .line 557
    const v0, 0x7f0a0170

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/RotateButton;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/RotateButton;->lockUntilLayoutOrientationReached(Z)V

    .line 559
    iput p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentRequestedOrientation:I

    .line 560
    return-void

    .line 541
    :cond_1
    const v0, 0x7f03006b

    .line 544
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOnDrawable:Landroid/graphics/drawable/Drawable;

    .line 545
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOffDrawable:Landroid/graphics/drawable/Drawable;

    .line 547
    const/4 v1, 0x1

    goto :goto_0

    .line 550
    :cond_2
    const v1, 0x7f03006c

    .line 553
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOnDrawable:Landroid/graphics/drawable/Drawable;

    .line 554
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOffDrawable:Landroid/graphics/drawable/Drawable;

    move v5, v0

    move v0, v1

    move v1, v5

    goto :goto_0
.end method

.method private setCameraParameters()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 723
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    .line 725
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 732
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 733
    const-string v2, "getSupportedPreviewFpsRange"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 734
    :try_start_1
    const-string v2, "setPreviewFpsRange"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v1

    move-object v2, v0

    .line 738
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 739
    const-string v4, "getSupportedPreviewFrameRates"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    .line 743
    :goto_1
    if-eqz v2, :cond_8

    if-eqz v1, :cond_8

    .line 744
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calling initFpsRange() with "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 745
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-static {v0, v1, v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFPSRange(Landroid/hardware/Camera$Parameters;II)Ljava/util/List;

    move-result-object v1

    .line 746
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_1

    .line 748
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    if-ge v2, v0, :cond_7

    .line 749
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    iput v0, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 752
    :cond_0
    :goto_2
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initFpsRange use fps="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setWhiteBalance(Ljava/lang/String;)V

    .line 766
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SHW-M110S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-P100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I897"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "recording-size-width"

    const/16 v2, 0x140

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 768
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "recording-size-height"

    const/16 v2, 0xf0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->set(Ljava/lang/String;I)V

    .line 772
    :cond_3
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "sony ericsson"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LT26i"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_4

    .line 773
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0x280

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 774
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0x1e0

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 775
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0xf

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 779
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    .line 780
    sget-boolean v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isGingerbreadOrHigher:Z

    if-eqz v1, :cond_b

    .line 781
    const-string v1, "continuous-video"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 782
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "continuous-video"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 791
    :cond_5
    :goto_4
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "VS910 4G"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 793
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const/16 v1, 0x500

    const/16 v2, 0x3c0

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 798
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    invoke-virtual {v0, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 807
    :goto_5
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    .line 808
    return-void

    .line 735
    :catch_0
    move-exception v0

    move-object v0, v6

    :goto_6
    move-object v1, v6

    move-object v2, v0

    goto/16 :goto_0

    .line 740
    :catch_1
    move-exception v0

    move-object v0, v6

    goto/16 :goto_1

    .line 750
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    if-le v2, v0, :cond_0

    .line 751
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    iput v0, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    goto/16 :goto_2

    .line 754
    :cond_8
    if-eqz v0, :cond_9

    .line 755
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-static {v1, v2}, Lcom/sgiggle/VideoCapture/VideoCaptureRaw;->initFrameRate(Landroid/hardware/Camera$Parameters;I)I

    move-result v1

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 756
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initFrameRate use fps="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 758
    :cond_9
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setPreviewFrameRate(I)V

    .line 759
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPreviewFrameRate use fps="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v2, v2, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 783
    :cond_a
    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 784
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 786
    :cond_b
    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 787
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraParameters:Landroid/hardware/Camera$Parameters;

    const-string v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 800
    :catch_2
    move-exception v0

    .line 802
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "failed to setCameraParameters in RecordVideoMailActivity"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 735
    :catch_3
    move-exception v1

    goto/16 :goto_6
.end method

.method private setDefaultProfile()V
    .locals 2

    .prologue
    .line 827
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/4 v1, 0x2

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    .line 828
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    sget v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->AUDIO_BIT_RATE:I

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    .line 829
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    sget v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->VIDEO_BIT_RATE:I

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 830
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    sget v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->AUDIO_CODEC:I

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    .line 831
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    sget v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->VIDEO_CODEC:I

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    .line 832
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0x1e

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 833
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0x140

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 834
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    const/16 v1, 0xf0

    iput v1, v0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 835
    return-void
.end method

.method private setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 3
    .parameter

    .prologue
    .line 706
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    invoke-virtual {v0, p1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 708
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->closeCamera()V

    .line 709
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "setPreviewDisplay failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private setRecButtonLightOn(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 1879
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOnDrawable:Landroid/graphics/drawable/Drawable;

    .line 1880
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1881
    return-void

    .line 1879
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recBtnOffDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private setToCamcorderProfile()V
    .locals 2

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    iput v1, v0, Landroid/media/CamcorderProfile;->audioBitRate:I

    .line 876
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioChannels:I

    iput v1, v0, Landroid/media/CamcorderProfile;->audioChannels:I

    .line 877
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    iput v1, v0, Landroid/media/CamcorderProfile;->audioCodec:I

    .line 878
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioSampleRate:I

    iput v1, v0, Landroid/media/CamcorderProfile;->audioSampleRate:I

    .line 879
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->duration:I

    iput v1, v0, Landroid/media/CamcorderProfile;->duration:I

    .line 880
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    iput v1, v0, Landroid/media/CamcorderProfile;->fileFormat:I

    .line 881
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->quality:I

    iput v1, v0, Landroid/media/CamcorderProfile;->quality:I

    .line 882
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    iput v1, v0, Landroid/media/CamcorderProfile;->videoBitRate:I

    .line 883
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    iput v1, v0, Landroid/media/CamcorderProfile;->videoCodec:I

    .line 884
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    iput v1, v0, Landroid/media/CamcorderProfile;->videoFrameRate:I

    .line 885
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    iput v1, v0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    .line 886
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_camcorderProfile:Landroid/media/CamcorderProfile;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_profile:Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;

    iget v1, v1, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    iput v1, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    .line 887
    return-void
.end method

.method private showFileUploader()V
    .locals 2

    .prologue
    .line 1548
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1549
    return-void
.end method

.method private startPrepareCameraThread()V
    .locals 2

    .prologue
    .line 590
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "prepareCamera(): opening camera and start preview"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    if-nez v0, :cond_0

    .line 592
    new-instance v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    .line 593
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->start()V

    .line 595
    :cond_0
    return-void
.end method

.method private startRecording()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1035
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "startRecording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1037
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    if-le v0, v3, :cond_0

    .line 1038
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 1043
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initializeRecorder()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1053
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    if-nez v0, :cond_1

    .line 1054
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Failed to initialize media recorder"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1055
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->handleRecordingError(Ljava/lang/Exception;)V

    .line 1076
    :goto_0
    return-void

    .line 1044
    :catch_0
    move-exception v0

    .line 1045
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 1046
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 1047
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->releaseMediaRecorder()V

    .line 1048
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "Failed to initialize media recorder"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->handleRecordingError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 1060
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->start()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1070
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationCompensation:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/Timer;->setOrientation(I)V

    .line 1071
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->start()V

    .line 1072
    iput-boolean v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 1073
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 1075
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->keepScreenOn()V

    goto :goto_0

    .line 1061
    :catch_1
    move-exception v0

    .line 1062
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 1063
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 1064
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "Unable to start recording"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1065
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->releaseMediaRecorder()V

    .line 1066
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->handleRecordingError(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private startSend()V
    .locals 18

    .prologue
    .line 1733
    const-string v2, "Tango.RecordVideomailActivity"

    const-string v3, "startSend()"

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1734
    const-wide/16 v2, 0x0

    .line 1736
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    move-object v4, v0

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    move-object v4, v0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 1737
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    move-object v5, v0

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1738
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1739
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v2

    move-wide v9, v2

    .line 1743
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v9, v2

    if-nez v2, :cond_0

    .line 1744
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->onUploadFailedFileNotFound()V

    .line 1745
    const-string v2, "Tango.RecordVideomailActivity"

    const-string v3, "uploadFile(): contentLength = 0. No need to request for upload token."

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1767
    :goto_1
    return-void

    .line 1750
    :cond_0
    new-instance v2, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    move-object v4, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentThumbnailFilename:Ljava/lang/String;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_calleesList:Ljava/util/List;

    move-object v6, v0

    invoke-direct/range {p0 .. p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getRecordDurationInSeconds()I

    move-result v7

    const-string v8, "video/mp4"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    const-wide/16 v13, 0x3e8

    div-long/2addr v11, v13

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isRotated()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    move v3, v0

    move v13, v3

    :goto_2
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isFlipped()Z

    move-result v14

    const/4 v15, 0x1

    const/16 v16, 0x1

    move-object/from16 v3, p0

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v17}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;JJIZZZLcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;)V

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 1765
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/TangoApp;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object v3, v0

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/TangoApp;->setSendVideomailActivityHelper(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)V

    .line 1766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->send()V

    goto :goto_1

    .line 1750
    :cond_1
    const/4 v3, 0x0

    move v13, v3

    goto :goto_2

    :cond_2
    move-wide v9, v2

    goto :goto_0
.end method

.method private stopPrepareCameraThread()V
    .locals 2

    .prologue
    .line 1366
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    if-eqz v0, :cond_0

    .line 1367
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "stopPrepareCameraThread()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;->requestExitAndWait()V

    .line 1369
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    .line 1371
    :cond_0
    return-void
.end method

.method private stopRecording()V
    .locals 1

    .prologue
    .line 1101
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    .line 1102
    return-void
.end method

.method private stopRecording(Z)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1109
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "stopRecording()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isStopping:Z

    .line 1112
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_0

    .line 1113
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->stop()V

    .line 1114
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setOnErrorListener(Landroid/media/MediaRecorder$OnErrorListener;)V

    .line 1115
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0, v2}, Landroid/media/MediaRecorder;->setOnInfoListener(Landroid/media/MediaRecorder$OnInfoListener;)V

    .line 1118
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_mediaRecorder:Landroid/media/MediaRecorder;

    invoke-virtual {v0}, Landroid/media/MediaRecorder;->stop()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1123
    :goto_0
    iput-boolean v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    .line 1125
    if-eqz p1, :cond_0

    .line 1126
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1130
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->releaseMediaRecorder()V

    .line 1135
    if-eqz p1, :cond_1

    .line 1136
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->restartPreview()V

    .line 1137
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1139
    :cond_1
    iput-boolean v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isStopping:Z

    .line 1140
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 1141
    return-void

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    const-string v1, "Tango.RecordVideomailActivity"

    const-string v2, "stopRecording: failed to stop media recorder"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private switchCamera(I)V
    .locals 3
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 1437
    if-nez p1, :cond_0

    .line 1438
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Camera: BACK"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1447
    :goto_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isPausing:Z

    if-eqz v0, :cond_2

    .line 1476
    :goto_1
    return-void

    .line 1439
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1440
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Camera: FRONT"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1442
    :cond_1
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Camera: OTHER"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1443
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    rem-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->switchCamera(I)V

    goto :goto_1

    .line 1451
    :cond_2
    iput p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    .line 1453
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_3

    .line 1454
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording()V

    .line 1455
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1458
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->closeCamera()V

    .line 1460
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initVideoPreferences()V

    .line 1463
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    aget v0, v0, v1

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getRequestedOrientation()I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 1464
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceView:Landroid/view/SurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/SurfaceView;->setVisibility(I)V

    .line 1465
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 1467
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setActivityLayout()V

    .line 1468
    const v0, 0x7f0a016d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/VideoPreviewLayout;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    .line 1469
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->setOnSizeChangedListener(Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;)V

    .line 1470
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initializeView()V

    .line 1471
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 1474
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resizeForPreviewAspectRatio()V

    .line 1475
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->prepareCamera()Z

    goto :goto_1
.end method

.method private throttleRecord()Z
    .locals 4

    .prologue
    .line 1716
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastRecord:J

    sub-long/2addr v0, v2

    .line 1717
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1718
    const/4 v0, 0x1

    .line 1721
    :goto_0
    return v0

    .line 1720
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastRecord:J

    .line 1721
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private throttleSwitchCamera()Z
    .locals 4

    .prologue
    .line 1703
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastCameraSwitch:J

    sub-long/2addr v0, v2

    .line 1704
    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 1705
    const/4 v0, 0x1

    .line 1708
    :goto_0
    return v0

    .line 1707
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_timestampLastCameraSwitch:J

    .line 1708
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatePerCameraLayoutOrientation()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 492
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    aput v1, v0, v1

    .line 493
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    aput v1, v0, v2

    .line 496
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Nexus S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P1010"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T959V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I510"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SGH-T839"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I997"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_layout_orientation:[I

    aput v2, v0, v2

    .line 508
    :cond_0
    return-void
.end method

.method private useFlippedLandscapeLayout()Z
    .locals 1

    .prologue
    .line 570
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public isReviewInProgress()Z
    .locals 1

    .prologue
    .line 1308
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isReviewInProgress:Z

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1314
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v1, :cond_1

    .line 1315
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleOnActivityResult(IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1317
    monitor-exit v0

    .line 1360
    :cond_0
    :goto_0
    return-void

    .line 1320
    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1323
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1328
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isReviewInProgress:Z

    .line 1330
    packed-switch p2, :pswitch_data_0

    .line 1354
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected result (logic error or previous activity crashed), code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1356
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->finish()V

    goto :goto_0

    .line 1320
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 1339
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->hide()V

    .line 1340
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->reset()V

    .line 1342
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1343
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1344
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    goto :goto_0

    .line 1349
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1350
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->finish()V

    goto :goto_0

    .line 1330
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1414
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_0

    .line 1415
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    .line 1418
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->cleanupFiles()V

    .line 1420
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    .line 1423
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1424
    return-void
.end method

.method public onBeforeSmsDialogShown()V
    .locals 1

    .prologue
    .line 1854
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentRequestedOrientation:I

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->useFlippedLandscapeLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setActivityLayout(I)V

    .line 1857
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1196
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isStopping:Z

    if-eqz v0, :cond_1

    .line 1197
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Stop in progress, ignoring event."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    :cond_0
    :goto_0
    return-void

    .line 1201
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_record:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 1202
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->isSending()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1206
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->throttleRecord()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1207
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Throttling record action..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1211
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1213
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_3

    .line 1215
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1217
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->onRecordFinished()V

    goto :goto_0

    .line 1219
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStartRecordingMessage()V

    .line 1220
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updateRecordUi(Z)V

    .line 1221
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->startRecording()V

    goto :goto_0

    .line 1223
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    if-ne p1, v0, :cond_5

    .line 1224
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1226
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->startSend()V

    goto :goto_0

    .line 1228
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    if-ne p1, v0, :cond_7

    .line 1229
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1230
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_6

    .line 1231
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->STOP:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1232
    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    .line 1235
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->cancelSend()V

    .line 1237
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->cleanupFiles()V

    .line 1240
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;->ABORT:Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->sendStopRecordingMessage(Lcom/sgiggle/xmpp/SessionMessages$StopRecordingVideoMailPayload$Type;)V

    .line 1241
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->finish()V

    goto :goto_0

    .line 1242
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    if-ne p1, v0, :cond_0

    .line 1243
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->throttleSwitchCamera()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1244
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Throttling switch camera action..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1246
    :cond_8
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    rem-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->switchCamera(I)V

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 478
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 479
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 254
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->requestWindowFeature(I)Z

    .line 259
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 260
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 263
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const v1, 0x3f333333

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    .line 265
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 268
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;

    .line 269
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 270
    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getCalleesList()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_calleesList:Ljava/util/List;

    .line 274
    :cond_0
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SHW-M110S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SAMSUNG-SGH-I997"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 279
    :cond_1
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    .line 280
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    .line 294
    :cond_2
    :goto_0
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    if-le v1, v6, :cond_3

    .line 295
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    .line 297
    :cond_3
    const-string v1, "Tango.RecordVideomailActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Number of cameras: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initVideoPreferences()V

    .line 302
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isVideomailReplaySupported:Z

    .line 305
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->updatePerCameraLayoutOrientation()V

    .line 308
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setActivityLayout()V

    .line 310
    const v1, 0x7f0a016d

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/production/widget/VideoPreviewLayout;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    .line 311
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_previewLayout:Lcom/sgiggle/production/widget/VideoPreviewLayout;

    invoke-virtual {v1, p0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->setOnSizeChangedListener(Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;)V

    .line 313
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initializeView()V

    .line 316
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resizeForPreviewAspectRatio()V

    .line 318
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    if-le v1, v4, :cond_6

    .line 319
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    invoke-virtual {v1, v5}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    .line 325
    :goto_1
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->hasMaxRecordingDuration()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v1

    check-cast v1, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getMaxRecordingDuration()I

    move-result v1

    if-lez v1, :cond_7

    .line 328
    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getMaxRecordingDuration()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    .line 329
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->calculateMaxRawVideoSize(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRawVideoSize:J

    .line 330
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Max recording duration (ms): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/widget/Timer;->setMaxTimerDuration(J)V

    .line 338
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/widget/Timer;->setStartCallbackAt(J)V

    .line 340
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    const/16 v1, 0x2710

    if-gt v0, v1, :cond_8

    .line 342
    iput-boolean v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alwaysShowTimerAlert:Z

    .line 350
    :goto_3
    new-instance v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;

    invoke-direct {v0, p0, p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;-><init>(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationListener:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;

    .line 352
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setRecordVideomailActivityInstance(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 353
    return-void

    .line 282
    :cond_4
    invoke-static {}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getNumberOfCameras()I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    .line 283
    new-instance v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;

    invoke-direct {v1}, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;-><init>()V

    move v2, v5

    .line 285
    :goto_4
    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_numberOfCameras:I

    if-ge v2, v3, :cond_2

    .line 286
    invoke-static {v2, v1}, Lcom/sgiggle/VideoCapture/CameraWrapper;->getCameraInfo(ILcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;)V

    .line 287
    iget v3, v1, Lcom/sgiggle/VideoCapture/CameraWrapper$CameraInfo;->facing:I

    if-ne v3, v4, :cond_5

    .line 288
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraId:I

    .line 289
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraMap:[I

    aput v2, v1, v4

    goto/16 :goto_0

    .line 285
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 321
    :cond_6
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_switchCamera:Lcom/sgiggle/production/widget/RotateButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/widget/RotateButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 332
    :cond_7
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Max recording duration MISSING or INVALID, check state machine. Default value will be used: 30000"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 345
    :cond_8
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_showAutoDismissTimerAlertAt:I

    .line 346
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_showAutoDismissTimerAlertAt:I

    add-int/lit16 v0, v0, 0x1388

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_hideAutoDismissTimerAlertAt:I

    .line 347
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    const/16 v1, 0x1388

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_showPermanentTimerAlertAt:I

    goto :goto_3
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 462
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 465
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->cancelSend()V

    .line 468
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 469
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 470
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 471
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 473
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setRecordVideomailActivityInstance(Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;)V

    .line 474
    return-void
.end method

.method public onDetached(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 1838
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onDetached"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1841
    if-eqz p1, :cond_0

    .line 1842
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1843
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1846
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v0

    .line 1847
    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 1848
    monitor-exit v0

    .line 1849
    return-void

    .line 1848
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onError(Landroid/media/MediaRecorder;II)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1405
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->handleRecordingError(Ljava/lang/Exception;)V

    .line 1406
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 1407
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onError - Error received: MEDIA_RECORDER_ERROR_UNKNOWN - stopping recording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1408
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording()V

    .line 1410
    :cond_0
    return-void
.end method

.method public onInfo(Landroid/media/MediaRecorder;II)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1375
    const/16 v0, 0x320

    if-ne p2, v0, :cond_0

    .line 1376
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onInfo: MEDIA_RECORDER_INFO_MAX_DURATION_REACHED - stopping recording"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1377
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_0

    .line 1378
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isMaxDurationReached:Z

    .line 1380
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_countdownTimer:Lcom/sgiggle/production/widget/Timer;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/Timer;->stopAtMaxDuration()V

    .line 1381
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertMessage:Landroid/widget/TextView;

    const v1, 0x7f0900b6

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    int-to-long v3, v3

    invoke-static {v3, v4}, Lcom/sgiggle/production/widget/Timer;->formatTime(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1382
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1384
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->onRecordFinished()V

    .line 1387
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 434
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isPausing:Z

    .line 439
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-eqz v0, :cond_0

    .line 440
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopRecording(Z)V

    .line 454
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->stopPrepareCameraThread()V

    .line 455
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resetScreenOn()V

    .line 456
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->closeCamera()V

    .line 457
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationListener:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->disable()V

    .line 458
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 414
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onResume()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 417
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isPausing:Z

    .line 419
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_orientationListener:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$RecorderOrientationListener;->enable()V

    .line 420
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->initVideoPreferences()V

    .line 421
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resizeForPreviewAspectRatio()V

    .line 422
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_prepareCameraThread:Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity$PrepareCameraThread;

    if-nez v0, :cond_0

    .line 423
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->prepareCamera()Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "onResume(): failed with preparing camera"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->keepScreenOnAwhile()V

    goto :goto_0
.end method

.method public onSendSuccess()V
    .locals 0

    .prologue
    .line 1828
    return-void
.end method

.method public onSizeChanged()V
    .locals 0

    .prologue
    .line 1146
    return-void
.end method

.method public onSmsDialogDismissed()V
    .locals 0

    .prologue
    .line 1863
    return-void
.end method

.method public onTimerStopped()V
    .locals 0

    .prologue
    .line 1696
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resetRecButtonLight()V

    .line 1697
    return-void
.end method

.method public onTimerUpdated(JJ)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v4, 0x7

    const/4 v2, 0x6

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1631
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_1

    .line 1633
    cmp-long v0, p1, v6

    if-ltz v0, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1636
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1639
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1642
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1648
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alwaysShowTimerAlert:Z

    if-eqz v0, :cond_2

    move v0, v3

    .line 1683
    :goto_0
    if-eqz v0, :cond_4

    .line 1684
    const-wide/16 v0, 0x3e7

    add-long/2addr v0, p3

    div-long/2addr v0, v6

    long-to-int v0, v0

    .line 1685
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0005

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1686
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertMessage:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1687
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1692
    :cond_1
    :goto_1
    return-void

    .line 1670
    :cond_2
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_showAutoDismissTimerAlertAt:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_5

    .line 1671
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_hideAutoDismissTimerAlertAt:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_3

    .line 1673
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->SHOW_TIMER_AUTO_DISMISS_ALERT:Z

    goto :goto_0

    .line 1674
    :cond_3
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_showPermanentTimerAlertAt:I

    int-to-long v0, v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_5

    move v0, v3

    .line 1676
    goto :goto_0

    .line 1689
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1

    :cond_5
    move v0, v5

    goto :goto_0
.end method

.method public onUploadFailedFileNotFound()V
    .locals 2

    .prologue
    .line 1832
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Upload failed, file not found."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1833
    const v0, 0x7f0900b9

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1834
    return-void
.end method

.method public onUploadPreExecute()V
    .locals 2

    .prologue
    .line 1808
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_uploadingMessage:Landroid/widget/TextView;

    const v1, 0x7f0900bc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1809
    return-void
.end method

.method public onUploadProgress(I)V
    .locals 3
    .parameter

    .prologue
    .line 1813
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upload in progress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1814
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_fileUploadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1815
    return-void
.end method

.method public onUploadRequested()V
    .locals 2

    .prologue
    .line 1800
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "Upload about to start..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1802
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_alertNotificaiton:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1803
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->showFileUploader()V

    .line 1804
    return-void
.end method

.method public onUploadWillRetry(I)V
    .locals 5
    .parameter

    .prologue
    .line 1819
    const-string v0, "Tango.RecordVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upload failed, will retry in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1820
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_uploadingMessage:Landroid/widget/TextView;

    const v1, 0x7f0900ba

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1821
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 483
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onUserInteraction()V

    .line 484
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isRecording:Z

    if-nez v0, :cond_0

    .line 485
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->keepScreenOnAwhile()V

    .line 486
    :cond_0
    return-void
.end method

.method protected reviewVideoMail()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1273
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1276
    const-string v0, "videoFilename"

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentFilename:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1277
    const-string v0, "videoThumbnailFilename"

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_currentThumbnailFilename:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1278
    const-string v0, "rotateOnServer"

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isRotated()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1279
    const-string v0, "rotationHint"

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_rotationHint:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1280
    const-string v0, "duration"

    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getRecordDurationInSeconds()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1281
    const-string v0, "flipped"

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isFlipped()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1282
    const-string v0, "mimeType"

    const-string v2, "video/mp4"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1283
    const-string v0, "timeCreated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1284
    const-string v0, "maxDurationReached"

    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isMaxDurationReached:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1285
    const-string v0, "maxDuration"

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_maxRecordingDuration:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1288
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;

    .line 1289
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$RecordVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    .line 1291
    :goto_0
    const-string v2, "recordVideoMailPayload"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1292
    const/high16 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1295
    iput-boolean v6, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isReviewInProgress:Z

    .line 1300
    invoke-virtual {p0, v1, v6}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1301
    return-void

    .line 1289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1151
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1152
    const-string v0, "Tango.RecordVideomailActivity"

    const-string v1, "holder.getSurface() == null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1181
    :cond_0
    :goto_0
    return-void

    .line 1156
    :cond_1
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    .line 1158
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_isPausing:Z

    if-nez v0, :cond_0

    .line 1171
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cameraDevice:Lcom/sgiggle/VideoCapture/CameraWrapper;

    if-eqz v0, :cond_0

    .line 1176
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->isCreating()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1177
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V

    goto :goto_0

    .line 1179
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->restartPreview()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .parameter

    .prologue
    .line 1186
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .parameter

    .prologue
    .line 1190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_surfaceHolder:Landroid/view/SurfaceHolder;

    .line 1191
    return-void
.end method

.method protected updateRecordUi(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1256
    if-eqz p1, :cond_0

    .line 1257
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1258
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1266
    :goto_0
    return-void

    .line 1260
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recordButtonTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1261
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_recordButtonTimer:Ljava/util/Timer;

    .line 1262
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->resetRecButtonLight()V

    .line 1263
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_send:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 1264
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/RecordVideomailActivity;->m_cancel:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
