.class public interface abstract Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;
.super Ljava/lang/Object;
.source "SendVideomailActivityHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SendVideomailActivityHelperListener"
.end annotation


# virtual methods
.method public abstract onBeforeSmsDialogShown()V
.end method

.method public abstract onDetached(Z)V
.end method

.method public abstract onSendSuccess()V
.end method

.method public abstract onSmsDialogDismissed()V
.end method

.method public abstract onUploadFailedFileNotFound()V
.end method

.method public abstract onUploadPreExecute()V
.end method

.method public abstract onUploadProgress(I)V
.end method

.method public abstract onUploadRequested()V
.end method

.method public abstract onUploadWillRetry(I)V
.end method
