.class public Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;
.super Ljava/lang/Object;
.source "SendVideomailActivityHelper.java"

# interfaces
.implements Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;
    }
.end annotation


# static fields
.field private static final REQUEST_CODE_SEND_SMS:I = 0xa

.field private static final TAG:Ljava/lang/String; = "SendVideomailActivityHelper"


# instance fields
.field private m_activity:Landroid/app/Activity;

.field private m_callees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private m_context:Landroid/content/Context;

.field private m_duration:I

.field private m_fileSize:J

.field private m_filename:Ljava/lang/String;

.field private m_flipped:Z

.field private m_fullscreen:Z

.field private m_isSelectingContacts:Z

.field private m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

.field private m_lockInPortrait:Z

.field private m_mimeType:Ljava/lang/String;

.field private m_nonTangoContactsSentTo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private m_rotationHint:I

.field private m_showMessageOnSent:Z

.field private m_thumbnailFilename:Ljava/lang/String;

.field private m_timeCreated:J

.field private m_videomailId:Ljava/lang/String;

.field private m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;JJIZZZLcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;I",
            "Ljava/lang/String;",
            "JJIZZZ",
            "Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    .line 63
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_showMessageOnSent:Z

    .line 148
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    .line 149
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    .line 150
    iput-object p2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_filename:Ljava/lang/String;

    .line 151
    iput-object p3, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_thumbnailFilename:Ljava/lang/String;

    .line 152
    iput-object p4, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_callees:Ljava/util/List;

    .line 153
    iput p5, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_duration:I

    .line 154
    iput-object p6, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_mimeType:Ljava/lang/String;

    .line 155
    iput-wide p7, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_fileSize:J

    .line 156
    iput-wide p9, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_timeCreated:J

    .line 157
    iput p11, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_rotationHint:I

    .line 158
    iput-boolean p12, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_flipped:Z

    .line 159
    move/from16 v0, p13

    move-object v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_fullscreen:Z

    .line 160
    move/from16 v0, p14

    move-object v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_lockInPortrait:Z

    .line 161
    move-object/from16 v0, p15

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    .line 162
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    return-object v0
.end method

.method private cancelUpload()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 304
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->cancel()V

    .line 309
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    if-eqz v0, :cond_1

    .line 310
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->postCancelVideoMailReceiversSelectionMessage()V

    .line 311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    .line 314
    :cond_1
    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    .line 315
    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailId:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setVideomailStateMachineUploadListener(Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;)V

    .line 317
    return-void
.end method

.method private detachListener(Z)V
    .locals 2
    .parameter

    .prologue
    .line 438
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    .line 440
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    .line 441
    invoke-interface {v0, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onDetached(Z)V

    .line 443
    :cond_0
    return-void
.end method

.method private handleDisplayVideoMailNonTangoNotificationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;)V
    .locals 6
    .parameter

    .prologue
    .line 347
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getCalleesList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_nonTangoContactsSentTo:Ljava/util/List;

    .line 348
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailNonTangoNotificationPayload;->getNonTangoUrl()Ljava/lang/String;

    move-result-object v0

    .line 351
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_nonTangoContactsSentTo:Ljava/util/List;

    invoke-static {v1}, Lcom/sgiggle/production/Utils;->getSmsAddressListFromContactList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 354
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900ae

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 357
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    invoke-static {v2}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->createSendBySmsWarningDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v2

    .line 358
    if-nez v2, :cond_0

    .line 360
    const/16 v2, 0xa

    invoke-direct {p0, v1, v0, v2}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->sendSms(Ljava/lang/String;Ljava/lang/String;I)V

    .line 377
    :goto_0
    return-void

    .line 362
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v3, :cond_1

    .line 363
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v3}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onBeforeSmsDialogShown()V

    .line 366
    :cond_1
    new-instance v3, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$1;

    invoke-direct {v3, p0, v1, v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$1;-><init>(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 375
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private handleUploadVideoMailFinishedEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailFinishedEvent;)V
    .locals 3
    .parameter

    .prologue
    .line 326
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->postDismissUploadVideoMailFinishedMessage()V

    .line 328
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_showMessageOnSent:Z

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    const v1, 0x7f0900b7

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onSendSuccess()V

    .line 336
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->detachListener(Z)V

    .line 337
    return-void
.end method

.method private postCancelVideoMailReceiversSelectionMessage()V
    .locals 3

    .prologue
    .line 403
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelVideoMailReceiversSelectionMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$CancelVideoMailReceiversSelectionMessage;-><init>()V

    .line 405
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 406
    return-void
.end method

.method private postConfirmVideoMailNonTangoNotificationMessage(Z)V
    .locals 3
    .parameter

    .prologue
    .line 393
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_nonTangoContactsSentTo:Ljava/util/List;

    invoke-direct {v0, v1, v2, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ConfirmVideoMailNonTangoNotificationMessage;-><init>(Ljava/lang/String;Ljava/util/List;Z)V

    .line 395
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 396
    return-void
.end method

.method private postDismissUploadVideoMailFinishedMessage()V
    .locals 3

    .prologue
    .line 383
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissUploadVideoMailFinishedMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DismissUploadVideoMailFinishedMessage;-><init>(Ljava/lang/String;)V

    .line 385
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 386
    return-void
.end method

.method private postVideoMailReceiversMessage()V
    .locals 3

    .prologue
    .line 413
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailReceiversMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    invoke-static {v1}, Lcom/sgiggle/production/ContactSelectionActivity;->getSupportedMediaType(Landroid/content/Context;)Lcom/sgiggle/xmpp/SessionMessages$MediaType;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailReceiversMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$MediaType;)V

    .line 415
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 416
    return-void
.end method

.method private sendSms(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 425
    invoke-static {p1, p2}, Lcom/sgiggle/production/Utils;->getSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 427
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    invoke-virtual {v1, v0, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :goto_0
    return-void

    .line 428
    :catch_0
    move-exception v0

    .line 429
    const-string v0, "SendVideomailActivityHelper"

    const-string v1, "Not activity was found for ACTION_VIEW (for sending SMS)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startUpload(Ljava/util/List;)V
    .locals 17
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    move-object v2, v0

    if-eqz v2, :cond_0

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    move-object v2, v0

    invoke-interface {v2}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onUploadRequested()V

    .line 288
    :cond_0
    new-instance v2, Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_context:Landroid/content/Context;

    move-object v3, v0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move v1, v4

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_filename:Ljava/lang/String;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_thumbnailFilename:Ljava/lang/String;

    move-object v6, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_duration:I

    move v8, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_mimeType:Ljava/lang/String;

    move-object v9, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_fileSize:J

    move-wide v10, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_timeCreated:J

    move-wide v12, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_rotationHint:I

    move v14, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_flipped:Z

    move v15, v0

    move-object/from16 v7, p1

    move-object/from16 v16, p0

    invoke-direct/range {v2 .. v16}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;JJIZLcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;)V

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    .line 296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    move-object v2, v0

    invoke-virtual {v2}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/TangoApp;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    move-object v3, v0

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/TangoApp;->setVideomailStateMachineUploadListener(Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailUploader:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->start()Z

    .line 298
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 274
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->cancelUpload()V

    .line 277
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->detachListener(Z)V

    .line 278
    return-void
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 219
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 223
    :sswitch_0
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    if-nez v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    .line 226
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailReceiversEvent;

    .line 228
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailReceiversEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailReceiversPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailReceiversPayload;->getCalleesList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/ContactSelectionActivity;->setLatestContactList(Ljava/util/List;)V

    .line 229
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    const-class v2, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 231
    const-string v1, "FullScreen"

    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_fullscreen:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 232
    const-string v1, "LockInPortrait"

    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_lockInPortrait:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 233
    const/high16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 234
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_activity:Landroid/app/Activity;

    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 238
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;

    .line 239
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleDisplayVideoMailNonTangoNotificationEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVideoMailNonTangoNotificationEvent;)V

    goto :goto_0

    .line 243
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailFinishedEvent;

    .line 244
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleUploadVideoMailFinishedEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailFinishedEvent;)V

    goto :goto_0

    .line 219
    :sswitch_data_0
    .sparse-switch
        0x8959 -> :sswitch_0
        0x895a -> :sswitch_1
        0x8965 -> :sswitch_2
    .end sparse-switch
.end method

.method public handleOnActivityResult(IILandroid/content/Intent;)Z
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 172
    const/16 v0, 0x14

    if-ne p1, v0, :cond_0

    .line 173
    packed-switch p2, :pswitch_data_0

    .line 193
    :goto_0
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_isSelectingContacts:Z

    move v0, v1

    .line 210
    :goto_1
    return v0

    .line 177
    :pswitch_0
    const-string v0, "SelectedContacts"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 180
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->startUpload(Ljava/util/List;)V

    goto :goto_0

    .line 185
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->postCancelVideoMailReceiversSelectionMessage()V

    .line 188
    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->detachListener(Z)V

    goto :goto_0

    .line 195
    :cond_0
    const/16 v0, 0xa

    if-ne p1, v0, :cond_1

    .line 199
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_showMessageOnSent:Z

    .line 206
    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->postConfirmVideoMailNonTangoNotificationMessage(Z)V

    move v0, v1

    .line 207
    goto :goto_1

    :cond_1
    move v0, v2

    .line 210
    goto :goto_1

    .line 173
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onUploadDetached()V
    .locals 2

    .prologue
    .line 485
    const-string v0, "SendVideomailActivityHelper"

    const-string v1, "onUploadDetached"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    return-void
.end method

.method public onUploadFailedFileNotFound()V
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onUploadFailedFileNotFound()V

    .line 481
    :cond_0
    return-void
.end method

.method public onUploadPreExecute()V
    .locals 1

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onUploadPreExecute()V

    .line 450
    :cond_0
    return-void
.end method

.method public onUploadProgress(I)V
    .locals 1
    .parameter

    .prologue
    .line 454
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v0, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onUploadProgress(I)V

    .line 457
    :cond_0
    return-void
.end method

.method public onUploadSuccess()V
    .locals 2

    .prologue
    .line 473
    const-string v0, "SendVideomailActivityHelper"

    const-string v1, "Upload succeeded!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    return-void
.end method

.method public onUploadVideoMailIdReceived(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 461
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_videomailId:Ljava/lang/String;

    .line 462
    return-void
.end method

.method public onUploadWillRetry(I)V
    .locals 1
    .parameter

    .prologue
    .line 466
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_listener:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;

    invoke-interface {v0, p1}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;->onUploadWillRetry(I)V

    .line 469
    :cond_0
    return-void
.end method

.method public send()V
    .locals 1

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->cancelUpload()V

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_showMessageOnSent:Z

    .line 260
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_callees:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_callees:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->postVideoMailReceiversMessage()V

    .line 267
    :goto_0
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->m_callees:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->startUpload(Ljava/util/List;)V

    goto :goto_0
.end method
