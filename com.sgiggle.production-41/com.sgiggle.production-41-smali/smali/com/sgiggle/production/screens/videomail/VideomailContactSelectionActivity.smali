.class public Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;
.super Lcom/sgiggle/production/ContactSelectionActivity;
.source "VideomailContactSelectionActivity.java"


# static fields
.field public static final CONTACT_SELECTION_LIMIT:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sgiggle/production/ContactSelectionActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected getOkButtonString(I)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 15
    if-nez p1, :cond_0

    .line 16
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 18
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0006

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected getSelectionLimit()I
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0xc8

    return v0
.end method

.method protected getSelectionLimitReachedString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 35
    const v0, 0x7f0900c6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;->getSelectionLimit()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getTitleString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailContactSelectionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
