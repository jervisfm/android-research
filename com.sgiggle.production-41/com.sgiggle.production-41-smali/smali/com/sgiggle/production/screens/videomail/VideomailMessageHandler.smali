.class public Lcom/sgiggle/production/screens/videomail/VideomailMessageHandler;
.super Ljava/lang/Object;
.source "VideomailMessageHandler.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.VideomailMessageHandler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static handleMessage(Landroid/content/Context;Lcom/sgiggle/messaging/Message;)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 24
    .line 27
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 28
    :cond_0
    const-string v0, "Tango.VideomailMessageHandler"

    const-string v1, "handleMessage: invalid parameters."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    .line 70
    :goto_0
    return v0

    .line 32
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    .line 34
    packed-switch v0, :pswitch_data_0

    move v0, v3

    goto :goto_0

    .line 36
    :pswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailResultEvent;

    .line 37
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    if-nez v0, :cond_3

    .line 39
    :cond_2
    const-string v0, "Tango.VideomailMessageHandler"

    const-string v1, "handleVideomailError: VideoMailResultEvent invalid."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_0

    .line 41
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VideoMailResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayload;

    .line 44
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayload;->hasReason()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayload;->getReason()Ljava/lang/String;

    move-result-object v1

    .line 46
    :goto_1
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayload;->hasOperation()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 47
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationErrorPayload;->getOperation()Ljava/lang/String;

    move-result-object v0

    .line 50
    const-string v2, "ForwardVideoMail"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 52
    const v2, 0x7f0900bb

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 53
    const/4 v2, 0x1

    .line 60
    :goto_2
    const-string v3, "Tango.VideomailMessageHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleVideomailError: Error received. Operation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " Reason: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 63
    goto :goto_0

    .line 44
    :cond_4
    const-string v1, "UNKNOWN"

    goto :goto_1

    .line 56
    :cond_5
    const-string v0, "UNKNOWN"

    move v2, v3

    goto :goto_2

    :cond_6
    move v2, v3

    goto :goto_2

    .line 34
    :pswitch_data_0
    .packed-switch 0x8953
        :pswitch_0
    .end packed-switch
.end method
