.class public Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;
.super Ljava/lang/Object;
.source "VideomailSharedPreferences.java"


# static fields
.field private static final FORCE_VIDEOMAIL_REPLAY_DEFAULT:Z = false

.field private static final KEY_FORCE_VIDEOMAIL_REPLAY:Ljava/lang/String; = "KEY_FORCE_VIDEOMAIL_REPLAY"

.field private static final KEY_SHOW_SEND_BY_SMS_WARNING_DIALOG:Ljava/lang/String; = "SHOW_SEND_BY_SMS_WARNING_DIALOG"

.field private static final SHARED_PREFERENCES_NAME:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.VideomailSharedPreferences"

.field private static final SHOW_SEND_BY_SMS_WARNING_DIALOG_DEFAULT:Z = true


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getEditor(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;
    .locals 1
    .parameter

    .prologue
    .line 31
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    return-object v0
.end method

.method public static getForceVideomailReplay(Landroid/content/Context;)Z
    .locals 3
    .parameter

    .prologue
    .line 66
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "KEY_FORCE_VIDEOMAIL_REPLAY"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2
    .parameter

    .prologue
    .line 26
    const-string v0, "com.sgiggle.production.screens.videomail.VideomailSharedPreferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public static getShowSendBySmsWarningDialog(Landroid/content/Context;)Z
    .locals 3
    .parameter

    .prologue
    .line 48
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->getSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "SHOW_SEND_BY_SMS_WARNING_DIALOG"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static setForceVideomailReplay(Landroid/content/Context;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->getEditor(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KEY_FORCE_VIDEOMAIL_REPLAY"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 59
    return-void
.end method

.method public static setShowSendBySmsWarningDialog(Landroid/content/Context;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSharedPreferences;->getEditor(Landroid/content/Context;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SHOW_SEND_BY_SMS_WARNING_DIALOG"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 41
    return-void
.end method
