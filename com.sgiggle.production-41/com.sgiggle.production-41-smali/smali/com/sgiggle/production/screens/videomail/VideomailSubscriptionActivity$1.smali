.class Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;
.super Landroid/os/Handler;
.source "VideomailSubscriptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 73
    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    .line 74
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/adapter/PaymentAdapter;->clear()V

    .line 76
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 79
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/Product;

    .line 80
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sgiggle/production/adapter/PaymentAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 84
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_2

    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->showDialog(I)V

    .line 92
    :cond_1
    :goto_1
    return-void

    .line 86
    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isDialogBillingNotSupportedShowing:Z
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$100(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #setter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isDialogBillingNotSupportedShowing:Z
    invoke-static {v0, v2}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$102(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Z)Z

    .line 89
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->showDialog(I)V

    goto :goto_1
.end method
