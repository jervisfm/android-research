.class Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;
.super Ljava/lang/Object;
.source "VideomailSubscriptionActivity.java"

# interfaces
.implements Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BillingPurchaseObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V

    return-void
.end method


# virtual methods
.method public onBillingSupported(Z)V
    .locals 2
    .parameter

    .prologue
    .line 100
    if-eqz p1, :cond_1

    .line 101
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->restoreDatabase()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$300(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V

    .line 110
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->enableAllItems(Z)V

    .line 111
    return-void

    .line 103
    :cond_1
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Billing is not supported"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 145
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Purchase response received"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    sget-object v0, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne p2, v0, :cond_1

    .line 147
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Purchase was successfully sent to the server"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 149
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_productId:Ljava/lang/String;

    sget-object v2, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PENDING:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/adapter/PaymentAdapter;->updateItemStatus(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)Lcom/sgiggle/production/model/Product;

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    sget-object v0, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_USER_CANCELED:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne p2, v0, :cond_2

    .line 152
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Purchase cancelled by user"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 154
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_productId:Ljava/lang/String;

    sget-object v2, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/adapter/PaymentAdapter;->updateItemStatus(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)Lcom/sgiggle/production/model/Product;

    goto :goto_0

    .line 157
    :cond_2
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Purchase failed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 159
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    iget-object v1, p1, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_productId:Ljava/lang/String;

    sget-object v2, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/adapter/PaymentAdapter;->updateItemStatus(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)Lcom/sgiggle/production/model/Product;

    .line 162
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 164
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onResponseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 171
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Restore Transactions response received"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    sget-object v0, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne p2, v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 176
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 177
    const-string v1, "db_initialized"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 178
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 179
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Completed restoring transactions"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    const-string v0, "Tango.VideomailSubscriptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Restore Transactions Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public postPurchaseStateChange(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$500(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$500(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    move-result-object v0

    invoke-virtual {v0, p2, p1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->setParams(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$500(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 136
    :cond_0
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne p1, v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->finish()V

    .line 141
    :cond_1
    :goto_0
    return-void

    .line 138
    :cond_2
    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->CANCELED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/sgiggle/production/payments/Constants$PurchaseState;->REFUNDED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne p1, v0, :cond_1

    goto :goto_0
.end method

.method public startBuyPageActivity(Landroid/app/PendingIntent;Landroid/content/Intent;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 115
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Launching purchase activity"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->skipWelcomePageOnce()V

    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :goto_0
    return-void

    .line 120
    :catch_0
    move-exception v0

    .line 121
    const-string v1, "Tango.VideomailSubscriptionActivity"

    const-string v2, "Error starting activity"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
