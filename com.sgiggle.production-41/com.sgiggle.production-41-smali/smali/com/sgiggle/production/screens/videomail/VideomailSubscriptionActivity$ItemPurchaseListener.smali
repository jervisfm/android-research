.class Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;
.super Ljava/lang/Object;
.source "VideomailSubscriptionActivity.java"

# interfaces
.implements Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemPurchaseListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 187
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V

    return-void
.end method


# virtual methods
.method public onPurchase(Lcom/sgiggle/production/model/Product;)V
    .locals 3
    .parameter

    .prologue
    .line 190
    const-string v0, "Tango.VideomailSubscriptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Purchase item: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/Product;->getProductName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MarketId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/Product;->getProductMarketId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$VMailAttemptPurchaseTrackingMessage;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/Product;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$VMailAttemptPurchaseTrackingMessage;-><init>(Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 195
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/service/BillingService;

    move-result-object v0

    invoke-virtual {p1}, Lcom/sgiggle/production/model/Product;->getProductMarketId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/service/BillingService;->requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Unable to purchase. Check if billing is supported"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-void
.end method
