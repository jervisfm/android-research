.class Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;
.super Ljava/lang/Object;
.source "VideomailSubscriptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateItems"
.end annotation


# instance fields
.field private m_itemId:Ljava/lang/String;

.field private m_state:Lcom/sgiggle/production/payments/Constants$PurchaseState;

.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 438
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 438
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 448
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 449
    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->m_itemId:Ljava/lang/String;

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->m_state:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/production/adapter/PaymentAdapter;->updateItemStatus(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)Lcom/sgiggle/production/model/Product;

    .line 451
    :cond_0
    return-void
.end method

.method public setParams(Ljava/lang/String;Lcom/sgiggle/production/payments/Constants$PurchaseState;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 443
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->m_itemId:Ljava/lang/String;

    .line 444
    iput-object p2, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;->m_state:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    .line 445
    return-void
.end method
