.class public Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "VideomailSubscriptionActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;,
        Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;,
        Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;
    }
.end annotation


# static fields
.field private static final DB_INITIALIZED:Ljava/lang/String; = "db_initialized"

.field private static final DIALOG_BILLING_NOT_SUPPORTED:I = 0x1

.field private static final DIALOG_PURCHASE_FAILED:I = 0x0

.field public static final KEY_BUNDLE:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.BUNDLE"

.field private static final MSG_SHOW_DIALOG_BILLING_NOT_SUPPORTED:I = 0x2

.field private static final MSG_SHOW_DIALOG_PURCHASE_FAILED:I = 0x1

.field private static final MSG_UPDATE_PRODUCT_CATALOG:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.VideomailSubscriptionActivity"

.field private static m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

.field private static m_catalog:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/model/Product;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_billingService:Lcom/sgiggle/production/service/BillingService;

.field private m_handler:Landroid/os/Handler;

.field private m_isBillingServiceBound:Z

.field private m_isDialogBillingNotSupportedShowing:Z

.field private m_observer:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;

.field private m_purchaseDatabase:Lcom/sgiggle/production/database/PurchaseDatabase;

.field private m_termosOfService:Landroid/widget/TextView;

.field private m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_catalog:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isDialogBillingNotSupportedShowing:Z

    .line 71
    new-instance v0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    .line 94
    new-instance v0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    .line 438
    return-void
.end method

.method static synthetic access$000()Lcom/sgiggle/production/adapter/PaymentAdapter;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isDialogBillingNotSupportedShowing:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isDialogBillingNotSupportedShowing:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->restoreDatabase()V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_updateItems:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$UpdateItems;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;)Lcom/sgiggle/production/service/BillingService;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    return-object v0
.end method

.method private static getProductCatalog(Lcom/sgiggle/messaging/Message;)Ljava/util/ArrayList;
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sgiggle/messaging/Message;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/model/Product;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 317
    if-nez p0, :cond_0

    .line 318
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Event is null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 364
    :goto_0
    return-object v0

    .line 322
    :cond_0
    instance-of v0, p0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductCatalogEvent;

    if-nez v0, :cond_1

    .line 323
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Event is not instance of DisplayProductCatalogEvent"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 324
    goto :goto_0

    .line 327
    :cond_1
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductCatalogEvent;

    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayProductCatalogEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;

    .line 329
    if-nez v0, :cond_2

    .line 330
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Payload is null"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 331
    goto :goto_0

    .line 334
    :cond_2
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryCount()I

    move-result v1

    .line 336
    const-string v2, "Tango.VideomailSubscriptionActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Received "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " products to sell"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 338
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 339
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogPayload;->getEntryList()Ljava/util/List;

    move-result-object v3

    .line 341
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v1, :cond_3

    .line 342
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    .line 344
    new-instance v5, Lcom/sgiggle/production/model/Product;

    invoke-direct {v5}, Lcom/sgiggle/production/model/Product;-><init>()V

    .line 345
    sget-object v6, Lcom/sgiggle/production/model/Product$Managed;->MANAGED:Lcom/sgiggle/production/model/Product$Managed;

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setManaged(Lcom/sgiggle/production/model/Product$Managed;)V

    .line 347
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductMarketId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setProductMarketId(Ljava/lang/String;)V

    .line 348
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sgiggle/production/model/Product;->setProductId(J)V

    .line 349
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getSKU()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setSku(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setProductName(Ljava/lang/String;)V

    .line 351
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getProductDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setProductDescription(Ljava/lang/String;)V

    .line 352
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategoryKey()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setCategoryKey(Ljava/lang/String;)V

    .line 353
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getCategory()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setCategory(Ljava/lang/String;)V

    .line 354
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getMarketId()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setMarketId(I)V

    .line 355
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getBeginTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sgiggle/production/model/Product;->setBeginTime(J)V

    .line 356
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getEndTime()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sgiggle/production/model/Product;->setEndTime(J)V

    .line 357
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getLeaseDuration()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setLeaseDuration(I)V

    .line 358
    sget-object v6, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASE:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    invoke-virtual {v5, v6}, Lcom/sgiggle/production/model/Product;->setPurchaseState(Lcom/sgiggle/production/payments/Constants$PurchaseState;)V

    .line 359
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getPrice()Lcom/sgiggle/xmpp/SessionMessages$Price;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/sgiggle/production/model/Product;->setPrice(Lcom/sgiggle/xmpp/SessionMessages$Price;)V

    .line 361
    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 364
    goto/16 :goto_0
.end method

.method private restoreDatabase()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 393
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 394
    const-string v1, "db_initialized"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 395
    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService;->restoreTransactions()Z

    .line 398
    :cond_0
    return-void
.end method

.method public static updateProductCatalog(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 368
    const-string v0, "Tango.VideomailSubscriptionActivity"

    const-string v1, "Add products in to adapter"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->getProductCatalog(Lcom/sgiggle/messaging/Message;)Ljava/util/ArrayList;

    move-result-object v0

    .line 372
    if-eqz v0, :cond_1

    .line 373
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_catalog:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 374
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_catalog:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 376
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_catalog:Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 378
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    if-eqz v1, :cond_0

    .line 379
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    invoke-virtual {v1}, Lcom/sgiggle/production/adapter/PaymentAdapter;->notifyDataSetChanged()V

    .line 381
    :cond_0
    const-string v1, "Tango.VideomailSubscriptionActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " products"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_1
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_termosOfService:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    .line 458
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 459
    const-string v1, "http://www.tango.me/terms-of-use/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 460
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->startActivity(Landroid/content/Intent;)V

    .line 462
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 212
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 214
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->requestWindowFeature(I)Z

    .line 216
    const v0, 0x7f03006d

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->setContentView(I)V

    .line 218
    const v0, 0x7f0a017a

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_termosOfService:Landroid/widget/TextView;

    .line 219
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_termosOfService:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    new-instance v0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;

    invoke-direct {v0, p0, v4}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_observer:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;

    .line 225
    new-instance v0, Lcom/sgiggle/production/database/PurchaseDatabase;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/database/PurchaseDatabase;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_purchaseDatabase:Lcom/sgiggle/production/database/PurchaseDatabase;

    .line 228
    const v0, 0x7f0a0179

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 230
    new-instance v1, Lcom/sgiggle/production/adapter/PaymentAdapter;

    sget-object v2, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_catalog:Ljava/util/ArrayList;

    new-instance v3, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;

    invoke-direct {v3, p0, v4}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$ItemPurchaseListener;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$1;)V

    invoke-direct {v1, p0, v2, v3}, Lcom/sgiggle/production/adapter/PaymentAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sgiggle/production/adapter/PaymentAdapter$PurchaseListener;)V

    sput-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    .line 231
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 234
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 305
    packed-switch p1, :pswitch_data_0

    .line 311
    const-string v0, "Tango.VideomailSubscriptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to create dialog. Invalid ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 307
    :pswitch_0
    invoke-static {p0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getPurchaseFailedDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 309
    :pswitch_1
    invoke-static {p0}, Lcom/sgiggle/production/payments/PurchaseUtils;->getBillingNotSupportedDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 305
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_purchaseDatabase:Lcom/sgiggle/production/database/PurchaseDatabase;

    invoke-virtual {v0}, Lcom/sgiggle/production/database/PurchaseDatabase;->close()V

    .line 269
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 271
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    .line 275
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_observer:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->unregisterObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)Z

    .line 276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    .line 278
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 279
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 253
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 255
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductCatalogFinishedMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$ProductCatalogFinishedMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 258
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    .line 263
    :cond_0
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 283
    check-cast p2, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;

    invoke-virtual {p2}, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;->getService()Lcom/sgiggle/production/service/BillingService;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    .line 285
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService;->checkBillingSupported()Z

    move-result v0

    .line 287
    const-string v1, "Tango.VideomailSubscriptionActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Billing Supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 292
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 295
    :cond_0
    sget-object v1, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_adapter:Lcom/sgiggle/production/adapter/PaymentAdapter;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/adapter/PaymentAdapter;->enableAllItems(Z)V

    .line 296
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .parameter

    .prologue
    .line 300
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_billingService:Lcom/sgiggle/production/service/BillingService;

    .line 301
    return-void
.end method

.method protected onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 238
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onStart()V

    .line 241
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_observer:Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity$BillingPurchaseObserver;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/payments/ResponseHandler;->registerObserver(Lcom/sgiggle/production/payments/ResponseHandler$PurchaseObserver;)V

    .line 244
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sgiggle/production/service/BillingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0, p0, v2}, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 248
    :cond_0
    iput-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailSubscriptionActivity;->m_isBillingServiceBound:Z

    .line 249
    return-void
.end method
