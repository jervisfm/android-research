.class Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;
.super Landroid/os/Handler;
.source "VideomailUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MainHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V
    .locals 0
    .parameter

    .prologue
    .line 270
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter

    .prologue
    const/16 v1, 0x1388

    .line 273
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 286
    :pswitch_0
    const-string v0, "VideomailUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unhandled message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :goto_0
    return-void

    .line 276
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$112(Lcom/sgiggle/production/screens/videomail/VideomailUploader;I)I

    .line 277
    new-instance v0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;
    invoke-static {v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$200(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 281
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$412(Lcom/sgiggle/production/screens/videomail/VideomailUploader;I)I

    .line 282
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendUploadMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$500(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    goto :goto_0

    .line 273
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
