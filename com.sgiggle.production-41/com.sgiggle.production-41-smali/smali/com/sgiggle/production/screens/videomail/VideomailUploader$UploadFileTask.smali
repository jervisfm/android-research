.class Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;
.super Landroid/os/AsyncTask;
.source "VideomailUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UploadFileTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final RESULT_CODE_BAD_RESPONSE_ERROR:I

.field final RESULT_CODE_FILE_DOES_NOT_EXIST:I

.field final RESULT_CODE_INVALID_URL_ERROR:I

.field final RESULT_CODE_NETWORK_ERROR:I

.field final RESULT_CODE_OK:I

.field final RESULT_CODE_UNKNOWN_ERROR:I

.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V
    .locals 1
    .parameter

    .prologue
    .line 296
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 297
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_NETWORK_ERROR:I

    .line 298
    const/4 v0, -0x2

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_INVALID_URL_ERROR:I

    .line 299
    const/4 v0, -0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_BAD_RESPONSE_ERROR:I

    .line 300
    const/4 v0, -0x4

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_UNKNOWN_ERROR:I

    .line 301
    const/4 v0, -0x5

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_FILE_DOES_NOT_EXIST:I

    .line 302
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->RESULT_CODE_OK:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 296
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Integer;
    .locals 6
    .parameter

    .prologue
    const/4 v5, -0x5

    const/4 v4, -0x4

    const/4 v1, 0x0

    .line 313
    aget-object v0, p1, v1

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    aget-object v1, p1, v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 314
    :cond_0
    const-string v0, "VideomailUploader"

    const-string v1, "UploadFileTask: error uploading file: file does not exist"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 357
    :goto_0
    return-object v0

    .line 318
    :cond_1
    new-instance v0, Lcom/sgiggle/production/network/TangoHttpClient;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_context:Landroid/content/Context;
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$700(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sgiggle/production/network/tango/TangoHttpConfiguration;

    invoke-direct {v2}, Lcom/sgiggle/production/network/tango/TangoHttpConfiguration;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/network/TangoHttpClient;-><init>(Landroid/content/Context;Lcom/sgiggle/production/network/HttpConfiguration;)V

    .line 321
    new-instance v1, Lcom/sgiggle/production/network/executor/RemoteExecutor;

    invoke-direct {v1, v0}, Lcom/sgiggle/production/network/executor/RemoteExecutor;-><init>(Lorg/apache/http/client/HttpClient;)V

    .line 322
    new-instance v0, Lcom/sgiggle/production/network/RequestFactory;

    invoke-direct {v0}, Lcom/sgiggle/production/network/RequestFactory;-><init>()V

    .line 325
    :try_start_0
    new-instance v2, Lcom/sgiggle/production/network/command/UploadCommand;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-direct {v2, v3}, Lcom/sgiggle/production/network/command/UploadCommand;-><init>(Ljava/lang/String;)V

    .line 328
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$800(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$800(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 329
    :cond_2
    const/4 v0, -0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 332
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailUrl:Ljava/lang/String;
    invoke-static {v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$800(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/network/command/UploadCommand;->setUrl(Ljava/lang/String;)V

    .line 333
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/network/command/UploadCommand;->setProgressListener(Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;)V

    .line 334
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/network/RequestFactory;->newHttpRequest(Lcom/sgiggle/production/network/command/Command;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    #setter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v3, v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$902(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    .line 336
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$900(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    new-instance v2, Lcom/sgiggle/production/network/command/handler/VideomailHandler;

    invoke-direct {v2}, Lcom/sgiggle/production/network/command/handler/VideomailHandler;-><init>()V

    invoke-virtual {v1, v0, v2}, Lcom/sgiggle/production/network/executor/RemoteExecutor;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lcom/sgiggle/production/network/command/handler/CommandHandler;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/model/VideomailResponse;

    .line 339
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/sgiggle/production/model/VideomailResponse;->getCode()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_4

    .line 341
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 343
    :cond_4
    const-string v1, "VideomailUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UploadFileTask: error uploading file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/model/VideomailResponse;->getErrorMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v0, -0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/sgiggle/production/network/command/handler/HandlerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/apache/http/MethodNotSupportedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    goto/16 :goto_0

    .line 346
    :catch_0
    move-exception v0

    .line 347
    const-string v1, "VideomailUploader"

    const-string v2, "UploadFileTask: error uploading file"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 348
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 349
    :catch_1
    move-exception v0

    .line 350
    const-string v1, "VideomailUploader"

    const-string v2, "UploadFileTask: error uploading file"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 351
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 352
    :catch_2
    move-exception v0

    .line 353
    const-string v1, "VideomailUploader"

    const-string v2, "UploadFileTask: error uploading file"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 354
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 355
    :catch_3
    move-exception v0

    .line 356
    const-string v1, "VideomailUploader"

    const-string v2, "UploadFileTask: error uploading file"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 357
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 296
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 363
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1000(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 398
    :cond_0
    :goto_0
    return-void

    .line 367
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 390
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #setter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z
    invoke-static {v0, v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1002(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Z)Z

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendFinishUploadMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    .line 392
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadFailedFileNotFound()V

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->detachListener()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1200(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    goto :goto_0

    .line 369
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #setter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z
    invoke-static {v0, v3}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1002(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Z)Z

    .line 371
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendFinishUploadMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    .line 372
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadSuccess()V

    .line 375
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #calls: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->detachListener()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1200(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V

    goto :goto_0

    .line 382
    :pswitch_2
    const-string v0, "VideomailUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UploadFileTask: error during upload. Result code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$1300(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 385
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I
    invoke-static {v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)I

    move-result v1

    div-int/lit16 v1, v1, 0x3e8

    invoke-interface {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadWillRetry(I)V

    goto/16 :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch -0x5
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 296
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->this$0:Lcom/sgiggle/production/screens/videomail/VideomailUploader;

    #getter for: Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadPreExecute()V

    .line 309
    :cond_0
    return-void
.end method
