.class public interface abstract Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
.super Ljava/lang/Object;
.source "VideomailUploader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/VideomailUploader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VideomailUploaderListener"
.end annotation


# virtual methods
.method public abstract onUploadDetached()V
.end method

.method public abstract onUploadFailedFileNotFound()V
.end method

.method public abstract onUploadPreExecute()V
.end method

.method public abstract onUploadProgress(I)V
.end method

.method public abstract onUploadSuccess()V
.end method

.method public abstract onUploadVideoMailIdReceived(Ljava/lang/String;)V
.end method

.method public abstract onUploadWillRetry(I)V
.end method
