.class public Lcom/sgiggle/production/screens/videomail/VideomailUploader;
.super Ljava/lang/Object;
.source "VideomailUploader.java"

# interfaces
.implements Lcom/sgiggle/production/screens/videomail/VideomailStateMachineUploadListener;
.implements Lcom/sgiggle/production/network/CountingMultipartEntity$ProgressListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;,
        Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;,
        Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;,
        Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    }
.end annotation


# static fields
.field private static final MSG_RETRY_UPLOAD:I = 0x1

.field private static final MSG_RETRY_UPLOAD_TOKEN:I = 0x3

.field private static final TAG:Ljava/lang/String; = "VideomailUploader"

.field private static final UPLOAD_RETRY_BACKOFF:I = 0x1388


# instance fields
.field private m_context:Landroid/content/Context;

.field private m_conversationId:Ljava/lang/String;

.field private m_duration:I

.field private m_fileSize:J

.field private m_filename:Ljava/lang/String;

.field private final m_handler:Landroid/os/Handler;

.field private m_isFinished:Z

.field private m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

.field private m_retryTimeout:I

.field private m_retryUploadTokenTimeout:I

.field private m_rotationHint:I

.field private m_thumbnailFilename:Ljava/lang/String;

.field private m_uploadVideoMailMessage:Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

.field private m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

.field private m_videomailId:Ljava/lang/String;

.field private m_videomailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;JJIZLcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;)V
    .locals 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;I",
            "Ljava/lang/String;",
            "JJIZ",
            "Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v3, 0x0

    iput v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I

    .line 58
    const/4 v3, 0x0

    iput v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryUploadTokenTimeout:I

    .line 67
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    .line 72
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_fileSize:J

    .line 73
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_conversationId:Ljava/lang/String;

    .line 74
    const/4 v3, 0x0

    iput v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_duration:I

    .line 76
    new-instance v3, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$MainHandler;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;)V

    iput-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;

    .line 151
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_context:Landroid/content/Context;

    .line 152
    move-object/from16 v0, p2

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_conversationId:Ljava/lang/String;

    .line 153
    move-object/from16 v0, p3

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    .line 154
    move-object/from16 v0, p4

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_thumbnailFilename:Ljava/lang/String;

    .line 155
    move/from16 v0, p12

    move-object v1, p0

    iput v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_rotationHint:I

    .line 156
    move/from16 v0, p6

    move-object v1, p0

    iput v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_duration:I

    .line 157
    move-object/from16 v0, p14

    move-object v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    .line 158
    move-wide/from16 v0, p8

    move-object v2, p0

    iput-wide v0, v2, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_fileSize:J

    .line 161
    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    sget-object v5, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iget-object v6, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    iget-object v7, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_thumbnailFilename:Ljava/lang/String;

    move-wide/from16 v0, p8

    long-to-int v0, v0

    move v8, v0

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v13

    move-object/from16 v4, p2

    move/from16 v9, p6

    move-wide/from16 v10, p10

    move/from16 v12, p12

    invoke-direct/range {v3 .. v13}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;Ljava/lang/String;IIJIZ)V

    iput-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uploadVideoMailMessage:Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    .line 172
    const-string v3, "VideomailUploader"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "rotationHint="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, v4

    move/from16 v1, p12

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isFlipped="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, v4

    move/from16 v1, p13

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)I
    .locals 1
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I

    return v0
.end method

.method static synthetic access$1000(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Z
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendFinishUploadMessage()V

    return-void
.end method

.method static synthetic access$112(Lcom/sgiggle/production/screens/videomail/VideomailUploader;I)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryTimeout:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->detachListener()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$412(Lcom/sgiggle/production/screens/videomail/VideomailUploader;I)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryUploadTokenTimeout:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryUploadTokenTimeout:I

    return v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendUploadMessage()V

    return-void
.end method

.method static synthetic access$600(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sgiggle/production/screens/videomail/VideomailUploader;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    return-object p1
.end method

.method private detachListener()V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    .line 481
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    .line 482
    invoke-interface {v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadDetached()V

    .line 484
    :cond_0
    return-void
.end method

.method public static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 217
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 218
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 219
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 220
    int-to-float v3, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 221
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, p0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 222
    return-object v0
.end method

.method private sendFinishUploadMessage()V
    .locals 6

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 452
    :goto_0
    return-void

    .line 438
    :cond_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_conversationId:Ljava/lang/String;

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_fileSize:J

    long-to-int v4, v4

    iget v5, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_duration:I

    invoke-direct/range {v0 .. v5}, Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;-><init>(Ljava/lang/String;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;Ljava/lang/String;II)V

    .line 445
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 449
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishUploadVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishUploadVideoMailMessage;-><init>(Ljava/lang/String;)V

    .line 451
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private sendUploadMessage()V
    .locals 3

    .prologue
    .line 425
    const-string v0, "VideomailUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uploadFile(): Posting message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uploadVideoMailMessage:Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uploadVideoMailMessage:Lcom/sgiggle/media_engine/MediaEngineMessage$SendConversationMessageMessage;

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 427
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 237
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    .line 240
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 241
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 243
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    if-eqz v0, :cond_1

    .line 249
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_uriRequest:Lorg/apache/http/client/methods/HttpUriRequest;

    invoke-interface {v0}, Lorg/apache/http/client/methods/HttpUriRequest;->abort()V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->detachListener()V

    goto :goto_0

    .line 250
    :catch_0
    move-exception v0

    .line 251
    const-string v1, "VideomailUploader"

    const-string v2, "Exception while aborting request: "

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isFinished()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    return v0
.end method

.method public onUploadTokenReceived(Lcom/sgiggle/messaging/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 407
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailResponseEvent;

    .line 409
    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    if-nez v0, :cond_1

    .line 410
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailResponseEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailUrl:Ljava/lang/String;

    .line 411
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$UploadVideoMailResponseEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$UploadVideoMailResponsePayload;->getVideoMailId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    .line 413
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_videomailId:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadVideoMailIdReceived(Ljava/lang/String;)V

    .line 417
    :cond_0
    new-instance v0, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;-><init>(Lcom/sgiggle/production/screens/videomail/VideomailUploader;Lcom/sgiggle/production/screens/videomail/VideomailUploader$1;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$UploadFileTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 419
    :cond_1
    return-void
.end method

.method public onVideomailErrorEvent(Lcom/sgiggle/messaging/Message;)V
    .locals 5
    .parameter

    .prologue
    .line 457
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryUploadTokenTimeout:I

    div-int/lit16 v0, v0, 0x3e8

    .line 458
    const-string v1, "VideomailUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to retrieve upload token. Retry in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seconds..."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_handler:Landroid/os/Handler;

    const/4 v2, 0x3

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_retryUploadTokenTimeout:I

    int-to-long v3, v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 461
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    if-eqz v1, :cond_0

    .line 462
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    invoke-interface {v1, v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadWillRetry(I)V

    .line 464
    :cond_0
    return-void
.end method

.method public start()Z
    .locals 4

    .prologue
    .line 182
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_isFinished:Z

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "VideomailUploader"

    const-string v1, "Uploader did not start, it\'s already been used. Create a new object an try again."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    const/4 v0, 0x0

    .line 209
    :goto_0
    return v0

    .line 187
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_2

    .line 188
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_3

    .line 191
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_rotationHint:I

    if-lez v1, :cond_1

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v1

    if-nez v1, :cond_1

    .line 192
    const-string v1, "VideomailUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Rotate bitmap for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_rotationHint:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    iget v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_rotationHint:I

    invoke-static {v0, v1}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 196
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_thumbnailFilename:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 197
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->sendUploadMessage()V

    .line 209
    const/4 v0, 0x1

    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    const-string v0, "VideomailUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error saving video thumbnail at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_thumbnailFilename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 203
    :cond_3
    const-string v0, "VideomailUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to generate thumbnail from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public transferred(J)V
    .locals 4
    .parameter

    .prologue
    .line 468
    long-to-float v0, p1

    iget-wide v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_fileSize:J

    long-to-float v1, v1

    div-float/2addr v0, v1

    const/high16 v1, 0x42c8

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f00

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 469
    const-string v1, "VideomailUploader"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bytes: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\tpercentage: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 470
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    if-eqz v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/VideomailUploader;->m_listener:Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;

    invoke-interface {v1, v0}, Lcom/sgiggle/production/screens/videomail/VideomailUploader$VideomailUploaderListener;->onUploadProgress(I)V

    .line 473
    :cond_0
    return-void
.end method
