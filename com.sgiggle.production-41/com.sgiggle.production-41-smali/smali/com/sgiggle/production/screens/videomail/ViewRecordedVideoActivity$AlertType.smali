.class final enum Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;
.super Ljava/lang/Enum;
.source "ViewRecordedVideoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AlertType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

.field public static final enum FILE_UPLOADER:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

.field public static final enum MAX_DURATION:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

.field public static final enum NONE:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

.field public static final enum ORIENTATION_WARNING:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->NONE:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    .line 67
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    const-string v1, "FILE_UPLOADER"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->FILE_UPLOADER:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    .line 68
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    const-string v1, "MAX_DURATION"

    invoke-direct {v0, v1, v4}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->MAX_DURATION:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    .line 69
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    const-string v1, "ORIENTATION_WARNING"

    invoke-direct {v0, v1, v5}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->ORIENTATION_WARNING:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    .line 65
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->NONE:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->FILE_UPLOADER:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->MAX_DURATION:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->ORIENTATION_WARNING:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->$VALUES:[Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;
    .locals 1
    .parameter

    .prologue
    .line 65
    const-class v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->$VALUES:[Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-virtual {v0}, [Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    return-object v0
.end method
