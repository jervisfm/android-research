.class public Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;
.super Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;
.source "ViewRecordedVideoActivity.java"

# interfaces
.implements Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$1;,
        Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;
    }
.end annotation


# static fields
.field public static final EXTRA_DURATION:Ljava/lang/String; = "duration"

.field public static final EXTRA_FLIPPED:Ljava/lang/String; = "flipped"

.field public static final EXTRA_MAX_DURATION:Ljava/lang/String; = "maxDuration"

.field public static final EXTRA_MAX_DURATION_REACHED:Ljava/lang/String; = "maxDurationReached"

.field public static final EXTRA_MIME_TYPE:Ljava/lang/String; = "mimeType"

.field public static final EXTRA_RECORD_VIDEOMAIL_PAYLOAD:Ljava/lang/String; = "recordVideoMailPayload"

.field public static final EXTRA_ROTATE_ON_SERVER:Ljava/lang/String; = "rotateOnServer"

.field public static final EXTRA_ROTATION_HINT:Ljava/lang/String; = "rotationHint"

.field public static final EXTRA_TIME_CREATED:Ljava/lang/String; = "timeCreated"

.field public static final EXTRA_VIDEO_FILENAME:Ljava/lang/String; = "videoFilename"

.field public static final EXTRA_VIDEO_THUMBNAIL_FILENAME:Ljava/lang/String; = "videoThumbnailFilename"

.field private static final MAX_VIDEO_PREPARE_RETRY:I = 0x0

.field public static final RESULT_FILE_NOT_FOUND:I = 0x4

.field public static final RESULT_RETAKE:I = 0x2

.field public static final RESULT_SENT_CANCELED:I = 0x3

.field public static final REVIEW_AND_SEND_REQUEST:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ViewRecordedVideoActivity"


# instance fields
.field private m_alertMaxDurationReached:Landroid/widget/LinearLayout;

.field private m_alertOrientationWarning:Landroid/widget/LinearLayout;

.field private m_calleesList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private m_contentLength:J

.field private m_duration:I

.field private m_fileUploadProgress:Landroid/widget/ProgressBar;

.field private m_fileUploader:Landroid/widget/LinearLayout;

.field private m_filename:Ljava/lang/String;

.field private m_isFlipped:Z

.field private m_maxDuration:I

.field private m_maxDurationReached:Z

.field private m_mimeType:Ljava/lang/String;

.field private m_rotateOnServer:Z

.field private m_rotationHint:I

.field private m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

.field private m_showOrientationWarning:Z

.field private m_thumbnailFilename:Ljava/lang/String;

.field private m_timeCreated:J

.field private m_uploadingMessage:Landroid/widget/TextView;

.field private m_videomailSenderLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    .line 51
    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_thumbnailFilename:Ljava/lang/String;

    .line 52
    iput v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_duration:I

    .line 53
    iput v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotationHint:I

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotateOnServer:Z

    .line 55
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_isFlipped:Z

    .line 57
    iput-wide v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_timeCreated:J

    .line 59
    iput v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDuration:I

    .line 60
    iput-wide v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_contentLength:J

    .line 72
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDurationReached:Z

    .line 73
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_videomailSenderLock:Ljava/lang/Object;

    return-void
.end method

.method private cancelSend()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v1

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v0, :cond_0

    .line 326
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->cancel()V

    .line 329
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 330
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/TangoApp;->setSendVideomailActivityHelper(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)V

    .line 331
    monitor-exit v1

    .line 332
    return-void

    .line 331
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private deleteVideoFile()V
    .locals 3

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 349
    :goto_0
    return-void

    .line 343
    :cond_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    const-string v0, "ViewRecordedVideoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "File deleted: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 347
    :cond_1
    const-string v0, "ViewRecordedVideoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not delete (or already deleted): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initAlertMessages()V
    .locals 9

    .prologue
    const v6, 0x7f0a015d

    const v5, 0x7f030062

    const/4 v4, 0x0

    const/16 v8, 0x8

    .line 172
    const v0, 0x7f0a00ac

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 175
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030063

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    .line 176
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v3, -0x2

    invoke-direct {v2, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 178
    const/16 v1, 0xa

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 180
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    const v3, 0x7f0a015f

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploadProgress:Landroid/widget/ProgressBar;

    .line 181
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    const v3, 0x7f0a0160

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_uploadingMessage:Landroid/widget/TextView;

    .line 182
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 183
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    .line 188
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 189
    const v3, 0x7f0900c0

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 191
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 192
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    :cond_0
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDurationReached:Z

    if-eqz v1, :cond_1

    .line 197
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v5, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    .line 198
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 199
    const v3, 0x7f0900b6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDuration:I

    int-to-long v6, v6

    invoke-static {v6, v7}, Lcom/sgiggle/production/widget/Timer;->formatTime(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 202
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 204
    :cond_1
    return-void
.end method

.method private showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V
    .locals 4
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 410
    .line 414
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$1;->$SwitchMap$com$sgiggle$production$screens$videomail$ViewRecordedVideoActivity$AlertType:[I

    invoke-virtual {p1}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move v0, v3

    move v1, v3

    move v2, v3

    .line 434
    :goto_0
    iget-object v3, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploader:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 435
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    .line 436
    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertMaxDurationReached:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 438
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    if-eqz v1, :cond_1

    .line 439
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_alertOrientationWarning:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 441
    :cond_1
    return-void

    :pswitch_0
    move v0, v3

    move v1, v3

    .line 417
    goto :goto_0

    :pswitch_1
    move v0, v3

    move v1, v2

    move v2, v3

    .line 421
    goto :goto_0

    :pswitch_2
    move v0, v2

    move v1, v3

    move v2, v3

    .line 425
    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private startSend()V
    .locals 18

    .prologue
    .line 292
    const-string v2, "ViewRecordedVideoActivity"

    const-string v3, "startSend()"

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_contentLength:J

    move-wide v2, v0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 295
    const-string v2, "ViewRecordedVideoActivity"

    const-string v3, "uploadFile(): contentLength = 0. No need to request for upload."

    invoke-static {v2, v3}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->onNoVideoError()V

    .line 318
    :goto_0
    return-void

    .line 301
    :cond_0
    new-instance v2, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    move-object v4, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_thumbnailFilename:Ljava/lang/String;

    move-object v5, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_calleesList:Ljava/util/List;

    move-object v6, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_duration:I

    move v7, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_mimeType:Ljava/lang/String;

    move-object v8, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_contentLength:J

    move-wide v9, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_timeCreated:J

    move-wide v11, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotateOnServer:Z

    move v3, v0

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotationHint:I

    move v3, v0

    move v13, v3

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_isFlipped:Z

    move v14, v0

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v3, p0

    move-object/from16 v17, p0

    invoke-direct/range {v2 .. v17}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;-><init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ILjava/lang/String;JJIZZZLcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper$SendVideomailActivityHelperListener;)V

    move-object v0, v2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    .line 316
    invoke-virtual/range {p0 .. p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/sgiggle/production/TangoApp;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object v3, v0

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/TangoApp;->setSendVideomailActivityHelper(Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;)V

    .line 317
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    move-object v2, v0

    invoke-virtual {v2}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->send()V

    goto :goto_0

    .line 301
    :cond_1
    const/4 v3, 0x0

    move v13, v3

    goto :goto_1
.end method


# virtual methods
.method public createVideoMediaController()Lcom/sgiggle/production/widget/VideomailMediaController;
    .locals 3

    .prologue
    .line 242
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController;

    const/4 v1, 0x1

    sget-object v2, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    invoke-direct {v0, p0, v1, v2}, Lcom/sgiggle/production/widget/VideomailMediaController;-><init>(Landroid/content/Context;ZLcom/sgiggle/production/widget/VideomailMediaController$Mode;)V

    return-object v0
.end method

.method protected getMaxPrepareRetry()I
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    return v0
.end method

.method protected isForcePreviewEnabled()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method protected isLoadingIndicatorEnabled()Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 152
    const-string v0, "ViewRecordedVideoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult(): requestCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", resultCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v0

    .line 155
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    if-eqz v1, :cond_0

    .line 156
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_sendHelper:Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;

    invoke-virtual {v1, p1, p2, p3}, Lcom/sgiggle/production/screens/videomail/SendVideomailActivityHelper;->handleOnActivityResult(IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 158
    monitor-exit v0

    .line 164
    :goto_0
    return-void

    .line 161
    :cond_0
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->setResult(I)V

    .line 268
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onBackPressed()V

    .line 269
    return-void
.end method

.method public onBeforeSmsDialogShown()V
    .locals 0

    .prologue
    .line 446
    return-void
.end method

.method protected onCancelRequested()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->setResult(I)V

    .line 274
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->finish()V

    .line 275
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const-wide/16 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 90
    invoke-super {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 96
    const-string v1, "videoFilename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    .line 97
    const-string v1, "videoThumbnailFilename"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_thumbnailFilename:Ljava/lang/String;

    .line 98
    const-string v1, "rotateOnServer"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotateOnServer:Z

    .line 99
    const-string v1, "rotationHint"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotationHint:I

    .line 100
    const-string v1, "duration"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_duration:I

    .line 101
    const-string v1, "flipped"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_isFlipped:Z

    .line 102
    const-string v1, "mimeType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_mimeType:Ljava/lang/String;

    .line 103
    const-string v1, "timeCreated"

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_timeCreated:J

    .line 104
    const-string v1, "maxDurationReached"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDurationReached:Z

    .line 105
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDurationReached:Z

    if-eqz v1, :cond_0

    .line 106
    const-string v1, "maxDuration"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDuration:I

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    sput-object v1, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_uri:Ljava/lang/String;

    .line 113
    const-string v1, "recordVideoMailPayload"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;

    .line 116
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$RecordVideoMailPayload;->getCalleesList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_calleesList:Ljava/util/List;

    .line 121
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotationHint:I

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_rotateOnServer:Z

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->isVideomailReplaySupported()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v4

    :goto_0
    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    .line 123
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    if-eqz v0, :cond_1

    .line 124
    invoke-virtual {p0, v4}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->setRequestedOrientation(I)V

    .line 128
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->initAlertMessages()V

    .line 131
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_maxDurationReached:Z

    if-eqz v0, :cond_6

    .line 132
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->MAX_DURATION:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V

    .line 138
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    .line 139
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_receivedUri:Z

    .line 140
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_filename:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 142
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_contentLength:J

    .line 145
    :cond_3
    iget-wide v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_contentLength:J

    cmp-long v0, v0, v5

    if-nez v0, :cond_4

    .line 146
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->onNoVideoError()V

    .line 148
    :cond_4
    return-void

    :cond_5
    move v0, v3

    .line 121
    goto :goto_0

    .line 133
    :cond_6
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    if-eqz v0, :cond_2

    .line 134
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->ORIENTATION_WARNING:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 208
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onDestroy()V

    .line 210
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->cancelSend()V

    .line 212
    return-void
.end method

.method public onDetached(Z)V
    .locals 0
    .parameter

    .prologue
    .line 403
    return-void
.end method

.method protected onMediaPlayerError()V
    .locals 2

    .prologue
    .line 232
    const v0, 0x7f0900bf

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 233
    return-void
.end method

.method protected onPlayRequested()V
    .locals 1

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_showOrientationWarning:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->ORIENTATION_WARNING:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    :goto_0
    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V

    .line 252
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPlayRequested()V

    .line 253
    return-void

    .line 250
    :cond_0
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->NONE:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    goto :goto_0
.end method

.method protected onRetakeRequested()V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->setResult(I)V

    .line 258
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->finish()V

    .line 259
    return-void
.end method

.method protected onSendRequested()V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_videomailSenderLock:Ljava/lang/Object;

    monitor-enter v0

    .line 281
    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->cancelSend()V

    .line 284
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->startSend()V

    .line 285
    monitor-exit v0

    .line 286
    return-void

    .line 285
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public onSendSuccess()V
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->NONE:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V

    .line 390
    return-void
.end method

.method public onSmsDialogDismissed()V
    .locals 0

    .prologue
    .line 451
    return-void
.end method

.method public onUploadFailedFileNotFound()V
    .locals 2

    .prologue
    .line 394
    const-string v0, "ViewRecordedVideoActivity"

    const-string v1, "Upload failed, file not found."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    const v0, 0x7f0900b9

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 396
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->setResult(I)V

    .line 397
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->finish()V

    .line 398
    return-void
.end method

.method public onUploadPreExecute()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_uploadingMessage:Landroid/widget/TextView;

    const v1, 0x7f0900bc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 365
    return-void
.end method

.method public onUploadProgress(I)V
    .locals 3
    .parameter

    .prologue
    .line 369
    const-string v0, "ViewRecordedVideoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upload in progress "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_fileUploadProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 371
    return-void
.end method

.method public onUploadRequested()V
    .locals 2

    .prologue
    .line 353
    const-string v0, "ViewRecordedVideoActivity"

    const-string v1, "Upload about to start..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->setReviewSendState()V

    .line 356
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;->FILE_UPLOADER:Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->showAlert(Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity$AlertType;)V

    .line 358
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_uploadingMessage:Landroid/widget/TextView;

    const v1, 0x7f0900bc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 359
    return-void
.end method

.method public onUploadWillRetry(I)V
    .locals 5
    .parameter

    .prologue
    .line 375
    const-string v0, "ViewRecordedVideoActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Upload failed, will retry in "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "s"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->m_uploadingMessage:Landroid/widget/TextView;

    const v1, 0x7f0900ba

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/screens/videomail/ViewRecordedVideoActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    return-void
.end method

.method protected startPlaybackOnFirstLaunch()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method
