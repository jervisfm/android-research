.class Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;
.super Landroid/os/Handler;
.source "ViewVideoBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    .line 76
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 103
    const-string v0, "Tango.ViewVideoBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 83
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_isDestroyed:Z
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->access$100(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    #getter for: Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->access$200(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 88
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNoVideoError()V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->updateVideomailPlayback()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->access$300(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V

    .line 94
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->access$208(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)I

    goto :goto_0

    .line 99
    :pswitch_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
