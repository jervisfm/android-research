.class Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;
.super Ljava/lang/Object;
.source "ViewVideoBaseActivity.java"

# interfaces
.implements Lcom/sgiggle/production/widget/VideomailMediaController$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControllerCallBack"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 370
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 370
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V

    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onCancelRequested()V

    .line 410
    return-void
.end method

.method public onDeleteMessage()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onDeleteRequested()V

    .line 390
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPauseRequested()V

    .line 375
    return-void
.end method

.method public onPlay()V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPlayRequested()V

    .line 380
    return-void
.end method

.method public onReply()V
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onReplyRequested()V

    .line 385
    return-void
.end method

.method public onRestart()V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onRestartRequested()V

    .line 395
    return-void
.end method

.method public onRetake()V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onRetakeRequested()V

    .line 405
    return-void
.end method

.method public onSend()V
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onSendRequested()V

    .line 400
    return-void
.end method
