.class public abstract Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;
.super Lcom/sgiggle/production/ActivityBase;
.source "ViewVideoBaseActivity.java"

# interfaces
.implements Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;
    }
.end annotation


# static fields
.field private static final CLEAR_SCREEN_DELAY:I = 0x2

.field private static final FETCH_URL_MAX_RETRY_COUNT:I = 0x5

.field public static final KEY_FIRST_LAUNCH:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH"

.field private static final KEY_VIDEO_PLAYING:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING"

.field private static final KEY_VIDEO_POSITION:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION"

.field public static final KEY_VIDEO_SOURCE:Ljava/lang/String; = "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE"

.field private static final MSG_SHOW_ERROR:I = 0x0

.field private static final MSG_UPDATE_VIDEO_PLAYBACK:I = 0x1

.field private static final SCREEN_DELAY:I = 0xea60

.field private static final TAG:Ljava/lang/String; = "Tango.ViewVideoBaseActivity"

.field public static m_receivedUri:Z

.field public static m_uri:Ljava/lang/String;


# instance fields
.field protected m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

.field private m_controllerCallback:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;

.field private m_fetchUrlRetryCount:I

.field private volatile m_firstLaunch:Z

.field private final m_handler:Landroid/os/Handler;

.field private m_isDestroyed:Z

.field private m_loadingIndicator:Landroid/widget/ProgressBar;

.field private m_videoPosition:I

.field protected m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

.field private m_wasPlaying:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_receivedUri:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/ActivityBase;-><init>()V

    .line 53
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    .line 54
    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    .line 64
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_isDestroyed:Z

    .line 65
    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I

    .line 67
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controllerCallback:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;

    .line 74
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$1;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    .line 370
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_isDestroyed:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 30
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I

    return v0
.end method

.method static synthetic access$208(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)I
    .locals 2
    .parameter

    .prologue
    .line 30
    iget v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->updateVideomailPlayback()V

    return-void
.end method

.method private keepScreenOn()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 298
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 299
    return-void
.end method

.method private keepScreenOnAwhile()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 291
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 292
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 293
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    const-wide/32 v1, 0xea60

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 294
    return-void
.end method

.method private resetScreenOn()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 288
    return-void
.end method

.method private updateVideomailPlayback()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 302
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_receivedUri:Z

    if-eqz v0, :cond_5

    .line 303
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 305
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_4

    .line 306
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->getCurrentPosition()I

    move-result v0

    .line 307
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/BetterVideoView;->stopPlayback()V

    .line 310
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    sget-object v2, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 311
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 313
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    if-eqz v0, :cond_2

    .line 314
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->startPlaybackOnFirstLaunch()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->start()V

    .line 322
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    .line 331
    :cond_2
    :goto_1
    return-void

    .line 318
    :cond_3
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->isForcePreviewEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->forcePreview()V

    goto :goto_0

    .line 324
    :cond_4
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 325
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNoVideoError()V

    goto :goto_1

    .line 327
    :cond_5
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_receivedUri:Z

    if-nez v0, :cond_2

    .line 329
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method


# virtual methods
.method public abstract createVideoMediaController()Lcom/sgiggle/production/widget/VideomailMediaController;
.end method

.method protected abstract getMaxPrepareRetry()I
.end method

.method protected isForcePreviewEnabled()Z
    .locals 1

    .prologue
    .line 358
    const/4 v0, 0x0

    return v0
.end method

.method protected isLoadingIndicatorEnabled()Z
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x1

    return v0
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 282
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onBackPressed()V

    .line 283
    return-void
.end method

.method protected onCancelRequested()V
    .locals 2

    .prologue
    .line 479
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onCancelRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    return-void
.end method

.method public onCompletion()V
    .locals 0

    .prologue
    .line 513
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .parameter

    .prologue
    .line 190
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->pause()V

    .line 196
    :cond_0
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 205
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->requestWindowFeature(I)Z

    .line 115
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 116
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x800

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 118
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onCreate(Landroid/os/Bundle;)V

    .line 120
    iput v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_fetchUrlRetryCount:I

    .line 122
    const v0, 0x7f030068

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->setContentView(I)V

    .line 125
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->createVideoMediaController()Lcom/sgiggle/production/widget/VideomailMediaController;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

    .line 127
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/VideomailMediaController;->setEnabled(Z)V

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controllerCallback:Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity$ControllerCallBack;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/VideomailMediaController;->setCallback(Lcom/sgiggle/production/widget/VideomailMediaController$Callback;)V

    .line 131
    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/BetterVideoView;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    .line 132
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_controller:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideomailMediaController(Lcom/sgiggle/production/widget/VideomailMediaController;)V

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getMaxPrepareRetry()I

    move-result v1

    invoke-virtual {v0, p0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->init(Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;I)V

    .line 135
    const v0, 0x7f0a0111

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_loadingIndicator:Landroid/widget/ProgressBar;

    .line 137
    if-eqz p1, :cond_0

    .line 140
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    .line 141
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 146
    :goto_0
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    .line 147
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 148
    const-string v1, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    .line 150
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 152
    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->start()V

    .line 158
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->setVolumeControlStream(I)V

    .line 160
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 161
    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setSpeakerphoneOn(Z)V

    .line 162
    return-void

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNoVideoError()V

    goto :goto_0
.end method

.method protected onDeleteRequested()V
    .locals 2

    .prologue
    .line 442
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onDeleteRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onDestroy()V

    .line 267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_isDestroyed:Z

    .line 269
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    .line 270
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_receivedUri:Z

    .line 271
    return-void
.end method

.method public onError()V
    .locals 3

    .prologue
    .line 497
    const-string v0, "Tango.ViewVideoBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "An error occured playing URI: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onMediaPlayerError()V

    .line 499
    return-void
.end method

.method protected abstract onMediaPlayerError()V
.end method

.method protected onNoVideoError()V
    .locals 2

    .prologue
    .line 366
    const v0, 0x7f0900be

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 367
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->finish()V

    .line 368
    return-void
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 243
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onPause()V

    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    .line 246
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    .line 249
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "Release media resources"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->stopPlayback()V

    .line 252
    const-string v0, "Tango.ViewVideoBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPause :::: video playback state :::: was playing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->resetScreenOn()V

    .line 256
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_isDestroyed:Z

    .line 259
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    .line 260
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_receivedUri:Z

    .line 262
    :cond_0
    return-void
.end method

.method protected onPauseRequested()V
    .locals 2

    .prologue
    .line 417
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onPauseRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->keepScreenOnAwhile()V

    .line 419
    return-void
.end method

.method protected onPlayRequested()V
    .locals 2

    .prologue
    .line 425
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onPlayRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->keepScreenOn()V

    .line 427
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->pause()V

    .line 170
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPauseRequested()V

    .line 172
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/ActivityBase;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 173
    return-void
.end method

.method public onPrepared()V
    .locals 2

    .prologue
    .line 503
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->isLoadingIndicatorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_loadingIndicator:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 506
    :cond_0
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onPrepared()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 508
    return-void
.end method

.method protected onReplyRequested()V
    .locals 2

    .prologue
    .line 433
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onReplyRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    return-void
.end method

.method protected onRestartRequested()V
    .locals 2

    .prologue
    .line 449
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onRestartRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->stopPlayback()V

    .line 451
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 452
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 456
    :goto_0
    return-void

    .line 454
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNoVideoError()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 220
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onResume()V

    .line 224
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->isLoadingIndicatorEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_loadingIndicator:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 228
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->updateVideomailPlayback()V

    .line 229
    const-string v0, "Tango.ViewVideoBaseActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume :::: video playback state :::: was playing: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", video position (ms): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    iget v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoPosition:I

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 233
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_wasPlaying:Z

    if-eqz v0, :cond_1

    .line 235
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->start()V

    .line 238
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->keepScreenOnAwhile()V

    .line 239
    return-void
.end method

.method protected onRetakeRequested()V
    .locals 2

    .prologue
    .line 471
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onRetakeRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 209
    invoke-super {p0, p1}, Lcom/sgiggle/production/ActivityBase;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 212
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_POSITION"

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/BetterVideoView;->getCurrentPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_PLAYING"

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 214
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_VIDEO_SOURCE"

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_uri:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v0, "com.sgiggle.production.screens.videomail.ViewVideomailActivity.KEY_FIRST_LAUNCH"

    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_firstLaunch:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 216
    return-void
.end method

.method protected onSendRequested()V
    .locals 2

    .prologue
    .line 463
    const-string v0, "Tango.ViewVideoBaseActivity"

    const-string v1, "onSendRequested"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    .prologue
    .line 275
    invoke-super {p0}, Lcom/sgiggle/production/ActivityBase;->onUserInteraction()V

    .line 276
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->keepScreenOnAwhile()V

    .line 278
    :cond_0
    return-void
.end method

.method protected startPlaybackOnFirstLaunch()Z
    .locals 1

    .prologue
    .line 339
    const/4 v0, 0x1

    return v0
.end method
