.class Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;
.super Ljava/lang/Object;
.source "ViewVideomailActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->createDeleteVideomailDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 386
    iput-object p1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 390
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    iget-object v0, v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videoView:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->stopPlayback()V

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postPauseMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->access$100(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 394
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postDeleteMessage()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->access$200(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 395
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;->this$0:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;

    #calls: Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V
    invoke-static {v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->access$300(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 396
    return-void
.end method
