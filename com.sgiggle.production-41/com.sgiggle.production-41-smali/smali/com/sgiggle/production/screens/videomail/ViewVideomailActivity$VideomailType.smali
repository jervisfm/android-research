.class final enum Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;
.super Ljava/lang/Enum;
.source "ViewVideomailActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "VideomailType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

.field public static final enum CONVERSATION_VIDEO_MESSAGE:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

.field public static final enum STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    const-string v1, "STANDALONE_VIDEOMAIL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    .line 58
    new-instance v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    const-string v1, "CONVERSATION_VIDEO_MESSAGE"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->CONVERSATION_VIDEO_MESSAGE:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    .line 56
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->CONVERSATION_VIDEO_MESSAGE:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->$VALUES:[Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;
    .locals 1
    .parameter

    .prologue
    .line 56
    const-class v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->$VALUES:[Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    invoke-virtual {v0}, [Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    return-object v0
.end method
