.class public Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;
.super Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;
.source "ViewVideomailActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$7;,
        Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;
    }
.end annotation


# static fields
.field private static final DIALOG_DELETE_VIDEOMAIL:I = 0x0

.field private static final DIALOG_REPLY:I = 0x1

.field private static final MAX_VIDEO_PREPARE_RETRY:I = 0x3

.field private static final REQUEST_CODE_FORWARD_SMS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ViewVideomailActivity"


# instance fields
.field private m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

.field private m_isCallEnabled:Z

.field private m_playedSuccessfully:Z

.field private m_showMessageOnSent:Z

.field private m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

.field private m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;-><init>()V

    .line 52
    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    .line 53
    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    .line 62
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_isCallEnabled:Z

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    .line 64
    iput-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_showMessageOnSent:Z

    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postStartSMSComposeMessage(Ljava/util/List;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postPauseMessage()V

    return-void
.end method

.method static synthetic access$200(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postDeleteMessage()V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postCallbackMessage()V

    return-void
.end method

.method static synthetic access$500(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->startForward()V

    return-void
.end method

.method public static clearOldPlaybackData()V
    .locals 1

    .prologue
    .line 376
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_uri:Ljava/lang/String;

    .line 377
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_receivedUri:Z

    .line 378
    return-void
.end method

.method private createDeleteVideomailDialog()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 381
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 382
    const v1, 0x7f09009b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09009c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x108008a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09009d

    new-instance v3, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$4;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f09009e

    new-instance v3, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$3;

    invoke-direct {v3, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$3;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 406
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method private createReplyDialog()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 410
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 411
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 412
    const v1, 0x7f0900ab

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 413
    iget-boolean v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_isCallEnabled:Z

    if-eqz v1, :cond_0

    .line 414
    const/high16 v1, 0x7f0c

    new-instance v2, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$5;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$5;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 438
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 428
    :cond_0
    const v1, 0x7f0c0001

    new-instance v2, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$6;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$6;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method private handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 4
    .parameter

    .prologue
    .line 244
    const-string v0, "ViewVideomailActivity"

    const-string v1, "Handle Call Error Event: displaying call error dialog"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    new-instance v3, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$AcknowledgeCallErrorMessage;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 251
    invoke-virtual {p0, p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    .line 252
    return-void
.end method

.method private handleForwardMessageResultEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;)V
    .locals 3
    .parameter

    .prologue
    .line 164
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    .line 165
    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$7;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ForwardMessageResultType:[I

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 179
    :goto_0
    return-void

    .line 167
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onForwardError()V

    goto :goto_0

    .line 171
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onForwardSuccess()V

    goto :goto_0

    .line 176
    :pswitch_2
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getSmsContactsList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getViewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onForwardBySmsNeeded(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 187
    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getReceiversList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/sgiggle/production/Utils;->getSmsAddressListFromContactList(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    .line 190
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0900ae

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerPayload;->getInfo()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 193
    invoke-direct {p0, v1, v0, v6}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->sendSms(Ljava/lang/String;Ljava/lang/String;I)V

    .line 194
    return-void
.end method

.method private isCallEnabled()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 453
    const/4 v0, 0x1

    .line 455
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    sget-object v2, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    if-ne v1, v2, :cond_3

    const-string v1, "_outbox"

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->hasCaller()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 458
    const-string v1, "ViewVideomailActivity"

    const-string v2, "Has a caller"

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasAccountid()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v3

    .line 473
    :cond_0
    :goto_0
    return v0

    .line 462
    :cond_1
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasAccountid()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    .line 464
    goto :goto_0

    .line 465
    :cond_2
    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    const-string v2, "mC5mPUPZh1ZsQP2zhN8s-g"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v3

    .line 467
    goto :goto_0

    :cond_3
    move v0, v3

    .line 471
    goto :goto_0
.end method

.method private onForwardBySmsNeeded(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 220
    invoke-static {p0}, Lcom/sgiggle/production/screens/videomail/VideomailUtils;->createSendBySmsWarningDialog(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    .line 221
    if-nez v0, :cond_0

    .line 223
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postStartSMSComposeMessage(Ljava/util/List;Ljava/lang/String;Z)V

    .line 237
    :goto_0
    return-void

    .line 228
    :cond_0
    new-instance v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$1;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 235
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private onForwardError()V
    .locals 2

    .prologue
    .line 209
    const v0, 0x7f0900bb

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 210
    return-void
.end method

.method private onForwardSuccess()V
    .locals 2

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_showMessageOnSent:Z

    if-eqz v0, :cond_0

    .line 201
    const v0, 0x7f0900b7

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 203
    :cond_0
    return-void
.end method

.method private onPlaybackFinished()V
    .locals 4

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    if-ne v0, v1, :cond_0

    .line 513
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    invoke-direct {v0, v1, v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMailMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 516
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 522
    :goto_0
    return-void

    .line 518
    :cond_0
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMessageMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishPlayVideoMessageMessage;-><init>()V

    .line 520
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private postCallbackMessage()V
    .locals 3

    .prologue
    .line 495
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getCaller()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-static {v2}, Lcom/sgiggle/production/Utils;->getDisplayName(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$MakeCallMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 497
    return-void
.end method

.method private postDeleteMessage()V
    .locals 3

    .prologue
    .line 506
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$DeleteVideoMailMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 509
    return-void
.end method

.method private postFinishSMSComposeMessage(Z)V
    .locals 3
    .parameter

    .prologue
    .line 301
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$FinishSMSComposeMessage;-><init>(Z)V

    .line 303
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 304
    return-void
.end method

.method private postForwardMessageRequestMessage()V
    .locals 3

    .prologue
    .line 500
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-direct {v0, v1}, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageRequestMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)V

    .line 502
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 503
    return-void
.end method

.method private postPauseMessage()V
    .locals 3

    .prologue
    .line 483
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PausePlayVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$PausePlayVideoMailMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 486
    return-void
.end method

.method private postPlayMessage()V
    .locals 3

    .prologue
    .line 489
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailMessage;

    iget-object v1, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getFolder()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;->getVideoMailId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailMessage;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 492
    return-void
.end method

.method private postStartSMSComposeMessage(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 290
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;->SMS_COMPOSER_FORWARD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;

    invoke-direct {v0, v1, p3, p1, p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$SMSComposerType;ZLjava/util/List;Ljava/lang/String;)V

    .line 293
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 294
    return-void
.end method

.method private sendSms(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 275
    invoke-static {p1, p2}, Lcom/sgiggle/production/Utils;->getSmsIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 277
    :try_start_0
    invoke-virtual {p0, v0, p3}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :goto_0
    return-void

    .line 278
    :catch_0
    move-exception v0

    .line 279
    const-string v0, "ViewVideomailActivity"

    const-string v1, "Not activity was found for ACTION_VIEW (for sending SMS)"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startForward()V
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_showMessageOnSent:Z

    .line 445
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postForwardMessageRequestMessage()V

    .line 446
    return-void
.end method

.method public static startPlayback(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 359
    check-cast p0, Lcom/sgiggle/media_engine/MediaEngineMessage$StartPlayVideoMailEvent;

    .line 362
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartPlayVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/sgiggle/media_engine/MediaEngineMessage$StartPlayVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailDownloadURLPayload;->getVideoMailUrl()Ljava/lang/String;

    move-result-object v0

    .line 364
    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_uri:Ljava/lang/String;

    .line 367
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_receivedUri:Z

    .line 368
    return-void
.end method


# virtual methods
.method public createVideoMediaController()Lcom/sgiggle/production/widget/VideomailMediaController;
    .locals 3

    .prologue
    .line 308
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController;

    const/4 v1, 0x1

    sget-object v2, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->VIEW_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    invoke-direct {v0, p0, v1, v2}, Lcom/sgiggle/production/widget/VideomailMediaController;-><init>(Landroid/content/Context;ZLcom/sgiggle/production/widget/VideomailMediaController$Mode;)V

    return-object v0
.end method

.method protected getMaxPrepareRetry()I
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x3

    return v0
.end method

.method public handleMessage(Lcom/sgiggle/messaging/Message;)V
    .locals 1
    .parameter

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/sgiggle/messaging/Message;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 157
    :goto_0
    return-void

    .line 143
    :sswitch_0
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;

    .line 144
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleForwardMessageResultEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$ForwardMessageResultEvent;)V

    goto :goto_0

    .line 148
    :sswitch_1
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;

    .line 149
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleStartSMSComposeEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$StartSMSComposeEvent;)V

    goto :goto_0

    .line 153
    :sswitch_2
    check-cast p1, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;

    .line 154
    invoke-direct {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->handleCallErrorEvent(Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V

    goto :goto_0

    .line 140
    :sswitch_data_0
    .sparse-switch
        0x88cb -> :sswitch_2
        0x89d0 -> :sswitch_0
        0x89d1 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 119
    if-ne p1, v1, :cond_0

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_showMessageOnSent:Z

    .line 130
    invoke-direct {p0, v1}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postFinishSMSComposeMessage(Z)V

    .line 132
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 478
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onBackPressed()V

    .line 479
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V

    .line 480
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    const-string v0, "ViewVideomailActivity"

    const-string v1, "ViewVideomailActivity created without an event to init the layout"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->onActivityResumeAfterKilled(Landroid/app/Activity;)V

    .line 100
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0, p0}, Lcom/sgiggle/production/TangoApp;->setVideoPlayerActivityInstance(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 79
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    instance-of v0, v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailEvent;

    if-eqz v0, :cond_1

    .line 80
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMailEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PlayVideoMailPayload;->getVideoMail()Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailEntry:Lcom/sgiggle/xmpp/SessionMessages$VideoMailEntry;

    .line 81
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    .line 98
    :goto_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->isCallEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_isCallEnabled:Z

    .line 99
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getFirstMessage()Lcom/sgiggle/messaging/Message;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageEvent;

    invoke-virtual {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$PlayVideoMessageEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessagePayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 88
    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_uri:Ljava/lang/String;

    .line 94
    :goto_2
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_receivedUri:Z

    .line 95
    sget-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->CONVERSATION_VIDEO_MESSAGE:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    iput-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    goto :goto_1

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_conversationMessage:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrl()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_uri:Ljava/lang/String;

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 339
    packed-switch p1, :pswitch_data_0

    .line 345
    const-string v0, "ViewVideomailActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported dialog id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    invoke-super {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 341
    :pswitch_0
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->createDeleteVideomailDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 343
    :pswitch_1
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->createReplyDialog()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 339
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDeleteRequested()V
    .locals 1

    .prologue
    .line 571
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onDeleteRequested()V

    .line 572
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->showDialog(I)V

    .line 573
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 322
    const-string v0, "ViewVideomailActivity"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setVideoPlayerActivityInstance(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 325
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onDestroy()V

    .line 334
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V

    .line 335
    return-void
.end method

.method protected onMediaPlayerError()V
    .locals 2

    .prologue
    .line 526
    const v0, 0x7f0900bd

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 527
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    .line 528
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V

    .line 529
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .parameter

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 105
    const-string v0, "ViewVideomailActivity"

    const-string v1, "onNewIntent: activity will restore."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    return-void
.end method

.method protected onNoVideoError()V
    .locals 1

    .prologue
    .line 538
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onNoVideoError()V

    .line 539
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    .line 540
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->onPlaybackFinished()V

    .line 541
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 314
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPause()V

    .line 315
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 316
    invoke-virtual {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/TangoApp;->setVideoPlayerActivityInstance(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    .line 318
    :cond_0
    return-void
.end method

.method protected onPauseRequested()V
    .locals 2

    .prologue
    .line 545
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPauseRequested()V

    .line 547
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    if-ne v0, v1, :cond_0

    .line 548
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postPauseMessage()V

    .line 550
    :cond_0
    return-void
.end method

.method protected onPlayRequested()V
    .locals 2

    .prologue
    .line 554
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPlayRequested()V

    .line 556
    iget-object v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_videomailType:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    sget-object v1, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;->STANDALONE_VIDEOMAIL:Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$VideomailType;

    if-ne v0, v1, :cond_0

    .line 557
    invoke-direct {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->postPlayMessage()V

    .line 559
    :cond_0
    return-void
.end method

.method public onPrepared()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->m_playedSuccessfully:Z

    .line 114
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onPrepared()V

    .line 115
    return-void
.end method

.method protected onReplyRequested()V
    .locals 1

    .prologue
    .line 563
    invoke-super {p0}, Lcom/sgiggle/production/screens/videomail/ViewVideoBaseActivity;->onReplyRequested()V

    .line 566
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;->showDialog(I)V

    .line 567
    return-void
.end method

.method public showCallErrorDialog(Landroid/content/Context;Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 255
    invoke-virtual {p2}, Lcom/sgiggle/media_engine/MediaEngineMessage$CallErrorEvent;->payload()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OptionalPayload;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/sgiggle/production/Utils;->getStringFromResource(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090027

    new-instance v2, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$2;

    invoke-direct {v2, p0}, Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity$2;-><init>(Lcom/sgiggle/production/screens/videomail/ViewVideomailActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 265
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 266
    return-void
.end method
