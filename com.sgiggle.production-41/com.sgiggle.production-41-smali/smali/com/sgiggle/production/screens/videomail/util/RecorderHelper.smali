.class public Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;
.super Ljava/lang/Object;
.source "RecorderHelper.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.RecordHelper"

.field private static final isGingerbreadOrHigher:Z

.field private static final isIcsOrHigher:Z

.field private static m_flip:Z

.field private static m_rotate:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 14
    sput-boolean v2, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    .line 15
    sput-boolean v3, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    .line 16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    move v0, v3

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isGingerbreadOrHigher:Z

    .line 17
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    move v0, v3

    :goto_1
    sput-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isIcsOrHigher:Z

    return-void

    :cond_0
    move v0, v2

    .line 16
    goto :goto_0

    :cond_1
    move v0, v2

    .line 17
    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getProfile(Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;I)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const/16 v7, 0xf0

    const/16 v6, 0x280

    const/16 v5, 0x140

    const/16 v4, 0xf

    const/16 v3, 0x1e0

    .line 259
    const-string v0, "Tango.RecordHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getProfile(): (CURRENT) width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; framerate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; bitrate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isIcsOrHigher:Z

    if-eqz v0, :cond_6

    .line 263
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 267
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 428
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 270
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID RAZR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID4"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    :cond_2
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 273
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 274
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 275
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 276
    const v0, 0xdac0

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    goto :goto_0

    .line 278
    :cond_3
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 279
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P3113"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9300"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I925"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    :cond_4
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 284
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 285
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 286
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    goto :goto_0

    .line 288
    :cond_5
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "sony ericsson"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LT22i"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 293
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 294
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 295
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    goto/16 :goto_0

    .line 298
    :cond_6
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_15

    .line 299
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P925"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 300
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 301
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 304
    :cond_7
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 305
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SHW-M110S"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 306
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 307
    const/16 v0, 0x250

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 308
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 309
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 310
    const/4 v0, 0x2

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 313
    :cond_8
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-P100"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 314
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 315
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 316
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 317
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 318
    const/4 v0, 0x2

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 321
    :cond_9
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 323
    :cond_a
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 324
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 325
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 326
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 327
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 330
    :cond_b
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_c

    const/4 v0, 0x1

    if-ne p1, v0, :cond_c

    .line 334
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 335
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 338
    :cond_c
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P6200"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P6210"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_e

    const/4 v0, 0x1

    if-ne p1, v0, :cond_e

    .line 341
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 342
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 343
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    goto/16 :goto_0

    .line 344
    :cond_e
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-S5830"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 345
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 346
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 347
    const/16 v0, 0xa

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    goto/16 :goto_0

    .line 348
    :cond_f
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SCH-I405"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 349
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 350
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 351
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    goto/16 :goto_0

    .line 353
    :cond_10
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 354
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROID3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 355
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 356
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 359
    :cond_11
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "DROIDX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 360
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 361
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 365
    :cond_12
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 366
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 367
    const v0, 0xaae60

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    goto/16 :goto_0

    .line 368
    :cond_13
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC PG09410"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 369
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 370
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 371
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    goto/16 :goto_0

    .line 373
    :cond_14
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HUAWEI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "M886"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 379
    :cond_15
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 380
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I897"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9000"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 387
    :cond_16
    iput v4, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameRate:I

    .line 388
    const/16 v0, 0x250

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 389
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 390
    const v0, 0xbb418

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 391
    const/4 v0, 0x2

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 394
    :cond_17
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-M920"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 395
    const v0, 0x61a80

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoBitRate:I

    .line 396
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    .line 397
    const/4 v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->fileFormat:I

    .line 398
    const/16 v0, 0x160

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 399
    const/16 v0, 0x120

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 402
    :cond_18
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-P1010"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "GT-I9003L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 405
    :cond_19
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoCodec:I

    goto/16 :goto_0

    .line 408
    :cond_1a
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "YP-GB1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    iput v6, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 411
    iput v3, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 415
    :cond_1b
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "lge"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "LG-P970"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 417
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 418
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    goto/16 :goto_0

    .line 419
    :cond_1c
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "motorola"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "Droid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 421
    iput v5, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameWidth:I

    .line 422
    iput v7, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->videoFrameHeight:I

    .line 423
    const/16 v0, 0x3e80

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioBitRate:I

    .line 424
    const/4 v0, 0x3

    iput v0, p0, Lcom/sgiggle/production/screens/videomail/CamcorderProfileWrapper;->audioCodec:I

    goto/16 :goto_0
.end method

.method public static getRecorderRotation(III)I
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v7, 0x21c

    const/16 v6, 0x168

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 79
    .line 80
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    .line 81
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    .line 83
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isIcsOrHigher:Z

    if-eqz v0, :cond_2

    .line 84
    if-ne p2, v4, :cond_1

    .line 85
    sub-int v0, p0, p1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 86
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Runnymede"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 88
    sub-int v0, v6, v0

    rem-int/lit16 v0, v0, 0x168

    .line 89
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    .line 252
    :cond_0
    :goto_0
    const-string v1, "Tango.RecordHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRecorderRotation: Captured media must be rotated by: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (m_rotate="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", m_flip="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    return v0

    .line 94
    :cond_1
    add-int v0, p0, p1

    rem-int/lit16 v0, v0, 0x168

    .line 95
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Runnymede"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto :goto_0

    .line 101
    :cond_2
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isGingerbreadOrHigher:Z

    if-eqz v0, :cond_1a

    .line 102
    if-ne p2, v4, :cond_14

    .line 103
    sub-int v0, p0, p1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 104
    const-string v1, "Tango.RecordHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Original captured media angle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 107
    add-int v0, p0, p1

    rem-int/lit16 v0, v0, 0x168

    .line 108
    const-string v1, "Tango.RecordHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Adjusted original captured media angle: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "PC36100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 111
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_3

    .line 112
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    .line 113
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 116
    :cond_3
    sub-int v0, v7, v0

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 118
    :cond_4
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Flyer"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 120
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 121
    :cond_5
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Incredible S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_6

    .line 122
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    .line 123
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    .line 124
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 125
    :cond_6
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Glacier"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Desire S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 127
    :cond_7
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    .line 128
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    .line 129
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 130
    :cond_8
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ADR6400L"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Rhyme S510b"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 132
    :cond_9
    sub-int v0, v7, v0

    rem-int/lit16 v0, v0, 0x168

    .line 133
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 134
    :cond_a
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC PG09410"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 135
    sub-int v0, v6, v0

    rem-int/lit16 v0, v0, 0x168

    .line 136
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 138
    :cond_b
    add-int/lit16 v0, v0, 0xb4

    rem-int/lit16 v0, v0, 0x168

    .line 139
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 142
    :cond_c
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 143
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9003"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9003L"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 145
    :cond_d
    const/16 v1, 0x276

    sub-int v0, v1, v0

    rem-int/lit16 v0, v0, 0x168

    .line 146
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    .line 148
    :cond_e
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    .line 150
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 153
    :cond_f
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HUAWEI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 154
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "M886"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_10

    .line 156
    const/16 v1, 0x1c2

    sub-int v0, v1, v0

    rem-int/lit16 v0, v0, 0x168

    .line 157
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 159
    :cond_10
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 163
    :cond_11
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "LGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 164
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-P999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 165
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 167
    :cond_12
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-E739"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 173
    :cond_13
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "ZTE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ZTE-N910"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 179
    :cond_14
    add-int v0, p0, p1

    rem-int/lit16 v0, v0, 0x168

    .line 181
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HTC"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 183
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "PC36100"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Nexus One"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ADR6300"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "T-Mobile G2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Glacier"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Rhyme S510b"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "HTC Desire S"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 193
    :cond_15
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HUAWEI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 194
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "M886"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    .line 196
    add-int/lit16 v0, v0, 0xb4

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 200
    :cond_16
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "LGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 201
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-P999"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 202
    sput-boolean v5, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    goto/16 :goto_0

    .line 204
    :cond_17
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-E739"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 210
    :cond_18
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 211
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SAMSUNG-SGH-I897"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    add-int/lit8 v0, p1, 0x5a

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 215
    :cond_19
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "ZTE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ZTE-N910"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    add-int/lit8 v0, v0, 0x5a

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_0

    .line 222
    :cond_1a
    add-int/lit8 v0, p1, 0x5a

    rem-int/lit16 v0, v0, 0x168

    .line 223
    if-ne p2, v4, :cond_0

    .line 224
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "ADR6400L"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "lge"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-P970"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 226
    :cond_1b
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 228
    :cond_1c
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 229
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-P1010"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SCH-I800"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 232
    :cond_1d
    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v0, v0, 0x168

    .line 233
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 235
    :cond_1e
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "YP-GB1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 239
    :cond_1f
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "PANTECH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    .line 240
    add-int/lit16 v0, v0, 0x10e

    rem-int/lit16 v0, v0, 0x168

    .line 241
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0

    .line 243
    :cond_20
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "LGE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "VS910 4G"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_21

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "LG-P925"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    :cond_21
    sput-boolean v4, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    goto/16 :goto_0
.end method

.method public static getSurfaceRotation(I)I
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/16 v3, 0x10e

    .line 20
    const/4 v0, 0x0

    .line 22
    sget-boolean v1, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->isGingerbreadOrHigher:Z

    if-eqz v1, :cond_4

    .line 23
    if-ne p0, v2, :cond_3

    .line 25
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HUAWEI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "M886"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    move v0, v3

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 29
    :cond_1
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "GT-I9000"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v1, "2.3.3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 32
    const/16 v0, 0x168

    goto :goto_0

    :cond_2
    move v0, v3

    .line 34
    goto :goto_0

    .line 40
    :cond_3
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "HUAWEI"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "M886"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v2, "2.3.5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    .line 42
    const/16 v0, 0xb4

    goto :goto_0

    .line 47
    :cond_4
    if-ne p0, v2, :cond_0

    .line 49
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "Samsung"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 51
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SPH-M920"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SGH-T959V"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SCH-I510"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SGH-T839"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "SAMSUNG-SGH-I997"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "YP-GB1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "YP-G1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v3

    .line 58
    goto/16 :goto_0

    .line 61
    :cond_5
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v2, "PANTECH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v3

    .line 62
    goto/16 :goto_0
.end method

.method public static isFlipped()Z
    .locals 1

    .prologue
    .line 71
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_flip:Z

    return v0
.end method

.method public static isRotated()Z
    .locals 1

    .prologue
    .line 75
    sget-boolean v0, Lcom/sgiggle/production/screens/videomail/util/RecorderHelper;->m_rotate:Z

    return v0
.end method
