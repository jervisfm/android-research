.class abstract Lcom/sgiggle/production/service/BillingService$BillingRequest;
.super Ljava/lang/Object;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "BillingRequest"
.end annotation


# instance fields
.field protected m_requestId:J

.field private final m_startId:I

.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput p2, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_startId:I

    .line 96
    return-void
.end method


# virtual methods
.method public getStartId()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_startId:I

    return v0
.end method

.method protected logResponseCode(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 179
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->valueOf(I)Lcom/sgiggle/production/payments/Constants$ResponseCode;

    move-result-object v0

    .line 181
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " received "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return-void
.end method

.method protected makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3
    .parameter

    .prologue
    .line 171
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 172
    const-string v1, "BILLING_REQUEST"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v1, "API_VERSION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 174
    const-string v1, "PACKAGE_NAME"

    iget-object v2, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v2}, Lcom/sgiggle/production/service/BillingService;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-object v0
.end method

.method protected onRemoteException(Landroid/os/RemoteException;)V
    .locals 2
    .parameter

    .prologue
    .line 150
    const-string v0, "BillingService"

    const-string v1, "remote billing service crashed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->this$0:Lcom/sgiggle/production/service/BillingService;

    const/4 v1, 0x0

    #setter for: Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;
    invoke-static {v0, v1}, Lcom/sgiggle/production/service/BillingService;->access$202(Lcom/sgiggle/production/service/BillingService;Lcom/android/vending/billing/IMarketBillingService;)Lcom/android/vending/billing/IMarketBillingService;

    .line 152
    return-void
.end method

.method protected responseCodeReceived(Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 0
    .parameter

    .prologue
    .line 168
    return-void
.end method

.method protected abstract run()J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public runIfConnected()Z
    .locals 4

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->this$0:Lcom/sgiggle/production/service/BillingService;

    #getter for: Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;
    invoke-static {v0}, Lcom/sgiggle/production/service/BillingService;->access$200(Lcom/sgiggle/production/service/BillingService;)Lcom/android/vending/billing/IMarketBillingService;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 131
    :try_start_0
    invoke-virtual {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->run()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_requestId:J

    .line 133
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "request id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_requestId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-wide v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_requestId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 137
    invoke-static {}, Lcom/sgiggle/production/service/BillingService;->access$300()Ljava/util/HashMap;

    move-result-object v0

    iget-wide v1, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->m_requestId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    const/4 v0, 0x1

    .line 146
    :goto_0
    return v0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->onRemoteException(Landroid/os/RemoteException;)V

    .line 146
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public runRequest()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 109
    invoke-virtual {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 119
    :goto_0
    return v0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService$BillingRequest;->this$0:Lcom/sgiggle/production/service/BillingService;

    #calls: Lcom/sgiggle/production/service/BillingService;->bindToMarketBillingService()Z
    invoke-static {v0}, Lcom/sgiggle/production/service/BillingService;->access$000(Lcom/sgiggle/production/service/BillingService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 115
    invoke-static {}, Lcom/sgiggle/production/service/BillingService;->access$100()Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 116
    goto :goto_0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
