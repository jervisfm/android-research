.class public Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;
.super Lcom/sgiggle/production/service/BillingService$BillingRequest;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CheckBillingSupported"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;)V
    .locals 1
    .parameter

    .prologue
    .line 187
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;->this$0:Lcom/sgiggle/production/service/BillingService;

    .line 191
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;-><init>(Lcom/sgiggle/production/service/BillingService;I)V

    .line 192
    return-void
.end method


# virtual methods
.method public bridge synthetic getStartId()I
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    return v0
.end method

.method protected run()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 196
    const-string v0, "CHECK_BILLING_SUPPORTED"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;->makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/service/BillingService;->sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 198
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 201
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CheckBillingSupported response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->valueOf(I)Lcom/sgiggle/production/payments/Constants$ResponseCode;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    sget-object v1, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    invoke-virtual {v1}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 205
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;->this$0:Lcom/sgiggle/production/service/BillingService;

    #getter for: Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;
    invoke-static {v1}, Lcom/sgiggle/production/service/BillingService;->access$400(Lcom/sgiggle/production/service/BillingService;)Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/payments/ResponseHandler;->checkBillingSupported(Z)V

    .line 207
    sget-wide v0, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    return-wide v0

    .line 204
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic runIfConnected()Z
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic runRequest()Z
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runRequest()Z

    move-result v0

    return v0
.end method
