.class public Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;
.super Lcom/sgiggle/production/service/BillingService$BillingRequest;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConfirmNotifications"
.end annotation


# instance fields
.field final m_notifyIds:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;I[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 261
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->this$0:Lcom/sgiggle/production/service/BillingService;

    .line 262
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/service/BillingService$BillingRequest;-><init>(Lcom/sgiggle/production/service/BillingService;I)V

    .line 263
    iput-object p3, p0, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->m_notifyIds:[Ljava/lang/String;

    .line 264
    return-void
.end method


# virtual methods
.method public bridge synthetic getStartId()I
    .locals 1

    .prologue
    .line 258
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    return v0
.end method

.method protected run()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 268
    const-string v0, "CONFIRM_NOTIFICATIONS"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 269
    const-string v1, "NOTIFY_IDS"

    iget-object v2, p0, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->m_notifyIds:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 270
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/service/BillingService;->sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 271
    const-string v1, "confirmNotifications"

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->logResponseCode(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 272
    const-string v1, "REQUEST_ID"

    sget-wide v2, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic runIfConnected()Z
    .locals 1

    .prologue
    .line 258
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic runRequest()Z
    .locals 1

    .prologue
    .line 258
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runRequest()Z

    move-result v0

    return v0
.end method
