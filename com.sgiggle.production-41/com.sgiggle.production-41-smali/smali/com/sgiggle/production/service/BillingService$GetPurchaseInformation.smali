.class public Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;
.super Lcom/sgiggle/production/service/BillingService$BillingRequest;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GetPurchaseInformation"
.end annotation


# instance fields
.field m_nonce:J

.field final m_notifyIds:[Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;I[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 284
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->this$0:Lcom/sgiggle/production/service/BillingService;

    .line 285
    invoke-direct {p0, p1, p2}, Lcom/sgiggle/production/service/BillingService$BillingRequest;-><init>(Lcom/sgiggle/production/service/BillingService;I)V

    .line 286
    iput-object p3, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->m_notifyIds:[Ljava/lang/String;

    .line 287
    return-void
.end method


# virtual methods
.method public bridge synthetic getStartId()I
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    return v0
.end method

.method protected onRemoteException(Landroid/os/RemoteException;)V
    .locals 2
    .parameter

    .prologue
    .line 304
    invoke-super {p0, p1}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->onRemoteException(Landroid/os/RemoteException;)V

    .line 305
    iget-wide v0, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->m_nonce:J

    invoke-static {v0, v1}, Lcom/sgiggle/production/util/Security;->removeNonce(J)V

    .line 306
    return-void
.end method

.method protected run()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 291
    invoke-static {}, Lcom/sgiggle/production/util/Security;->generateNonce()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->m_nonce:J

    .line 293
    const-string v0, "GET_PURCHASE_INFORMATION"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 294
    const-string v1, "NONCE"

    iget-wide v2, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->m_nonce:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 295
    const-string v1, "NOTIFY_IDS"

    iget-object v2, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->m_notifyIds:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 296
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/service/BillingService;->sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 297
    const-string v1, "getPurchaseInformation"

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->logResponseCode(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 298
    const-string v1, "REQUEST_ID"

    sget-wide v2, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic runIfConnected()Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic runRequest()Z
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runRequest()Z

    move-result v0

    return v0
.end method
