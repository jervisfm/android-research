.class public Lcom/sgiggle/production/service/BillingService$RequestPurchase;
.super Lcom/sgiggle/production/service/BillingService$BillingRequest;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RequestPurchase"
.end annotation


# instance fields
.field public final m_developerPayload:Ljava/lang/String;

.field public final m_productId:Ljava/lang/String;

.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/service/BillingService$RequestPurchase;-><init>(Lcom/sgiggle/production/service/BillingService;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->this$0:Lcom/sgiggle/production/service/BillingService;

    .line 223
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;-><init>(Lcom/sgiggle/production/service/BillingService;I)V

    .line 224
    iput-object p2, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_productId:Ljava/lang/String;

    .line 225
    iput-object p3, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_developerPayload:Ljava/lang/String;

    .line 226
    return-void
.end method


# virtual methods
.method public bridge synthetic getStartId()I
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    return v0
.end method

.method protected responseCodeReceived(Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 1
    .parameter

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->this$0:Lcom/sgiggle/production/service/BillingService;

    #getter for: Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;
    invoke-static {v0}, Lcom/sgiggle/production/service/BillingService;->access$400(Lcom/sgiggle/production/service/BillingService;)Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/production/payments/ResponseHandler;->responseCodeReceived(Lcom/sgiggle/production/service/BillingService$RequestPurchase;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V

    .line 252
    return-void
.end method

.method protected run()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 230
    const-string v0, "REQUEST_PURCHASE"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 231
    const-string v1, "ITEM_ID"

    iget-object v2, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_productId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_developerPayload:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 234
    const-string v1, "DEVELOPER_PAYLOAD"

    iget-object v2, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->m_developerPayload:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/service/BillingService;->sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 237
    const-string v0, "PURCHASE_INTENT"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 238
    if-nez v0, :cond_1

    .line 239
    const-string v0, "BillingService"

    const-string v1, "Error with requestPurchase"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    sget-wide v0, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    .line 245
    :goto_0
    return-wide v0

    .line 243
    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 244
    iget-object v3, p0, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->this$0:Lcom/sgiggle/production/service/BillingService;

    #getter for: Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;
    invoke-static {v3}, Lcom/sgiggle/production/service/BillingService;->access$400(Lcom/sgiggle/production/service/BillingService;)Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v3

    invoke-virtual {v3, v0, v2}, Lcom/sgiggle/production/payments/ResponseHandler;->buyPageIntentResponse(Landroid/app/PendingIntent;Landroid/content/Intent;)V

    .line 245
    const-string v0, "REQUEST_ID"

    sget-wide v2, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public bridge synthetic runIfConnected()Z
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic runRequest()Z
    .locals 1

    .prologue
    .line 211
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runRequest()Z

    move-result v0

    return v0
.end method
