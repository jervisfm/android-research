.class public Lcom/sgiggle/production/service/BillingService$RestoreTransactions;
.super Lcom/sgiggle/production/service/BillingService$BillingRequest;
.source "BillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/service/BillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RestoreTransactions"
.end annotation


# instance fields
.field m_nonce:J

.field final synthetic this$0:Lcom/sgiggle/production/service/BillingService;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/service/BillingService;)V
    .locals 1
    .parameter

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->this$0:Lcom/sgiggle/production/service/BillingService;

    .line 319
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;-><init>(Lcom/sgiggle/production/service/BillingService;I)V

    .line 320
    return-void
.end method


# virtual methods
.method public bridge synthetic getStartId()I
    .locals 1

    .prologue
    .line 312
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    return v0
.end method

.method protected onRemoteException(Landroid/os/RemoteException;)V
    .locals 2
    .parameter

    .prologue
    .line 341
    invoke-super {p0, p1}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->onRemoteException(Landroid/os/RemoteException;)V

    .line 342
    iget-wide v0, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->m_nonce:J

    invoke-static {v0, v1}, Lcom/sgiggle/production/util/Security;->removeNonce(J)V

    .line 343
    return-void
.end method

.method protected responseCodeReceived(Lcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 1
    .parameter

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->this$0:Lcom/sgiggle/production/service/BillingService;

    #getter for: Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;
    invoke-static {v0}, Lcom/sgiggle/production/service/BillingService;->access$400(Lcom/sgiggle/production/service/BillingService;)Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/production/payments/ResponseHandler;->responseCodeReceived(Lcom/sgiggle/production/service/BillingService$RestoreTransactions;Lcom/sgiggle/production/payments/Constants$ResponseCode;)V

    .line 348
    return-void
.end method

.method protected run()J
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 324
    invoke-static {}, Lcom/sgiggle/production/util/Security;->generateNonce()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->m_nonce:J

    .line 326
    const-string v0, "RESTORE_TRANSACTIONS"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->makeRequestBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 327
    const-string v1, "NONCE"

    iget-wide v2, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->m_nonce:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 328
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->this$0:Lcom/sgiggle/production/service/BillingService;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/service/BillingService;->sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 329
    const-string v1, "restoreTransactions"

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->logResponseCode(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 331
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->valueOf(I)Lcom/sgiggle/production/payments/Constants$ResponseCode;

    move-result-object v1

    .line 332
    sget-object v2, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_OK:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    if-ne v1, v2, :cond_0

    .line 333
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->this$0:Lcom/sgiggle/production/service/BillingService;

    const/4 v2, 0x1

    #setter for: Lcom/sgiggle/production/service/BillingService;->m_isRestoreOperationDone:Z
    invoke-static {v1, v2}, Lcom/sgiggle/production/service/BillingService;->access$502(Lcom/sgiggle/production/service/BillingService;Z)Z

    .line 335
    :cond_0
    const-string v1, "REQUEST_ID"

    sget-wide v2, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic runIfConnected()Z
    .locals 1

    .prologue
    .line 312
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic runRequest()Z
    .locals 1

    .prologue
    .line 312
    invoke-super {p0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runRequest()Z

    move-result v0

    return v0
.end method
