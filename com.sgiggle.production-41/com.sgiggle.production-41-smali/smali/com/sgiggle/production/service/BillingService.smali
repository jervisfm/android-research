.class public Lcom/sgiggle/production/service/BillingService;
.super Landroid/app/Service;
.source "BillingService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;,
        Lcom/sgiggle/production/service/BillingService$RestoreTransactions;,
        Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;,
        Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;,
        Lcom/sgiggle/production/service/BillingService$RequestPurchase;,
        Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;,
        Lcom/sgiggle/production/service/BillingService$BillingRequest;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String; = "BillingService"

.field private static m_pendingRequests:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/sgiggle/production/service/BillingService$BillingRequest;",
            ">;"
        }
    .end annotation
.end field

.field private static m_sentRequests:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/sgiggle/production/service/BillingService$BillingRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private m_isRestoreOperationDone:Z

.field private m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

.field private m_marketBindResult:Z

.field private m_packagePrefix:Ljava/lang/String;

.field private m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

.field private final m_serviceBinder:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 50
    iput-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_isRestoreOperationDone:Z

    .line 56
    iput-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBindResult:Z

    .line 80
    new-instance v0, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/service/BillingService$BillingServiceBinder;-><init>(Lcom/sgiggle/production/service/BillingService;)V

    iput-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_serviceBinder:Landroid/os/IBinder;

    .line 357
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/service/BillingService;)Z
    .locals 1
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/sgiggle/production/service/BillingService;->bindToMarketBillingService()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100()Ljava/util/Queue;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/service/BillingService;)Lcom/android/vending/billing/IMarketBillingService;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sgiggle/production/service/BillingService;Lcom/android/vending/billing/IMarketBillingService;)Lcom/android/vending/billing/IMarketBillingService;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

    return-object p1
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/service/BillingService;)Lcom/sgiggle/production/payments/ResponseHandler;
    .locals 1
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sgiggle/production/service/BillingService;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/sgiggle/production/service/BillingService;->m_isRestoreOperationDone:Z

    return p1
.end method

.method private addPackagePrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 580
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService;->m_packagePrefix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private bindToMarketBillingService()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 528
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.MarketBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p0, v1}, Lcom/sgiggle/production/service/BillingService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBindResult:Z

    .line 530
    iget-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBindResult:Z

    if-eqz v0, :cond_0

    .line 531
    const-string v0, "BillingService"

    const-string v1, "Service bind successful"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 540
    :goto_0
    return v0

    .line 534
    :cond_0
    const-string v0, "BillingService"

    const-string v1, "Could not bind to the MarketBillingService"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 540
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 536
    :catch_0
    move-exception v0

    .line 537
    const-string v1, "BillingService"

    const-string v2, "Security Exception: "

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private checkResponseCode(JLcom/sgiggle/production/payments/Constants$ResponseCode;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 605
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/service/BillingService$BillingRequest;

    .line 607
    if-eqz v0, :cond_0

    .line 609
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    invoke-virtual {v0, p3}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->responseCodeReceived(Lcom/sgiggle/production/payments/Constants$ResponseCode;)V

    .line 615
    :cond_0
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 616
    return-void
.end method

.method private confirmNotifications(I[Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 505
    new-instance v0, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;

    invoke-direct {v0, p0, p1, p2}, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;-><init>(Lcom/sgiggle/production/service/BillingService;I[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$ConfirmNotifications;->runRequest()Z

    move-result v0

    return v0
.end method

.method private getPurchaseInformation(I[Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 523
    new-instance v0, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;

    invoke-direct {v0, p0, p1, p2}, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;-><init>(Lcom/sgiggle/production/service/BillingService;I[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$GetPurchaseInformation;->runRequest()Z

    move-result v0

    return v0
.end method

.method private purchaseStateChanged(ILjava/lang/String;Ljava/lang/String;)V
    .locals 18
    .parameter
    .parameter
    .parameter

    .prologue
    .line 545
    invoke-static/range {p2 .. p3}, Lcom/sgiggle/production/util/Security;->verifyPurchase(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 547
    if-nez v3, :cond_0

    .line 577
    :goto_0
    return-void

    .line 551
    :cond_0
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 552
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sgiggle/production/util/Security$VerifiedPurchase;

    move-object v11, v0

    .line 553
    iget-object v3, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->notificationId:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 554
    iget-object v3, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->notificationId:Ljava/lang/String;

    move-object/from16 v0, v16

    move-object v1, v3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_1
    iget-object v3, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->productId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object v1, v3

    invoke-direct {v0, v1}, Lcom/sgiggle/production/service/BillingService;->removePackagePrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 558
    iget-object v3, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->developerPayload:Ljava/lang/String;

    .line 559
    if-nez v3, :cond_5

    move-object v5, v4

    .line 562
    :goto_2
    iget-object v3, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    sget-object v6, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v3, v6, :cond_2

    .line 563
    iget-object v8, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->orderId:Ljava/lang/String;

    iget-wide v9, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->purchaseTime:J

    move-object/from16 v3, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    invoke-direct/range {v3 .. v10}, Lcom/sgiggle/production/service/BillingService;->recordVGoodPurchased(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 567
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    move-object v6, v0

    iget-object v8, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->purchaseState:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    iget-object v10, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->orderId:Ljava/lang/String;

    iget-wide v11, v11, Lcom/sgiggle/production/util/Security$VerifiedPurchase;->purchaseTime:J

    const/4 v13, 0x0

    move-object/from16 v7, p0

    move-object v9, v5

    move-object/from16 v14, p2

    move-object/from16 v15, p3

    invoke-virtual/range {v6 .. v15}, Lcom/sgiggle/production/payments/ResponseHandler;->purchaseResponse(Landroid/content/Context;Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 570
    :cond_3
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 571
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    move-object/from16 v0, v16

    move-object v1, v3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 572
    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object v2, v3

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/service/BillingService;->confirmNotifications(I[Ljava/lang/String;)Z

    .line 576
    :cond_4
    const/4 v3, 0x0

    move v0, v3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sgiggle/production/service/BillingService;->m_isRestoreOperationDone:Z

    goto :goto_0

    :cond_5
    move-object v5, v3

    goto :goto_2
.end method

.method private recordPurchasedAttempt(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 684
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAttemptMessage;

    invoke-direct {v0, p1}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseAttemptMessage;-><init>(Ljava/lang/String;)V

    .line 685
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 686
    return-void
.end method

.method private recordVGoodPurchased(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 676
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;

    const/4 v4, 0x1

    const-wide/16 v1, 0x3e8

    div-long v6, p6, v1

    iget-boolean v9, p0, Lcom/sgiggle/production/service/BillingService;->m_isRestoreOperationDone:Z

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object v8, p4

    invoke-direct/range {v0 .. v9}, Lcom/sgiggle/media_engine/MediaEngineMessage$PurchaseVGoodMessage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;JLjava/lang/String;Z)V

    .line 680
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 681
    return-void
.end method

.method private removePackagePrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_packagePrefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 585
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_packagePrefix:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 587
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method

.method private runPendingRequests()V
    .locals 4

    .prologue
    .line 623
    const/4 v0, -0x1

    move v1, v0

    .line 626
    :cond_0
    :goto_0
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/service/BillingService$BillingRequest;

    if-eqz v0, :cond_3

    .line 627
    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->runIfConnected()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 629
    sget-object v2, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 633
    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 634
    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$BillingRequest;->getStartId()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 639
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/service/BillingService;->bindToMarketBillingService()Z

    .line 653
    :cond_2
    :goto_1
    return-void

    .line 647
    :cond_3
    if-ltz v1, :cond_2

    .line 649
    const-string v0, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopping service, startId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 651
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/service/BillingService;->stopSelf(I)V

    goto :goto_1
.end method


# virtual methods
.method public checkBillingSupported()Z
    .locals 3

    .prologue
    .line 454
    new-instance v0, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;-><init>(Lcom/sgiggle/production/service/BillingService;)V

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$CheckBillingSupported;->runRequest()Z

    move-result v0

    .line 455
    if-nez v0, :cond_0

    .line 456
    iget-object v1, p0, Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/payments/ResponseHandler;->checkBillingSupported(Z)V

    .line 457
    :cond_0
    return v0
.end method

.method public handleCommand(Landroid/content/Intent;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 409
    if-nez p1, :cond_1

    .line 410
    const-string v0, "BillingService"

    const-string v1, "BillingService started with null intent!"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 411
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/service/BillingService;->stopSelfResult(I)Z

    .line 442
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 419
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleCommand() action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    const-string v1, "com.sgiggle.production.tango.CONFIRM_NOTIFICATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 423
    const-string v0, "notification_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 424
    invoke-direct {p0, p2, v0}, Lcom/sgiggle/production/service/BillingService;->confirmNotifications(I[Ljava/lang/String;)Z

    .line 439
    :cond_2
    :goto_1
    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_sentRequests:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sgiggle/production/service/BillingService;->m_pendingRequests:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 440
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/service/BillingService;->stopSelfResult(I)Z

    goto :goto_0

    .line 425
    :cond_3
    const-string v1, "com.sgiggle.production.tango.GET_PURCHASE_INFORMATION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 426
    const-string v0, "notification_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 427
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    invoke-direct {p0, p2, v1}, Lcom/sgiggle/production/service/BillingService;->getPurchaseInformation(I[Ljava/lang/String;)Z

    goto :goto_1

    .line 428
    :cond_4
    const-string v1, "com.android.vending.billing.PURCHASE_STATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 429
    const-string v0, "inapp_signed_data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 430
    const-string v1, "inapp_signature"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 431
    invoke-direct {p0, p2, v0, v1}, Lcom/sgiggle/production/service/BillingService;->purchaseStateChanged(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 432
    :cond_5
    const-string v1, "com.android.vending.billing.RESPONSE_CODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 433
    const-string v0, "request_id"

    const-wide/16 v1, -0x1

    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 434
    const-string v2, "response_code"

    sget-object v3, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_ERROR:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    invoke-virtual {v3}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->ordinal()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 435
    invoke-static {v2}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->valueOf(I)Lcom/sgiggle/production/payments/Constants$ResponseCode;

    move-result-object v2

    .line 436
    invoke-direct {p0, v0, v1, v2}, Lcom/sgiggle/production/service/BillingService;->checkResponseCode(JLcom/sgiggle/production/payments/Constants$ResponseCode;)V

    goto :goto_1
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .parameter

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_serviceBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 377
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 379
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/service/BillingService;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/TangoApp;

    invoke-virtual {v0}, Lcom/sgiggle/production/TangoApp;->getResponseHandler()Lcom/sgiggle/production/payments/ResponseHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_responseHandler:Lcom/sgiggle/production/payments/ResponseHandler;

    .line 387
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sgiggle/production/TangoApp;->getInstance()Lcom/sgiggle/production/TangoApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/TangoApp;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_packagePrefix:Ljava/lang/String;

    .line 388
    return-void

    .line 380
    :catch_0
    move-exception v0

    .line 382
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 392
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 394
    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBindResult:Z

    if-eqz v0, :cond_0

    .line 395
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/service/BillingService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 396
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBindResult:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 398
    :catch_0
    move-exception v0

    .line 399
    const-string v1, "BillingService"

    const-string v2, "Error while unbinding from service: "

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 662
    const-string v0, "BillingService"

    const-string v1, "MarketBillingService connected"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    invoke-static {p2}, Lcom/android/vending/billing/IMarketBillingService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/vending/billing/IMarketBillingService;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

    .line 664
    invoke-direct {p0}, Lcom/sgiggle/production/service/BillingService;->runPendingRequests()V

    .line 665
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .parameter

    .prologue
    .line 669
    const-string v0, "BillingService"

    const-string v1, "Billing service disconnected"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

    .line 671
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 405
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/production/service/BillingService;->handleCommand(Landroid/content/Intent;I)V

    .line 406
    return-void
.end method

.method public requestPurchase(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 474
    const-string v0, "BillingService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestPurchase: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    invoke-direct {p0, p2}, Lcom/sgiggle/production/service/BillingService;->recordPurchasedAttempt(Ljava/lang/String;)V

    .line 476
    invoke-direct {p0, p1}, Lcom/sgiggle/production/service/BillingService;->addPackagePrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 477
    new-instance v1, Lcom/sgiggle/production/service/BillingService$RequestPurchase;

    invoke-direct {v1, p0, v0, p2}, Lcom/sgiggle/production/service/BillingService$RequestPurchase;-><init>(Lcom/sgiggle/production/service/BillingService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/sgiggle/production/service/BillingService$RequestPurchase;->runRequest()Z

    move-result v0

    return v0
.end method

.method public restoreTransactions()Z
    .locals 1

    .prologue
    .line 488
    new-instance v0, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;-><init>(Lcom/sgiggle/production/service/BillingService;)V

    invoke-virtual {v0}, Lcom/sgiggle/production/service/BillingService$RestoreTransactions;->runRequest()Z

    move-result v0

    return v0
.end method

.method public sendBillingRequestSafe(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 365
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/service/BillingService;->m_marketBillingService:Lcom/android/vending/billing/IMarketBillingService;

    invoke-interface {v0, p1}, Lcom/android/vending/billing/IMarketBillingService;->sendBillingRequest(Landroid/os/Bundle;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 371
    :goto_0
    return-object v0

    .line 366
    :catch_0
    move-exception v0

    .line 367
    const-string v1, "BillingService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MarketBillingService failed:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 369
    const-string v1, "RESPONSE_CODE"

    sget-object v2, Lcom/sgiggle/production/payments/Constants$ResponseCode;->RESULT_SERVICE_UNAVAILABLE:Lcom/sgiggle/production/payments/Constants$ResponseCode;

    invoke-virtual {v2}, Lcom/sgiggle/production/payments/Constants$ResponseCode;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 370
    const-string v1, "REQUEST_ID"

    sget-wide v2, Lcom/sgiggle/production/payments/Constants;->BILLING_RESPONSE_INVALID_REQUEST_ID:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public setContext(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 445
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/service/BillingService;->attachBaseContext(Landroid/content/Context;)V

    .line 446
    return-void
.end method
