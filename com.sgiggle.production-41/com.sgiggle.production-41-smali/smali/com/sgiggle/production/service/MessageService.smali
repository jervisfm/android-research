.class public Lcom/sgiggle/production/service/MessageService;
.super Landroid/app/Service;
.source "MessageService.java"


# static fields
.field private static final ACTION_KEEPALIVE:Ljava/lang/String; = "tango.service.KEEP_ALIVE"

.field public static final ACTION_STOP:Ljava/lang/String; = "tango.service.STOP"

.field private static final KEEP_ALIVE_INTERVAL:J = 0x75300L

.field private static final PREF_STARTED:Ljava/lang/String; = "isStarted"

.field private static final TAG:Ljava/lang/String; = "Tango.MessageService"

.field private static final WAKE_LOCK_ACQUIRED_TIMEOUT:J = 0x3a98L


# instance fields
.field private m_prefs:Landroid/content/SharedPreferences;

.field private m_started:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private keepAlive()V
    .locals 3

    .prologue
    .line 112
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->readRegistrationFlag(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    const-string v0, "Tango.MessageService"

    const-string v1, "Tango registration not sent, stop Alarm Service"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/service/MessageService;->setKeepAlives(Z)V

    .line 129
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-static {p0}, Lcom/sgiggle/production/Utils;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    const-string v0, "Tango.MessageService"

    const-string v1, "Network not available, don\'t send heartbeat"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 123
    :cond_1
    const-string v0, "Tango.MessageService"

    const-string v1, "keepAlive(): acquire wake-lock: Timeout=15000"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-wide/16 v0, 0x3a98

    const-string v2, "Tango.MessageService"

    invoke-static {p0, v0, v1, v2}, Lcom/sgiggle/production/Utils;->acquirePartialWakeLock(Landroid/content/Context;JLjava/lang/String;)V

    .line 127
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$KeepTangoPushNotificationAliveMessage;

    invoke-direct {v2}, Lcom/sgiggle/media_engine/MediaEngineMessage$KeepTangoPushNotificationAliveMessage;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    goto :goto_0
.end method

.method private setKeepAlives(Z)V
    .locals 7
    .parameter

    .prologue
    const-wide/32 v4, 0x75300

    const/4 v1, 0x0

    .line 87
    const-string v0, "Tango.MessageService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setKeepAlives(): bStart = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " m_started "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-boolean v0, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    if-ne v0, p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 92
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 94
    const-string v2, "tango.service.KEEP_ALIVE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 97
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/service/MessageService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 99
    if-eqz p1, :cond_1

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 108
    :goto_1
    invoke-direct {p0, p1}, Lcom/sgiggle/production/service/MessageService;->setStarted(Z)V

    goto :goto_0

    .line 105
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method private setStarted(Z)V
    .locals 2
    .parameter

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sgiggle/production/service/MessageService;->m_prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "isStarted"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    iput-boolean p1, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    .line 148
    return-void
.end method

.method private wasStarted()Z
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/service/MessageService;->m_prefs:Landroid/content/SharedPreferences;

    const-string v1, "isStarted"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .parameter

    .prologue
    .line 37
    const-string v0, "Tango.MessageService"

    const-string v1, "onBind()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    :try_start_0
    invoke-static {}, Lcom/sgiggle/production/TangoApp;->ensureInitialized()V
    :try_end_0
    .catch Lcom/sgiggle/production/WrongTangoRuntimeVersionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_0
    const-string v0, "Tango.MessageService"

    const-string v1, "onCreate()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 53
    const-string v0, "Tango.MessageService"

    invoke-virtual {p0, v0, v4}, Lcom/sgiggle/production/service/MessageService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/service/MessageService;->m_prefs:Landroid/content/SharedPreferences;

    .line 59
    invoke-direct {p0}, Lcom/sgiggle/production/service/MessageService;->wasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "Tango.MessageService"

    const-string v1, "onCreate(): Service was crashed. Cleanup..."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    invoke-direct {p0, v4}, Lcom/sgiggle/production/service/MessageService;->setKeepAlives(Z)V

    .line 64
    :cond_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    .line 47
    const-string v1, "Tango.MessageService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Initialization failed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sgiggle/production/WrongTangoRuntimeVersionException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 133
    const-string v0, "Tango.MessageService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy() started = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 136
    iget-boolean v0, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/service/MessageService;->setKeepAlives(Z)V

    .line 139
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 68
    const-string v0, "Tango.MessageService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand(): intent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-eqz p1, :cond_0

    const-string v0, "tango.service.KEEP_ALIVE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/service/MessageService;->keepAlive()V

    .line 83
    :goto_0
    return v3

    .line 72
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "tango.service.STOP"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/production/service/MessageService;->m_started:Z

    if-eqz v0, :cond_1

    .line 74
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/service/MessageService;->setKeepAlives(Z)V

    .line 75
    invoke-virtual {p0}, Lcom/sgiggle/production/service/MessageService;->stopSelf()V

    goto :goto_0

    .line 77
    :cond_1
    invoke-direct {p0, v3}, Lcom/sgiggle/production/service/MessageService;->setKeepAlives(Z)V

    goto :goto_0
.end method
