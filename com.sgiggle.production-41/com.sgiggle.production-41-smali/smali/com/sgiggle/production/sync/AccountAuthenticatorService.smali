.class public Lcom/sgiggle/production/sync/AccountAuthenticatorService;
.super Landroid/app/Service;
.source "AccountAuthenticatorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.AccountAuthenticatorService"

.field private static s_accountAuthenticator:Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/production/sync/AccountAuthenticatorService;->s_accountAuthenticator:Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    return-void
.end method

.method private getAuthenticator()Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/sgiggle/production/sync/AccountAuthenticatorService;->s_accountAuthenticator:Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sgiggle/production/sync/AccountAuthenticatorService;->s_accountAuthenticator:Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    .line 36
    :cond_0
    sget-object v0, Lcom/sgiggle/production/sync/AccountAuthenticatorService;->s_accountAuthenticator:Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .parameter

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 26
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.accounts.AccountAuthenticator"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27
    invoke-direct {p0}, Lcom/sgiggle/production/sync/AccountAuthenticatorService;->getAuthenticator()Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/production/sync/AccountAuthenticatorService$AccountAuthenticatorImpl;->getIBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 29
    :cond_0
    return-object v0
.end method
