.class Lcom/sgiggle/production/sync/LoginActivity$LoginTask;
.super Landroid/os/AsyncTask;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/sync/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoginTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/sync/LoginActivity;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/sync/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/sync/LoginActivity;Lcom/sgiggle/production/sync/LoginActivity$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;-><init>(Lcom/sgiggle/production/sync/LoginActivity;)V

    return-void
.end method


# virtual methods
.method public varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 50
    aget-object v0, p1, v5

    .line 51
    aget-object v1, p1, v4

    .line 55
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    const v3, 0x7f090087

    invoke-virtual {v2, v3}, Lcom/sgiggle/production/sync/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 62
    iget-object v3, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 64
    array-length v3, v3

    if-lez v3, :cond_0

    .line 65
    const-string v0, "Tango.sync.LoginActivity"

    const-string v1, "Tango sync account has been created. Do nothing."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 96
    :goto_1
    return-object v0

    .line 56
    :catch_0
    move-exception v2

    .line 57
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 72
    :cond_0
    new-instance v3, Landroid/accounts/Account;

    invoke-direct {v3, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 74
    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, Landroid/accounts/AccountManager;->addAccountExplicitly(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const-string v0, "com.android.contacts"

    invoke-static {v3, v0, v4}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 77
    const-string v0, "com.android.contacts"

    invoke-static {v3, v0, v4}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 90
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 91
    const-string v1, "authAccount"

    iget-object v2, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    const-string v1, "accountType"

    iget-object v2, v3, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    invoke-virtual {v1, v0}, Lcom/sgiggle/production/sync/LoginActivity;->setAccountAuthenticatorResult(Landroid/os/Bundle;)V

    .line 94
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 96
    :cond_1
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 47
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/sync/LoginActivity;->removeDialog(I)V

    .line 104
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->this$0:Lcom/sgiggle/production/sync/LoginActivity;

    invoke-virtual {v0}, Lcom/sgiggle/production/sync/LoginActivity;->finish()V

    .line 107
    :cond_0
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
