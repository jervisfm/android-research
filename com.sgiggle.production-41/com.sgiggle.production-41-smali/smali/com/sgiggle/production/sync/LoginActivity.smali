.class public Lcom/sgiggle/production/sync/LoginActivity;
.super Landroid/accounts/AccountAuthenticatorActivity;
.source "LoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/sync/LoginActivity$1;,
        Lcom/sgiggle/production/sync/LoginActivity$LoginTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.sync.LoginActivity"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/accounts/AccountAuthenticatorActivity;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 21
    invoke-super {p0, p1}, Landroid/accounts/AccountAuthenticatorActivity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const v0, 0x7f030054

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/sync/LoginActivity;->setContentView(I)V

    .line 24
    const v0, 0x7f090088

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/sync/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 27
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/sync/LoginActivity;->showDialog(I)V

    .line 29
    if-nez p1, :cond_0

    .line 30
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 31
    new-instance v1, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;-><init>(Lcom/sgiggle/production/sync/LoginActivity;Lcom/sgiggle/production/sync/LoginActivity$1;)V

    .line 32
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/sgiggle/production/sync/LoginActivity$LoginTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 35
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2
    .parameter

    .prologue
    .line 39
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 40
    const v1, 0x7f09008a

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/sync/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 41
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 42
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 43
    invoke-virtual {v0, p0}, Landroid/app/ProgressDialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 44
    return-object v0
.end method
