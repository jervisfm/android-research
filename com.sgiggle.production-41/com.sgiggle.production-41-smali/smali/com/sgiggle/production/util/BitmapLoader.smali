.class abstract Lcom/sgiggle/production/util/BitmapLoader;
.super Ljava/lang/Object;
.source "BitmapLoader.java"


# instance fields
.field private m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field private m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    .line 22
    return-void
.end method


# virtual methods
.method public addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    invoke-virtual {v0, p1, p2}, Lcom/sgiggle/production/util/BitmapLoaderThread;->queueTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)Z

    .line 67
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 50
    return-void
.end method

.method protected abstract createLoadBitmapThread()Lcom/sgiggle/production/util/BitmapLoaderThread;
.end method

.method protected getBitmaps()Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method public getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    if-nez v0, :cond_0

    move-object v0, v2

    .line 86
    :goto_0
    return-object v0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p1}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getImageId()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 80
    if-nez v0, :cond_1

    move-object v0, v2

    .line 81
    goto :goto_0

    .line 83
    :cond_1
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 84
    if-nez v1, :cond_2

    .line 85
    iget-object v1, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_bitmaps:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p1}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getImageId()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_2
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/sgiggle/production/util/BitmapLoader;->createLoadBitmapThread()Lcom/sgiggle/production/util/BitmapLoaderThread;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sgiggle/production/util/BitmapLoaderThread;->stop:Z

    .line 32
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/util/BitmapLoaderThread;->setPriority(I)V

    .line 33
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    invoke-virtual {v0}, Lcom/sgiggle/production/util/BitmapLoaderThread;->start()V

    .line 34
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sgiggle/production/util/BitmapLoaderThread;->stop:Z

    .line 41
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    invoke-virtual {v0}, Lcom/sgiggle/production/util/BitmapLoaderThread;->interrupt()V

    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoader;->m_loadBitmapThread:Lcom/sgiggle/production/util/BitmapLoaderThread;

    iget-object v0, v0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 43
    return-void
.end method
