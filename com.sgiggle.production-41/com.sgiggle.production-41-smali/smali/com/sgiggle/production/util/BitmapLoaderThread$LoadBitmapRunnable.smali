.class public Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;
.super Ljava/lang/Object;
.source "BitmapLoaderThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/util/BitmapLoaderThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LoadBitmapRunnable"
.end annotation


# instance fields
.field private m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

.field private m_view:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sgiggle/production/util/BitmapLoaderThread;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/util/BitmapLoaderThread;Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->this$0:Lcom/sgiggle/production/util/BitmapLoaderThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput-object p2, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    .line 110
    iput-object p3, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    .line 114
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    invoke-interface {v1}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getViewTag()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    invoke-interface {v0}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getImageId()Ljava/lang/Object;

    move-result-object v1

    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->this$0:Lcom/sgiggle/production/util/BitmapLoaderThread;

    iget-object v2, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    invoke-interface {v2}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getDataToLoadImage()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/util/BitmapLoaderThread;->loadImage(Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 120
    if-nez v2, :cond_2

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    invoke-interface {v0}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->onLoadFailed()V

    .line 140
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->this$0:Lcom/sgiggle/production/util/BitmapLoaderThread;

    iget-object v0, v0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskedQueueIds:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 141
    :cond_1
    return-void

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->this$0:Lcom/sgiggle/production/util/BitmapLoaderThread;

    iget-object v0, v0, Lcom/sgiggle/production/util/BitmapLoaderThread;->m_imageMap:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v2}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v3}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v3, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_model:Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;

    invoke-interface {v3}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getViewTag()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->m_view:Landroid/widget/ImageView;

    new-instance v3, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable$1;

    invoke-direct {v3, p0, v2}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable$1;-><init>(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
