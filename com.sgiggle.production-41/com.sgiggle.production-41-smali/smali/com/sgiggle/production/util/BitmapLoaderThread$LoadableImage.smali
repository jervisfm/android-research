.class public interface abstract Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;
.super Ljava/lang/Object;
.source "BitmapLoaderThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/util/BitmapLoaderThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LoadableImage"
.end annotation


# virtual methods
.method public abstract getDataToLoadImage()Ljava/lang/Object;
.end method

.method public abstract getImageId()Ljava/lang/Object;
.end method

.method public abstract getViewTag()I
.end method

.method public abstract onLoadFailed()V
.end method

.method public abstract shouldHaveImage()Z
.end method
