.class public abstract Lcom/sgiggle/production/util/BitmapLoaderThread;
.super Ljava/lang/Thread;
.source "BitmapLoaderThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;,
        Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.BitmapLoaderThread"


# instance fields
.field protected m_imageMap:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field public stop:Z

.field public taskQueue:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;",
            ">;"
        }
    .end annotation
.end field

.field public taskedQueueIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 26
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    .line 31
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskedQueueIds:Ljava/util/Set;

    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->stop:Z

    .line 39
    iput-object p1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->m_imageMap:Ljava/util/concurrent/ConcurrentHashMap;

    .line 40
    return-void
.end method


# virtual methods
.method protected abstract loadImage(Ljava/lang/Object;)Landroid/graphics/Bitmap;
.end method

.method public queueTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)Z
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    monitor-enter v0

    .line 152
    :try_start_0
    invoke-interface {p1}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;->getImageId()Ljava/lang/Object;

    move-result-object v1

    .line 153
    iget-object v2, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskedQueueIds:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    const-string v2, "Tango.BitmapLoaderThread"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "queueTask: ignoring, task already queued for id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v1, 0x0

    monitor-exit v0

    move v0, v1

    .line 163
    :goto_0
    return v0

    .line 158
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskedQueueIds:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    new-instance v1, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;

    invoke-direct {v1, p0, p1, p2}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;-><init>(Lcom/sgiggle/production/util/BitmapLoaderThread;Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 161
    iget-object v2, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 163
    const/4 v1, 0x1

    monitor-exit v0

    move v0, v1

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 2

    .prologue
    .line 177
    :cond_0
    :try_start_0
    iget-boolean v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->stop:Z

    if-nez v0, :cond_3

    .line 179
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    monitor-enter v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :try_start_1
    iget-object v1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V

    .line 182
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    iget-object v1, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 189
    :try_start_3
    iget-object v0, p0, Lcom/sgiggle/production/util/BitmapLoaderThread;->taskQueue:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;

    .line 190
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 192
    if-eqz v0, :cond_2

    .line 193
    :try_start_4
    invoke-virtual {v0}, Lcom/sgiggle/production/util/BitmapLoaderThread$LoadBitmapRunnable;->run()V

    .line 197
    :cond_2
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    :cond_3
    :goto_0
    return-void

    .line 182
    :catchall_0
    move-exception v1

    :try_start_5
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v1
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0

    .line 202
    :catch_0
    move-exception v0

    goto :goto_0

    .line 190
    :catchall_1
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
.end method
