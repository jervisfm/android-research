.class public Lcom/sgiggle/production/util/BitmapOrientationDetector;
.super Ljava/lang/Object;
.source "BitmapOrientationDetector.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BitmapOrientationDetector"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static exifOrientationToDegrees(I)I
    .locals 1
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x6

    if-ne p0, v0, :cond_0

    .line 43
    const/16 v0, 0x5a

    .line 49
    :goto_0
    return v0

    .line 44
    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    .line 45
    const/16 v0, 0xb4

    goto :goto_0

    .line 46
    :cond_1
    const/16 v0, 0x8

    if-ne p0, v0, :cond_2

    .line 47
    const/16 v0, 0x10e

    goto :goto_0

    .line 49
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getOrientationOfImage(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 20
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "content"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    new-array v2, v2, [Ljava/lang/String;

    const-string v0, "orientation"

    aput-object v0, v2, v6

    .line 22
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 24
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 25
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 38
    :goto_0
    return v0

    .line 27
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29
    :try_start_0
    new-instance v0, Landroid/media/ExifInterface;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 30
    const-string v1, "Orientation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/production/util/BitmapOrientationDetector;->exifOrientationToDegrees(I)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    const-string v1, "BitmapOrientationDetector"

    const-string v2, "Error checking exif"

    invoke-static {v1, v2, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move v0, v6

    .line 38
    goto :goto_0
.end method
