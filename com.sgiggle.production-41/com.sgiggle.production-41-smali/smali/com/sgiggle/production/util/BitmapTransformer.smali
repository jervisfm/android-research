.class public Lcom/sgiggle/production/util/BitmapTransformer;
.super Ljava/lang/Object;
.source "BitmapTransformer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;
    }
.end annotation


# static fields
.field private static final BITMAP_PIXEL_LIMIT_FOR_MEMORY:I = 0x16e360

.field private static final JPEG_FILE_QUALITY:I = 0x1e

.field private static final TAG:Ljava/lang/String; = "com.sgiggle.production.util.BitmapTransformer"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    return-void
.end method

.method public static downscale(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/high16 v8, 0x3f00

    const/4 v7, 0x1

    .line 94
    if-nez p0, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    .line 97
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 98
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 99
    if-le p1, v0, :cond_7

    move v2, v0

    .line 102
    :goto_1
    if-le p2, v1, :cond_6

    move v3, v1

    .line 106
    :goto_2
    int-to-float v4, v2

    int-to-float v5, v0

    div-float/2addr v4, v5

    .line 107
    int-to-float v5, v3

    int-to-float v6, v1

    div-float/2addr v5, v6

    .line 109
    if-eqz p3, :cond_4

    .line 110
    cmpg-float v6, v4, v5

    if-gez v6, :cond_1

    move v4, v5

    .line 115
    :cond_1
    :goto_3
    int-to-float v0, v0

    mul-float/2addr v0, v4

    add-float/2addr v0, v8

    float-to-int v0, v0

    .line 116
    int-to-float v1, v1

    mul-float/2addr v1, v4

    add-float/2addr v1, v8

    float-to-int v1, v1

    .line 117
    if-ge v0, v7, :cond_2

    move v0, v7

    .line 120
    :cond_2
    if-ge v1, v7, :cond_3

    move v1, v7

    .line 123
    :cond_3
    invoke-static {p0, v0, v1, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 125
    if-eqz p3, :cond_5

    .line 126
    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 127
    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 128
    invoke-static {v4, v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 112
    :cond_4
    cmpg-float v6, v4, v5

    if-ltz v6, :cond_1

    move v4, v5

    goto :goto_3

    :cond_5
    move-object v0, v4

    goto :goto_0

    :cond_6
    move v3, p2

    goto :goto_2

    :cond_7
    move v2, p1

    goto :goto_1
.end method

.method public static loadFile(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 54
    new-instance v0, Lcom/sgiggle/production/util/BitmapTransformer$2;

    invoke-direct {v0, p0, p1}, Lcom/sgiggle/production/util/BitmapTransformer$2;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    .line 60
    invoke-static {v0}, Lcom/sgiggle/production/util/BitmapTransformer;->loadFile(Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static loadFile(Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;)Landroid/graphics/Bitmap;
    .locals 6
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 64
    .line 67
    :try_start_0
    invoke-interface {p0}, Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;->getBitmapInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 68
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 69
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 70
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 71
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    mul-int/2addr v2, v3

    move v3, v2

    move v2, v4

    .line 73
    :goto_0
    const v4, 0x16e360

    if-le v3, v4, :cond_0

    .line 74
    div-int/lit8 v3, v3, 0x4

    .line 75
    mul-int/lit8 v2, v2, 0x2

    goto :goto_0

    .line 77
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 78
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 79
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 82
    invoke-interface {p0}, Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;->getBitmapInputStream()Ljava/io/InputStream;

    move-result-object v0

    .line 83
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 84
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 88
    :goto_1
    return-object v0

    .line 86
    :catch_0
    move-exception v0

    move-object v1, v5

    .line 87
    :goto_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 88
    goto :goto_1

    .line 86
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public static loadFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 44
    new-instance v0, Lcom/sgiggle/production/util/BitmapTransformer$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/util/BitmapTransformer$1;-><init>(Ljava/lang/String;)V

    .line 50
    invoke-static {v0}, Lcom/sgiggle/production/util/BitmapTransformer;->loadFile(Lcom/sgiggle/production/util/BitmapTransformer$IBitmapStreamSource;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static rotate(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 135
    if-nez p0, :cond_0

    .line 136
    const/4 v0, 0x0

    .line 143
    :goto_0
    return-object v0

    .line 138
    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 139
    if-eqz p1, :cond_1

    .line 140
    int-to-float v0, p1

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 143
    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object v0, p0

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static saveFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x0

    .line 27
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 28
    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x1e

    invoke-virtual {p0, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 34
    if-eqz v1, :cond_0

    .line 35
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    const-string v0, "com.sgiggle.production.util.BitmapTransformer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "can\'t save file : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 29
    :catch_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 30
    :goto_1
    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 33
    :catchall_0
    move-exception v0

    .line 34
    :goto_2
    if-eqz v1, :cond_1

    .line 35
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 39
    :cond_1
    :goto_3
    throw v0

    .line 37
    :catch_2
    move-exception v1

    .line 38
    const-string v1, "com.sgiggle.production.util.BitmapTransformer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "can\'t save file : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 33
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    .line 29
    :catch_3
    move-exception v0

    goto :goto_1
.end method
