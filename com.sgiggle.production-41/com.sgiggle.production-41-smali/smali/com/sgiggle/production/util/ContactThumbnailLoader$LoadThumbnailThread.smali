.class Lcom/sgiggle/production/util/ContactThumbnailLoader$LoadThumbnailThread;
.super Lcom/sgiggle/production/util/BitmapLoaderThread;
.source "ContactThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/util/ContactThumbnailLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadThumbnailThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/util/ContactThumbnailLoader;


# direct methods
.method public constructor <init>(Lcom/sgiggle/production/util/ContactThumbnailLoader;Ljava/util/concurrent/ConcurrentHashMap;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sgiggle/production/util/ContactThumbnailLoader$LoadThumbnailThread;->this$0:Lcom/sgiggle/production/util/ContactThumbnailLoader;

    .line 36
    invoke-direct {p0, p2}, Lcom/sgiggle/production/util/BitmapLoaderThread;-><init>(Ljava/util/concurrent/ConcurrentHashMap;)V

    .line 37
    return-void
.end method


# virtual methods
.method protected loadImage(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter

    .prologue
    .line 41
    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/sgiggle/contacts/ContactStore;->getPhotoByContactId(J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
