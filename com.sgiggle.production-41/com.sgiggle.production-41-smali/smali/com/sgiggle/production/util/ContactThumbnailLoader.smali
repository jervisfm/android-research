.class public Lcom/sgiggle/production/util/ContactThumbnailLoader;
.super Lcom/sgiggle/production/util/BitmapLoader;
.source "ContactThumbnailLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/ContactThumbnailLoader$LoadThumbnailThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ContactThumbnailLoader"

.field private static s_instance:Lcom/sgiggle/production/util/ContactThumbnailLoader;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sgiggle/production/util/BitmapLoader;-><init>()V

    .line 23
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/sgiggle/production/util/ContactThumbnailLoader;->s_instance:Lcom/sgiggle/production/util/ContactThumbnailLoader;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lcom/sgiggle/production/util/ContactThumbnailLoader;

    invoke-direct {v0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;-><init>()V

    sput-object v0, Lcom/sgiggle/production/util/ContactThumbnailLoader;->s_instance:Lcom/sgiggle/production/util/ContactThumbnailLoader;

    .line 31
    :cond_0
    sget-object v0, Lcom/sgiggle/production/util/ContactThumbnailLoader;->s_instance:Lcom/sgiggle/production/util/ContactThumbnailLoader;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/util/BitmapLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 64
    const-string v0, "Tango.ContactThumbnailLoader"

    const-string v1, "clear()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->clear()V

    .line 66
    return-void
.end method

.method protected createLoadBitmapThread()Lcom/sgiggle/production/util/BitmapLoaderThread;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/sgiggle/production/util/ContactThumbnailLoader$LoadThumbnailThread;

    invoke-virtual {p0}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getBitmaps()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/util/ContactThumbnailLoader$LoadThumbnailThread;-><init>(Lcom/sgiggle/production/util/ContactThumbnailLoader;Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method public bridge synthetic getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/sgiggle/production/util/BitmapLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 52
    const-string v0, "Tango.ContactThumbnailLoader"

    const-string v1, "start()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->start()V

    .line 54
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "Tango.ContactThumbnailLoader"

    const-string v1, "stop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->stop()V

    .line 60
    return-void
.end method
