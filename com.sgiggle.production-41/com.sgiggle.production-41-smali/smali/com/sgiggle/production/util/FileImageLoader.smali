.class public Lcom/sgiggle/production/util/FileImageLoader;
.super Lcom/sgiggle/production/util/BitmapLoader;
.source "FileImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/FileImageLoader$LoadFileImageThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.FileImageLoader"

.field private static s_instance:Lcom/sgiggle/production/util/FileImageLoader;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/util/BitmapLoader;-><init>()V

    .line 24
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/util/FileImageLoader;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sgiggle/production/util/FileImageLoader;->s_instance:Lcom/sgiggle/production/util/FileImageLoader;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/sgiggle/production/util/FileImageLoader;

    invoke-direct {v0}, Lcom/sgiggle/production/util/FileImageLoader;-><init>()V

    sput-object v0, Lcom/sgiggle/production/util/FileImageLoader;->s_instance:Lcom/sgiggle/production/util/FileImageLoader;

    .line 32
    :cond_0
    sget-object v0, Lcom/sgiggle/production/util/FileImageLoader;->s_instance:Lcom/sgiggle/production/util/FileImageLoader;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/util/BitmapLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    return-void
.end method

.method public bridge synthetic clear()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->clear()V

    return-void
.end method

.method protected createLoadBitmapThread()Lcom/sgiggle/production/util/BitmapLoaderThread;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/sgiggle/production/util/FileImageLoader$LoadFileImageThread;

    invoke-virtual {p0}, Lcom/sgiggle/production/util/FileImageLoader;->getBitmaps()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/util/FileImageLoader$LoadFileImageThread;-><init>(Lcom/sgiggle/production/util/FileImageLoader;Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method public bridge synthetic getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/sgiggle/production/util/BitmapLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic start()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->start()V

    return-void
.end method

.method public bridge synthetic stop()V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->stop()V

    return-void
.end method
