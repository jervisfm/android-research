.class public Lcom/sgiggle/production/util/HttpImageLoader;
.super Lcom/sgiggle/production/util/BitmapLoader;
.source "HttpImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/HttpImageLoader$LoadHttpImageThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.HttpImageLoader"

.field private static s_instance:Lcom/sgiggle/production/util/HttpImageLoader;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sgiggle/production/util/BitmapLoader;-><init>()V

    .line 25
    return-void
.end method

.method public static getInstance()Lcom/sgiggle/production/util/HttpImageLoader;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/sgiggle/production/util/HttpImageLoader;->s_instance:Lcom/sgiggle/production/util/HttpImageLoader;

    if-nez v0, :cond_0

    .line 30
    new-instance v0, Lcom/sgiggle/production/util/HttpImageLoader;

    invoke-direct {v0}, Lcom/sgiggle/production/util/HttpImageLoader;-><init>()V

    sput-object v0, Lcom/sgiggle/production/util/HttpImageLoader;->s_instance:Lcom/sgiggle/production/util/HttpImageLoader;

    .line 33
    :cond_0
    sget-object v0, Lcom/sgiggle/production/util/HttpImageLoader;->s_instance:Lcom/sgiggle/production/util/HttpImageLoader;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/util/BitmapLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 125
    const-string v0, "Tango.HttpImageLoader"

    const-string v1, "clear()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->clear()V

    .line 127
    return-void
.end method

.method protected createLoadBitmapThread()Lcom/sgiggle/production/util/BitmapLoaderThread;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/sgiggle/production/util/HttpImageLoader$LoadHttpImageThread;

    invoke-virtual {p0}, Lcom/sgiggle/production/util/HttpImageLoader;->getBitmaps()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/production/util/HttpImageLoader$LoadHttpImageThread;-><init>(Lcom/sgiggle/production/util/HttpImageLoader;Ljava/util/concurrent/ConcurrentHashMap;)V

    return-object v0
.end method

.method public bridge synthetic getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/sgiggle/production/util/BitmapLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public start()V
    .locals 2

    .prologue
    .line 113
    const-string v0, "Tango.HttpImageLoader"

    const-string v1, "start()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->start()V

    .line 115
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 119
    const-string v0, "Tango.HttpImageLoader"

    const-string v1, "stop()"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    invoke-super {p0}, Lcom/sgiggle/production/util/BitmapLoader;->stop()V

    .line 121
    return-void
.end method
