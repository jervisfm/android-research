.class public Lcom/sgiggle/production/util/Security;
.super Ljava/lang/Object;
.source "Security.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/util/Security$VerifiedPurchase;
    }
.end annotation


# static fields
.field private static final KEY_FACTORY_ALGORITHM:Ljava/lang/String; = "RSA"

.field private static final RANDOM:Ljava/security/SecureRandom; = null

.field private static final SIGNATURE_ALGORITHM:Ljava/lang/String; = "SHA1withRSA"

.field private static final TAG:Ljava/lang/String; = "Security"

.field private static sKnownNonces:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/sgiggle/production/util/Security;->RANDOM:Ljava/security/SecureRandom;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/sgiggle/production/util/Security;->sKnownNonces:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method public static generateNonce()J
    .locals 4

    .prologue
    .line 79
    sget-object v0, Lcom/sgiggle/production/util/Security;->RANDOM:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    .line 80
    sget-object v2, Lcom/sgiggle/production/util/Security;->sKnownNonces:Ljava/util/HashSet;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 81
    return-wide v0
.end method

.method public static generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;
    .locals 3
    .parameter

    .prologue
    .line 198
    :try_start_0
    invoke-static {p0}, Lcom/sgiggle/production/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v0

    .line 199
    const-string v1, "RSA"

    invoke-static {v1}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 200
    new-instance v2, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v2, v0}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/sgiggle/production/util/Base64DecoderException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 201
    :catch_0
    move-exception v0

    .line 202
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 203
    :catch_1
    move-exception v0

    .line 204
    const-string v1, "Security"

    const-string v2, "Invalid key specification."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 206
    :catch_2
    move-exception v0

    .line 207
    const-string v1, "Security"

    const-string v2, "Base64 decoding failed."

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static isNonceKnown(J)Z
    .locals 2
    .parameter

    .prologue
    .line 89
    sget-object v0, Lcom/sgiggle/production/util/Security;->sKnownNonces:Ljava/util/HashSet;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static removeNonce(J)V
    .locals 2
    .parameter

    .prologue
    .line 85
    sget-object v0, Lcom/sgiggle/production/util/Security;->sKnownNonces:Ljava/util/HashSet;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 86
    return-void
.end method

.method public static verify(Ljava/security/PublicKey;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 223
    const-string v0, "Security"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "signature: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :try_start_0
    const-string v0, "SHA1withRSA"

    invoke-static {v0}, Ljava/security/Signature;->getInstance(Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v0

    .line 228
    invoke-virtual {v0, p0}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 229
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->update([B)V

    .line 230
    invoke-static {p2}, Lcom/sgiggle/production/util/Base64;->decode(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    const-string v0, "Security"

    const-string v1, "Signature verification failed."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/sgiggle/production/util/Base64DecoderException; {:try_start_0 .. :try_end_0} :catch_3

    move v0, v3

    .line 244
    :goto_0
    return v0

    .line 234
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    const-string v0, "Security"

    const-string v1, "NoSuchAlgorithmException."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move v0, v3

    .line 244
    goto :goto_0

    .line 237
    :catch_1
    move-exception v0

    .line 238
    const-string v0, "Security"

    const-string v1, "Invalid key specification."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 239
    :catch_2
    move-exception v0

    .line 240
    const-string v0, "Security"

    const-string v1, "Signature exception."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 241
    :catch_3
    move-exception v0

    .line 242
    const-string v0, "Security"

    const-string v1, "Base64 decoding failed."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static verifyPurchase(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 19
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sgiggle/production/util/Security$VerifiedPurchase;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    if-nez p0, :cond_0

    .line 106
    const-string v3, "Security"

    const-string v4, "data is null"

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const/4 v3, 0x0

    .line 186
    :goto_0
    return-object v3

    .line 110
    :cond_0
    const-string v3, "Security"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "signedData: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v0, v4

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    const/4 v3, 0x0

    .line 113
    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 127
    const-string v3, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArKXV6pVXcaAGaLvj+9F3cSR3tCruAKT0qMYQhz1CnGSZ9Tex1t00GNbOJzmH+4SQI+N1Gij0XGdY4CsS2r6/cpPQMq60HJtb1Zr4UUA8QqPbsQdXAqZA/VO7uVtnVWi1KLUhyAsDxj8BmAr//WSlMniJY/lLdT1k4MLlDHEzYBlXuEGzZhLI7mWex5CvKzymPe1uNCuq55h1WiTtfajlI9tuGnUlgAey4NzU5omMapp+dJrcQrFoNFFZMs6ETBdHxAzu3NdNuSMS5nXjPA6rP7FbZv4DgN/wvPNs2AKaTb+LIRoyjI4ikNCyagkk2oYI9e4iz81FHtd1DJ6+UfiqEwIDAQAB"

    .line 128
    invoke-static {v3}, Lcom/sgiggle/production/util/Security;->generatePublicKey(Ljava/lang/String;)Ljava/security/PublicKey;

    move-result-object v3

    .line 129
    move-object v0, v3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/sgiggle/production/util/Security;->verify(Ljava/security/PublicKey;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 130
    if-nez v3, :cond_1

    .line 131
    const-string v3, "Security"

    const-string v4, "signature does not match data."

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    move v11, v3

    .line 138
    :goto_1
    const/4 v3, 0x0

    .line 141
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    move-object v0, v4

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 144
    const-string v5, "nonce"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v12

    .line 145
    const-string v5, "orders"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 146
    if-eqz v14, :cond_6

    .line 147
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    move v15, v3

    .line 153
    :goto_2
    invoke-static {v12, v13}, Lcom/sgiggle/production/util/Security;->isNonceKnown(J)Z

    move-result v3

    if-nez v3, :cond_2

    .line 154
    const-string v3, "Security"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Nonce not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v3, 0x0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v3

    .line 150
    const/4 v3, 0x0

    goto :goto_0

    .line 158
    :cond_2
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 160
    const/4 v3, 0x0

    move/from16 v17, v3

    :goto_3
    move/from16 v0, v17

    move v1, v15

    if-ge v0, v1, :cond_5

    .line 161
    :try_start_1
    move-object v0, v14

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 162
    const-string v4, "purchaseState"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 163
    invoke-static {v4}, Lcom/sgiggle/production/payments/Constants$PurchaseState;->valueOf(I)Lcom/sgiggle/production/payments/Constants$PurchaseState;

    move-result-object v4

    .line 164
    const-string v5, "productId"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 165
    const-string v5, "purchaseTime"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 166
    const-string v5, "orderId"

    const-string v7, ""

    invoke-virtual {v3, v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 167
    const/4 v5, 0x0

    .line 168
    const-string v10, "notificationId"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 169
    const-string v5, "notificationId"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 171
    :cond_3
    const-string v10, "developerPayload"

    const/16 v18, 0x0

    move-object v0, v3

    move-object v1, v10

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 175
    sget-object v3, Lcom/sgiggle/production/payments/Constants$PurchaseState;->PURCHASED:Lcom/sgiggle/production/payments/Constants$PurchaseState;

    if-ne v4, v3, :cond_4

    if-nez v11, :cond_4

    .line 160
    :goto_4
    add-int/lit8 v3, v17, 0x1

    move/from16 v17, v3

    goto :goto_3

    .line 178
    :cond_4
    new-instance v3, Lcom/sgiggle/production/util/Security$VerifiedPurchase;

    invoke-direct/range {v3 .. v10}, Lcom/sgiggle/production/util/Security$VerifiedPurchase;-><init>(Lcom/sgiggle/production/payments/Constants$PurchaseState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    move-object/from16 v0, v16

    move-object v1, v3

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 181
    :catch_1
    move-exception v3

    .line 182
    const-string v4, "Security"

    const-string v5, "JSON exception: "

    invoke-static {v4, v5, v3}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 183
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 185
    :cond_5
    invoke-static {v12, v13}, Lcom/sgiggle/production/util/Security;->removeNonce(J)V

    move-object/from16 v3, v16

    .line 186
    goto/16 :goto_0

    :cond_6
    move v15, v3

    goto/16 :goto_2

    :cond_7
    move v11, v3

    goto/16 :goto_1
.end method
