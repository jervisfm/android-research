.class public Lcom/sgiggle/production/vendor/htc/IntegrationConstants;
.super Ljava/lang/Object;
.source "IntegrationConstants.java"


# static fields
.field public static final ACTION_CHANGE_WIFI_MODE:Ljava/lang/String; = "com.htc.intent.action.CHANGE_WIFI_MODE"

.field public static final ACTION_PS_AUDIO_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.PS_AUDIO_STATE_CHANGED"

.field public static final ACTION_PS_CALL_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.PS_CALL_STATE_CHANGED"

.field public static final ACTION_SCREEN_OFF:Ljava/lang/String; = "android.intent.action.SCREEN_OFF"

.field public static final ACTION_SCREEN_ON:Ljava/lang/String; = "android.intent.action.SCREEN_ON"

.field public static final ACTION_VoIP_BLUETOOTH:Ljava/lang/String; = "android.intent.action.VoIP_BLUETOOTH"

.field public static final ACTION_VoIP_HEADSET:Ljava/lang/String; = "android.intent.action.VoIP_HEADSET"

.field public static final ACTION_VoIP_PRESENCE_SYNC:Ljava/lang/String; = "com.android.contacts.im.VoIP.SYNC"

.field public static final ACTION_VoIP_RESUME_CALL:Ljava/lang/String; = "android.intent.action.VoIP_RESUME_CALL"

.field public static final BUTTON_ACCEPT:I = 0x1

.field public static final BUTTON_REJECT:I = 0x3

.field public static final CS_CALL_RESUME:I = 0x2

.field public static final HTC_LOCKSCREEN_CLASS:Ljava/lang/String; = "com.android.phone.VoIPIncomingCallArc"

.field public static final HTC_LOCKSCREEN_PACKAGE:Ljava/lang/String; = "com.android.phone"

.field public static final KEY_MODE:Ljava/lang/String; = "comeIn"

.field public static final MIMETYPE_CHAT_CAPABILITY:Ljava/lang/String; = "com.android.htccontacts/chat_capability"

.field public static final PARAM_AUDIO_SPEAKER:Ljava/lang/String; = "AudioType"

.field public static final PARAM_CALL_TYPE:Ljava/lang/String; = "call_type"

.field public static final PARAM_EVENT:Ljava/lang/String; = "Event"

.field public static final PARAM_LOCKSCREEN_RESULT:Ljava/lang/String; = "result"

.field public static final PARAM_PS_CALLER_AVATAR:Ljava/lang/String; = "photo"

.field public static final PARAM_PS_CALLER_ID:Ljava/lang/String; = "id"

.field public static final PARAM_PS_CALLER_NAME:Ljava/lang/String; = "name"

.field public static final PARAM_PS_CALL_STATE:Ljava/lang/String; = "state"

.field public static final PARAM_RESUME_TYPE:Ljava/lang/String; = "ResumeType"

.field public static final PARAM_STATE:Ljava/lang/String; = "state"

.field public static final PRESENCE_AVAILABLE_VIDEO:I = 0xa

.field public static final PS_CALL_RESUME:I = 0x1

#the value of this static final field might be set in the static constructor
.field public static final SUPPORT_CALL_WAITING:Z = false

.field public static final VoIP_CALL_ACCEPTED_AUDIO:I = 0x1

.field public static final VoIP_CALL_ACCEPTED_VIDEO:I = 0x2

.field public static final VoIP_CALL_REJECTED:I = 0x3

.field public static final VoIP_CALL_TYPE_AUDIO:I = 0x1

.field public static final VoIP_CALL_TYPE_VIDEO:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "HTC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Bliss"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Rhyme"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Runnymede"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "HTC Sensation XL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sgiggle/production/vendor/htc/IntegrationConstants;->SUPPORT_CALL_WAITING:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
