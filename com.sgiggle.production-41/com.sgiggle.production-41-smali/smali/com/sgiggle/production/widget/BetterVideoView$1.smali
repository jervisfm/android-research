.class Lcom/sgiggle/production/widget/BetterVideoView$1;
.super Landroid/os/Handler;
.source "BetterVideoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/BetterVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/BetterVideoView;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/widget/BetterVideoView;)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 87
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    const-string v0, "Tango.BetterVideoView"

    const-string v1, "Stopping short playback"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #calls: Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V
    invoke-static {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->access$000(Lcom/sgiggle/production/widget/BetterVideoView;)V

    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #setter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z
    invoke-static {v0, v2}, Lcom/sgiggle/production/widget/BetterVideoView;->access$102(Lcom/sgiggle/production/widget/BetterVideoView;Z)Z

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    iget-object v1, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_positionBeforeShortPlay:I
    invoke-static {v1}, Lcom/sgiggle/production/widget/BetterVideoView;->access$200(Lcom/sgiggle/production/widget/BetterVideoView;)I

    move-result v1

    #calls: Lcom/sgiggle/production/widget/BetterVideoView;->pauseAt(I)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->access$300(Lcom/sgiggle/production/widget/BetterVideoView;I)V

    .line 99
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;
    invoke-static {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->access$400(Lcom/sgiggle/production/widget/BetterVideoView;)Lcom/sgiggle/production/widget/VideomailMediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;
    invoke-static {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->access$400(Lcom/sgiggle/production/widget/BetterVideoView;)Lcom/sgiggle/production/widget/VideomailMediaController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sgiggle/production/widget/VideomailMediaController;->setProgressUiFrozen(Z)V

    goto :goto_0

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    invoke-static {v0}, Lcom/sgiggle/production/widget/BetterVideoView;->access$508(Lcom/sgiggle/production/widget/BetterVideoView;)I

    .line 106
    const-string v0, "Tango.BetterVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Retrying playback - Attempt "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I
    invoke-static {v2}, Lcom/sgiggle/production/widget/BetterVideoView;->access$500(Lcom/sgiggle/production/widget/BetterVideoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_maxVideoPrepareRetry:I
    invoke-static {v2}, Lcom/sgiggle/production/widget/BetterVideoView;->access$600(Lcom/sgiggle/production/widget/BetterVideoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    iget-object v1, p0, Lcom/sgiggle/production/widget/BetterVideoView$1;->this$0:Lcom/sgiggle/production/widget/BetterVideoView;

    #getter for: Lcom/sgiggle/production/widget/BetterVideoView;->m_videoUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sgiggle/production/widget/BetterVideoView;->access$700(Lcom/sgiggle/production/widget/BetterVideoView;)Landroid/net/Uri;

    move-result-object v1

    #calls: Lcom/sgiggle/production/widget/BetterVideoView;->setVideoUriInternal(Landroid/net/Uri;)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/widget/BetterVideoView;->access$800(Lcom/sgiggle/production/widget/BetterVideoView;Landroid/net/Uri;)V

    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
