.class public Lcom/sgiggle/production/widget/BetterVideoView;
.super Landroid/widget/VideoView;
.source "BetterVideoView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;
    }
.end annotation


# static fields
.field private static final MSG_RETRY_VIDEO_PREPARE:I = 0x2

.field private static final MSG_STOP_SHORT_PLAYBACK:I = 0x1

.field private static final RETRY_PREPARE_DELAY:I = 0x7d0

.field private static final SHORT_PLAYBACK_DURATION:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "Tango.BetterVideoView"


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

.field private m_maxVideoPrepareRetry:I

.field private m_playShortlyWhenReady:Z

.field private m_positionBeforeShortPlay:I

.field private m_shouldBePlaying:Z

.field private m_surfacePrepared:Z

.field private m_videoPrepareRetryCount:I

.field private m_videoPrepared:Z

.field private m_videoUri:Landroid/net/Uri;

.field private m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 165
    invoke-direct {p0, p1, p2}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepared:Z

    .line 46
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_surfacePrepared:Z

    .line 48
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z

    .line 49
    iput v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_positionBeforeShortPlay:I

    .line 52
    iput v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_maxVideoPrepareRetry:I

    .line 53
    iput v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoUri:Landroid/net/Uri;

    .line 85
    new-instance v0, Lcom/sgiggle/production/widget/BetterVideoView$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/BetterVideoView$1;-><init>(Lcom/sgiggle/production/widget/BetterVideoView;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_handler:Landroid/os/Handler;

    .line 166
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 169
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/widget/BetterVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 170
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/widget/BetterVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 171
    invoke-virtual {p0, p0}, Lcom/sgiggle/production/widget/BetterVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 172
    return-void
.end method

.method private abortShortPlayback()V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z

    .line 237
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/widget/BetterVideoView;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    return-void
.end method

.method static synthetic access$102(Lcom/sgiggle/production/widget/BetterVideoView;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sgiggle/production/widget/BetterVideoView;)I
    .locals 1
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_positionBeforeShortPlay:I

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/widget/BetterVideoView;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/BetterVideoView;->pauseAt(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sgiggle/production/widget/BetterVideoView;)Lcom/sgiggle/production/widget/VideomailMediaController;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/widget/BetterVideoView;)I
    .locals 1
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    return v0
.end method

.method static synthetic access$508(Lcom/sgiggle/production/widget/BetterVideoView;)I
    .locals 2
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/widget/BetterVideoView;)I
    .locals 1
    .parameter

    .prologue
    .line 31
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_maxVideoPrepareRetry:I

    return v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/widget/BetterVideoView;)Landroid/net/Uri;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sgiggle/production/widget/BetterVideoView;Landroid/net/Uri;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoUriInternal(Landroid/net/Uri;)V

    return-void
.end method

.method private checkPlayShortly()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 187
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    .line 215
    :cond_1
    :goto_0
    return-void

    .line 192
    :cond_2
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepared:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_surfacePrepared:Z

    if-eqz v0, :cond_1

    .line 194
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    .line 195
    const-string v0, "Tango.BetterVideoView"

    const-string v1, "Starting short playback for few ms."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->getCurrentPosition()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_positionBeforeShortPlay:I

    .line 199
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_positionBeforeShortPlay:I

    const/16 v1, 0x1f4

    sub-int/2addr v0, v1

    .line 200
    if-gez v0, :cond_3

    .line 201
    const/4 v0, 0x0

    .line 205
    :cond_3
    iget-object v1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    if-eqz v1, :cond_4

    .line 206
    iget-object v1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v1, v3}, Lcom/sgiggle/production/widget/VideomailMediaController;->setProgressUiFrozen(Z)V

    .line 209
    :cond_4
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 210
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->start()V

    .line 213
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_handler:Landroid/os/Handler;

    const-wide/16 v1, 0x1f4

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private pauseAt(I)V
    .locals 1
    .parameter

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->getDuration()I

    move-result v0

    .line 154
    if-le p1, v0, :cond_0

    .line 160
    :goto_0
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/BetterVideoView;->seekTo(I)V

    .line 161
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->pause()V

    .line 162
    return-void

    .line 156
    :cond_0
    if-gez p1, :cond_1

    .line 157
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v0, p1

    goto :goto_0
.end method

.method private resetRetryCount()V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    .line 146
    return-void
.end method

.method private setVideoPrepared(Z)V
    .locals 0
    .parameter

    .prologue
    .line 244
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepared:Z

    .line 245
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->checkPlayShortly()V

    .line 246
    return-void
.end method

.method private setVideoUriInternal(Landroid/net/Uri;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoUri:Landroid/net/Uri;

    .line 82
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setVideoURI(Landroid/net/Uri;)V

    .line 83
    return-void
.end method


# virtual methods
.method public forcePreview()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_playShortlyWhenReady:Z

    .line 179
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->checkPlayShortly()V

    .line 180
    return-void
.end method

.method public init(Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    .line 66
    iput p2, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_maxVideoPrepareRetry:I

    .line 67
    return-void
.end method

.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 343
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    .line 344
    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoPrepared(Z)V

    .line 345
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->finishedPlaying()V

    .line 347
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;->onCompletion()V

    .line 350
    :cond_0
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 305
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoPrepared(Z)V

    .line 306
    const-string v0, ""

    .line 309
    sparse-switch p2, :sswitch_data_0

    .line 321
    :goto_0
    const-string v1, "Tango.BetterVideoView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error occurred during playback: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 324
    iget v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    iget v1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_maxVideoPrepareRetry:I

    if-ge v0, v1, :cond_0

    .line 325
    const-string v0, "Tango.BetterVideoView"

    const-string v1, "Retrying video prepare in 2000 ms"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v4

    .line 338
    :goto_1
    return v0

    .line 311
    :sswitch_0
    const-string v0, "The video is streamed and its container is not valid for progressive playback i.e the video\'s index (e.g moov atom) is not at the start of the file."

    goto :goto_0

    .line 314
    :sswitch_1
    const-string v0, "Media server died. In this case, the application must release the MediaPlayer object and instantiate a new one."

    goto :goto_0

    .line 317
    :sswitch_2
    const-string v0, "Unspecified media player error."

    goto :goto_0

    .line 331
    :cond_0
    const-string v0, "Tango.BetterVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video prepare failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videoPrepareRetryCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " times. Max reached, won\'t retry."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    if-eqz v0, :cond_2

    .line 336
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;->onError()V

    :cond_2
    move v0, v4

    .line 338
    goto :goto_1

    .line 309
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x64 -> :sswitch_1
        0xc8 -> :sswitch_0
    .end sparse-switch
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 267
    invoke-direct {p0, v3}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoPrepared(Z)V

    .line 273
    new-instance v0, Lcom/sgiggle/production/widget/BetterVideoView$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/BetterVideoView$2;-><init>(Lcom/sgiggle/production/widget/BetterVideoView;)V

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 282
    new-instance v0, Lcom/sgiggle/production/widget/BetterVideoView$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/BetterVideoView$3;-><init>(Lcom/sgiggle/production/widget/BetterVideoView;)V

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 290
    const-string v0, "Tango.BetterVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->getDuration()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0, v3}, Lcom/sgiggle/production/widget/VideomailMediaController;->setEnabled(Z)V

    .line 292
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->showAndStayIfShort()V

    .line 294
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_listener:Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/BetterVideoView$BetterVideoViewListener;->onPrepared()V

    .line 298
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    if-eqz v0, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->start()V

    .line 301
    :cond_1
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    .line 220
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    .line 221
    invoke-super {p0}, Landroid/widget/VideoView;->pause()V

    .line 222
    return-void
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .locals 1
    .parameter

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    if-eq p1, v0, :cond_0

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    .line 122
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 123
    return-void
.end method

.method public setVideoURI(Landroid/net/Uri;)V
    .locals 3
    .parameter

    .prologue
    .line 71
    const-string v0, "Tango.BetterVideoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->resetRetryCount()V

    .line 77
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/BetterVideoView;->setVideoUriInternal(Landroid/net/Uri;)V

    .line 78
    return-void
.end method

.method public setVideomailMediaController(Lcom/sgiggle/production/widget/VideomailMediaController;)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_videomailMediaController:Lcom/sgiggle/production/widget/VideomailMediaController;

    .line 131
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/widget/BetterVideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 132
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    .line 137
    invoke-super {p0}, Landroid/widget/VideoView;->start()V

    .line 138
    return-void
.end method

.method public stopPlayback()V
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_shouldBePlaying:Z

    .line 227
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    .line 228
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->resetRetryCount()V

    .line 229
    invoke-super {p0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 230
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 250
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_surfacePrepared:Z

    .line 251
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->checkPlayShortly()V

    .line 252
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .parameter

    .prologue
    .line 257
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .parameter

    .prologue
    .line 261
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/BetterVideoView;->m_surfacePrepared:Z

    .line 262
    invoke-direct {p0}, Lcom/sgiggle/production/widget/BetterVideoView;->abortShortPlayback()V

    .line 263
    return-void
.end method
