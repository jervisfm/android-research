.class public Lcom/sgiggle/production/widget/BetterViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "BetterViewPager.java"


# instance fields
.field private m_scrollableChildResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/BetterViewPager;->m_scrollableChildResId:I

    .line 24
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter

    .prologue
    .line 28
    iget v0, p0, Lcom/sgiggle/production/widget/BetterViewPager;->m_scrollableChildResId:I

    if-lez v0, :cond_0

    .line 29
    iget v0, p0, Lcom/sgiggle/production/widget/BetterViewPager;->m_scrollableChildResId:I

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/BetterViewPager;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 33
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 40
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public resetScrollableChildId()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/BetterViewPager;->m_scrollableChildResId:I

    .line 54
    return-void
.end method

.method public setScrollableChildResId(I)V
    .locals 0
    .parameter

    .prologue
    .line 49
    iput p1, p0, Lcom/sgiggle/production/widget/BetterViewPager;->m_scrollableChildResId:I

    .line 50
    return-void
.end method
