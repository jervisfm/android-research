.class public Lcom/sgiggle/production/widget/ConversationListItemView;
.super Landroid/widget/RelativeLayout;
.source "ConversationListItemView.java"


# instance fields
.field private final COLOR_READ:I

.field private final COLOR_UNREAD:I

.field private final JUST_NOW_LABEL:Ljava/lang/String;

.field private m_badge:Landroid/widget/TextView;

.field private m_contactThumbnail:Landroid/widget/ImageView;

.field private m_context:Landroid/content/Context;

.field private m_statusIcon:Landroid/view/View;

.field private m_summary:Landroid/widget/TextView;

.field private m_summarySecondary:Landroid/widget/TextView;

.field private m_timestamp:Landroid/widget/TextView;

.field private m_title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    iput-object p1, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_context:Landroid/content/Context;

    .line 56
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 57
    const v1, 0x7f030020

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->COLOR_READ:I

    .line 61
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->COLOR_UNREAD:I

    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_context:Landroid/content/Context;

    const v1, 0x7f090133

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->JUST_NOW_LABEL:Ljava/lang/String;

    .line 64
    const v0, 0x7f0a0091

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    .line 65
    const v0, 0x7f0a0092

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_title:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0a0093

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_timestamp:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0a0097

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_summary:Landroid/widget/TextView;

    .line 68
    const v0, 0x7f0a0098

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_summarySecondary:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a0094

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_statusIcon:Landroid/view/View;

    .line 70
    const v0, 0x7f0a0099

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ConversationListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_badge:Landroid/widget/TextView;

    .line 71
    return-void
.end method


# virtual methods
.method public fill(Landroid/content/Context;Lcom/sgiggle/production/model/ConversationSummary;)V
    .locals 10
    .parameter
    .parameter

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 79
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getLastMessage()Lcom/sgiggle/production/model/ConversationMessage;

    move-result-object v0

    .line 82
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getUnreadMessageCount()I

    move-result v1

    if-lez v1, :cond_0

    move v1, v8

    .line 83
    :goto_0
    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/model/ConversationMessage;->getSummaryPrimaryText(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v2

    .line 84
    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/model/ConversationMessage;->getSummarySecondaryText(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v3

    .line 86
    if-eqz v1, :cond_1

    .line 87
    iget v4, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->COLOR_UNREAD:I

    .line 93
    :goto_1
    iget-object v5, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_title:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getDisplayString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v5, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_summary:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    iget-object v2, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_summary:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    iget-object v2, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_summarySecondary:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    iget-object v2, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_timestamp:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ConversationListItemView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->getTimestampMs()J

    move-result-wide v4

    iget-object v6, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->JUST_NOW_LABEL:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, Lcom/sgiggle/production/Utils;->getRelativeTimeString(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    invoke-virtual {v0}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    move v0, v8

    .line 107
    :goto_2
    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_badge:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_statusIcon:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 117
    :goto_3
    const/4 v0, 0x0

    .line 118
    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getPeer()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    const v1, 0x7f020091

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 140
    :goto_4
    return-void

    :cond_0
    move v1, v7

    .line 82
    goto :goto_0

    .line 89
    :cond_1
    iget v4, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->COLOR_READ:I

    goto :goto_1

    :cond_2
    move v0, v7

    .line 106
    goto :goto_2

    .line 111
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_statusIcon:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_badge:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/sgiggle/production/model/ConversationSummary;->getUnreadMessageCount()I

    move-result v1

    invoke-static {v0, v1, v7, p1}, Lcom/sgiggle/production/Utils;->setBadgeCount(Landroid/widget/TextView;IZLandroid/content/Context;)V

    goto :goto_3

    .line 123
    :cond_4
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 124
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    .line 125
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->shouldHaveImage()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 126
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 127
    if-nez v0, :cond_5

    .line 129
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v1, v3}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 134
    :cond_5
    if-eqz v0, :cond_6

    .line 135
    iget-object v1, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_4

    .line 137
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/widget/ConversationListItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    const v1, 0x7f020090

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4
.end method
