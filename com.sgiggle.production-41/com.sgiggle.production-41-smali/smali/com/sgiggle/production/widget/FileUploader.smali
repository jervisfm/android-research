.class public Lcom/sgiggle/production/widget/FileUploader;
.super Landroid/widget/FrameLayout;
.source "FileUploader.java"


# static fields
.field private static final UPDATE_PROGRESS:I = 0x1


# instance fields
.field private m_handler:Landroid/os/Handler;

.field private m_progressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    new-instance v0, Lcom/sgiggle/production/widget/FileUploader$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/FileUploader$1;-><init>(Lcom/sgiggle/production/widget/FileUploader;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_handler:Landroid/os/Handler;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/widget/FileUploader;)Landroid/widget/ProgressBar;
    .locals 1
    .parameter

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_progressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .parameter

    .prologue
    const/high16 v4, 0x4000

    .line 40
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 42
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FileUploader;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 43
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FileUploader;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 44
    iget-object v2, p0, Lcom/sgiggle/production/widget/FileUploader;->m_progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getHeight()I

    move-result v2

    int-to-float v2, v2

    .line 46
    neg-float v3, v0

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 47
    const/high16 v2, -0x3d4c

    div-float/2addr v0, v4

    div-float/2addr v1, v4

    invoke-virtual {p1, v2, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 49
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 51
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 52
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 34
    const v0, 0x7f0a015f

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/FileUploader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_progressBar:Landroid/widget/ProgressBar;

    .line 35
    iget-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 36
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 63
    return-void
.end method

.method public updateProgress(I)V
    .locals 2
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sgiggle/production/widget/FileUploader;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 56
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 57
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 59
    return-void
.end method
