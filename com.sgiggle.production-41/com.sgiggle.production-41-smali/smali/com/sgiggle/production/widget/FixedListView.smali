.class public Lcom/sgiggle/production/widget/FixedListView;
.super Landroid/widget/LinearLayout;
.source "FixedListView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;
    }
.end annotation


# instance fields
.field private m_adapter:Landroid/widget/Adapter;

.field private m_itemClickListener:Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/FixedListView;->init(Landroid/widget/Adapter;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/Adapter;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-direct {p0, p2}, Lcom/sgiggle/production/widget/FixedListView;->init(Landroid/widget/Adapter;)V

    .line 39
    return-void
.end method

.method private fillViews()V
    .locals 5

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FixedListView;->removeAllViews()V

    .line 134
    :cond_1
    return-void

    .line 110
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FixedListView;->removeAllViews()V

    .line 112
    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    .line 116
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 118
    iget-object v2, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    const/4 v3, 0x0

    invoke-interface {v2, v1, v3, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 120
    if-eqz v2, :cond_3

    .line 123
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FixedListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x40a0

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f00

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 124
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/FixedListView;->addView(Landroid/view/View;)V

    .line 125
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const v4, 0x1080062

    invoke-virtual {v2, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 127
    invoke-virtual {v2, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 129
    const/4 v2, 0x1

    sub-int v2, v0, v2

    if-ge v1, v2, :cond_3

    .line 130
    invoke-direct {p0}, Lcom/sgiggle/production/widget/FixedListView;->getSeperator()Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/FixedListView;->addView(Landroid/view/View;)V

    .line 116
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private getSeperator()Landroid/view/View;
    .locals 4

    .prologue
    .line 137
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/FixedListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 138
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 139
    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 141
    return-object v0
.end method

.method private init(Landroid/widget/Adapter;)V
    .locals 1
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/FixedListView;->setOrientation(I)V

    .line 64
    invoke-direct {p0}, Lcom/sgiggle/production/widget/FixedListView;->fillViews()V

    .line 65
    return-void
.end method


# virtual methods
.method public getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_itemClickListener:Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sgiggle/production/widget/FixedListView;->m_itemClickListener:Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;

    invoke-interface {v0, p1}, Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;->onItemClick(Landroid/view/View;)V

    .line 153
    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/sgiggle/production/widget/FixedListView;->fillViews()V

    .line 94
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sgiggle/production/widget/FixedListView;->m_adapter:Landroid/widget/Adapter;

    .line 76
    invoke-direct {p0}, Lcom/sgiggle/production/widget/FixedListView;->fillViews()V

    .line 77
    return-void
.end method

.method public setOnItemClickListener(Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sgiggle/production/widget/FixedListView;->m_itemClickListener:Lcom/sgiggle/production/widget/FixedListView$OnItemClickListener;

    .line 146
    return-void
.end method
