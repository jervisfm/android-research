.class Lcom/sgiggle/production/widget/HorizontalListView$1;
.super Landroid/database/DataSetObserver;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/widget/HorizontalListView;)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    monitor-enter v0

    .line 120
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    const/4 v2, 0x1

    #setter for: Lcom/sgiggle/production/widget/HorizontalListView;->mDataChanged:Z
    invoke-static {v1, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->access$002(Lcom/sgiggle/production/widget/HorizontalListView;Z)Z

    .line 121
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->invalidate()V

    .line 123
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V

    .line 124
    return-void

    .line 121
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onInvalidated()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->reset()V

    .line 129
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->invalidate()V

    .line 130
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$1;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V

    .line 131
    return-void
.end method
