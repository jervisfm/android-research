.class Lcom/sgiggle/production/widget/HorizontalListView$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/HorizontalListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/widget/HorizontalListView;)V
    .locals 0
    .parameter

    .prologue
    .line 419
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/widget/HorizontalListView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sgiggle/production/widget/HorizontalListView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 8
    .parameter

    .prologue
    .line 470
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 471
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v1

    .line 472
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v1, :cond_0

    .line 473
    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v2, v4}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 474
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 475
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v5

    .line 476
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v6

    .line 477
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v7

    .line 478
    invoke-virtual {v0, v3, v6, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 479
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v0, v3, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 480
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$400(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$400(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v3, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v5, v5, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v6}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 488
    :cond_0
    return-void

    .line 472
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    monitor-enter v0

    .line 435
    :try_start_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget v2, v1, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    float-to-int v3, p3

    add-int/2addr v2, v3

    iput v2, v1, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 436
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V

    .line 439
    const/4 v0, 0x1

    return v0

    .line 436
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7
    .parameter

    .prologue
    .line 444
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 445
    const/4 v1, 0x0

    move v6, v1

    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v1

    if-ge v6, v1, :cond_1

    .line 446
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    invoke-virtual {v1, v6}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 447
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 448
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v3

    .line 449
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v4

    .line 450
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    .line 451
    invoke-virtual {v0, v1, v4, v3, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 452
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 453
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$100(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$100(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v3, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v4, v4, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$300(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/sgiggle/production/widget/HorizontalListView;->access$300(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v3, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    iget-object v4, v4, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/sgiggle/production/widget/HorizontalListView$3;->this$0:Lcom/sgiggle/production/widget/HorizontalListView;

    #getter for: Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/sgiggle/production/widget/HorizontalListView;->access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 465
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 445
    :cond_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto/16 :goto_0
.end method
