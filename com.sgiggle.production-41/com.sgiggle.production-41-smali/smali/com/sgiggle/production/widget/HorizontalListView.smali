.class public Lcom/sgiggle/production/widget/HorizontalListView;
.super Landroid/widget/AdapterView;
.source "HorizontalListView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/ListAdapter;

.field public mAlwaysOverrideTouch:Z

.field private mCenterSingleItem:Z

.field protected mCurrentX:I

.field private mDataChanged:Z

.field private mDataObserver:Landroid/database/DataSetObserver;

.field private mDisplayOffset:I

.field private mGesture:Landroid/view/GestureDetector;

.field private mLeftViewIndex:I

.field private mMaxX:I

.field private mMeasureHeightForVisibleOnly:Z

.field protected mNextX:I

.field private mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

.field private mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mPressedChild:Landroid/view/View;

.field private mRemovedViewQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mRightViewIndex:I

.field protected mScroller:Landroid/widget/Scroller;

.field private mSpacing:F

.field private scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAlwaysOverrideTouch:Z

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    .line 54
    iput v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    .line 57
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    .line 58
    iput v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    .line 65
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataChanged:Z

    .line 66
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMeasureHeightForVisibleOnly:Z

    .line 71
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCenterSingleItem:Z

    .line 115
    new-instance v0, Lcom/sgiggle/production/widget/HorizontalListView$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/HorizontalListView$1;-><init>(Lcom/sgiggle/production/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    .line 419
    new-instance v0, Lcom/sgiggle/production/widget/HorizontalListView$3;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/HorizontalListView$3;-><init>(Lcom/sgiggle/production/widget/HorizontalListView;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    .line 75
    invoke-direct {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->initView()V

    .line 78
    new-array v0, v1, [I

    const v1, 0x1010113

    aput v1, v0, v2

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 80
    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    .line 81
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 83
    return-void
.end method

.method static synthetic access$002(Lcom/sgiggle/production/widget/HorizontalListView;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataChanged:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/widget/HorizontalListView;)I
    .locals 1
    .parameter

    .prologue
    .line 49
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    return v0
.end method

.method static synthetic access$300(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/widget/HorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private addAndMeasureChild(Landroid/view/View;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v1, -0x1

    const/high16 v2, -0x8000

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 181
    if-nez v0, :cond_0

    .line 182
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 185
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 186
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getHeight()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->measure(II)V

    .line 188
    return-void
.end method

.method private fillList(I)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 295
    .line 296
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_1

    .line 298
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 300
    :goto_0
    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/widget/HorizontalListView;->fillListRight(II)V

    .line 303
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 304
    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 307
    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/sgiggle/production/widget/HorizontalListView;->fillListLeft(II)V

    .line 309
    return-void

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method private fillListLeft(II)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 331
    move v1, p1

    :goto_0
    add-int v0, v1, p2

    if-lez v0, :cond_0

    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    if-ltz v0, :cond_0

    .line 332
    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v2, v3, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 333
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 334
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    float-to-int v3, v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 335
    iget v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    .line 336
    iget v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    float-to-int v3, v3

    add-int/2addr v0, v3

    sub-int v0, v2, v0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    goto :goto_0

    .line 338
    :cond_0
    return-void
.end method

.method private fillListRight(II)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 312
    move v1, p1

    :goto_0
    add-int v0, v1, p2

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getWidth()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 314
    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-interface {v2, v3, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 315
    const/4 v2, -0x1

    invoke-direct {p0, v0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->addAndMeasureChild(Landroid/view/View;I)V

    .line 316
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    float-to-int v2, v2

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 318
    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    if-ne v1, v2, :cond_0

    .line 319
    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCurrentX:I

    add-int/2addr v1, v0

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    .line 322
    :cond_0
    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    if-gez v1, :cond_1

    .line 323
    const/4 v1, 0x0

    iput v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    .line 325
    :cond_1
    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    move v1, v0

    .line 326
    goto :goto_0

    .line 328
    :cond_2
    return-void
.end method

.method private declared-synchronized initView()V
    .locals 3

    .prologue
    .line 86
    monitor-enter p0

    const/4 v0, -0x1

    :try_start_0
    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    .line 87
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCurrentX:I

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 91
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    .line 92
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    .line 93
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnGesture:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mGesture:Landroid/view/GestureDetector;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private positionItems(I)V
    .locals 8
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 361
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v0

    .line 362
    iget-boolean v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCenterSingleItem:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 363
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 364
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 365
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    .line 366
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getHeight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 367
    div-int/lit8 v4, v3, 0x2

    add-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v5

    invoke-virtual {v0, v2, v4, v1, v3}, Landroid/view/View;->layout(IIII)V

    .line 379
    :cond_0
    return-void

    .line 368
    :cond_1
    if-lez v0, :cond_0

    .line 369
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    .line 370
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    move v1, v0

    move v0, v2

    .line 371
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 372
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 373
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    float-to-int v4, v4

    add-int/2addr v3, v4

    .line 374
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getHeight()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 375
    div-int/lit8 v5, v4, 0x2

    add-int v6, v1, v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v7

    invoke-virtual {v2, v1, v5, v6, v4}, Landroid/view/View;->layout(IIII)V

    .line 376
    add-int/2addr v1, v3

    .line 371
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private removeNonVisibleItems(I)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 341
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 342
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, p1

    if-gtz v1, :cond_0

    .line 343
    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mSpacing:F

    float-to-int v3, v3

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDisplayOffset:I

    .line 344
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 345
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 346
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mLeftViewIndex:I

    .line 347
    invoke-virtual {p0, v5}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 351
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, v4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 352
    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getWidth()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 353
    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRemovedViewQueue:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    .line 354
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->removeViewInLayout(Landroid/view/View;)V

    .line 355
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    sub-int/2addr v0, v4

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mRightViewIndex:I

    .line 356
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v0

    sub-int/2addr v0, v4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 358
    :cond_1
    return-void
.end method


# virtual methods
.method protected dispatchSetPressed(Z)V
    .locals 1
    .parameter

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setPressed(Z)V

    .line 507
    if-nez p1, :cond_0

    .line 508
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    .line 510
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter

    .prologue
    .line 388
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 400
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mGesture:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 401
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 402
    return v0

    .line 390
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    invoke-interface {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;->startScrolling()V

    goto :goto_0

    .line 395
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    invoke-interface {v0}, Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;->isGoingToStop()V

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected findChild(FF)Landroid/view/View;
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 513
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 514
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 515
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 516
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 517
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    .line 518
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    .line 519
    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v6

    .line 520
    invoke-virtual {v0, v3, v5, v4, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 521
    float-to-int v3, p1

    float-to-int v4, p2

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 525
    :goto_1
    return-object v0

    .line 514
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 525
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDown(Landroid/view/MotionEvent;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 415
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 416
    return v1
.end method

.method protected onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 406
    monitor-enter p0

    .line 407
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    neg-float v3, p3

    float-to-int v3, v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    iget v6, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 408
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V

    .line 411
    const/4 v0, 0x1

    return v0

    .line 408
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected declared-synchronized onLayout(ZIIII)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 247
    monitor-enter p0

    :try_start_0
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 249
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 253
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataChanged:Z

    if-eqz v0, :cond_2

    .line 254
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCurrentX:I

    .line 255
    invoke-direct {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->initView()V

    .line 256
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 257
    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 258
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataChanged:Z

    .line 261
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 262
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v0

    .line 263
    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 266
    :cond_3
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    if-gtz v0, :cond_4

    .line 267
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 268
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 270
    :cond_4
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    if-lt v0, v1, :cond_5

    .line 271
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMaxX:I

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    .line 272
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 275
    :cond_5
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCurrentX:I

    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    sub-int/2addr v0, v1

    .line 277
    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->removeNonVisibleItems(I)V

    .line 278
    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->fillList(I)V

    .line 279
    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->positionItems(I)V

    .line 281
    iget v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    iput v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCurrentX:I

    .line 283
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Lcom/sgiggle/production/widget/HorizontalListView$2;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/HorizontalListView$2;-><init>(Lcom/sgiggle/production/widget/HorizontalListView;)V

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onMeasure(II)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 192
    invoke-super {p0, p1, p2}, Landroid/widget/AdapterView;->onMeasure(II)V

    .line 194
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, 0x4000

    if-eq v0, v1, :cond_3

    .line 197
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMeasureHeightForVisibleOnly:Z

    if-eqz v0, :cond_5

    .line 198
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildCount()I

    move-result v0

    .line 199
    if-lez v0, :cond_4

    move v1, v7

    move v2, v7

    .line 200
    :goto_0
    if-ge v1, v0, :cond_1

    .line 201
    invoke-virtual {p0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 202
    invoke-virtual {v3, v7, v7}, Landroid/view/View;->measure(II)V

    .line 203
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    if-le v4, v2, :cond_0

    .line 204
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 200
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 234
    :goto_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    const/high16 v2, -0x8000

    if-ne v1, v2, :cond_2

    .line 235
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 236
    if-ge v1, v0, :cond_2

    move v0, v1

    .line 241
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->setMeasuredDimension(II)V

    .line 243
    :cond_3
    return-void

    .line 207
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_8

    .line 209
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 210
    invoke-virtual {v0, v7, v7}, Landroid/view/View;->measure(II)V

    .line 211
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-le v1, v7, :cond_8

    .line 212
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_1

    .line 219
    :cond_5
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 220
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    move v3, v7

    move v4, v7

    .line 221
    :goto_2
    if-ge v3, v2, :cond_7

    .line 222
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v5

    .line 223
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 224
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v6

    invoke-interface {v6, v3, v0, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 225
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    invoke-virtual {v0, v7, v7}, Landroid/view/View;->measure(II)V

    .line 228
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    if-le v5, v4, :cond_6

    .line 229
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 221
    :goto_3
    add-int/lit8 v3, v3, 0x1

    move v4, v0

    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_3

    :cond_7
    move v0, v4

    goto/16 :goto_1

    :cond_8
    move v0, v7

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 493
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    if-nez v0, :cond_0

    .line 494
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/widget/HorizontalListView;->findChild(FF)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    .line 495
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/HorizontalListView;->setPressed(Z)V

    .line 497
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mPressedChild:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 498
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/HorizontalListView;->setPressed(Z)V

    .line 500
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized reset()V
    .locals 1

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->initView()V

    .line 170
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->removeAllViewsInLayout()V

    .line 171
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized scrollTo(I)V
    .locals 5
    .parameter

    .prologue
    .line 382
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    const/4 v2, 0x0

    iget v3, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mNextX:I

    sub-int v3, p1, v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    .line 383
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 384
    monitor-exit p0

    return-void

    .line 382
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/sgiggle/production/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .parameter

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 151
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 152
    iget-object v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mDataObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 153
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->reset()V

    .line 154
    return-void
.end method

.method public setCenterSingleItem(Z)V
    .locals 0
    .parameter

    .prologue
    .line 529
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mCenterSingleItem:Z

    .line 530
    return-void
.end method

.method public setHeightMeasureMode(Z)V
    .locals 1
    .parameter

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMeasureHeightForVisibleOnly:Z

    if-eq v0, p1, :cond_0

    .line 163
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mMeasureHeightForVisibleOnly:Z

    .line 164
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/HorizontalListView;->requestLayout()V

    .line 166
    :cond_0
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;

    .line 108
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 113
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 103
    return-void
.end method

.method public setScrollListener(Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sgiggle/production/widget/HorizontalListView;->scrollListener:Lcom/sgiggle/cafe/vgood/VGoodButtonsBarView$ScrollListener;

    .line 98
    return-void
.end method

.method public setSelection(I)V
    .locals 0
    .parameter

    .prologue
    .line 177
    return-void
.end method
