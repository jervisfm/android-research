.class Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;
.super Ljava/lang/Object;
.source "MessageListCompoundItemView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sgiggle/production/widget/MessageListCompoundItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/MessageListCompoundItemView;

.field final synthetic val$contact:Lcom/sgiggle/production/model/ConversationContact;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/widget/MessageListCompoundItemView;Lcom/sgiggle/production/model/ConversationContact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;->this$0:Lcom/sgiggle/production/widget/MessageListCompoundItemView;

    iput-object p2, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;->val$contact:Lcom/sgiggle/production/model/ConversationContact;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 133
    const-string v0, "Tango.MessageListCompoundItemView"

    const-string v1, "fill: jumping to contact detail."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v0

    const-string v1, "jingle"

    new-instance v2, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;

    iget-object v3, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;->val$contact:Lcom/sgiggle/production/model/ConversationContact;

    invoke-virtual {v3}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sgiggle/media_engine/MediaEngineMessage$ViewContactDetailMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact;)V

    invoke-virtual {v0, v1, v2}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 136
    return-void
.end method
