.class public abstract Lcom/sgiggle/production/widget/MessageListCompoundItemView;
.super Lcom/sgiggle/production/widget/MessageListItemView;
.source "MessageListCompoundItemView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.MessageListCompoundItemView"


# instance fields
.field protected m_contactName:Landroid/widget/TextView;

.field protected m_contactThumbnail:Landroid/widget/ImageView;

.field protected m_contactThumbnailWrapper:Landroid/view/View;

.field protected m_messageContentPositioner:Landroid/widget/LinearLayout;

.field protected m_messageContentWrapper:Landroid/view/View;

.field protected m_statusIcon:Landroid/widget/ImageView;

.field protected m_statusProgress:Landroid/view/View;

.field protected m_statusWrapper:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x3

    const/4 v8, 0x1

    const/4 v1, 0x5

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 86
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 89
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentPositioner:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 92
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_timestamp:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 93
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusRead:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 95
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentWrapper:Landroid/view/View;

    const v1, 0x7f02010c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 96
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnailWrapper:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactName:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusRead:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 151
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isFromSelf()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 152
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusSending()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 153
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200cb

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 154
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusProgress:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    move v0, v8

    .line 165
    :goto_1
    if-eqz v0, :cond_7

    .line 166
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusWrapper:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 170
    :goto_2
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentPositioner:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 102
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_timestamp:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentWrapper:Landroid/view/View;

    const v1, 0x7f02010b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 107
    const/4 v0, 0x0

    .line 108
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getFromContact()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v1

    .line 109
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 110
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    const v1, 0x7f020091

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 140
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnailWrapper:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactName:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getFromContact()Lcom/sgiggle/production/model/ConversationContact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getDisplayNameShort()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactName:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 113
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 114
    invoke-virtual {v1}, Lcom/sgiggle/production/model/ConversationContact;->shouldHaveImage()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 115
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 116
    if-nez v0, :cond_2

    .line 118
    invoke-static {}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->getInstance()Lcom/sgiggle/production/util/ContactThumbnailLoader;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v1, v3}, Lcom/sgiggle/production/util/ContactThumbnailLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 123
    :cond_2
    if-eqz v0, :cond_3

    .line 124
    iget-object v2, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 130
    :goto_4
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    new-instance v2, Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;

    invoke-direct {v2, p0, v1}, Lcom/sgiggle/production/widget/MessageListCompoundItemView$1;-><init>(Lcom/sgiggle/production/widget/MessageListCompoundItemView;Lcom/sgiggle/production/model/ConversationContact;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 126
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    const v2, 0x7f020090

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_4

    .line 156
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusError()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusIcon:Landroid/widget/ImageView;

    const v1, 0x7f0200ca

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 158
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusProgress:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    move v0, v8

    .line 159
    goto/16 :goto_1

    .line 160
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->isStatusReadShow()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 161
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusRead:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_6
    move v0, v6

    goto/16 :goto_1

    .line 168
    :cond_7
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusWrapper:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method protected abstract getContentResId()I
.end method

.method protected inflateLayout(Landroid/view/LayoutInflater;)V
    .locals 2
    .parameter

    .prologue
    .line 61
    const v0, 0x7f030033

    invoke-virtual {p1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 64
    const v0, 0x7f0a00ce

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 65
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->getContentResId()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 66
    return-void
.end method

.method protected initViews()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "Tango.MessageListCompoundItemView"

    const-string v1, "initViews"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->initViews()V

    .line 74
    const v0, 0x7f0a00cc

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentPositioner:Landroid/widget/LinearLayout;

    .line 75
    const v0, 0x7f0a00cd

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_messageContentWrapper:Landroid/view/View;

    .line 76
    const v0, 0x7f0a00cf

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusWrapper:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusWrapper:Landroid/view/View;

    const v1, 0x7f0a00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusIcon:Landroid/widget/ImageView;

    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusWrapper:Landroid/view/View;

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_statusProgress:Landroid/view/View;

    .line 79
    const v0, 0x7f0a00ca

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactName:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0a00c9

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnailWrapper:Landroid/view/View;

    .line 81
    const v0, 0x7f0a00cb

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->m_contactThumbnail:Landroid/widget/ImageView;

    .line 82
    return-void
.end method
