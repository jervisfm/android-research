.class public Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;
.super Lcom/sgiggle/production/widget/MessageListCompoundItemView;
.source "MessageListCompoundItemViewPicture.java"


# instance fields
.field protected m_downloadProgressBar:Landroid/widget/ProgressBar;

.field protected m_thumbnail:Landroid/widget/ImageView;

.field protected m_uploadProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 54
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 55
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessagePicture;

    .line 58
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getThumbnailLocalPath()Ljava/lang/String;

    move-result-object v0

    .line 59
    const/4 v1, 0x0

    .line 61
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 62
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->shouldHaveImage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/util/FileImageLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 66
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, p1, v2}, Lcom/sgiggle/production/util/FileImageLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 72
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 75
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->isStatusSending()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_downloadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->isLoadingStatusLoading()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    :goto_2
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 79
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_downloadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessagePicture;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 82
    return-void

    :cond_1
    move v1, v4

    .line 75
    goto :goto_1

    :cond_2
    move v1, v4

    .line 76
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method protected getContentResId()I
    .locals 1

    .prologue
    .line 49
    const v0, 0x7f030034

    return v0
.end method

.method protected initViews()V
    .locals 2

    .prologue
    .line 40
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->initViews()V

    .line 41
    const v0, 0x7f0a00d4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_thumbnail:Landroid/widget/ImageView;

    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_thumbnail:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 43
    const v0, 0x7f0a00d7

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    .line 44
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewPicture;->m_downloadProgressBar:Landroid/widget/ProgressBar;

    .line 45
    return-void
.end method
