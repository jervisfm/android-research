.class public Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;
.super Lcom/sgiggle/production/widget/MessageListCompoundItemView;
.source "MessageListCompoundItemViewText.java"


# instance fields
.field protected m_text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method private static ignoreReleaseIfWindowNotFocused(Landroid/widget/TextView;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/widget/TextView;->getAutoLinkMask()I

    move-result v0

    if-lez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText$1;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 71
    :cond_0
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 75
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;->m_text:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    return-void
.end method

.method protected getContentResId()I
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f030035

    return v0
.end method

.method protected initViews()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->initViews()V

    .line 44
    const v0, 0x7f0a00d8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;->m_text:Landroid/widget/TextView;

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 48
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;->m_text:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewText;->ignoreReleaseIfWindowNotFocused(Landroid/widget/TextView;)V

    .line 50
    :cond_0
    return-void
.end method
