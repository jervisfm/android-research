.class public Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;
.super Lcom/sgiggle/production/widget/MessageListCompoundItemView;
.source "MessageListCompoundItemViewVgood.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected m_buyLink:Landroid/widget/TextView;

.field protected m_buyWrapper:Landroid/view/View;

.field protected m_productId:Ljava/lang/String;

.field protected m_thumbnail:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method private setDefaultImage()V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_thumbnail:Landroid/widget/ImageView;

    const v1, 0x7f0200af

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 87
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 64
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessageVGood;

    .line 65
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getVGoodIconPath()Ljava/lang/String;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_2

    .line 67
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/util/FileImageLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 80
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_buyWrapper:Landroid/view/View;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->canBePurchased()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_buyLink:Landroid/widget/TextView;

    const v1, 0x7f09015f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 82
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->getProductId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_productId:Ljava/lang/String;

    .line 83
    return-void

    .line 70
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVGood;->shouldHaveImage()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-direct {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->setDefaultImage()V

    .line 72
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, p1, v1}, Lcom/sgiggle/production/util/FileImageLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 74
    :cond_1
    invoke-direct {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->setDefaultImage()V

    goto :goto_0

    .line 77
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->setDefaultImage()V

    goto :goto_0

    .line 80
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method

.method protected getContentResId()I
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f030036

    return v0
.end method

.method protected initViews()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->initViews()V

    .line 50
    const v0, 0x7f0a00d4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_thumbnail:Landroid/widget/ImageView;

    .line 51
    const v0, 0x7f0a00da

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_buyWrapper:Landroid/view/View;

    .line 52
    const v0, 0x7f0a0029

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_buyLink:Landroid/widget/TextView;

    .line 53
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVgood;->m_buyLink:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 91
    new-instance v0, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;

    invoke-direct {v0}, Lcom/sgiggle/media_engine/MediaEngineMessage$DisplayVgoodPaymentMessage;-><init>()V

    .line 92
    invoke-static {}, Lcom/sgiggle/messaging/MessageRouter;->getInstance()Lcom/sgiggle/messaging/MessageRouter;

    move-result-object v1

    const-string v2, "jingle"

    invoke-virtual {v1, v2, v0}, Lcom/sgiggle/messaging/MessageRouter;->postMessage(Ljava/lang/String;Lcom/sgiggle/messaging/Message;)V

    .line 93
    return-void
.end method
