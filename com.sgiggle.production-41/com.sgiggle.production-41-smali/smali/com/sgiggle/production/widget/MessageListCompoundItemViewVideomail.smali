.class public Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;
.super Lcom/sgiggle/production/widget/MessageListCompoundItemView;
.source "MessageListCompoundItemViewVideomail.java"


# instance fields
.field protected m_playButton:Landroid/view/View;

.field protected m_spinner:Landroid/view/View;

.field protected m_thumbnail:Landroid/widget/ImageView;

.field protected m_thumbnail_wrapper:Landroid/view/View;

.field protected m_uploadProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 58
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 59
    check-cast p1, Lcom/sgiggle/production/model/ConversationMessageVideo;

    .line 62
    const/4 v0, 0x0

    .line 63
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getThumbnailLocalPath()Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->shouldHaveImage()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/production/util/FileImageLoader;->getLoadedBitmap(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 68
    if-nez v0, :cond_0

    .line 70
    invoke-static {}, Lcom/sgiggle/production/util/FileImageLoader;->getInstance()Lcom/sgiggle/production/util/FileImageLoader;

    move-result-object v1

    iget-object v2, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, p1, v2}, Lcom/sgiggle/production/util/FileImageLoader;->addLoadTask(Lcom/sgiggle/production/util/BitmapLoaderThread$LoadableImage;Landroid/widget/ImageView;)V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 79
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isLoadingStatusLoading()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_spinner:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_playButton:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 89
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->isStatusSending()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessageVideo;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 91
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_spinner:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_playButton:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v1, v4

    .line 89
    goto :goto_1
.end method

.method protected getContentResId()I
    .locals 1

    .prologue
    .line 53
    const v0, 0x7f030037

    return v0
.end method

.method protected initViews()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListCompoundItemView;->initViews()V

    .line 44
    const v0, 0x7f0a00d3

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_thumbnail_wrapper:Landroid/view/View;

    .line 45
    const v0, 0x7f0a00d4

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_thumbnail:Landroid/widget/ImageView;

    .line 46
    const v0, 0x7f0a00d6

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_playButton:Landroid/view/View;

    .line 47
    const v0, 0x7f0a00db

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_spinner:Landroid/view/View;

    .line 48
    const v0, 0x7f0a00d7

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListCompoundItemViewVideomail;->m_uploadProgressBar:Landroid/widget/ProgressBar;

    .line 49
    return-void
.end method
