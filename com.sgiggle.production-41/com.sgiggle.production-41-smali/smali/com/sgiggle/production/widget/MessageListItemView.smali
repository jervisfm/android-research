.class public abstract Lcom/sgiggle/production/widget/MessageListItemView;
.super Landroid/widget/RelativeLayout;
.source "MessageListItemView.java"


# instance fields
.field private final JUST_NOW_LABEL:Ljava/lang/String;

.field protected m_context:Landroid/content/Context;

.field protected m_statusRead:Landroid/widget/TextView;

.field protected m_timestamp:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    iput-object p1, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_context:Landroid/content/Context;

    .line 42
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_context:Landroid/content/Context;

    const v1, 0x7f090133

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListItemView;->JUST_NOW_LABEL:Ljava/lang/String;

    .line 45
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 46
    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListItemView;->inflateLayout(Landroid/view/LayoutInflater;)V

    .line 49
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->initViews()V

    .line 50
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090169

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getReadStatusTimestampMs()J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getReadStatusTimestampMs()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sgiggle/production/widget/MessageListItemView;->JUST_NOW_LABEL:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/sgiggle/production/Utils;->getRelativeTimeString(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_statusRead:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_timestamp:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getTimestampMs()J

    move-result-wide v2

    iget-object v4, p0, Lcom/sgiggle/production/widget/MessageListItemView;->JUST_NOW_LABEL:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/sgiggle/production/Utils;->getRelativeTimeString(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    return-void
.end method

.method protected abstract inflateLayout(Landroid/view/LayoutInflater;)V
.end method

.method protected initViews()V
    .locals 1

    .prologue
    .line 57
    const v0, 0x7f0a00c8

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_timestamp:Landroid/widget/TextView;

    .line 58
    const v0, 0x7f0a00d2

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListItemView;->m_statusRead:Landroid/widget/TextView;

    .line 59
    return-void
.end method
