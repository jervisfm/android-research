.class public Lcom/sgiggle/production/widget/MessageListSimpleItemView;
.super Lcom/sgiggle/production/widget/MessageListItemView;
.source "MessageListSimpleItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/MessageListSimpleItemView$1;
    }
.end annotation


# instance fields
.field protected m_duration:Landroid/widget/TextView;

.field protected m_icon:Landroid/widget/ImageView;

.field protected m_text:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/MessageListSimpleItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/MessageListSimpleItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/sgiggle/production/widget/MessageListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/sgiggle/production/widget/MessageListItemView;->fill(Lcom/sgiggle/production/model/ConversationMessage;Z)V

    .line 61
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    .line 63
    const/4 v1, -0x1

    .line 64
    invoke-virtual {p1}, Lcom/sgiggle/production/model/ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v2

    .line 66
    sget-object v3, Lcom/sgiggle/production/widget/MessageListSimpleItemView$1;->$SwitchMap$com$sgiggle$xmpp$SessionMessages$ConversationMessageType:[I

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->ordinal()I

    move-result v0

    aget v0, v3, v0

    .line 81
    if-lez v1, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_icon:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 83
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_icon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 88
    :goto_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_text:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_icon:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected inflateLayout(Landroid/view/LayoutInflater;)V
    .locals 1
    .parameter

    .prologue
    .line 43
    const v0, 0x7f030038

    invoke-virtual {p1, v0, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 44
    return-void
.end method

.method protected initViews()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/sgiggle/production/widget/MessageListItemView;->initViews()V

    .line 51
    const v0, 0x7f0a00dc

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_icon:Landroid/widget/ImageView;

    .line 52
    const v0, 0x7f0a0055

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_text:Landroid/widget/TextView;

    .line 53
    const v0, 0x7f0a00dd

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/MessageListSimpleItemView;->m_duration:Landroid/widget/TextView;

    .line 54
    return-void
.end method
