.class public Lcom/sgiggle/production/widget/RotateButton;
.super Landroid/widget/Button;
.source "RotateButton.java"


# static fields
.field private static final ANIMATION_SPEED:I = 0xb4


# instance fields
.field private m_animationEndTime:J

.field private m_animationStartTime:J

.field private m_currentAngle:I

.field private m_lockUntilLayoutOrientationReached:Z

.field private m_rotateCW:Z

.field private m_startAngle:I

.field private m_targetAngle:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_lockUntilLayoutOrientationReached:Z

    .line 28
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .parameter

    .prologue
    .line 78
    iget v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    iget v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_targetAngle:I

    if-eq v0, v1, :cond_0

    .line 79
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 81
    iget-wide v2, p0, Lcom/sgiggle/production/widget/RotateButton;->m_animationEndTime:J

    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    .line 82
    iget-wide v2, p0, Lcom/sgiggle/production/widget/RotateButton;->m_animationStartTime:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 83
    iget v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_startAngle:I

    iget-boolean v2, p0, Lcom/sgiggle/production/widget/RotateButton;->m_rotateCW:Z

    if-eqz v2, :cond_1

    :goto_0
    mul-int/lit16 v0, v0, 0xb4

    div-int/lit16 v0, v0, 0x3e8

    add-int/2addr v0, v1

    .line 84
    if-ltz v0, :cond_2

    rem-int/lit16 v0, v0, 0x168

    .line 85
    :goto_1
    iput v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    .line 86
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/RotateButton;->invalidate()V

    .line 92
    :cond_0
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 93
    iget v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/RotateButton;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/sgiggle/production/widget/RotateButton;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 94
    invoke-super {p0, p1}, Landroid/widget/Button;->draw(Landroid/graphics/Canvas;)V

    .line 95
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 96
    return-void

    .line 83
    :cond_1
    neg-int v0, v0

    goto :goto_0

    .line 84
    :cond_2
    rem-int/lit16 v0, v0, 0x168

    add-int/lit16 v0, v0, 0x168

    goto :goto_1

    .line 88
    :cond_3
    iget v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_targetAngle:I

    iput v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    goto :goto_2
.end method

.method public lockUntilLayoutOrientationReached(Z)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_lockUntilLayoutOrientationReached:Z

    .line 39
    return-void
.end method

.method public setDegree(I)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 43
    if-ltz p1, :cond_1

    rem-int/lit16 v0, p1, 0x168

    .line 46
    :goto_0
    iget-boolean v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_lockUntilLayoutOrientationReached:Z

    if-eqz v1, :cond_3

    .line 47
    if-eqz v0, :cond_2

    .line 74
    :cond_0
    :goto_1
    return-void

    .line 43
    :cond_1
    rem-int/lit16 v0, p1, 0x168

    add-int/lit16 v0, v0, 0x168

    goto :goto_0

    .line 50
    :cond_2
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/RotateButton;->m_lockUntilLayoutOrientationReached:Z

    .line 53
    :cond_3
    iget v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_targetAngle:I

    if-eq v0, v1, :cond_0

    .line 57
    iput v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_targetAngle:I

    .line 58
    iget v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    iput v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_startAngle:I

    .line 60
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_animationStartTime:J

    .line 62
    iget v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_targetAngle:I

    iget v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_currentAngle:I

    sub-int/2addr v0, v1

    .line 64
    if-ltz v0, :cond_5

    .line 67
    :goto_2
    const/16 v1, 0xb4

    if-le v0, v1, :cond_4

    const/16 v1, 0x168

    sub-int/2addr v0, v1

    .line 69
    :cond_4
    if-ltz v0, :cond_6

    const/4 v1, 0x1

    :goto_3
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_rotateCW:Z

    .line 71
    iget-wide v1, p0, Lcom/sgiggle/production/widget/RotateButton;->m_animationStartTime:J

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    div-int/lit16 v0, v0, 0xb4

    int-to-long v3, v0

    add-long v0, v1, v3

    iput-wide v0, p0, Lcom/sgiggle/production/widget/RotateButton;->m_animationEndTime:J

    .line 73
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/RotateButton;->invalidate()V

    goto :goto_1

    .line 64
    :cond_5
    add-int/lit16 v0, v0, 0x168

    goto :goto_2

    :cond_6
    move v1, v2

    .line 69
    goto :goto_3
.end method
