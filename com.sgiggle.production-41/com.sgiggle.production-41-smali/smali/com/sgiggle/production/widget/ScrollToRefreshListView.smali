.class public Lcom/sgiggle/production/widget/ScrollToRefreshListView;
.super Landroid/widget/ListView;
.source "ScrollToRefreshListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.HistoryListView"


# instance fields
.field private m_headerViewContent:Landroid/view/View;

.field private m_headerViewHeight:I

.field private m_isRefreshing:Z

.field private m_listener:Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;

.field private m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private m_originalTranscriptMode:I

.field private m_previousTotal:I

.field private m_refreshEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 48
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    .line 51
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_refreshEnabled:Z

    .line 61
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewHeight:I

    .line 64
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_originalTranscriptMode:I

    .line 80
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->initialize(Landroid/content/Context;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    .line 51
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_refreshEnabled:Z

    .line 61
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewHeight:I

    .line 64
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_originalTranscriptMode:I

    .line 85
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->initialize(Landroid/content/Context;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    .line 51
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 54
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_refreshEnabled:Z

    .line 61
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewHeight:I

    .line 64
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_originalTranscriptMode:I

    .line 90
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->initialize(Landroid/content/Context;)V

    .line 91
    return-void
.end method

.method private initialize(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getTranscriptMode()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_originalTranscriptMode:I

    .line 101
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 102
    const v1, 0x7f030042

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    const v1, 0x7f0a0058

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewContent:Landroid/view/View;

    .line 105
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 107
    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->measureView(Landroid/view/View;)V

    .line 108
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewHeight:I

    .line 110
    invoke-virtual {p0, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->resetRefreshState(Z)V

    .line 112
    invoke-super {p0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 113
    return-void
.end method

.method private measureView(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 129
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 130
    if-nez v0, :cond_0

    .line 131
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 136
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v3, v3, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 137
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 139
    if-lez v0, :cond_1

    .line 140
    const/high16 v2, 0x4000

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 144
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 145
    return-void

    .line 142
    :cond_1
    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public onRefreshDone(IZ)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 161
    invoke-virtual {p0, p2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setRefreshEnabled(Z)V

    .line 166
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getCount()I

    move-result v0

    .line 167
    add-int/lit8 v1, p1, 0x1

    .line 168
    iget v2, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewHeight:I

    .line 170
    if-ge v1, v0, :cond_0

    .line 171
    const-string v0, "Tango.HistoryListView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSelectionFromTop to: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    invoke-virtual {p0, v1, v2}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setSelectionFromTop(II)V

    .line 175
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 197
    if-nez p2, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_refreshEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    iget v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    if-eq p4, v0, :cond_0

    .line 199
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 200
    iput p4, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    .line 202
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_listener:Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;

    if-eqz v0, :cond_0

    .line 203
    const-string v0, "Tango.HistoryListView"

    const-string v1, "onScroll: requesting refresh"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_listener:Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;->onRefreshRequested()V

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 219
    :cond_1
    iget v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_originalTranscriptMode:I

    if-ne v0, v2, :cond_3

    .line 220
    if-eqz p3, :cond_2

    if-eq p3, p4, :cond_2

    add-int v0, p2, p3

    if-ne v0, p4, :cond_4

    .line 222
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getTranscriptMode()I

    move-result v0

    if-eq v0, v3, :cond_3

    .line 224
    invoke-virtual {p0, v3}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setTranscriptMode(I)V

    .line 233
    :cond_3
    :goto_0
    return-void

    .line 228
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getTranscriptMode()I

    move-result v0

    if-eqz v0, :cond_3

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setTranscriptMode(I)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 238
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 241
    :cond_0
    return-void
.end method

.method public resetRefreshState(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 119
    invoke-virtual {p0, p1}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setRefreshEnabled(Z)V

    .line 120
    iput v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_previousTotal:I

    .line 121
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_isRefreshing:Z

    .line 122
    return-void
.end method

.method public setListener(Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;)V
    .locals 0
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_listener:Lcom/sgiggle/production/widget/ScrollToRefreshListView$ScrollToRefreshListViewListener;

    .line 153
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .parameter

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_onScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 246
    return-void
.end method

.method public setRefreshEnabled(Z)V
    .locals 3
    .parameter

    .prologue
    .line 182
    const-string v0, "Tango.HistoryListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setRefreshAvailable to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    if-eqz p1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewContent:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 190
    :goto_0
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_refreshEnabled:Z

    .line 191
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->m_headerViewContent:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSelectionToBottom()V
    .locals 2

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->getCount()I

    move-result v0

    .line 253
    if-lez v0, :cond_0

    .line 254
    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/ScrollToRefreshListView;->setSelection(I)V

    .line 256
    :cond_0
    return-void
.end method
