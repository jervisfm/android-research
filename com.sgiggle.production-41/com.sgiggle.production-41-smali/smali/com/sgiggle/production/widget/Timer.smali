.class public Lcom/sgiggle/production/widget/Timer;
.super Landroid/widget/FrameLayout;
.source "Timer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/Timer$Callback;
    }
.end annotation


# static fields
.field private static final MESSAGE_UPDATE_COUNTER:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.Timer"


# instance fields
.field private m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

.field private m_duration:J

.field private m_maxDuration:J

.field private m_orientation:I

.field private m_recordingTime:Landroid/widget/TextView;

.field private m_startCallbackAt:J

.field private m_startTime:J

.field private m_timerHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sgiggle/production/widget/Timer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sgiggle/production/widget/Timer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const-wide/16 v0, 0x7530

    iput-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_startCallbackAt:J

    .line 49
    new-instance v0, Lcom/sgiggle/production/widget/Timer$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/Timer$1;-><init>(Lcom/sgiggle/production/widget/Timer;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_timerHandler:Landroid/os/Handler;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/production/widget/Timer;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sgiggle/production/widget/Timer;->updateTimer()V

    return-void
.end method

.method public static formatTime(J)Ljava/lang/String;
    .locals 8
    .parameter

    .prologue
    const-wide/16 v4, 0x3c

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 158
    const-wide/16 v0, 0x3e8

    div-long v0, p0, v0

    .line 162
    div-long v2, v0, v4

    .line 163
    rem-long/2addr v0, v4

    .line 165
    const-string v4, "%02d"

    .line 166
    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 167
    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v4, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 169
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateTimer()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v7, 0x0

    .line 130
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 131
    iget-wide v2, p0, Lcom/sgiggle/production/widget/Timer;->m_startTime:J

    sub-long/2addr v0, v2

    .line 133
    iput-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    .line 134
    iget-object v2, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    iget-wide v3, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    invoke-static {v3, v4}, Lcom/sgiggle/production/widget/Timer;->formatTime(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-wide v2, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    iget-wide v4, p0, Lcom/sgiggle/production/widget/Timer;->m_startCallbackAt:J

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/sgiggle/production/widget/Timer;->m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/sgiggle/production/widget/Timer;->m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

    iget-wide v3, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    iget-wide v5, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    sub-long/2addr v3, v5

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/sgiggle/production/widget/Timer$Callback;->onTimerUpdated(JJ)V

    .line 141
    :cond_0
    iget-wide v2, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    iget-wide v4, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 142
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_timerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_1
    rem-long/2addr v0, v8

    sub-long v0, v8, v0

    .line 147
    iget-object v2, p0, Lcom/sgiggle/production/widget/Timer;->m_timerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v7, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    return-wide v0
.end method

.method public getMaxTimerDuration()J
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    return-wide v0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 127
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 79
    const v0, 0x7f0a0172

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/Timer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    .line 81
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/Timer;->reset()V

    .line 82
    return-void
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    const-wide/16 v1, 0x0

    invoke-static {v1, v2}, Lcom/sgiggle/production/widget/Timer;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    return-void
.end method

.method public setMaxTimerDuration(J)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-wide p1, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    .line 86
    return-void
.end method

.method public setOrientation(I)V
    .locals 3
    .parameter

    .prologue
    .line 173
    rem-int/lit8 v0, p1, 0x5a

    if-nez v0, :cond_1

    .line 174
    rem-int/lit16 v0, p1, 0x168

    iput v0, p0, Lcom/sgiggle/production/widget/Timer;->m_orientation:I

    .line 175
    iget v0, p0, Lcom/sgiggle/production/widget/Timer;->m_orientation:I

    if-gez v0, :cond_0

    .line 176
    iget v0, p0, Lcom/sgiggle/production/widget/Timer;->m_orientation:I

    add-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/sgiggle/production/widget/Timer;->m_orientation:I

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 179
    :cond_1
    const-string v0, "Tango.Timer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid orientation: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setStartCallbackAt(J)V
    .locals 0
    .parameter

    .prologue
    .line 93
    iput-wide p1, p0, Lcom/sgiggle/production/widget/Timer;->m_startCallbackAt:J

    .line 94
    return-void
.end method

.method public setTimerCallback(Lcom/sgiggle/production/widget/Timer$Callback;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sgiggle/production/widget/Timer;->m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

    .line 73
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/Timer;->reset()V

    .line 98
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_startTime:J

    .line 99
    invoke-direct {p0}, Lcom/sgiggle/production/widget/Timer;->updateTimer()V

    .line 100
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    return-void
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_timerHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 105
    const-string v0, "Tango.Timer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Timer stopped at "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_callback:Lcom/sgiggle/production/widget/Timer$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/Timer$Callback;->onTimerStopped()V

    .line 110
    :cond_0
    return-void
.end method

.method public stopAtMaxDuration()V
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/Timer;->stop()V

    .line 116
    iget-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_maxDuration:J

    iput-wide v0, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    .line 117
    iget-object v0, p0, Lcom/sgiggle/production/widget/Timer;->m_recordingTime:Landroid/widget/TextView;

    iget-wide v1, p0, Lcom/sgiggle/production/widget/Timer;->m_duration:J

    invoke-static {v1, v2}, Lcom/sgiggle/production/widget/Timer;->formatTime(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-void
.end method
