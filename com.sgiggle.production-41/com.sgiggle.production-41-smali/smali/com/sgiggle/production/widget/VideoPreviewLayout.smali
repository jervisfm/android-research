.class public Lcom/sgiggle/production/widget/VideoPreviewLayout;
.super Landroid/view/ViewGroup;
.source "VideoPreviewLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;
    }
.end annotation


# instance fields
.field private m_aspectRatio:D

.field private final m_displayMetrics:Landroid/util/DisplayMetrics;

.field private m_frame:Landroid/widget/FrameLayout;

.field private m_sizeListener:Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const-wide v0, 0x3ff5555555555555L

    iput-wide v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    .line 17
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_displayMetrics:Landroid/util/DisplayMetrics;

    .line 21
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_displayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 32
    const v0, 0x7f0a016e

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_frame:Landroid/widget/FrameLayout;

    .line 34
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_frame:Landroid/widget/FrameLayout;

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must provide a child frame layout with id as \"preview_frame\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->getWidth()I

    move-result v0

    .line 51
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->getHeight()I

    move-result v1

    .line 53
    iget-object v2, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_frame:Landroid/widget/FrameLayout;

    .line 55
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    .line 56
    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v2

    add-int/2addr v2, v4

    .line 57
    sub-int/2addr v1, v2

    .line 58
    sub-int/2addr v0, v3

    .line 61
    int-to-double v4, v0

    int-to-double v6, v1

    iget-wide v8, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    mul-double/2addr v6, v8

    cmpl-double v4, v4, v6

    if-lez v4, :cond_1

    .line 62
    int-to-double v4, v1

    iget-wide v6, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0

    add-double/2addr v4, v6

    double-to-int v0, v4

    .line 67
    :goto_0
    add-int/2addr v0, v3

    .line 68
    add-int/2addr v1, v2

    .line 70
    sub-int v2, p4, p2

    sub-int/2addr v2, v0

    div-int/lit8 v2, v2, 0x2

    .line 71
    sub-int v3, p5, p3

    sub-int/2addr v3, v1

    div-int/lit8 v3, v3, 0x2

    .line 73
    iget-object v4, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_frame:Landroid/widget/FrameLayout;

    const/high16 v5, 0x4000

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    const/high16 v5, 0x4000

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/widget/FrameLayout;->measure(II)V

    .line 77
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_frame:Landroid/widget/FrameLayout;

    add-int v1, p2, v2

    add-int v4, p3, v3

    sub-int v2, p4, v2

    sub-int v3, p5, v3

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 79
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_sizeListener:Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_sizeListener:Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;->onSizeChanged()V

    .line 82
    :cond_0
    return-void

    .line 64
    :cond_1
    int-to-double v4, v0

    iget-wide v6, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0

    add-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_0
.end method

.method public setAspectRatio(D)V
    .locals 2
    .parameter

    .prologue
    .line 40
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid ratio"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_0
    iget-wide v0, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    cmpl-double v0, v0, p1

    if-eqz v0, :cond_1

    .line 43
    iput-wide p1, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_aspectRatio:D

    .line 44
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideoPreviewLayout;->requestLayout()V

    .line 46
    :cond_1
    return-void
.end method

.method public setOnSizeChangedListener(Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;)V
    .locals 0
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideoPreviewLayout;->m_sizeListener:Lcom/sgiggle/production/widget/VideoPreviewLayout$OnSizeChangedListener;

    .line 26
    return-void
.end method
