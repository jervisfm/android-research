.class Lcom/sgiggle/production/widget/VideomailMediaController$1;
.super Landroid/os/Handler;
.source "VideomailMediaController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/VideomailMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/VideomailMediaController;


# direct methods
.method constructor <init>(Lcom/sgiggle/production/widget/VideomailMediaController;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController$1;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter

    .prologue
    .line 89
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$1;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$100(Lcom/sgiggle/production/widget/VideomailMediaController;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$1;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    const/4 v1, 0x0

    #calls: Lcom/sgiggle/production/widget/VideomailMediaController;->hide(Z)V
    invoke-static {v0, v1}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$200(Lcom/sgiggle/production/widget/VideomailMediaController;Z)V

    goto :goto_0

    .line 97
    :pswitch_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$1;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #calls: Lcom/sgiggle/production/widget/VideomailMediaController;->setProgress()I
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$300(Lcom/sgiggle/production/widget/VideomailMediaController;)I

    move-result v0

    .line 98
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController$1;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z
    invoke-static {v1}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$400(Lcom/sgiggle/production/widget/VideomailMediaController;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sgiggle/production/widget/VideomailMediaController$1;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 100
    const/16 v2, 0x3e8

    rem-int/lit16 v0, v0, 0x3e8

    sub-int v0, v2, v0

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/sgiggle/production/widget/VideomailMediaController$1;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
