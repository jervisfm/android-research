.class public interface abstract Lcom/sgiggle/production/widget/VideomailMediaController$Callback;
.super Ljava/lang/Object;
.source "VideomailMediaController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/VideomailMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation


# virtual methods
.method public abstract onCancel()V
.end method

.method public abstract onDeleteMessage()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPlay()V
.end method

.method public abstract onReply()V
.end method

.method public abstract onRestart()V
.end method

.method public abstract onRetake()V
.end method

.method public abstract onSend()V
.end method
