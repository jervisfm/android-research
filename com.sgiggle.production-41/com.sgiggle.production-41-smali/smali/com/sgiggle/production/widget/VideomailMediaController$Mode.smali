.class public final enum Lcom/sgiggle/production/widget/VideomailMediaController$Mode;
.super Ljava/lang/Enum;
.source "VideomailMediaController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/VideomailMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Mode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/production/widget/VideomailMediaController$Mode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

.field public static final enum REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

.field public static final enum VIEW_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 119
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    const-string v1, "VIEW_VIDEOMAIL"

    invoke-direct {v0, v1, v2}, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->VIEW_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    .line 120
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    const-string v1, "REVIEW_RECORDED_VIDEOMAIL"

    invoke-direct {v0, v1, v3}, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    .line 118
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    sget-object v1, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->VIEW_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->$VALUES:[Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 118
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/production/widget/VideomailMediaController$Mode;
    .locals 1
    .parameter

    .prologue
    .line 118
    const-class v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/production/widget/VideomailMediaController$Mode;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->$VALUES:[Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    invoke-virtual {v0}, [Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    return-object v0
.end method
