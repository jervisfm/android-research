.class Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;
.super Ljava/lang/Object;
.source "VideomailMediaController.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/production/widget/VideomailMediaController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SeekbarListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sgiggle/production/widget/VideomailMediaController;


# direct methods
.method private constructor <init>(Lcom/sgiggle/production/widget/VideomailMediaController;)V
    .locals 0
    .parameter

    .prologue
    .line 685
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/production/widget/VideomailMediaController;Lcom/sgiggle/production/widget/VideomailMediaController$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 685
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;-><init>(Lcom/sgiggle/production/widget/VideomailMediaController;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 689
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$500(Lcom/sgiggle/production/widget/VideomailMediaController;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 695
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$600(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/widget/MediaController$MediaPlayerControl;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->getDuration()I

    move-result v0

    int-to-long v0, v0

    .line 696
    int-to-long v2, p2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 697
    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;
    invoke-static {v2}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$600(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/widget/MediaController$MediaPlayerControl;

    move-result-object v2

    long-to-int v3, v0

    invoke-interface {v2, v3}, Landroid/widget/MediaController$MediaPlayerControl;->seekTo(I)V

    .line 699
    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_elapsedTime:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$800(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    long-to-int v0, v0

    #calls: Lcom/sgiggle/production/widget/VideomailMediaController;->formatTime(I)Ljava/lang/String;
    invoke-static {v3, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$700(Lcom/sgiggle/production/widget/VideomailMediaController;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .parameter

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$900(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 709
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$900(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 710
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .parameter

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #calls: Lcom/sgiggle/production/widget/VideomailMediaController;->setProgress()I
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$300(Lcom/sgiggle/production/widget/VideomailMediaController;)I

    .line 715
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->updatePlayIcon()V

    .line 717
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    #getter for: Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->access$900(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 720
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;->this$0:Lcom/sgiggle/production/widget/VideomailMediaController;

    invoke-virtual {v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->show()V

    .line 721
    return-void
.end method
