.class public Lcom/sgiggle/production/widget/VideomailMediaController;
.super Landroid/widget/MediaController;
.source "VideomailMediaController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;,
        Lcom/sgiggle/production/widget/VideomailMediaController$Mode;,
        Lcom/sgiggle/production/widget/VideomailMediaController$Callback;
    }
.end annotation


# static fields
.field private static final HIDE_CONTROLLER:I = 0x0

.field private static final TAG:Ljava/lang/String; = "Tango.VideomailMediaController"

.field private static final TIMEOUT:I = 0xfa0

.field private static final UPDATE_PROGRESS:I = 0x1


# instance fields
.field private m_anchor:Landroid/view/View;

.field private m_btnCancel:Landroid/widget/Button;

.field private m_btnDelete:Landroid/widget/Button;

.field private m_btnPlayPause:Landroid/widget/Button;

.field private m_btnReply:Landroid/widget/Button;

.field private m_btnRetake:Landroid/widget/Button;

.field private m_btnSend:Landroid/widget/Button;

.field private m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

.field private m_canSeek:Z

.field private m_elapsedTime:Landroid/widget/TextView;

.field private m_enabled:Z

.field private m_handler:Landroid/os/Handler;

.field private m_inflater:Landroid/view/LayoutInflater;

.field private m_isInitialSeekComplete:Z

.field private m_isProgressUiFrozen:Z

.field private m_mediaControllerView:Landroid/view/View;

.field private m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

.field private m_player:Landroid/widget/MediaController$MediaPlayerControl;

.field private m_progress:Landroid/widget/ProgressBar;

.field private m_seekBarWrapper:Landroid/view/View;

.field private m_seekListener:Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;

.field private m_showAlways:Z

.field private m_showing:Z

.field private m_totalTime:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    invoke-direct {p0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    .line 40
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;

    invoke-direct {v0, p0, v3}, Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;-><init>(Lcom/sgiggle/production/widget/VideomailMediaController;Lcom/sgiggle/production/widget/VideomailMediaController$1;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_seekListener:Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;

    .line 56
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isProgressUiFrozen:Z

    .line 67
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_enabled:Z

    .line 69
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    .line 71
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z

    .line 73
    invoke-static {}, Lcom/sgiggle/production/widget/VideomailMediaController;->isSeekEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    .line 75
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isInitialSeekComplete:Z

    .line 77
    sget-object v0, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->VIEW_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    .line 87
    new-instance v0, Lcom/sgiggle/production/widget/VideomailMediaController$1;

    invoke-direct {v0, p0}, Lcom/sgiggle/production/widget/VideomailMediaController$1;-><init>(Lcom/sgiggle/production/widget/VideomailMediaController;)V

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    .line 132
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_inflater:Landroid/view/LayoutInflater;

    .line 133
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_inflater:Landroid/view/LayoutInflater;

    const v1, 0x7f030069

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    .line 134
    return-void

    :cond_0
    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/sgiggle/production/widget/VideomailMediaController$Mode;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/VideomailMediaController;-><init>(Landroid/content/Context;)V

    .line 146
    iput-boolean p2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z

    .line 147
    iput-object p3, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    .line 148
    return-void
.end method

.method static synthetic access$100(Lcom/sgiggle/production/widget/VideomailMediaController;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sgiggle/production/widget/VideomailMediaController;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/VideomailMediaController;->hide(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sgiggle/production/widget/VideomailMediaController;)I
    .locals 1
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->setProgress()I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sgiggle/production/widget/VideomailMediaController;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    return v0
.end method

.method static synthetic access$500(Lcom/sgiggle/production/widget/VideomailMediaController;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/widget/MediaController$MediaPlayerControl;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sgiggle/production/widget/VideomailMediaController;I)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/VideomailMediaController;->formatTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_elapsedTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sgiggle/production/widget/VideomailMediaController;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    return-object v0
.end method

.method private buildAndAttachControllerView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 245
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_1

    .line 246
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v2, -0x2

    invoke-direct {v1, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 247
    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 249
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    if-ne v0, v2, :cond_0

    .line 250
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    check-cast v0, Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    check-cast v0, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    invoke-virtual {v0, v2, v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 255
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->initControllerView(Landroid/view/View;)V

    .line 257
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_enabled:Z

    if-nez v0, :cond_1

    .line 258
    invoke-direct {p0, v3}, Lcom/sgiggle/production/widget/VideomailMediaController;->hide(Z)V

    .line 261
    :cond_1
    return-void
.end method

.method private delayHide()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 634
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 635
    if-eqz v0, :cond_0

    .line 636
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 637
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 639
    :cond_0
    return-void
.end method

.method private formatTime(I)Ljava/lang/String;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 502
    div-int/lit16 v0, p1, 0x3e8

    .line 506
    div-int/lit8 v1, v0, 0x3c

    .line 507
    rem-int/lit8 v0, v0, 0x3c

    .line 509
    const-string v2, "%02d"

    .line 510
    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 511
    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 513
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hide(Z)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 285
    if-eqz p1, :cond_0

    .line 286
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 287
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 290
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    if-eqz v0, :cond_1

    .line 292
    :try_start_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 293
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->slideOut()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_0
    iput-boolean v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    .line 300
    :cond_1
    return-void

    .line 294
    :catch_0
    move-exception v0

    .line 295
    const-string v0, "Tango.VideomailMediaController"

    const-string v1, "MediaController already removed"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private initControllerView(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 170
    const v0, 0x7f0a016c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    .line 171
    const v0, 0x7f0a0169

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    .line 172
    const v0, 0x7f0a016a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    .line 173
    const v0, 0x7f0a00e4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    .line 174
    const v0, 0x7f0a016b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    .line 175
    const v0, 0x7f0a00e3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnCancel:Landroid/widget/Button;

    .line 177
    const v0, 0x7f0a0166

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    .line 178
    const v0, 0x7f0a0164

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_seekBarWrapper:Landroid/view/View;

    .line 180
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    instance-of v0, v0, Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    check-cast v0, Landroid/widget/SeekBar;

    .line 182
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_seekListener:Lcom/sgiggle/production/widget/VideomailMediaController$SeekbarListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 183
    iget-boolean v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 186
    :cond_0
    const v0, 0x7f0a0168

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_totalTime:Landroid/widget/TextView;

    .line 187
    const v0, 0x7f0a0167

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_elapsedTime:Landroid/widget/TextView;

    .line 189
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    sget-object v1, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    if-ne v0, v1, :cond_1

    .line 193
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnCancel:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 200
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 201
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 203
    iput-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    .line 204
    iput-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    .line 223
    :goto_0
    invoke-direct {p0, v2, v2}, Lcom/sgiggle/production/widget/VideomailMediaController;->setDurationAndPosition(II)V

    .line 224
    return-void

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 209
    iput-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    .line 212
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 215
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 216
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 218
    iput-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    .line 219
    iput-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    goto :goto_0
.end method

.method private static isSeekEnabled()Z
    .locals 2

    .prologue
    .line 725
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v1, "Samsung"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 726
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SPH-D700"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 727
    const/4 v0, 0x0

    .line 731
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private setDurationAndPosition(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_totalTime:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_totalTime:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/sgiggle/production/widget/VideomailMediaController;->formatTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_elapsedTime:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_elapsedTime:Landroid/widget/TextView;

    invoke-direct {p0, p2}, Lcom/sgiggle/production/widget/VideomailMediaController;->formatTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 492
    :cond_1
    return-void
.end method

.method private setProgress()I
    .locals 9

    .prologue
    const-wide/16 v7, 0x3e8

    const/16 v6, 0x3e8

    const/4 v2, 0x0

    .line 437
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    if-nez v0, :cond_0

    move v0, v2

    .line 476
    :goto_0
    return v0

    .line 442
    :cond_0
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isProgressUiFrozen:Z

    if-eqz v0, :cond_1

    .line 443
    const-string v0, "Tango.VideomailMediaController"

    const-string v1, "Progress UI is locked, seekbar and time won\'t be updated until unlocked"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 444
    goto :goto_0

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->getCurrentPosition()I

    move-result v0

    .line 448
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v1}, Landroid/widget/MediaController$MediaPlayerControl;->getDuration()I

    move-result v1

    .line 450
    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_4

    .line 451
    if-lez v1, :cond_3

    .line 455
    int-to-long v2, v0

    mul-long/2addr v2, v7

    const-wide/16 v4, 0x3e7

    add-long/2addr v2, v4

    long-to-double v2, v2

    int-to-double v4, v1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-long v2, v2

    .line 457
    sub-int v4, v1, v0

    if-ge v4, v6, :cond_2

    move-wide v2, v7

    .line 462
    :cond_2
    iget-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v6}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 463
    iget-object v4, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 467
    :cond_3
    iget-object v2, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v2}, Landroid/widget/MediaController$MediaPlayerControl;->getBufferPercentage()I

    move-result v2

    .line 468
    iget-object v3, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_progress:Landroid/widget/ProgressBar;

    mul-int/lit8 v2, v2, 0xa

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    .line 472
    :cond_4
    invoke-direct {p0, v1, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->setDurationAndPosition(II)V

    .line 474
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->updatePlayIcon()V

    goto :goto_0
.end method

.method private slideIn()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 391
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 392
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v6, 0x3f80

    move v3, v1

    move v4, v2

    move v5, v1

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 401
    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 402
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 404
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 405
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 407
    :cond_0
    return-void
.end method

.method private slideOut()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 413
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 414
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/high16 v8, 0x3f80

    move v3, v1

    move v4, v2

    move v5, v1

    move v6, v2

    move v7, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 422
    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 423
    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 425
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 426
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 428
    :cond_0
    return-void
.end method


# virtual methods
.method public finishedPlaying()V
    .locals 2

    .prologue
    .line 671
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    if-eqz v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/widget/MediaController$MediaPlayerControl;->seekTo(I)V

    .line 675
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->updatePlayIcon()V

    .line 677
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_2

    .line 678
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_canSeek:Z

    if-nez v0, :cond_1

    .line 679
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onRestart()V

    .line 681
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onPause()V

    .line 683
    :cond_2
    return-void
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 522
    const/4 v0, 0x0

    .line 523
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    if-eqz v1, :cond_0

    .line 524
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->getDuration()I

    move-result v0

    .line 527
    :cond_0
    return v0
.end method

.method public getElapsedTime()I
    .locals 2

    .prologue
    .line 536
    const/4 v0, 0x0

    .line 537
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    if-eqz v1, :cond_0

    .line 538
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->getCurrentPosition()I

    move-result v0

    .line 541
    :cond_0
    return v0
.end method

.method public hide()V
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isInitialSeekComplete:Z

    if-nez v0, :cond_1

    .line 277
    :cond_0
    :goto_0
    return-void

    .line 274
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z

    if-nez v0, :cond_0

    .line 275
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->hide(Z)V

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 314
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 568
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->delayHide()V

    .line 570
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    .line 571
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->togglePlay()V

    .line 597
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    if-ne p1, v0, :cond_2

    .line 573
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onReply()V

    goto :goto_0

    .line 576
    :cond_2
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    if-ne p1, v0, :cond_3

    .line 577
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onDeleteMessage()V

    goto :goto_0

    .line 580
    :cond_3
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    .line 581
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onRetake()V

    goto :goto_0

    .line 584
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 586
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 587
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->pause()V

    .line 589
    :cond_5
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onSend()V

    goto :goto_0

    .line 592
    :cond_6
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnCancel:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 593
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 594
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onCancel()V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mediaControllerView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->initControllerView(Landroid/view/View;)V

    .line 162
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 561
    const/16 v0, 0xfa0

    invoke-virtual {p0, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->show(I)V

    .line 562
    const/4 v0, 0x1

    return v0
.end method

.method public setAnchorView(Landroid/view/View;)V
    .locals 0
    .parameter

    .prologue
    .line 236
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    .line 237
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->removeAllViews()V

    .line 238
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->buildAndAttachControllerView()V

    .line 239
    return-void
.end method

.method public setCallback(Lcom/sgiggle/production/widget/VideomailMediaController$Callback;)V
    .locals 0
    .parameter

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    .line 152
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .parameter

    .prologue
    .line 545
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 546
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnReply:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnDelete:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 553
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 554
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 556
    :cond_2
    return-void
.end method

.method public setInitialSeekComplete()V
    .locals 1

    .prologue
    .line 307
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isInitialSeekComplete:Z

    .line 308
    return-void
.end method

.method public setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    .line 229
    return-void
.end method

.method public setProgressUiFrozen(Z)V
    .locals 1
    .parameter

    .prologue
    .line 739
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isProgressUiFrozen:Z

    if-ne v0, p1, :cond_1

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iput-boolean p1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isProgressUiFrozen:Z

    .line 745
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_isProgressUiFrozen:Z

    if-nez v0, :cond_0

    .line 747
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->setProgress()I

    goto :goto_0
.end method

.method public setReviewPlayState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 603
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    sget-object v1, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    if-eq v0, v1, :cond_0

    .line 604
    const-string v0, "Tango.VideomailMediaController"

    const-string v1, "Logic error: mode not supported."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    :goto_0
    return-void

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_seekBarWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 608
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 609
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 610
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 611
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnCancel:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public setReviewSendState()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 618
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_mode:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    sget-object v1, Lcom/sgiggle/production/widget/VideomailMediaController$Mode;->REVIEW_RECORDED_VIDEOMAIL:Lcom/sgiggle/production/widget/VideomailMediaController$Mode;

    if-eq v0, v1, :cond_0

    .line 619
    const-string v0, "Tango.VideomailMediaController"

    const-string v1, "Logic error: mode not supported."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    :goto_0
    return-void

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_seekBarWrapper:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 624
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnSend:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 625
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnRetake:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnCancel:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 322
    const/16 v0, 0xfa0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/widget/VideomailMediaController;->show(IZ)V

    .line 323
    return-void
.end method

.method public show(IZ)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_enabled:Z

    if-nez v0, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    if-nez v0, :cond_2

    .line 355
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->slideIn()V

    .line 359
    :cond_2
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_anchor:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 360
    invoke-direct {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->setProgress()I

    .line 362
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    if-eqz v0, :cond_3

    .line 363
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 366
    :cond_3
    iput-boolean v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showing:Z

    .line 370
    :cond_4
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 375
    iget-boolean v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_showAlways:Z

    if-nez v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 380
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 381
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 382
    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_handler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public showAndStay()V
    .locals 2

    .prologue
    .line 329
    const/16 v0, 0xfa0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/production/widget/VideomailMediaController;->show(IZ)V

    .line 330
    return-void
.end method

.method public showAndStayIfShort()V
    .locals 2

    .prologue
    const/16 v1, 0xfa0

    .line 337
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->getDuration()I

    move-result v0

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 338
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/sgiggle/production/widget/VideomailMediaController;->show(IZ)V

    .line 339
    return-void

    .line 337
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public togglePlay()V
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->pause()V

    .line 649
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 650
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onPause()V

    .line 661
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->updatePlayIcon()V

    .line 662
    return-void

    .line 653
    :cond_1
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v0}, Landroid/widget/MediaController$MediaPlayerControl;->start()V

    .line 654
    invoke-virtual {p0}, Lcom/sgiggle/production/widget/VideomailMediaController;->showAndStayIfShort()V

    .line 656
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_callback:Lcom/sgiggle/production/widget/VideomailMediaController$Callback;

    invoke-interface {v0}, Lcom/sgiggle/production/widget/VideomailMediaController$Callback;->onPlay()V

    goto :goto_0
.end method

.method public updatePlayIcon()V
    .locals 2

    .prologue
    .line 667
    iget-object v0, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_btnPlayPause:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sgiggle/production/widget/VideomailMediaController;->m_player:Landroid/widget/MediaController$MediaPlayerControl;

    invoke-interface {v1}, Landroid/widget/MediaController$MediaPlayerControl;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f02004b

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 668
    return-void

    .line 667
    :cond_0
    const v1, 0x7f02004f

    goto :goto_0
.end method
