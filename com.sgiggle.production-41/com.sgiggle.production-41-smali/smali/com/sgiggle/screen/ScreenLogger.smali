.class public Lcom/sgiggle/screen/ScreenLogger;
.super Ljava/lang/Object;
.source "ScreenLogger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Tango.ScreenLogger"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v0, "Tango.ScreenLogger"

    const-string v1, "ScreenLogger(): ENTER."

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 16
    return-void
.end method


# virtual methods
.method public native getAllParameters()Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
