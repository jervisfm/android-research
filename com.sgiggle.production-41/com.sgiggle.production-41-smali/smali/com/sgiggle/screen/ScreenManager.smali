.class public Lcom/sgiggle/screen/ScreenManager;
.super Ljava/lang/Object;
.source "ScreenManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/screen/ScreenManager$Mode;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ScreenManager"

.field private static context:Landroid/content/Context;

.field private static mode:I

.field private static pm:Landroid/os/PowerManager;

.field private static wl:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    sput v0, Lcom/sgiggle/screen/ScreenManager;->mode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static disableAutoOff()V
    .locals 0

    .prologue
    .line 84
    return-void
.end method

.method public static enableAutoOff()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public static keepOn()V
    .locals 0

    .prologue
    .line 46
    return-void
.end method

.method public static normal()V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method private static release()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method

.method public static updateContext(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 33
    return-void
.end method
