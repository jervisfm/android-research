.class public Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;
.super Ljava/lang/Object;
.source "ServerOwnedConfig.java"


# static fields
.field public static final CRASH_REPORT_FORM_KEY:Ljava/lang/String; = "crashreport.form.key"

.field public static final CRASH_REPORT_FORM_URI:Ljava/lang/String; = "crashreport.form.uri"

.field public static final CRASH_REPORT_LOGCAT_ARGS:Ljava/lang/String; = "crashreport.logcatargs"

.field static final TAG:I = 0x66


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInt32(Ljava/lang/String;I)I
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-static {p0, p1}, Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;->nativeGetInt32(Ljava/lang/String;I)I

    move-result v0

    .line 13
    const/16 v1, 0x66

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server owned config requested "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 14
    return v0
.end method

.method public static getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-static {p0, p1}, Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;->nativeGetString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    const/16 v1, 0x66

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Server owned config requested "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    .line 24
    return-object v0
.end method

.method private static native nativeGetInt32(Ljava/lang/String;I)I
.end method

.method private static native nativeGetString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method
