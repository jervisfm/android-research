.class public Lcom/sgiggle/util/ClientCrashReporter;
.super Ljava/lang/Object;
.source "ClientCrashReporter.java"


# static fields
.field static final TAG:Ljava/lang/String; = "ClientCrashReporter"

.field private static s_me:Lcom/sgiggle/util/ClientCrashReporter;


# instance fields
.field private m_isAcraInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/util/ClientCrashReporter;->s_me:Lcom/sgiggle/util/ClientCrashReporter;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/util/ClientCrashReporter;->m_isAcraInitialized:Z

    .line 16
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/sgiggle/util/ClientCrashReporter;
    .locals 2

    .prologue
    .line 21
    const-class v0, Lcom/sgiggle/util/ClientCrashReporter;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/sgiggle/util/ClientCrashReporter;->s_me:Lcom/sgiggle/util/ClientCrashReporter;

    if-nez v1, :cond_0

    .line 22
    new-instance v1, Lcom/sgiggle/util/ClientCrashReporter;

    invoke-direct {v1}, Lcom/sgiggle/util/ClientCrashReporter;-><init>()V

    sput-object v1, Lcom/sgiggle/util/ClientCrashReporter;->s_me:Lcom/sgiggle/util/ClientCrashReporter;

    .line 24
    :cond_0
    sget-object v1, Lcom/sgiggle/util/ClientCrashReporter;->s_me:Lcom/sgiggle/util/ClientCrashReporter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    .line 21
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public init(Landroid/app/Application;)V
    .locals 6
    .parameter

    .prologue
    .line 31
    const-string v0, "crashreport.form.uri"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 32
    const-string v1, "crashreport.form.key"

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 33
    const-string v2, "crashreport.logcatargs"

    const-string v3, "-t|200|-v|threadtime"

    invoke-static {v2, v3}, Lcom/sgiggle/serverownedconfig/ServerOwnedConfig;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 36
    :cond_0
    const-string v3, "ClientCrashReporter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACRA is enabled formUri="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " formKey="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-static {}, Lorg/acra/ACRA;->getConfig()Lorg/acra/ACRAConfiguration;

    move-result-object v3

    .line 39
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 40
    invoke-virtual {v3, v0}, Lorg/acra/ACRAConfiguration;->setFormUri(Ljava/lang/String;)V

    .line 42
    :cond_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 43
    invoke-virtual {v3, v1}, Lorg/acra/ACRAConfiguration;->setFormKey(Ljava/lang/String;)V

    .line 45
    :cond_2
    const-string v0, "\\|"

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/acra/ACRAConfiguration;->setLogcatArguments([Ljava/lang/String;)V

    .line 47
    invoke-static {p1}, Lorg/acra/ACRA;->init(Landroid/app/Application;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/util/ClientCrashReporter;->m_isAcraInitialized:Z

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_3
    const-string v0, "ClientCrashReporter"

    const-string v1, "ACRA is disabled"

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public reportException(Ljava/lang/Exception;)V
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/sgiggle/util/ClientCrashReporter;->m_isAcraInitialized:Z

    if-eqz v0, :cond_0

    .line 57
    invoke-static {}, Lorg/acra/ACRA;->getErrorReporter()Lorg/acra/ErrorReporter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/acra/ErrorReporter;->handleException(Ljava/lang/Throwable;)V

    .line 59
    :cond_0
    return-void
.end method
