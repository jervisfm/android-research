.class public Lcom/sgiggle/util/Log;
.super Lcom/sgiggle/util/LogModule;
.source "Log.java"


# static fields
.field public static final debug:I = 0x2

.field public static final error:I = 0x10

.field public static final fatal:I = 0x20

.field public static final info:I = 0x4

.field private static m_useTangoLogs:Z = false

.field public static final trace:I = 0x1

.field public static final warn:I = 0x8


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sgiggle/util/Log;->m_useTangoLogs:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sgiggle/util/LogModule;-><init>()V

    return-void
.end method

.method protected static addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    if-eqz p0, :cond_0

    .line 82
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    if-eqz p1, :cond_1

    .line 86
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 87
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 88
    invoke-virtual {p1, v2}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 89
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 90
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    :cond_1
    return-object v0
.end method

.method public static native addWriter(Ljava/lang/String;)Z
.end method

.method public static d(ILjava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 56
    const/4 v0, 0x2

    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static d(ILjava/lang/String;Ljava/lang/Throwable;)I
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x2

    invoke-static {p1, p2}, Lcom/sgiggle/util/Log;->addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 67
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 72
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sgiggle/util/Log;->d(ILjava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method public static e(ILjava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 57
    const/16 v0, 0x10

    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static e(ILjava/lang/String;Ljava/lang/Throwable;)I
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    const/16 v0, 0x10

    invoke-static {p1, p2}, Lcom/sgiggle/util/Log;->addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 68
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sgiggle/util/Log;->e(ILjava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method public static native gzCompressFile(Ljava/lang/String;Ljava/lang/String;I)Z
.end method

.method public static i(ILjava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 58
    const/4 v0, 0x4

    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static i(ILjava/lang/String;Ljava/lang/Throwable;)I
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 63
    const/4 v0, 0x4

    invoke-static {p1, p2}, Lcom/sgiggle/util/Log;->addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 69
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 74
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sgiggle/util/Log;->i(ILjava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method public static native log(IILjava/lang/String;)V
.end method

.method public static log(ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 98
    const/4 v0, 0x4

    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V

    .line 99
    return-void
.end method

.method private static logTangoOrSystem(IILjava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 105
    sget-boolean v0, Lcom/sgiggle/util/Log;->m_useTangoLogs:Z

    if-eqz v0, :cond_0

    .line 106
    invoke-static {p0, p1, p2}, Lcom/sgiggle/util/Log;->log(IILjava/lang/String;)V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-static {p0}, Lcom/sgiggle/util/Log;->severity2androidPriority(I)I

    move-result v0

    const-string v1, "Tango"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "moduleId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sgiggle/util/AndroidUtilLogUnhider;->println(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static native removeWriter(Ljava/lang/String;)Z
.end method

.method public static setUseTangoLogs(Z)V
    .locals 0
    .parameter

    .prologue
    .line 102
    sput-boolean p0, Lcom/sgiggle/util/Log;->m_useTangoLogs:Z

    .line 103
    return-void
.end method

.method private static severity2androidPriority(I)I
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x6

    .line 117
    sparse-switch p0, :sswitch_data_0

    .line 126
    :goto_0
    :sswitch_0
    return v0

    .line 118
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 119
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 120
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 121
    :sswitch_4
    const/4 v0, 0x5

    goto :goto_0

    .line 123
    :sswitch_5
    const/4 v0, 0x7

    goto :goto_0

    .line 117
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0x10 -> :sswitch_0
        0x20 -> :sswitch_5
    .end sparse-switch
.end method

.method public static v(ILjava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 59
    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    return v0
.end method

.method public static v(ILjava/lang/String;Ljava/lang/Throwable;)I
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 64
    invoke-static {p1, p2}, Lcom/sgiggle/util/Log;->addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p0, v0}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    return v1
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 70
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 75
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sgiggle/util/Log;->v(ILjava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method public static w(ILjava/lang/String;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 60
    const/16 v0, 0x8

    invoke-static {v0, p0, p1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static w(ILjava/lang/String;Ljava/lang/Throwable;)I
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    const/16 v0, 0x8

    invoke-static {p1, p2}, Lcom/sgiggle/util/Log;->addExceptionInfo(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/sgiggle/util/Log;->logTangoOrSystem(IILjava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 71
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 76
    const/16 v0, 0x76

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/sgiggle/util/Log;->w(ILjava/lang/String;Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method
