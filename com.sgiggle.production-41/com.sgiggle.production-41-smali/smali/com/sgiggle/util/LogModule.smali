.class public Lcom/sgiggle/util/LogModule;
.super Ljava/lang/Object;
.source "LogModule.java"


# static fields
.field public static final AudioProcessor:I = 0x1

.field public static final AudioUnitWrapper:I = 0x0

.field public static final BufferChain:I = 0x2

.field public static final FrameAllocator:I = 0x3

.field public static final FramePresenter:I = 0x4

.field public static final H264Decoder:I = 0x5

.field public static final H264Encoder:I = 0x6

.field public static final HandleTable:I = 0x7

.field public static final JitterBuffer:I = 0x8

.field public static final LowBandwidthTrigger:I = 0x9

.field public static final PacketDrop:I = 0xa

.field public static final RTPDepacketizer:I = 0xb

.field public static final RTPPacket:I = 0xc

.field public static final RTPPacketizer:I = 0xd

.field public static final RTSPServer:I = 0xe

.field public static final Renderer:I = 0xf

.field public static final ScreenManager:I = 0x10

.field public static final VideoBitRate:I = 0x11

.field public static final VideoCapture:I = 0x12

.field public static final VideoCaptureData:I = 0x13

.field public static final VideoPlayPipeline:I = 0x14

.field public static final VideoRateController:I = 0x15

.field public static final acapture:I = 0x16

.field public static final android:I = 0x17

.field public static final aplay:I = 0x18

.field public static final aroute:I = 0x19

.field public static final assets:I = 0x1a

.field public static final audioAECAlignedInputFarEndDump:I = 0x1c

.field public static final audioAECAlignedInputNearEndDump:I = 0x1d

.field public static final audioAECInputFarEndDump:I = 0x1e

.field public static final audioAECInputNearEndDump:I = 0x1f

.field public static final audioAECOutputDump:I = 0x20

.field public static final audioAGCOutputDump:I = 0x21

.field public static final audioDecoderOutputDump:I = 0x22

.field public static final audioHPFInputDump:I = 0x23

.field public static final audioHPFOutputDump:I = 0x24

.field public static final audioNSOutputDump:I = 0x25

.field public static final audioRcvPipelineOut:I = 0x26

.field public static final audioTxPipelineIn:I = 0x27

.field public static final audio_pipeline:I = 0x2a

.field public static final audiomode:I = 0x2b

.field public static final auth_token:I = 0x96

.field public static final automator:I = 0x2c

.field public static final avatar:I = 0x2d

.field public static final avi:I = 0x2e

.field public static final cafe:I = 0x2f

.field public static final call_state:I = 0x30

.field public static final camera_mgr:I = 0x31

.field public static final circ:I = 0x32

.field public static final connectivity:I = 0x33

.field public static final contacts:I = 0x34

.field public static final cpu_ctrl:I = 0x36

.field public static final d3d:I = 0x38

.field public static final dispatcher_thread:I = 0x97

.field public static final dns_resolver:I = 0x39

.field public static final dynamic_cfg:I = 0x3a

.field public static final engine_capture:I = 0x3b

.field public static final engine_render:I = 0x3c

.field public static final fb:I = 0x3d

.field public static final file_transfer:I = 0x3e

.field public static final force_idr:I = 0x3f

.field public static final game:I = 0x9a

.field public static final global_config:I = 0x40

.field public static final h264InDump:I = 0x41

.field public static final h264OutDump:I = 0x42

.field public static final h264_capture:I = 0x43

.field public static final h264_renderer:I = 0x44

.field public static final http:I = 0x45

.field public static final http_details:I = 0x46

.field public static final init:I = 0x47

.field public static final iphone:I = 0x48

.field public static final jingle:I = 0x49

.field public static final local_storage:I = 0x4a

.field public static final logA:I = 0x4b

.field public static final logB:I = 0x4c

.field public static final logInternal:I = 0x4d

.field public static final lua:I = 0x4e

.field public static final lua_gl:I = 0x4f

.field public static final lua_script:I = 0x9b

.field public static final messaging:I = 0x50

.field public static final nativecalllog:I = 0x35

.field public static final network:I = 0x51

.field public static final p2p:I = 0x52

.field public static final phone_formatter:I = 0x53

.field public static final pipeline:I = 0x54

.field public static final pj:I = 0x55

.field public static final postcall:I = 0x56

.field public static final pr:I = 0x57

.field public static final product:I = 0x58

.field public static final profile_encoder:I = 0x59

.field public static final profile_packetizer:I = 0x5a

.field public static final profile_rotation:I = 0x5b

.field public static final property_tree:I = 0x5c

.field public static final protobuf:I = 0x5d

.field public static final python_bindings:I = 0x5e

.field public static final python_client:I = 0x5f

.field public static final python_system:I = 0x60

.field public static final qosctrl:I = 0x61

.field public static final recommend:I = 0x9c

.field public static final rtp2h264:I = 0x62

.field public static final rtp2tiff:I = 0x63

.field public static final rtpplay:I = 0x64

.field public static final s1:I = 0x65

.field public static final server_owned_config:I = 0x66

.field public static final server_owned_impl_selector:I = 0x67

.field public static final sns:I = 0x68

.field public static final soundeff:I = 0x69

.field public static final state_machine:I = 0x6a

.field public static final stats:I = 0x6b

.field public static final store:I = 0x98

.field public static final stress_test:I = 0x6c

.field public static final swift_call_state:I = 0x6d

.field public static final swift_client:I = 0x6e

.field public static final swift_common:I = 0x6f

.field public static final swift_ctrl_packet:I = 0x70

.field public static final swift_data_packet:I = 0x71

.field public static final swift_server:I = 0x72

.field public static final swift_server_list_mgr:I = 0x73

.field public static final swift_server_routing_table:I = 0x74

.field public static final swift_state_machine:I = 0x75

.field public static final tango_client:I = 0x76

.field public static final tango_push:I = 0x77

.field public static final tc:I = 0x99

.field public static final telephony:I = 0x78

.field public static final test:I = 0x79

.field public static final testing:I = 0x7a

.field public static final testing_client:I = 0x7b

.field public static final testing_notice:I = 0x7c

.field public static final testing_server:I = 0x7d

.field public static final thread_pool:I = 0x7e

.field public static final thread_post:I = 0x7f

.field public static final thread_prio:I = 0x80

.field public static final tiff:I = 0x81

.field public static final trigger_crash:I = 0x37

.field public static final two_way_view:I = 0x82

.field public static final udp_sender:I = 0x83

.field public static final ui_state:I = 0x85

.field public static final util:I = 0x84

.field public static final vcapture:I = 0x86

.field public static final vgood:I = 0x87

.field public static final vgreeting:I = 0x1b

.field public static final video_pipeline:I = 0x88

.field public static final video_pipeline_capture:I = 0x89

.field public static final video_pipeline_render:I = 0x8a

.field public static final video_ringback:I = 0x8b

.field public static final videoprep:I = 0x8c

.field public static final vmail:I = 0x8d

.field public static final voip_background:I = 0x9e

.field public static final voip_socket:I = 0x9d

.field public static final vplay:I = 0x8e

.field public static final webRTC:I = 0x8f

.field public static final webrtcApmDebugRecording:I = 0x29

.field public static final webrtcRtpDump:I = 0x28

.field public static final welcome:I = 0x90

.field public static final win_engine:I = 0x91

.field public static final win_msg:I = 0x92

.field public static final win_phone:I = 0x93

.field public static final win_ui:I = 0x94

.field public static final xmitter:I = 0x95


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
