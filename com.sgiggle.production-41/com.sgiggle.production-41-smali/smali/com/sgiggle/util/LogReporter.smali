.class public Lcom/sgiggle/util/LogReporter;
.super Ljava/lang/Object;
.source "LogReporter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/util/LogReporter$AppData;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field private static s_provider:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/sgiggle/util/LogReporter$AppData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "LogReporter"

    sput-object v0, Lcom/sgiggle/util/LogReporter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static native enableUri(Ljava/lang/String;)Z
.end method

.method private static getAppData()Lcom/sgiggle/util/LogReporter$AppData;
    .locals 5

    .prologue
    .line 76
    const/4 v1, 0x0

    .line 79
    :try_start_0
    sget-object v0, Lcom/sgiggle/util/LogReporter;->s_provider:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/util/LogReporter$AppData;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 83
    sget-object v2, Lcom/sgiggle/util/LogReporter;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to get an instance of AppData: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/sgiggle/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public static native getBinLogFilePath()Ljava/lang/String;
.end method

.method public static native getLogEmail()Ljava/lang/String;
.end method

.method public static localStoragePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Lcom/sgiggle/util/LogReporter;->getAppData()Lcom/sgiggle/util/LogReporter$AppData;

    move-result-object v0

    .line 41
    if-nez v0, :cond_0

    .line 42
    const/4 v0, 0x0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/sgiggle/util/LogReporter$AppData;->localStoragePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static native onEmailSent()V
.end method

.method public static native outFileName()Ljava/lang/String;
.end method

.method public static releaseInstance()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sgiggle/util/LogReporter;->s_provider:Ljava/lang/Class;

    .line 35
    invoke-static {}, Lcom/sgiggle/util/LogReporter;->releaseNativeLogReporter()V

    .line 36
    return-void
.end method

.method private static native releaseNativeLogReporter()V
.end method

.method public static native restore()Z
.end method

.method public static scheduleEmail()Z
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/sgiggle/util/LogReporter;->getAppData()Lcom/sgiggle/util/LogReporter$AppData;

    move-result-object v0

    .line 50
    if-nez v0, :cond_0

    .line 51
    const/4 v0, 0x0

    .line 53
    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lcom/sgiggle/util/LogReporter$AppData;->scheduleEmail()Z

    move-result v0

    goto :goto_0
.end method

.method public static setAppDataProvider(Ljava/lang/Class;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sgiggle/util/LogReporter$AppData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    sput-object p0, Lcom/sgiggle/util/LogReporter;->s_provider:Ljava/lang/Class;

    .line 29
    return-void
.end method
