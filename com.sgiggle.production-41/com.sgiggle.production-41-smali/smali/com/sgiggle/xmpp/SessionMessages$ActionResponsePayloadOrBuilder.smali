.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ActionResponsePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ActionResponsePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getActionname()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getIsacceptrequest()Z
.end method

.method public abstract getNotificationid()Ljava/lang/String;
.end method

.method public abstract hasActionname()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasIsacceptrequest()Z
.end method

.method public abstract hasNotificationid()Z
.end method
