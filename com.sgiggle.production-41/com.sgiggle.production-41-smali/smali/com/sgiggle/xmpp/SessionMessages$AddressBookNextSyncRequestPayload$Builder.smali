.class public final Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private nextOffset_:I

.field private requestId_:I

.field private version_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 44689
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 44851
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44690
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->maybeForceBuilderInitialization()V

    .line 44691
    return-void
.end method

.method static synthetic access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44684
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$57100()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44730
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    .line 44731
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 44732
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 44735
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44696
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 44694
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 2

    .prologue
    .line 44721
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    .line 44722
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 44723
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 44725
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 5

    .prologue
    .line 44739
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 44740
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44741
    const/4 v2, 0x0

    .line 44742
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 44743
    or-int/lit8 v2, v2, 0x1

    .line 44745
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->access$57302(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44746
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 44747
    or-int/lit8 v2, v2, 0x2

    .line 44749
    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->access$57402(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I

    .line 44750
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 44751
    or-int/lit8 v2, v2, 0x4

    .line 44753
    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->access$57502(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I

    .line 44754
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 44755
    or-int/lit8 v1, v2, 0x8

    .line 44757
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->access$57602(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I

    .line 44758
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->access$57702(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I

    .line 44759
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44700
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 44701
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44702
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44703
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    .line 44704
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44705
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    .line 44706
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44707
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    .line 44708
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44709
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44889
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44890
    return-object p0
.end method

.method public clearNextOffset()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44929
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44930
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    .line 44932
    return-object p0
.end method

.method public clearRequestId()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44950
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44951
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    .line 44953
    return-object p0
.end method

.method public clearVersion()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44908
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44909
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    .line 44911
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 2

    .prologue
    .line 44713
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 44856
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 44684
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1

    .prologue
    .line 44717
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getNextOffset()I
    .locals 1

    .prologue
    .line 44920
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    return v0
.end method

.method public getRequestId()I
    .locals 1

    .prologue
    .line 44941
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 44899
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44853
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNextOffset()Z
    .locals 2

    .prologue
    .line 44917
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRequestId()Z
    .locals 2

    .prologue
    .line 44938
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVersion()Z
    .locals 2

    .prologue
    .line 44896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44780
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 44800
    :goto_0
    return v0

    .line 44784
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->hasVersion()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 44786
    goto :goto_0

    .line 44788
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->hasNextOffset()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 44790
    goto :goto_0

    .line 44792
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->hasRequestId()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 44794
    goto :goto_0

    .line 44796
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 44798
    goto :goto_0

    .line 44800
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 44875
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 44877
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44883
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44884
    return-object p0

    .line 44880
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44684
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44684
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44684
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44808
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 44809
    sparse-switch v0, :sswitch_data_0

    .line 44814
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 44816
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 44812
    goto :goto_1

    .line 44821
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 44822
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 44823
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 44825
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 44826
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    goto :goto_0

    .line 44830
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44831
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    goto :goto_0

    .line 44835
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44836
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    goto :goto_0

    .line 44840
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44841
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    goto :goto_0

    .line 44809
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44763
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 44776
    :goto_0
    return-object v0

    .line 44764
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44765
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    .line 44767
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasVersion()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 44768
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getVersion()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->setVersion(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    .line 44770
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasNextOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 44771
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getNextOffset()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->setNextOffset(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    .line 44773
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasRequestId()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44774
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getRequestId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->setRequestId(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    :cond_4
    move-object v0, p0

    .line 44776
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44869
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44871
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44872
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44859
    if-nez p1, :cond_0

    .line 44860
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 44862
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44865
    return-object p0
.end method

.method public setNextOffset(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44923
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44924
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->nextOffset_:I

    .line 44926
    return-object p0
.end method

.method public setRequestId(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44944
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44945
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->requestId_:I

    .line 44947
    return-object p0
.end method

.method public setVersion(I)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44902
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->bitField0_:I

    .line 44903
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->version_:I

    .line 44905
    return-object p0
.end method
