.class public final Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressBookNextSyncRequestPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final NEXT_OFFSET_FIELD_NUMBER:I = 0x3

.field public static final REQUEST_ID_FIELD_NUMBER:I = 0x4

.field public static final VERSION_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private nextOffset_:I

.field private requestId_:I

.field private version_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44960
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    .line 44961
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->initFields()V

    .line 44962
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 44473
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 44533
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    .line 44579
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedSerializedSize:I

    .line 44474
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 44475
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 44533
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    .line 44579
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedSerializedSize:I

    .line 44475
    return-void
.end method

.method static synthetic access$57302(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$57402(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I

    return p1
.end method

.method static synthetic access$57502(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I

    return p1
.end method

.method static synthetic access$57602(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I

    return p1
.end method

.method static synthetic access$57702(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44468
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1

    .prologue
    .line 44479
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 44528
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 44529
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I

    .line 44530
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I

    .line 44531
    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I

    .line 44532
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44677
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57100()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 44680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44646
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    .line 44647
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44648
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    .line 44650
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44657
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    .line 44658
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44659
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    .line 44661
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44613
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44619
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44667
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44673
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44635
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44641
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44624
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 44630
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;->access$57000(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 44494
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 44468
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;
    .locals 1

    .prologue
    .line 44483
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;

    return-object v0
.end method

.method public getNextOffset()I
    .locals 1

    .prologue
    .line 44514
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I

    return v0
.end method

.method public getRequestId()I
    .locals 1

    .prologue
    .line 44524
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 44581
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedSerializedSize:I

    .line 44582
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 44602
    :goto_0
    return v0

    .line 44584
    :cond_0
    const/4 v0, 0x0

    .line 44585
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 44586
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 44589
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 44590
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 44593
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 44594
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 44597
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 44598
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 44601
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 44504
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 44491
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNextOffset()Z
    .locals 2

    .prologue
    .line 44511
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRequestId()Z
    .locals 2

    .prologue
    .line 44521
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVersion()Z
    .locals 2

    .prologue
    .line 44501
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44535
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    .line 44536
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 44559
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 44536
    goto :goto_0

    .line 44538
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 44539
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 44540
    goto :goto_0

    .line 44542
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasVersion()Z

    move-result v0

    if-nez v0, :cond_3

    .line 44543
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 44544
    goto :goto_0

    .line 44546
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasNextOffset()Z

    move-result v0

    if-nez v0, :cond_4

    .line 44547
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 44548
    goto :goto_0

    .line 44550
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->hasRequestId()Z

    move-result v0

    if-nez v0, :cond_5

    .line 44551
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 44552
    goto :goto_0

    .line 44554
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 44555
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 44556
    goto :goto_0

    .line 44558
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 44559
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44468
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44678
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44468
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;
    .locals 1

    .prologue
    .line 44682
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 44607
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 44564
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->getSerializedSize()I

    .line 44565
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 44566
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 44568
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 44569
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->version_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 44571
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 44572
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->nextOffset_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 44574
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 44575
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayload;->requestId_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 44577
    :cond_3
    return-void
.end method
