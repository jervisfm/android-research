.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AddressBookNextSyncRequestPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AddressBookNextSyncRequestPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getNextOffset()I
.end method

.method public abstract getRequestId()I
.end method

.method public abstract getVersion()I
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasNextOffset()Z
.end method

.method public abstract hasRequestId()Z
.end method

.method public abstract hasVersion()Z
.end method
