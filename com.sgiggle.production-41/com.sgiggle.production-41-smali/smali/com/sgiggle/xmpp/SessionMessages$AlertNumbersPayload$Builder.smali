.class public final Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private appNumber_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private timestamp_:J

.field private unreadMissedCallNumber_:I

.field private unreadVideoMailNumber_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$90300()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$90502(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$90602(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$90702(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$90802(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    or-int/lit8 v1, v2, 0x10

    :goto_0
    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J
    invoke-static {v0, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$90902(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;J)J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->access$91002(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAppNumber()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearTimestamp()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    return-object p0
.end method

.method public clearUnreadMissedCallNumber()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    return-object p0
.end method

.method public clearUnreadVideoMailNumber()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAppNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    return-wide v0
.end method

.method public getUnreadMissedCallNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    return v0
.end method

.method public getUnreadVideoMailNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    return v0
.end method

.method public hasAppNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMissedCallNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadVideoMailNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasAppNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getAppNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->setAppNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasUnreadMissedCallNumber()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getUnreadMissedCallNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->setUnreadMissedCallNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasUnreadVideoMailNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getUnreadVideoMailNumber()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->setUnreadVideoMailNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasTimestamp()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->setTimestamp(J)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    :cond_5
    move-object v0, p0

    goto :goto_0
.end method

.method public setAppNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->appNumber_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setTimestamp(J)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->timestamp_:J

    return-object p0
.end method

.method public setUnreadMissedCallNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadMissedCallNumber_:I

    return-object p0
.end method

.method public setUnreadVideoMailNumber(I)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->unreadVideoMailNumber_:I

    return-object p0
.end method
