.class public final Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlertNumbersPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    }
.end annotation


# static fields
.field public static final APP_NUMBER_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TIMESTAMP_FIELD_NUMBER:I = 0x5

.field public static final UNREAD_MISSED_CALL_NUMBER_FIELD_NUMBER:I = 0x3

.field public static final UNREAD_VIDEO_MAIL_NUMBER_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;


# instance fields
.field private appNumber_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private timestamp_:J

.field private unreadMissedCallNumber_:I

.field private unreadVideoMailNumber_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$90502(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$90602(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I

    return p1
.end method

.method static synthetic access$90702(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I

    return p1
.end method

.method static synthetic access$90802(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I

    return p1
.end method

.method static synthetic access$90902(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J

    return-wide p1
.end method

.method static synthetic access$91002(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90300()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;->access$90200(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAppNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getTimestamp()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J

    return-wide v0
.end method

.method public getUnreadMissedCallNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I

    return v0
.end method

.method public getUnreadVideoMailNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I

    return v0
.end method

.method public hasAppNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimestamp()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMissedCallNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadVideoMailNumber()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;)Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->appNumber_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadMissedCallNumber_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->unreadVideoMailNumber_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayload;->timestamp_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_4
    return-void
.end method
