.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AlertNumbersPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlertNumbersPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAppNumber()I
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getTimestamp()J
.end method

.method public abstract getUnreadMissedCallNumber()I
.end method

.method public abstract getUnreadVideoMailNumber()I
.end method

.method public abstract hasAppNumber()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasTimestamp()Z
.end method

.method public abstract hasUnreadMissedCallNumber()Z
.end method

.method public abstract hasUnreadVideoMailNumber()Z
.end method
