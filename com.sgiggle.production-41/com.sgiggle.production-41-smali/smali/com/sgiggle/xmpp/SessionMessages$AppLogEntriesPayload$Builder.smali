.class public final Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 54062
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 54199
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54242
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54063
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 54064
    return-void
.end method

.method static synthetic access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54057
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$69700()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54099
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    .line 54100
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54101
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 54104
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54069
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntriesIsMutable()V
    .locals 2

    .prologue
    .line 54245
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 54246
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54247
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54249
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 54067
    return-void
.end method


# virtual methods
.method public addAllEntries(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 54312
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54313
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 54315
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 54305
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54306
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 54308
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 54288
    if-nez p2, :cond_0

    .line 54289
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54291
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54292
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 54294
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 54298
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54299
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54301
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54278
    if-nez p1, :cond_0

    .line 54279
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54281
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54282
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54284
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 2

    .prologue
    .line 54090
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    .line 54091
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 54092
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 54094
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 4

    .prologue
    .line 54108
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 54109
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54110
    const/4 v2, 0x0

    .line 54111
    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 54112
    or-int/lit8 v1, v2, 0x1

    .line 54114
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$69902(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54115
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 54116
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54117
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54119
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$70002(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;Ljava/util/List;)Ljava/util/List;

    .line 54120
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$70102(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;I)I

    .line 54121
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54073
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 54074
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54075
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54076
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54077
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54078
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54235
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54237
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54238
    return-object p0
.end method

.method public clearEntries()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54318
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54321
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2

    .prologue
    .line 54082
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 54204
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 54057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1

    .prologue
    .line 54086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter

    .prologue
    .line 54258
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 54255
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54252
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 54143
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 54157
    :goto_0
    return v0

    .line 54147
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 54149
    goto :goto_0

    :cond_1
    move v0, v2

    .line 54151
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getEntriesCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 54152
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 54154
    goto :goto_0

    .line 54151
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 54157
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 54223
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 54225
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54231
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54232
    return-object p0

    .line 54228
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54057
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54057
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54057
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54165
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 54166
    sparse-switch v0, :sswitch_data_0

    .line 54171
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 54173
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 54169
    goto :goto_1

    .line 54178
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 54179
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 54180
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 54182
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 54183
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    goto :goto_0

    .line 54187
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    .line 54188
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 54189
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->addEntries(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    goto :goto_0

    .line 54166
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 54125
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 54139
    :goto_0
    return-object v0

    .line 54126
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54127
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    .line 54129
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$70000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 54130
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 54131
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$70000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 54132
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    move-object v0, p0

    .line 54139
    goto :goto_0

    .line 54134
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54135
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->access$70000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntries(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54324
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54325
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 54327
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54217
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54219
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54220
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54207
    if-nez p1, :cond_0

    .line 54208
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54210
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 54212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->bitField0_:I

    .line 54213
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 54272
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54273
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 54275
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 54262
    if-nez p2, :cond_0

    .line 54263
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54265
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 54266
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 54268
    return-object p0
.end method
