.class public final Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppLogEntriesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ENTRIES_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 54334
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    .line 54335
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->initFields()V

    .line 54336
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53877
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 53926
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    .line 53960
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedSerializedSize:I

    .line 53878
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53872
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53879
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 53926
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    .line 53960
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedSerializedSize:I

    .line 53879
    return-void
.end method

.method static synthetic access$69902(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53872
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$70000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 53872
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$70002(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53872
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$70102(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53872
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1

    .prologue
    .line 53883
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 53923
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 53924
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    .line 53925
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54050
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69700()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 54053
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    .line 54020
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54021
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    .line 54023
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54030
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    .line 54031
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54032
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    .line 54034
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53986
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53992
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54040
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54046
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54008
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54014
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53997
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 54003
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;->access$69600(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 53898
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53872
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;
    .locals 1

    .prologue
    .line 53887
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter

    .prologue
    .line 53915
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 53912
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53905
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 53919
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;

    return-object v0
.end method

.method public getEntriesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53909
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53962
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedSerializedSize:I

    .line 53963
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 53975
    :goto_0
    return v0

    .line 53966
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 53967
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 53970
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 53971
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 53970
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 53974
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 53975
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53895
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53928
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    .line 53929
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 53946
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 53929
    goto :goto_0

    .line 53931
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 53932
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 53933
    goto :goto_0

    .line 53935
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 53936
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 53937
    goto :goto_0

    :cond_3
    move v0, v2

    .line 53939
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getEntriesCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 53940
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 53941
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 53942
    goto :goto_0

    .line 53939
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 53945
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 53946
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53872
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54051
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53872
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 54055
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 53980
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 53951
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->getSerializedSize()I

    .line 53952
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 53953
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 53955
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 53956
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 53955
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 53958
    :cond_1
    return-void
.end method
