.class public final Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private severity_:I

.field private text_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53674
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 53813
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53675
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->maybeForceBuilderInitialization()V

    .line 53676
    return-void
.end method

.method static synthetic access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53669
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$69100()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53711
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    .line 53712
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 53713
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 53716
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53681
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 53679
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 2

    .prologue
    .line 53702
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    .line 53703
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 53704
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 53706
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 5

    .prologue
    .line 53720
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 53721
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53722
    const/4 v2, 0x0

    .line 53723
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 53724
    or-int/lit8 v2, v2, 0x1

    .line 53726
    :cond_0
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->access$69302(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;I)I

    .line 53727
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 53728
    or-int/lit8 v1, v2, 0x2

    .line 53730
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->access$69402(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53731
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->access$69502(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;I)I

    .line 53732
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53685
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 53686
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    .line 53687
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53688
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53689
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53690
    return-object p0
.end method

.method public clearSeverity()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53807
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    .line 53809
    return-object p0
.end method

.method public clearText()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53837
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53838
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53840
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 2

    .prologue
    .line 53694
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1

    .prologue
    .line 53698
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getSeverity()I
    .locals 1

    .prologue
    .line 53797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53819
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 53820
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 53821
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53824
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasSeverity()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53794
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 53815
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53747
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->hasSeverity()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 53755
    :goto_0
    return v0

    .line 53751
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->hasText()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 53753
    goto :goto_0

    .line 53755
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53669
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53669
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53669
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 53764
    sparse-switch v0, :sswitch_data_0

    .line 53769
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 53771
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 53767
    goto :goto_1

    .line 53776
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53777
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    goto :goto_0

    .line 53781
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53782
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    .line 53764
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53736
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 53743
    :goto_0
    return-object v0

    .line 53737
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->hasSeverity()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53738
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getSeverity()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->setSeverity(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    .line 53740
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->hasText()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 53741
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    :cond_2
    move-object v0, p0

    .line 53743
    goto :goto_0
.end method

.method public setSeverity(I)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53800
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53801
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->severity_:I

    .line 53803
    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53828
    if-nez p1, :cond_0

    .line 53829
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53831
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53832
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53834
    return-object p0
.end method

.method setText(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 53843
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->bitField0_:I

    .line 53844
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->text_:Ljava/lang/Object;

    .line 53846
    return-void
.end method
