.class public final Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppLogEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    }
.end annotation


# static fields
.field public static final SEVERITY_FIELD_NUMBER:I = 0x1

.field public static final TEXT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private severity_:I

.field private text_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53852
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    .line 53853
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->initFields()V

    .line 53854
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53484
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 53544
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    .line 53572
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedSerializedSize:I

    .line 53485
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53479
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 53486
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 53544
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    .line 53572
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedSerializedSize:I

    .line 53486
    return-void
.end method

.method static synthetic access$69302(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53479
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I

    return p1
.end method

.method static synthetic access$69402(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53479
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$69502(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 53479
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1

    .prologue
    .line 53490
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    return-object v0
.end method

.method private getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 53529
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    .line 53530
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 53531
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 53533
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    .line 53536
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 53541
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I

    .line 53542
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    .line 53543
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53662
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69100()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 53665
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53631
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    .line 53632
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53633
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    .line 53635
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53642
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    .line 53643
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 53644
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    .line 53646
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53598
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53604
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53652
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53658
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53620
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53626
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53609
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 53615
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;->access$69000(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 53479
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;
    .locals 1

    .prologue
    .line 53494
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 53574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedSerializedSize:I

    .line 53575
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 53587
    :goto_0
    return v0

    .line 53577
    :cond_0
    const/4 v0, 0x0

    .line 53578
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 53579
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 53582
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 53583
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 53586
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSeverity()I
    .locals 1

    .prologue
    .line 53505
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 53515
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    .line 53516
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 53517
    check-cast v0, Ljava/lang/String;

    .line 53525
    :goto_0
    return-object v0

    .line 53519
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 53521
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 53522
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53523
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->text_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 53525
    goto :goto_0
.end method

.method public hasSeverity()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 53502
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 53512
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53546
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    .line 53547
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 53558
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 53547
    goto :goto_0

    .line 53549
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->hasSeverity()Z

    move-result v0

    if-nez v0, :cond_2

    .line 53550
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 53551
    goto :goto_0

    .line 53553
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->hasText()Z

    move-result v0

    if-nez v0, :cond_3

    .line 53554
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 53555
    goto :goto_0

    .line 53557
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->memoizedIsInitialized:B

    move v0, v3

    .line 53558
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53479
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53663
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 53479
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;
    .locals 1

    .prologue
    .line 53667
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;)Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 53592
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 53563
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getSerializedSize()I

    .line 53564
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 53565
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->severity_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 53567
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 53568
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AppLogEntry;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 53570
    :cond_1
    return-void
.end method
