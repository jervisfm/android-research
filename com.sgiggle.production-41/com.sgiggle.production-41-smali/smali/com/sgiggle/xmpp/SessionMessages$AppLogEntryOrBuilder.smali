.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AppLogEntryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AppLogEntryOrBuilder"
.end annotation


# virtual methods
.method public abstract getSeverity()I
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract hasSeverity()Z
.end method

.method public abstract hasText()Z
.end method
