.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AssetResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AssetResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAssetid()J
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getSuccess()Z
.end method

.method public abstract hasAssetid()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasSuccess()Z
.end method
