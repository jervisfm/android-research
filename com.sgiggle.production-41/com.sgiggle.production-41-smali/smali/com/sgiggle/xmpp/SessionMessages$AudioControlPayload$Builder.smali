.class public final Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private mute_:Z

.field private speakeron_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 13557
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 13693
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13558
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->maybeForceBuilderInitialization()V

    .line 13559
    return-void
.end method

.method static synthetic access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13552
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$16900()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13596
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    .line 13597
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13598
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 13601
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13564
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 13562
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 2

    .prologue
    .line 13587
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    .line 13588
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 13589
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 13591
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 5

    .prologue
    .line 13605
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 13606
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13607
    const/4 v2, 0x0

    .line 13608
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 13609
    or-int/lit8 v2, v2, 0x1

    .line 13611
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->access$17102(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13612
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 13613
    or-int/lit8 v2, v2, 0x2

    .line 13615
    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->access$17202(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Z)Z

    .line 13616
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 13617
    or-int/lit8 v1, v2, 0x4

    .line 13619
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->access$17302(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Z)Z

    .line 13620
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->access$17402(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;I)I

    .line 13621
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13568
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 13569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13570
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13571
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    .line 13572
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13573
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    .line 13574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13575
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13729
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13731
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13732
    return-object p0
.end method

.method public clearMute()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13771
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13772
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    .line 13774
    return-object p0
.end method

.method public clearSpeakeron()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13750
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13751
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    .line 13753
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 2

    .prologue
    .line 13579
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 13698
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13552
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1

    .prologue
    .line 13583
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMute()Z
    .locals 1

    .prologue
    .line 13762
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    return v0
.end method

.method public getSpeakeron()Z
    .locals 1

    .prologue
    .line 13741
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13695
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMute()Z
    .locals 2

    .prologue
    .line 13759
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakeron()Z
    .locals 2

    .prologue
    .line 13738
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13639
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 13647
    :goto_0
    return v0

    .line 13643
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 13645
    goto :goto_0

    .line 13647
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 13717
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 13719
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13725
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13726
    return-object p0

    .line 13722
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13552
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13552
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13552
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13655
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 13656
    sparse-switch v0, :sswitch_data_0

    .line 13661
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 13663
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 13659
    goto :goto_1

    .line 13668
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 13669
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13670
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 13672
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 13673
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    goto :goto_0

    .line 13677
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13678
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    goto :goto_0

    .line 13682
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13683
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    goto :goto_0

    .line 13656
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13625
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 13635
    :goto_0
    return-object v0

    .line 13626
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13627
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    .line 13629
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->hasSpeakeron()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 13630
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getSpeakeron()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    .line 13632
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->hasMute()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 13633
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getMute()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->setMute(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 13635
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13711
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13713
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13714
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13701
    if-nez p1, :cond_0

    .line 13702
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 13704
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13706
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13707
    return-object p0
.end method

.method public setMute(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13765
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13766
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mute_:Z

    .line 13768
    return-object p0
.end method

.method public setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13744
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->bitField0_:I

    .line 13745
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->speakeron_:Z

    .line 13747
    return-object p0
.end method
