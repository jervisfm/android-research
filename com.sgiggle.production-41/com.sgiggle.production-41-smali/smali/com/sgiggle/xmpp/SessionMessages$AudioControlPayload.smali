.class public final Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioControlPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final MUTE_FIELD_NUMBER:I = 0x3

.field public static final SPEAKERON_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private mute_:Z

.field private speakeron_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13781
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    .line 13782
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->initFields()V

    .line 13783
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 13371
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 13420
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    .line 13451
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedSerializedSize:I

    .line 13372
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13366
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 13373
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 13420
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    .line 13451
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedSerializedSize:I

    .line 13373
    return-void
.end method

.method static synthetic access$17102(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13366
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$17202(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13366
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z

    return p1
.end method

.method static synthetic access$17302(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13366
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z

    return p1
.end method

.method static synthetic access$17402(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13366
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1

    .prologue
    .line 13377
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13416
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13417
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z

    .line 13418
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z

    .line 13419
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13545
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16900()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 13548
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13514
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    .line 13515
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13516
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    .line 13518
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13525
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    .line 13526
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13527
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    .line 13529
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13481
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13487
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13535
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13541
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13503
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13509
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13492
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13498
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;->access$16800(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 13392
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13366
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;
    .locals 1

    .prologue
    .line 13381
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;

    return-object v0
.end method

.method public getMute()Z
    .locals 1

    .prologue
    .line 13412
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 13453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedSerializedSize:I

    .line 13454
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13470
    :goto_0
    return v0

    .line 13456
    :cond_0
    const/4 v0, 0x0

    .line 13457
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 13458
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13461
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 13462
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13465
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 13466
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13469
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSpeakeron()Z
    .locals 1

    .prologue
    .line 13402
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13389
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMute()Z
    .locals 2

    .prologue
    .line 13409
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakeron()Z
    .locals 2

    .prologue
    .line 13399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13422
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    .line 13423
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 13434
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 13423
    goto :goto_0

    .line 13425
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 13426
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 13427
    goto :goto_0

    .line 13429
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 13430
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 13431
    goto :goto_0

    .line 13433
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 13434
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13366
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13546
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13366
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;
    .locals 1

    .prologue
    .line 13550
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 13475
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 13439
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->getSerializedSize()I

    .line 13440
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 13443
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 13444
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->speakeron_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13446
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 13447
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayload;->mute_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13449
    :cond_2
    return-void
.end method
