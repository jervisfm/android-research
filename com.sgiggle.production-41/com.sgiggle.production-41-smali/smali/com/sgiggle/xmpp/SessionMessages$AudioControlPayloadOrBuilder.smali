.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AudioControlPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioControlPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getMute()Z
.end method

.method public abstract getSpeakeron()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMute()Z
.end method

.method public abstract hasSpeakeron()Z
.end method
