.class public final Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AudioModePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AudioModePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private devchange_:Z

.field private muted_:Z

.field private speakeron_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 14020
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 14174
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14021
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->maybeForceBuilderInitialization()V

    .line 14022
    return-void
.end method

.method static synthetic access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14015
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$17600()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 14061
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    .line 14062
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14063
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 14066
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14027
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 14025
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 2

    .prologue
    .line 14052
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    .line 14053
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 14054
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 14056
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 5

    .prologue
    .line 14070
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 14071
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14072
    const/4 v2, 0x0

    .line 14073
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 14074
    or-int/lit8 v2, v2, 0x1

    .line 14076
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->access$17802(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14077
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 14078
    or-int/lit8 v2, v2, 0x2

    .line 14080
    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->access$17902(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z

    .line 14081
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 14082
    or-int/lit8 v2, v2, 0x4

    .line 14084
    :cond_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->access$18002(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z

    .line 14085
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 14086
    or-int/lit8 v1, v2, 0x8

    .line 14088
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->access$18102(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z

    .line 14089
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->access$18202(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;I)I

    .line 14090
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14031
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 14032
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14033
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14034
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    .line 14035
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14036
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    .line 14037
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14038
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    .line 14039
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14040
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14210
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14213
    return-object p0
.end method

.method public clearDevchange()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14273
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14274
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    .line 14276
    return-object p0
.end method

.method public clearMuted()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14252
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    .line 14255
    return-object p0
.end method

.method public clearSpeakeron()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14231
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    .line 14234
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 2

    .prologue
    .line 14044
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 14179
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 14015
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1

    .prologue
    .line 14048
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDevchange()Z
    .locals 1

    .prologue
    .line 14264
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    return v0
.end method

.method public getMuted()Z
    .locals 1

    .prologue
    .line 14243
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    return v0
.end method

.method public getSpeakeron()Z
    .locals 1

    .prologue
    .line 14222
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 14176
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevchange()Z
    .locals 2

    .prologue
    .line 14261
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMuted()Z
    .locals 2

    .prologue
    .line 14240
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakeron()Z
    .locals 2

    .prologue
    .line 14219
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 14111
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 14123
    :goto_0
    return v0

    .line 14115
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->hasDevchange()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 14117
    goto :goto_0

    .line 14119
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 14121
    goto :goto_0

    .line 14123
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 14198
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 14200
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14206
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14207
    return-object p0

    .line 14203
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14015
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14015
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14015
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14131
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 14132
    sparse-switch v0, :sswitch_data_0

    .line 14137
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 14139
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 14135
    goto :goto_1

    .line 14144
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 14145
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14146
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 14148
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 14149
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    goto :goto_0

    .line 14153
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14154
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    goto :goto_0

    .line 14158
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14159
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    goto :goto_0

    .line 14163
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14164
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    goto :goto_0

    .line 14132
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14094
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 14107
    :goto_0
    return-object v0

    .line 14095
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 14096
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    .line 14098
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasSpeakeron()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 14099
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getSpeakeron()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    .line 14101
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasMuted()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 14102
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getMuted()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->setMuted(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    .line 14104
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasDevchange()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 14105
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getDevchange()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->setDevchange(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    :cond_4
    move-object v0, p0

    .line 14107
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14192
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14194
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14195
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14182
    if-nez p1, :cond_0

    .line 14183
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 14185
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 14187
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14188
    return-object p0
.end method

.method public setDevchange(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14267
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14268
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->devchange_:Z

    .line 14270
    return-object p0
.end method

.method public setMuted(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14246
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14247
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->muted_:Z

    .line 14249
    return-object p0
.end method

.method public setSpeakeron(Z)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14225
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->bitField0_:I

    .line 14226
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->speakeron_:Z

    .line 14228
    return-object p0
.end method
