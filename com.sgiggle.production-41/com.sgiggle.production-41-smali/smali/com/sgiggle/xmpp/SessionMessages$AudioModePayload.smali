.class public final Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AudioModePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AudioModePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DEVCHANGE_FIELD_NUMBER:I = 0x4

.field public static final MUTED_FIELD_NUMBER:I = 0x3

.field public static final SPEAKERON_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private devchange_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private muted_:Z

.field private speakeron_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14283
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    .line 14284
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->initFields()V

    .line 14285
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 13812
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 13872
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    .line 13910
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedSerializedSize:I

    .line 13813
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 13814
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 13872
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    .line 13910
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedSerializedSize:I

    .line 13814
    return-void
.end method

.method static synthetic access$17802(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$17902(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z

    return p1
.end method

.method static synthetic access$18002(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z

    return p1
.end method

.method static synthetic access$18102(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z

    return p1
.end method

.method static synthetic access$18202(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13807
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1

    .prologue
    .line 13818
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13867
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 13868
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z

    .line 13869
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z

    .line 13870
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z

    .line 13871
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14008
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17600()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 14011
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13977
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    .line 13978
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13979
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    .line 13981
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13988
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    .line 13989
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 13990
    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    .line 13992
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13944
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13950
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13998
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 14004
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13966
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 13972
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13955
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 13961
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;->access$17500(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 13833
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 13807
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;
    .locals 1

    .prologue
    .line 13822
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;

    return-object v0
.end method

.method public getDevchange()Z
    .locals 1

    .prologue
    .line 13863
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z

    return v0
.end method

.method public getMuted()Z
    .locals 1

    .prologue
    .line 13853
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 13912
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedSerializedSize:I

    .line 13913
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 13933
    :goto_0
    return v0

    .line 13915
    :cond_0
    const/4 v0, 0x0

    .line 13916
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 13917
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 13920
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 13921
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13924
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 13925
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13928
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 13929
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 13932
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSpeakeron()Z
    .locals 1

    .prologue
    .line 13843
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevchange()Z
    .locals 2

    .prologue
    .line 13860
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMuted()Z
    .locals 2

    .prologue
    .line 13850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSpeakeron()Z
    .locals 2

    .prologue
    .line 13840
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13874
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    .line 13875
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 13890
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 13875
    goto :goto_0

    .line 13877
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 13878
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 13879
    goto :goto_0

    .line 13881
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->hasDevchange()Z

    move-result v0

    if-nez v0, :cond_3

    .line 13882
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 13883
    goto :goto_0

    .line 13885
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 13886
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 13887
    goto :goto_0

    .line 13889
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 13890
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13807
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14009
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 13807
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;
    .locals 1

    .prologue
    .line 14013
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;)Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 13938
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 13895
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->getSerializedSize()I

    .line 13896
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 13897
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 13899
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 13900
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->speakeron_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13902
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 13903
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->muted_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13905
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 13906
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AudioModePayload;->devchange_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 13908
    :cond_3
    return-void
.end method
