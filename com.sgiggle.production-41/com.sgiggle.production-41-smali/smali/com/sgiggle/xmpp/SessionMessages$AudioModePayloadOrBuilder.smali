.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AudioModePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioModePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getDevchange()Z
.end method

.method public abstract getMuted()Z
.end method

.method public abstract getSpeakeron()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDevchange()Z
.end method

.method public abstract hasMuted()Z
.end method

.method public abstract hasSpeakeron()Z
.end method
