.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

.field private localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

.field private remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

.field private sendInBackground_:Z


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$112900(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$113000()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113202(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113302(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113402(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113502(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    or-int/lit8 v1, v2, 0x10

    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->sendInBackground_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113602(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;Z)Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->access$113702(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;I)I

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    return-object p0
.end method

.method public clearLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearSendInBackground()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    return-object v0
.end method

.method public getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    return-object v0
.end method

.method public getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    return-object v0
.end method

.method public getSendInBackground()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    return v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDirection()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLocalAvatarInfo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRemoteAvatarInfo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendInBackground()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasLocalAvatarInfo()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasRemoteAvatarInfo()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasLocalAvatarInfo()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    goto :goto_0

    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->hasRemoteAvatarInfo()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setRemoteAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->hasDirection()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getDirection()Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setDirection(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->hasLocalAvatarInfo()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getLocalAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->hasRemoteAvatarInfo()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getRemoteAvatarInfo()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->mergeRemoteAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->hasSendInBackground()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;->getSendInBackground()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->setSendInBackground(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;

    :cond_5
    move-object v0, p0

    goto :goto_0
.end method

.method public mergeLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    goto :goto_0
.end method

.method public mergeRemoteAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setDirection(Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->direction_:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    return-object p0
.end method

.method public setLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setLocalAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->localAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setRemoteAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setRemoteAvatarInfo(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->remoteAvatarInfo_:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setSendInBackground(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Builder;->sendInBackground_:Z

    return-object p0
.end method
