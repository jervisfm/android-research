.class public final enum Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Direction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction; = null

.field public static final enum BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction; = null

.field public static final BOTH_VALUE:I = 0x3

.field public static final enum NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction; = null

.field public static final NONE_VALUE:I = 0x0

.field public static final enum RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction; = null

.field public static final RECEIVER_VALUE:I = 0x2

.field public static final enum SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction; = null

.field public static final SENDER_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    const-string v1, "SENDER"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    const-string v1, "RECEIVER"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    const-string v1, "BOTH"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->value:I

    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->NONE:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->SENDER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->RECEIVER:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->BOTH:Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
    .locals 1

    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarControlPayload$Direction;->value:I

    return v0
.end method
