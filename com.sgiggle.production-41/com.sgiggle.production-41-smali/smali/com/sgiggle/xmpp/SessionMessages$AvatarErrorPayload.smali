.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvatarErrorPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ERROR_FIELD_NUMBER:I = 0x2

.field public static final EXTRA_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

.field private extra_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$115002(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$115102(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    return-object p1
.end method

.method static synthetic access$115202(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$115302(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    return-object v0
.end method

.method private getExtraBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;->TEST_AVATAR_ERROR:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114800()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;->access$114700(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    return-object v0
.end method

.method public getExtra()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->extra_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getExtraBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasExtra()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->hasError()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload$Error;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarErrorPayload;->getExtraBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    return-void
.end method
