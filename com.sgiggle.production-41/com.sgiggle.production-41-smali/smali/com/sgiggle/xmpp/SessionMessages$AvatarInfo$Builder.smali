.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarInfoOrBuilder;"
    }
.end annotation


# instance fields
.field private animation_:Ljava/lang/Object;

.field private avatarid_:J

.field private bitField0_:I

.field private mediaDir_:Ljava/lang/Object;

.field private mediaFile_:Ljava/lang/Object;

.field private trackNames_:Lcom/google/protobuf/LazyStringList;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$113900()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;-><init>()V

    return-object v0
.end method

.method private ensureTrackNamesIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllTrackNames(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addTrackNames(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method addTrackNames(Lcom/google/protobuf/ByteString;)V
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114102(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;J)J

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114202(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114302(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    or-int/lit8 v1, v2, 0x8

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114402(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    new-instance v2, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v2, v3}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    :cond_3
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114502(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114602(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;I)I

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 2

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAnimation()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearAvatarid()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    return-object p0
.end method

.method public clearMediaDir()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDir()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearMediaFile()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFile()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearTrackNames()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAnimation()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getAvatarid()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    return-wide v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public getMediaDir()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMediaFile()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTrackNames(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTrackNamesCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getTrackNamesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasAnimation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAvatarid()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaDir()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaFile()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->hasAvatarid()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->hasAvatarid()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAvatarid()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->setAvatarid(J)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->hasMediaDir()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDir()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->setMediaDir(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->hasMediaFile()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFile()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->setMediaFile(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->hasAnimation()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->setAnimation(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    :cond_4
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114500(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114500(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    :cond_5
    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->access$114500(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public setAnimation(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    return-object p0
.end method

.method setAnimation(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->animation_:Ljava/lang/Object;

    return-void
.end method

.method public setAvatarid(J)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->avatarid_:J

    return-object p0
.end method

.method public setMediaDir(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    return-object p0
.end method

.method setMediaDir(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaDir_:Ljava/lang/Object;

    return-void
.end method

.method public setMediaFile(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    return-object p0
.end method

.method setMediaFile(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mediaFile_:Ljava/lang/Object;

    return-void
.end method

.method public setTrackNames(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->ensureTrackNamesIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
