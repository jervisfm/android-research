.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarInfoOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvatarInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    }
.end annotation


# static fields
.field public static final ANIMATION_FIELD_NUMBER:I = 0x4

.field public static final AVATARID_FIELD_NUMBER:I = 0x1

.field public static final MEDIADIR_FIELD_NUMBER:I = 0x2

.field public static final MEDIAFILE_FIELD_NUMBER:I = 0x3

.field public static final TRACKNAMES_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;


# instance fields
.field private animation_:Ljava/lang/Object;

.field private avatarid_:J

.field private bitField0_:I

.field private mediaDir_:Ljava/lang/Object;

.field private mediaFile_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private trackNames_:Lcom/google/protobuf/LazyStringList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$114102(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J

    return-wide p1
.end method

.method static synthetic access$114202(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$114302(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$114402(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$114500(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$114502(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$114602(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    return p1
.end method

.method private getAnimationBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    return-object v0
.end method

.method private getMediaDirBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMediaFileBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113900()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;->access$113800(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnimation()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->animation_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getAvatarid()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J

    return-wide v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;

    return-object v0
.end method

.method public getMediaDir()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaDir_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getMediaFile()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->mediaFile_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J

    invoke-static {v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDirBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFileBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimationBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    move v1, v3

    move v2, v3

    :goto_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getTrackNamesList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedSerializedSize:I

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public getTrackNames(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getTrackNamesCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getTrackNamesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public hasAnimation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAvatarid()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaDir()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaFile()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->hasAvatarid()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0

    :cond_2
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;)Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_0

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->avatarid_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaDirBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getMediaFileBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->getAnimationBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarInfo;->trackNames_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method
