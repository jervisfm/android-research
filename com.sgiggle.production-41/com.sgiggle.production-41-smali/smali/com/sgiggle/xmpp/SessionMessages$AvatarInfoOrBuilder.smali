.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AvatarInfoOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AvatarInfoOrBuilder"
.end annotation


# virtual methods
.method public abstract getAnimation()Ljava/lang/String;
.end method

.method public abstract getAvatarid()J
.end method

.method public abstract getMediaDir()Ljava/lang/String;
.end method

.method public abstract getMediaFile()Ljava/lang/String;
.end method

.method public abstract getTrackNames(I)Ljava/lang/String;
.end method

.method public abstract getTrackNamesCount()I
.end method

.method public abstract getTrackNamesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasAnimation()Z
.end method

.method public abstract hasAvatarid()Z
.end method

.method public abstract hasMediaDir()Z
.end method

.method public abstract hasMediaFile()Z
.end method
