.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private animParam_:Ljava/lang/Object;

.field private animation_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private demo_:Z

.field private ignoreStatus_:Z

.field private isLocal_:Z

.field private looping_:Z

.field private taskId_:I

.field private track_:Ljava/lang/Object;

.field private updatedTracks_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$91200()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91402(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91502(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91602(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91702(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91802(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$91902(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$92002(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z

    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$92102(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z

    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$92202(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v1, v1, 0x200

    const/16 v3, 0x200

    if-ne v1, v3, :cond_9

    or-int/lit16 v1, v2, 0x200

    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$92302(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->access$92402(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I

    return-object v0

    :cond_9
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAnimParam()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimParam()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearAnimation()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimation()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearDemo()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    return-object p0
.end method

.method public clearIgnoreStatus()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    return-object p0
.end method

.method public clearIsLocal()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    return-object p0
.end method

.method public clearLooping()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    return-object p0
.end method

.method public clearTaskId()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    return-object p0
.end method

.method public clearTrack()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTrack()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearUpdatedTracks()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAnimParam()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getAnimation()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDemo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    return v0
.end method

.method public getIgnoreStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    return v0
.end method

.method public getIsLocal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    return v0
.end method

.method public getLooping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    return v0
.end method

.method public getTaskId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    return v0
.end method

.method public getTrack()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getUpdatedTracks()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    return v0
.end method

.method public hasAnimParam()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAnimation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDemo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIgnoreStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsLocal()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLooping()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTaskId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTrack()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpdatedTracks()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasTaskId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasIsLocal()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasAnimation()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasLooping()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    goto/16 :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasTaskId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTaskId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setTaskId(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasIsLocal()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getIsLocal()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setIsLocal(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasAnimation()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setAnimation(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasLooping()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getLooping()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setLooping(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasTrack()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTrack()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setTrack(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasIgnoreStatus()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getIgnoreStatus()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setIgnoreStatus(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasDemo()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDemo()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setDemo(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasAnimParam()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimParam()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setAnimParam(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasUpdatedTracks()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getUpdatedTracks()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->setUpdatedTracks(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    :cond_a
    move-object v0, p0

    goto/16 :goto_0
.end method

.method public setAnimParam(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    return-object p0
.end method

.method setAnimParam(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animParam_:Ljava/lang/Object;

    return-void
.end method

.method public setAnimation(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    return-object p0
.end method

.method setAnimation(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->animation_:Ljava/lang/Object;

    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setDemo(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->demo_:Z

    return-object p0
.end method

.method public setIgnoreStatus(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->ignoreStatus_:Z

    return-object p0
.end method

.method public setIsLocal(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->isLocal_:Z

    return-object p0
.end method

.method public setLooping(Z)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->looping_:Z

    return-object p0
.end method

.method public setTaskId(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->taskId_:I

    return-object p0
.end method

.method public setTrack(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    return-object p0
.end method

.method setTrack(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->track_:Ljava/lang/Object;

    return-void
.end method

.method public setUpdatedTracks(I)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->updatedTracks_:I

    return-object p0
.end method
