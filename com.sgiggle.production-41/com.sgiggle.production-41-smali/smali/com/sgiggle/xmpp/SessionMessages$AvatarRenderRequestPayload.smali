.class public final Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvatarRenderRequestPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    }
.end annotation


# static fields
.field public static final ANIMATION_FIELD_NUMBER:I = 0x4

.field public static final ANIM_PARAM_FIELD_NUMBER:I = 0x9

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DEMO_FIELD_NUMBER:I = 0x8

.field public static final IGNORE_STATUS_FIELD_NUMBER:I = 0x7

.field public static final IS_LOCAL_FIELD_NUMBER:I = 0x3

.field public static final LOOPING_FIELD_NUMBER:I = 0x5

.field public static final TASK_ID_FIELD_NUMBER:I = 0x2

.field public static final TRACK_FIELD_NUMBER:I = 0x6

.field public static final UPDATED_TRACKS_FIELD_NUMBER:I = 0xa

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;


# instance fields
.field private animParam_:Ljava/lang/Object;

.field private animation_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private demo_:Z

.field private ignoreStatus_:Z

.field private isLocal_:Z

.field private looping_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private taskId_:I

.field private track_:Ljava/lang/Object;

.field private updatedTracks_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$91402(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$91502(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I

    return p1
.end method

.method static synthetic access$91602(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z

    return p1
.end method

.method static synthetic access$91702(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$91802(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z

    return p1
.end method

.method static synthetic access$91902(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$92002(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z

    return p1
.end method

.method static synthetic access$92102(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z

    return p1
.end method

.method static synthetic access$92202(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$92302(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I

    return p1
.end method

.method static synthetic access$92402(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    return p1
.end method

.method private getAnimParamBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getAnimationBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    return-object v0
.end method

.method private getTrackBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91200()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;->access$91100(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnimParam()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animParam_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getAnimation()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->animation_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;

    return-object v0
.end method

.method public getDemo()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z

    return v0
.end method

.method public getIgnoreStatus()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z

    return v0
.end method

.method public getIsLocal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z

    return v0
.end method

.method public getLooping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimationBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTrackBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimParamBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getTaskId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I

    return v0
.end method

.method public getTrack()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->track_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getUpdatedTracks()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I

    return v0
.end method

.method public hasAnimParam()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAnimation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDemo()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIgnoreStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsLocal()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLooping()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTaskId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTrack()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUpdatedTracks()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasTaskId()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasIsLocal()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasAnimation()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->hasLooping()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;)Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->taskId_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->isLocal_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimationBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->looping_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getTrackBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->ignoreStatus_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->demo_:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->getAnimParamBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayload;->updatedTracks_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_9
    return-void
.end method
