.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$AvatarRenderRequestPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AvatarRenderRequestPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAnimParam()Ljava/lang/String;
.end method

.method public abstract getAnimation()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getDemo()Z
.end method

.method public abstract getIgnoreStatus()Z
.end method

.method public abstract getIsLocal()Z
.end method

.method public abstract getLooping()Z
.end method

.method public abstract getTaskId()I
.end method

.method public abstract getTrack()Ljava/lang/String;
.end method

.method public abstract getUpdatedTracks()I
.end method

.method public abstract hasAnimParam()Z
.end method

.method public abstract hasAnimation()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDemo()Z
.end method

.method public abstract hasIgnoreStatus()Z
.end method

.method public abstract hasIsLocal()Z
.end method

.method public abstract hasLooping()Z
.end method

.method public abstract hasTaskId()Z
.end method

.method public abstract hasTrack()Z
.end method

.method public abstract hasUpdatedTracks()Z
.end method
