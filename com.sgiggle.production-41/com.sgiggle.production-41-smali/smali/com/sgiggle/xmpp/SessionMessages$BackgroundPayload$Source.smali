.class public final enum Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Source"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source; = null

.field public static final enum IDLE:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source; = null

.field public static final IDLE_VALUE:I = 0x1

.field public static final enum USER:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

.field public static final USER_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39761
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    const-string v1, "USER"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->USER:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    .line 39762
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->IDLE:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    .line 39759
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->USER:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->IDLE:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    .line 39784
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 39793
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39794
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->value:I

    .line 39795
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39781
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    .locals 1
    .parameter

    .prologue
    .line 39772
    packed-switch p0, :pswitch_data_0

    .line 39775
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 39773
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->USER:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    goto :goto_0

    .line 39774
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->IDLE:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    goto :goto_0

    .line 39772
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    .locals 1
    .parameter

    .prologue
    .line 39759
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    .locals 1

    .prologue
    .line 39759
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 39769
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->value:I

    return v0
.end method
