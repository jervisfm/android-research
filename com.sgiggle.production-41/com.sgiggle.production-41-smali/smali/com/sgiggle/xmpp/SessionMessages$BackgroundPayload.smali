.class public final Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackgroundPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final SOURCE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 40151
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    .line 40152
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->initFields()V

    .line 40153
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 39746
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 39825
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    .line 39853
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedSerializedSize:I

    .line 39747
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39741
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 39748
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39825
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    .line 39853
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedSerializedSize:I

    .line 39748
    return-void
.end method

.method static synthetic access$51402(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39741
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$51502(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39741
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    return-object p1
.end method

.method static synthetic access$51602(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39741
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1

    .prologue
    .line 39752
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 39822
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39823
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->USER:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    .line 39824
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;
    .locals 1

    .prologue
    .line 39943
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51200()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39946
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39912
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    .line 39913
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39914
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    .line 39916
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39923
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    .line 39924
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39925
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    .line 39927
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39879
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39885
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39933
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39939
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39901
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39907
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39890
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39896
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;->access$51100(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 39808
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39741
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;
    .locals 1

    .prologue
    .line 39756
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 39855
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedSerializedSize:I

    .line 39856
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 39868
    :goto_0
    return v0

    .line 39858
    :cond_0
    const/4 v0, 0x0

    .line 39859
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 39860
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39863
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 39864
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39867
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;
    .locals 1

    .prologue
    .line 39818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39805
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    .prologue
    .line 39815
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39827
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    .line 39828
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 39839
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 39828
    goto :goto_0

    .line 39830
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 39831
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 39832
    goto :goto_0

    .line 39834
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 39835
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 39836
    goto :goto_0

    .line 39838
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 39839
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39741
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;
    .locals 1

    .prologue
    .line 39944
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39741
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;
    .locals 1

    .prologue
    .line 39948
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;)Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 39873
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 39844
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->getSerializedSize()I

    .line 39845
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 39846
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 39848
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 39849
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BackgroundPayload$Source;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 39851
    :cond_1
    return-void
.end method
