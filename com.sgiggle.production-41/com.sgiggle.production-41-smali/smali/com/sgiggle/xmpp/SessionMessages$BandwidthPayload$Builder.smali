.class public final Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private bandwidthInKbps_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private displaymessage_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 34608
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 34744
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34808
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34609
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->maybeForceBuilderInitialization()V

    .line 34610
    return-void
.end method

.method static synthetic access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34603
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$43800()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34647
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    .line 34648
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34649
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 34652
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34615
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 34613
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 2

    .prologue
    .line 34638
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    .line 34639
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 34640
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 34642
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 5

    .prologue
    .line 34656
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 34657
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34658
    const/4 v2, 0x0

    .line 34659
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 34660
    or-int/lit8 v2, v2, 0x1

    .line 34662
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->access$44002(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34663
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 34664
    or-int/lit8 v2, v2, 0x2

    .line 34666
    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->access$44102(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;I)I

    .line 34667
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 34668
    or-int/lit8 v1, v2, 0x4

    .line 34670
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->access$44202(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34671
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->access$44302(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;I)I

    .line 34672
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34619
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 34620
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34621
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34622
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    .line 34623
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34624
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34625
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34626
    return-object p0
.end method

.method public clearBandwidthInKbps()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34801
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34802
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    .line 34804
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34780
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34782
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34783
    return-object p0
.end method

.method public clearDisplaymessage()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34832
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDisplaymessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34835
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 2

    .prologue
    .line 34630
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBandwidthInKbps()I
    .locals 1

    .prologue
    .line 34792
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 34749
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34603
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1

    .prologue
    .line 34634
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDisplaymessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34813
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34814
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 34815
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 34816
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34819
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBandwidthInKbps()Z
    .locals 2

    .prologue
    .line 34789
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34746
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaymessage()Z
    .locals 2

    .prologue
    .line 34810
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34690
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 34698
    :goto_0
    return v0

    .line 34694
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 34696
    goto :goto_0

    .line 34698
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 34768
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 34770
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34776
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34777
    return-object p0

    .line 34773
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34603
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34603
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34603
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34706
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 34707
    sparse-switch v0, :sswitch_data_0

    .line 34712
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 34714
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 34710
    goto :goto_1

    .line 34719
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 34720
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 34721
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 34723
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 34724
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    goto :goto_0

    .line 34728
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34729
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    goto :goto_0

    .line 34733
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34734
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    goto :goto_0

    .line 34707
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34676
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 34686
    :goto_0
    return-object v0

    .line 34677
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34678
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    .line 34680
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->hasBandwidthInKbps()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 34681
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getBandwidthInKbps()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->setBandwidthInKbps(I)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    .line 34683
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->hasDisplaymessage()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34684
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDisplaymessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->setDisplaymessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 34686
    goto :goto_0
.end method

.method public setBandwidthInKbps(I)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34795
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34796
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bandwidthInKbps_:I

    .line 34798
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34762
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34764
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34765
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34752
    if-nez p1, :cond_0

    .line 34753
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34755
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34757
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34758
    return-object p0
.end method

.method public setDisplaymessage(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34823
    if-nez p1, :cond_0

    .line 34824
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34826
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34827
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34829
    return-object p0
.end method

.method setDisplaymessage(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 34838
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->bitField0_:I

    .line 34839
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->displaymessage_:Ljava/lang/Object;

    .line 34841
    return-void
.end method
