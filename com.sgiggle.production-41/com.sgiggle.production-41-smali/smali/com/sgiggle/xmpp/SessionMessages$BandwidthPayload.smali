.class public final Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BandwidthPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    }
.end annotation


# static fields
.field public static final BANDWIDTHINKBPS_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DISPLAYMESSAGE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;


# instance fields
.field private bandwidthInKbps_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private displaymessage_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34847
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    .line 34848
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->initFields()V

    .line 34849
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 34400
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 34471
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    .line 34502
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedSerializedSize:I

    .line 34401
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34395
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 34402
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 34471
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    .line 34502
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedSerializedSize:I

    .line 34402
    return-void
.end method

.method static synthetic access$44002(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34395
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$44102(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34395
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I

    return p1
.end method

.method static synthetic access$44202(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34395
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$44302(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34395
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1

    .prologue
    .line 34406
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    return-object v0
.end method

.method private getDisplaymessageBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 34455
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    .line 34456
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34457
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 34459
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    .line 34462
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 34467
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 34468
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I

    .line 34469
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    .line 34470
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34596
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43800()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 34599
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34565
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    .line 34566
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34567
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    .line 34569
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34576
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    .line 34577
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34578
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    .line 34580
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34532
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34538
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34586
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34592
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34560
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 34549
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;->access$43700(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBandwidthInKbps()I
    .locals 1

    .prologue
    .line 34431
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 34421
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34395
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;
    .locals 1

    .prologue
    .line 34410
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;

    return-object v0
.end method

.method public getDisplaymessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    .line 34442
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34443
    check-cast v0, Ljava/lang/String;

    .line 34451
    :goto_0
    return-object v0

    .line 34445
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 34447
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 34448
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34449
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->displaymessage_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 34451
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 34504
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedSerializedSize:I

    .line 34505
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 34521
    :goto_0
    return v0

    .line 34507
    :cond_0
    const/4 v0, 0x0

    .line 34508
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 34509
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34512
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 34513
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 34516
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 34517
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDisplaymessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 34520
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBandwidthInKbps()Z
    .locals 2

    .prologue
    .line 34428
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34418
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplaymessage()Z
    .locals 2

    .prologue
    .line 34438
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34473
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    .line 34474
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 34485
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 34474
    goto :goto_0

    .line 34476
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 34477
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 34478
    goto :goto_0

    .line 34480
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 34481
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 34482
    goto :goto_0

    .line 34484
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 34485
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34395
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34597
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34395
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;
    .locals 1

    .prologue
    .line 34601
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;)Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 34526
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 34490
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getSerializedSize()I

    .line 34491
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 34492
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 34494
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 34495
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bandwidthInKbps_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 34497
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 34498
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayload;->getDisplaymessageBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 34500
    :cond_2
    return-void
.end method
