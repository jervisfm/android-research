.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$BandwidthPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BandwidthPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBandwidthInKbps()I
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getDisplaymessage()Ljava/lang/String;
.end method

.method public abstract hasBandwidthInKbps()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDisplaymessage()Z
.end method
