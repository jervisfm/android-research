.class public final Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BaseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$Base;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$Base;",
        "Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$BaseOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private sequenceId_:J

.field private type_:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1427
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 1428
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->maybeForceBuilderInitialization()V

    .line 1429
    return-void
.end method

.method static synthetic access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1422
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1464
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    .line 1465
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1466
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 1469
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1434
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 1432
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 2

    .prologue
    .line 1455
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    .line 1456
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1457
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 1459
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 5

    .prologue
    .line 1473
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Base;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 1474
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1475
    const/4 v2, 0x0

    .line 1476
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1477
    or-int/lit8 v2, v2, 0x1

    .line 1479
    :cond_0
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Base;->access$302(Lcom/sgiggle/xmpp/SessionMessages$Base;I)I

    .line 1480
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 1481
    or-int/lit8 v1, v2, 0x2

    .line 1483
    :goto_0
    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J
    invoke-static {v0, v2, v3}, Lcom/sgiggle/xmpp/SessionMessages$Base;->access$402(Lcom/sgiggle/xmpp/SessionMessages$Base;J)J

    .line 1484
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base;->access$502(Lcom/sgiggle/xmpp/SessionMessages$Base;I)I

    .line 1485
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 2

    .prologue
    .line 1438
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 1439
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    .line 1440
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1441
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    .line 1442
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1443
    return-object p0
.end method

.method public clearSequenceId()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 2

    .prologue
    .line 1580
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1581
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    .line 1583
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1559
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1560
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    .line 1562
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 2

    .prologue
    .line 1447
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1422
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 1451
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public getSequenceId()J
    .locals 2

    .prologue
    .line 1571
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1550
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    return v0
.end method

.method public hasSequenceId()Z
    .locals 2

    .prologue
    .line 1568
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1547
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1500
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1508
    :goto_0
    return v0

    .line 1504
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->hasSequenceId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1506
    goto :goto_0

    .line 1508
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1422
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1422
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1422
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1516
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 1517
    sparse-switch v0, :sswitch_data_0

    .line 1522
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 1524
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 1520
    goto :goto_1

    .line 1529
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1530
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    goto :goto_0

    .line 1534
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1535
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    goto :goto_0

    .line 1517
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 2
    .parameter

    .prologue
    .line 1489
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 1496
    :goto_0
    return-object v0

    .line 1490
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base;->hasType()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1491
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 1493
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base;->hasSequenceId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1494
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getSequenceId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->setSequenceId(J)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_2
    move-object v0, p0

    .line 1496
    goto :goto_0
.end method

.method public setSequenceId(J)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1575
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->sequenceId_:J

    .line 1577
    return-object p0
.end method

.method public setType(I)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1553
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->bitField0_:I

    .line 1554
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->type_:I

    .line 1556
    return-object p0
.end method
