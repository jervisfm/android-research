.class public final Lcom/sgiggle/xmpp/SessionMessages$Base;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BaseOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Base"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    }
.end annotation


# static fields
.field public static final SEQUENCE_ID_FIELD_NUMBER:I = 0x2

.field public static final TYPE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Base;


# instance fields
.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private sequenceId_:J

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1590
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Base;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$Base;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 1591
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Base;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->initFields()V

    .line 1592
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1259
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 1297
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    .line 1325
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedSerializedSize:I

    .line 1260
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1254
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1261
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 1297
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    .line 1325
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedSerializedSize:I

    .line 1261
    return-void
.end method

.method static synthetic access$302(Lcom/sgiggle/xmpp/SessionMessages$Base;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1254
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I

    return p1
.end method

.method static synthetic access$402(Lcom/sgiggle/xmpp/SessionMessages$Base;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1254
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/sgiggle/xmpp/SessionMessages$Base;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1254
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 1265
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Base;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    .line 1294
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I

    .line 1295
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J

    .line 1296
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1415
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$100()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1
    .parameter

    .prologue
    .line 1418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1384
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 1385
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1386
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    .line 1388
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1395
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 1396
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1397
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    .line 1399
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1351
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1357
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1405
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1411
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1373
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1379
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1362
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 1368
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->access$000(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1254
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 1269
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Base;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getSequenceId()J
    .locals 2

    .prologue
    .line 1290
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J

    return-wide v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1327
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedSerializedSize:I

    .line 1328
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1340
    :goto_0
    return v0

    .line 1330
    :cond_0
    const/4 v0, 0x0

    .line 1331
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 1332
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1335
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 1336
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1339
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 1280
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I

    return v0
.end method

.method public hasSequenceId()Z
    .locals 2

    .prologue
    .line 1287
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1277
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1299
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    .line 1300
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 1311
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 1300
    goto :goto_0

    .line 1302
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->hasType()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1303
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    move v0, v2

    .line 1304
    goto :goto_0

    .line 1306
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->hasSequenceId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1307
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    move v0, v2

    .line 1308
    goto :goto_0

    .line 1310
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->memoizedIsInitialized:B

    move v0, v3

    .line 1311
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1254
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1416
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1254
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;
    .locals 1

    .prologue
    .line 1420
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 1345
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 1316
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getSerializedSize()I

    .line 1317
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1318
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->type_:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 1320
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 1321
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Base;->sequenceId_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 1323
    :cond_1
    return-void
.end method
