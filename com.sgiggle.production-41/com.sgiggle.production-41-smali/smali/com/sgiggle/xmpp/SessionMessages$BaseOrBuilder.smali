.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$BaseOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseOrBuilder"
.end annotation


# virtual methods
.method public abstract getSequenceId()J
.end method

.method public abstract getType()I
.end method

.method public abstract hasSequenceId()Z
.end method

.method public abstract hasType()Z
.end method
