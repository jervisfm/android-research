.class public final Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BuddiesListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;",
        "Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$BuddiesListOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private buddy_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6909
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 7018
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 6910
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->maybeForceBuilderInitialization()V

    .line 6911
    return-void
.end method

.method static synthetic access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6904
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7900()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6944
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    .line 6945
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6946
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 6949
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6916
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;-><init>()V

    return-object v0
.end method

.method private ensureBuddyIsMutable()V
    .locals 2

    .prologue
    .line 7021
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 7022
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 7023
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    .line 7025
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 6914
    return-void
.end method


# virtual methods
.method public addAllBuddy(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;"
        }
    .end annotation

    .prologue
    .line 7088
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7089
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 7091
    return-object p0
.end method

.method public addBuddy(ILcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 7081
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7082
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 7084
    return-object p0
.end method

.method public addBuddy(ILcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 7064
    if-nez p2, :cond_0

    .line 7065
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7067
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7068
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 7070
    return-object p0
.end method

.method public addBuddy(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 7074
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7075
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7077
    return-object p0
.end method

.method public addBuddy(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7054
    if-nez p1, :cond_0

    .line 7055
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7057
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7058
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 7060
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 2

    .prologue
    .line 6935
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    .line 6936
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6937
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 6939
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 3

    .prologue
    .line 6953
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 6954
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    .line 6955
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 6956
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 6957
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    .line 6959
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->access$8102(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;Ljava/util/List;)Ljava/util/List;

    .line 6960
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6920
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 6921
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 6922
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    .line 6923
    return-object p0
.end method

.method public clearBuddy()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 7094
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 7095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    .line 7097
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 2

    .prologue
    .line 6927
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter

    .prologue
    .line 7034
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    return-object v0
.end method

.method public getBuddyCount()I
    .locals 1

    .prologue
    .line 7031
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBuddyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 7028
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1

    .prologue
    .line 6931
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6979
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->getBuddyCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 6980
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->getBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    .line 6985
    :goto_1
    return v0

    .line 6979
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6985
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6904
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6904
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6904
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6993
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6994
    sparse-switch v0, :sswitch_data_0

    .line 6999
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 7001
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 6997
    goto :goto_1

    .line 7006
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    .line 7007
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 7008
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->addBuddy(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    goto :goto_0

    .line 6994
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 6964
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 6975
    :goto_0
    return-object v0

    .line 6965
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->access$8100(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 6966
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6967
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->access$8100(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    .line 6968
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 6975
    goto :goto_0

    .line 6970
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 6971
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->access$8100(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 7100
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7101
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 7103
    return-object p0
.end method

.method public setBuddy(ILcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 7048
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7049
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 7051
    return-object p0
.end method

.method public setBuddy(ILcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 7038
    if-nez p2, :cond_0

    .line 7039
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 7041
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->ensureBuddyIsMutable()V

    .line 7042
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 7044
    return-object p0
.end method
