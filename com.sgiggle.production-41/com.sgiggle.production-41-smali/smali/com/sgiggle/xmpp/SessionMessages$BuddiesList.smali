.class public final Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BuddiesListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BuddiesList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    }
.end annotation


# static fields
.field public static final BUDDY_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;


# instance fields
.field private buddy_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 7110
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    .line 7111
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->initFields()V

    .line 7112
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 6751
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6788
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedIsInitialized:B

    .line 6811
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedSerializedSize:I

    .line 6752
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6746
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 6753
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6788
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedIsInitialized:B

    .line 6811
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedSerializedSize:I

    .line 6753
    return-void
.end method

.method static synthetic access$8100(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 6746
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8102(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6746
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1

    .prologue
    .line 6757
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 6786
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    .line 6787
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6897
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7900()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6900
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6866
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    .line 6867
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6868
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    .line 6870
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6877
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    .line 6878
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6879
    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    .line 6881
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6839
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6855
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6861
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6844
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6850
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;->access$7800(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter

    .prologue
    .line 6778
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    return-object v0
.end method

.method public getBuddyCount()I
    .locals 1

    .prologue
    .line 6775
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getBuddyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6768
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    return-object v0
.end method

.method public getBuddyOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 6782
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;

    return-object v0
.end method

.method public getBuddyOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6772
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6746
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;
    .locals 1

    .prologue
    .line 6761
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 6813
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedSerializedSize:I

    .line 6814
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6822
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 6817
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 6818
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 6817
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 6821
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedSerializedSize:I

    move v0, v2

    .line 6822
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6790
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedIsInitialized:B

    .line 6791
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 6800
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 6791
    goto :goto_0

    :cond_1
    move v0, v2

    .line 6793
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getBuddyCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 6794
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    .line 6795
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedIsInitialized:B

    move v0, v2

    .line 6796
    goto :goto_0

    .line 6793
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6799
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->memoizedIsInitialized:B

    move v0, v3

    .line 6800
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6746
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6898
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6746
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;
    .locals 1

    .prologue
    .line 6902
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;)Lcom/sgiggle/xmpp/SessionMessages$BuddiesList$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 6827
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6805
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->getSerializedSize()I

    .line 6806
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 6807
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$BuddiesList;->buddy_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 6806
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 6809
    :cond_0
    return-void
.end method
