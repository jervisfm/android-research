.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$BuddiesListOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BuddiesListOrBuilder"
.end annotation


# virtual methods
.method public abstract getBuddy(I)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
.end method

.method public abstract getBuddyCount()I
.end method

.method public abstract getBuddyList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
            ">;"
        }
    .end annotation
.end method
