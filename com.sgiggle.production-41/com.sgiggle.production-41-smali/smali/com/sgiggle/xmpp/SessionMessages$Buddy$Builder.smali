.class public final Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$Buddy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$Buddy;",
        "Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;"
    }
.end annotation


# instance fields
.field private ask_:Ljava/lang/Object;

.field private bitField0_:I

.field private jid_:Ljava/lang/Object;

.field private name_:Ljava/lang/Object;

.field private subscription_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 6429
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 6583
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6619
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6655
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6691
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6430
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->maybeForceBuilderInitialization()V

    .line 6431
    return-void
.end method

.method static synthetic access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6424
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7100()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6470
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    .line 6471
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6472
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 6475
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6436
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 6434
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 2

    .prologue
    .line 6461
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    .line 6462
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 6463
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 6465
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 5

    .prologue
    .line 6479
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 6480
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6481
    const/4 v2, 0x0

    .line 6482
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 6483
    or-int/lit8 v2, v2, 0x1

    .line 6485
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->access$7302(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6486
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 6487
    or-int/lit8 v2, v2, 0x2

    .line 6489
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->access$7402(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6490
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 6491
    or-int/lit8 v2, v2, 0x4

    .line 6493
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->access$7502(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6494
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 6495
    or-int/lit8 v1, v2, 0x8

    .line 6497
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->access$7602(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6498
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->access$7702(Lcom/sgiggle/xmpp/SessionMessages$Buddy;I)I

    .line 6499
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6440
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 6441
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6442
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6443
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6444
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6445
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6446
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6447
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6448
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6449
    return-object p0
.end method

.method public clearAsk()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6715
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6716
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getAsk()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6718
    return-object p0
.end method

.method public clearJid()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6607
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6608
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getJid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6610
    return-object p0
.end method

.method public clearName()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6643
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6644
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6646
    return-object p0
.end method

.method public clearSubscription()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6679
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6680
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getSubscription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6682
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 2

    .prologue
    .line 6453
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAsk()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6696
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6697
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 6698
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 6699
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6702
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6424
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1

    .prologue
    .line 6457
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6588
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6589
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 6590
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 6591
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6594
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6624
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6625
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 6626
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 6627
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6630
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubscription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6660
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6661
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 6662
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 6663
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6666
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasAsk()Z
    .locals 2

    .prologue
    .line 6693
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6585
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    .line 6621
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscription()Z
    .locals 2

    .prologue
    .line 6657
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6520
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->hasJid()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 6536
    :goto_0
    return v0

    .line 6524
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->hasName()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 6526
    goto :goto_0

    .line 6528
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->hasSubscription()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 6530
    goto :goto_0

    .line 6532
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->hasAsk()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 6534
    goto :goto_0

    .line 6536
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6424
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6424
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6424
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6544
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 6545
    sparse-switch v0, :sswitch_data_0

    .line 6550
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 6552
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 6548
    goto :goto_1

    .line 6557
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6558
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    goto :goto_0

    .line 6562
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6563
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    goto :goto_0

    .line 6567
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6568
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    goto :goto_0

    .line 6572
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6573
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    goto :goto_0

    .line 6545
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6503
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 6516
    :goto_0
    return-object v0

    .line 6504
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasJid()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6505
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    .line 6507
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 6508
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->setName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    .line 6510
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasSubscription()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 6511
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getSubscription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->setSubscription(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    .line 6513
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasAsk()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 6514
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getAsk()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->setAsk(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    :cond_4
    move-object v0, p0

    .line 6516
    goto :goto_0
.end method

.method public setAsk(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6706
    if-nez p1, :cond_0

    .line 6707
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6709
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6710
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6712
    return-object p0
.end method

.method setAsk(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 6721
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6722
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->ask_:Ljava/lang/Object;

    .line 6724
    return-void
.end method

.method public setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6598
    if-nez p1, :cond_0

    .line 6599
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6601
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6602
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6604
    return-object p0
.end method

.method setJid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 6613
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6614
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->jid_:Ljava/lang/Object;

    .line 6616
    return-void
.end method

.method public setName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6634
    if-nez p1, :cond_0

    .line 6635
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6637
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6638
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6640
    return-object p0
.end method

.method setName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 6649
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6650
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->name_:Ljava/lang/Object;

    .line 6652
    return-void
.end method

.method public setSubscription(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6670
    if-nez p1, :cond_0

    .line 6671
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6673
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6674
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6676
    return-object p0
.end method

.method setSubscription(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 6685
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->bitField0_:I

    .line 6686
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->subscription_:Ljava/lang/Object;

    .line 6688
    return-void
.end method
