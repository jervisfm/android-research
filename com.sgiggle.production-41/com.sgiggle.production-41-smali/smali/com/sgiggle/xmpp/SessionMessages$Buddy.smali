.class public final Lcom/sgiggle/xmpp/SessionMessages$Buddy;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Buddy"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    }
.end annotation


# static fields
.field public static final ASK_FIELD_NUMBER:I = 0x4

.field public static final JID_FIELD_NUMBER:I = 0x1

.field public static final NAME_FIELD_NUMBER:I = 0x2

.field public static final SUBSCRIPTION_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Buddy;


# instance fields
.field private ask_:Ljava/lang/Object;

.field private bitField0_:I

.field private jid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private name_:Ljava/lang/Object;

.field private subscription_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 6730
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    .line 6731
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->initFields()V

    .line 6732
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 6129
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 6277
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    .line 6319
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedSerializedSize:I

    .line 6130
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 6131
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 6277
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    .line 6319
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedSerializedSize:I

    .line 6131
    return-void
.end method

.method static synthetic access$7302(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$7402(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$7502(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$7602(Lcom/sgiggle/xmpp/SessionMessages$Buddy;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$7702(Lcom/sgiggle/xmpp/SessionMessages$Buddy;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 6124
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    return p1
.end method

.method private getAskBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6260
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    .line 6261
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6262
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6264
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    .line 6267
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1

    .prologue
    .line 6135
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    return-object v0
.end method

.method private getJidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6164
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    .line 6165
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6166
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6168
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    .line 6171
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6196
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    .line 6197
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6198
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6200
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    .line 6203
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSubscriptionBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 6228
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    .line 6229
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6230
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 6232
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    .line 6235
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 6272
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    .line 6273
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    .line 6274
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    .line 6275
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    .line 6276
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6417
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7100()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1
    .parameter

    .prologue
    .line 6420
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    .line 6387
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6388
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    .line 6390
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6397
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    .line 6398
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 6399
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    .line 6401
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6353
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6359
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6407
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6413
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6375
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6381
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6364
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 6370
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;->access$7000(Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAsk()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6246
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    .line 6247
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6248
    check-cast v0, Ljava/lang/String;

    .line 6256
    :goto_0
    return-object v0

    .line 6250
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6252
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6253
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6254
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->ask_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6256
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 6124
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy;
    .locals 1

    .prologue
    .line 6139
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Buddy;

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6150
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    .line 6151
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6152
    check-cast v0, Ljava/lang/String;

    .line 6160
    :goto_0
    return-object v0

    .line 6154
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6156
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6157
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6158
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->jid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6160
    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6182
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    .line 6183
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6184
    check-cast v0, Ljava/lang/String;

    .line 6192
    :goto_0
    return-object v0

    .line 6186
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6188
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6189
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6190
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->name_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6192
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 6321
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedSerializedSize:I

    .line 6322
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 6342
    :goto_0
    return v0

    .line 6324
    :cond_0
    const/4 v0, 0x0

    .line 6325
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 6326
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6329
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 6330
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6333
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 6334
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getSubscriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6337
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 6338
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getAskBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6341
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSubscription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 6214
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    .line 6215
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 6216
    check-cast v0, Ljava/lang/String;

    .line 6224
    :goto_0
    return-object v0

    .line 6218
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 6220
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 6221
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 6222
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->subscription_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 6224
    goto :goto_0
.end method

.method public hasAsk()Z
    .locals 2

    .prologue
    .line 6243
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 6147
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasName()Z
    .locals 2

    .prologue
    .line 6179
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscription()Z
    .locals 2

    .prologue
    .line 6211
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 6279
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    .line 6280
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 6299
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 6280
    goto :goto_0

    .line 6282
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasJid()Z

    move-result v0

    if-nez v0, :cond_2

    .line 6283
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    move v0, v2

    .line 6284
    goto :goto_0

    .line 6286
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasName()Z

    move-result v0

    if-nez v0, :cond_3

    .line 6287
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    move v0, v2

    .line 6288
    goto :goto_0

    .line 6290
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasSubscription()Z

    move-result v0

    if-nez v0, :cond_4

    .line 6291
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    move v0, v2

    .line 6292
    goto :goto_0

    .line 6294
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->hasAsk()Z

    move-result v0

    if-nez v0, :cond_5

    .line 6295
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    move v0, v2

    .line 6296
    goto :goto_0

    .line 6298
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->memoizedIsInitialized:B

    move v0, v3

    .line 6299
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6124
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 6124
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;
    .locals 1

    .prologue
    .line 6422
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Buddy;)Lcom/sgiggle/xmpp/SessionMessages$Buddy$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 6347
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 6304
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getSerializedSize()I

    .line 6305
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 6306
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6308
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 6309
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6311
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 6312
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getSubscriptionBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6314
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 6315
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Buddy;->getAskBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 6317
    :cond_3
    return-void
.end method
