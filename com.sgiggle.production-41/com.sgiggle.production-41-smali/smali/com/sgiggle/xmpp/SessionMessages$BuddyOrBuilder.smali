.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$BuddyOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BuddyOrBuilder"
.end annotation


# virtual methods
.method public abstract getAsk()Ljava/lang/String;
.end method

.method public abstract getJid()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSubscription()Ljava/lang/String;
.end method

.method public abstract hasAsk()Z
.end method

.method public abstract hasJid()Z
.end method

.method public abstract hasName()Z
.end method

.method public abstract hasSubscription()Z
.end method
