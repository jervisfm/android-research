.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

.field private resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42035
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 42210
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42253
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 42277
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 42301
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42036
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 42037
    return-void
.end method

.method static synthetic access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42030
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$53700()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42076
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    .line 42077
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42078
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 42081
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42042
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntriesIsMutable()V
    .locals 2

    .prologue
    .line 42304
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 42305
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42306
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42308
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 42040
    return-void
.end method


# virtual methods
.method public addAllEntries(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 42371
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42372
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 42374
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 42364
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42365
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 42367
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42347
    if-nez p2, :cond_0

    .line 42348
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42350
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42351
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 42353
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42357
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42358
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42360
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42337
    if-nez p1, :cond_0

    .line 42338
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42340
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42341
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42343
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 2

    .prologue
    .line 42067
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    .line 42068
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42069
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 42071
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 5

    .prologue
    .line 42085
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 42086
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42087
    const/4 v2, 0x0

    .line 42088
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 42089
    or-int/lit8 v2, v2, 0x1

    .line 42091
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$53902(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42092
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 42093
    or-int/lit8 v2, v2, 0x2

    .line 42095
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54002(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 42096
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_3

    .line 42097
    or-int/lit8 v1, v2, 0x4

    .line 42099
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54102(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 42100
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_2

    .line 42101
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42102
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x9

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42104
    :cond_2
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54202(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Ljava/util/List;)Ljava/util/List;

    .line 42105
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54302(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;I)I

    .line 42106
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42046
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 42047
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42048
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42049
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 42050
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42051
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 42052
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42053
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42054
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42055
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42246
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42248
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42249
    return-object p0
.end method

.method public clearEntries()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42377
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42378
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42380
    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42294
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42295
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 42297
    return-object p0
.end method

.method public clearResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42270
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42271
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 42273
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2

    .prologue
    .line 42059
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 42215
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1

    .prologue
    .line 42063
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 42317
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 42314
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42311
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 1

    .prologue
    .line 42282
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    return-object v0
.end method

.method public getResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 1

    .prologue
    .line 42258
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 42212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 42279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultType()Z
    .locals 2

    .prologue
    .line 42255
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42134
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 42150
    :goto_0
    return v0

    .line 42138
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->hasResultType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 42140
    goto :goto_0

    .line 42142
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->hasError()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 42144
    goto :goto_0

    .line 42146
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 42148
    goto :goto_0

    .line 42150
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42234
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 42236
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42242
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42243
    return-object p0

    .line 42239
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42030
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42030
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42030
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 42159
    sparse-switch v0, :sswitch_data_0

    .line 42164
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 42166
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 42162
    goto :goto_1

    .line 42171
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 42172
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42173
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 42175
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 42176
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    goto :goto_0

    .line 42180
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 42181
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    move-result-object v0

    .line 42182
    if-eqz v0, :cond_0

    .line 42183
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42184
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    goto :goto_0

    .line 42189
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 42190
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    move-result-object v0

    .line 42191
    if-eqz v0, :cond_0

    .line 42192
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42193
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    goto :goto_0

    .line 42198
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    .line 42199
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 42200
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    goto :goto_0

    .line 42159
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42110
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 42130
    :goto_0
    return-object v0

    .line 42111
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42112
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    .line 42114
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasResultType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42115
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->setResultType(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    .line 42117
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42118
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getError()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->setError(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    .line 42120
    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54200(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 42121
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 42122
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54200(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42123
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    :cond_4
    :goto_1
    move-object v0, p0

    .line 42130
    goto :goto_0

    .line 42125
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42126
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->access$54200(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42383
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42384
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 42386
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42228
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42230
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42231
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42218
    if-nez p1, :cond_0

    .line 42219
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42221
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42223
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42224
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 42331
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42332
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 42334
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42321
    if-nez p2, :cond_0

    .line 42322
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42324
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42325
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 42327
    return-object p0
.end method

.method public setError(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42285
    if-nez p1, :cond_0

    .line 42286
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42288
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42289
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 42291
    return-object p0
.end method

.method public setResultType(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42261
    if-nez p1, :cond_0

    .line 42262
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42264
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->bitField0_:I

    .line 42265
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 42267
    return-object p0
.end method
