.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState; = null

.field public static final enum NETWORK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState; = null

.field public static final NETWORK_VALUE:I = 0x2

.field public static final enum OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState; = null

.field public static final OK_VALUE:I = 0x0

.field public static final enum SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState; = null

.field public static final SERVER_FAILURE_VALUE:I = 0x3

.field public static final enum TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState; = null

.field public static final TIMEOUT_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41780
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41781
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    const-string v1, "TIMEOUT"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41782
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41783
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    const-string v1, "SERVER_FAILURE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41778
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41809
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 41818
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41819
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->value:I

    .line 41820
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41806
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 1
    .parameter

    .prologue
    .line 41795
    packed-switch p0, :pswitch_data_0

    .line 41800
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 41796
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    goto :goto_0

    .line 41797
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    goto :goto_0

    .line 41798
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->NETWORK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    goto :goto_0

    .line 41799
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->SERVER_FAILURE:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    goto :goto_0

    .line 41795
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 1
    .parameter

    .prologue
    .line 41778
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 1

    .prologue
    .line 41778
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 41792
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->value:I

    return v0
.end method
