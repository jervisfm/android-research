.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResultType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType; = null

.field public static final enum CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType; = null

.field public static final CACHED_VALUE:I = 0x0

.field public static final enum SERVER:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType; = null

.field public static final SERVER_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41739
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    const-string v1, "CACHED"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 41740
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 41737
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 41762
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 41771
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41772
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->value:I

    .line 41773
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41759
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 1
    .parameter

    .prologue
    .line 41750
    packed-switch p0, :pswitch_data_0

    .line 41753
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 41751
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    goto :goto_0

    .line 41752
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->SERVER:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    goto :goto_0

    .line 41750
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 1
    .parameter

    .prologue
    .line 41737
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 1

    .prologue
    .line 41737
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 41747
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->value:I

    return v0
.end method
