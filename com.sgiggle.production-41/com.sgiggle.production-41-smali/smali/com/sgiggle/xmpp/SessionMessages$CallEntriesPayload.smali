.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallEntriesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;,
        Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ENTRIES_FIELD_NUMBER:I = 0x4

.field public static final ERROR_FIELD_NUMBER:I = 0x3

.field public static final RESULT_TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field

.field private error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42393
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    .line 42394
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->initFields()V

    .line 42395
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 41724
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 41883
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    .line 41925
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedSerializedSize:I

    .line 41725
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 41726
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 41883
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    .line 41925
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedSerializedSize:I

    .line 41726
    return-void
.end method

.method static synthetic access$53902(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$54002(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    return-object p1
.end method

.method static synthetic access$54102(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    return-object p1
.end method

.method static synthetic access$54200(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 41719
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$54202(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$54302(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 41719
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1

    .prologue
    .line 41730
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 41878
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 41879
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->CACHED:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    .line 41880
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->OK:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    .line 41881
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    .line 41882
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42023
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53700()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42026
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41992
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    .line 41993
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41994
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    .line 41996
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42003
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    .line 42004
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42005
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    .line 42007
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 41959
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 41965
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42013
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41981
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41987
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 41970
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 41976
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;->access$53600(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 41833
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 41719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;
    .locals 1

    .prologue
    .line 41734
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 41870
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 41867
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41860
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 41874
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;

    return-object v0
.end method

.method public getEntriesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41864
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getError()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;
    .locals 1

    .prologue
    .line 41853
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    return-object v0
.end method

.method public getResultType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;
    .locals 1

    .prologue
    .line 41843
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 41927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedSerializedSize:I

    .line 41928
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 41948
    :goto_0
    return v0

    .line 41931
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_4

    .line 41932
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    .line 41935
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 41936
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 41939
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 41940
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    .line 41943
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 41944
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 41943
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 41947
    :cond_3
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 41948
    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 41830
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 41850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasResultType()Z
    .locals 2

    .prologue
    .line 41840
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41885
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    .line 41886
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 41905
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 41886
    goto :goto_0

    .line 41888
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 41889
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 41890
    goto :goto_0

    .line 41892
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasResultType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 41893
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 41894
    goto :goto_0

    .line 41896
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->hasError()Z

    move-result v0

    if-nez v0, :cond_4

    .line 41897
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 41898
    goto :goto_0

    .line 41900
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 41901
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 41902
    goto :goto_0

    .line 41904
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 41905
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 41719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42024
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 41719
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42028
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 41953
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 41910
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->getSerializedSize()I

    .line 41911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 41912
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 41914
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 41915
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->resultType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ResultType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 41917
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 41918
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->error_:Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload$ErrorState;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 41920
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 41921
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 41920
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 41923
    :cond_3
    return-void
.end method
