.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;"
    }
.end annotation


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private bitField0_:I

.field private callId_:Ljava/lang/Object;

.field private callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

.field private deviceContactId_:J

.field private displayName_:Ljava/lang/Object;

.field private duration_:I

.field private email_:Ljava/lang/Object;

.field private firstName_:Ljava/lang/Object;

.field private lastName_:Ljava/lang/Object;

.field private middleName_:Ljava/lang/Object;

.field private namePrefix_:Ljava/lang/Object;

.field private nameSuffix_:Ljava/lang/Object;

.field private peerId_:Ljava/lang/Object;

.field private phoneNumber_:Ljava/lang/Object;

.field private startTime_:J


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40909
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 41205
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41241
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41277
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41313
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41349
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 41436
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41472
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41508
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41544
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41580
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41616
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41652
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 40910
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->maybeForceBuilderInitialization()V

    .line 40911
    return-void
.end method

.method static synthetic access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40904
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$51800()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40972
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    .line 40973
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 40974
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 40977
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 40916
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 40914
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 2

    .prologue
    .line 40963
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    .line 40964
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 40965
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 40967
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 5

    .prologue
    .line 40981
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 40982
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40983
    const/4 v2, 0x0

    .line 40984
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 40985
    or-int/lit8 v2, v2, 0x1

    .line 40987
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52002(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40988
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 40989
    or-int/lit8 v2, v2, 0x2

    .line 40991
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52102(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40992
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 40993
    or-int/lit8 v2, v2, 0x4

    .line 40995
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52202(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40996
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 40997
    or-int/lit8 v2, v2, 0x8

    .line 40999
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52302(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41000
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 41001
    or-int/lit8 v2, v2, 0x10

    .line 41003
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52402(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 41004
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 41005
    or-int/lit8 v2, v2, 0x20

    .line 41007
    :cond_5
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52502(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;J)J

    .line 41008
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 41009
    or-int/lit8 v2, v2, 0x40

    .line 41011
    :cond_6
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52602(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;I)I

    .line 41012
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 41013
    or-int/lit16 v2, v2, 0x80

    .line 41015
    :cond_7
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52702(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;J)J

    .line 41016
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 41017
    or-int/lit16 v2, v2, 0x100

    .line 41019
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52802(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41020
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 41021
    or-int/lit16 v2, v2, 0x200

    .line 41023
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$52902(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41024
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 41025
    or-int/lit16 v2, v2, 0x400

    .line 41027
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53002(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41028
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 41029
    or-int/lit16 v2, v2, 0x800

    .line 41031
    :cond_b
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53102(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41032
    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 41033
    or-int/lit16 v2, v2, 0x1000

    .line 41035
    :cond_c
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53202(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41036
    and-int/lit16 v3, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    .line 41037
    or-int/lit16 v2, v2, 0x2000

    .line 41039
    :cond_d
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53302(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41040
    and-int/lit16 v1, v1, 0x4000

    const/16 v3, 0x4000

    if-ne v1, v3, :cond_e

    .line 41041
    or-int/lit16 v1, v2, 0x4000

    .line 41043
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53402(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41044
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->access$53502(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;I)I

    .line 41045
    return-object v0

    :cond_e
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 40920
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 40921
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 40922
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40923
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 40924
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40925
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 40926
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40927
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 40928
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40929
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40930
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40931
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    .line 40932
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40933
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    .line 40934
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40935
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    .line 40936
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40937
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 40938
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40939
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 40940
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40941
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 40942
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40943
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 40944
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40945
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 40946
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40947
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 40948
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40949
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 40950
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 40951
    return-object p0
.end method

.method public clearAccountId()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41229
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41230
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getAccountId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41232
    return-object p0
.end method

.method public clearCallId()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41568
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41569
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41571
    return-object p0
.end method

.method public clearCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41366
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41367
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 41369
    return-object p0
.end method

.method public clearDeviceContactId()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 2

    .prologue
    .line 41429
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41430
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    .line 41432
    return-object p0
.end method

.method public clearDisplayName()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41265
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41266
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41268
    return-object p0
.end method

.method public clearDuration()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41408
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41409
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    .line 41411
    return-object p0
.end method

.method public clearEmail()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41337
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41338
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41340
    return-object p0
.end method

.method public clearFirstName()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41460
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41461
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getFirstName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41463
    return-object p0
.end method

.method public clearLastName()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41496
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41497
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getLastName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41499
    return-object p0
.end method

.method public clearMiddleName()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41640
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41641
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getMiddleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41643
    return-object p0
.end method

.method public clearNamePrefix()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41604
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41605
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNamePrefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41607
    return-object p0
.end method

.method public clearNameSuffix()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41676
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41677
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNameSuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 41679
    return-object p0
.end method

.method public clearPeerId()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41532
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41533
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPeerId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41535
    return-object p0
.end method

.method public clearPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 41301
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41302
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41304
    return-object p0
.end method

.method public clearStartTime()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 2

    .prologue
    .line 41387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41388
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    .line 41390
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 2

    .prologue
    .line 40955
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41210
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41211
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41212
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41213
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41216
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCallId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41549
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41550
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41551
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41552
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41555
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 1

    .prologue
    .line 41354
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 40904
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1

    .prologue
    .line 40959
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 41420
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41246
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41247
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41248
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41249
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41252
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 41399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41318
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41319
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41320
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41321
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41324
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41441
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41442
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41443
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41444
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41447
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41477
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41478
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41479
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41480
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41483
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddleName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41621
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41622
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41623
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41624
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41627
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamePrefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41585
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41586
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41587
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41588
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41591
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameSuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41657
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 41658
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41659
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41660
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 41663
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPeerId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41513
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41514
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41515
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41516
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41519
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41282
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41283
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 41284
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 41285
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41288
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 41378
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    return-wide v0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 41207
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallId()Z
    .locals 2

    .prologue
    .line 41546
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallType()Z
    .locals 2

    .prologue
    .line 41351
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 41417
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayName()Z
    .locals 2

    .prologue
    .line 41243
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 41396
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 41315
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstName()Z
    .locals 2

    .prologue
    .line 41438
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastName()Z
    .locals 2

    .prologue
    .line 41474
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddleName()Z
    .locals 2

    .prologue
    .line 41618
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamePrefix()Z
    .locals 2

    .prologue
    .line 41582
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameSuffix()Z
    .locals 2

    .prologue
    .line 41654
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerId()Z
    .locals 2

    .prologue
    .line 41510
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 41279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStartTime()Z
    .locals 2

    .prologue
    .line 41375
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 41099
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40904
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 40904
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40904
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 41107
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 41108
    sparse-switch v0, :sswitch_data_0

    .line 41113
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 41115
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 41111
    goto :goto_1

    .line 41120
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41121
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    goto :goto_0

    .line 41125
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41126
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    goto :goto_0

    .line 41130
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41131
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    goto :goto_0

    .line 41135
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41136
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    goto :goto_0

    .line 41140
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 41141
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    move-result-object v0

    .line 41142
    if-eqz v0, :cond_0

    .line 41143
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41144
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    goto :goto_0

    .line 41149
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41150
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    goto :goto_0

    .line 41154
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41155
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    goto :goto_0

    .line 41159
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41160
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    goto :goto_0

    .line 41164
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41165
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41169
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41170
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41174
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41175
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41179
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41180
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41184
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41185
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41189
    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41190
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41194
    :sswitch_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41195
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 41108
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 2
    .parameter

    .prologue
    .line 41049
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 41095
    :goto_0
    return-object v0

    .line 41050
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasAccountId()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41051
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getAccountId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41053
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasDisplayName()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 41054
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41056
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 41057
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setPhoneNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41059
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 41060
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41062
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasCallType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 41063
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setCallType(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41065
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasStartTime()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 41066
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getStartTime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setStartTime(J)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41068
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 41069
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41071
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasDeviceContactId()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 41072
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDeviceContactId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41074
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasFirstName()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 41075
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getFirstName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setFirstName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41077
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasLastName()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 41078
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getLastName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setLastName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41080
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasPeerId()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 41081
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPeerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setPeerId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41083
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasCallId()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 41084
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setCallId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41086
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasNamePrefix()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 41087
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNamePrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setNamePrefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41089
    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasMiddleName()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 41090
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getMiddleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setMiddleName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    .line 41092
    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->hasNameSuffix()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 41093
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNameSuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->setNameSuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    :cond_f
    move-object v0, p0

    .line 41095
    goto/16 :goto_0
.end method

.method public setAccountId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41220
    if-nez p1, :cond_0

    .line 41221
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41223
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41224
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41226
    return-object p0
.end method

.method setAccountId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41235
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41236
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->accountId_:Ljava/lang/Object;

    .line 41238
    return-void
.end method

.method public setCallId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41559
    if-nez p1, :cond_0

    .line 41560
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41562
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41563
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41565
    return-object p0
.end method

.method setCallId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41574
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41575
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callId_:Ljava/lang/Object;

    .line 41577
    return-void
.end method

.method public setCallType(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41357
    if-nez p1, :cond_0

    .line 41358
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41360
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41361
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 41363
    return-object p0
.end method

.method public setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41423
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41424
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->deviceContactId_:J

    .line 41426
    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41256
    if-nez p1, :cond_0

    .line 41257
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41259
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41260
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41262
    return-object p0
.end method

.method setDisplayName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41271
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41272
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->displayName_:Ljava/lang/Object;

    .line 41274
    return-void
.end method

.method public setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41402
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41403
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->duration_:I

    .line 41405
    return-object p0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41328
    if-nez p1, :cond_0

    .line 41329
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41331
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41332
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41334
    return-object p0
.end method

.method setEmail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41343
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41344
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->email_:Ljava/lang/Object;

    .line 41346
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41451
    if-nez p1, :cond_0

    .line 41452
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41454
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41455
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41457
    return-object p0
.end method

.method setFirstName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41466
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41467
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->firstName_:Ljava/lang/Object;

    .line 41469
    return-void
.end method

.method public setLastName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41487
    if-nez p1, :cond_0

    .line 41488
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41490
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41491
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41493
    return-object p0
.end method

.method setLastName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41502
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41503
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->lastName_:Ljava/lang/Object;

    .line 41505
    return-void
.end method

.method public setMiddleName(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41631
    if-nez p1, :cond_0

    .line 41632
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41634
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41635
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41637
    return-object p0
.end method

.method setMiddleName(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41646
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41647
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->middleName_:Ljava/lang/Object;

    .line 41649
    return-void
.end method

.method public setNamePrefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41595
    if-nez p1, :cond_0

    .line 41596
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41598
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41599
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41601
    return-object p0
.end method

.method setNamePrefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41610
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41611
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->namePrefix_:Ljava/lang/Object;

    .line 41613
    return-void
.end method

.method public setNameSuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41667
    if-nez p1, :cond_0

    .line 41668
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41670
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41671
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 41673
    return-object p0
.end method

.method setNameSuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41682
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41683
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->nameSuffix_:Ljava/lang/Object;

    .line 41685
    return-void
.end method

.method public setPeerId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41523
    if-nez p1, :cond_0

    .line 41524
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41526
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41527
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41529
    return-object p0
.end method

.method setPeerId(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41538
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41539
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->peerId_:Ljava/lang/Object;

    .line 41541
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41292
    if-nez p1, :cond_0

    .line 41293
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 41295
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41296
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41298
    return-object p0
.end method

.method setPhoneNumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 41307
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41308
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->phoneNumber_:Ljava/lang/Object;

    .line 41310
    return-void
.end method

.method public setStartTime(J)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 41381
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->bitField0_:I

    .line 41382
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->startTime_:J

    .line 41384
    return-object p0
.end method
