.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CallType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType; = null

.field public static final enum INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType; = null

.field public static final INBOUND_CONNECTED_VALUE:I = 0x0

.field public static final enum INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType; = null

.field public static final INBOUND_MISSED_VALUE:I = 0x1

.field public static final enum OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType; = null

.field public static final OUTBOUND_CONNECTED_VALUE:I = 0x2

.field public static final enum OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType; = null

.field public static final OUTBOUND_NOT_ANSWERED_VALUE:I = 0x3

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40241
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    const-string v1, "INBOUND_CONNECTED"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40242
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    const-string v1, "INBOUND_MISSED"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40243
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    const-string v1, "OUTBOUND_CONNECTED"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40244
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    const-string v1, "OUTBOUND_NOT_ANSWERED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40239
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40270
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 40279
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40280
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->value:I

    .line 40281
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40267
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 1
    .parameter

    .prologue
    .line 40256
    packed-switch p0, :pswitch_data_0

    .line 40261
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 40257
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    goto :goto_0

    .line 40258
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    goto :goto_0

    .line 40259
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    goto :goto_0

    .line 40260
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    goto :goto_0

    .line 40256
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 1
    .parameter

    .prologue
    .line 40239
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 1

    .prologue
    .line 40239
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 40253
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->value:I

    return v0
.end method
