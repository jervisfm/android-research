.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallEntry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    }
.end annotation


# static fields
.field public static final ACCOUNT_ID_FIELD_NUMBER:I = 0x1

.field public static final CALL_ID_FIELD_NUMBER:I = 0xc

.field public static final CALL_TYPE_FIELD_NUMBER:I = 0x5

.field public static final DEVICE_CONTACT_ID_FIELD_NUMBER:I = 0x8

.field public static final DISPLAY_NAME_FIELD_NUMBER:I = 0x2

.field public static final DURATION_FIELD_NUMBER:I = 0x7

.field public static final EMAIL_FIELD_NUMBER:I = 0x4

.field public static final FIRST_NAME_FIELD_NUMBER:I = 0x9

.field public static final LAST_NAME_FIELD_NUMBER:I = 0xa

.field public static final MIDDLE_NAME_FIELD_NUMBER:I = 0xe

.field public static final NAME_PREFIX_FIELD_NUMBER:I = 0xd

.field public static final NAME_SUFFIX_FIELD_NUMBER:I = 0xf

.field public static final PEER_ID_FIELD_NUMBER:I = 0xb

.field public static final PHONE_NUMBER_FIELD_NUMBER:I = 0x3

.field public static final START_TIME_FIELD_NUMBER:I = 0x6

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;


# instance fields
.field private accountId_:Ljava/lang/Object;

.field private bitField0_:I

.field private callId_:Ljava/lang/Object;

.field private callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

.field private deviceContactId_:J

.field private displayName_:Ljava/lang/Object;

.field private duration_:I

.field private email_:Ljava/lang/Object;

.field private firstName_:Ljava/lang/Object;

.field private lastName_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private middleName_:Ljava/lang/Object;

.field private namePrefix_:Ljava/lang/Object;

.field private nameSuffix_:Ljava/lang/Object;

.field private peerId_:Ljava/lang/Object;

.field private phoneNumber_:Ljava/lang/Object;

.field private startTime_:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41691
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    .line 41692
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->initFields()V

    .line 41693
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 40226
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 40696
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedIsInitialized:B

    .line 40755
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedSerializedSize:I

    .line 40227
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 40228
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 40696
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedIsInitialized:B

    .line 40755
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedSerializedSize:I

    .line 40228
    return-void
.end method

.method static synthetic access$52002(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$52102(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$52202(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$52302(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$52402(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    return-object p1
.end method

.method static synthetic access$52502(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J

    return-wide p1
.end method

.method static synthetic access$52602(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I

    return p1
.end method

.method static synthetic access$52702(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J

    return-wide p1
.end method

.method static synthetic access$52802(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$52902(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53002(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53102(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53202(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53302(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53402(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$53502(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40221
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    return p1
.end method

.method private getAccountIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40308
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    .line 40309
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40310
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40312
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    .line 40315
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCallIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40572
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    .line 40573
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40574
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40576
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    .line 40579
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1

    .prologue
    .line 40232
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method private getDisplayNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40340
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    .line 40341
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40342
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40344
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    .line 40347
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getEmailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40404
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    .line 40405
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40406
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40408
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    .line 40411
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40476
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    .line 40477
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40478
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40480
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    .line 40483
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40508
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    .line 40509
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40510
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40512
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    .line 40515
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddleNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40636
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    .line 40637
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40638
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40640
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    .line 40643
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamePrefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40604
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    .line 40605
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40606
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40608
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    .line 40611
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameSuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40668
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    .line 40669
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40670
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40672
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    .line 40675
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPeerIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40540
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    .line 40541
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40542
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40544
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    .line 40547
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPhoneNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 40372
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    .line 40373
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40374
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 40376
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    .line 40379
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const-wide/16 v1, 0x0

    .line 40680
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    .line 40681
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    .line 40682
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    .line 40683
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    .line 40684
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    .line 40685
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J

    .line 40686
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I

    .line 40687
    iput-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J

    .line 40688
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    .line 40689
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    .line 40690
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    .line 40691
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    .line 40692
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    .line 40693
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    .line 40694
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    .line 40695
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 40897
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51800()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1
    .parameter

    .prologue
    .line 40900
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40866
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    .line 40867
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40868
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    .line 40870
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40877
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    .line 40878
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40879
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    .line 40881
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40833
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40839
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40887
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40855
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40861
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40844
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 40850
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->access$51700(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40294
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    .line 40295
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40296
    check-cast v0, Ljava/lang/String;

    .line 40304
    :goto_0
    return-object v0

    .line 40298
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40300
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40301
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40302
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->accountId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40304
    goto :goto_0
.end method

.method public getCallId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40558
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    .line 40559
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40560
    check-cast v0, Ljava/lang/String;

    .line 40568
    :goto_0
    return-object v0

    .line 40562
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40564
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40565
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40566
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40568
    goto :goto_0
.end method

.method public getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
    .locals 1

    .prologue
    .line 40422
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 40221
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1

    .prologue
    .line 40236
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 40452
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J

    return-wide v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40326
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    .line 40327
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40328
    check-cast v0, Ljava/lang/String;

    .line 40336
    :goto_0
    return-object v0

    .line 40330
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40332
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40333
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40334
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->displayName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40336
    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 40442
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I

    return v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40390
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    .line 40391
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40392
    check-cast v0, Ljava/lang/String;

    .line 40400
    :goto_0
    return-object v0

    .line 40394
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40396
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40397
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40398
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->email_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40400
    goto :goto_0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40462
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    .line 40463
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40464
    check-cast v0, Ljava/lang/String;

    .line 40472
    :goto_0
    return-object v0

    .line 40466
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40468
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40469
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40470
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->firstName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40472
    goto :goto_0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40494
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    .line 40495
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40496
    check-cast v0, Ljava/lang/String;

    .line 40504
    :goto_0
    return-object v0

    .line 40498
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40500
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40501
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40502
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->lastName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40504
    goto :goto_0
.end method

.method public getMiddleName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40622
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    .line 40623
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40624
    check-cast v0, Ljava/lang/String;

    .line 40632
    :goto_0
    return-object v0

    .line 40626
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40628
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40629
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40630
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->middleName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40632
    goto :goto_0
.end method

.method public getNamePrefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40590
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    .line 40591
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40592
    check-cast v0, Ljava/lang/String;

    .line 40600
    :goto_0
    return-object v0

    .line 40594
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40596
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40597
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40598
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->namePrefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40600
    goto :goto_0
.end method

.method public getNameSuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40654
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    .line 40655
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40656
    check-cast v0, Ljava/lang/String;

    .line 40664
    :goto_0
    return-object v0

    .line 40658
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40660
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40661
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40662
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->nameSuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40664
    goto :goto_0
.end method

.method public getPeerId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40526
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    .line 40527
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40528
    check-cast v0, Ljava/lang/String;

    .line 40536
    :goto_0
    return-object v0

    .line 40530
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40532
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40533
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40534
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->peerId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40536
    goto :goto_0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40358
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    .line 40359
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 40360
    check-cast v0, Ljava/lang/String;

    .line 40368
    :goto_0
    return-object v0

    .line 40362
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 40364
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 40365
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 40366
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->phoneNumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 40368
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 40757
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedSerializedSize:I

    .line 40758
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 40822
    :goto_0
    return v0

    .line 40760
    :cond_0
    const/4 v0, 0x0

    .line 40761
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 40762
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40765
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 40766
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40769
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 40770
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40773
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 40774
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40777
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 40778
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 40781
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 40782
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 40785
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 40786
    const/4 v1, 0x7

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 40789
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 40790
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J

    invoke-static {v5, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 40793
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 40794
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getFirstNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40797
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 40798
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getLastNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40801
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 40802
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPeerIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40805
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 40806
    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40809
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    .line 40810
    const/16 v1, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNamePrefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40813
    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    .line 40814
    const/16 v1, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getMiddleNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40817
    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_f

    .line 40818
    const/16 v1, 0xf

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNameSuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 40821
    :cond_f
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getStartTime()J
    .locals 2

    .prologue
    .line 40432
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J

    return-wide v0
.end method

.method public hasAccountId()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 40291
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallId()Z
    .locals 2

    .prologue
    .line 40555
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallType()Z
    .locals 2

    .prologue
    .line 40419
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 40449
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayName()Z
    .locals 2

    .prologue
    .line 40323
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    .prologue
    .line 40439
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 40387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstName()Z
    .locals 2

    .prologue
    .line 40459
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastName()Z
    .locals 2

    .prologue
    .line 40491
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddleName()Z
    .locals 2

    .prologue
    .line 40619
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamePrefix()Z
    .locals 2

    .prologue
    .line 40587
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameSuffix()Z
    .locals 2

    .prologue
    .line 40651
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerId()Z
    .locals 2

    .prologue
    .line 40523
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 40355
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasStartTime()Z
    .locals 2

    .prologue
    .line 40429
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 40698
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedIsInitialized:B

    .line 40699
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 40702
    :goto_0
    return v0

    .line 40699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 40701
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->memoizedIsInitialized:B

    move v0, v2

    .line 40702
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 40221
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 40898
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 40221
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;
    .locals 1

    .prologue
    .line 40902
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 40827
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 40707
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getSerializedSize()I

    .line 40708
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 40709
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40711
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 40712
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDisplayNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40714
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 40715
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPhoneNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40717
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 40718
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40720
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 40721
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->callType_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 40723
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 40724
    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->startTime_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 40726
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 40727
    const/4 v0, 0x7

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->duration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 40729
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 40730
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->deviceContactId_:J

    invoke-virtual {p1, v4, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 40732
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 40733
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getFirstNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40735
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 40736
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getLastNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40738
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 40739
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getPeerIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40741
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 40742
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getCallIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40744
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 40745
    const/16 v0, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNamePrefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40747
    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 40748
    const/16 v0, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getMiddleNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40750
    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    .line 40751
    const/16 v0, 0xf

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getNameSuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 40753
    :cond_e
    return-void
.end method
