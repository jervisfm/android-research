.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntryListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallEntryListOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 43093
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 43196
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43094
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->maybeForceBuilderInitialization()V

    .line 43095
    return-void
.end method

.method static synthetic access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43088
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$55200()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43128
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    .line 43129
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43130
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 43133
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43100
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntriesIsMutable()V
    .locals 2

    .prologue
    .line 43199
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 43200
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    .line 43203
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 43098
    return-void
.end method


# virtual methods
.method public addAllEntries(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;"
        }
    .end annotation

    .prologue
    .line 43266
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43267
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 43269
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 43259
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43260
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 43262
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 43242
    if-nez p2, :cond_0

    .line 43243
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43245
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43246
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 43248
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 43252
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43253
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43255
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43232
    if-nez p1, :cond_0

    .line 43233
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43235
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43236
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 43238
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 2

    .prologue
    .line 43119
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    .line 43120
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 43121
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 43123
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 3

    .prologue
    .line 43137
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 43138
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    .line 43139
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 43140
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43141
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    .line 43143
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->access$55402(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;Ljava/util/List;)Ljava/util/List;

    .line 43144
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43104
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 43105
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43106
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    .line 43107
    return-object p0
.end method

.method public clearEntries()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43272
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43273
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    .line 43275
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 2

    .prologue
    .line 43111
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 43088
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1

    .prologue
    .line 43115
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 43212
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 43209
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43206
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 43163
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43088
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43088
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43088
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43171
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 43172
    sparse-switch v0, :sswitch_data_0

    .line 43177
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 43179
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 43175
    goto :goto_1

    .line 43184
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    .line 43185
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 43186
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    goto :goto_0

    .line 43172
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 43148
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 43159
    :goto_0
    return-object v0

    .line 43149
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->access$55400(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 43150
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43151
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->access$55400(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    .line 43152
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 43159
    goto :goto_0

    .line 43154
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43155
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->access$55400(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43278
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43279
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 43281
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 43226
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43227
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 43229
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 43216
    if-nez p2, :cond_0

    .line 43217
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 43219
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->ensureEntriesIsMutable()V

    .line 43220
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 43222
    return-object p0
.end method
