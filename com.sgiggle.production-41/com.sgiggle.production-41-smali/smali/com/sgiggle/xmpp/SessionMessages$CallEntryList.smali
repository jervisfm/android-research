.class public final Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallEntryListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallEntryList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    }
.end annotation


# static fields
.field public static final ENTRIES_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;


# instance fields
.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43288
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    .line 43289
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->initFields()V

    .line 43290
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 42941
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 42978
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedIsInitialized:B

    .line 42995
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedSerializedSize:I

    .line 42942
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42936
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 42943
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42978
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedIsInitialized:B

    .line 42995
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedSerializedSize:I

    .line 42943
    return-void
.end method

.method static synthetic access$55400(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 42936
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$55402(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42936
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1

    .prologue
    .line 42947
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 42976
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    .line 42977
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43081
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55200()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 43084
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43050
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    .line 43051
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43052
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    .line 43054
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43061
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    .line 43062
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43063
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    .line 43065
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43017
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43023
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43071
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43077
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43039
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43045
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43028
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 43034
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;->access$55100(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42936
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;
    .locals 1

    .prologue
    .line 42951
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;

    return-object v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 42968
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 42965
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42958
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 42972
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;

    return-object v0
.end method

.method public getEntriesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42962
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 42997
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedSerializedSize:I

    .line 42998
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 43006
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 43001
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 43002
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 43001
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 43005
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedSerializedSize:I

    move v0, v2

    .line 43006
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 42980
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedIsInitialized:B

    .line 42981
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 42984
    :goto_0
    return v0

    .line 42981
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 42983
    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->memoizedIsInitialized:B

    move v0, v2

    .line 42984
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42936
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43082
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42936
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;
    .locals 1

    .prologue
    .line 43086
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;)Lcom/sgiggle/xmpp/SessionMessages$CallEntryList$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 43011
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->getSerializedSize()I

    .line 42990
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 42991
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryList;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 42990
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42993
    :cond_0
    return-void
.end method
