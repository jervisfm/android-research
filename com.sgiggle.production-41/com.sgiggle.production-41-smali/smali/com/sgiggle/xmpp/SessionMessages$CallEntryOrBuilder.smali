.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallEntryOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountId()Ljava/lang/String;
.end method

.method public abstract getCallId()Ljava/lang/String;
.end method

.method public abstract getCallType()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$CallType;
.end method

.method public abstract getDeviceContactId()J
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getEmail()Ljava/lang/String;
.end method

.method public abstract getFirstName()Ljava/lang/String;
.end method

.method public abstract getLastName()Ljava/lang/String;
.end method

.method public abstract getMiddleName()Ljava/lang/String;
.end method

.method public abstract getNamePrefix()Ljava/lang/String;
.end method

.method public abstract getNameSuffix()Ljava/lang/String;
.end method

.method public abstract getPeerId()Ljava/lang/String;
.end method

.method public abstract getPhoneNumber()Ljava/lang/String;
.end method

.method public abstract getStartTime()J
.end method

.method public abstract hasAccountId()Z
.end method

.method public abstract hasCallId()Z
.end method

.method public abstract hasCallType()Z
.end method

.method public abstract hasDeviceContactId()Z
.end method

.method public abstract hasDisplayName()Z
.end method

.method public abstract hasDuration()Z
.end method

.method public abstract hasEmail()Z
.end method

.method public abstract hasFirstName()Z
.end method

.method public abstract hasLastName()Z
.end method

.method public abstract hasMiddleName()Z
.end method

.method public abstract hasNamePrefix()Z
.end method

.method public abstract hasNameSuffix()Z
.end method

.method public abstract hasPeerId()Z
.end method

.method public abstract hasPhoneNumber()Z
.end method

.method public abstract hasStartTime()Z
.end method
