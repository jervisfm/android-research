.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AgreeAnswerOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions; = null

.field public static final enum AGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions; = null

.field public static final AGREE_VALUE:I = 0x2

.field public static final enum DISAGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions; = null

.field public static final DISAGREE_VALUE:I = 0x1

.field public static final enum NOT_SURE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions; = null

.field public static final NOT_SURE_VALUE:I = 0x3

.field public static final enum UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field public static final UNANSWERED_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v1, "UNANSWERED"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v1, "DISAGREE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v1, "AGREE"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v1, "NOT_SURE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->value:I

    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;",
            ">;"
        }
    .end annotation

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->DISAGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->AGREE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->NOT_SURE:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->value:I

    return v0
.end method
