.class public final Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private comments_:Ljava/lang/Object;

.field private delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private overallRating_:I

.field private pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$104100()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104302(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104402(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;I)I

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104502(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104602(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104702(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104802(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$104902(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    or-int/lit16 v2, v2, 0x80

    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105002(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x100

    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105102(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    or-int/lit16 v2, v2, 0x200

    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105202(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x400

    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105302(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_b

    or-int/lit16 v1, v2, 0x800

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105402(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Ljava/lang/Object;)Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->access$105502(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;I)I

    return-object v0

    :cond_b
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBadLipsync()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearComments()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getComments()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearDelay()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearDistortedSpeech()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearHeardEcho()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearHeardNoise()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearOverallRating()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    return-object p0
.end method

.method public clearPictureBlurry()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearPictureFreezing()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearPictureRotated()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public clearUnnaturalMovement()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBadLipsync()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public getDelay()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getDistortedSpeech()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getHeardEcho()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getHeardNoise()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getOverallRating()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    return v0
.end method

.method public getPictureBlurry()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getPictureFreezing()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getPictureRotated()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getUnnaturalMovement()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public hasBadLipsync()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasComments()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDelay()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDistortedSpeech()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeardEcho()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeardNoise()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOverallRating()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureBlurry()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureFreezing()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureRotated()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnnaturalMovement()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasOverallRating()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasHeardEcho()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasHeardNoise()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasDelay()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasDistortedSpeech()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasPictureFreezing()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasPictureBlurry()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasPictureRotated()Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasUnnaturalMovement()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasBadLipsync()Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasComments()Z

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x200

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    goto/16 :goto_0

    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasOverallRating()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getOverallRating()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setOverallRating(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasHeardEcho()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getHeardEcho()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setHeardEcho(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasHeardNoise()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getHeardNoise()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setHeardNoise(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasDelay()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDelay()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setDelay(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasDistortedSpeech()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDistortedSpeech()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setDistortedSpeech(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureFreezing()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getPictureFreezing()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureFreezing(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureBlurry()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getPictureBlurry()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureBlurry(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureRotated()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getPictureRotated()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setPictureRotated(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasUnnaturalMovement()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getUnnaturalMovement()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setUnnaturalMovement(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasBadLipsync()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getBadLipsync()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setBadLipsync(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasComments()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getComments()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->setComments(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    :cond_c
    move-object v0, p0

    goto/16 :goto_0
.end method

.method public setBadLipsync(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setComments(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    return-object p0
.end method

.method setComments(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->comments_:Ljava/lang/Object;

    return-void
.end method

.method public setDelay(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setDistortedSpeech(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setHeardEcho(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setHeardNoise(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setOverallRating(I)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->overallRating_:I

    return-object p0
.end method

.method public setPictureBlurry(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setPictureFreezing(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setPictureRotated(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method

.method public setUnnaturalMovement(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p0
.end method
