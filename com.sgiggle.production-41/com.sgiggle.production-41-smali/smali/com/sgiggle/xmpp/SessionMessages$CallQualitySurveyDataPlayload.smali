.class public final Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CallQualitySurveyDataPlayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    }
.end annotation


# static fields
.field public static final BADLIPSYNC_FIELD_NUMBER:I = 0xb

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final COMMENTS_FIELD_NUMBER:I = 0xc

.field public static final DELAY_FIELD_NUMBER:I = 0x5

.field public static final DISTORTEDSPEECH_FIELD_NUMBER:I = 0x6

.field public static final HEARDECHO_FIELD_NUMBER:I = 0x3

.field public static final HEARDNOISE_FIELD_NUMBER:I = 0x4

.field public static final OVERALLRATING_FIELD_NUMBER:I = 0x2

.field public static final PICTUREBLURRY_FIELD_NUMBER:I = 0x8

.field public static final PICTUREFREEZING_FIELD_NUMBER:I = 0x7

.field public static final PICTUREROTATED_FIELD_NUMBER:I = 0x9

.field public static final UNNATURALMOVEMENT_FIELD_NUMBER:I = 0xa

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;


# instance fields
.field private badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private comments_:Ljava/lang/Object;

.field private delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private overallRating_:I

.field private pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

.field private unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$104302(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$104402(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I

    return p1
.end method

.method static synthetic access$104502(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$104602(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$104702(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$104802(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$104902(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$105002(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$105102(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$105202(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$105302(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object p1
.end method

.method static synthetic access$105402(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$105502(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    return p1
.end method

.method private getCommentsBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->UNANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104100()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;->access$104000(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBadLipsync()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getComments()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->comments_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;

    return-object v0
.end method

.method public getDelay()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getDistortedSpeech()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getHeardEcho()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getHeardNoise()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getOverallRating()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I

    return v0
.end method

.method public getPictureBlurry()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getPictureFreezing()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getPictureRotated()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getCommentsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getUnnaturalMovement()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    return-object v0
.end method

.method public hasBadLipsync()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasComments()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDelay()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDistortedSpeech()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeardEcho()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeardNoise()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOverallRating()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureBlurry()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureFreezing()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPictureRotated()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnnaturalMovement()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasOverallRating()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasHeardEcho()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasHeardNoise()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasDelay()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasDistortedSpeech()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureFreezing()Z

    move-result v0

    if-nez v0, :cond_8

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureBlurry()Z

    move-result v0

    if-nez v0, :cond_9

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasPictureRotated()Z

    move-result v0

    if-nez v0, :cond_a

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasUnnaturalMovement()Z

    move-result v0

    if-nez v0, :cond_b

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasBadLipsync()Z

    move-result v0

    if-nez v0, :cond_c

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->hasComments()Z

    move-result v0

    if-nez v0, :cond_d

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_e

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v2

    goto/16 :goto_0

    :cond_e
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->memoizedIsInitialized:B

    move v0, v3

    goto/16 :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;)Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->overallRating_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardEcho_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->heardNoise_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->delay_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->distortedSpeech_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureFreezing_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureBlurry_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->pictureRotated_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->unnaturalMovement_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->badLipsync_:Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload;->getCommentsBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_b
    return-void
.end method
