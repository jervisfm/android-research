.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallQualitySurveyDataPlayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBadLipsync()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getComments()Ljava/lang/String;
.end method

.method public abstract getDelay()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getDistortedSpeech()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getHeardEcho()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getHeardNoise()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getOverallRating()I
.end method

.method public abstract getPictureBlurry()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getPictureFreezing()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getPictureRotated()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract getUnnaturalMovement()Lcom/sgiggle/xmpp/SessionMessages$CallQualitySurveyDataPlayload$AgreeAnswerOptions;
.end method

.method public abstract hasBadLipsync()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasComments()Z
.end method

.method public abstract hasDelay()Z
.end method

.method public abstract hasDistortedSpeech()Z
.end method

.method public abstract hasHeardEcho()Z
.end method

.method public abstract hasHeardNoise()Z
.end method

.method public abstract hasOverallRating()Z
.end method

.method public abstract hasPictureBlurry()Z
.end method

.method public abstract hasPictureFreezing()Z
.end method

.method public abstract hasPictureRotated()Z
.end method

.method public abstract hasUnnaturalMovement()Z
.end method
