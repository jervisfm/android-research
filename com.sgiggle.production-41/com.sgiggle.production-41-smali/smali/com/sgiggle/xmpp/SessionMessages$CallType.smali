.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CallType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CallType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CallType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final enum CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final CALL_TYPE_INBOUND_CONNECTED_VALUE:I = 0x0

.field public static final enum CALL_TYPE_INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final CALL_TYPE_INBOUND_MISSED_VALUE:I = 0x1

.field public static final enum CALL_TYPE_OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final CALL_TYPE_OUTBOUND_CONNECTED_VALUE:I = 0x2

.field public static final enum CALL_TYPE_OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final CALL_TYPE_OUTBOUND_NOT_ANSWERED_VALUE:I = 0x3

.field public static final enum CALL_TYPE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$CallType; = null

.field public static final CALL_TYPE_UNKNOWN_VALUE:I = 0x4

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 633
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    const-string v1, "CALL_TYPE_INBOUND_CONNECTED"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 634
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    const-string v1, "CALL_TYPE_INBOUND_MISSED"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 635
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    const-string v1, "CALL_TYPE_OUTBOUND_CONNECTED"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 636
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    const-string v1, "CALL_TYPE_OUTBOUND_NOT_ANSWERED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 637
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    const-string v1, "CALL_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$CallType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 631
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CallType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallType;

    .line 665
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CallType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 674
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 675
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->value:I

    .line 676
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 662
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 1
    .parameter

    .prologue
    .line 650
    packed-switch p0, :pswitch_data_0

    .line 656
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 651
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 652
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_INBOUND_MISSED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 653
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_CONNECTED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 654
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_OUTBOUND_NOT_ANSWERED:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 655
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->CALL_TYPE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$CallType;

    goto :goto_0

    .line 650
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 1
    .parameter

    .prologue
    .line 631
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CallType;
    .locals 1

    .prologue
    .line 631
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CallType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CallType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CallType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 647
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CallType;->value:I

    return v0
.end method
