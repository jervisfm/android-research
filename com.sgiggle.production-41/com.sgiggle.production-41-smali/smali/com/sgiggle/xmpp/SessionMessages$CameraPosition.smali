.class public final enum Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CameraPosition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CameraPosition; = null

.field public static final enum CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition; = null

.field public static final CAM_BACK_VALUE:I = 0x1

.field public static final enum CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition; = null

.field public static final CAM_FRONT_VALUE:I = 0x2

.field public static final enum CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

.field public static final CAM_NONE_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    const-string v1, "CAM_NONE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 67
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    const-string v1, "CAM_BACK"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 68
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    const-string v1, "CAM_FRONT"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    .line 92
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->value:I

    .line 103
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1
    .parameter

    .prologue
    .line 79
    packed-switch p0, :pswitch_data_0

    .line 83
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 80
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_NONE:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto :goto_0

    .line 81
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_BACK:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto :goto_0

    .line 82
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->CAM_FRONT:Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1
    .parameter

    .prologue
    .line 64
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CameraPosition;->value:I

    return v0
.end method
