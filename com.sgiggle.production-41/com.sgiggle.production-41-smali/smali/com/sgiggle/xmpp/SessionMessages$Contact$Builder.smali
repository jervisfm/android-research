.class public final Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
        "Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private deviceContactId_:J

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private favorite_:Z

.field private firstname_:Ljava/lang/Object;

.field private hasPicture_:Z

.field private isSystemAccount_:Z

.field private lastname_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private nativeFavorite_:Z

.field private phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22430
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 22718
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22754
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22790
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22826
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22869
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22989
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 23025
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23061
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23097
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 22431
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->maybeForceBuilderInitialization()V

    .line 22432
    return-void
.end method

.method static synthetic access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22425
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$27900()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22491
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    .line 22492
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 22493
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 22496
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22437
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 22435
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 2

    .prologue
    .line 22482
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    .line 22483
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 22484
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 22486
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 5

    .prologue
    .line 22500
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 22501
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22502
    const/4 v2, 0x0

    .line 22503
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 22504
    or-int/lit8 v2, v2, 0x1

    .line 22506
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28102(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22507
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 22508
    or-int/lit8 v2, v2, 0x2

    .line 22510
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28202(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22511
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 22512
    or-int/lit8 v2, v2, 0x4

    .line 22514
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28302(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22515
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 22516
    or-int/lit8 v2, v2, 0x8

    .line 22518
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28402(Lcom/sgiggle/xmpp/SessionMessages$Contact;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22519
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 22520
    or-int/lit8 v2, v2, 0x10

    .line 22522
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28502(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22523
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 22524
    or-int/lit8 v2, v2, 0x20

    .line 22526
    :cond_5
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28602(Lcom/sgiggle/xmpp/SessionMessages$Contact;J)J

    .line 22527
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 22528
    or-int/lit8 v2, v2, 0x40

    .line 22530
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28702(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z

    .line 22531
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 22532
    or-int/lit16 v2, v2, 0x80

    .line 22534
    :cond_7
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28802(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z

    .line 22535
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 22536
    or-int/lit16 v2, v2, 0x100

    .line 22538
    :cond_8
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$28902(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z

    .line 22539
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 22540
    or-int/lit16 v2, v2, 0x200

    .line 22542
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29002(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22543
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 22544
    or-int/lit16 v2, v2, 0x400

    .line 22546
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29102(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22547
    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    .line 22548
    or-int/lit16 v2, v2, 0x800

    .line 22550
    :cond_b
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29202(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22551
    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    .line 22552
    or-int/lit16 v2, v2, 0x1000

    .line 22554
    :cond_c
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29302(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22555
    and-int/lit16 v1, v1, 0x2000

    const/16 v3, 0x2000

    if-ne v1, v3, :cond_d

    .line 22556
    or-int/lit16 v1, v2, 0x2000

    .line 22558
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29402(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z

    .line 22559
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->access$29502(Lcom/sgiggle/xmpp/SessionMessages$Contact;I)I

    .line 22560
    return-object v0

    :cond_d
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22441
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 22442
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22443
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22444
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22445
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22446
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22447
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22448
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22449
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22450
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22452
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    .line 22453
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22454
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    .line 22455
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22456
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    .line 22457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22458
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    .line 22459
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22460
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 22461
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22462
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 22463
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22464
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 22465
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22466
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 22467
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22468
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    .line 22469
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22470
    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22814
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22815
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22817
    return-object p0
.end method

.method public clearDeviceContactId()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 2

    .prologue
    .line 22919
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22920
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    .line 22922
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 23013
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23014
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 23016
    return-object p0
.end method

.method public clearEmail()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22893
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22894
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22896
    return-object p0
.end method

.method public clearFavorite()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22940
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22941
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    .line 22943
    return-object p0
.end method

.method public clearFirstname()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22743
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22745
    return-object p0
.end method

.method public clearHasPicture()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 23147
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    .line 23150
    return-object p0
.end method

.method public clearIsSystemAccount()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22961
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22962
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    .line 22964
    return-object p0
.end method

.method public clearLastname()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22778
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22779
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22781
    return-object p0
.end method

.method public clearMiddlename()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 23085
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23088
    return-object p0
.end method

.method public clearNameprefix()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 23049
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23050
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23052
    return-object p0
.end method

.method public clearNamesuffix()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 23121
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23122
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 23124
    return-object p0
.end method

.method public clearNativeFavorite()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22982
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22983
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    .line 22985
    return-object p0
.end method

.method public clearPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22862
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22865
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 2

    .prologue
    .line 22474
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22795
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22796
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 22797
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 22798
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22801
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 22425
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 22478
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 22910
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    return-wide v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22994
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 22995
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 22996
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 22997
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 23000
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22874
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22875
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 22876
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 22877
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22880
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getFavorite()Z
    .locals 1

    .prologue
    .line 22931
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    return v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22723
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22724
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 22725
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 22726
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22729
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getHasPicture()Z
    .locals 1

    .prologue
    .line 23138
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    return v0
.end method

.method public getIsSystemAccount()Z
    .locals 1

    .prologue
    .line 22952
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    return v0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22759
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22760
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 22761
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 22762
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22765
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23066
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23067
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 23068
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 23069
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23072
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23030
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23031
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 23032
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 23033
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23036
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 23102
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 23103
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 23104
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 23105
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 23108
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNativeFavorite()Z
    .locals 1

    .prologue
    .line 22973
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    return v0
.end method

.method public getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 22831
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 22792
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 22907
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 22991
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 22871
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFavorite()Z
    .locals 2

    .prologue
    .line 22928
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 22720
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHasPicture()Z
    .locals 2

    .prologue
    .line 23135
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsSystemAccount()Z
    .locals 2

    .prologue
    .line 22949
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 22756
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 23063
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 23027
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 23099
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeFavorite()Z
    .locals 2

    .prologue
    .line 22970
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 22828
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 22611
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22612
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_0

    .line 22614
    const/4 v0, 0x0

    .line 22617
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22425
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22425
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22425
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22625
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 22626
    sparse-switch v0, :sswitch_data_0

    .line 22631
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 22633
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 22629
    goto :goto_1

    .line 22638
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22639
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    goto :goto_0

    .line 22643
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22644
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    goto :goto_0

    .line 22648
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22649
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    goto :goto_0

    .line 22653
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    .line 22654
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPhoneNumber()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 22655
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    .line 22657
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 22658
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    goto :goto_0

    .line 22662
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22663
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    goto :goto_0

    .line 22667
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22668
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    goto :goto_0

    .line 22672
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22673
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    goto :goto_0

    .line 22677
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22678
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    goto/16 :goto_0

    .line 22682
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22683
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    goto/16 :goto_0

    .line 22687
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22688
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 22692
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22693
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 22697
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22698
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 22702
    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22703
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 22707
    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22708
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    goto/16 :goto_0

    .line 22626
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 2
    .parameter

    .prologue
    .line 22564
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 22607
    :goto_0
    return-object v0

    .line 22565
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasFirstname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22566
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22568
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasLastname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 22569
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22571
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasAccountid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 22572
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22574
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 22575
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergePhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22577
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 22578
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22580
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasDeviceContactId()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 22581
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDeviceContactId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22583
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasFavorite()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 22584
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFavorite()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22586
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasIsSystemAccount()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 22587
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getIsSystemAccount()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setIsSystemAccount(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22589
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasNativeFavorite()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 22590
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNativeFavorite()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNativeFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22592
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 22593
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22595
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasNameprefix()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 22596
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22598
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasMiddlename()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 22599
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22601
    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasNamesuffix()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 22602
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 22604
    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasHasPicture()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 22605
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getHasPicture()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->setHasPicture(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_e
    move-object v0, p0

    .line 22607
    goto/16 :goto_0
.end method

.method public mergePhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 2
    .parameter

    .prologue
    .line 22850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 22852
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22858
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22859
    return-object p0

    .line 22855
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    goto :goto_0
.end method

.method public setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22805
    if-nez p1, :cond_0

    .line 22806
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22808
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22809
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22811
    return-object p0
.end method

.method setAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 22820
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22821
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->accountid_:Ljava/lang/Object;

    .line 22823
    return-void
.end method

.method public setDeviceContactId(J)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22913
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22914
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->deviceContactId_:J

    .line 22916
    return-object p0
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23004
    if-nez p1, :cond_0

    .line 23005
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23007
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23008
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 23010
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 23019
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23020
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->displayname_:Ljava/lang/Object;

    .line 23022
    return-void
.end method

.method public setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22884
    if-nez p1, :cond_0

    .line 22885
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22887
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22888
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22890
    return-object p0
.end method

.method setEmail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 22899
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22900
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->email_:Ljava/lang/Object;

    .line 22902
    return-void
.end method

.method public setFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22934
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22935
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->favorite_:Z

    .line 22937
    return-object p0
.end method

.method public setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22733
    if-nez p1, :cond_0

    .line 22734
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22736
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22737
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22739
    return-object p0
.end method

.method setFirstname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 22748
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22749
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->firstname_:Ljava/lang/Object;

    .line 22751
    return-void
.end method

.method public setHasPicture(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23141
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23142
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->hasPicture_:Z

    .line 23144
    return-object p0
.end method

.method public setIsSystemAccount(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22955
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22956
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->isSystemAccount_:Z

    .line 22958
    return-object p0
.end method

.method public setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22769
    if-nez p1, :cond_0

    .line 22770
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22772
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22773
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22775
    return-object p0
.end method

.method setLastname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 22784
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22785
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->lastname_:Ljava/lang/Object;

    .line 22787
    return-void
.end method

.method public setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23076
    if-nez p1, :cond_0

    .line 23077
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23079
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23080
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23082
    return-object p0
.end method

.method setMiddlename(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 23091
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23092
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->middlename_:Ljava/lang/Object;

    .line 23094
    return-void
.end method

.method public setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23040
    if-nez p1, :cond_0

    .line 23041
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23043
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23044
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23046
    return-object p0
.end method

.method setNameprefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 23055
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23056
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nameprefix_:Ljava/lang/Object;

    .line 23058
    return-void
.end method

.method public setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 23112
    if-nez p1, :cond_0

    .line 23113
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 23115
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23116
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 23118
    return-object p0
.end method

.method setNamesuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 23127
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 23128
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->namesuffix_:Ljava/lang/Object;

    .line 23130
    return-void
.end method

.method public setNativeFavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22976
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22977
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->nativeFavorite_:Z

    .line 22979
    return-object p0
.end method

.method public setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22844
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22846
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22847
    return-object p0
.end method

.method public setPhoneNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22834
    if-nez p1, :cond_0

    .line 22835
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 22837
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22839
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->bitField0_:I

    .line 22840
    return-object p0
.end method
