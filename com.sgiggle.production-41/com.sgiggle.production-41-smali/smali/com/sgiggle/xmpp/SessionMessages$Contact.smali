.class public final Lcom/sgiggle/xmpp/SessionMessages$Contact;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Contact"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x3

.field public static final DEVICECONTACTID_FIELD_NUMBER:I = 0x6

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0xa

.field public static final EMAIL_FIELD_NUMBER:I = 0x5

.field public static final FAVORITE_FIELD_NUMBER:I = 0x7

.field public static final FIRSTNAME_FIELD_NUMBER:I = 0x1

.field public static final HASPICTURE_FIELD_NUMBER:I = 0xe

.field public static final ISSYSTEMACCOUNT_FIELD_NUMBER:I = 0x8

.field public static final LASTNAME_FIELD_NUMBER:I = 0x2

.field public static final MIDDLENAME_FIELD_NUMBER:I = 0xc

.field public static final NAMEPREFIX_FIELD_NUMBER:I = 0xb

.field public static final NAMESUFFIX_FIELD_NUMBER:I = 0xd

.field public static final NATIVEFAVORITE_FIELD_NUMBER:I = 0x9

.field public static final PHONENUMBER_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Contact;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private deviceContactId_:J

.field private displayname_:Ljava/lang/Object;

.field private email_:Ljava/lang/Object;

.field private favorite_:Z

.field private firstname_:Ljava/lang/Object;

.field private hasPicture_:Z

.field private isSystemAccount_:Z

.field private lastname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private nativeFavorite_:Z

.field private phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23157
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 23158
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->initFields()V

    .line 23159
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 21872
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 22218
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedIsInitialized:B

    .line 22280
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedSerializedSize:I

    .line 21873
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;-><init>(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 21874
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 22218
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedIsInitialized:B

    .line 22280
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedSerializedSize:I

    .line 21874
    return-void
.end method

.method static synthetic access$28102(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$28202(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$28302(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$28402(Lcom/sgiggle/xmpp/SessionMessages$Contact;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object p1
.end method

.method static synthetic access$28502(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$28602(Lcom/sgiggle/xmpp/SessionMessages$Contact;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J

    return-wide p1
.end method

.method static synthetic access$28702(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z

    return p1
.end method

.method static synthetic access$28802(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z

    return p1
.end method

.method static synthetic access$28902(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z

    return p1
.end method

.method static synthetic access$29002(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$29102(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$29202(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$29302(Lcom/sgiggle/xmpp/SessionMessages$Contact;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$29402(Lcom/sgiggle/xmpp/SessionMessages$Contact;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z

    return p1
.end method

.method static synthetic access$29502(Lcom/sgiggle/xmpp/SessionMessages$Contact;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 21867
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 21971
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    .line 21972
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21973
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 21975
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    .line 21978
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 21878
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 22085
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    .line 22086
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22087
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 22089
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    .line 22092
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getEmailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 22013
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    .line 22014
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22015
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 22017
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    .line 22020
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 21907
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    .line 21908
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21909
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 21911
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    .line 21914
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 21939
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    .line 21940
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21941
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 21943
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    .line 21946
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddlenameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 22149
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    .line 22150
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22151
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 22153
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    .line 22156
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameprefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 22117
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    .line 22118
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22119
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 22121
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    .line 22124
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamesuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 22181
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    .line 22182
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22183
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 22185
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    .line 22188
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22203
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    .line 22204
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    .line 22205
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    .line 22206
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 22207
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    .line 22208
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J

    .line 22209
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z

    .line 22210
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z

    .line 22211
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z

    .line 22212
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    .line 22213
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    .line 22214
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    .line 22215
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    .line 22216
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z

    .line 22217
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22418
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27900()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1
    .parameter

    .prologue
    .line 22421
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22387
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 22388
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22389
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    .line 22391
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22398
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 22399
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 22400
    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    .line 22402
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22354
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22360
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22408
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22414
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22376
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22382
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22365
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 22371
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->access$27800(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21957
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    .line 21958
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21959
    check-cast v0, Ljava/lang/String;

    .line 21967
    :goto_0
    return-object v0

    .line 21961
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 21963
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 21964
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21965
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 21967
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21867
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 21882
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getDeviceContactId()J
    .locals 2

    .prologue
    .line 22031
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J

    return-wide v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22071
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    .line 22072
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22073
    check-cast v0, Ljava/lang/String;

    .line 22081
    :goto_0
    return-object v0

    .line 22075
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 22077
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 22078
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22079
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 22081
    goto :goto_0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21999
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    .line 22000
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22001
    check-cast v0, Ljava/lang/String;

    .line 22009
    :goto_0
    return-object v0

    .line 22003
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 22005
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 22006
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22007
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->email_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 22009
    goto :goto_0
.end method

.method public getFavorite()Z
    .locals 1

    .prologue
    .line 22041
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z

    return v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21893
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    .line 21894
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21895
    check-cast v0, Ljava/lang/String;

    .line 21903
    :goto_0
    return-object v0

    .line 21897
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 21899
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 21900
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21901
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->firstname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 21903
    goto :goto_0
.end method

.method public getHasPicture()Z
    .locals 1

    .prologue
    .line 22199
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z

    return v0
.end method

.method public getIsSystemAccount()Z
    .locals 1

    .prologue
    .line 22051
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z

    return v0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21925
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    .line 21926
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 21927
    check-cast v0, Ljava/lang/String;

    .line 21935
    :goto_0
    return-object v0

    .line 21929
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 21931
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 21932
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21933
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->lastname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 21935
    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22135
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    .line 22136
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22137
    check-cast v0, Ljava/lang/String;

    .line 22145
    :goto_0
    return-object v0

    .line 22139
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 22141
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 22142
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22143
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->middlename_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 22145
    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22103
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    .line 22104
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22105
    check-cast v0, Ljava/lang/String;

    .line 22113
    :goto_0
    return-object v0

    .line 22107
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 22109
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 22110
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22111
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nameprefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 22113
    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 22167
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    .line 22168
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 22169
    check-cast v0, Ljava/lang/String;

    .line 22177
    :goto_0
    return-object v0

    .line 22171
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 22173
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 22174
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 22175
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->namesuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 22177
    goto :goto_0
.end method

.method public getNativeFavorite()Z
    .locals 1

    .prologue
    .line 22061
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z

    return v0
.end method

.method public getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 21989
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 22282
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedSerializedSize:I

    .line 22283
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 22343
    :goto_0
    return v0

    .line 22285
    :cond_0
    const/4 v0, 0x0

    .line 22286
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 22287
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22290
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 22291
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22294
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 22295
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22298
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v5, :cond_4

    .line 22299
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22302
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 22303
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22306
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 22307
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22310
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 22311
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22314
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 22315
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22318
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 22319
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22322
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 22323
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22326
    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_b

    .line 22327
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22330
    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_c

    .line 22331
    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22334
    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_d

    .line 22335
    const/16 v1, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 22338
    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_e

    .line 22339
    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 22342
    :cond_e
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 21954
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeviceContactId()Z
    .locals 2

    .prologue
    .line 22028
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 22068
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 21996
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFavorite()Z
    .locals 2

    .prologue
    .line 22038
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21890
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHasPicture()Z
    .locals 2

    .prologue
    .line 22196
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsSystemAccount()Z
    .locals 2

    .prologue
    .line 22048
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 21922
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 22132
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 22100
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 22164
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNativeFavorite()Z
    .locals 2

    .prologue
    .line 22058
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPhoneNumber()Z
    .locals 2

    .prologue
    .line 21986
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 22220
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedIsInitialized:B

    .line 22221
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 22230
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 22221
    goto :goto_0

    .line 22223
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPhoneNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 22224
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getPhoneNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    .line 22225
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedIsInitialized:B

    move v0, v3

    .line 22226
    goto :goto_0

    .line 22229
    :cond_2
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->memoizedIsInitialized:B

    move v0, v2

    .line 22230
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21867
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22419
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21867
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;
    .locals 1

    .prologue
    .line 22423
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 22348
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 22235
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getSerializedSize()I

    .line 22236
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 22237
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22239
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 22240
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22242
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 22243
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22245
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v4, :cond_3

    .line 22246
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->phoneNumber_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 22248
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 22249
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22251
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    .line 22252
    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->deviceContactId_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 22254
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    .line 22255
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->favorite_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 22257
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_7

    .line 22258
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isSystemAccount_:Z

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 22260
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_8

    .line 22261
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->nativeFavorite_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 22263
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_9

    .line 22264
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22266
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_a

    .line 22267
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22269
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_b

    .line 22270
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22272
    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_c

    .line 22273
    const/16 v0, 0xd

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 22275
    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_d

    .line 22276
    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$Contact;->hasPicture_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 22278
    :cond_d
    return-void
.end method
