.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountValidated_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$112100()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112302(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112402(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112502(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112602(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    or-int/lit8 v1, v2, 0x10

    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112702(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Z)Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->access$112802(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;I)I

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAccountValidated()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearEntry()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountValidated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    return-object v0
.end method

.method public hasAccountValidated()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEntry()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->hasContact()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public mergeEntry(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->hasContact()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->hasEntry()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setEntry(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasEntry()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeEntry(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasAccountValidated()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getAccountValidated()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->setAccountValidated(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    :cond_5
    move-object v0, p0

    goto :goto_0
.end method

.method public setAccountValidated(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->accountValidated_:Z

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setEntry(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setEntry(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    return-object p0
.end method
