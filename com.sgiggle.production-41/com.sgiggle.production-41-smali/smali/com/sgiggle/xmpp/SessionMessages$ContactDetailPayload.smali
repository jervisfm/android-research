.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactDetailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
    }
.end annotation


# static fields
.field public static final ACCOUNT_VALIDATED_FIELD_NUMBER:I = 0x5

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACT_FIELD_NUMBER:I = 0x2

.field public static final ENTRY_FIELD_NUMBER:I = 0x3

.field public static final SOURCE_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;


# instance fields
.field private accountValidated_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$112302(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$112402(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$112502(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object p1
.end method

.method static synthetic access$112602(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    return-object p1
.end method

.method static synthetic access$112702(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z

    return p1
.end method

.method static synthetic access$112802(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112100()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;->access$112000(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountValidated()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;

    return-object v0
.end method

.method public getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    return-object v0
.end method

.method public hasAccountValidated()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEntry()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->hasContact()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->entry_:Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload;->accountValidated_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    return-void
.end method
