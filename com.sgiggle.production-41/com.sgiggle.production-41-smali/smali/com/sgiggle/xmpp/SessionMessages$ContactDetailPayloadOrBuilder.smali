.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactDetailPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountValidated()Z
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getEntry()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
.end method

.method public abstract getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactDetailPayload$Source;
.end method

.method public abstract hasAccountValidated()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasContact()Z
.end method

.method public abstract hasEntry()Z
.end method

.method public abstract hasSource()Z
.end method
