.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private devicecontactid_:J

.field private displayname_:Ljava/lang/Object;

.field private emailaddress_:Lcom/google/protobuf/LazyStringList;

.field private firstname_:Ljava/lang/Object;

.field private haspicture_:Z

.field private isnativefavorite_:Z

.field private lastname_:Ljava/lang/Object;

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private phonenumber_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46062
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 46352
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46388
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46424
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46460
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46516
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46668
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46704
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46740
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46776
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46063
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->maybeForceBuilderInitialization()V

    .line 46064
    return-void
.end method

.method static synthetic access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46057
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$58600()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46119
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    .line 46120
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46121
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 46124
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46069
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEmailaddressIsMutable()V
    .locals 2

    .prologue
    .line 46462
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 46463
    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46464
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46466
    :cond_0
    return-void
.end method

.method private ensurePhonenumberIsMutable()V
    .locals 2

    .prologue
    .line 46519
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 46520
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46521
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46523
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 46067
    return-void
.end method


# virtual methods
.method public addAllEmailaddress(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;"
        }
    .end annotation

    .prologue
    .line 46498
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46499
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 46501
    return-object p0
.end method

.method public addAllPhonenumber(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;"
        }
    .end annotation

    .prologue
    .line 46586
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46587
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 46589
    return-object p0
.end method

.method public addEmailaddress(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46488
    if-nez p1, :cond_0

    .line 46489
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46491
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46492
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    .line 46494
    return-object p0
.end method

.method addEmailaddress(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46510
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46511
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    .line 46513
    return-void
.end method

.method public addPhonenumber(ILcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 46579
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46580
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 46582
    return-object p0
.end method

.method public addPhonenumber(ILcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 46562
    if-nez p2, :cond_0

    .line 46563
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46565
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46566
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 46568
    return-object p0
.end method

.method public addPhonenumber(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2
    .parameter

    .prologue
    .line 46572
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46573
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46575
    return-object p0
.end method

.method public addPhonenumber(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46552
    if-nez p1, :cond_0

    .line 46553
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46555
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46556
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 46558
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 2

    .prologue
    .line 46110
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    .line 46111
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 46112
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 46114
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 5

    .prologue
    .line 46128
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 46129
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46130
    const/4 v2, 0x0

    .line 46131
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 46132
    or-int/lit8 v2, v2, 0x1

    .line 46134
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$58802(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46135
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 46136
    or-int/lit8 v2, v2, 0x2

    .line 46138
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$58902(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46139
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 46140
    or-int/lit8 v2, v2, 0x4

    .line 46142
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59002(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46143
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 46144
    new-instance v3, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v3, v4}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46146
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46148
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59102(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    .line 46149
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 46150
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46151
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x11

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46153
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59202(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/util/List;)Ljava/util/List;

    .line 46154
    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    .line 46155
    or-int/lit8 v2, v2, 0x8

    .line 46157
    :cond_5
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59302(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;J)J

    .line 46158
    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    .line 46159
    or-int/lit8 v2, v2, 0x10

    .line 46161
    :cond_6
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59402(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Z)Z

    .line 46162
    and-int/lit16 v3, v1, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    .line 46163
    or-int/lit8 v2, v2, 0x20

    .line 46165
    :cond_7
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59502(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Z)Z

    .line 46166
    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    .line 46167
    or-int/lit8 v2, v2, 0x40

    .line 46169
    :cond_8
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59602(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46170
    and-int/lit16 v3, v1, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    .line 46171
    or-int/lit16 v2, v2, 0x80

    .line 46173
    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59702(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46174
    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    .line 46175
    or-int/lit16 v2, v2, 0x100

    .line 46177
    :cond_a
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59802(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46178
    and-int/lit16 v1, v1, 0x800

    const/16 v3, 0x800

    if-ne v1, v3, :cond_b

    .line 46179
    or-int/lit16 v1, v2, 0x200

    .line 46181
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59902(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46182
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$60002(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;I)I

    .line 46183
    return-object v0

    :cond_b
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46073
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 46074
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46075
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46076
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46077
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46078
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46079
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46080
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46081
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46082
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46083
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46084
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    .line 46085
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46086
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    .line 46087
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46088
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    .line 46089
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46090
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46091
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46092
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46093
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46094
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46095
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46096
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46097
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46098
    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46448
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46449
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getAccountid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46451
    return-object p0
.end method

.method public clearDevicecontactid()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2

    .prologue
    .line 46619
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46620
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    .line 46622
    return-object p0
.end method

.method public clearDisplayname()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46728
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46729
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46731
    return-object p0
.end method

.method public clearEmailaddress()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46504
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46505
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46507
    return-object p0
.end method

.method public clearFirstname()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46377
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getFirstname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46379
    return-object p0
.end method

.method public clearHaspicture()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46640
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46641
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    .line 46643
    return-object p0
.end method

.method public clearIsnativefavorite()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46661
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46662
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    .line 46664
    return-object p0
.end method

.method public clearLastname()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46412
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46413
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getLastname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46415
    return-object p0
.end method

.method public clearMiddlename()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46692
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46693
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46695
    return-object p0
.end method

.method public clearNameprefix()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46764
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46765
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46767
    return-object p0
.end method

.method public clearNamesuffix()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46800
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46801
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46803
    return-object p0
.end method

.method public clearPhonenumber()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46592
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46593
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46595
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2

    .prologue
    .line 46102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46429
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46430
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46431
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46432
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46435
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1

    .prologue
    .line 46106
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public getDevicecontactid()J
    .locals 2

    .prologue
    .line 46610
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    return-wide v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46709
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46710
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46711
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46712
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46715
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getEmailaddress(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 46475
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getEmailaddressCount()I
    .locals 1

    .prologue
    .line 46472
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getEmailaddressList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46469
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46358
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46359
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46360
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46363
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getHaspicture()Z
    .locals 1

    .prologue
    .line 46631
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    return v0
.end method

.method public getIsnativefavorite()Z
    .locals 1

    .prologue
    .line 46652
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    return v0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46393
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46394
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46395
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46396
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46399
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46673
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46674
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46675
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46676
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46679
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46745
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46746
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46747
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46748
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46751
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46781
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46782
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 46783
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 46784
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46787
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter

    .prologue
    .line 46532
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    return-object v0
.end method

.method public getPhonenumberCount()I
    .locals 1

    .prologue
    .line 46529
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPhonenumberList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46526
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 46426
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicecontactid()Z
    .locals 2

    .prologue
    .line 46607
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 46706
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 46354
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHaspicture()Z
    .locals 2

    .prologue
    .line 46628
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsnativefavorite()Z
    .locals 2

    .prologue
    .line 46649
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 46390
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 46670
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 46742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 46778
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46242
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->hasFirstname()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 46264
    :goto_0
    return v0

    .line 46246
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->hasLastname()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 46248
    goto :goto_0

    .line 46250
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 46252
    goto :goto_0

    .line 46254
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->hasDevicecontactid()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 46256
    goto :goto_0

    :cond_3
    move v0, v2

    .line 46258
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->getPhonenumberCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 46259
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->getPhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    .line 46261
    goto :goto_0

    .line 46258
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 46264
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46057
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46057
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46057
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 46273
    sparse-switch v0, :sswitch_data_0

    .line 46278
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 46280
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 46276
    goto :goto_1

    .line 46285
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46286
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    goto :goto_0

    .line 46290
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46291
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    goto :goto_0

    .line 46295
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46296
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    goto :goto_0

    .line 46300
    :sswitch_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46301
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    .line 46305
    :sswitch_5
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    .line 46306
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 46307
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->addPhonenumber(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    goto :goto_0

    .line 46311
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46312
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    goto :goto_0

    .line 46316
    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46317
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    goto :goto_0

    .line 46321
    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46322
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    goto :goto_0

    .line 46326
    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46327
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 46331
    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46332
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 46336
    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46337
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 46341
    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46342
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    goto/16 :goto_0

    .line 46273
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2
    .parameter

    .prologue
    .line 46187
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 46238
    :goto_0
    return-object v0

    .line 46188
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasFirstname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46189
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getFirstname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46191
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasLastname()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 46192
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getLastname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46194
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasAccountid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 46195
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getAccountid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46197
    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59100(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 46198
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 46199
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59100(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 46200
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46207
    :cond_4
    :goto_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59200(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 46208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 46209
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59200(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    .line 46210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46217
    :cond_5
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasDevicecontactid()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 46218
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDevicecontactid()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setDevicecontactid(J)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46220
    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasHaspicture()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 46221
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getHaspicture()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setHaspicture(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46223
    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasIsnativefavorite()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 46224
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getIsnativefavorite()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setIsnativefavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46226
    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasMiddlename()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 46227
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getMiddlename()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46229
    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasDisplayname()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 46230
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDisplayname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46232
    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasNameprefix()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 46233
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNameprefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    .line 46235
    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasNamesuffix()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 46236
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNamesuffix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    :cond_c
    move-object v0, p0

    .line 46238
    goto/16 :goto_0

    .line 46202
    :cond_d
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46203
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59100(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 46212
    :cond_e
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46213
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->access$59200(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_2
.end method

.method public removePhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46598
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46599
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 46601
    return-object p0
.end method

.method public setAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46439
    if-nez p1, :cond_0

    .line 46440
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46442
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46443
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46445
    return-object p0
.end method

.method setAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46454
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46455
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->accountid_:Ljava/lang/Object;

    .line 46457
    return-void
.end method

.method public setDevicecontactid(J)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46613
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46614
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->devicecontactid_:J

    .line 46616
    return-object p0
.end method

.method public setDisplayname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46719
    if-nez p1, :cond_0

    .line 46720
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46722
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46723
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46725
    return-object p0
.end method

.method setDisplayname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46734
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46735
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->displayname_:Ljava/lang/Object;

    .line 46737
    return-void
.end method

.method public setEmailaddress(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 46479
    if-nez p2, :cond_0

    .line 46480
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46482
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensureEmailaddressIsMutable()V

    .line 46483
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 46485
    return-object p0
.end method

.method public setFirstname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46367
    if-nez p1, :cond_0

    .line 46368
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46370
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46371
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46373
    return-object p0
.end method

.method setFirstname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46382
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46383
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->firstname_:Ljava/lang/Object;

    .line 46385
    return-void
.end method

.method public setHaspicture(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46634
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46635
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->haspicture_:Z

    .line 46637
    return-object p0
.end method

.method public setIsnativefavorite(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46655
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46656
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->isnativefavorite_:Z

    .line 46658
    return-object p0
.end method

.method public setLastname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46403
    if-nez p1, :cond_0

    .line 46404
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46406
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46407
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46409
    return-object p0
.end method

.method setLastname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46418
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46419
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->lastname_:Ljava/lang/Object;

    .line 46421
    return-void
.end method

.method public setMiddlename(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46683
    if-nez p1, :cond_0

    .line 46684
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46686
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46687
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46689
    return-object p0
.end method

.method setMiddlename(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46698
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46699
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->middlename_:Ljava/lang/Object;

    .line 46701
    return-void
.end method

.method public setNameprefix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46755
    if-nez p1, :cond_0

    .line 46756
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46758
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46759
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46761
    return-object p0
.end method

.method setNameprefix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46770
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46771
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->nameprefix_:Ljava/lang/Object;

    .line 46773
    return-void
.end method

.method public setNamesuffix(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46791
    if-nez p1, :cond_0

    .line 46792
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46794
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46795
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46797
    return-object p0
.end method

.method setNamesuffix(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 46806
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->bitField0_:I

    .line 46807
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->namesuffix_:Ljava/lang/Object;

    .line 46809
    return-void
.end method

.method public setPhonenumber(ILcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 46546
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46547
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 46549
    return-object p0
.end method

.method public setPhonenumber(ILcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 46536
    if-nez p2, :cond_0

    .line 46537
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 46539
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->ensurePhonenumberIsMutable()V

    .line 46540
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 46542
    return-object p0
.end method
