.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x3

.field public static final DEVICECONTACTID_FIELD_NUMBER:I = 0x6

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0xa

.field public static final EMAILADDRESS_FIELD_NUMBER:I = 0x4

.field public static final FIRSTNAME_FIELD_NUMBER:I = 0x1

.field public static final HASPICTURE_FIELD_NUMBER:I = 0x7

.field public static final ISNATIVEFAVORITE_FIELD_NUMBER:I = 0x8

.field public static final LASTNAME_FIELD_NUMBER:I = 0x2

.field public static final MIDDLENAME_FIELD_NUMBER:I = 0x9

.field public static final NAMEPREFIX_FIELD_NUMBER:I = 0xb

.field public static final NAMESUFFIX_FIELD_NUMBER:I = 0xc

.field public static final PHONENUMBER_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private bitField0_:I

.field private devicecontactid_:J

.field private displayname_:Ljava/lang/Object;

.field private emailaddress_:Lcom/google/protobuf/LazyStringList;

.field private firstname_:Ljava/lang/Object;

.field private haspicture_:Z

.field private isnativefavorite_:Z

.field private lastname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private middlename_:Ljava/lang/Object;

.field private nameprefix_:Ljava/lang/Object;

.field private namesuffix_:Ljava/lang/Object;

.field private phonenumber_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46815
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    .line 46816
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->initFields()V

    .line 46817
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 45526
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 45843
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    .line 45915
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedSerializedSize:I

    .line 45527
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 45528
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 45843
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    .line 45915
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedSerializedSize:I

    .line 45528
    return-void
.end method

.method static synthetic access$58802(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$58902(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59002(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59100(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/google/protobuf/LazyStringList;
    .locals 1
    .parameter

    .prologue
    .line 45521
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$59102(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method static synthetic access$59200(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 45521
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$59202(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$59302(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J

    return-wide p1
.end method

.method static synthetic access$59402(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z

    return p1
.end method

.method static synthetic access$59502(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z

    return p1
.end method

.method static synthetic access$59602(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59702(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59802(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$59902(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$60002(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45521
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45625
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    .line 45626
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45627
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45629
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    .line 45632
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1

    .prologue
    .line 45532
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45754
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    .line 45755
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45756
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45758
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    .line 45761
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getFirstnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45561
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    .line 45562
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45563
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45565
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    .line 45568
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getLastnameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45593
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    .line 45594
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45595
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45597
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    .line 45600
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMiddlenameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45722
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    .line 45723
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45724
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45726
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    .line 45729
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNameprefixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45786
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    .line 45787
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45788
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45790
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    .line 45793
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getNamesuffixBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45818
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    .line 45819
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45820
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45822
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    .line 45825
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 45830
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    .line 45831
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    .line 45832
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    .line 45833
    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    .line 45834
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    .line 45835
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J

    .line 45836
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z

    .line 45837
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z

    .line 45838
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    .line 45839
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    .line 45840
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    .line 45841
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    .line 45842
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46050
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58600()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46053
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    .line 46020
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46021
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    .line 46023
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46030
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    .line 46031
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46032
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    .line 46034
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45986
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45992
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46040
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46046
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46008
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46014
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45997
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46003
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->access$58500(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45611
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    .line 45612
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45613
    check-cast v0, Ljava/lang/String;

    .line 45621
    :goto_0
    return-object v0

    .line 45615
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45617
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45618
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45619
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45621
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 45521
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1

    .prologue
    .line 45536
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public getDevicecontactid()J
    .locals 2

    .prologue
    .line 45678
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J

    return-wide v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45740
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    .line 45741
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45742
    check-cast v0, Ljava/lang/String;

    .line 45750
    :goto_0
    return-object v0

    .line 45744
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45746
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45747
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45748
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45750
    goto :goto_0
.end method

.method public getEmailaddress(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 45647
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getEmailaddressCount()I
    .locals 1

    .prologue
    .line 45644
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getEmailaddressList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45641
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public getFirstname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45547
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    .line 45548
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45549
    check-cast v0, Ljava/lang/String;

    .line 45557
    :goto_0
    return-object v0

    .line 45551
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45553
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45554
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45555
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->firstname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45557
    goto :goto_0
.end method

.method public getHaspicture()Z
    .locals 1

    .prologue
    .line 45688
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z

    return v0
.end method

.method public getIsnativefavorite()Z
    .locals 1

    .prologue
    .line 45698
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z

    return v0
.end method

.method public getLastname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45579
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    .line 45580
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45581
    check-cast v0, Ljava/lang/String;

    .line 45589
    :goto_0
    return-object v0

    .line 45583
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45585
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45586
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45587
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->lastname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45589
    goto :goto_0
.end method

.method public getMiddlename()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    .line 45709
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45710
    check-cast v0, Ljava/lang/String;

    .line 45718
    :goto_0
    return-object v0

    .line 45712
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45714
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45715
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45716
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->middlename_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45718
    goto :goto_0
.end method

.method public getNameprefix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45772
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    .line 45773
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45774
    check-cast v0, Ljava/lang/String;

    .line 45782
    :goto_0
    return-object v0

    .line 45776
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45778
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45779
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45780
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->nameprefix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45782
    goto :goto_0
.end method

.method public getNamesuffix()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45804
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    .line 45805
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45806
    check-cast v0, Ljava/lang/String;

    .line 45814
    :goto_0
    return-object v0

    .line 45808
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45810
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45811
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45812
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->namesuffix_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45814
    goto :goto_0
.end method

.method public getPhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter

    .prologue
    .line 45664
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    return-object v0
.end method

.method public getPhonenumberCount()I
    .locals 1

    .prologue
    .line 45661
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getPhonenumberList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45654
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    return-object v0
.end method

.method public getPhonenumberOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 45668
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;

    return-object v0
.end method

.method public getPhonenumberOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45658
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 45917
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedSerializedSize:I

    .line 45918
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 45975
    :goto_0
    return v0

    .line 45921
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_c

    .line 45922
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v4

    .line 45925
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_1

    .line 45926
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45929
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 45930
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v4

    move v2, v4

    .line 45935
    :goto_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 45936
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v3, v1}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v3

    invoke-static {v3}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v3

    add-int/2addr v2, v3

    .line 45935
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 45939
    :cond_3
    add-int/2addr v0, v2

    .line 45940
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getEmailaddressList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    move v1, v4

    move v2, v0

    .line 45942
    :goto_3
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 45943
    const/4 v3, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 45942
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_3

    .line 45946
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_b

    .line 45947
    const/4 v0, 0x6

    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J

    invoke-static {v0, v3, v4}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v0

    add-int/2addr v0, v2

    .line 45950
    :goto_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 45951
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 45954
    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    .line 45955
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 45958
    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    .line 45959
    const/16 v1, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45962
    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_8

    .line 45963
    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45966
    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_9

    .line 45967
    const/16 v1, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45970
    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_a

    .line 45971
    const/16 v1, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45974
    :cond_a
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_4

    :cond_c
    move v0, v4

    goto/16 :goto_1
.end method

.method public hasAccountid()Z
    .locals 2

    .prologue
    .line 45608
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDevicecontactid()Z
    .locals 2

    .prologue
    .line 45675
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    .prologue
    .line 45737
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFirstname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45544
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHaspicture()Z
    .locals 2

    .prologue
    .line 45685
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsnativefavorite()Z
    .locals 2

    .prologue
    .line 45695
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLastname()Z
    .locals 2

    .prologue
    .line 45576
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMiddlename()Z
    .locals 2

    .prologue
    .line 45705
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNameprefix()Z
    .locals 2

    .prologue
    .line 45769
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNamesuffix()Z
    .locals 2

    .prologue
    .line 45801
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45845
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    .line 45846
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 45871
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 45846
    goto :goto_0

    .line 45848
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasFirstname()Z

    move-result v0

    if-nez v0, :cond_2

    .line 45849
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v2

    .line 45850
    goto :goto_0

    .line 45852
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasLastname()Z

    move-result v0

    if-nez v0, :cond_3

    .line 45853
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v2

    .line 45854
    goto :goto_0

    .line 45856
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_4

    .line 45857
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v2

    .line 45858
    goto :goto_0

    .line 45860
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->hasDevicecontactid()Z

    move-result v0

    if-nez v0, :cond_5

    .line 45861
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v2

    .line 45862
    goto :goto_0

    :cond_5
    move v0, v2

    .line 45864
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getPhonenumberCount()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 45865
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getPhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_6

    .line 45866
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v2

    .line 45867
    goto :goto_0

    .line 45864
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 45870
    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->memoizedIsInitialized:B

    move v0, v3

    .line 45871
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 45521
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46051
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 45521
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;
    .locals 1

    .prologue
    .line 46055
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 45980
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45876
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getSerializedSize()I

    .line 45877
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 45878
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getFirstnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45880
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    .line 45881
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getLastnameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45883
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    .line 45884
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    move v0, v2

    .line 45886
    :goto_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 45887
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->emailaddress_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v4, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45886
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v1, v2

    .line 45889
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 45890
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->phonenumber_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 45889
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 45892
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_5

    .line 45893
    const/4 v0, 0x6

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->devicecontactid_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    .line 45895
    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 45896
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->haspicture_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 45898
    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_7

    .line 45899
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isnativefavorite_:Z

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 45901
    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_8

    .line 45902
    const/16 v0, 0x9

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getMiddlenameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45904
    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_9

    .line 45905
    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45907
    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    .line 45908
    const/16 v0, 0xb

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNameprefixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45910
    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    .line 45911
    const/16 v0, 0xc

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->getNamesuffixBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45913
    :cond_b
    return-void
.end method
