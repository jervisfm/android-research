.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemListOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 46994
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 47103
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 46995
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->maybeForceBuilderInitialization()V

    .line 46996
    return-void
.end method

.method static synthetic access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46989
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$60200()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 47029
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    .line 47030
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47031
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 47034
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 47001
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactsIsMutable()V
    .locals 2

    .prologue
    .line 47106
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 47107
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 47108
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    .line 47110
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 46999
    return-void
.end method


# virtual methods
.method public addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;"
        }
    .end annotation

    .prologue
    .line 47173
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47174
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 47176
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 47166
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47167
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 47169
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47149
    if-nez p2, :cond_0

    .line 47150
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47152
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47153
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 47155
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 47159
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47160
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47162
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 47139
    if-nez p1, :cond_0

    .line 47140
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47142
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47143
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47145
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 2

    .prologue
    .line 47020
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    .line 47021
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 47022
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 47024
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 3

    .prologue
    .line 47038
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 47039
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    .line 47040
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 47041
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 47042
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    .line 47044
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->access$60402(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;Ljava/util/List;)Ljava/util/List;

    .line 47045
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 47005
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 47006
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 47007
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    .line 47008
    return-object p0
.end method

.method public clearContacts()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 47179
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 47180
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    .line 47182
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 2

    .prologue
    .line 47012
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter

    .prologue
    .line 47119
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 47116
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47113
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46989
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1

    .prologue
    .line 47016
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 47064
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 47065
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    .line 47070
    :goto_1
    return v0

    .line 47064
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47070
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46989
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46989
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46989
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47078
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 47079
    sparse-switch v0, :sswitch_data_0

    .line 47084
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 47086
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 47082
    goto :goto_1

    .line 47091
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    .line 47092
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 47093
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->addContacts(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    goto :goto_0

    .line 47079
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 2
    .parameter

    .prologue
    .line 47049
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 47060
    :goto_0
    return-object v0

    .line 47050
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->access$60400(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 47051
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 47052
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->access$60400(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    .line 47053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 47060
    goto :goto_0

    .line 47055
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47056
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->access$60400(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 47185
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47186
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 47188
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 47133
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47134
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 47136
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47123
    if-nez p2, :cond_0

    .line 47124
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 47126
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->ensureContactsIsMutable()V

    .line 47127
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 47129
    return-object p0
.end method
