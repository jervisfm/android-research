.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactItemList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    }
.end annotation


# static fields
.field public static final CONTACTS_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;


# instance fields
.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47195
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    .line 47196
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->initFields()V

    .line 47197
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 46836
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 46873
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedIsInitialized:B

    .line 46896
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedSerializedSize:I

    .line 46837
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46831
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 46838
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 46873
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedIsInitialized:B

    .line 46896
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedSerializedSize:I

    .line 46838
    return-void
.end method

.method static synthetic access$60400(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 46831
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$60402(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46831
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1

    .prologue
    .line 46842
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 46871
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    .line 46872
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 46982
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60200()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1
    .parameter

    .prologue
    .line 46985
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46951
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    .line 46952
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46953
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    .line 46955
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46962
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    .line 46963
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46964
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    .line 46966
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46918
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46924
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46972
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46978
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46940
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46946
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46929
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 46935
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;->access$60100(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter

    .prologue
    .line 46863
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 46860
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46853
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 46867
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46857
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 46831
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;
    .locals 1

    .prologue
    .line 46846
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 46898
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedSerializedSize:I

    .line 46899
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 46907
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 46902
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 46903
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 46902
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 46906
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedSerializedSize:I

    move v0, v2

    .line 46907
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46875
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedIsInitialized:B

    .line 46876
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 46885
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 46876
    goto :goto_0

    :cond_1
    move v0, v2

    .line 46878
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 46879
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    .line 46880
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedIsInitialized:B

    move v0, v2

    .line 46881
    goto :goto_0

    .line 46878
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 46884
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->memoizedIsInitialized:B

    move v0, v3

    .line 46885
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46831
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 46983
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 46831
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;
    .locals 1

    .prologue
    .line 46987
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemList$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 46912
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46890
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->getSerializedSize()I

    .line 46891
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 46892
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemList;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 46891
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46894
    :cond_0
    return-void
.end method
