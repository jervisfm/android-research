.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactItemOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountid()Ljava/lang/String;
.end method

.method public abstract getDevicecontactid()J
.end method

.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getEmailaddress(I)Ljava/lang/String;
.end method

.method public abstract getEmailaddressCount()I
.end method

.method public abstract getEmailaddressList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFirstname()Ljava/lang/String;
.end method

.method public abstract getHaspicture()Z
.end method

.method public abstract getIsnativefavorite()Z
.end method

.method public abstract getLastname()Ljava/lang/String;
.end method

.method public abstract getMiddlename()Ljava/lang/String;
.end method

.method public abstract getNameprefix()Ljava/lang/String;
.end method

.method public abstract getNamesuffix()Ljava/lang/String;
.end method

.method public abstract getPhonenumber(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
.end method

.method public abstract getPhonenumberCount()I
.end method

.method public abstract getPhonenumberList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasAccountid()Z
.end method

.method public abstract hasDevicecontactid()Z
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasFirstname()Z
.end method

.method public abstract hasHaspicture()Z
.end method

.method public abstract hasIsnativefavorite()Z
.end method

.method public abstract hasLastname()Z
.end method

.method public abstract hasMiddlename()Z
.end method

.method public abstract hasNameprefix()Z
.end method

.method public abstract hasNamesuffix()Z
.end method
