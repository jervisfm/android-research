.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private countryCode_:Ljava/lang/Object;

.field private subscriberNumber_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 45221
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 45361
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45397
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45433
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45222
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->maybeForceBuilderInitialization()V

    .line 45223
    return-void
.end method

.method static synthetic access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45216
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$57900()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45260
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    .line 45261
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45262
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 45265
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45228
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 45226
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 2

    .prologue
    .line 45251
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    .line 45252
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 45253
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 45255
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 5

    .prologue
    .line 45269
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 45270
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45271
    const/4 v2, 0x0

    .line 45272
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 45273
    or-int/lit8 v2, v2, 0x1

    .line 45275
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->access$58102(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45276
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 45277
    or-int/lit8 v2, v2, 0x2

    .line 45279
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->access$58202(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45280
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 45281
    or-int/lit8 v1, v2, 0x4

    .line 45283
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->access$58302(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45284
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->access$58402(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;I)I

    .line 45285
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45232
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 45233
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45234
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45235
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45236
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45237
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45238
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45239
    return-object p0
.end method

.method public clearCountryCode()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45385
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45388
    return-object p0
.end method

.method public clearSubscriberNumber()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45421
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45422
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45424
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45450
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45451
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45453
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 2

    .prologue
    .line 45243
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45366
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45367
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 45368
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 45369
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45372
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 45216
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1

    .prologue
    .line 45247
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45402
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45403
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 45404
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 45405
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45408
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1

    .prologue
    .line 45438
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45363
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscriberNumber()Z
    .locals 2

    .prologue
    .line 45399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 45435
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45303
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->hasCountryCode()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 45315
    :goto_0
    return v0

    .line 45307
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->hasSubscriberNumber()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 45309
    goto :goto_0

    .line 45311
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 45313
    goto :goto_0

    .line 45315
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45216
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45216
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45216
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 45324
    sparse-switch v0, :sswitch_data_0

    .line 45329
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 45331
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 45327
    goto :goto_1

    .line 45336
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45337
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    goto :goto_0

    .line 45341
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45342
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    goto :goto_0

    .line 45346
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 45347
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v0

    .line 45348
    if-eqz v0, :cond_0

    .line 45349
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45350
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    goto :goto_0

    .line 45324
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45289
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 45299
    :goto_0
    return-object v0

    .line 45290
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasCountryCode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45291
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->setCountryCode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    .line 45293
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasSubscriberNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45294
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getSubscriberNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    .line 45296
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 45297
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    :cond_3
    move-object v0, p0

    .line 45299
    goto :goto_0
.end method

.method public setCountryCode(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45376
    if-nez p1, :cond_0

    .line 45377
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 45379
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45380
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45382
    return-object p0
.end method

.method setCountryCode(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 45391
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45392
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->countryCode_:Ljava/lang/Object;

    .line 45394
    return-void
.end method

.method public setSubscriberNumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45412
    if-nez p1, :cond_0

    .line 45413
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 45415
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45416
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45418
    return-object p0
.end method

.method setSubscriberNumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 45427
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45428
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->subscriberNumber_:Ljava/lang/Object;

    .line 45430
    return-void
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45441
    if-nez p1, :cond_0

    .line 45442
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 45444
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->bitField0_:I

    .line 45445
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45447
    return-object p0
.end method
