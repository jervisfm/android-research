.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactItemPhoneNumber"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    }
.end annotation


# static fields
.field public static final COUNTRYCODE_FIELD_NUMBER:I = 0x1

.field public static final SUBSCRIBERNUMBER_FIELD_NUMBER:I = 0x2

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;


# instance fields
.field private bitField0_:I

.field private countryCode_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private subscriberNumber_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45460
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    .line 45461
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->initFields()V

    .line 45462
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 44987
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 45080
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    .line 45115
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedSerializedSize:I

    .line 44988
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44982
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 44989
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 45080
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    .line 45115
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedSerializedSize:I

    .line 44989
    return-void
.end method

.method static synthetic access$58102(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$58202(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$58302(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;Lcom/sgiggle/xmpp/SessionMessages$PhoneType;)Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44982
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object p1
.end method

.method static synthetic access$58402(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44982
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    return p1
.end method

.method private getCountryCodeBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45022
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    .line 45023
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45024
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45026
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    .line 45029
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1

    .prologue
    .line 44993
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    return-object v0
.end method

.method private getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 45054
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 45055
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45056
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 45058
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 45061
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 45076
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    .line 45077
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 45078
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->PHONE_TYPE_GENERIC:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    .line 45079
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45209
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57900()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1
    .parameter

    .prologue
    .line 45212
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45178
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    .line 45179
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45180
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    .line 45182
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45189
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    .line 45190
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45191
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    .line 45193
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45145
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45151
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45199
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45205
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45167
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45173
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45156
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 45162
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;->access$57800(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCountryCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45008
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    .line 45009
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45010
    check-cast v0, Ljava/lang/String;

    .line 45018
    :goto_0
    return-object v0

    .line 45012
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45014
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45015
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45016
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->countryCode_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45018
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 44982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;
    .locals 1

    .prologue
    .line 44997
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 45117
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedSerializedSize:I

    .line 45118
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 45134
    :goto_0
    return v0

    .line 45120
    :cond_0
    const/4 v0, 0x0

    .line 45121
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 45122
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getCountryCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45125
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 45126
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 45129
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 45130
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 45133
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSubscriberNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45040
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    .line 45041
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45042
    check-cast v0, Ljava/lang/String;

    .line 45050
    :goto_0
    return-object v0

    .line 45044
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 45046
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 45047
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45048
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->subscriberNumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 45050
    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
    .locals 1

    .prologue
    .line 45072
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    return-object v0
.end method

.method public hasCountryCode()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 45005
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSubscriberNumber()Z
    .locals 2

    .prologue
    .line 45037
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 45069
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 45082
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    .line 45083
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 45098
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 45083
    goto :goto_0

    .line 45085
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasCountryCode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 45086
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    move v0, v2

    .line 45087
    goto :goto_0

    .line 45089
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasSubscriberNumber()Z

    move-result v0

    if-nez v0, :cond_3

    .line 45090
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    move v0, v2

    .line 45091
    goto :goto_0

    .line 45093
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->hasType()Z

    move-result v0

    if-nez v0, :cond_4

    .line 45094
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    move v0, v2

    .line 45095
    goto :goto_0

    .line 45097
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->memoizedIsInitialized:B

    move v0, v3

    .line 45098
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45210
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 44982
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;
    .locals 1

    .prologue
    .line 45214
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 45139
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 45103
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getSerializedSize()I

    .line 45104
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 45105
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getCountryCodeBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45107
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 45108
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->getSubscriberNumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 45110
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 45111
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumber;->type_:Lcom/sgiggle/xmpp/SessionMessages$PhoneType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 45113
    :cond_2
    return-void
.end method
