.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactItemPhoneNumberOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactItemPhoneNumberOrBuilder"
.end annotation


# virtual methods
.method public abstract getCountryCode()Ljava/lang/String;
.end method

.method public abstract getSubscriberNumber()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$PhoneType;
.end method

.method public abstract hasCountryCode()Z
.end method

.method public abstract hasSubscriberNumber()Z
.end method

.method public abstract hasType()Z
.end method
