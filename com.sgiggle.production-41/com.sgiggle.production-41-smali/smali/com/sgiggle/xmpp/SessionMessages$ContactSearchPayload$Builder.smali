.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycode_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

.field private email_:Ljava/lang/Object;

.field private number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 49356
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 49555
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49598
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49641
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49677
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49766
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49357
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->maybeForceBuilderInitialization()V

    .line 49358
    return-void
.end method

.method static synthetic access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49351
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$62800()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49399
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    .line 49400
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49401
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 49404
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49363
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCountrycodeIsMutable()V
    .locals 2

    .prologue
    .line 49680
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 49681
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49682
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49684
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 49361
    return-void
.end method


# virtual methods
.method public addAllCountrycode(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 49747
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49748
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 49750
    return-object p0
.end method

.method public addCountrycode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 49740
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49741
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 49743
    return-object p0
.end method

.method public addCountrycode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 49723
    if-nez p2, :cond_0

    .line 49724
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49726
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49727
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 49729
    return-object p0
.end method

.method public addCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49733
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49734
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49736
    return-object p0
.end method

.method public addCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49713
    if-nez p1, :cond_0

    .line 49714
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49716
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49717
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49719
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 2

    .prologue
    .line 49390
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    .line 49391
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49392
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 49394
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 5

    .prologue
    .line 49408
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 49409
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49410
    const/4 v2, 0x0

    .line 49411
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 49412
    or-int/lit8 v2, v2, 0x1

    .line 49414
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63002(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49415
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 49416
    or-int/lit8 v2, v2, 0x2

    .line 49418
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63102(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49419
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 49420
    or-int/lit8 v2, v2, 0x4

    .line 49422
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63202(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49423
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 49424
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49425
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x9

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49427
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63302(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Ljava/util/List;)Ljava/util/List;

    .line 49428
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 49429
    or-int/lit8 v1, v2, 0x8

    .line 49431
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63402(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49432
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63502(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;I)I

    .line 49433
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49367
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 49368
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49369
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49370
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49371
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49372
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49373
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49374
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49375
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49376
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49377
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49378
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49591
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49593
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49594
    return-object p0
.end method

.method public clearCountrycode()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49753
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49754
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49756
    return-object p0
.end method

.method public clearDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49802
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49804
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49805
    return-object p0
.end method

.method public clearEmail()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49665
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49666
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getEmail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49668
    return-object p0
.end method

.method public clearNumber()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49634
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49636
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49637
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2

    .prologue
    .line 49382
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 49560
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 49693
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountrycodeCount()I
    .locals 1

    .prologue
    .line 49690
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountrycodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49687
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 49771
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 49351
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1

    .prologue
    .line 49386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49646
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49647
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 49648
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 49649
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49652
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 49603
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 49557
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDefaultCountrycode()Z
    .locals 2

    .prologue
    .line 49768
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 49643
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumber()Z
    .locals 2

    .prologue
    .line 49600
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49464
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 49490
    :goto_0
    return v0

    .line 49468
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 49470
    goto :goto_0

    .line 49472
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49473
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 49475
    goto :goto_0

    :cond_2
    move v0, v2

    .line 49478
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getCountrycodeCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 49479
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 49481
    goto :goto_0

    .line 49478
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 49484
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasDefaultCountrycode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 49485
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    .line 49487
    goto :goto_0

    .line 49490
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49579
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 49581
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49587
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49588
    return-object p0

    .line 49584
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeDefaultCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49790
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 49792
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49798
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49799
    return-object p0

    .line 49795
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49351
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49351
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49351
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 49499
    sparse-switch v0, :sswitch_data_0

    .line 49504
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 49506
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 49502
    goto :goto_1

    .line 49511
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 49512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49513
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 49515
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 49516
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    goto :goto_0

    .line 49520
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    .line 49521
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasNumber()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49522
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    .line 49524
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 49525
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->setNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    goto :goto_0

    .line 49529
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49530
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    goto :goto_0

    .line 49534
    :sswitch_4
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 49535
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 49536
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->addCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    goto :goto_0

    .line 49540
    :sswitch_5
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 49541
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->hasDefaultCountrycode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 49542
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 49544
    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 49545
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->setDefaultCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    goto :goto_0

    .line 49499
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49437
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 49460
    :goto_0
    return-object v0

    .line 49438
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49439
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    .line 49441
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 49442
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    .line 49444
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasEmail()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 49445
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    .line 49447
    :cond_3
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63300(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 49448
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 49449
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63300(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    .line 49450
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49457
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasDefaultCountrycode()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 49458
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeDefaultCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    :cond_5
    move-object v0, p0

    .line 49460
    goto :goto_0

    .line 49452
    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49453
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->access$63300(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public mergeNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49622
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 49624
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49630
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49631
    return-object p0

    .line 49627
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    goto :goto_0
.end method

.method public removeCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49759
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49760
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 49762
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49573
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49575
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49576
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49563
    if-nez p1, :cond_0

    .line 49564
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49566
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49568
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49569
    return-object p0
.end method

.method public setCountrycode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 49707
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 49710
    return-object p0
.end method

.method public setCountrycode(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 49697
    if-nez p2, :cond_0

    .line 49698
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49700
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->ensureCountrycodeIsMutable()V

    .line 49701
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 49703
    return-object p0
.end method

.method public setDefaultCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49784
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49786
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49787
    return-object p0
.end method

.method public setDefaultCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49774
    if-nez p1, :cond_0

    .line 49775
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49777
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49779
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49780
    return-object p0
.end method

.method public setEmail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49656
    if-nez p1, :cond_0

    .line 49657
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49659
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49660
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49662
    return-object p0
.end method

.method setEmail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 49671
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49672
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->email_:Ljava/lang/Object;

    .line 49674
    return-void
.end method

.method public setNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49616
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49618
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49619
    return-object p0
.end method

.method public setNumber(Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49606
    if-nez p1, :cond_0

    .line 49607
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49609
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49611
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->bitField0_:I

    .line 49612
    return-object p0
.end method
