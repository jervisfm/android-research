.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactSearchPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final COUNTRYCODE_FIELD_NUMBER:I = 0x4

.field public static final DEFAULT_COUNTRYCODE_FIELD_NUMBER:I = 0x5

.field public static final EMAIL_FIELD_NUMBER:I = 0x3

.field public static final NUMBER_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycode_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

.field private email_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49812
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    .line 49813
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->initFields()V

    .line 49814
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 49083
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 49187
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    .line 49242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedSerializedSize:I

    .line 49084
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 49085
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 49187
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    .line 49242
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedSerializedSize:I

    .line 49085
    return-void
.end method

.method static synthetic access$63002(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$63102(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;)Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object p1
.end method

.method static synthetic access$63202(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$63300(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 49078
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$63302(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$63402(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object p1
.end method

.method static synthetic access$63502(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49078
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1

    .prologue
    .line 49089
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    return-object v0
.end method

.method private getEmailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 49138
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    .line 49139
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49140
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 49142
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    .line 49145
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 49181
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49182
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    .line 49183
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    .line 49184
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    .line 49185
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 49186
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49344
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62800()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49347
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49313
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    .line 49314
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49315
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    .line 49317
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49324
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    .line 49325
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49326
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    .line 49328
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49280
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49286
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49334
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49340
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49302
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49308
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49291
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49297
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;->access$62700(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 49104
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 49163
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountrycodeCount()I
    .locals 1

    .prologue
    .line 49160
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountrycodeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49153
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    return-object v0
.end method

.method public getCountrycodeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 49167
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;

    return-object v0
.end method

.method public getCountrycodeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49157
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    return-object v0
.end method

.method public getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 49177
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 49078
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;
    .locals 1

    .prologue
    .line 49093
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 49124
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    .line 49125
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 49126
    check-cast v0, Ljava/lang/String;

    .line 49134
    :goto_0
    return-object v0

    .line 49128
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 49130
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 49131
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49132
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->email_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 49134
    goto :goto_0
.end method

.method public getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
    .locals 1

    .prologue
    .line 49114
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 49244
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedSerializedSize:I

    .line 49245
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 49269
    :goto_0
    return v0

    .line 49248
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    .line 49249
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    .line 49252
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 49253
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49256
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 49257
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    move v1, v3

    move v2, v0

    .line 49260
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 49261
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 49260
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 49264
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 49265
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 49268
    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 49101
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDefaultCountrycode()Z
    .locals 2

    .prologue
    .line 49174
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmail()Z
    .locals 2

    .prologue
    .line 49121
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNumber()Z
    .locals 2

    .prologue
    .line 49111
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49189
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    .line 49190
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 49219
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 49190
    goto :goto_0

    .line 49192
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 49193
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49194
    goto :goto_0

    .line 49196
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 49197
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49198
    goto :goto_0

    .line 49200
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasNumber()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 49201
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 49202
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49203
    goto :goto_0

    :cond_4
    move v0, v2

    .line 49206
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getCountrycodeCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 49207
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 49208
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49209
    goto :goto_0

    .line 49206
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 49212
    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->hasDefaultCountrycode()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 49213
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    .line 49214
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49215
    goto :goto_0

    .line 49218
    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 49219
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49078
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49345
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49078
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;
    .locals 1

    .prologue
    .line 49349
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 49274
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 49224
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getSerializedSize()I

    .line 49225
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 49226
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49228
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 49229
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->number_:Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49231
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 49232
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->getEmailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 49234
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 49235
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->countrycode_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49237
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 49238
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayload;->defaultCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49240
    :cond_4
    return-void
.end method
