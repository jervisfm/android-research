.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactSearchPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactSearchPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCountrycode(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract getCountrycodeCount()I
.end method

.method public abstract getCountrycodeList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getDefaultCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract getEmail()Ljava/lang/String;
.end method

.method public abstract getNumber()Lcom/sgiggle/xmpp/SessionMessages$PhoneNumber;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasDefaultCountrycode()Z
.end method

.method public abstract hasEmail()Z
.end method

.method public abstract hasNumber()Z
.end method
