.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 50011
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 50145
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50188
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50012
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->maybeForceBuilderInitialization()V

    .line 50013
    return-void
.end method

.method static synthetic access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50006
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$63700()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50048
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    .line 50049
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50050
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 50053
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50018
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 50016
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 2

    .prologue
    .line 50039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    .line 50040
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 50041
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 50043
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 5

    .prologue
    .line 50057
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 50058
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50059
    const/4 v2, 0x0

    .line 50060
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 50061
    or-int/lit8 v2, v2, 0x1

    .line 50063
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->access$63902(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50064
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 50065
    or-int/lit8 v1, v2, 0x2

    .line 50067
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->access$64002(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50068
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->access$64102(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;I)I

    .line 50069
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50022
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 50023
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50024
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50025
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50026
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50027
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50181
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50183
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50184
    return-object p0
.end method

.method public clearContact()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50224
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50226
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50227
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 2

    .prologue
    .line 50031
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 50150
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 50193
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50006
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1

    .prologue
    .line 50035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50147
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 50190
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50084
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 50100
    :goto_0
    return v0

    .line 50088
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->hasContact()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 50090
    goto :goto_0

    .line 50092
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 50094
    goto :goto_0

    .line 50096
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 50098
    goto :goto_0

    .line 50100
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 50169
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 50171
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50177
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50178
    return-object p0

    .line 50174
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 50212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 50214
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50220
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50221
    return-object p0

    .line 50217
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50006
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50006
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50006
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50108
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 50109
    sparse-switch v0, :sswitch_data_0

    .line 50114
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 50116
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 50112
    goto :goto_1

    .line 50121
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 50122
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50123
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 50125
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 50126
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    goto :goto_0

    .line 50130
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 50131
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->hasContact()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 50132
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    .line 50134
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 50135
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    goto :goto_0

    .line 50109
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50073
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 50080
    :goto_0
    return-object v0

    .line 50074
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50075
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    .line 50077
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->hasContact()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50078
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 50080
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50163
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50165
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50166
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50153
    if-nez p1, :cond_0

    .line 50154
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50156
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50158
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50159
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50206
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50208
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50209
    return-object p0
.end method

.method public setContact(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50196
    if-nez p1, :cond_0

    .line 50197
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 50199
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 50201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->bitField0_:I

    .line 50202
    return-object p0
.end method
