.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactSearchResultPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACT_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50234
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    .line 50235
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->initFields()V

    .line 50236
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 49835
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 49873
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    .line 49909
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedSerializedSize:I

    .line 49836
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49830
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 49837
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 49873
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    .line 49909
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedSerializedSize:I

    .line 49837
    return-void
.end method

.method static synthetic access$63902(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49830
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$64002(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49830
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$64102(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49830
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1

    .prologue
    .line 49841
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 49870
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 49871
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    .line 49872
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 49999
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63700()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50002
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49968
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    .line 49969
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49970
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    .line 49972
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49979
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    .line 49980
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49981
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    .line 49983
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49935
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49941
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49989
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49995
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49957
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49963
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49946
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 49952
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;->access$63600(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 49856
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    .prologue
    .line 49866
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 49830
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;
    .locals 1

    .prologue
    .line 49845
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 49911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedSerializedSize:I

    .line 49912
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 49924
    :goto_0
    return v0

    .line 49914
    :cond_0
    const/4 v0, 0x0

    .line 49915
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 49916
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49919
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 49920
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 49923
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 49853
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContact()Z
    .locals 2

    .prologue
    .line 49863
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49875
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    .line 49876
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 49895
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 49876
    goto :goto_0

    .line 49878
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 49879
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49880
    goto :goto_0

    .line 49882
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->hasContact()Z

    move-result v0

    if-nez v0, :cond_3

    .line 49883
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49884
    goto :goto_0

    .line 49886
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 49887
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49888
    goto :goto_0

    .line 49890
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getContact()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 49891
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 49892
    goto :goto_0

    .line 49894
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 49895
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49830
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50000
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 49830
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;
    .locals 1

    .prologue
    .line 50004
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 49929
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 49900
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->getSerializedSize()I

    .line 49901
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 49902
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49904
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 49905
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchResultPayload;->contact_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 49907
    :cond_1
    return-void
.end method
