.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactSearchSwitchTypePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50622
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    .line 50623
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->initFields()V

    .line 50624
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 50257
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 50295
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    .line 50327
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedSerializedSize:I

    .line 50258
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50252
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 50259
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 50295
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    .line 50327
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedSerializedSize:I

    .line 50259
    return-void
.end method

.method static synthetic access$64502(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50252
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$64602(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50252
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->type_:I

    return p1
.end method

.method static synthetic access$64702(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 50252
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1

    .prologue
    .line 50263
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 50292
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 50293
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->type_:I

    .line 50294
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    .locals 1

    .prologue
    .line 50417
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64300()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 50420
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50386
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    .line 50387
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50388
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    .line 50390
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50397
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    .line 50398
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50399
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    .line 50401
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50353
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50359
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50407
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50413
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50375
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50381
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50364
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 50370
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;->access$64200(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 50278
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 50252
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;
    .locals 1

    .prologue
    .line 50267
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 50329
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedSerializedSize:I

    .line 50330
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 50342
    :goto_0
    return v0

    .line 50332
    :cond_0
    const/4 v0, 0x0

    .line 50333
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 50334
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 50337
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 50338
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->type_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 50341
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 50288
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 50275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 50285
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50297
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    .line 50298
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 50313
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 50298
    goto :goto_0

    .line 50300
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 50301
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50302
    goto :goto_0

    .line 50304
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 50305
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50306
    goto :goto_0

    .line 50308
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 50309
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 50310
    goto :goto_0

    .line 50312
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 50313
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50252
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    .locals 1

    .prologue
    .line 50418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 50252
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;
    .locals 1

    .prologue
    .line 50422
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 50347
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 50318
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->getSerializedSize()I

    .line 50319
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 50320
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 50322
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 50323
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactSearchSwitchTypePayload;->type_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 50325
    :cond_1
    return-void
.end method
