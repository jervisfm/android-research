.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactStoreOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactStore;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactStoreOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private contactitem_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 48845
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 48954
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 48846
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->maybeForceBuilderInitialization()V

    .line 48847
    return-void
.end method

.method static synthetic access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48840
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$62400()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48880
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    .line 48881
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48882
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 48885
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48852
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactitemIsMutable()V
    .locals 2

    .prologue
    .line 48957
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 48958
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 48959
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    .line 48961
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 48850
    return-void
.end method


# virtual methods
.method public addAllContactitem(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;"
        }
    .end annotation

    .prologue
    .line 49024
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 49025
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 49027
    return-object p0
.end method

.method public addContactitem(ILcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 49017
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 49018
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 49020
    return-object p0
.end method

.method public addContactitem(ILcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 49000
    if-nez p2, :cond_0

    .line 49001
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 49003
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 49004
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 49006
    return-object p0
.end method

.method public addContactitem(Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 2
    .parameter

    .prologue
    .line 49010
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 49011
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49013
    return-object p0
.end method

.method public addContactitem(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48990
    if-nez p1, :cond_0

    .line 48991
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48993
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 48994
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48996
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 2

    .prologue
    .line 48871
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    .line 48872
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 48873
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 48875
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 3

    .prologue
    .line 48889
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 48890
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    .line 48891
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 48892
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 48893
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    .line 48895
    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->access$62602(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;Ljava/util/List;)Ljava/util/List;

    .line 48896
    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48856
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 48857
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 48858
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    .line 48859
    return-object p0
.end method

.method public clearContactitem()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 49030
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 49031
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    .line 49033
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 2

    .prologue
    .line 48863
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter

    .prologue
    .line 48970
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public getContactitemCount()I
    .locals 1

    .prologue
    .line 48967
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactitemList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48964
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 48840
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1

    .prologue
    .line 48867
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48915
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->getContactitemCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 48916
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->getContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    .line 48921
    :goto_1
    return v0

    .line 48915
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48921
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48840
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48840
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48840
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48929
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 48930
    sparse-switch v0, :sswitch_data_0

    .line 48935
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 48937
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 48933
    goto :goto_1

    .line 48942
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;

    move-result-object v0

    .line 48943
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 48944
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->addContactitem(Lcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    goto :goto_0

    .line 48930
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 2
    .parameter

    .prologue
    .line 48900
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 48911
    :goto_0
    return-object v0

    .line 48901
    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->access$62600(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48902
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48903
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->access$62600(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    .line 48904
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    .line 48911
    goto :goto_0

    .line 48906
    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 48907
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->access$62600(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter

    .prologue
    .line 49036
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 49037
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 49039
    return-object p0
.end method

.method public setContactitem(ILcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 48984
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 48985
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 48987
    return-object p0
.end method

.method public setContactitem(ILcom/sgiggle/xmpp/SessionMessages$ContactItem;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 48974
    if-nez p2, :cond_0

    .line 48975
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 48977
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->ensureContactitemIsMutable()V

    .line 48978
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 48980
    return-object p0
.end method
