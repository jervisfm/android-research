.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactStoreOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactStore"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    }
.end annotation


# static fields
.field public static final CONTACTITEM_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactStore;


# instance fields
.field private contactitem_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 49046
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    .line 49047
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->initFields()V

    .line 49048
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 48687
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 48724
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedIsInitialized:B

    .line 48747
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedSerializedSize:I

    .line 48688
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48682
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 48689
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 48724
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedIsInitialized:B

    .line 48747
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedSerializedSize:I

    .line 48689
    return-void
.end method

.method static synthetic access$62600(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 48682
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$62602(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48682
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1

    .prologue
    .line 48693
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 48722
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    .line 48723
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48833
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62400()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1
    .parameter

    .prologue
    .line 48836
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48802
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    .line 48803
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48804
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    .line 48806
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48813
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    .line 48814
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48815
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    .line 48817
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48769
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48775
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48823
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48829
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48791
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48797
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48780
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 48786
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;->access$62300(Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
    .locals 1
    .parameter

    .prologue
    .line 48714
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    return-object v0
.end method

.method public getContactitemCount()I
    .locals 1

    .prologue
    .line 48711
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactitemList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48704
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    return-object v0
.end method

.method public getContactitemOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 48718
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;

    return-object v0
.end method

.method public getContactitemOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItemOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 48682
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore;
    .locals 1

    .prologue
    .line 48697
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactStore;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 48749
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedSerializedSize:I

    .line 48750
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 48758
    :goto_0
    return v0

    :cond_0
    move v1, v2

    .line 48753
    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 48754
    const/4 v3, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 48753
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    .line 48757
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedSerializedSize:I

    move v0, v2

    .line 48758
    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 48726
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedIsInitialized:B

    .line 48727
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 48736
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 48727
    goto :goto_0

    :cond_1
    move v0, v2

    .line 48729
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getContactitemCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 48730
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactItem;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    .line 48731
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedIsInitialized:B

    move v0, v2

    .line 48732
    goto :goto_0

    .line 48729
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48735
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->memoizedIsInitialized:B

    move v0, v3

    .line 48736
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 48682
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48834
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 48682
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;
    .locals 1

    .prologue
    .line 48838
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactStore;)Lcom/sgiggle/xmpp/SessionMessages$ContactStore$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 48763
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48741
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->getSerializedSize()I

    .line 48742
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 48743
    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactStore;->contactitem_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 48742
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 48745
    :cond_0
    return-void
.end method
