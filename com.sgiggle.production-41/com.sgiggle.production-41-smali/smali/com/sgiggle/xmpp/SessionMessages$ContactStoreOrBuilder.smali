.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactStoreOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactStoreOrBuilder"
.end annotation


# virtual methods
.method public abstract getContactitem(I)Lcom/sgiggle/xmpp/SessionMessages$ContactItem;
.end method

.method public abstract getContactitemCount()I
.end method

.method public abstract getContactitemList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactItem;",
            ">;"
        }
    .end annotation
.end method
