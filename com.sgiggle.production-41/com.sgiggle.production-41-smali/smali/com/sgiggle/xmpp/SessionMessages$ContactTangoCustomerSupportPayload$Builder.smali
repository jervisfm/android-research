.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private tangoemail_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 52040
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 52166
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52209
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52041
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->maybeForceBuilderInitialization()V

    .line 52042
    return-void
.end method

.method static synthetic access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52035
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$66700()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 52077
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    .line 52078
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52079
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 52082
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52047
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 52045
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 2

    .prologue
    .line 52068
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    .line 52069
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 52070
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 52072
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 5

    .prologue
    .line 52086
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 52087
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52088
    const/4 v2, 0x0

    .line 52089
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 52090
    or-int/lit8 v2, v2, 0x1

    .line 52092
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->access$66902(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52093
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 52094
    or-int/lit8 v1, v2, 0x2

    .line 52096
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->access$67002(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52097
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->access$67102(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;I)I

    .line 52098
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52051
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 52052
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52053
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52054
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52055
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52056
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52202
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52204
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52205
    return-object p0
.end method

.method public clearTangoemail()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52233
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52234
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getTangoemail()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52236
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 2

    .prologue
    .line 52060
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 52171
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 52035
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1

    .prologue
    .line 52064
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public getTangoemail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52214
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52215
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 52216
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 52217
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52220
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 52168
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTangoemail()Z
    .locals 2

    .prologue
    .line 52211
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52113
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 52125
    :goto_0
    return v0

    .line 52117
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->hasTangoemail()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 52119
    goto :goto_0

    .line 52121
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 52123
    goto :goto_0

    .line 52125
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 52190
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 52192
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52198
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52199
    return-object p0

    .line 52195
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52035
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52035
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52035
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 52134
    sparse-switch v0, :sswitch_data_0

    .line 52139
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 52141
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 52137
    goto :goto_1

    .line 52146
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 52147
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 52148
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 52150
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 52151
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    goto :goto_0

    .line 52155
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52156
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    goto :goto_0

    .line 52134
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 52109
    :goto_0
    return-object v0

    .line 52103
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52104
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    .line 52106
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->hasTangoemail()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52107
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getTangoemail()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->setTangoemail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    :cond_2
    move-object v0, p0

    .line 52109
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52184
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52186
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52187
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52174
    if-nez p1, :cond_0

    .line 52175
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52177
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 52179
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52180
    return-object p0
.end method

.method public setTangoemail(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52224
    if-nez p1, :cond_0

    .line 52225
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 52227
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52228
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52230
    return-object p0
.end method

.method setTangoemail(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 52239
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->bitField0_:I

    .line 52240
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->tangoemail_:Ljava/lang/Object;

    .line 52242
    return-void
.end method
