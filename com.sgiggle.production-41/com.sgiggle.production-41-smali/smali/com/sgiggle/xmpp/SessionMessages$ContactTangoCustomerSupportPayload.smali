.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactTangoCustomerSupportPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TANGOEMAIL_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private tangoemail_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52248
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    .line 52249
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->initFields()V

    .line 52250
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51846
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 51906
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    .line 51938
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedSerializedSize:I

    .line 51847
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51841
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 51848
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 51906
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    .line 51938
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedSerializedSize:I

    .line 51848
    return-void
.end method

.method static synthetic access$66902(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51841
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$67002(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51841
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$67102(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51841
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1

    .prologue
    .line 51852
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    return-object v0
.end method

.method private getTangoemailBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 51891
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    .line 51892
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51893
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 51895
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    .line 51898
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 51903
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 51904
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    .line 51905
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52028
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66700()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 52031
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51997
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    .line 51998
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51999
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    .line 52001
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52008
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    .line 52009
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52010
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    .line 52012
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51964
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51970
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52018
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52024
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51986
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51992
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51975
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 51981
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;->access$66600(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 51867
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 51841
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;
    .locals 1

    .prologue
    .line 51856
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 51940
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedSerializedSize:I

    .line 51941
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 51953
    :goto_0
    return v0

    .line 51943
    :cond_0
    const/4 v0, 0x0

    .line 51944
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 51945
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51948
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 51949
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getTangoemailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51952
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getTangoemail()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51877
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    .line 51878
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 51879
    check-cast v0, Ljava/lang/String;

    .line 51887
    :goto_0
    return-object v0

    .line 51881
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 51883
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 51884
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 51885
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->tangoemail_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 51887
    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 51864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTangoemail()Z
    .locals 2

    .prologue
    .line 51874
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51908
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    .line 51909
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 51924
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 51909
    goto :goto_0

    .line 51911
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 51912
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51913
    goto :goto_0

    .line 51915
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->hasTangoemail()Z

    move-result v0

    if-nez v0, :cond_3

    .line 51916
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51917
    goto :goto_0

    .line 51919
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 51920
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 51921
    goto :goto_0

    .line 51923
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 51924
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51841
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52029
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 51841
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;
    .locals 1

    .prologue
    .line 52033
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 51958
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 51929
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getSerializedSize()I

    .line 51930
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 51931
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 51933
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 51934
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactTangoCustomerSupportPayload;->getTangoemailBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 51936
    :cond_1
    return-void
.end method
