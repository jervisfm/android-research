.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private accountValidated_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private fromServer_:Z

.field private sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 27529
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 27730
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27773
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27883
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27907
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27931
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    .line 27530
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->maybeForceBuilderInitialization()V

    .line 27531
    return-void
.end method

.method static synthetic access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27524
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$34900()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27574
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    .line 27575
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 27576
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 27579
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27536
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureContactsIsMutable()V
    .locals 2

    .prologue
    .line 27776
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 27777
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27778
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27780
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 27534
    return-void
.end method


# virtual methods
.method public addAllContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 27843
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27844
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 27846
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 27836
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27837
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 27839
    return-object p0
.end method

.method public addContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27819
    if-nez p2, :cond_0

    .line 27820
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27822
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27823
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 27825
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 27829
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27830
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27832
    return-object p0
.end method

.method public addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27809
    if-nez p1, :cond_0

    .line 27810
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27812
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27813
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27815
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 2

    .prologue
    .line 27565
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    .line 27566
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 27567
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 27569
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 5

    .prologue
    .line 27583
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 27584
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27585
    const/4 v2, 0x0

    .line 27586
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 27587
    or-int/lit8 v2, v2, 0x1

    .line 27589
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35102(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27590
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 27591
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27592
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27594
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35202(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Ljava/util/List;)Ljava/util/List;

    .line 27595
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 27596
    or-int/lit8 v2, v2, 0x2

    .line 27598
    :cond_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35302(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Z)Z

    .line 27599
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 27600
    or-int/lit8 v2, v2, 0x4

    .line 27602
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35402(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27603
    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    .line 27604
    or-int/lit8 v2, v2, 0x8

    .line 27606
    :cond_4
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35502(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27607
    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_5

    .line 27608
    or-int/lit8 v1, v2, 0x10

    .line 27610
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35602(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Z)Z

    .line 27611
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35702(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;I)I

    .line 27612
    return-object v0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27540
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 27541
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27542
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27543
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27544
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    .line 27546
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27547
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27548
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27549
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27550
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27551
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    .line 27552
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27553
    return-object p0
.end method

.method public clearAccountValidated()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27945
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27946
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    .line 27948
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27766
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27768
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27769
    return-object p0
.end method

.method public clearContacts()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27849
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27852
    return-object p0
.end method

.method public clearFromServer()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27876
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27877
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    .line 27879
    return-object p0
.end method

.method public clearSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27900
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27901
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27903
    return-object p0
.end method

.method public clearSourcePage()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27924
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27925
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27927
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2

    .prologue
    .line 27557
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountValidated()Z
    .locals 1

    .prologue
    .line 27936
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 27735
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 27789
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 27786
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27783
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27524
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1

    .prologue
    .line 27561
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getFromServer()Z
    .locals 1

    .prologue
    .line 27867
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    return v0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1

    .prologue
    .line 27888
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method

.method public getSourcePage()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 1

    .prologue
    .line 27912
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    return-object v0
.end method

.method public hasAccountValidated()Z
    .locals 2

    .prologue
    .line 27933
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27732
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromServer()Z
    .locals 2

    .prologue
    .line 27864
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    .prologue
    .line 27885
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSourcePage()Z
    .locals 2

    .prologue
    .line 27909
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27646
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 27660
    :goto_0
    return v0

    .line 27650
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 27652
    goto :goto_0

    :cond_1
    move v0, v2

    .line 27654
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 27655
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 27657
    goto :goto_0

    .line 27654
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 27660
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 27754
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 27756
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27762
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27763
    return-object p0

    .line 27759
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27524
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27524
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27524
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27668
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 27669
    sparse-switch v0, :sswitch_data_0

    .line 27674
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 27676
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 27672
    goto :goto_1

    .line 27681
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 27682
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27683
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 27685
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 27686
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    goto :goto_0

    .line 27690
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    .line 27691
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 27692
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->addContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    goto :goto_0

    .line 27696
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27697
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    goto :goto_0

    .line 27701
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 27702
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    move-result-object v0

    .line 27703
    if-eqz v0, :cond_0

    .line 27704
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27705
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 27710
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 27711
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    move-result-object v0

    .line 27712
    if-eqz v0, :cond_0

    .line 27713
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27714
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    goto :goto_0

    .line 27719
    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27720
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    goto :goto_0

    .line 27669
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 27616
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 27642
    :goto_0
    return-object v0

    .line 27617
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 27618
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    .line 27620
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35200(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 27621
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 27622
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35200(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    .line 27623
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27630
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasFromServer()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 27631
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getFromServer()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->setFromServer(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    .line 27633
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27634
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    .line 27636
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasSourcePage()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 27637
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getSourcePage()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->setSourcePage(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    .line 27639
    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasAccountValidated()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 27640
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getAccountValidated()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->setAccountValidated(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    :cond_6
    move-object v0, p0

    .line 27642
    goto :goto_0

    .line 27625
    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27626
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->access$35200(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27855
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27856
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 27858
    return-object p0
.end method

.method public setAccountValidated(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27939
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27940
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->accountValidated_:Z

    .line 27942
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27748
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27750
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27751
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27738
    if-nez p1, :cond_0

    .line 27739
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27741
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27743
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27744
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 27803
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27804
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27806
    return-object p0
.end method

.method public setContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27793
    if-nez p2, :cond_0

    .line 27794
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27796
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->ensureContactsIsMutable()V

    .line 27797
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 27799
    return-object p0
.end method

.method public setFromServer(Z)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27870
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27871
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->fromServer_:Z

    .line 27873
    return-object p0
.end method

.method public setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27891
    if-nez p1, :cond_0

    .line 27892
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27894
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27895
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27897
    return-object p0
.end method

.method public setSourcePage(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27915
    if-nez p1, :cond_0

    .line 27916
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27918
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->bitField0_:I

    .line 27919
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27921
    return-object p0
.end method
