.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SourcePage"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage; = null

.field public static final enum FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage; = null

.field public static final FROM_CONTACT_PAGE_VALUE:I = 0x0

.field public static final enum FROM_DETAIL_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage; = null

.field public static final FROM_DETAIL_PAGE_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27246
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    const-string v1, "FROM_CONTACT_PAGE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27247
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    const-string v1, "FROM_DETAIL_PAGE"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_DETAIL_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27244
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_DETAIL_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27269
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 27278
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27279
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->value:I

    .line 27280
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27266
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 1
    .parameter

    .prologue
    .line 27257
    packed-switch p0, :pswitch_data_0

    .line 27260
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 27258
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    goto :goto_0

    .line 27259
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_DETAIL_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    goto :goto_0

    .line 27257
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 1
    .parameter

    .prologue
    .line 27244
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 1

    .prologue
    .line 27244
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 27254
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->value:I

    return v0
.end method
