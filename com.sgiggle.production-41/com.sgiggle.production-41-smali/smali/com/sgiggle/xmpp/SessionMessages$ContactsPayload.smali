.class public final Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ContactsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    }
.end annotation


# static fields
.field public static final ACCOUNT_VALIDATED_FIELD_NUMBER:I = 0x6

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONTACTS_FIELD_NUMBER:I = 0x2

.field public static final FROM_SERVER_FIELD_NUMBER:I = 0x3

.field public static final SOURCE_FIELD_NUMBER:I = 0x4

.field public static final SOURCE_PAGE_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;


# instance fields
.field private accountValidated_:Z

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private contacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private fromServer_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27955
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    .line 27956
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->initFields()V

    .line 27957
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 27231
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 27365
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    .line 27411
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedSerializedSize:I

    .line 27232
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 27233
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 27365
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    .line 27411
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedSerializedSize:I

    .line 27233
    return-void
.end method

.method static synthetic access$35102(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$35200(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 27226
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$35202(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$35302(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z

    return p1
.end method

.method static synthetic access$35402(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object p1
.end method

.method static synthetic access$35502(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    return-object p1
.end method

.method static synthetic access$35602(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z

    return p1
.end method

.method static synthetic access$35702(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27226
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1

    .prologue
    .line 27237
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 27358
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 27359
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    .line 27360
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z

    .line 27361
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 27362
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->FROM_CONTACT_PAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    .line 27363
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z

    .line 27364
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27517
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34900()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 27520
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27486
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    .line 27487
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27488
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    .line 27490
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27497
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    .line 27498
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 27499
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    .line 27501
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27453
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27459
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27507
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27513
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27475
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27481
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27464
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 27470
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;->access$34800(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountValidated()Z
    .locals 1

    .prologue
    .line 27354
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 27293
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1
    .parameter

    .prologue
    .line 27310
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getContactsCount()I
    .locals 1

    .prologue
    .line 27307
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27300
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public getContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 27314
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27304
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 27226
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;
    .locals 1

    .prologue
    .line 27241
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;

    return-object v0
.end method

.method public getFromServer()Z
    .locals 1

    .prologue
    .line 27324
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27413
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedSerializedSize:I

    .line 27414
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 27442
    :goto_0
    return v0

    .line 27417
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 27418
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 27421
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 27422
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 27421
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 27425
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_5

    .line 27426
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    .line 27429
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 27430
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->getNumber()I

    move-result v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27433
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    .line 27434
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 27437
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    .line 27438
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 27441
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1

    .prologue
    .line 27334
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method

.method public getSourcePage()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
    .locals 1

    .prologue
    .line 27344
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    return-object v0
.end method

.method public hasAccountValidated()Z
    .locals 2

    .prologue
    .line 27351
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27290
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromServer()Z
    .locals 2

    .prologue
    .line 27321
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    .prologue
    .line 27331
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSourcePage()Z
    .locals 2

    .prologue
    .line 27341
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27367
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    .line 27368
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 27385
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 27368
    goto :goto_0

    .line 27370
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 27371
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 27372
    goto :goto_0

    .line 27374
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 27375
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 27376
    goto :goto_0

    :cond_3
    move v0, v2

    .line 27378
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 27379
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 27380
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 27381
    goto :goto_0

    .line 27378
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 27384
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 27385
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27226
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27518
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 27226
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;
    .locals 1

    .prologue
    .line 27522
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;)Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 27447
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 27390
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->getSerializedSize()I

    .line 27391
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 27392
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 27394
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 27395
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->contacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 27394
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 27397
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 27398
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->fromServer_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 27400
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 27401
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->getNumber()I

    move-result v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 27403
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 27404
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->sourcePage_:Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 27406
    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    .line 27407
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload;->accountValidated_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 27409
    :cond_5
    return-void
.end method
