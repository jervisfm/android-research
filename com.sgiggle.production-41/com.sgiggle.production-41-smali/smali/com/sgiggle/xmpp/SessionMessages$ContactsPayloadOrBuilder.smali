.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ContactsPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContactsPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountValidated()Z
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getContactsCount()I
.end method

.method public abstract getContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getFromServer()Z
.end method

.method public abstract getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
.end method

.method public abstract getSourcePage()Lcom/sgiggle/xmpp/SessionMessages$ContactsPayload$SourcePage;
.end method

.method public abstract hasAccountValidated()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasFromServer()Z
.end method

.method public abstract hasSource()Z
.end method

.method public abstract hasSourcePage()Z
.end method
