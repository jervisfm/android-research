.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContactsSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final enum ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final ADDRESS_BOOK_AND_CONTACT_FILTER_VALUE:I = 0x2

.field public static final enum ADDRESS_BOOK_AND_LOCAL_STORAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final ADDRESS_BOOK_AND_LOCAL_STORAGE_VALUE:I = 0x1

.field public static final enum CLOUD_ADDRESS_BOOK_SYNC:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final CLOUD_ADDRESS_BOOK_SYNC_VALUE:I = 0x3

.field public static final enum CLOUD_REVERSE_LOOKUP:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final CLOUD_REVERSE_LOOKUP_VALUE:I = 0x4

.field public static final enum CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource; = null

.field public static final CONTACTS_SOURCE_UNKNOWN_VALUE:I = -0x1

.field public static final enum LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

.field public static final LOCAL_STORAGE_ONLY_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 154
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "CONTACTS_SOURCE_UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 155
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "LOCAL_STORAGE_ONLY"

    invoke-direct {v0, v1, v5, v5, v4}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 156
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "ADDRESS_BOOK_AND_LOCAL_STORAGE"

    invoke-direct {v0, v1, v6, v6, v5}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_LOCAL_STORAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 157
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "ADDRESS_BOOK_AND_CONTACT_FILTER"

    invoke-direct {v0, v1, v7, v7, v6}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 158
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "CLOUD_ADDRESS_BOOK_SYNC"

    invoke-direct {v0, v1, v8, v8, v7}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_ADDRESS_BOOK_SYNC:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 159
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    const-string v1, "CLOUD_REVERSE_LOOKUP"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_REVERSE_LOOKUP:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 152
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_LOCAL_STORAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_ADDRESS_BOOK_SYNC:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_REVERSE_LOOKUP:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 189
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 199
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->value:I

    .line 200
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1
    .parameter

    .prologue
    .line 173
    packed-switch p0, :pswitch_data_0

    .line 180
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 175
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->LOCAL_STORAGE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 176
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_LOCAL_STORAGE:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 177
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->ADDRESS_BOOK_AND_CONTACT_FILTER:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 178
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_ADDRESS_BOOK_SYNC:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 179
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CLOUD_REVERSE_LOOKUP:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1
    .parameter

    .prologue
    .line 152
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->value:I

    return v0
.end method
