.class public final Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private assetId_:J

.field private assetPath_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private restart_:Z

.field private seed_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38517
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 38689
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38795
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38518
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->maybeForceBuilderInitialization()V

    .line 38519
    return-void
.end method

.method static synthetic access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38512
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$48900()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38560
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    .line 38561
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38562
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 38565
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38524
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 38522
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 2

    .prologue
    .line 38551
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    .line 38552
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 38553
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 38555
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 5

    .prologue
    .line 38569
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 38570
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38571
    const/4 v2, 0x0

    .line 38572
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 38573
    or-int/lit8 v2, v2, 0x1

    .line 38575
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49102(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38576
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 38577
    or-int/lit8 v2, v2, 0x2

    .line 38579
    :cond_1
    iget-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J
    invoke-static {v0, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49202(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;J)J

    .line 38580
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 38581
    or-int/lit8 v2, v2, 0x4

    .line 38583
    :cond_2
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49302(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;I)I

    .line 38584
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 38585
    or-int/lit8 v2, v2, 0x8

    .line 38587
    :cond_3
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49402(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Z)Z

    .line 38588
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 38589
    or-int/lit8 v1, v2, 0x10

    .line 38591
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49502(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38592
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->access$49602(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;I)I

    .line 38593
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38528
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 38529
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38530
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38531
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    .line 38532
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38533
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    .line 38534
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38535
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    .line 38536
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38537
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38538
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38539
    return-object p0
.end method

.method public clearAssetId()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 2

    .prologue
    .line 38746
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38747
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    .line 38749
    return-object p0
.end method

.method public clearAssetPath()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38819
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38820
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38822
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38725
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38727
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38728
    return-object p0
.end method

.method public clearRestart()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38789
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    .line 38791
    return-object p0
.end method

.method public clearSeed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38768
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    .line 38770
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 2

    .prologue
    .line 38543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAssetId()J
    .locals 2

    .prologue
    .line 38737
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    return-wide v0
.end method

.method public getAssetPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38800
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38801
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 38802
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 38803
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38806
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 38694
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 38512
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1

    .prologue
    .line 38547
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getRestart()Z
    .locals 1

    .prologue
    .line 38779
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    return v0
.end method

.method public getSeed()I
    .locals 1

    .prologue
    .line 38758
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    return v0
.end method

.method public hasAssetId()Z
    .locals 2

    .prologue
    .line 38734
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAssetPath()Z
    .locals 2

    .prologue
    .line 38797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38691
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRestart()Z
    .locals 2

    .prologue
    .line 38776
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSeed()Z
    .locals 2

    .prologue
    .line 38755
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38617
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 38633
    :goto_0
    return v0

    .line 38621
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->hasAssetId()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 38623
    goto :goto_0

    .line 38625
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->hasSeed()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 38627
    goto :goto_0

    .line 38629
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 38631
    goto :goto_0

    .line 38633
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 38713
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 38715
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38721
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38722
    return-object p0

    .line 38718
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38512
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38512
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38512
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38641
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 38642
    sparse-switch v0, :sswitch_data_0

    .line 38647
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 38649
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 38645
    goto :goto_1

    .line 38654
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 38655
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 38656
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 38658
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 38659
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    goto :goto_0

    .line 38663
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38664
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    goto :goto_0

    .line 38668
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38669
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    goto :goto_0

    .line 38673
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38674
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    goto :goto_0

    .line 38678
    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38679
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    goto :goto_0

    .line 38642
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 38597
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 38613
    :goto_0
    return-object v0

    .line 38598
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38599
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    .line 38601
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasAssetId()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38602
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->setAssetId(J)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    .line 38604
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasSeed()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 38605
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getSeed()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->setSeed(I)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    .line 38607
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasRestart()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 38608
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getRestart()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->setRestart(Z)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    .line 38610
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasAssetPath()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 38611
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->setAssetPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    :cond_5
    move-object v0, p0

    .line 38613
    goto :goto_0
.end method

.method public setAssetId(J)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38740
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38741
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetId_:J

    .line 38743
    return-object p0
.end method

.method public setAssetPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38810
    if-nez p1, :cond_0

    .line 38811
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38813
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38814
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38816
    return-object p0
.end method

.method setAssetPath(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 38825
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38826
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->assetPath_:Ljava/lang/Object;

    .line 38828
    return-void
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38707
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38709
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38710
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38697
    if-nez p1, :cond_0

    .line 38698
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38700
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38702
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38703
    return-object p0
.end method

.method public setRestart(Z)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38782
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38783
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->restart_:Z

    .line 38785
    return-object p0
.end method

.method public setSeed(I)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38761
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->bitField0_:I

    .line 38762
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->seed_:I

    .line 38764
    return-object p0
.end method
