.class public final Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ControlAnimationPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    }
.end annotation


# static fields
.field public static final ASSET_ID_FIELD_NUMBER:I = 0x2

.field public static final ASSET_PATH_FIELD_NUMBER:I = 0x5

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final RESTART_FIELD_NUMBER:I = 0x4

.field public static final SEED_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;


# instance fields
.field private assetId_:J

.field private assetPath_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private restart_:Z

.field private seed_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38834
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    .line 38835
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->initFields()V

    .line 38836
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 38265
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 38358
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    .line 38403
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedSerializedSize:I

    .line 38266
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 38267
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 38358
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    .line 38403
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedSerializedSize:I

    .line 38267
    return-void
.end method

.method static synthetic access$49102(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$49202(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;J)J
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J

    return-wide p1
.end method

.method static synthetic access$49302(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I

    return p1
.end method

.method static synthetic access$49402(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z

    return p1
.end method

.method static synthetic access$49502(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$49602(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38260
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    return p1
.end method

.method private getAssetPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 38340
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    .line 38341
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 38342
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 38344
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    .line 38347
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1

    .prologue
    .line 38271
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 38352
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 38353
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J

    .line 38354
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I

    .line 38355
    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z

    .line 38356
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    .line 38357
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38505
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48900()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 38508
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38474
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    .line 38475
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38476
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    .line 38478
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38485
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    .line 38486
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38487
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    .line 38489
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38441
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38447
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38495
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38501
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38463
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38469
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38452
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 38458
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;->access$48800(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAssetId()J
    .locals 2

    .prologue
    .line 38296
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J

    return-wide v0
.end method

.method public getAssetPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 38326
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    .line 38327
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 38328
    check-cast v0, Ljava/lang/String;

    .line 38336
    :goto_0
    return-object v0

    .line 38330
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 38332
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 38333
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38334
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetPath_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 38336
    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 38286
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 38260
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;
    .locals 1

    .prologue
    .line 38275
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;

    return-object v0
.end method

.method public getRestart()Z
    .locals 1

    .prologue
    .line 38316
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z

    return v0
.end method

.method public getSeed()I
    .locals 1

    .prologue
    .line 38306
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 38405
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedSerializedSize:I

    .line 38406
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 38430
    :goto_0
    return v0

    .line 38408
    :cond_0
    const/4 v0, 0x0

    .line 38409
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 38410
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38413
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 38414
    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J

    invoke-static {v3, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 38417
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 38418
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 38421
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 38422
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 38425
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 38426
    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 38429
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasAssetId()Z
    .locals 2

    .prologue
    .line 38293
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAssetPath()Z
    .locals 2

    .prologue
    .line 38323
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38283
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRestart()Z
    .locals 2

    .prologue
    .line 38313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSeed()Z
    .locals 2

    .prologue
    .line 38303
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38360
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    .line 38361
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 38380
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 38361
    goto :goto_0

    .line 38363
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 38364
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38365
    goto :goto_0

    .line 38367
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasAssetId()Z

    move-result v0

    if-nez v0, :cond_3

    .line 38368
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38369
    goto :goto_0

    .line 38371
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->hasSeed()Z

    move-result v0

    if-nez v0, :cond_4

    .line 38372
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38373
    goto :goto_0

    .line 38375
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 38376
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 38377
    goto :goto_0

    .line 38379
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 38380
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38260
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38506
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 38260
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;
    .locals 1

    .prologue
    .line 38510
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 38435
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 38385
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getSerializedSize()I

    .line 38386
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 38387
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 38389
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 38390
    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->assetId_:J

    invoke-virtual {p1, v2, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt64(IJ)V

    .line 38392
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 38393
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->seed_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    .line 38395
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 38396
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->restart_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 38398
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 38399
    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayload;->getAssetPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 38401
    :cond_4
    return-void
.end method
