.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ControlAnimationPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ControlAnimationPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAssetId()J
.end method

.method public abstract getAssetPath()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getRestart()Z
.end method

.method public abstract getSeed()I
.end method

.method public abstract hasAssetId()Z
.end method

.method public abstract hasAssetPath()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasRestart()Z
.end method

.method public abstract hasSeed()Z
.end method
