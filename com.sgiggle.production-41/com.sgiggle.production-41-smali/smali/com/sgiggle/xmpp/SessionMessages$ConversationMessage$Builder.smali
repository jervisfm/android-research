.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private bitField1_:I

.field private channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

.field private conversationId_:Ljava/lang/Object;

.field private duration_:I

.field private height_:I

.field private isEcard_:Z

.field private isForUpdate_:Z

.field private isForwaredMessage_:Z

.field private isFromMe_:Z

.field private isOfflineMessage_:Z

.field private loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

.field private mediaId_:Ljava/lang/Object;

.field private mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

.field private messageId_:I

.field private originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

.field private path_:Ljava/lang/Object;

.field private peerCapHash_:Ljava/lang/Object;

.field private peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

.field private progress_:I

.field private read_:Z

.field private recorderAblePlayback_:Z

.field private robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

.field private sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

.field private senderJid_:Ljava/lang/Object;

.field private senderMsgId_:Ljava/lang/Object;

.field private serverShareId_:Ljava/lang/Object;

.field private shouldVideoBeTrimed_:Z

.field private size_:I

.field private textIfNotSupport_:Ljava/lang/Object;

.field private text_:Ljava/lang/Object;

.field private thumbnailPath_:Ljava/lang/Object;

.field private thumbnailUrl_:Ljava/lang/Object;

.field private timeCreated_:J

.field private timePeerRead_:J

.field private timeSend_:J

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

.field private url_:Ljava/lang/Object;

.field private vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

.field private videoRotation_:I

.field private videoTrimmerEndTimestamp_:I

.field private videoTrimmerStartTimestamp_:I

.field private webPageUrl_:Ljava/lang/Object;

.field private width_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->MEDIA_SOURCE_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->ROBOT_MESSAGE_NONE:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$122300()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 12

    const/high16 v11, 0x4

    const/high16 v10, 0x2

    const/high16 v9, 0x1

    const v8, 0x8000

    const/high16 v7, -0x8000

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v3, 0x0

    const/4 v4, 0x0

    and-int/lit8 v5, v1, 0x1

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    or-int/lit8 v3, v3, 0x1

    :cond_0
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$122502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v5, v1, 0x2

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    or-int/lit8 v3, v3, 0x2

    :cond_1
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$122602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    and-int/lit8 v5, v1, 0x4

    const/4 v6, 0x4

    if-ne v5, v6, :cond_2

    or-int/lit8 v3, v3, 0x4

    :cond_2
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$122702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v5, v1, 0x8

    const/16 v6, 0x8

    if-ne v5, v6, :cond_3

    or-int/lit8 v3, v3, 0x8

    :cond_3
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$122802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v5, v1, 0x10

    const/16 v6, 0x10

    if-ne v5, v6, :cond_4

    or-int/lit8 v3, v3, 0x10

    :cond_4
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$122902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v5, v1, 0x20

    const/16 v6, 0x20

    if-ne v5, v6, :cond_5

    or-int/lit8 v3, v3, 0x20

    :cond_5
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v5, v1, 0x40

    const/16 v6, 0x40

    if-ne v5, v6, :cond_6

    or-int/lit8 v3, v3, 0x40

    :cond_6
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v5, v1, 0x80

    const/16 v6, 0x80

    if-ne v5, v6, :cond_7

    or-int/lit16 v3, v3, 0x80

    :cond_7
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int/lit16 v5, v1, 0x100

    const/16 v6, 0x100

    if-ne v5, v6, :cond_8

    or-int/lit16 v3, v3, 0x100

    :cond_8
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int/lit16 v5, v1, 0x200

    const/16 v6, 0x200

    if-ne v5, v6, :cond_9

    or-int/lit16 v3, v3, 0x200

    :cond_9
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v5, v1, 0x400

    const/16 v6, 0x400

    if-ne v5, v6, :cond_a

    or-int/lit16 v3, v3, 0x400

    :cond_a
    iget-boolean v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    and-int/lit16 v5, v1, 0x800

    const/16 v6, 0x800

    if-ne v5, v6, :cond_b

    or-int/lit16 v3, v3, 0x800

    :cond_b
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    and-int/lit16 v5, v1, 0x1000

    const/16 v6, 0x1000

    if-ne v5, v6, :cond_c

    or-int/lit16 v3, v3, 0x1000

    :cond_c
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    and-int/lit16 v5, v1, 0x2000

    const/16 v6, 0x2000

    if-ne v5, v6, :cond_d

    or-int/lit16 v3, v3, 0x2000

    :cond_d
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v5, v1, 0x4000

    const/16 v6, 0x4000

    if-ne v5, v6, :cond_e

    or-int/lit16 v3, v3, 0x4000

    :cond_e
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$123902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    and-int v5, v1, v8

    if-ne v5, v8, :cond_f

    or-int/2addr v3, v8

    :cond_f
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int v5, v1, v9

    if-ne v5, v9, :cond_10

    or-int/2addr v3, v9

    :cond_10
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int v5, v1, v10

    if-ne v5, v10, :cond_11

    or-int/2addr v3, v10

    :cond_11
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int v5, v1, v11

    if-ne v5, v11, :cond_12

    or-int/2addr v3, v11

    :cond_12
    iget-boolean v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    const/high16 v5, 0x8

    and-int/2addr v5, v1

    const/high16 v6, 0x8

    if-ne v5, v6, :cond_13

    const/high16 v5, 0x8

    or-int/2addr v3, v5

    :cond_13
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    const/high16 v5, 0x10

    and-int/2addr v5, v1

    const/high16 v6, 0x10

    if-ne v5, v6, :cond_14

    const/high16 v5, 0x10

    or-int/2addr v3, v5

    :cond_14
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    const/high16 v5, 0x20

    and-int/2addr v5, v1

    const/high16 v6, 0x20

    if-ne v5, v6, :cond_15

    const/high16 v5, 0x20

    or-int/2addr v3, v5

    :cond_15
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;)Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    const/high16 v5, 0x40

    and-int/2addr v5, v1

    const/high16 v6, 0x40

    if-ne v5, v6, :cond_16

    const/high16 v5, 0x40

    or-int/2addr v3, v5

    :cond_16
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/high16 v5, 0x80

    and-int/2addr v5, v1

    const/high16 v6, 0x80

    if-ne v5, v6, :cond_17

    const/high16 v5, 0x80

    or-int/2addr v3, v5

    :cond_17
    iget-wide v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J
    invoke-static {v0, v5, v6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J

    const/high16 v5, 0x100

    and-int/2addr v5, v1

    const/high16 v6, 0x100

    if-ne v5, v6, :cond_18

    const/high16 v5, 0x100

    or-int/2addr v3, v5

    :cond_18
    iget v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$124902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    const/high16 v5, 0x200

    and-int/2addr v5, v1

    const/high16 v6, 0x200

    if-ne v5, v6, :cond_19

    const/high16 v5, 0x200

    or-int/2addr v3, v5

    :cond_19
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const/high16 v5, 0x400

    and-int/2addr v5, v1

    const/high16 v6, 0x400

    if-ne v5, v6, :cond_1a

    const/high16 v5, 0x400

    or-int/2addr v3, v5

    :cond_1a
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    const/high16 v5, 0x800

    and-int/2addr v5, v1

    const/high16 v6, 0x800

    if-ne v5, v6, :cond_1b

    const/high16 v5, 0x800

    or-int/2addr v3, v5

    :cond_1b
    iget-boolean v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    const/high16 v5, 0x1000

    and-int/2addr v5, v1

    const/high16 v6, 0x1000

    if-ne v5, v6, :cond_1c

    const/high16 v5, 0x1000

    or-int/2addr v3, v5

    :cond_1c
    iget-boolean v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    const/high16 v5, 0x2000

    and-int/2addr v5, v1

    const/high16 v6, 0x2000

    if-ne v5, v6, :cond_1d

    const/high16 v5, 0x2000

    or-int/2addr v3, v5

    :cond_1d
    iget-object v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    invoke-static {v0, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const/high16 v5, 0x4000

    and-int/2addr v5, v1

    const/high16 v6, 0x4000

    if-ne v5, v6, :cond_1e

    const/high16 v5, 0x4000

    or-int/2addr v3, v5

    :cond_1e
    iget-wide v5, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J
    invoke-static {v0, v5, v6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_2a

    or-int v1, v3, v7

    :goto_0
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int/lit8 v3, v2, 0x1

    const/4 v5, 0x1

    if-ne v3, v5, :cond_29

    or-int/lit8 v3, v4, 0x1

    :goto_1
    iget-wide v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J
    invoke-static {v0, v4, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J

    and-int/lit8 v4, v2, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1f

    or-int/lit8 v3, v3, 0x2

    :cond_1f
    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    and-int/lit8 v4, v2, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_20

    or-int/lit8 v3, v3, 0x4

    :cond_20
    iget-boolean v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$125902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    and-int/lit8 v4, v2, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_21

    or-int/lit8 v3, v3, 0x8

    :cond_21
    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v4, v2, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_22

    or-int/lit8 v3, v3, 0x10

    :cond_22
    iget-boolean v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    and-int/lit8 v4, v2, 0x20

    const/16 v5, 0x20

    if-ne v4, v5, :cond_23

    or-int/lit8 v3, v3, 0x20

    :cond_23
    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v4, v2, 0x40

    const/16 v5, 0x40

    if-ne v4, v5, :cond_24

    or-int/lit8 v3, v3, 0x40

    :cond_24
    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v4, v2, 0x80

    const/16 v5, 0x80

    if-ne v4, v5, :cond_25

    or-int/lit16 v3, v3, 0x80

    :cond_25
    iget-object v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit16 v4, v2, 0x100

    const/16 v5, 0x100

    if-ne v4, v5, :cond_26

    or-int/lit16 v3, v3, 0x100

    :cond_26
    iget-boolean v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    and-int/lit16 v4, v2, 0x200

    const/16 v5, 0x200

    if-ne v4, v5, :cond_27

    or-int/lit16 v3, v3, 0x200

    :cond_27
    iget v4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I
    invoke-static {v0, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    and-int/lit16 v2, v2, 0x400

    const/16 v4, 0x400

    if-ne v2, v4, :cond_28

    or-int/lit16 v2, v3, 0x400

    :goto_2
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->access$126902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I

    return-object v0

    :cond_28
    move v2, v3

    goto :goto_2

    :cond_29
    move v3, v4

    goto/16 :goto_1

    :cond_2a
    move v1, v3

    goto/16 :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->MEDIA_SOURCE_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->ROBOT_MESSAGE_NONE:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x20000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x40000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-wide v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    return-object p0
.end method

.method public clearChannel()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object p0
.end method

.method public clearConversationId()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearDuration()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    return-object p0
.end method

.method public clearHeight()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    return-object p0
.end method

.method public clearIsEcard()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    return-object p0
.end method

.method public clearIsForUpdate()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x10000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    return-object p0
.end method

.method public clearIsForwaredMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    return-object p0
.end method

.method public clearIsFromMe()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x8000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    return-object p0
.end method

.method public clearIsOfflineMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    return-object p0
.end method

.method public clearLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x20000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object p0
.end method

.method public clearMediaId()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearMediaSourceType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x200001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->MEDIA_SOURCE_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    return-object p0
.end method

.method public clearMessageId()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x1000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    return-object p0
.end method

.method public clearOriginalType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p0
.end method

.method public clearPath()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearPeer()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x400001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearPeerCapHash()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeerCapHash()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearProduct()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearProgress()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    return-object p0
.end method

.method public clearRead()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    return-object p0
.end method

.method public clearRecorderAblePlayback()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    return-object p0
.end method

.method public clearRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x4000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->ROBOT_MESSAGE_NONE:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    return-object p0
.end method

.method public clearSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x2000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object p0
.end method

.method public clearSenderJid()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderJid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearSenderMsgId()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderMsgId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearServerShareId()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getServerShareId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearShouldVideoBeTrimed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x40001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    return-object p0
.end method

.method public clearSize()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    return-object p0
.end method

.method public clearText()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearTextIfNotSupport()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextIfNotSupport()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearThumbnailPath()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearThumbnailUrl()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearTimeCreated()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x40000001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    return-object p0
.end method

.method public clearTimePeerRead()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    return-object p0
.end method

.method public clearTimeSend()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x800001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p0
.end method

.method public clearUrl()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearVideoRotation()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    return-object p0
.end method

.method public clearVideoTrimmerEndTimestamp()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x100001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    return-object p0
.end method

.method public clearVideoTrimmerStartTimestamp()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x80001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    return-object p0
.end method

.method public clearWebPageUrl()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getWebPageUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearWidth()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getChannel()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    return v0
.end method

.method public getIsEcard()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    return v0
.end method

.method public getIsForUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    return v0
.end method

.method public getIsForwaredMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    return v0
.end method

.method public getIsFromMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    return v0
.end method

.method public getIsOfflineMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    return v0
.end method

.method public getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object v0
.end method

.method public getMediaId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getMediaSourceType()Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    return-object v0
.end method

.method public getMessageId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    return v0
.end method

.method public getOriginalType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getPeerCapHash()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    return v0
.end method

.method public getRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    return v0
.end method

.method public getRecorderAblePlayback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    return v0
.end method

.method public getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    return-object v0
.end method

.method public getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method

.method public getSenderJid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getSenderMsgId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getServerShareId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getShouldVideoBeTrimed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    return v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTextIfNotSupport()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getThumbnailPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getTimeCreated()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    return-wide v0
.end method

.method public getTimePeerRead()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    return-wide v0
.end method

.method public getTimeSend()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    return-wide v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVideoRotation()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    return v0
.end method

.method public getVideoTrimmerEndTimestamp()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    return v0
.end method

.method public getVideoTrimmerStartTimestamp()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    return v0
.end method

.method public getWebPageUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    return v0
.end method

.method public hasChannel()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasConversationId()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeight()Z
    .locals 2

    const/high16 v1, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsEcard()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsForUpdate()Z
    .locals 2

    const/high16 v1, 0x1000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsForwaredMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsFromMe()Z
    .locals 2

    const/high16 v1, 0x800

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsOfflineMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLoadingStatus()Z
    .locals 2

    const/high16 v1, 0x2000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaSourceType()Z
    .locals 2

    const/high16 v1, 0x20

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageId()Z
    .locals 2

    const/high16 v1, 0x100

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOriginalType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPath()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    const/high16 v1, 0x40

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerCapHash()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProduct()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProgress()Z
    .locals 2

    const/high16 v1, -0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRead()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecorderAblePlayback()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRobotMessageType()Z
    .locals 2

    const/high16 v1, 0x400

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendStatus()Z
    .locals 2

    const/high16 v1, 0x200

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSenderJid()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSenderMsgId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasServerShareId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShouldVideoBeTrimed()Z
    .locals 2

    const/high16 v1, 0x4

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTextIfNotSupport()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnailPath()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnailUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    const/high16 v1, 0x4000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimePeerRead()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeSend()Z
    .locals 2

    const/high16 v1, 0x80

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodBundle()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRotation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoTrimmerEndTimestamp()Z
    .locals 2

    const/high16 v1, 0x10

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoTrimmerStartTimestamp()Z
    .locals 2

    const/high16 v1, 0x8

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWebPageUrl()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWidth()Z
    .locals 2

    const/high16 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasConversationId()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasProduct()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    goto :goto_0

    :sswitch_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    goto :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    goto/16 :goto_0

    :sswitch_b
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasVgoodBundle()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    goto/16 :goto_0

    :sswitch_c
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasProduct()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setProduct(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    goto/16 :goto_0

    :sswitch_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x4000

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto/16 :goto_0

    :sswitch_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    goto/16 :goto_0

    :sswitch_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    goto/16 :goto_0

    :sswitch_13
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    goto/16 :goto_0

    :sswitch_14
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->hasPeer()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    goto/16 :goto_0

    :sswitch_15
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    goto/16 :goto_0

    :sswitch_16
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    goto/16 :goto_0

    :sswitch_17
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v2, 0x200

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto/16 :goto_0

    :sswitch_18
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v2, 0x400

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    goto/16 :goto_0

    :sswitch_19
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    goto/16 :goto_0

    :sswitch_1a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    goto/16 :goto_0

    :sswitch_1b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v2, 0x2000

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto/16 :goto_0

    :sswitch_1c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x4000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    goto/16 :goto_0

    :sswitch_1d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, -0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    goto/16 :goto_0

    :sswitch_1e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt64()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    goto/16 :goto_0

    :sswitch_1f
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    goto/16 :goto_0

    :sswitch_20
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    goto/16 :goto_0

    :sswitch_21
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_22
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    goto/16 :goto_0

    :sswitch_23
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_24
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_25
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    goto/16 :goto_0

    :sswitch_26
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    goto/16 :goto_0

    :sswitch_27
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    goto/16 :goto_0

    :sswitch_28
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    goto/16 :goto_0

    :sswitch_29
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    goto/16 :goto_0

    :sswitch_2a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    goto/16 :goto_0

    :sswitch_2b
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v2, 0x20

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x52 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x98 -> :sswitch_12
        0xa0 -> :sswitch_13
        0xaa -> :sswitch_14
        0xb0 -> :sswitch_15
        0xb8 -> :sswitch_16
        0xc0 -> :sswitch_17
        0xc8 -> :sswitch_18
        0xd0 -> :sswitch_19
        0xd8 -> :sswitch_1a
        0xe0 -> :sswitch_1b
        0xe8 -> :sswitch_1c
        0xf0 -> :sswitch_1d
        0xf8 -> :sswitch_1e
        0x148 -> :sswitch_1f
        0x150 -> :sswitch_20
        0x15a -> :sswitch_21
        0x160 -> :sswitch_22
        0x16a -> :sswitch_23
        0x172 -> :sswitch_24
        0x17a -> :sswitch_25
        0x180 -> :sswitch_26
        0x188 -> :sswitch_27
        0x190 -> :sswitch_28
        0x1e8 -> :sswitch_29
        0x1f0 -> :sswitch_2a
        0x1f8 -> :sswitch_2b
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasConversationId()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasText()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasPath()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasThumbnailUrl()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setThumbnailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasThumbnailPath()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setThumbnailPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_7
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasSize()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSize()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSize(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_8
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasDuration()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDuration()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_9
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasMediaId()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMediaId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasIsEcard()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsEcard()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setIsEcard(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasVgoodBundle()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProduct()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeProduct(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTextIfNotSupport()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextIfNotSupport()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasOriginalType()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getOriginalType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setOriginalType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasWebPageUrl()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getWebPageUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setWebPageUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_10
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasWidth()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getWidth()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setWidth(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_11
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasHeight()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getHeight()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setHeight(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_12
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasShouldVideoBeTrimed()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getShouldVideoBeTrimed()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setShouldVideoBeTrimed(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_13
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasVideoTrimmerStartTimestamp()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVideoTrimmerStartTimestamp()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setVideoTrimmerStartTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_14
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasVideoTrimmerEndTimestamp()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVideoTrimmerEndTimestamp()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setVideoTrimmerEndTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_15
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasMediaSourceType()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaSourceType()Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMediaSourceType(Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_16
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergePeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_17
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimeSend()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeSend()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTimeSend(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_18
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasMessageId()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMessageId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_19
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasSendStatus()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSendStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRobotMessageType()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setRobotMessageType(Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasIsFromMe()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsFromMe()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setIsFromMe(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasIsForUpdate()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsForUpdate()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setIsForUpdate(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasLoadingStatus()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimeCreated()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimeCreated()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_1f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProgress()Z

    move-result v0

    if-eqz v0, :cond_20

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProgress()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setProgress(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_20
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasTimePeerRead()Z

    move-result v0

    if-eqz v0, :cond_21

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTimePeerRead()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setTimePeerRead(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_21
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasChannel()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getChannel()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setChannel(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_22
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRead()Z

    move-result v0

    if-eqz v0, :cond_23

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRead()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setRead(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_23
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasServerShareId()Z

    move-result v0

    if-eqz v0, :cond_24

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getServerShareId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setServerShareId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_24
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasIsOfflineMessage()Z

    move-result v0

    if-eqz v0, :cond_25

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsOfflineMessage()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setIsOfflineMessage(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_25
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasSenderMsgId()Z

    move-result v0

    if-eqz v0, :cond_26

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderMsgId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSenderMsgId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_26
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasSenderJid()Z

    move-result v0

    if-eqz v0, :cond_27

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setSenderJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_27
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasPeerCapHash()Z

    move-result v0

    if-eqz v0, :cond_28

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeerCapHash()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setPeerCapHash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_28
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasIsForwaredMessage()Z

    move-result v0

    if-eqz v0, :cond_29

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getIsForwaredMessage()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setIsForwaredMessage(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_29
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasVideoRotation()Z

    move-result v0

    if-eqz v0, :cond_2a

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getVideoRotation()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setVideoRotation(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_2a
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasRecorderAblePlayback()Z

    move-result v0

    if-eqz v0, :cond_2b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getRecorderAblePlayback()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->setRecorderAblePlayback(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_2b
    move-object v0, p0

    goto/16 :goto_0
.end method

.method public mergePeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 3

    const/high16 v2, 0x40

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/2addr v0, v2

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public mergeProduct(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    goto :goto_0
.end method

.method public mergeVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    goto :goto_0
.end method

.method public setChannel(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object p0
.end method

.method public setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method setConversationId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->conversationId_:Ljava/lang/Object;

    return-void
.end method

.method public setDuration(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->duration_:I

    return-object p0
.end method

.method public setHeight(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->height_:I

    return-object p0
.end method

.method public setIsEcard(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isEcard_:Z

    return-object p0
.end method

.method public setIsForUpdate(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x1000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForUpdate_:Z

    return-object p0
.end method

.method public setIsForwaredMessage(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isForwaredMessage_:Z

    return-object p0
.end method

.method public setIsFromMe(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x800

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isFromMe_:Z

    return-object p0
.end method

.method public setIsOfflineMessage(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->isOfflineMessage_:Z

    return-object p0
.end method

.method public setLoadingStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x2000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object p0
.end method

.method public setMediaId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    return-object p0
.end method

.method setMediaId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaId_:Ljava/lang/Object;

    return-void
.end method

.method public setMediaSourceType(Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x20

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    return-object p0
.end method

.method public setMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x100

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->messageId_:I

    return-object p0
.end method

.method public setOriginalType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p0
.end method

.method public setPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    return-object p0
.end method

.method setPath(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->path_:Ljava/lang/Object;

    return-void
.end method

.method public setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x40

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPeerCapHash(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    return-object p0
.end method

.method setPeerCapHash(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->peerCapHash_:Ljava/lang/Object;

    return-void
.end method

.method public setProduct(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setProduct(Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setProgress(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, -0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->progress_:I

    return-object p0
.end method

.method public setRead(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->read_:Z

    return-object p0
.end method

.method public setRecorderAblePlayback(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->recorderAblePlayback_:Z

    return-object p0
.end method

.method public setRobotMessageType(Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x400

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    return-object p0
.end method

.method public setSendStatus(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x200

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object p0
.end method

.method public setSenderJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    return-object p0
.end method

.method setSenderJid(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderJid_:Ljava/lang/Object;

    return-void
.end method

.method public setSenderMsgId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    return-object p0
.end method

.method setSenderMsgId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->senderMsgId_:Ljava/lang/Object;

    return-void
.end method

.method public setServerShareId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    return-object p0
.end method

.method setServerShareId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->serverShareId_:Ljava/lang/Object;

    return-void
.end method

.method public setShouldVideoBeTrimed(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x4

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->shouldVideoBeTrimed_:Z

    return-object p0
.end method

.method public setSize(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->size_:I

    return-object p0
.end method

.method public setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    return-object p0
.end method

.method setText(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->text_:Ljava/lang/Object;

    return-void
.end method

.method public setTextIfNotSupport(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    return-object p0
.end method

.method setTextIfNotSupport(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->textIfNotSupport_:Ljava/lang/Object;

    return-void
.end method

.method public setThumbnailPath(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    return-object p0
.end method

.method setThumbnailPath(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailPath_:Ljava/lang/Object;

    return-void
.end method

.method public setThumbnailUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method setThumbnailUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->thumbnailUrl_:Ljava/lang/Object;

    return-void
.end method

.method public setTimeCreated(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x4000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeCreated_:J

    return-object p0
.end method

.method public setTimePeerRead(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timePeerRead_:J

    return-object p0
.end method

.method public setTimeSend(J)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x80

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->timeSend_:J

    return-object p0
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p0
.end method

.method public setUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    return-object p0
.end method

.method setUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->url_:Ljava/lang/Object;

    return-void
.end method

.method public setVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    return-object p0
.end method

.method public setVideoRotation(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField1_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoRotation_:I

    return-object p0
.end method

.method public setVideoTrimmerEndTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x10

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerEndTimestamp_:I

    return-object p0
.end method

.method public setVideoTrimmerStartTimestamp(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x8

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->videoTrimmerStartTimestamp_:I

    return-object p0
.end method

.method public setWebPageUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method setWebPageUrl(Lcom/google/protobuf/ByteString;)V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->webPageUrl_:Ljava/lang/Object;

    return-void
.end method

.method public setWidth(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->width_:I

    return-object p0
.end method
