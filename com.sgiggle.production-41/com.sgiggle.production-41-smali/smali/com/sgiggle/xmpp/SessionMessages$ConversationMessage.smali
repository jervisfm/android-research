.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationMessage"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    }
.end annotation


# static fields
.field public static final CHANNEL_FIELD_NUMBER:I = 0x29

.field public static final CONVERSATION_ID_FIELD_NUMBER:I = 0x2

.field public static final DURATION_FIELD_NUMBER:I = 0x9

.field public static final HEIGHT_FIELD_NUMBER:I = 0x13

.field public static final IS_ECARD_FIELD_NUMBER:I = 0xb

.field public static final IS_FORWARED_MESSAGE_FIELD_NUMBER:I = 0x30

.field public static final IS_FOR_UPDATE_FIELD_NUMBER:I = 0x1b

.field public static final IS_FROM_ME_FIELD_NUMBER:I = 0x1a

.field public static final IS_OFFLINE_MESSAGE_FIELD_NUMBER:I = 0x2c

.field public static final LOADING_STATUS_FIELD_NUMBER:I = 0x1c

.field public static final MEDIA_ID_FIELD_NUMBER:I = 0xa

.field public static final MEDIA_SOURCE_TYPE_FIELD_NUMBER:I = 0x3f

.field public static final MESSAGE_ID_FIELD_NUMBER:I = 0x17

.field public static final ORIGINAL_TYPE_FIELD_NUMBER:I = 0x10

.field public static final PATH_FIELD_NUMBER:I = 0xf

.field public static final PEER_CAP_HASH_FIELD_NUMBER:I = 0x2f

.field public static final PEER_FIELD_NUMBER:I = 0x15

.field public static final PRODUCT_FIELD_NUMBER:I = 0xd

.field public static final PROGRESS_FIELD_NUMBER:I = 0x1e

.field public static final READ_FIELD_NUMBER:I = 0x2a

.field public static final RECORDER_ABLE_PLAYBACK_FIELD_NUMBER:I = 0x32

.field public static final ROBOT_MESSAGE_TYPE_FIELD_NUMBER:I = 0x19

.field public static final SENDER_JID_FIELD_NUMBER:I = 0x2e

.field public static final SENDER_MSG_ID_FIELD_NUMBER:I = 0x2d

.field public static final SEND_STATUS_FIELD_NUMBER:I = 0x18

.field public static final SERVER_SHARE_ID_FIELD_NUMBER:I = 0x2b

.field public static final SHOULD_VIDEO_BE_TRIMED_FIELD_NUMBER:I = 0x14

.field public static final SIZE_FIELD_NUMBER:I = 0x8

.field public static final TEXT_FIELD_NUMBER:I = 0x4

.field public static final TEXT_IF_NOT_SUPPORT_FIELD_NUMBER:I = 0xe

.field public static final THUMBNAIL_PATH_FIELD_NUMBER:I = 0x7

.field public static final THUMBNAIL_URL_FIELD_NUMBER:I = 0x6

.field public static final TIME_CREATED_FIELD_NUMBER:I = 0x1d

.field public static final TIME_PEER_READ_FIELD_NUMBER:I = 0x1f

.field public static final TIME_SEND_FIELD_NUMBER:I = 0x16

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field public static final URL_FIELD_NUMBER:I = 0x5

.field public static final VGOOD_BUNDLE_FIELD_NUMBER:I = 0xc

.field public static final VIDEO_ROTATION_FIELD_NUMBER:I = 0x31

.field public static final VIDEO_TRIMMER_END_TIMESTAMP_FIELD_NUMBER:I = 0x3e

.field public static final VIDEO_TRIMMER_START_TIMESTAMP_FIELD_NUMBER:I = 0x3d

.field public static final WEB_PAGE_URL_FIELD_NUMBER:I = 0x11

.field public static final WIDTH_FIELD_NUMBER:I = 0x12

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;


# instance fields
.field private bitField0_:I

.field private bitField1_:I

.field private channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

.field private conversationId_:Ljava/lang/Object;

.field private duration_:I

.field private height_:I

.field private isEcard_:Z

.field private isForUpdate_:Z

.field private isForwaredMessage_:Z

.field private isFromMe_:Z

.field private isOfflineMessage_:Z

.field private loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

.field private mediaId_:Ljava/lang/Object;

.field private mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private messageId_:I

.field private originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

.field private path_:Ljava/lang/Object;

.field private peerCapHash_:Ljava/lang/Object;

.field private peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

.field private progress_:I

.field private read_:Z

.field private recorderAblePlayback_:Z

.field private robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

.field private sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

.field private senderJid_:Ljava/lang/Object;

.field private senderMsgId_:Ljava/lang/Object;

.field private serverShareId_:Ljava/lang/Object;

.field private shouldVideoBeTrimed_:Z

.field private size_:I

.field private textIfNotSupport_:Ljava/lang/Object;

.field private text_:Ljava/lang/Object;

.field private thumbnailPath_:Ljava/lang/Object;

.field private thumbnailUrl_:Ljava/lang/Object;

.field private timeCreated_:J

.field private timePeerRead_:J

.field private timeSend_:J

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

.field private url_:Ljava/lang/Object;

.field private vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

.field private videoRotation_:I

.field private videoTrimmerEndTimestamp_:I

.field private videoTrimmerStartTimestamp_:I

.field private webPageUrl_:Ljava/lang/Object;

.field private width_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$122502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$122602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p1
.end method

.method static synthetic access$122702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$122802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$122902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$123002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$123102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$123202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I

    return p1
.end method

.method static synthetic access$123302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I

    return p1
.end method

.method static synthetic access$123402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$123502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z

    return p1
.end method

.method static synthetic access$123602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object p1
.end method

.method static synthetic access$123702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;)Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object p1
.end method

.method static synthetic access$123802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$123902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object p1
.end method

.method static synthetic access$124002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$124102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I

    return p1
.end method

.method static synthetic access$124202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I

    return p1
.end method

.method static synthetic access$124302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z

    return p1
.end method

.method static synthetic access$124402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I

    return p1
.end method

.method static synthetic access$124502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I

    return p1
.end method

.method static synthetic access$124602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;)Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    return-object p1
.end method

.method static synthetic access$124702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$124802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J

    return-wide p1
.end method

.method static synthetic access$124902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I

    return p1
.end method

.method static synthetic access$125002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object p1
.end method

.method static synthetic access$125102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;)Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    return-object p1
.end method

.method static synthetic access$125202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z

    return p1
.end method

.method static synthetic access$125302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z

    return p1
.end method

.method static synthetic access$125402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object p1
.end method

.method static synthetic access$125502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J

    return-wide p1
.end method

.method static synthetic access$125602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I

    return p1
.end method

.method static synthetic access$125702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;J)J
    .locals 0

    iput-wide p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J

    return-wide p1
.end method

.method static synthetic access$125802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object p1
.end method

.method static synthetic access$125902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z

    return p1
.end method

.method static synthetic access$126002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$126102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z

    return p1
.end method

.method static synthetic access$126202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$126302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$126402(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$126502(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z

    return p1
.end method

.method static synthetic access$126602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I

    return p1
.end method

.method static synthetic access$126702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z

    return p1
.end method

.method static synthetic access$126802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    return p1
.end method

.method static synthetic access$126902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    return p1
.end method

.method private getConversationIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method private getMediaIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPeerCapHashBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSenderJidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getSenderMsgIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getServerShareIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTextIfNotSupportBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getThumbnailPathBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getThumbnailUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getWebPageUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->MEDIA_SOURCE_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->ROBOT_MESSAGE_NONE:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I

    iput-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122300()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->access$122200(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getChannel()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->conversationId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getDuration()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I

    return v0
.end method

.method public getIsEcard()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z

    return v0
.end method

.method public getIsForUpdate()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z

    return v0
.end method

.method public getIsForwaredMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z

    return v0
.end method

.method public getIsFromMe()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z

    return v0
.end method

.method public getIsOfflineMessage()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z

    return v0
.end method

.method public getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object v0
.end method

.method public getMediaId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getMediaSourceType()Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    return-object v0
.end method

.method public getMessageId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I

    return v0
.end method

.method public getOriginalType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->path_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getPeerCapHash()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peerCapHash_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I

    return v0
.end method

.method public getRead()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z

    return v0
.end method

.method public getRecorderAblePlayback()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z

    return v0
.end method

.method public getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    return-object v0
.end method

.method public getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method

.method public getSenderJid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderJid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSenderMsgId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->senderMsgId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 9

    const/high16 v8, -0x8000

    const/16 v7, 0x10

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_4

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_7

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I

    invoke-static {v6, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_8

    const/16 v1, 0x9

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_8
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_9

    const/16 v1, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_a

    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_b

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_c

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_d

    const/16 v1, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextIfNotSupportBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_e

    const/16 v1, 0xf

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_f

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v1

    invoke-static {v7, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_10

    const/16 v1, 0x11

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getWebPageUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_10
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x1

    and-int/2addr v1, v2

    const/high16 v2, 0x1

    if-ne v1, v2, :cond_11

    const/16 v1, 0x12

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_11
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x2

    and-int/2addr v1, v2

    const/high16 v2, 0x2

    if-ne v1, v2, :cond_12

    const/16 v1, 0x13

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_12
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x4

    and-int/2addr v1, v2

    const/high16 v2, 0x4

    if-ne v1, v2, :cond_13

    const/16 v1, 0x14

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_13
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x40

    and-int/2addr v1, v2

    const/high16 v2, 0x40

    if-ne v1, v2, :cond_14

    const/16 v1, 0x15

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_14
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x80

    and-int/2addr v1, v2

    const/high16 v2, 0x80

    if-ne v1, v2, :cond_15

    const/16 v1, 0x16

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_15
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x100

    and-int/2addr v1, v2

    const/high16 v2, 0x100

    if-ne v1, v2, :cond_16

    const/16 v1, 0x17

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_16
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x200

    and-int/2addr v1, v2

    const/high16 v2, 0x200

    if-ne v1, v2, :cond_17

    const/16 v1, 0x18

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_17
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x400

    and-int/2addr v1, v2

    const/high16 v2, 0x400

    if-ne v1, v2, :cond_18

    const/16 v1, 0x19

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_18
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x800

    and-int/2addr v1, v2

    const/high16 v2, 0x800

    if-ne v1, v2, :cond_19

    const/16 v1, 0x1a

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_19
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x1000

    and-int/2addr v1, v2

    const/high16 v2, 0x1000

    if-ne v1, v2, :cond_1a

    const/16 v1, 0x1b

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x2000

    and-int/2addr v1, v2

    const/high16 v2, 0x2000

    if-ne v1, v2, :cond_1b

    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x4000

    and-int/2addr v1, v2

    const/high16 v2, 0x4000

    if-ne v1, v2, :cond_1c

    const/16 v1, 0x1d

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v1, v8

    if-ne v1, v8, :cond_1d

    const/16 v1, 0x1e

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1e

    const/16 v1, 0x1f

    iget-wide v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/CodedOutputStream;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1f

    const/16 v1, 0x29

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1f
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_20

    const/16 v1, 0x2a

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_20
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_21

    const/16 v1, 0x2b

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getServerShareIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_21
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x10

    if-ne v1, v7, :cond_22

    const/16 v1, 0x2c

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_22
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_23

    const/16 v1, 0x2d

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderMsgIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_23
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_24

    const/16 v1, 0x2e

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_24
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_25

    const/16 v1, 0x2f

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeerCapHashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_25
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_26

    const/16 v1, 0x30

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_26
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_27

    const/16 v1, 0x31

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_27
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_28

    const/16 v1, 0x32

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_28
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x8

    and-int/2addr v1, v2

    const/high16 v2, 0x8

    if-ne v1, v2, :cond_29

    const/16 v1, 0x3d

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_29
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x10

    and-int/2addr v1, v2

    const/high16 v2, 0x10

    if-ne v1, v2, :cond_2a

    const/16 v1, 0x3e

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v2, 0x20

    and-int/2addr v1, v2

    const/high16 v2, 0x20

    if-ne v1, v2, :cond_2b

    const/16 v1, 0x3f

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2b
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedSerializedSize:I

    goto/16 :goto_0
.end method

.method public getServerShareId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->serverShareId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getShouldVideoBeTrimed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z

    return v0
.end method

.method public getSize()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->text_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getTextIfNotSupport()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->textIfNotSupport_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getThumbnailPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailPath_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getThumbnailUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->thumbnailUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getTimeCreated()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J

    return-wide v0
.end method

.method public getTimePeerRead()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J

    return-wide v0
.end method

.method public getTimeSend()J
    .locals 2

    iget-wide v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J

    return-wide v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->url_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVideoRotation()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I

    return v0
.end method

.method public getVideoTrimmerEndTimestamp()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I

    return v0
.end method

.method public getVideoTrimmerStartTimestamp()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I

    return v0
.end method

.method public getWebPageUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->webPageUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I

    return v0
.end method

.method public hasChannel()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasConversationId()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDuration()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeight()Z
    .locals 2

    const/high16 v1, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsEcard()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsForUpdate()Z
    .locals 2

    const/high16 v1, 0x1000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsForwaredMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsFromMe()Z
    .locals 2

    const/high16 v1, 0x800

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasIsOfflineMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLoadingStatus()Z
    .locals 2

    const/high16 v1, 0x2000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMediaSourceType()Z
    .locals 2

    const/high16 v1, 0x20

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageId()Z
    .locals 2

    const/high16 v1, 0x100

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOriginalType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPath()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    const/high16 v1, 0x40

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerCapHash()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProduct()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasProgress()Z
    .locals 2

    const/high16 v1, -0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRead()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecorderAblePlayback()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRobotMessageType()Z
    .locals 2

    const/high16 v1, 0x400

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSendStatus()Z
    .locals 2

    const/high16 v1, 0x200

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSenderJid()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSenderMsgId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasServerShareId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasShouldVideoBeTrimed()Z
    .locals 2

    const/high16 v1, 0x4

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSize()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTextIfNotSupport()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnailPath()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasThumbnailUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeCreated()Z
    .locals 2

    const/high16 v1, 0x4000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimePeerRead()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTimeSend()Z
    .locals 2

    const/high16 v1, 0x80

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVgoodBundle()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoRotation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoTrimmerEndTimestamp()Z
    .locals 2

    const/high16 v1, 0x10

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVideoTrimmerStartTimestamp()Z
    .locals 2

    const/high16 v1, 0x8

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWebPageUrl()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWidth()Z
    .locals 2

    const/high16 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasConversationId()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasProduct()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/high16 v7, -0x8000

    const/16 v6, 0x10

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->type_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_4

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_5

    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getThumbnailPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->size_:I

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_7

    const/16 v0, 0x9

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->duration_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_8

    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getMediaIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_9

    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isEcard_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_a

    const/16 v0, 0xc

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->vgoodBundle_:Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_b

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->product_:Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_c

    const/16 v0, 0xe

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getTextIfNotSupportBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_d

    const/16 v0, 0xf

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPathBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->originalType_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v6, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_f

    const/16 v0, 0x11

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getWebPageUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x1

    and-int/2addr v0, v1

    const/high16 v1, 0x1

    if-ne v0, v1, :cond_10

    const/16 v0, 0x12

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->width_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x2

    and-int/2addr v0, v1

    const/high16 v1, 0x2

    if-ne v0, v1, :cond_11

    const/16 v0, 0x13

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->height_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_11
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x4

    and-int/2addr v0, v1

    const/high16 v1, 0x4

    if-ne v0, v1, :cond_12

    const/16 v0, 0x14

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->shouldVideoBeTrimed_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x40

    and-int/2addr v0, v1

    const/high16 v1, 0x40

    if-ne v0, v1, :cond_13

    const/16 v0, 0x15

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_13
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x80

    and-int/2addr v0, v1

    const/high16 v1, 0x80

    if-ne v0, v1, :cond_14

    const/16 v0, 0x16

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeSend_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_14
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x100

    and-int/2addr v0, v1

    const/high16 v1, 0x100

    if-ne v0, v1, :cond_15

    const/16 v0, 0x17

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->messageId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_15
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x200

    and-int/2addr v0, v1

    const/high16 v1, 0x200

    if-ne v0, v1, :cond_16

    const/16 v0, 0x18

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->sendStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_16
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x400

    and-int/2addr v0, v1

    const/high16 v1, 0x400

    if-ne v0, v1, :cond_17

    const/16 v0, 0x19

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->robotMessageType_:Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_17
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x800

    and-int/2addr v0, v1

    const/high16 v1, 0x800

    if-ne v0, v1, :cond_18

    const/16 v0, 0x1a

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isFromMe_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_18
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x1000

    and-int/2addr v0, v1

    const/high16 v1, 0x1000

    if-ne v0, v1, :cond_19

    const/16 v0, 0x1b

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForUpdate_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_19
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x2000

    and-int/2addr v0, v1

    const/high16 v1, 0x2000

    if-ne v0, v1, :cond_1a

    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->loadingStatus_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x4000

    and-int/2addr v0, v1

    const/high16 v1, 0x4000

    if-ne v0, v1, :cond_1b

    const/16 v0, 0x1d

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timeCreated_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    and-int/2addr v0, v7

    if-ne v0, v7, :cond_1c

    const/16 v0, 0x1e

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->progress_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_1c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1d

    const/16 v0, 0x1f

    iget-wide v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->timePeerRead_:J

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeInt64(IJ)V

    :cond_1d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1e

    const/16 v0, 0x29

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->channel_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_1f

    const/16 v0, 0x2a

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->read_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_20

    const/16 v0, 0x2b

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getServerShareIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_20
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x10

    if-ne v0, v6, :cond_21

    const/16 v0, 0x2c

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isOfflineMessage_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_21
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_22

    const/16 v0, 0x2d

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderMsgIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_22
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_23

    const/16 v0, 0x2e

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getSenderJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_23
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_24

    const/16 v0, 0x2f

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getPeerCapHashBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_24
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_25

    const/16 v0, 0x30

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isForwaredMessage_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_25
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_26

    const/16 v0, 0x31

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoRotation_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_26
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField1_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_27

    const/16 v0, 0x32

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->recorderAblePlayback_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_27
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x8

    and-int/2addr v0, v1

    const/high16 v1, 0x8

    if-ne v0, v1, :cond_28

    const/16 v0, 0x3d

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerStartTimestamp_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_28
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x10

    and-int/2addr v0, v1

    const/high16 v1, 0x10

    if-ne v0, v1, :cond_29

    const/16 v0, 0x3e

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->videoTrimmerEndTimestamp_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_29
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->bitField0_:I

    const/high16 v1, 0x20

    and-int/2addr v0, v1

    const/high16 v1, 0x20

    if-ne v0, v1, :cond_2a

    const/16 v0, 0x3f

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->mediaSourceType_:Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_2a
    return-void
.end method
