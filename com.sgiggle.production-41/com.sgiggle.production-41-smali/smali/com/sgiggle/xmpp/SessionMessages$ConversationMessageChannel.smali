.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConversationMessageChannel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel; = null

.field public static final enum MESSAGE_CHANNEL_LOCAL:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel; = null

.field public static final MESSAGE_CHANNEL_LOCAL_VALUE:I = 0x9

.field public static final enum MESSAGE_CHANNEL_PPPLS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel; = null

.field public static final MESSAGE_CHANNEL_PPPLS_VALUE:I = 0x2

.field public static final enum MESSAGE_CHANNEL_VM_SERVER:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel; = null

.field public static final MESSAGE_CHANNEL_VM_SERVER_VALUE:I = 0x1

.field public static final enum MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

.field public static final MESSAGE_CHANNEL_XMPP_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 928
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    const-string v1, "MESSAGE_CHANNEL_XMPP"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    .line 929
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    const-string v1, "MESSAGE_CHANNEL_VM_SERVER"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_VM_SERVER:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    .line 930
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    const-string v1, "MESSAGE_CHANNEL_PPPLS"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_PPPLS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    .line 931
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    const-string v1, "MESSAGE_CHANNEL_LOCAL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_LOCAL:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    .line 926
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_VM_SERVER:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_PPPLS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_LOCAL:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    .line 957
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 966
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 967
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->value:I

    .line 968
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 954
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 1
    .parameter

    .prologue
    .line 943
    sparse-switch p0, :sswitch_data_0

    .line 948
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 944
    :sswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    goto :goto_0

    .line 945
    :sswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_VM_SERVER:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    goto :goto_0

    .line 946
    :sswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_PPPLS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    goto :goto_0

    .line 947
    :sswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->MESSAGE_CHANNEL_LOCAL:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    goto :goto_0

    .line 943
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 1
    .parameter

    .prologue
    .line 926
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
    .locals 1

    .prologue
    .line 926
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 940
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;->value:I

    return v0
.end method
