.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConversationMessageLoadingStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final enum STATUS_CONTENT_MEDIA_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_MEDIA_ERROR_VALUE:I = 0xb

.field public static final enum STATUS_CONTENT_MEDIA_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_MEDIA_LOADED_VALUE:I = 0x2

.field public static final enum STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_MEDIA_LOADING_VALUE:I = 0x3

.field public static final enum STATUS_CONTENT_THUMBNAIL_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_THUMBNAIL_ERROR_VALUE:I = 0xa

.field public static final enum STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_THUMBNAIL_LOADED_VALUE:I = 0x0

.field public static final enum STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus; = null

.field public static final STATUS_CONTENT_THUMBNAIL_LOADING_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 875
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_THUMBNAIL_LOADED"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 876
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_THUMBNAIL_LOADING"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 877
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_MEDIA_LOADED"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 878
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_MEDIA_LOADING"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 879
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_THUMBNAIL_ERROR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v9, v9, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 880
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    const-string v1, "STATUS_CONTENT_MEDIA_ERROR"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/16 v4, 0xb

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 873
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    .line 910
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 919
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 920
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->value:I

    .line 921
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 907
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 1
    .parameter

    .prologue
    .line 894
    packed-switch p0, :pswitch_data_0

    .line 901
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 895
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 896
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 897
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 898
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_LOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 899
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_THUMBNAIL_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 900
    :pswitch_6
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->STATUS_CONTENT_MEDIA_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    goto :goto_0

    .line 894
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 1
    .parameter

    .prologue
    .line 873
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
    .locals 1

    .prologue
    .line 873
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 891
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;->value:I

    return v0
.end method
