.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationMessageNotificationReceivedPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final GO_TO_CONVERSATION_FIELD_NUMBER:I = 0x5

.field public static final MESSAGE_CONTENT_FIELD_NUMBER:I = 0x6

.field public static final MESSAGE_PUSH_ID_FIELD_NUMBER:I = 0x7

.field public static final PEER_ACCOUNT_ID_FIELD_NUMBER:I = 0x2

.field public static final PEER_NAME_FIELD_NUMBER:I = 0x3

.field public static final TYPE_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private goToConversation_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private messageContent_:Ljava/lang/Object;

.field private messagePushId_:Ljava/lang/Object;

.field private peerAccountId_:Ljava/lang/Object;

.field private peerName_:Ljava/lang/Object;

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$134602(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$134702(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$134802(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$134902(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->type_:I

    return p1
.end method

.method static synthetic access$135002(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->goToConversation_:Z

    return p1
.end method

.method static synthetic access$135102(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$135202(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$135302(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    return-object v0
.end method

.method private getMessageContentBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getMessagePushIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPeerAccountIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getPeerNameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->type_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->goToConversation_:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134400()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;->access$134300(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;

    return-object v0
.end method

.method public getGoToConversation()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->goToConversation_:Z

    return v0
.end method

.method public getMessageContent()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messageContent_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getMessagePushId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->messagePushId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getPeerAccountId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerAccountId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getPeerName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->peerName_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getPeerAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getPeerNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->type_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->goToConversation_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_6

    const/4 v1, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getMessageContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_7

    const/4 v1, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getMessagePushIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_7
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasGoToConversation()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageContent()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessagePushId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerAccountId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeerName()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->hasGoToConversation()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getPeerAccountIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getPeerNameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->type_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->goToConversation_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getMessageContentBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayload;->getMessagePushIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_6
    return-void
.end method
