.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageNotificationReceivedPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationMessageNotificationReceivedPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getGoToConversation()Z
.end method

.method public abstract getMessageContent()Ljava/lang/String;
.end method

.method public abstract getMessagePushId()Ljava/lang/String;
.end method

.method public abstract getPeerAccountId()Ljava/lang/String;
.end method

.method public abstract getPeerName()Ljava/lang/String;
.end method

.method public abstract getType()I
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasGoToConversation()Z
.end method

.method public abstract hasMessageContent()Z
.end method

.method public abstract hasMessagePushId()Z
.end method

.method public abstract hasPeerAccountId()Z
.end method

.method public abstract hasPeerName()Z
.end method

.method public abstract hasType()Z
.end method
