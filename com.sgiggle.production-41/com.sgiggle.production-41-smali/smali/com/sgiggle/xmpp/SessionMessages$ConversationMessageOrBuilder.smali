.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationMessageOrBuilder"
.end annotation


# virtual methods
.method public abstract getChannel()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageChannel;
.end method

.method public abstract getConversationId()Ljava/lang/String;
.end method

.method public abstract getDuration()I
.end method

.method public abstract getHeight()I
.end method

.method public abstract getIsEcard()Z
.end method

.method public abstract getIsForUpdate()Z
.end method

.method public abstract getIsForwaredMessage()Z
.end method

.method public abstract getIsFromMe()Z
.end method

.method public abstract getIsOfflineMessage()Z
.end method

.method public abstract getLoadingStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageLoadingStatus;
.end method

.method public abstract getMediaId()Ljava/lang/String;
.end method

.method public abstract getMediaSourceType()Lcom/sgiggle/xmpp/SessionMessages$MediaSourceType;
.end method

.method public abstract getMessageId()I
.end method

.method public abstract getOriginalType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getPeerCapHash()Ljava/lang/String;
.end method

.method public abstract getProduct()Lcom/sgiggle/xmpp/SessionMessages$ProductCatalogEntry;
.end method

.method public abstract getProgress()I
.end method

.method public abstract getRead()Z
.end method

.method public abstract getRecorderAblePlayback()Z
.end method

.method public abstract getRobotMessageType()Lcom/sgiggle/xmpp/SessionMessages$RobotMessageType;
.end method

.method public abstract getSendStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
.end method

.method public abstract getSenderJid()Ljava/lang/String;
.end method

.method public abstract getSenderMsgId()Ljava/lang/String;
.end method

.method public abstract getServerShareId()Ljava/lang/String;
.end method

.method public abstract getShouldVideoBeTrimed()Z
.end method

.method public abstract getSize()I
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getTextIfNotSupport()Ljava/lang/String;
.end method

.method public abstract getThumbnailPath()Ljava/lang/String;
.end method

.method public abstract getThumbnailUrl()Ljava/lang/String;
.end method

.method public abstract getTimeCreated()J
.end method

.method public abstract getTimePeerRead()J
.end method

.method public abstract getTimeSend()J
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public abstract getVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.end method

.method public abstract getVideoRotation()I
.end method

.method public abstract getVideoTrimmerEndTimestamp()I
.end method

.method public abstract getVideoTrimmerStartTimestamp()I
.end method

.method public abstract getWebPageUrl()Ljava/lang/String;
.end method

.method public abstract getWidth()I
.end method

.method public abstract hasChannel()Z
.end method

.method public abstract hasConversationId()Z
.end method

.method public abstract hasDuration()Z
.end method

.method public abstract hasHeight()Z
.end method

.method public abstract hasIsEcard()Z
.end method

.method public abstract hasIsForUpdate()Z
.end method

.method public abstract hasIsForwaredMessage()Z
.end method

.method public abstract hasIsFromMe()Z
.end method

.method public abstract hasIsOfflineMessage()Z
.end method

.method public abstract hasLoadingStatus()Z
.end method

.method public abstract hasMediaId()Z
.end method

.method public abstract hasMediaSourceType()Z
.end method

.method public abstract hasMessageId()Z
.end method

.method public abstract hasOriginalType()Z
.end method

.method public abstract hasPath()Z
.end method

.method public abstract hasPeer()Z
.end method

.method public abstract hasPeerCapHash()Z
.end method

.method public abstract hasProduct()Z
.end method

.method public abstract hasProgress()Z
.end method

.method public abstract hasRead()Z
.end method

.method public abstract hasRecorderAblePlayback()Z
.end method

.method public abstract hasRobotMessageType()Z
.end method

.method public abstract hasSendStatus()Z
.end method

.method public abstract hasSenderJid()Z
.end method

.method public abstract hasSenderMsgId()Z
.end method

.method public abstract hasServerShareId()Z
.end method

.method public abstract hasShouldVideoBeTrimed()Z
.end method

.method public abstract hasSize()Z
.end method

.method public abstract hasText()Z
.end method

.method public abstract hasTextIfNotSupport()Z
.end method

.method public abstract hasThumbnailPath()Z
.end method

.method public abstract hasThumbnailUrl()Z
.end method

.method public abstract hasTimeCreated()Z
.end method

.method public abstract hasTimePeerRead()Z
.end method

.method public abstract hasTimeSend()Z
.end method

.method public abstract hasType()Z
.end method

.method public abstract hasUrl()Z
.end method

.method public abstract hasVgoodBundle()Z
.end method

.method public abstract hasVideoRotation()Z
.end method

.method public abstract hasVideoTrimmerEndTimestamp()Z
.end method

.method public abstract hasVideoTrimmerStartTimestamp()Z
.end method

.method public abstract hasWebPageUrl()Z
.end method

.method public abstract hasWidth()Z
.end method
