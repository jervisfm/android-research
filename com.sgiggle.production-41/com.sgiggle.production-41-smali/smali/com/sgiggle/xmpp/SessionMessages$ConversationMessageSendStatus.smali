.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConversationMessageSendStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final enum STATUS_ERROR_ACCOUNT_NOT_VALIDATED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_ACCOUNT_NOT_VALIDATED_VALUE:I = 0x8

.field public static final enum STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED_VALUE:I = 0x9

.field public static final enum STATUS_ERROR_NOT_LOGIN:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_NOT_LOGIN_VALUE:I = 0x3

.field public static final enum STATUS_ERROR_NO_JID:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_NO_JID_VALUE:I = 0x4

.field public static final enum STATUS_ERROR_NO_NETWORK:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_NO_NETWORK_VALUE:I = 0x2

.field public static final enum STATUS_ERROR_PEER_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_PEER_NOT_SUPPORT_VALUE:I = 0x7

.field public static final enum STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT_VALUE:I = 0xe

.field public static final enum STATUS_ERROR_SEND_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_SEND_TIMEOUT_VALUE:I = 0x5

.field public static final enum STATUS_ERROR_UPLOAD:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_UPLOAD_VALUE:I = 0xa

.field public static final enum STATUS_ERROR_VIDEO_TRIMMER_FAILED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_VIDEO_TRIMMER_FAILED_VALUE:I = 0xf

.field public static final enum STATUS_ERROR_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_ERROR_XMPP_VALUE:I = 0x6

.field public static final enum STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_INIT_VALUE:I = 0xb

.field public static final enum STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final enum STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_READY_TO_SEND_VALUE:I = 0xd

.field public static final enum STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_READ_AND_SHOW_STATUS_VALUE:I = 0x12

.field public static final STATUS_READ_VALUE:I = 0x11

.field public static final enum STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_SENDING_VALUE:I = 0x0

.field public static final enum STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_SENT_VALUE:I = 0x1

.field public static final enum STATUS_TRIMMING_VIDEO:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_TRIMMING_VIDEO_VALUE:I = 0x10

.field public static final enum STATUS_UPLOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus; = null

.field public static final STATUS_UPLOADING_VALUE:I = 0xc

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 783
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_INIT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v5, v5, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 784
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_TRIMMING_VIDEO"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v6, v6, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_TRIMMING_VIDEO:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 785
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_UPLOADING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v7, v7, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_UPLOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 786
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_READY_TO_SEND"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v8, v8, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 787
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_SENDING"

    invoke-direct {v0, v1, v9, v9, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 788
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_SENT"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 789
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_READ"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/16 v4, 0x11

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 790
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_READ_AND_SHOW_STATUS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0x12

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 791
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_NO_NETWORK"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3, v7}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_NETWORK:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 792
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_NOT_LOGIN"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3, v8}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NOT_LOGIN:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 793
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_NO_JID"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3, v9}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_JID:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 794
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_SEND_TIMEOUT"

    const/16 v2, 0xb

    const/16 v3, 0xb

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_SEND_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 795
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_XMPP"

    const/16 v2, 0xc

    const/16 v3, 0xc

    const/4 v4, 0x6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 796
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_PEER_NOT_SUPPORT"

    const/16 v2, 0xd

    const/16 v3, 0xd

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 797
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_ACCOUNT_NOT_VALIDATED"

    const/16 v2, 0xe

    const/16 v3, 0xe

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACCOUNT_NOT_VALIDATED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 798
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED"

    const/16 v2, 0xf

    const/16 v3, 0xf

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 799
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_UPLOAD"

    const/16 v2, 0x10

    const/16 v3, 0x10

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_UPLOAD:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 800
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT"

    const/16 v2, 0x11

    const/16 v3, 0x11

    const/16 v4, 0xe

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 801
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    const-string v1, "STATUS_ERROR_VIDEO_TRIMMER_FAILED"

    const/16 v2, 0x12

    const/16 v3, 0x12

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_VIDEO_TRIMMER_FAILED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 781
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_TRIMMING_VIDEO:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_UPLOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_NETWORK:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NOT_LOGIN:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_JID:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_SEND_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACCOUNT_NOT_VALIDATED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_UPLOAD:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_VIDEO_TRIMMER_FAILED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    .line 857
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 866
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 867
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->value:I

    .line 868
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 854
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1
    .parameter

    .prologue
    .line 828
    packed-switch p0, :pswitch_data_0

    .line 848
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 829
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_INIT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 830
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_TRIMMING_VIDEO:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 831
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_UPLOADING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 832
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READY_TO_SEND:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 833
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENDING:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 834
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_SENT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 835
    :pswitch_6
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 836
    :pswitch_7
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_READ_AND_SHOW_STATUS:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 837
    :pswitch_8
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_NETWORK:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 838
    :pswitch_9
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NOT_LOGIN:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 839
    :pswitch_a
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_NO_JID:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 840
    :pswitch_b
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_SEND_TIMEOUT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 841
    :pswitch_c
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_XMPP:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 842
    :pswitch_d
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 843
    :pswitch_e
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACCOUNT_NOT_VALIDATED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 844
    :pswitch_f
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_ACKNOWLEDGEMENT_NOT_RECEIVED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 845
    :pswitch_10
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_UPLOAD:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 846
    :pswitch_11
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_PEER_PLATFORM_NOT_SUPPORT:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 847
    :pswitch_12
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->STATUS_ERROR_VIDEO_TRIMMER_FAILED:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    goto :goto_0

    .line 828
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_11
        :pswitch_12
        :pswitch_1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1
    .parameter

    .prologue
    .line 781
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;
    .locals 1

    .prologue
    .line 781
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 825
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageSendStatus;->value:I

    return v0
.end method
