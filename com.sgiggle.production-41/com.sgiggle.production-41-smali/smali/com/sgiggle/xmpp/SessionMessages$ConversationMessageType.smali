.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConversationMessageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final enum AUDIO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final AUDIO_MESSAGE_VALUE:I = 0x2

.field public static final enum IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final IMAGE_MESSAGE_VALUE:I = 0x3

.field public static final enum LOCATION_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final LOCATION_MESSAGE_VALUE:I = 0x4

.field public static final enum READ_RECEIPT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final READ_RECEIPT_MESSAGE_VALUE:I = 0xa

.field public static final enum SYSTEM_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final SYSTEM_MESSAGE_VALUE:I = 0x9

.field public static final enum TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final TEXT_MESSAGE_VALUE:I = 0x0

.field public static final enum VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final VGOOD_MESSAGE_VALUE:I = 0x5

.field public static final enum VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType; = null

.field public static final VIDEO_MESSAGE_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 724
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "TEXT_MESSAGE"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 725
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "VIDEO_MESSAGE"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 726
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "AUDIO_MESSAGE"

    invoke-direct {v0, v1, v7, v7, v7}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->AUDIO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 727
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "IMAGE_MESSAGE"

    invoke-direct {v0, v1, v8, v8, v8}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 728
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "LOCATION_MESSAGE"

    invoke-direct {v0, v1, v9, v9, v9}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->LOCATION_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 729
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "VGOOD_MESSAGE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 730
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "SYSTEM_MESSAGE"

    const/4 v2, 0x6

    const/4 v3, 0x6

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->SYSTEM_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 731
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    const-string v1, "READ_RECEIPT_MESSAGE"

    const/4 v2, 0x7

    const/4 v3, 0x7

    const/16 v4, 0xa

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->READ_RECEIPT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 722
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->AUDIO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->LOCATION_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->SYSTEM_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->READ_RECEIPT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    .line 765
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 774
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 775
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->value:I

    .line 776
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 762
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1
    .parameter

    .prologue
    .line 747
    packed-switch p0, :pswitch_data_0

    .line 756
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 748
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->TEXT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 749
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VIDEO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 750
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->AUDIO_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 751
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->IMAGE_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 752
    :pswitch_5
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->LOCATION_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 753
    :pswitch_6
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->VGOOD_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 754
    :pswitch_7
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->SYSTEM_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 755
    :pswitch_8
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->READ_RECEIPT_MESSAGE:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    goto :goto_0

    .line 747
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1
    .parameter

    .prologue
    .line 722
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;
    .locals 1

    .prologue
    .line 722
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 744
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageType;->value:I

    return v0
.end method
