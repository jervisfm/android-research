.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private anchor_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private countLimit_:I

.field private emptySlotCount_:I

.field private fallbackToConversationList_:Z

.field private fromPushNotification_:Z

.field private messageFromPushAvailable_:Z

.field private message_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private moreMessageAvailable_:Z

.field private myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

.field private otherConversationsUnreadMsgCount_:I

.field private peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private previousMessageId_:I

.field private retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

.field private unreadMessageCount_:I

.field private vgoodBundle_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/16 v0, 0x14

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$129800()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureMessageIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private ensureVgoodBundleIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x200

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllMessage(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addAllVgoodBundle(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 8

    const/high16 v7, 0x2

    const/high16 v6, 0x1

    const v5, 0x8000

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130002(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130102(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/lang/Object;)Ljava/lang/Object;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130202(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x8

    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130302(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x10

    :cond_4
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130402(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    and-int/lit8 v3, v1, 0x20

    const/16 v4, 0x20

    if-ne v3, v4, :cond_5

    or-int/lit8 v2, v2, 0x20

    :cond_5
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130502(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    and-int/lit8 v3, v1, 0x40

    const/16 v4, 0x40

    if-ne v3, v4, :cond_6

    or-int/lit8 v2, v2, 0x40

    :cond_6
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130602(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x80

    const/16 v4, 0x80

    if-ne v3, v4, :cond_7

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x81

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_7
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130702(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/util/List;)Ljava/util/List;

    and-int/lit16 v3, v1, 0x100

    const/16 v4, 0x100

    if-ne v3, v4, :cond_8

    or-int/lit16 v2, v2, 0x80

    :cond_8
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130802(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, 0x200

    const/16 v4, 0x200

    if-ne v3, v4, :cond_9

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v3, v3, -0x201

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_9
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130902(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/util/List;)Ljava/util/List;

    and-int/lit16 v3, v1, 0x400

    const/16 v4, 0x400

    if-ne v3, v4, :cond_a

    or-int/lit16 v2, v2, 0x100

    :cond_a
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131002(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    and-int/lit16 v3, v1, 0x800

    const/16 v4, 0x800

    if-ne v3, v4, :cond_b

    or-int/lit16 v2, v2, 0x200

    :cond_b
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131102(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z

    and-int/lit16 v3, v1, 0x1000

    const/16 v4, 0x1000

    if-ne v3, v4, :cond_c

    or-int/lit16 v2, v2, 0x400

    :cond_c
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131202(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    and-int/lit16 v3, v1, 0x2000

    const/16 v4, 0x2000

    if-ne v3, v4, :cond_d

    or-int/lit16 v2, v2, 0x800

    :cond_d
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131302(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z

    and-int/lit16 v3, v1, 0x4000

    const/16 v4, 0x4000

    if-ne v3, v4, :cond_e

    or-int/lit16 v2, v2, 0x1000

    :cond_e
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131402(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z

    and-int v3, v1, v5

    if-ne v3, v5, :cond_f

    or-int/lit16 v2, v2, 0x2000

    :cond_f
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131502(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    and-int v3, v1, v6

    if-ne v3, v6, :cond_10

    or-int/lit16 v2, v2, 0x4000

    :cond_10
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131602(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_11

    or-int v1, v2, v5

    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131702(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$131802(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I

    return-object v0

    :cond_11
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 3

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAnchor()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x8001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearConversationId()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method public clearCountLimit()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    return-object p0
.end method

.method public clearEmptySlotCount()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x401

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    return-object p0
.end method

.method public clearFallbackToConversationList()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x4001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    return-object p0
.end method

.method public clearFromPushNotification()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x801

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearMessageFromPushAvailable()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x2001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    return-object p0
.end method

.method public clearMoreMessageAvailable()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x101

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    return-object p0
.end method

.method public clearMyself()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x41

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x10001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    return-object p0
.end method

.method public clearOtherConversationsUnreadMsgCount()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, -0x20001

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    return-object p0
.end method

.method public clearPeer()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearPreviousMessageId()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    return-object p0
.end method

.method public clearRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x1001

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p0
.end method

.method public clearUnreadMessageCount()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method

.method public clearVgoodBundle()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAnchor()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCountLimit()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getEmptySlotCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    return v0
.end method

.method public getFallbackToConversationList()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    return v0
.end method

.method public getFromPushNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    return v0
.end method

.method public getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getMessageCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMessageFromPushAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    return v0
.end method

.method public getMessageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getMoreMessageAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    return v0
.end method

.method public getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    return-object v0
.end method

.method public getOtherConversationsUnreadMsgCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    return v0
.end method

.method public getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getPreviousMessageId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    return v0
.end method

.method public getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    return v0
.end method

.method public getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVgoodBundleCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVgoodBundleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasAnchor()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasConversationId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountLimit()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmptySlotCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFallbackToConversationList()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromPushNotification()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageFromPushAvailable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreMessageAvailable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMyself()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOpenConversationContext()Z
    .locals 2

    const/high16 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOtherConversationsUnreadMsgCount()Z
    .locals 2

    const/high16 v1, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPreviousMessageId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRetrieveOfflineMessageStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasMyself()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getMessageCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    goto :goto_0

    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasPeer()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readUInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    goto :goto_0

    :sswitch_6
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    goto :goto_0

    :sswitch_7
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->hasMyself()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_3
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    goto/16 :goto_0

    :sswitch_8
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->addMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    goto/16 :goto_0

    :sswitch_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->addVgoodBundle(Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    goto/16 :goto_0

    :sswitch_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    goto/16 :goto_0

    :sswitch_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto/16 :goto_0

    :sswitch_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    goto/16 :goto_0

    :sswitch_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    goto/16 :goto_0

    :sswitch_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/high16 v2, 0x1

    or-int/2addr v1, v2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    goto/16 :goto_0

    :sswitch_12
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0xa8 -> :sswitch_f
        0xb0 -> :sswitch_10
        0xb8 -> :sswitch_11
        0xc0 -> :sswitch_12
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasConversationId()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergePeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasCountLimit()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getCountLimit()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setCountLimit(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasPreviousMessageId()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getPreviousMessageId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setPreviousMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasUnreadMessageCount()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getUnreadMessageCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_6
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasMyself()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_7
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_13

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x81

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_8
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasMoreMessageAvailable()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMoreMessageAvailable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setMoreMessageAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_9
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130900(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_14

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130900(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit16 v0, v0, -0x201

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    :cond_a
    :goto_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasEmptySlotCount()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getEmptySlotCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setEmptySlotCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_b
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasFromPushNotification()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getFromPushNotification()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setFromPushNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_c
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasRetrieveOfflineMessageStatus()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setRetrieveOfflineMessageStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_d
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasMessageFromPushAvailable()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMessageFromPushAvailable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setMessageFromPushAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_e
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasFallbackToConversationList()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getFallbackToConversationList()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setFallbackToConversationList(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_f
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasAnchor()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getAnchor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setAnchor(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_10
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasOpenConversationContext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setOpenConversationContext(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_11
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasOtherConversationsUnreadMsgCount()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getOtherConversationsUnreadMsgCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->setOtherConversationsUnreadMsgCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    :cond_12
    move-object v0, p0

    goto/16 :goto_0

    :cond_13
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    :cond_14
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->access$130900(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_2
.end method

.method public mergeMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public mergePeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public removeMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public removeVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setAnchor(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const v1, 0x8000

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->anchor_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setConversationId(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-object p0
.end method

.method setConversationId(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->conversationId_:Ljava/lang/Object;

    return-void
.end method

.method public setCountLimit(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->countLimit_:I

    return-object p0
.end method

.method public setEmptySlotCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x400

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->emptySlotCount_:I

    return-object p0
.end method

.method public setFallbackToConversationList(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x4000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fallbackToConversationList_:Z

    return-object p0
.end method

.method public setFromPushNotification(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x800

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->fromPushNotification_:Z

    return-object p0
.end method

.method public setMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMessage(ILcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureMessageIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->message_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMessageFromPushAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->messageFromPushAvailable_:Z

    return-object p0
.end method

.method public setMoreMessageAvailable(Z)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x100

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->moreMessageAvailable_:Z

    return-object p0
.end method

.method public setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setOpenConversationContext(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/high16 v1, 0x1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    return-object p0
.end method

.method public setOtherConversationsUnreadMsgCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    const/high16 v1, 0x2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->otherConversationsUnreadMsgCount_:I

    return-object p0
.end method

.method public setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPeer(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setPreviousMessageId(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->previousMessageId_:I

    return-object p0
.end method

.method public setRetrieveOfflineMessageStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p0
.end method

.method public setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method

.method public setVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setVgoodBundle(ILcom/sgiggle/xmpp/SessionMessages$VGoodBundle;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->ensureVgoodBundleIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
