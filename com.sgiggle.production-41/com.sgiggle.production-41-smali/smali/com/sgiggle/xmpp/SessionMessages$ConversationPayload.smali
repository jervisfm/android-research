.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
    }
.end annotation


# static fields
.field public static final ANCHOR_FIELD_NUMBER:I = 0x16

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONVERSATION_ID_FIELD_NUMBER:I = 0x2

.field public static final COUNT_LIMIT_FIELD_NUMBER:I = 0x4

.field public static final EMPTY_SLOT_COUNT_FIELD_NUMBER:I = 0xb

.field public static final FALLBACK_TO_CONVERSATION_LIST_FIELD_NUMBER:I = 0x15

.field public static final FROM_PUSH_NOTIFICATION_FIELD_NUMBER:I = 0xc

.field public static final MESSAGE_FIELD_NUMBER:I = 0x8

.field public static final MESSAGE_FROM_PUSH_AVAILABLE_FIELD_NUMBER:I = 0xe

.field public static final MORE_MESSAGE_AVAILABLE_FIELD_NUMBER:I = 0x9

.field public static final MYSELF_FIELD_NUMBER:I = 0x7

.field public static final OPEN_CONVERSATION_CONTEXT_FIELD_NUMBER:I = 0x17

.field public static final OTHER_CONVERSATIONS_UNREAD_MSG_COUNT_FIELD_NUMBER:I = 0x18

.field public static final PEER_FIELD_NUMBER:I = 0x3

.field public static final PREVIOUS_MESSAGE_ID_FIELD_NUMBER:I = 0x5

.field public static final RETRIEVE_OFFLINE_MESSAGE_STATUS_FIELD_NUMBER:I = 0xd

.field public static final UNREAD_MESSAGE_COUNT_FIELD_NUMBER:I = 0x6

.field public static final VGOOD_BUNDLE_FIELD_NUMBER:I = 0xa

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;


# instance fields
.field private anchor_:I

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private countLimit_:I

.field private emptySlotCount_:I

.field private fallbackToConversationList_:Z

.field private fromPushNotification_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private messageFromPushAvailable_:Z

.field private message_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation
.end field

.field private moreMessageAvailable_:Z

.field private myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

.field private otherConversationsUnreadMsgCount_:I

.field private peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private previousMessageId_:I

.field private retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

.field private unreadMessageCount_:I

.field private vgoodBundle_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$130002(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$130102(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$130202(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$130302(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I

    return p1
.end method

.method static synthetic access$130402(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I

    return p1
.end method

.method static synthetic access$130502(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I

    return p1
.end method

.method static synthetic access$130602(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$130700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$130702(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$130802(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z

    return p1
.end method

.method static synthetic access$130900(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$130902(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$131002(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I

    return p1
.end method

.method static synthetic access$131102(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z

    return p1
.end method

.method static synthetic access$131202(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p1
.end method

.method static synthetic access$131302(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z

    return p1
.end method

.method static synthetic access$131402(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z

    return p1
.end method

.method static synthetic access$131502(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I

    return p1
.end method

.method static synthetic access$131602(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    return-object p1
.end method

.method static synthetic access$131702(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I

    return p1
.end method

.method static synthetic access$131802(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    return p1
.end method

.method private getConversationIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const/16 v0, 0x14

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z

    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129800()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;->access$129700(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAnchor()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I

    return v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->conversationId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getCountLimit()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;

    return-object v0
.end method

.method public getEmptySlotCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I

    return v0
.end method

.method public getFallbackToConversationList()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z

    return v0
.end method

.method public getFromPushNotification()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z

    return v0
.end method

.method public getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getMessageCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getMessageFromPushAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z

    return v0
.end method

.method public getMessageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    return-object v0
.end method

.method public getMessageOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;

    return-object v0
.end method

.method public getMessageOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessageOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    return-object v0
.end method

.method public getMoreMessageAvailable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z

    return v0
.end method

.method public getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    return-object v0
.end method

.method public getOtherConversationsUnreadMsgCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I

    return v0
.end method

.method public getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getPreviousMessageId()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I

    return v0
.end method

.method public getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_12

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    if-ne v1, v6, :cond_3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    const/4 v1, 0x5

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_5

    const/4 v1, 0x6

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_6

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_6
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v6, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_11

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    move v1, v3

    move v2, v0

    :goto_4
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    const/16 v3, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_4

    :cond_8
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_10

    const/16 v0, 0xb

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    :goto_5
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x200

    const/16 v2, 0x200

    if-ne v1, v2, :cond_9

    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x400

    const/16 v2, 0x400

    if-ne v1, v2, :cond_a

    const/16 v1, 0xd

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_a
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x800

    const/16 v2, 0x800

    if-ne v1, v2, :cond_b

    const/16 v1, 0xe

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x1000

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_c

    const/16 v1, 0x15

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_c
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x2000

    const/16 v2, 0x2000

    if-ne v1, v2, :cond_d

    const/16 v1, 0x16

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_d
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v1, v1, 0x4000

    const/16 v2, 0x4000

    if-ne v1, v2, :cond_e

    const/16 v1, 0x17

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_e
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    const v2, 0x8000

    and-int/2addr v1, v2

    const v2, 0x8000

    if-ne v1, v2, :cond_f

    const/16 v1, 0x18

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_f
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedSerializedSize:I

    goto/16 :goto_0

    :cond_10
    move v0, v2

    goto/16 :goto_5

    :cond_11
    move v0, v2

    goto/16 :goto_3

    :cond_12
    move v0, v3

    goto/16 :goto_1
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I

    return v0
.end method

.method public getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;

    return-object v0
.end method

.method public getVgoodBundleCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVgoodBundleList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public getVgoodBundleOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;

    return-object v0
.end method

.method public getVgoodBundleOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundleOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    return-object v0
.end method

.method public hasAnchor()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasConversationId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountLimit()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmptySlotCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFallbackToConversationList()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromPushNotification()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessageFromPushAvailable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMoreMessageAvailable()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMyself()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOpenConversationContext()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOtherConversationsUnreadMsgCount()Z
    .locals 2

    const v1, 0x8000

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPreviousMessageId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRetrieveOfflineMessageStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasPeer()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->hasMyself()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMessageCount()I

    move-result v1

    if-ge v0, v1, :cond_7

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v5, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->countLimit_:I

    invoke-virtual {p1, v4, v0}, Lcom/google/protobuf/CodedOutputStream;->writeUInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->previousMessageId_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->unreadMessageCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_6

    const/4 v0, 0x7

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_6
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->message_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v5, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x80

    const/16 v1, 0x80

    if-ne v0, v1, :cond_8

    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->moreMessageAvailable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_8
    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    const/16 v2, 0xa

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->vgoodBundle_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_9
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x100

    const/16 v1, 0x100

    if-ne v0, v1, :cond_a

    const/16 v0, 0xb

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->emptySlotCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_a
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x200

    const/16 v1, 0x200

    if-ne v0, v1, :cond_b

    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fromPushNotification_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_b
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x400

    const/16 v1, 0x400

    if-ne v0, v1, :cond_c

    const/16 v0, 0xd

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_c
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_d

    const/16 v0, 0xe

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->messageFromPushAvailable_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_d
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_e

    const/16 v0, 0x15

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->fallbackToConversationList_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_e
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_f

    const/16 v0, 0x16

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->anchor_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_f
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    and-int/lit16 v0, v0, 0x4000

    const/16 v1, 0x4000

    if-ne v0, v1, :cond_10

    const/16 v0, 0x17

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->openConversationContext_:Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_10
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->bitField0_:I

    const v1, 0x8000

    and-int/2addr v0, v1

    const v1, 0x8000

    if-ne v0, v1, :cond_11

    const/16 v0, 0x18

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload;->otherConversationsUnreadMsgCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_11
    return-void
.end method
