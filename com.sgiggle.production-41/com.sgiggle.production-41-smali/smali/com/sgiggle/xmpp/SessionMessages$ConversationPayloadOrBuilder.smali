.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ConversationPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAnchor()I
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getConversationId()Ljava/lang/String;
.end method

.method public abstract getCountLimit()I
.end method

.method public abstract getEmptySlotCount()I
.end method

.method public abstract getFallbackToConversationList()Z
.end method

.method public abstract getFromPushNotification()Z
.end method

.method public abstract getMessage(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end method

.method public abstract getMessageCount()I
.end method

.method public abstract getMessageFromPushAvailable()Z
.end method

.method public abstract getMessageList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMoreMessageAvailable()Z
.end method

.method public abstract getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getOpenConversationContext()Lcom/sgiggle/xmpp/SessionMessages$ConversationPayload$OpenConversationContext;
.end method

.method public abstract getOtherConversationsUnreadMsgCount()I
.end method

.method public abstract getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getPreviousMessageId()I
.end method

.method public abstract getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
.end method

.method public abstract getUnreadMessageCount()I
.end method

.method public abstract getVgoodBundle(I)Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;
.end method

.method public abstract getVgoodBundleCount()I
.end method

.method public abstract getVgoodBundleList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VGoodBundle;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasAnchor()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasConversationId()Z
.end method

.method public abstract hasCountLimit()Z
.end method

.method public abstract hasEmptySlotCount()Z
.end method

.method public abstract hasFallbackToConversationList()Z
.end method

.method public abstract hasFromPushNotification()Z
.end method

.method public abstract hasMessageFromPushAvailable()Z
.end method

.method public abstract hasMoreMessageAvailable()Z
.end method

.method public abstract hasMyself()Z
.end method

.method public abstract hasOpenConversationContext()Z
.end method

.method public abstract hasOtherConversationsUnreadMsgCount()Z
.end method

.method public abstract hasPeer()Z
.end method

.method public abstract hasPreviousMessageId()Z
.end method

.method public abstract hasRetrieveOfflineMessageStatus()Z
.end method

.method public abstract hasUnreadMessageCount()Z
.end method
