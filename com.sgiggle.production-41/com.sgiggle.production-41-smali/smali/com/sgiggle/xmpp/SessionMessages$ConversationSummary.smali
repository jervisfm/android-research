.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationSummary"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    }
.end annotation


# static fields
.field public static final CONVERSATION_ID_FIELD_NUMBER:I = 0x3

.field public static final LAST_FIELD_NUMBER:I = 0x5

.field public static final PEER_FIELD_NUMBER:I = 0x2

.field public static final UNREAD_MESSAGE_COUNT_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;


# instance fields
.field private bitField0_:I

.field private conversationId_:Ljava/lang/Object;

.field private last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private unreadMessageCount_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$132202(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$132302(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$132402(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->unreadMessageCount_:I

    return p1
.end method

.method static synthetic access$132502(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object p1
.end method

.method static synthetic access$132602(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    return p1
.end method

.method private getConversationIdBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->unreadMessageCount_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$132000()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->access$131900(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getConversationId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->conversationId_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    return-object v0
.end method

.method public getLast()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->unreadMessageCount_:I

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->unreadMessageCount_:I

    return v0
.end method

.method public hasConversationId()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLast()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPeer()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->hasPeer()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->hasConversationId()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->hasLast()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getLast()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->peer_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->getConversationIdBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->unreadMessageCount_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->last_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_3
    return-void
.end method
