.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private conversationSummary_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;"
        }
    .end annotation
.end field

.field private myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

.field private unreadConversationCount_:I

.field private unreadMessageCount_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$133400()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureConversationSummaryIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllConversationSummary(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addConversationSummary(ILcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addConversationSummary(ILcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addConversationSummary(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addConversationSummary(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133602(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133702(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x5

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133802(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Ljava/util/List;)Ljava/util/List;

    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    or-int/lit8 v2, v2, 0x4

    :cond_3
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133902(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I

    and-int/lit8 v3, v1, 0x10

    const/16 v4, 0x10

    if-ne v3, v4, :cond_4

    or-int/lit8 v2, v2, 0x8

    :cond_4
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$134002(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_5

    or-int/lit8 v1, v2, 0x10

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$134102(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$134202(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I

    return-object v0

    :cond_5
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearConversationSummary()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearMyself()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x21

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p0
.end method

.method public clearUnreadConversationCount()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    return-object p0
.end method

.method public clearUnreadMessageCount()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    return-object v0
.end method

.method public getConversationSummaryCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getConversationSummaryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method

.method public getUnreadConversationCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    return v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMyself()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRetrieveOfflineMessageStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadConversationCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasMyself()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasUnreadMessageCount()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasUnreadConversationCount()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasRetrieveOfflineMessageStatus()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getConversationSummaryCount()I

    move-result v1

    if-ge v0, v1, :cond_8

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_7

    move v0, v2

    goto :goto_0

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->hasMyself()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->addConversationSummary(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    goto :goto_0

    :sswitch_5
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasMyself()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133800(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133800(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasUnreadMessageCount()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getUnreadMessageCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasUnreadConversationCount()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getUnreadConversationCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->setUnreadConversationCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    :cond_5
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasRetrieveOfflineMessageStatus()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->setRetrieveOfflineMessageStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    :cond_6
    move-object v0, p0

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->access$133800(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public mergeMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    goto :goto_0
.end method

.method public removeConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setConversationSummary(ILcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setConversationSummary(ILcom/sgiggle/xmpp/SessionMessages$ConversationSummary;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->ensureConversationSummaryIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setMyself(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setRetrieveOfflineMessageStatus(Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p0
.end method

.method public setUnreadConversationCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadConversationCount_:I

    return-object p0
.end method

.method public setUnreadMessageCount(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->bitField0_:I

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->unreadMessageCount_:I

    return-object p0
.end method
