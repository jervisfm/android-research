.class public final Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ConversationSummaryListPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CONVERSATION_SUMMARY_FIELD_NUMBER:I = 0x3

.field public static final MYSELF_FIELD_NUMBER:I = 0x2

.field public static final RETRIEVE_OFFLINE_MESSAGE_STATUS_FIELD_NUMBER:I = 0x6

.field public static final UNREAD_CONVERSATION_COUNT_FIELD_NUMBER:I = 0x5

.field public static final UNREAD_MESSAGE_COUNT_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private conversationSummary_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

.field private retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

.field private unreadConversationCount_:I

.field private unreadMessageCount_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$133602(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$133702(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object p1
.end method

.method static synthetic access$133800(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$133802(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$133902(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I

    return p1
.end method

.method static synthetic access$134002(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I

    return p1
.end method

.method static synthetic access$134102(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;)Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object p1
.end method

.method static synthetic access$134202(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    const/4 v1, 0x0

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->OFFLINE_MESSAGE_STATUS_IDLE:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133400()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;->access$133300(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    return-object v0
.end method

.method public getConversationSummaryCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getConversationSummaryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    return-object v0
.end method

.method public getConversationSummaryOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryOrBuilder;

    return-object v0
.end method

.method public getConversationSummaryOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;

    return-object v0
.end method

.method public getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I

    invoke-static {v5, v0}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v0

    add-int/2addr v0, v2

    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    const/4 v1, 0x5

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_4

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_1
.end method

.method public getUnreadConversationCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I

    return v0
.end method

.method public getUnreadMessageCount()I
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMyself()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRetrieveOfflineMessageStatus()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadConversationCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUnreadMessageCount()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasMyself()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasUnreadMessageCount()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasUnreadConversationCount()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->hasRetrieveOfflineMessageStatus()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_8

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_8
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getConversationSummaryCount()I

    move-result v1

    if-ge v0, v1, :cond_a

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_9

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->myself_:Lcom/sgiggle/xmpp/SessionMessages$Contact;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->conversationSummary_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadMessageCount_:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->unreadConversationCount_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    :cond_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayload;->retrieveOfflineMessageStatus_:Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_5
    return-void
.end method
