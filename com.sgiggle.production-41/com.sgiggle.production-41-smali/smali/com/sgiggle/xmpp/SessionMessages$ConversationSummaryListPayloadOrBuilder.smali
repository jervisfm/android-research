.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryListPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationSummaryListPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getConversationSummary(I)Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;
.end method

.method public abstract getConversationSummaryCount()I
.end method

.method public abstract getConversationSummaryList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ConversationSummary;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMyself()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getRetrieveOfflineMessageStatus()Lcom/sgiggle/xmpp/SessionMessages$RetrieveOfflineMessageStatus;
.end method

.method public abstract getUnreadConversationCount()I
.end method

.method public abstract getUnreadMessageCount()I
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMyself()Z
.end method

.method public abstract hasRetrieveOfflineMessageStatus()Z
.end method

.method public abstract hasUnreadConversationCount()Z
.end method

.method public abstract hasUnreadMessageCount()Z
.end method
