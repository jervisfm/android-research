.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ConversationSummaryOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ConversationSummaryOrBuilder"
.end annotation


# virtual methods
.method public abstract getConversationId()Ljava/lang/String;
.end method

.method public abstract getLast()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end method

.method public abstract getPeer()Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getUnreadMessageCount()I
.end method

.method public abstract hasConversationId()Z
.end method

.method public abstract hasLast()Z
.end method

.method public abstract hasPeer()Z
.end method

.method public abstract hasUnreadMessageCount()Z
.end method
