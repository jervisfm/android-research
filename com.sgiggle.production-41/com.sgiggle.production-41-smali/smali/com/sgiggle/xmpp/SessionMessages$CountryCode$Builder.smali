.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;"
    }
.end annotation


# instance fields
.field private bitField0_:I

.field private countrycodenumber_:Ljava/lang/Object;

.field private countryid_:Ljava/lang/Object;

.field private countryisocc_:Ljava/lang/Object;

.field private countryname_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 21035
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 21177
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21213
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21249
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21285
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21036
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->maybeForceBuilderInitialization()V

    .line 21037
    return-void
.end method

.method static synthetic access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21030
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$26400()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 21076
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    .line 21077
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21078
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 21081
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21042
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 21040
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 2

    .prologue
    .line 21067
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    .line 21068
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 21069
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 21071
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 5

    .prologue
    .line 21085
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 21086
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21087
    const/4 v2, 0x0

    .line 21088
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 21089
    or-int/lit8 v2, v2, 0x1

    .line 21091
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->access$26602(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21092
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 21093
    or-int/lit8 v2, v2, 0x2

    .line 21095
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->access$26702(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21096
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 21097
    or-int/lit8 v2, v2, 0x4

    .line 21099
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->access$26802(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21100
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 21101
    or-int/lit8 v1, v2, 0x8

    .line 21103
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->access$26902(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21104
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->access$27002(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;I)I

    .line 21105
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21046
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 21047
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21048
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21049
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21050
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21051
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21052
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21053
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21054
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21055
    return-object p0
.end method

.method public clearCountrycodenumber()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21237
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21238
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21240
    return-object p0
.end method

.method public clearCountryid()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21273
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21274
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21276
    return-object p0
.end method

.method public clearCountryisocc()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21309
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21310
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisocc()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21312
    return-object p0
.end method

.method public clearCountryname()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21201
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21202
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryname()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21204
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 2

    .prologue
    .line 21059
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getCountrycodenumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21218
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21219
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 21220
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 21221
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21224
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCountryid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21254
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21255
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 21256
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 21257
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21260
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCountryisocc()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21290
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21291
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 21292
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 21293
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21296
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getCountryname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21182
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21183
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 21184
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 21185
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21188
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 21030
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 21063
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public hasCountrycodenumber()Z
    .locals 2

    .prologue
    .line 21215
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryid()Z
    .locals 2

    .prologue
    .line 21251
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryisocc()Z
    .locals 2

    .prologue
    .line 21287
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 21179
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 1

    .prologue
    .line 21126
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->hasCountrycodenumber()Z

    move-result v0

    if-nez v0, :cond_0

    .line 21128
    const/4 v0, 0x0

    .line 21130
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21030
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21030
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21030
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21138
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 21139
    sparse-switch v0, :sswitch_data_0

    .line 21144
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 21146
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 21142
    goto :goto_1

    .line 21151
    :sswitch_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21152
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    goto :goto_0

    .line 21156
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21157
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    goto :goto_0

    .line 21161
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21162
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    goto :goto_0

    .line 21166
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21167
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    goto :goto_0

    .line 21139
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21109
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 21122
    :goto_0
    return-object v0

    .line 21110
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->hasCountryname()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 21111
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryname()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 21113
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->hasCountrycodenumber()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21114
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountrycodenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 21116
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->hasCountryid()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 21117
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 21119
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->hasCountryisocc()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 21120
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisocc()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->setCountryisocc(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    :cond_4
    move-object v0, p0

    .line 21122
    goto :goto_0
.end method

.method public setCountrycodenumber(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21228
    if-nez p1, :cond_0

    .line 21229
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21231
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21232
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21234
    return-object p0
.end method

.method setCountrycodenumber(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 21243
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21244
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countrycodenumber_:Ljava/lang/Object;

    .line 21246
    return-void
.end method

.method public setCountryid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21264
    if-nez p1, :cond_0

    .line 21265
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21267
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21268
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21270
    return-object p0
.end method

.method setCountryid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 21279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21280
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryid_:Ljava/lang/Object;

    .line 21282
    return-void
.end method

.method public setCountryisocc(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21300
    if-nez p1, :cond_0

    .line 21301
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21303
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21304
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21306
    return-object p0
.end method

.method setCountryisocc(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 21315
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21316
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryisocc_:Ljava/lang/Object;

    .line 21318
    return-void
.end method

.method public setCountryname(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21192
    if-nez p1, :cond_0

    .line 21193
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 21195
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21196
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21198
    return-object p0
.end method

.method setCountryname(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 21207
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->bitField0_:I

    .line 21208
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->countryname_:Ljava/lang/Object;

    .line 21210
    return-void
.end method
