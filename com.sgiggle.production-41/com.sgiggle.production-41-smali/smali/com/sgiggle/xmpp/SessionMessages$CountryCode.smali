.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CountryCode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    }
.end annotation


# static fields
.field public static final COUNTRYCODENUMBER_FIELD_NUMBER:I = 0x2

.field public static final COUNTRYID_FIELD_NUMBER:I = 0x3

.field public static final COUNTRYISOCC_FIELD_NUMBER:I = 0x4

.field public static final COUNTRYNAME_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;


# instance fields
.field private bitField0_:I

.field private countrycodenumber_:Ljava/lang/Object;

.field private countryid_:Ljava/lang/Object;

.field private countryisocc_:Ljava/lang/Object;

.field private countryname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21324
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 21325
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->initFields()V

    .line 21326
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 20747
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 20895
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedIsInitialized:B

    .line 20925
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedSerializedSize:I

    .line 20748
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 20749
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 20895
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedIsInitialized:B

    .line 20925
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedSerializedSize:I

    .line 20749
    return-void
.end method

.method static synthetic access$26602(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$26702(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$26802(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$26902(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$27002(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 20742
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    return p1
.end method

.method private getCountrycodenumberBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 20814
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    .line 20815
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20816
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 20818
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    .line 20821
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCountryidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 20846
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    .line 20847
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20848
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 20850
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    .line 20853
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCountryisoccBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 20878
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    .line 20879
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20880
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 20882
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    .line 20885
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCountrynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 20782
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    .line 20783
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20784
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 20786
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    .line 20789
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 20753
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 20890
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    .line 20891
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    .line 20892
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    .line 20893
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    .line 20894
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21023
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26400()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1
    .parameter

    .prologue
    .line 21026
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20992
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 20993
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 20994
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    .line 20996
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21003
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 21004
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 21005
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    .line 21007
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20959
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20965
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21013
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21019
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20981
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20987
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20970
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 20976
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->access$26300(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getCountrycodenumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20800
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    .line 20801
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20802
    check-cast v0, Ljava/lang/String;

    .line 20810
    :goto_0
    return-object v0

    .line 20804
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 20806
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 20807
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20808
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countrycodenumber_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 20810
    goto :goto_0
.end method

.method public getCountryid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20832
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    .line 20833
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20834
    check-cast v0, Ljava/lang/String;

    .line 20842
    :goto_0
    return-object v0

    .line 20836
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 20838
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 20839
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20840
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 20842
    goto :goto_0
.end method

.method public getCountryisocc()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20864
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    .line 20865
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20866
    check-cast v0, Ljava/lang/String;

    .line 20874
    :goto_0
    return-object v0

    .line 20868
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 20870
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 20871
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20872
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryisocc_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 20874
    goto :goto_0
.end method

.method public getCountryname()Ljava/lang/String;
    .locals 2

    .prologue
    .line 20768
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    .line 20769
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 20770
    check-cast v0, Ljava/lang/String;

    .line 20778
    :goto_0
    return-object v0

    .line 20772
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 20774
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 20775
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 20776
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->countryname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 20778
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 20742
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 20757
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 20927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedSerializedSize:I

    .line 20928
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 20948
    :goto_0
    return v0

    .line 20930
    :cond_0
    const/4 v0, 0x0

    .line 20931
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 20932
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20935
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 20936
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20939
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 20940
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20943
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 20944
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisoccBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 20947
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasCountrycodenumber()Z
    .locals 2

    .prologue
    .line 20797
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryid()Z
    .locals 2

    .prologue
    .line 20829
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryisocc()Z
    .locals 2

    .prologue
    .line 20861
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountryname()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 20765
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 20897
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedIsInitialized:B

    .line 20898
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    .line 20905
    :goto_0
    return v0

    :cond_0
    move v0, v3

    .line 20898
    goto :goto_0

    .line 20900
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->hasCountrycodenumber()Z

    move-result v0

    if-nez v0, :cond_2

    .line 20901
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedIsInitialized:B

    move v0, v3

    .line 20902
    goto :goto_0

    .line 20904
    :cond_2
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->memoizedIsInitialized:B

    move v0, v2

    .line 20905
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20742
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21024
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 20742
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;
    .locals 1

    .prologue
    .line 21028
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 20953
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 20910
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getSerializedSize()I

    .line 20911
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 20912
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20914
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 20915
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountrycodenumberBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20917
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 20918
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20920
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 20921
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getCountryisoccBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 20923
    :cond_3
    return-void
.end method
