.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CountryCodeOrBuilder"
.end annotation


# virtual methods
.method public abstract getCountrycodenumber()Ljava/lang/String;
.end method

.method public abstract getCountryid()Ljava/lang/String;
.end method

.method public abstract getCountryisocc()Ljava/lang/String;
.end method

.method public abstract getCountryname()Ljava/lang/String;
.end method

.method public abstract hasCountrycodenumber()Z
.end method

.method public abstract hasCountryid()Z
.end method

.method public abstract hasCountryisocc()Z
.end method

.method public abstract hasCountryname()Z
.end method
