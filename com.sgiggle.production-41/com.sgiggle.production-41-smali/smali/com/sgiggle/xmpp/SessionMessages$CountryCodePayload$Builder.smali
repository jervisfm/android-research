.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 29923
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 30055
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30098
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 29924
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->maybeForceBuilderInitialization()V

    .line 29925
    return-void
.end method

.method static synthetic access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29918
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$38000()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29960
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    .line 29961
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29962
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 29965
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29930
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 29928
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 2

    .prologue
    .line 29951
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    .line 29952
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 29953
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 29955
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 5

    .prologue
    .line 29969
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 29970
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 29971
    const/4 v2, 0x0

    .line 29972
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 29973
    or-int/lit8 v2, v2, 0x1

    .line 29975
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->access$38202(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29976
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 29977
    or-int/lit8 v1, v2, 0x2

    .line 29979
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->access$38302(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 29980
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->access$38402(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;I)I

    .line 29981
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29934
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 29935
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29936
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 29937
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 29938
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 29939
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 30091
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30093
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30094
    return-object p0
.end method

.method public clearCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 30134
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30136
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30137
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 2

    .prologue
    .line 29943
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 30060
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 30103
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29918
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1

    .prologue
    .line 29947
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 30057
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountrycode()Z
    .locals 2

    .prologue
    .line 30100
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29996
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 30010
    :goto_0
    return v0

    .line 30000
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 30002
    goto :goto_0

    .line 30004
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->hasCountrycode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 30005
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 30007
    goto :goto_0

    .line 30010
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30079
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 30081
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30087
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30088
    return-object p0

    .line 30084
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public mergeCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30122
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 30124
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30130
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30131
    return-object p0

    .line 30127
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29918
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29918
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29918
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30018
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 30019
    sparse-switch v0, :sswitch_data_0

    .line 30024
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 30026
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 30022
    goto :goto_1

    .line 30031
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 30032
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30033
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 30035
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30036
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    goto :goto_0

    .line 30040
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 30041
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->hasCountrycode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 30042
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 30044
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30045
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->setCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    goto :goto_0

    .line 30019
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29985
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 29992
    :goto_0
    return-object v0

    .line 29986
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 29987
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    .line 29989
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->hasCountrycode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 29990
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    :cond_2
    move-object v0, p0

    .line 29992
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30073
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30075
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30076
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30063
    if-nez p1, :cond_0

    .line 30064
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30066
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30068
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30069
    return-object p0
.end method

.method public setCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30116
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30118
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30119
    return-object p0
.end method

.method public setCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30106
    if-nez p1, :cond_0

    .line 30107
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30109
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30111
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->bitField0_:I

    .line 30112
    return-object p0
.end method
