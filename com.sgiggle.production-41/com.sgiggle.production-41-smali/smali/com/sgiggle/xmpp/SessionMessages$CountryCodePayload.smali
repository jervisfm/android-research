.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CountryCodePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final COUNTRYCODE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30144
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    .line 30145
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->initFields()V

    .line 30146
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 29749
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 29787
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    .line 29821
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedSerializedSize:I

    .line 29750
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29744
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 29751
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 29787
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    .line 29821
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedSerializedSize:I

    .line 29751
    return-void
.end method

.method static synthetic access$38202(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29744
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$38302(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29744
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object p1
.end method

.method static synthetic access$38402(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29744
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1

    .prologue
    .line 29755
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 29784
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 29785
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 29786
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29911
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$38000()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 29914
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29880
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    .line 29881
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29882
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    .line 29884
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29891
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    .line 29892
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 29893
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    .line 29895
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29847
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29853
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29901
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29907
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29869
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29875
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29858
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 29864
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;->access$37900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 29770
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 29780
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 29744
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;
    .locals 1

    .prologue
    .line 29759
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 29823
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedSerializedSize:I

    .line 29824
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 29836
    :goto_0
    return v0

    .line 29826
    :cond_0
    const/4 v0, 0x0

    .line 29827
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 29828
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29831
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 29832
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29835
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29767
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCountrycode()Z
    .locals 2

    .prologue
    .line 29777
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 29789
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    .line 29790
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 29807
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 29790
    goto :goto_0

    .line 29792
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 29793
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 29794
    goto :goto_0

    .line 29796
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 29797
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 29798
    goto :goto_0

    .line 29800
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->hasCountrycode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 29801
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 29802
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 29803
    goto :goto_0

    .line 29806
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 29807
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29744
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29912
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 29744
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;
    .locals 1

    .prologue
    .line 29916
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 29841
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 29812
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->getSerializedSize()I

    .line 29813
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 29814
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 29816
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 29817
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayload;->countrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 29819
    :cond_1
    return-void
.end method
