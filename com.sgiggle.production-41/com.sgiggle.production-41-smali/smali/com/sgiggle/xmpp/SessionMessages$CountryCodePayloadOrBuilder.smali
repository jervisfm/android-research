.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$CountryCodePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CountryCodePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCountrycode()Z
.end method
