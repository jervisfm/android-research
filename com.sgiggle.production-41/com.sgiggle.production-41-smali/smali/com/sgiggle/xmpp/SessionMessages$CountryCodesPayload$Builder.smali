.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycodes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30382
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 30543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30586
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30675
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30383
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 30384
    return-void
.end method

.method static synthetic access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30377
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$38600()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30421
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    .line 30422
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30423
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 30426
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30389
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureCountrycodesIsMutable()V
    .locals 2

    .prologue
    .line 30589
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 30590
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30591
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30593
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 30387
    return-void
.end method


# virtual methods
.method public addAllCountrycodes(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 30656
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30657
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 30659
    return-object p0
.end method

.method public addCountrycodes(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30649
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30650
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 30652
    return-object p0
.end method

.method public addCountrycodes(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30632
    if-nez p2, :cond_0

    .line 30633
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30635
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30636
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 30638
    return-object p0
.end method

.method public addCountrycodes(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30642
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30643
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30645
    return-object p0
.end method

.method public addCountrycodes(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30622
    if-nez p1, :cond_0

    .line 30623
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30625
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30626
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30628
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 2

    .prologue
    .line 30412
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    .line 30413
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 30414
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 30416
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 5

    .prologue
    .line 30430
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 30431
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30432
    const/4 v2, 0x0

    .line 30433
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 30434
    or-int/lit8 v2, v2, 0x1

    .line 30436
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$38802(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30437
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 30438
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30439
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30441
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$38902(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Ljava/util/List;)Ljava/util/List;

    .line 30442
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 30443
    or-int/lit8 v1, v2, 0x2

    .line 30445
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$39002(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30446
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$39102(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;I)I

    .line 30447
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30393
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 30394
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30395
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30396
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30397
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30398
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30399
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30400
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30579
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30581
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30582
    return-object p0
.end method

.method public clearCountrycodes()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30662
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30665
    return-object p0
.end method

.method public clearSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30711
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30713
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30714
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2

    .prologue
    .line 30404
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 30548
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 30602
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountrycodesCount()I
    .locals 1

    .prologue
    .line 30599
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountrycodesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30596
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30377
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1

    .prologue
    .line 30408
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 30680
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 30545
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSelectedCountrycode()Z
    .locals 2

    .prologue
    .line 30677
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30472
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 30492
    :goto_0
    return v0

    .line 30476
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 30478
    goto :goto_0

    :cond_1
    move v0, v2

    .line 30480
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getCountrycodesCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 30481
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 30483
    goto :goto_0

    .line 30480
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 30486
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->hasSelectedCountrycode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 30487
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 30489
    goto :goto_0

    .line 30492
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30567
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 30569
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30575
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30576
    return-object p0

    .line 30572
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30377
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30377
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30377
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 30501
    sparse-switch v0, :sswitch_data_0

    .line 30506
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 30508
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 30504
    goto :goto_1

    .line 30513
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 30514
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 30515
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 30517
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30518
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    goto :goto_0

    .line 30522
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 30523
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30524
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->addCountrycodes(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    goto :goto_0

    .line 30528
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    .line 30529
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->hasSelectedCountrycode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 30530
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    .line 30532
    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 30533
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->setSelectedCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    goto :goto_0

    .line 30501
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30451
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 30468
    :goto_0
    return-object v0

    .line 30452
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30453
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    .line 30455
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$38900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 30456
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 30457
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$38900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    .line 30458
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30465
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->hasSelectedCountrycode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 30466
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeSelectedCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 30468
    goto :goto_0

    .line 30460
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30461
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->access$38900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public mergeSelectedCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 30699
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 30701
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30707
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30708
    return-object p0

    .line 30704
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    goto :goto_0
.end method

.method public removeCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30668
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30669
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 30671
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30561
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30563
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30564
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30551
    if-nez p1, :cond_0

    .line 30552
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30554
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30556
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30557
    return-object p0
.end method

.method public setCountrycodes(ILcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30616
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30617
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 30619
    return-object p0
.end method

.method public setCountrycodes(ILcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30606
    if-nez p2, :cond_0

    .line 30607
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30609
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->ensureCountrycodesIsMutable()V

    .line 30610
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 30612
    return-object p0
.end method

.method public setSelectedCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30693
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30695
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30696
    return-object p0
.end method

.method public setSelectedCountrycode(Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30683
    if-nez p1, :cond_0

    .line 30684
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30686
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30688
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->bitField0_:I

    .line 30689
    return-object p0
.end method
