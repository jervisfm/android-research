.class public final Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CountryCodesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final COUNTRYCODES_FIELD_NUMBER:I = 0x2

.field public static final SELECTEDCOUNTRYCODE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private countrycodes_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30721
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    .line 30722
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->initFields()V

    .line 30723
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 30173
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 30233
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    .line 30276
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedSerializedSize:I

    .line 30174
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30168
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 30175
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 30233
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    .line 30276
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedSerializedSize:I

    .line 30175
    return-void
.end method

.method static synthetic access$38802(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30168
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$38900(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 30168
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$38902(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30168
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$39002(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;Lcom/sgiggle/xmpp/SessionMessages$CountryCode;)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30168
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object p1
.end method

.method static synthetic access$39102(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30168
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1

    .prologue
    .line 30179
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 30229
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 30230
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    .line 30231
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    .line 30232
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30370
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38600()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 30373
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30339
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    .line 30340
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30341
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    .line 30343
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30350
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    .line 30351
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 30352
    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    .line 30354
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30306
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30312
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30360
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30366
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30328
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30334
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30317
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 30323
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;->access$38500(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 30194
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1
    .parameter

    .prologue
    .line 30211
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getCountrycodesCount()I
    .locals 1

    .prologue
    .line 30208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCountrycodesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30201
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    return-object v0
.end method

.method public getCountrycodesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 30215
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;

    return-object v0
.end method

.method public getCountrycodesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCodeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30205
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 30168
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;
    .locals 1

    .prologue
    .line 30183
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;

    return-object v0
.end method

.method public getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
    .locals 1

    .prologue
    .line 30225
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30278
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedSerializedSize:I

    .line 30279
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 30295
    :goto_0
    return v0

    .line 30282
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 30283
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 30286
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 30287
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 30286
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 30290
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_2

    .line 30291
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 30294
    :goto_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 30191
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSelectedCountrycode()Z
    .locals 2

    .prologue
    .line 30222
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30235
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    .line 30236
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 30259
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 30236
    goto :goto_0

    .line 30238
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 30239
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 30240
    goto :goto_0

    .line 30242
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 30243
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 30244
    goto :goto_0

    :cond_3
    move v0, v2

    .line 30246
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getCountrycodesCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 30247
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 30248
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 30249
    goto :goto_0

    .line 30246
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 30252
    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->hasSelectedCountrycode()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 30253
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCode;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    .line 30254
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 30255
    goto :goto_0

    .line 30258
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 30259
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30168
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30371
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 30168
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;
    .locals 1

    .prologue
    .line 30375
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;)Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 30300
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 30264
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->getSerializedSize()I

    .line 30265
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 30266
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 30268
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 30269
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->countrycodes_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 30268
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 30271
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 30272
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayload;->selectedCountrycode_:Lcom/sgiggle/xmpp/SessionMessages$CountryCode;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 30274
    :cond_2
    return-void
.end method
