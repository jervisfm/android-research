.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$CountryCodesPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CountryCodesPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCountrycodes(I)Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract getCountrycodesCount()I
.end method

.method public abstract getCountrycodesList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CountryCode;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getSelectedCountrycode()Lcom/sgiggle/xmpp/SessionMessages$CountryCode;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasSelectedCountrycode()Z
.end method
