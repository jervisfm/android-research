.class public final Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private deleteAll_:Z

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 42619
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 42764
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42828
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42620
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->maybeForceBuilderInitialization()V

    .line 42621
    return-void
.end method

.method static synthetic access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42614
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$54500()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42658
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    .line 42659
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42660
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 42663
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42626
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureEntriesIsMutable()V
    .locals 2

    .prologue
    .line 42831
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 42832
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42833
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42835
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 42624
    return-void
.end method


# virtual methods
.method public addAllEntries(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 42898
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42899
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 42901
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 42891
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42892
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 42894
    return-object p0
.end method

.method public addEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42874
    if-nez p2, :cond_0

    .line 42875
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42877
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42878
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 42880
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42884
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42885
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42887
    return-object p0
.end method

.method public addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42864
    if-nez p1, :cond_0

    .line 42865
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42867
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42868
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42870
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 2

    .prologue
    .line 42649
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    .line 42650
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 42651
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 42653
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 5

    .prologue
    .line 42667
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 42668
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42669
    const/4 v2, 0x0

    .line 42670
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 42671
    or-int/lit8 v2, v2, 0x1

    .line 42673
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54702(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42674
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 42675
    or-int/lit8 v1, v2, 0x2

    .line 42677
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54802(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Z)Z

    .line 42678
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 42679
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42680
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42682
    :cond_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54902(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Ljava/util/List;)Ljava/util/List;

    .line 42683
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$55002(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;I)I

    .line 42684
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42630
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 42631
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42632
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    .line 42634
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42635
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42636
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42637
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42800
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42802
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42803
    return-object p0
.end method

.method public clearDeleteAll()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42821
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42822
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    .line 42824
    return-object p0
.end method

.method public clearEntries()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42904
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42905
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42907
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2

    .prologue
    .line 42641
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 42769
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42614
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1

    .prologue
    .line 42645
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDeleteAll()Z
    .locals 1

    .prologue
    .line 42812
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    return v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 42844
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 42841
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42838
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 42766
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeleteAll()Z
    .locals 2

    .prologue
    .line 42809
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42709
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 42717
    :goto_0
    return v0

    .line 42713
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 42715
    goto :goto_0

    .line 42717
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42788
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 42790
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42796
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42797
    return-object p0

    .line 42793
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42614
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42614
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42614
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42725
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 42726
    sparse-switch v0, :sswitch_data_0

    .line 42731
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 42733
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 42729
    goto :goto_1

    .line 42738
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 42739
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42740
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 42742
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 42743
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    goto :goto_0

    .line 42747
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42748
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    goto :goto_0

    .line 42752
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;

    move-result-object v0

    .line 42753
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 42754
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->addEntries(Lcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    goto :goto_0

    .line 42726
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 42688
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 42705
    :goto_0
    return-object v0

    .line 42689
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42690
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    .line 42692
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->hasDeleteAll()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 42693
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getDeleteAll()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->setDeleteAll(Z)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    .line 42695
    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54900(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 42696
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 42697
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54900(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    .line 42698
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    :cond_3
    :goto_1
    move-object v0, p0

    .line 42705
    goto :goto_0

    .line 42700
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42701
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->access$54900(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeEntries(I)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42910
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42911
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 42913
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42782
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42784
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42785
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42772
    if-nez p1, :cond_0

    .line 42773
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42775
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42777
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42778
    return-object p0
.end method

.method public setDeleteAll(Z)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42815
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->bitField0_:I

    .line 42816
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->deleteAll_:Z

    .line 42818
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 42858
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42859
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$CallEntry$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 42861
    return-object p0
.end method

.method public setEntries(ILcom/sgiggle/xmpp/SessionMessages$CallEntry;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 42848
    if-nez p2, :cond_0

    .line 42849
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 42851
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->ensureEntriesIsMutable()V

    .line 42852
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->entries_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 42854
    return-object p0
.end method
