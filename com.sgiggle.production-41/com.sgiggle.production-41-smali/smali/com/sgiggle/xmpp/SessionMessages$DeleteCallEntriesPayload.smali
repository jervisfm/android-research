.class public final Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteCallEntriesPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final DELETE_ALL_FIELD_NUMBER:I = 0x2

.field public static final ENTRIES_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private deleteAll_:Z

.field private entries_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42920
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    .line 42921
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->initFields()V

    .line 42922
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 42422
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 42482
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    .line 42513
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedSerializedSize:I

    .line 42423
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42417
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 42424
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 42482
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    .line 42513
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedSerializedSize:I

    .line 42424
    return-void
.end method

.method static synthetic access$54702(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42417
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$54802(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42417
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z

    return p1
.end method

.method static synthetic access$54900(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 42417
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$54902(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42417
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$55002(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42417
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1

    .prologue
    .line 42428
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 42478
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 42479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z

    .line 42480
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    .line 42481
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42607
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54500()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 42610
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42576
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    .line 42577
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42578
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    .line 42580
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42587
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    .line 42588
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42589
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    .line 42591
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42543
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42549
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42597
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42603
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42565
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42571
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42554
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 42560
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;->access$54400(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 42443
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 42417
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;
    .locals 1

    .prologue
    .line 42432
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;

    return-object v0
.end method

.method public getDeleteAll()Z
    .locals 1

    .prologue
    .line 42453
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z

    return v0
.end method

.method public getEntries(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntry;
    .locals 1
    .parameter

    .prologue
    .line 42470
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntry;

    return-object v0
.end method

.method public getEntriesCount()I
    .locals 1

    .prologue
    .line 42467
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getEntriesList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42460
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getEntriesOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 42474
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;

    return-object v0
.end method

.method public getEntriesOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$CallEntryOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42464
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42515
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedSerializedSize:I

    .line 42516
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 42532
    :goto_0
    return v0

    .line 42519
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 42520
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 42523
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 42524
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    .line 42527
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 42528
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 42527
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 42531
    :cond_2
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 42532
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 42440
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDeleteAll()Z
    .locals 2

    .prologue
    .line 42450
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42484
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    .line 42485
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 42496
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 42485
    goto :goto_0

    .line 42487
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 42488
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 42489
    goto :goto_0

    .line 42491
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 42492
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 42493
    goto :goto_0

    .line 42495
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 42496
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42417
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42608
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 42417
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;
    .locals 1

    .prologue
    .line 42612
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 42537
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 42501
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->getSerializedSize()I

    .line 42502
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 42503
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 42505
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 42506
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->deleteAll_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 42508
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 42509
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteCallEntriesPayload;->entries_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 42508
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42511
    :cond_2
    return-void
.end method
