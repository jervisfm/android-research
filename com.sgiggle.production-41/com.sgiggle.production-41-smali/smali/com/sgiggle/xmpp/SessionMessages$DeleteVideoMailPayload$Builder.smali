.class public final Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

.field private videoMails_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 58044
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 58203
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58246
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 58270
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58045
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->maybeForceBuilderInitialization()V

    .line 58046
    return-void
.end method

.method static synthetic access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58039
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$74800()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 58083
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    .line 58084
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58085
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 58088
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58051
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureVideoMailsIsMutable()V
    .locals 2

    .prologue
    .line 58273
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 58274
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58275
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58277
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 58049
    return-void
.end method


# virtual methods
.method public addAllVideoMails(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 58340
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58341
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 58343
    return-object p0
.end method

.method public addVideoMails(ILcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 58333
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58334
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58336
    return-object p0
.end method

.method public addVideoMails(ILcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 58316
    if-nez p2, :cond_0

    .line 58317
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58319
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58320
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 58322
    return-object p0
.end method

.method public addVideoMails(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58326
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58327
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58329
    return-object p0
.end method

.method public addVideoMails(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58306
    if-nez p1, :cond_0

    .line 58307
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58309
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58310
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58312
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 2

    .prologue
    .line 58074
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    .line 58075
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58076
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 58078
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 5

    .prologue
    .line 58092
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 58093
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58094
    const/4 v2, 0x0

    .line 58095
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 58096
    or-int/lit8 v2, v2, 0x1

    .line 58098
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75002(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58099
    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 58100
    or-int/lit8 v1, v2, 0x2

    .line 58102
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75102(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 58103
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    .line 58104
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58105
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58107
    :cond_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75202(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Ljava/util/List;)Ljava/util/List;

    .line 58108
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75302(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;I)I

    .line 58109
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58055
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 58056
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58057
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58058
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 58059
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58060
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58061
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58062
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58239
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58241
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58242
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58263
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58264
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 58266
    return-object p0
.end method

.method public clearVideoMails()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58346
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58347
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58349
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2

    .prologue
    .line 58066
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 58208
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 58039
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1

    .prologue
    .line 58070
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 58251
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    return-object v0
.end method

.method public getVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter

    .prologue
    .line 58286
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    return-object v0
.end method

.method public getVideoMailsCount()I
    .locals 1

    .prologue
    .line 58283
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVideoMailsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58280
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 58205
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 58248
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58134
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 58152
    :goto_0
    return v0

    .line 58138
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 58140
    goto :goto_0

    .line 58142
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 58144
    goto :goto_0

    :cond_2
    move v0, v2

    .line 58146
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getVideoMailsCount()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 58147
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_3

    move v0, v2

    .line 58149
    goto :goto_0

    .line 58146
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58152
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58227
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 58229
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58235
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58236
    return-object p0

    .line 58232
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58039
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58039
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58039
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 58161
    sparse-switch v0, :sswitch_data_0

    .line 58166
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 58168
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 58164
    goto :goto_1

    .line 58173
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 58174
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 58175
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 58177
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 58178
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    goto :goto_0

    .line 58182
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 58183
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    move-result-object v0

    .line 58184
    if-eqz v0, :cond_0

    .line 58185
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58186
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    goto :goto_0

    .line 58191
    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;

    move-result-object v0

    .line 58192
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 58193
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->addVideoMails(Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    goto :goto_0

    .line 58161
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 58113
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 58130
    :goto_0
    return-object v0

    .line 58114
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58115
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    .line 58117
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58118
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    .line 58120
    :cond_2
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75200(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 58121
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 58122
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75200(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    .line 58123
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    :cond_3
    :goto_1
    move-object v0, p0

    .line 58130
    goto :goto_0

    .line 58125
    :cond_4
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58126
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->access$75200(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58352
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58353
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 58355
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58221
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58223
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58224
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58211
    if-nez p1, :cond_0

    .line 58212
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58214
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 58216
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58217
    return-object p0
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58254
    if-nez p1, :cond_0

    .line 58255
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58257
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->bitField0_:I

    .line 58258
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 58260
    return-object p0
.end method

.method public setVideoMails(ILcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 58300
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58301
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 58303
    return-object p0
.end method

.method public setVideoMails(ILcom/sgiggle/xmpp/SessionMessages$VideoMailId;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 58290
    if-nez p2, :cond_0

    .line 58291
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58293
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->ensureVideoMailsIsMutable()V

    .line 58294
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 58296
    return-object p0
.end method
