.class public final enum Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type; = null

.field public static final enum DELETE_ALL:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type; = null

.field public static final DELETE_ALL_VALUE:I = 0x2

.field public static final enum DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type; = null

.field public static final DELETE_AND_QUERY_VALUE:I = 0x1

.field public static final enum DELETE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

.field public static final DELETE_ONLY_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57808
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    const-string v1, "DELETE_ONLY"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 57809
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    const-string v1, "DELETE_AND_QUERY"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 57810
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    const-string v1, "DELETE_ALL"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ALL:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 57806
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ALL:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 57834
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 57843
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57844
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->value:I

    .line 57845
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57831
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 57821
    packed-switch p0, :pswitch_data_0

    .line 57825
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 57822
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ONLY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    goto :goto_0

    .line 57823
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    goto :goto_0

    .line 57824
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_ALL:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    goto :goto_0

    .line 57821
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 57806
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 57806
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 57818
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->value:I

    return v0
.end method
