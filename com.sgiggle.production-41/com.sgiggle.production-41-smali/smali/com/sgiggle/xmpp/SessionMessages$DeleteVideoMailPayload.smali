.class public final Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeleteVideoMailPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field public static final VIDEO_MAILS_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

.field private videoMails_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58362
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    .line 58363
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->initFields()V

    .line 58364
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 57793
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 57897
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    .line 57938
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedSerializedSize:I

    .line 57794
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57788
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 57795
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 57897
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    .line 57938
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedSerializedSize:I

    .line 57795
    return-void
.end method

.method static synthetic access$75002(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57788
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$75102(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57788
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    return-object p1
.end method

.method static synthetic access$75200(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 57788
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$75202(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57788
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$75302(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57788
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1

    .prologue
    .line 57799
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 57893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 57894
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->DELETE_AND_QUERY:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    .line 57895
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    .line 57896
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58032
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74800()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 58035
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58001
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    .line 58002
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58003
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    .line 58005
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58012
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    .line 58013
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58014
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    .line 58016
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57968
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57974
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58022
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58028
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57990
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57996
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57979
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 57985
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;->access$74700(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 57858
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 57788
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;
    .locals 1

    .prologue
    .line 57803
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57940
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedSerializedSize:I

    .line 57941
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 57957
    :goto_0
    return v0

    .line 57944
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_3

    .line 57945
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 57948
    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    .line 57949
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    move v1, v2

    move v2, v0

    .line 57952
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 57953
    const/4 v3, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 57952
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 57956
    :cond_2
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 57957
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
    .locals 1

    .prologue
    .line 57868
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    return-object v0
.end method

.method public getVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
    .locals 1
    .parameter

    .prologue
    .line 57885
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    return-object v0
.end method

.method public getVideoMailsCount()I
    .locals 1

    .prologue
    .line 57882
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVideoMailsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57875
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    return-object v0
.end method

.method public getVideoMailsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 57889
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;

    return-object v0
.end method

.method public getVideoMailsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailIdOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57879
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 57855
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 57865
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57899
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    .line 57900
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 57921
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 57900
    goto :goto_0

    .line 57902
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 57903
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57904
    goto :goto_0

    .line 57906
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    .line 57907
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57908
    goto :goto_0

    .line 57910
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 57911
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57912
    goto :goto_0

    :cond_4
    move v0, v2

    .line 57914
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getVideoMailsCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 57915
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 57916
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 57917
    goto :goto_0

    .line 57914
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57920
    :cond_6
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 57921
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57788
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58033
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 57788
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;
    .locals 1

    .prologue
    .line 58037
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;)Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 57962
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 57926
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->getSerializedSize()I

    .line 57927
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 57928
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 57930
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 57931
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 57933
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 57934
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload;->videoMails_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 57933
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57936
    :cond_2
    return-void
.end method
