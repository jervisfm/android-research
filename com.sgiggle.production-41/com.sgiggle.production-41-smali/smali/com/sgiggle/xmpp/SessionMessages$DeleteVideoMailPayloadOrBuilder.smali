.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DeleteVideoMailPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$DeleteVideoMailPayload$Type;
.end method

.method public abstract getVideoMails(I)Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;
.end method

.method public abstract getVideoMailsCount()I
.end method

.method public abstract getVideoMailsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$VideoMailId;",
            ">;"
        }
    .end annotation
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasType()Z
.end method
