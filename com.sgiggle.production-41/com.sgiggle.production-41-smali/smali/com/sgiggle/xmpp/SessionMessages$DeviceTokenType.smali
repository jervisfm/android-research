.class public final enum Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceTokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final enum DEVICE_TOKEN_ANDROID:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final DEVICE_TOKEN_ANDROID_VALUE:I = 0x2

.field public static final enum DEVICE_TOKEN_IPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final DEVICE_TOKEN_IPHONE_VALUE:I = 0x1

.field public static final enum DEVICE_TOKEN_TANGO:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final DEVICE_TOKEN_TANGO_VALUE:I = 0x0

.field public static final enum DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final DEVICE_TOKEN_UNKNOWN_VALUE:I = -0x1

.field public static final enum DEVICE_TOKEN_WINPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType; = null

.field public static final DEVICE_TOKEN_WINPHONE_VALUE:I = 0x3

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 257
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    const-string v1, "DEVICE_TOKEN_UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 258
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    const-string v1, "DEVICE_TOKEN_TANGO"

    invoke-direct {v0, v1, v4, v4, v3}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_TANGO:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 259
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    const-string v1, "DEVICE_TOKEN_IPHONE"

    invoke-direct {v0, v1, v5, v5, v4}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_IPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 260
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    const-string v1, "DEVICE_TOKEN_ANDROID"

    invoke-direct {v0, v1, v6, v6, v5}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_ANDROID:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 261
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    const-string v1, "DEVICE_TOKEN_WINPHONE"

    invoke-direct {v0, v1, v7, v7, v6}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_WINPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 255
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_TANGO:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_IPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_ANDROID:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_WINPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    .line 289
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 298
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 299
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->value:I

    .line 300
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 1
    .parameter

    .prologue
    .line 274
    packed-switch p0, :pswitch_data_0

    .line 280
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 275
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 276
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_TANGO:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 277
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_IPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 278
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_ANDROID:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 279
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->DEVICE_TOKEN_WINPHONE:Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    goto :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 1
    .parameter

    .prologue
    .line 255
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;
    .locals 1

    .prologue
    .line 255
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DeviceTokenType;->value:I

    return v0
.end method
