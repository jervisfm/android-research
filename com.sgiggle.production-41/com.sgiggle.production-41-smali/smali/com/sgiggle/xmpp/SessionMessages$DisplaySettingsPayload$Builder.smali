.class public final Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private alerts2Dismiss_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 37494
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 37625
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37668
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37495
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->maybeForceBuilderInitialization()V

    .line 37496
    return-void
.end method

.method static synthetic access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37489
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$47700()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37531
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    .line 37532
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 37533
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 37536
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37501
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAlerts2DismissIsMutable()V
    .locals 2

    .prologue
    .line 37671
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 37672
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37673
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37675
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 37499
    return-void
.end method


# virtual methods
.method public addAlerts2Dismiss(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 37731
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37732
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 37734
    return-object p0
.end method

.method public addAlerts2Dismiss(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 37714
    if-nez p2, :cond_0

    .line 37715
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37717
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37718
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 37720
    return-object p0
.end method

.method public addAlerts2Dismiss(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 37724
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37725
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37727
    return-object p0
.end method

.method public addAlerts2Dismiss(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37704
    if-nez p1, :cond_0

    .line 37705
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37707
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37708
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 37710
    return-object p0
.end method

.method public addAllAlerts2Dismiss(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 37738
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37739
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 37741
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 2

    .prologue
    .line 37522
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    .line 37523
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 37524
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 37526
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 4

    .prologue
    .line 37540
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 37541
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37542
    const/4 v2, 0x0

    .line 37543
    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_1

    .line 37544
    or-int/lit8 v1, v2, 0x1

    .line 37546
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$47902(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37547
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 37548
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37549
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37551
    :cond_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$48002(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;Ljava/util/List;)Ljava/util/List;

    .line 37552
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$48102(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;I)I

    .line 37553
    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37505
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 37506
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37507
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37508
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37509
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37510
    return-object p0
.end method

.method public clearAlerts2Dismiss()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37744
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37745
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37747
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37661
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37663
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37664
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2

    .prologue
    .line 37514
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAlerts2Dismiss(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 37684
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlerts2DismissCount()I
    .locals 1

    .prologue
    .line 37681
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlerts2DismissList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37678
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 37630
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1

    .prologue
    .line 37518
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37627
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37575
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 37583
    :goto_0
    return v0

    .line 37579
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 37581
    goto :goto_0

    .line 37583
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 37649
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 37651
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37657
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37658
    return-object p0

    .line 37654
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37489
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37489
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37489
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 37592
    sparse-switch v0, :sswitch_data_0

    .line 37597
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 37599
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 37595
    goto :goto_1

    .line 37604
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 37605
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37606
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 37608
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 37609
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    goto :goto_0

    .line 37613
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;

    move-result-object v0

    .line 37614
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 37615
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->addAlerts2Dismiss(Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    goto :goto_0

    .line 37592
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 37557
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 37571
    :goto_0
    return-object v0

    .line 37558
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37559
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    .line 37561
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$48000(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 37562
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 37563
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$48000(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    .line 37564
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    :cond_2
    :goto_1
    move-object v0, p0

    .line 37571
    goto :goto_0

    .line 37566
    :cond_3
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37567
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->access$48000(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeAlerts2Dismiss(I)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37750
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37751
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 37753
    return-object p0
.end method

.method public setAlerts2Dismiss(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 37698
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37699
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 37701
    return-object p0
.end method

.method public setAlerts2Dismiss(ILcom/sgiggle/xmpp/SessionMessages$OperationalAlert;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 37688
    if-nez p2, :cond_0

    .line 37689
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37691
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->ensureAlerts2DismissIsMutable()V

    .line 37692
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 37694
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37643
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37645
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37646
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37633
    if-nez p1, :cond_0

    .line 37634
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37636
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37638
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->bitField0_:I

    .line 37639
    return-object p0
.end method
