.class public final Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplaySettingsPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    }
.end annotation


# static fields
.field public static final ALERTS2DISMISS_FIELD_NUMBER:I = 0x2

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;


# instance fields
.field private alerts2Dismiss_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation
.end field

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37760
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    .line 37761
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->initFields()V

    .line 37762
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 37315
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 37364
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    .line 37392
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedSerializedSize:I

    .line 37316
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37310
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 37317
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 37364
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    .line 37392
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedSerializedSize:I

    .line 37317
    return-void
.end method

.method static synthetic access$47902(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37310
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$48000(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37310
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$48002(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37310
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$48102(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37310
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1

    .prologue
    .line 37321
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 37361
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 37362
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    .line 37363
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37482
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47700()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 37485
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37451
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    .line 37452
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37453
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    .line 37455
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37462
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    .line 37463
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37464
    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    .line 37466
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37424
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37472
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37478
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37440
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37446
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37429
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 37435
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;->access$47600(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAlerts2Dismiss(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;
    .locals 1
    .parameter

    .prologue
    .line 37353
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;

    return-object v0
.end method

.method public getAlerts2DismissCount()I
    .locals 1

    .prologue
    .line 37350
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAlerts2DismissList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlert;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37343
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    return-object v0
.end method

.method public getAlerts2DismissOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 37357
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;

    return-object v0
.end method

.method public getAlerts2DismissOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$OperationalAlertOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37347
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 37336
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 37310
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;
    .locals 1

    .prologue
    .line 37325
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37394
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedSerializedSize:I

    .line 37395
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 37407
    :goto_0
    return v0

    .line 37398
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_2

    .line 37399
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 37402
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 37403
    const/4 v3, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 37402
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 37406
    :cond_1
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedSerializedSize:I

    move v0, v2

    .line 37407
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 37333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 37366
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    .line 37367
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 37378
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 37367
    goto :goto_0

    .line 37369
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 37370
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 37371
    goto :goto_0

    .line 37373
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 37374
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 37375
    goto :goto_0

    .line 37377
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 37378
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37310
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37483
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 37310
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;
    .locals 1

    .prologue
    .line 37487
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 37412
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 37383
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->getSerializedSize()I

    .line 37384
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 37385
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 37387
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 37388
    const/4 v2, 0x2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplaySettingsPayload;->alerts2Dismiss_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 37387
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 37390
    :cond_1
    return-void
.end method
