.class public final Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisplayTangoPushPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x4

.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLID_FIELD_NUMBER:I = 0x5

.field public static final DISPLAYNAME_FIELD_NUMBER:I = 0x3

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;


# instance fields
.field private accountid_:Ljava/lang/Object;

.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private callid_:Ljava/lang/Object;

.field private displayname_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$106802(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$106902(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;)Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    return-object p1
.end method

.method static synthetic access$107002(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$107102(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$107202(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$107302(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    return p1
.end method

.method private getAccountidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getCallidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    return-object v0
.end method

.method private getDisplaynameBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->TP_DISPLAY_INCOMING_CALL:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106600()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;->access$106500(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->accountid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCallid()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->callid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;

    return-object v0
.end method

.method public getDisplayname()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->displayname_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->getNumber()I

    move-result v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    const/4 v1, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    return-object v0
.end method

.method public hasAccountid()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCallid()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDisplayname()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->hasDisplayname()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->hasAccountid()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->hasCallid()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;)Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getDisplaynameBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getAccountidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    const/4 v0, 0x5

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayload;->getCallidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_4
    return-void
.end method
