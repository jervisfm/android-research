.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$DisplayTangoPushPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DisplayTangoPushPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountid()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getCallid()Ljava/lang/String;
.end method

.method public abstract getDisplayname()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$TangoPushDisplayType;
.end method

.method public abstract hasAccountid()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasCallid()Z
.end method

.method public abstract hasDisplayname()Z
.end method

.method public abstract hasType()Z
.end method
