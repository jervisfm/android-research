.class public final Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ErrorPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ErrorPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Ljava/lang/Object;

.field private type_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2156
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 2300
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2343
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2157
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->maybeForceBuilderInitialization()V

    .line 2158
    return-void
.end method

.method static synthetic access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2151
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2195
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    .line 2196
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2197
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 2200
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2163
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 2161
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 2

    .prologue
    .line 2186
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    .line 2187
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2188
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 2190
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 5

    .prologue
    .line 2204
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 2205
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2206
    const/4 v2, 0x0

    .line 2207
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 2208
    or-int/lit8 v2, v2, 0x1

    .line 2210
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->access$1402(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2211
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 2212
    or-int/lit8 v2, v2, 0x2

    .line 2214
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->access$1502(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 2216
    or-int/lit8 v1, v2, 0x4

    .line 2218
    :goto_0
    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->access$1602(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;I)I

    .line 2219
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->access$1702(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;I)I

    .line 2220
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2167
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 2168
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2169
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2170
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2171
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2172
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    .line 2173
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2174
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2336
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2338
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2339
    return-object p0
.end method

.method public clearError()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2367
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2368
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getError()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2370
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2393
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2394
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    .line 2396
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 2

    .prologue
    .line 2178
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 2305
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1

    .prologue
    .line 2182
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2348
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2349
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2350
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 2351
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2354
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 2384
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2302
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 2345
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 2381
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2238
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2254
    :goto_0
    return v0

    .line 2242
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->hasError()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2244
    goto :goto_0

    .line 2246
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2248
    goto :goto_0

    .line 2250
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 2252
    goto :goto_0

    .line 2254
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 2324
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 2326
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2332
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2333
    return-object p0

    .line 2329
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2151
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2151
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2151
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2262
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 2263
    sparse-switch v0, :sswitch_data_0

    .line 2268
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 2270
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 2266
    goto :goto_1

    .line 2275
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 2276
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2277
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 2279
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 2280
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    goto :goto_0

    .line 2284
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2285
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    goto :goto_0

    .line 2289
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2290
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    goto :goto_0

    .line 2263
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2224
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 2234
    :goto_0
    return-object v0

    .line 2225
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2226
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    .line 2228
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasError()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2229
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getError()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->setError(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    .line 2231
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2232
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->setType(I)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    :cond_3
    move-object v0, p0

    .line 2234
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2318
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2320
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2321
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2308
    if-nez p1, :cond_0

    .line 2309
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2311
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2313
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2314
    return-object p0
.end method

.method public setError(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2358
    if-nez p1, :cond_0

    .line 2359
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 2361
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2362
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2364
    return-object p0
.end method

.method setError(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 2373
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2374
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->error_:Ljava/lang/Object;

    .line 2376
    return-void
.end method

.method public setType(I)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->bitField0_:I

    .line 2388
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->type_:I

    .line 2390
    return-object p0
.end method
