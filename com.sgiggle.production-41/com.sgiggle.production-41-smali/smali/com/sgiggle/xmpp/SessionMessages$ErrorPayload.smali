.class public final Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ErrorPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ErrorPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ERROR_FIELD_NUMBER:I = 0x2

.field public static final TYPE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private error_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private type_:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2403
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    .line 2404
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->initFields()V

    .line 2405
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1940
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 2011
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    .line 2050
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedSerializedSize:I

    .line 1941
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1935
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 1942
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 2011
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    .line 2050
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedSerializedSize:I

    .line 1942
    return-void
.end method

.method static synthetic access$1402(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1935
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1935
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1935
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I

    return p1
.end method

.method static synthetic access$1702(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1935
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1

    .prologue
    .line 1946
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    return-object v0
.end method

.method private getErrorBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 1985
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    .line 1986
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1987
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 1989
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    .line 1992
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 2007
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 2008
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    .line 2009
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I

    .line 2010
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2144
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1200()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 2147
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2113
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    .line 2114
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2115
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    .line 2117
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2124
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    .line 2125
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2126
    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    .line 2128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2080
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2086
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2134
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2140
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2102
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2108
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2091
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 2097
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;->access$1100(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 1935
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;
    .locals 1

    .prologue
    .line 1950
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;

    return-object v0
.end method

.method public getError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1971
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    .line 1972
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1973
    check-cast v0, Ljava/lang/String;

    .line 1981
    :goto_0
    return-object v0

    .line 1975
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 1977
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 1978
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1979
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->error_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 1981
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2052
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedSerializedSize:I

    .line 2053
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2069
    :goto_0
    return v0

    .line 2055
    :cond_0
    const/4 v0, 0x0

    .line 2056
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 2057
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2060
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 2061
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2064
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 2065
    const/4 v1, 0x3

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2068
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 2003
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I

    return v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1958
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasError()Z
    .locals 2

    .prologue
    .line 1968
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 2000
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2013
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    .line 2014
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 2033
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 2014
    goto :goto_0

    .line 2016
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2017
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2018
    goto :goto_0

    .line 2020
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasError()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2021
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2022
    goto :goto_0

    .line 2024
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2025
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2026
    goto :goto_0

    .line 2028
    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2029
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 2030
    goto :goto_0

    .line 2032
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 2033
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1935
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2145
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 1935
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;
    .locals 1

    .prologue
    .line 2149
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;)Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 2074
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 2038
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getSerializedSize()I

    .line 2039
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2040
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 2042
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 2043
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->getErrorBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 2045
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 2046
    const/4 v0, 0x3

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorPayload;->type_:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 2048
    :cond_2
    return-void
.end method
