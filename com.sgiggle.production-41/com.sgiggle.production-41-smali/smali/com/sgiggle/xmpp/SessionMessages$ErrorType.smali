.class public final enum Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ErrorType;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final enum CLIENT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final CLIENT_ERROR_VALUE:I = 0x4

.field public static final enum NETWORK_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final NETWORK_ERROR_VALUE:I = 0x2

.field public static final enum NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final NONE_ERROR_VALUE:I = 0x0

.field public static final enum SERVER_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final SERVER_ERROR_VALUE:I = 0x3

.field public static final enum TIMEOUT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType; = null

.field public static final TIMEOUT_ERROR_VALUE:I = 0x1

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ErrorType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 583
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v1, "NONE_ERROR"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 584
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v1, "TIMEOUT_ERROR"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->TIMEOUT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 585
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v1, "NETWORK_ERROR"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NETWORK_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 586
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->SERVER_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 587
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    const-string v1, "CLIENT_ERROR"

    invoke-direct {v0, v1, v6, v6, v6}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->CLIENT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 581
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->TIMEOUT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NETWORK_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->SERVER_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->CLIENT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    .line 615
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ErrorType$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 624
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 625
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->value:I

    .line 626
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$ErrorType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 612
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 1
    .parameter

    .prologue
    .line 600
    packed-switch p0, :pswitch_data_0

    .line 606
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 601
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NONE_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    .line 602
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->TIMEOUT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    .line 603
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->NETWORK_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    .line 604
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->SERVER_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    .line 605
    :pswitch_4
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->CLIENT_ERROR:Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    goto :goto_0

    .line 600
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 1
    .parameter

    .prologue
    .line 581
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$ErrorType;
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$ErrorType;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 597
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ErrorType;->value:I

    return v0
.end method
