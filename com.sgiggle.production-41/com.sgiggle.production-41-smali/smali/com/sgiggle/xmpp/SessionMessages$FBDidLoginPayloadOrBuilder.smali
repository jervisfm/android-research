.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$FBDidLoginPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FBDidLoginPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccessToken()Ljava/lang/String;
.end method

.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getExpirationTime()J
.end method

.method public abstract hasAccessToken()Z
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasExpirationTime()Z
.end method
