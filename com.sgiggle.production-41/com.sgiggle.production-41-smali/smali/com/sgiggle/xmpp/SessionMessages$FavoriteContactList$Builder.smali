.class public final Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;",
        "Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactListOrBuilder;"
    }
.end annotation


# instance fields
.field private accountid_:Lcom/google/protobuf/LazyStringList;

.field private bitField0_:I


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$109300()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;-><init>()V

    return-object v0
.end method

.method private ensureAccountidIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Lcom/google/protobuf/LazyStringArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v0, v1}, Lcom/google/protobuf/LazyStringArrayList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAccountid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method addAccountid(Lcom/google/protobuf/ByteString;)V
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    return-void
.end method

.method public addAllAccountid(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 3

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/google/protobuf/UnmodifiableLazyStringList;

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-direct {v1, v2}, Lcom/google/protobuf/UnmodifiableLazyStringList;-><init>(Lcom/google/protobuf/LazyStringList;)V

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v1, v1, -0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    :cond_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->access$109502(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearAccountid()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getAccountid(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAccountidCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getAccountidList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public final isInitialized()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->add(Lcom/google/protobuf/ByteString;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->access$109500(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->access$109500(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/google/protobuf/LazyStringList;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->bitField0_:I

    :cond_1
    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->access$109500(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/google/protobuf/LazyStringList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/protobuf/LazyStringList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public setAccountid(ILjava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->ensureAccountidIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1, p2}, Lcom/google/protobuf/LazyStringList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method
