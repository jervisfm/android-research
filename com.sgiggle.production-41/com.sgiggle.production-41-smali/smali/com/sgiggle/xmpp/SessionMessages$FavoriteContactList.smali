.class public final Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactListOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FavoriteContactList"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    }
.end annotation


# static fields
.field public static final ACCOUNTID_FIELD_NUMBER:I = 0x1

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;


# instance fields
.field private accountid_:Lcom/google/protobuf/LazyStringList;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;-><init>(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$109500(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/google/protobuf/LazyStringList;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method static synthetic access$109502(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;Lcom/google/protobuf/LazyStringList;)Lcom/google/protobuf/LazyStringList;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    return-object p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    sget-object v0, Lcom/google/protobuf/LazyStringArrayList;->EMPTY:Lcom/google/protobuf/LazyStringList;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109300()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;->access$109200(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getAccountid(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0, p1}, Lcom/google/protobuf/LazyStringList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getAccountidCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v0}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v0

    return v0
.end method

.method public getAccountidList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v3

    move v1, v3

    :goto_1
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSizeNoTag(Lcom/google/protobuf/ByteString;)I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int v0, v3, v1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->getAccountidList()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x1

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;)Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->getSerializedSize()I

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v1}, Lcom/google/protobuf/LazyStringList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactList;->accountid_:Lcom/google/protobuf/LazyStringList;

    invoke-interface {v2, v0}, Lcom/google/protobuf/LazyStringList;->getByteString(I)Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
