.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$FavoriteContactListOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FavoriteContactListOrBuilder"
.end annotation


# virtual methods
.method public abstract getAccountid(I)Ljava/lang/String;
.end method

.method public abstract getAccountidCount()I
.end method

.method public abstract getAccountidList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method
