.class public final Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private byTimeout_:Z

.field private fromServer_:Z

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 39457
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 39611
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39675
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39458
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->maybeForceBuilderInitialization()V

    .line 39459
    return-void
.end method

.method static synthetic access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39452
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$50400()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39498
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    .line 39499
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39500
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 39503
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39464
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 39462
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 2

    .prologue
    .line 39489
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    .line 39490
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 39491
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 39493
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 5

    .prologue
    .line 39507
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 39508
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39509
    const/4 v2, 0x0

    .line 39510
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 39511
    or-int/lit8 v2, v2, 0x1

    .line 39513
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->access$50602(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39514
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 39515
    or-int/lit8 v2, v2, 0x2

    .line 39517
    :cond_1
    iget-boolean v3, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->access$50702(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Z)Z

    .line 39518
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 39519
    or-int/lit8 v2, v2, 0x4

    .line 39521
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->access$50802(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39522
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 39523
    or-int/lit8 v1, v2, 0x8

    .line 39525
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->access$50902(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Z)Z

    .line 39526
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->access$51002(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;I)I

    .line 39527
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39468
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 39469
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39470
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39471
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    .line 39472
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39473
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39474
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39475
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    .line 39476
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39477
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39647
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39649
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39650
    return-object p0
.end method

.method public clearByTimeout()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39713
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39714
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    .line 39716
    return-object p0
.end method

.method public clearFromServer()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39668
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39669
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    .line 39671
    return-object p0
.end method

.method public clearSource()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39692
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39693
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39695
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 2

    .prologue
    .line 39481
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 39616
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getByTimeout()Z
    .locals 1

    .prologue
    .line 39704
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39452
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1

    .prologue
    .line 39485
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public getFromServer()Z
    .locals 1

    .prologue
    .line 39659
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    return v0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1

    .prologue
    .line 39680
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39613
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasByTimeout()Z
    .locals 2

    .prologue
    .line 39701
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromServer()Z
    .locals 2

    .prologue
    .line 39656
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    .prologue
    .line 39677
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39548
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 39556
    :goto_0
    return v0

    .line 39552
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 39554
    goto :goto_0

    .line 39556
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 39635
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 39637
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39643
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39644
    return-object p0

    .line 39640
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39452
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39452
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39452
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39564
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 39565
    sparse-switch v0, :sswitch_data_0

    .line 39570
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 39572
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 39568
    goto :goto_1

    .line 39577
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 39578
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 39579
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 39581
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 39582
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    goto :goto_0

    .line 39586
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39587
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    goto :goto_0

    .line 39591
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 39592
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    move-result-object v0

    .line 39593
    if-eqz v0, :cond_0

    .line 39594
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39595
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    goto :goto_0

    .line 39600
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39601
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    goto :goto_0

    .line 39565
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39531
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 39544
    :goto_0
    return-object v0

    .line 39532
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 39533
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    .line 39535
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->hasFromServer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 39536
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getFromServer()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->setFromServer(Z)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    .line 39538
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->hasSource()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 39539
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    .line 39541
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->hasByTimeout()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 39542
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getByTimeout()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->setByTimeout(Z)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    :cond_4
    move-object v0, p0

    .line 39544
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39629
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39631
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39632
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39619
    if-nez p1, :cond_0

    .line 39620
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39622
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39624
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39625
    return-object p0
.end method

.method public setByTimeout(Z)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39707
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39708
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->byTimeout_:Z

    .line 39710
    return-object p0
.end method

.method public setFromServer(Z)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39662
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39663
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->fromServer_:Z

    .line 39665
    return-object p0
.end method

.method public setSource(Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39683
    if-nez p1, :cond_0

    .line 39684
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 39686
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->bitField0_:I

    .line 39687
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39689
    return-object p0
.end method
