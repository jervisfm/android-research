.class public final Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FilteredContactsUpdatePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final BY_TIMEOUT_FIELD_NUMBER:I = 0x4

.field public static final FROM_SERVER_FIELD_NUMBER:I = 0x2

.field public static final SOURCE_FIELD_NUMBER:I = 0x3

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private byTimeout_:Z

.field private fromServer_:Z

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39723
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    .line 39724
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->initFields()V

    .line 39725
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 39253
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 39313
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    .line 39347
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedSerializedSize:I

    .line 39254
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 39255
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 39313
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    .line 39347
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedSerializedSize:I

    .line 39255
    return-void
.end method

.method static synthetic access$50602(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$50702(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z

    return p1
.end method

.method static synthetic access$50802(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;)Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object p1
.end method

.method static synthetic access$50902(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z

    return p1
.end method

.method static synthetic access$51002(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39248
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1

    .prologue
    .line 39259
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    return-object v0
.end method

.method private initFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39308
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 39309
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z

    .line 39310
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->CONTACTS_SOURCE_UNKNOWN:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    .line 39311
    iput-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z

    .line 39312
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39445
    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50400()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 39448
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39414
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    .line 39415
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39416
    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    .line 39418
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39425
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    .line 39426
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39427
    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    .line 39429
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39381
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39387
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39435
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39441
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39403
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 39409
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39392
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 39398
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;->access$50300(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 39274
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getByTimeout()Z
    .locals 1

    .prologue
    .line 39304
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 39248
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;
    .locals 1

    .prologue
    .line 39263
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;

    return-object v0
.end method

.method public getFromServer()Z
    .locals 1

    .prologue
    .line 39284
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 39349
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedSerializedSize:I

    .line 39350
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 39370
    :goto_0
    return v0

    .line 39352
    :cond_0
    const/4 v0, 0x0

    .line 39353
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 39354
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 39357
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 39358
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39361
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 39362
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 39365
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 39366
    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 39369
    :cond_4
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
    .locals 1

    .prologue
    .line 39294
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 39271
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasByTimeout()Z
    .locals 2

    .prologue
    .line 39301
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasFromServer()Z
    .locals 2

    .prologue
    .line 39281
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSource()Z
    .locals 2

    .prologue
    .line 39291
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39315
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    .line 39316
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 39327
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 39316
    goto :goto_0

    .line 39318
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 39319
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 39320
    goto :goto_0

    .line 39322
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 39323
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 39324
    goto :goto_0

    .line 39326
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 39327
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39248
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39446
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 39248
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;
    .locals 1

    .prologue
    .line 39450
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;)Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 39375
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 39332
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->getSerializedSize()I

    .line 39333
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 39334
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 39336
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 39337
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->fromServer_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 39339
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 39340
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->source_:Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 39342
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 39343
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayload;->byTimeout_:Z

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 39345
    :cond_3
    return-void
.end method
