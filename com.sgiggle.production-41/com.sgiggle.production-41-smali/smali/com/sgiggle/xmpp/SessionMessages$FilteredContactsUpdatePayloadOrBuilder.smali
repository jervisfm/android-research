.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$FilteredContactsUpdatePayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "FilteredContactsUpdatePayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getByTimeout()Z
.end method

.method public abstract getFromServer()Z
.end method

.method public abstract getSource()Lcom/sgiggle/xmpp/SessionMessages$ContactsSource;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasByTimeout()Z
.end method

.method public abstract hasFromServer()Z
.end method

.method public abstract hasSource()Z
.end method
