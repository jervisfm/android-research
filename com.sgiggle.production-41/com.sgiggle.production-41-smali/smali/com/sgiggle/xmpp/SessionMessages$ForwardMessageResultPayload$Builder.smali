.class public final Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

.field private smsContacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

.field private viewUrl_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->FORWARD_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->maybeForceBuilderInitialization()V

    return-void
.end method

.method static synthetic access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$136800()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureSmsContactsIsMutable()V
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    return-void
.end method


# virtual methods
.method public addAllSmsContacts(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;"
        }
    .end annotation

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    return-object p0
.end method

.method public addSmsContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addSmsContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    return-object p0
.end method

.method public addSmsContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public addSmsContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 5

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    const/4 v2, 0x0

    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    or-int/lit8 v2, v2, 0x1

    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137002(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    or-int/lit8 v2, v2, 0x2

    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137102(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    or-int/lit8 v2, v2, 0x4

    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137202(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_4

    or-int/lit8 v1, v2, 0x8

    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137302(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Ljava/lang/Object;)Ljava/lang/Object;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    iget v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v2, v2, -0x11

    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    :cond_3
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137402(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Ljava/util/List;)Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137502(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;I)I

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->FORWARD_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearMessage()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearSmsContacts()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->FORWARD_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    return-object p0
.end method

.method public clearViewUrl()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getViewUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getSmsContactsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSmsContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    return-object v0
.end method

.method public getViewUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasViewUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->hasType()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->hasMessage()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getSmsContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_6

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    goto :goto_1

    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    goto :goto_0

    :sswitch_3
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->hasMessage()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    :cond_2
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    goto :goto_0

    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    goto :goto_0

    :sswitch_5
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;

    move-result-object v0

    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->addSmsContacts(Lcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasMessage()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasViewUrl()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getViewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->setViewUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    :cond_4
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137400(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137400(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    :cond_5
    :goto_1
    move-object v0, p0

    goto :goto_0

    :cond_6
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->access$137400(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public mergeMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    goto :goto_0
.end method

.method public removeSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setMessage(Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    return-object p0
.end method

.method public setSmsContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 2

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Contact$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setSmsContacts(ILcom/sgiggle/xmpp/SessionMessages$Contact;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->ensureSmsContactsIsMutable()V

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    return-object p0
.end method

.method public setViewUrl(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    return-object p0
.end method

.method setViewUrl(Lcom/google/protobuf/ByteString;)V
    .locals 1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->bitField0_:I

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->viewUrl_:Ljava/lang/Object;

    return-void
.end method
