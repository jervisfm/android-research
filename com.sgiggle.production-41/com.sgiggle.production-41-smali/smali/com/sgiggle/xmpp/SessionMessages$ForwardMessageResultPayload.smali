.class public final Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ForwardMessageResultPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final MESSAGE_FIELD_NUMBER:I = 0x3

.field public static final SMS_CONTACTS_FIELD_NUMBER:I = 0x5

.field public static final TYPE_FIELD_NUMBER:I = 0x2

.field public static final VIEW_URL_FIELD_NUMBER:I = 0x4

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

.field private smsContacts_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end field

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

.field private viewUrl_:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$137002(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$137102(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    return-object p1
.end method

.method static synthetic access$137202(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;)Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object p1
.end method

.method static synthetic access$137302(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$137400(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$137402(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$137502(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    return-object v0
.end method

.method private getViewUrlBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->FORWARD_SUCCESS:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136800()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;->access$136700(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;

    return-object v0
.end method

.method public getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v3

    :goto_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->getNumber()I

    move-result v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getViewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_3
    move v1, v3

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    const/4 v3, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    :cond_4
    iput v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedSerializedSize:I

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public getSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Contact;

    return-object v0
.end method

.method public getSmsContactsCount()I
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getSmsContactsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    return-object v0
.end method

.method public getSmsContactsOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;

    return-object v0
.end method

.method public getSmsContactsOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$ContactOrBuilder;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    return-object v0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    return-object v0
.end method

.method public getViewUrl()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->viewUrl_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasMessage()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasViewUrl()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasType()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->hasMessage()Z

    move-result v0

    if-nez v0, :cond_4

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_5

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_6

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v2

    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getSmsContactsCount()I

    move-result v1

    if-ge v0, v1, :cond_8

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Contact;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_7

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_8
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;)Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;->getNumber()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->message_:Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->getViewUrlBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    :cond_3
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayload;->smsContacts_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    return-void
.end method
