.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ForwardMessageResultPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getMessage()Lcom/sgiggle/xmpp/SessionMessages$ConversationMessage;
.end method

.method public abstract getSmsContacts(I)Lcom/sgiggle/xmpp/SessionMessages$Contact;
.end method

.method public abstract getSmsContactsCount()I
.end method

.method public abstract getSmsContactsList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Contact;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$ForwardMessageResultType;
.end method

.method public abstract getViewUrl()Ljava/lang/String;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasMessage()Z
.end method

.method public abstract hasType()Z
.end method

.method public abstract hasViewUrl()Z
.end method
