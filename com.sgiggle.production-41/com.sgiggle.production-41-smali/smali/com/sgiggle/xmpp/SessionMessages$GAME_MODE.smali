.class public final enum Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GAME_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE; = null

.field public static final enum GAME_OFF:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE; = null

.field public static final enum GAME_OFF_ON_BACKGROUND:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE; = null

.field public static final GAME_OFF_ON_BACKGROUND_VALUE:I = 0x2

.field public static final GAME_OFF_VALUE:I = 0x1

.field public static final enum GAME_ON:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

.field public static final GAME_ON_VALUE:I

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    const-string v1, "GAME_ON"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_ON:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    .line 111
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    const-string v1, "GAME_OFF"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    .line 112
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    const-string v1, "GAME_OFF_ON_BACKGROUND"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF_ON_BACKGROUND:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    .line 108
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_ON:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF_ON_BACKGROUND:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    .line 136
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->value:I

    .line 147
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;
    .locals 1
    .parameter

    .prologue
    .line 123
    packed-switch p0, :pswitch_data_0

    .line 127
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_ON:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    goto :goto_0

    .line 125
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    goto :goto_0

    .line 126
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->GAME_OFF_ON_BACKGROUND:Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;
    .locals 1
    .parameter

    .prologue
    .line 108
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GAME_MODE;->value:I

    return v0
.end method
