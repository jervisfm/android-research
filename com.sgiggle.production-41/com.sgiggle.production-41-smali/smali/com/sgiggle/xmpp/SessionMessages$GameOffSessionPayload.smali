.class public final Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GameOffSessionPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final ONBACKGROUND_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private onBackground_:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->initFields()V

    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedSerializedSize:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedSerializedSize:I

    return-void
.end method

.method static synthetic access$147202(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0

    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$147302(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->onBackground_:Z

    return p1
.end method

.method static synthetic access$147402(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;I)I
    .locals 0

    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    return-object v0
.end method

.method private initFields()V
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->onBackground_:Z

    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    .locals 1

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$147000()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;->access$146900(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;
    .locals 1

    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;

    return-object v0
.end method

.method public getOnBackground()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->onBackground_:Z

    return v0
.end method

.method public getSerializedSize()I
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedSerializedSize:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->onBackground_:Z

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOnBackground()Z
    .locals 2

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    move v0, v2

    goto :goto_0

    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->memoizedIsInitialized:B

    move v0, v3

    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    .locals 1

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;
    .locals 1

    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;)Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->getSerializedSize()I

    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GameOffSessionPayload;->onBackground_:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    :cond_1
    return-void
.end method
