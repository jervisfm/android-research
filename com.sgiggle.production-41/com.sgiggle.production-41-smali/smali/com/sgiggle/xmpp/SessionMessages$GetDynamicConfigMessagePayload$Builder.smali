.class public final Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private caller_:Z

.field private jid_:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36488
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 36628
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36671
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36489
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->maybeForceBuilderInitialization()V

    .line 36490
    return-void
.end method

.method static synthetic access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36483
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$46300()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36527
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    .line 36528
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36529
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 36532
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36495
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 36493
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 2

    .prologue
    .line 36518
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    .line 36519
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36520
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 36522
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 5

    .prologue
    .line 36536
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 36537
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36538
    const/4 v2, 0x0

    .line 36539
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 36540
    or-int/lit8 v2, v2, 0x1

    .line 36542
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->access$46502(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36543
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 36544
    or-int/lit8 v2, v2, 0x2

    .line 36546
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->access$46602(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36547
    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2

    .line 36548
    or-int/lit8 v1, v2, 0x4

    .line 36550
    :goto_0
    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->access$46702(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Z)Z

    .line 36551
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->access$46802(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;I)I

    .line 36552
    return-object v0

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36499
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 36500
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36501
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36502
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36503
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    .line 36505
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36506
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36664
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36666
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36667
    return-object p0
.end method

.method public clearCaller()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36721
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36722
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    .line 36724
    return-object p0
.end method

.method public clearJid()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36695
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36696
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getJid()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36698
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 2

    .prologue
    .line 36510
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 36633
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCaller()Z
    .locals 1

    .prologue
    .line 36712
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 36483
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1

    .prologue
    .line 36514
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36676
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36677
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 36678
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 36679
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36682
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36630
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCaller()Z
    .locals 2

    .prologue
    .line 36709
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    .line 36673
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36570
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 36582
    :goto_0
    return v0

    .line 36574
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->hasCaller()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 36576
    goto :goto_0

    .line 36578
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 36580
    goto :goto_0

    .line 36582
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 36652
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 36654
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36660
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36661
    return-object p0

    .line 36657
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36483
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36483
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36483
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36590
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 36591
    sparse-switch v0, :sswitch_data_0

    .line 36596
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 36598
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 36594
    goto :goto_1

    .line 36603
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 36604
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 36605
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 36607
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 36608
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    goto :goto_0

    .line 36612
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36613
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    goto :goto_0

    .line 36617
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36618
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBool()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    goto :goto_0

    .line 36591
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36556
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 36566
    :goto_0
    return-object v0

    .line 36557
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36558
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    .line 36560
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->hasJid()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36561
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getJid()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    .line 36563
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->hasCaller()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 36564
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getCaller()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->setCaller(Z)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    :cond_3
    move-object v0, p0

    .line 36566
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36646
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36648
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36649
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36636
    if-nez p1, :cond_0

    .line 36637
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36639
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36641
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36642
    return-object p0
.end method

.method public setCaller(Z)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36715
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36716
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->caller_:Z

    .line 36718
    return-object p0
.end method

.method public setJid(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36686
    if-nez p1, :cond_0

    .line 36687
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36689
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36690
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36692
    return-object p0
.end method

.method setJid(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 36701
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->bitField0_:I

    .line 36702
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->jid_:Ljava/lang/Object;

    .line 36704
    return-void
.end method
