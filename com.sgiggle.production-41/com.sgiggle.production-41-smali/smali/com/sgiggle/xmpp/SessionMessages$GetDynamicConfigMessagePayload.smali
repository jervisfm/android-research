.class public final Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetDynamicConfigMessagePayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CALLER_FIELD_NUMBER:I = 0x3

.field public static final JID_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private caller_:Z

.field private jid_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36731
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    .line 36732
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->initFields()V

    .line 36733
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 36276
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 36347
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    .line 36382
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedSerializedSize:I

    .line 36277
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36271
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 36278
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 36347
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    .line 36382
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedSerializedSize:I

    .line 36278
    return-void
.end method

.method static synthetic access$46502(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36271
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$46602(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36271
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$46702(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36271
    iput-boolean p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z

    return p1
.end method

.method static synthetic access$46802(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36271
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1

    .prologue
    .line 36282
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    return-object v0
.end method

.method private getJidBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 36321
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    .line 36322
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36323
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 36325
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    .line 36328
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 36343
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 36344
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    .line 36345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z

    .line 36346
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36476
    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46300()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 36479
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36445
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    .line 36446
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36447
    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    .line 36449
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36456
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    .line 36457
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36458
    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    .line 36460
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36412
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36418
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36466
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36472
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36434
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36440
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36423
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 36429
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;->access$46200(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 36297
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCaller()Z
    .locals 1

    .prologue
    .line 36339
    iget-boolean v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z

    return v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 36271
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;
    .locals 1

    .prologue
    .line 36286
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;

    return-object v0
.end method

.method public getJid()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36307
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    .line 36308
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 36309
    check-cast v0, Ljava/lang/String;

    .line 36317
    :goto_0
    return-object v0

    .line 36311
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 36313
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 36314
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 36315
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->jid_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 36317
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 36384
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedSerializedSize:I

    .line 36385
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 36401
    :goto_0
    return v0

    .line 36387
    :cond_0
    const/4 v0, 0x0

    .line 36388
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 36389
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36392
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 36393
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 36396
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 36397
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 36400
    :cond_3
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36294
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCaller()Z
    .locals 2

    .prologue
    .line 36336
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasJid()Z
    .locals 2

    .prologue
    .line 36304
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 36349
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    .line 36350
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 36365
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 36350
    goto :goto_0

    .line 36352
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 36353
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36354
    goto :goto_0

    .line 36356
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->hasCaller()Z

    move-result v0

    if-nez v0, :cond_3

    .line 36357
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36358
    goto :goto_0

    .line 36360
    :cond_3
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_4

    .line 36361
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    move v0, v2

    .line 36362
    goto :goto_0

    .line 36364
    :cond_4
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->memoizedIsInitialized:B

    move v0, v3

    .line 36365
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36271
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36477
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 36271
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;
    .locals 1

    .prologue
    .line 36481
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;)Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 36406
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 36370
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getSerializedSize()I

    .line 36371
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 36372
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 36374
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 36375
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->getJidBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 36377
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 36378
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/sgiggle/xmpp/SessionMessages$GetDynamicConfigMessagePayload;->caller_:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBool(IZ)V

    .line 36380
    :cond_2
    return-void
.end method
