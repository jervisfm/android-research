.class public final Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private hide_:I

.field private level_:Ljava/lang/Object;

.field private text_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 35195
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 35363
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35427
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35463
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35499
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35196
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->maybeForceBuilderInitialization()V

    .line 35197
    return-void
.end method

.method static synthetic access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35190
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$44500()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35238
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    .line 35239
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35240
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 35243
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35202
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;-><init>()V

    return-object v0
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 35200
    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 2

    .prologue
    .line 35229
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    .line 35230
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35231
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 35233
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 5

    .prologue
    .line 35247
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 35248
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35249
    const/4 v2, 0x0

    .line 35250
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 35251
    or-int/lit8 v2, v2, 0x1

    .line 35253
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$44702(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35254
    and-int/lit8 v3, v1, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 35255
    or-int/lit8 v2, v2, 0x2

    .line 35257
    :cond_1
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$44802(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;I)I

    .line 35258
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 35259
    or-int/lit8 v2, v2, 0x4

    .line 35261
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$44902(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35262
    and-int/lit8 v3, v1, 0x8

    const/16 v4, 0x8

    if-ne v3, v4, :cond_3

    .line 35263
    or-int/lit8 v2, v2, 0x8

    .line 35265
    :cond_3
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$45002(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35266
    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    .line 35267
    or-int/lit8 v1, v2, 0x10

    .line 35269
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$45102(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35270
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->access$45202(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;I)I

    .line 35271
    return-object v0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35206
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 35207
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35208
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35209
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    .line 35210
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35211
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35212
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35213
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35214
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35215
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35216
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35217
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35399
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35401
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35402
    return-object p0
.end method

.method public clearHide()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35420
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35421
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    .line 35423
    return-object p0
.end method

.method public clearLevel()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35487
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35488
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getLevel()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35490
    return-object p0
.end method

.method public clearText()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35451
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35452
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35454
    return-object p0
.end method

.method public clearType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35516
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x11

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35517
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35519
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 2

    .prologue
    .line 35221
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 35368
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 35190
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1

    .prologue
    .line 35225
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public getHide()I
    .locals 1

    .prologue
    .line 35411
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    return v0
.end method

.method public getLevel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35468
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35469
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 35470
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 35471
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35474
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35432
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35433
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 35434
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 35435
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35438
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 1

    .prologue
    .line 35504
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 35365
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHide()Z
    .locals 2

    .prologue
    .line 35408
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLevel()Z
    .locals 2

    .prologue
    .line 35465
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 35429
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 35501
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35295
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 35303
    :goto_0
    return v0

    .line 35299
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 35301
    goto :goto_0

    .line 35303
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 35387
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 35389
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35395
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35396
    return-object p0

    .line 35392
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35190
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35190
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35190
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35311
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 35312
    sparse-switch v0, :sswitch_data_0

    .line 35317
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 35319
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 35315
    goto :goto_1

    .line 35324
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 35325
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 35326
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 35328
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 35329
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    goto :goto_0

    .line 35333
    :sswitch_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35334
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readInt32()I

    move-result v0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    goto :goto_0

    .line 35338
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35339
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    goto :goto_0

    .line 35343
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35344
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    goto :goto_0

    .line 35348
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readEnum()I

    move-result v0

    .line 35349
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    move-result-object v0

    .line 35350
    if-eqz v0, :cond_0

    .line 35351
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35352
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    goto :goto_0

    .line 35312
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35275
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 35291
    :goto_0
    return-object v0

    .line 35276
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35277
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    .line 35279
    :cond_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasHide()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 35280
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getHide()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->setHide(I)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    .line 35282
    :cond_2
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasText()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 35283
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    .line 35285
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasLevel()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 35286
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getLevel()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->setLevel(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    .line 35288
    :cond_4
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasType()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 35289
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->setType(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    :cond_5
    move-object v0, p0

    .line 35291
    goto :goto_0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35381
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35383
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35384
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35371
    if-nez p1, :cond_0

    .line 35372
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35374
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35376
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35377
    return-object p0
.end method

.method public setHide(I)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35414
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35415
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->hide_:I

    .line 35417
    return-object p0
.end method

.method public setLevel(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35478
    if-nez p1, :cond_0

    .line 35479
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35481
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35482
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35484
    return-object p0
.end method

.method setLevel(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 35493
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35494
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->level_:Ljava/lang/Object;

    .line 35496
    return-void
.end method

.method public setText(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35442
    if-nez p1, :cond_0

    .line 35443
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35445
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35446
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35448
    return-object p0
.end method

.method setText(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 35457
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35458
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->text_:Ljava/lang/Object;

    .line 35460
    return-void
.end method

.method public setType(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35507
    if-nez p1, :cond_0

    .line 35508
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35510
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->bitField0_:I

    .line 35511
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35513
    return-object p0
.end method
