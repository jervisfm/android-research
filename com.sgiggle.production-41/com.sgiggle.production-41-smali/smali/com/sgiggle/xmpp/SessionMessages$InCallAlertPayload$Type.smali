.class public final enum Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
.super Ljava/lang/Enum;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/Internal$EnumLite;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;",
        ">;",
        "Lcom/google/protobuf/Internal$EnumLite;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type; = null

.field public static final enum ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type; = null

.field public static final ANIMATION_VALUE:I = 0x3

.field public static final enum DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type; = null

.field public static final DEFAULT_VALUE:I = 0x0

.field public static final enum PASS_THROUGH:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type; = null

.field public static final PASS_THROUGH_VALUE:I = 0x1

.field public static final enum SWITCH_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type; = null

.field public static final SWITCH_CAMERA_VALUE:I = 0x2

.field private static internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34897
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 34898
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    const-string v1, "PASS_THROUGH"

    invoke-direct {v0, v1, v3, v3, v3}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->PASS_THROUGH:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 34899
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    const-string v1, "SWITCH_CAMERA"

    invoke-direct {v0, v1, v4, v4, v4}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->SWITCH_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 34900
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    const-string v1, "ANIMATION"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 34895
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->PASS_THROUGH:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->SWITCH_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 34926
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type$1;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type$1;-><init>()V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 34935
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34936
    iput p4, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->value:I

    .line 34937
    return-void
.end method

.method public static internalGetValueMap()Lcom/google/protobuf/Internal$EnumLiteMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/protobuf/Internal$EnumLiteMap",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34923
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->internalValueMap:Lcom/google/protobuf/Internal$EnumLiteMap;

    return-object v0
.end method

.method public static valueOf(I)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 34912
    packed-switch p0, :pswitch_data_0

    .line 34917
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 34913
    :pswitch_0
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    goto :goto_0

    .line 34914
    :pswitch_1
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->PASS_THROUGH:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    goto :goto_0

    .line 34915
    :pswitch_2
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->SWITCH_CAMERA:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    goto :goto_0

    .line 34916
    :pswitch_3
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->ANIMATION:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    goto :goto_0

    .line 34912
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 1
    .parameter

    .prologue
    .line 34895
    const-class v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    return-object v0
.end method

.method public static values()[Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 1

    .prologue
    .line 34895
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->$VALUES:[Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    invoke-virtual {v0}, [Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    return-object v0
.end method


# virtual methods
.method public final getNumber()I
    .locals 1

    .prologue
    .line 34909
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->value:I

    return v0
.end method
