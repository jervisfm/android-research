.class public final Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InCallAlertPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;,
        Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final HIDE_FIELD_NUMBER:I = 0x2

.field public static final LEVEL_FIELD_NUMBER:I = 0x4

.field public static final TEXT_FIELD_NUMBER:I = 0x3

.field public static final TYPE_FIELD_NUMBER:I = 0x5

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private hide_:I

.field private level_:Ljava/lang/Object;

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I

.field private text_:Ljava/lang/Object;

.field private type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35526
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    .line 35527
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->initFields()V

    .line 35528
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 34882
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 35044
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    .line 35081
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedSerializedSize:I

    .line 34883
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 34884
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 35044
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    .line 35081
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedSerializedSize:I

    .line 34884
    return-void
.end method

.method static synthetic access$44702(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$44802(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I

    return p1
.end method

.method static synthetic access$44902(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$45002(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$45102(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    return-object p1
.end method

.method static synthetic access$45202(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34877
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    return p1
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1

    .prologue
    .line 34888
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    return-object v0
.end method

.method private getLevelBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 35016
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    .line 35017
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35018
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 35020
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    .line 35023
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private getTextBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 34984
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    .line 34985
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34986
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 34988
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    .line 34991
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 35038
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 35039
    const/4 v0, 0x0

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I

    .line 35040
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    .line 35041
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    .line 35042
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->DEFAULT:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    .line 35043
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35183
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44500()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 35186
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35152
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    .line 35153
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35154
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    .line 35156
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35163
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    .line 35164
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 35165
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    .line 35167
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35119
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35125
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35173
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35179
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35141
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35147
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35130
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 35136
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;->access$44400(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 34950
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 34877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;
    .locals 1

    .prologue
    .line 34892
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;

    return-object v0
.end method

.method public getHide()I
    .locals 1

    .prologue
    .line 34960
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I

    return v0
.end method

.method public getLevel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 35002
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    .line 35003
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 35004
    check-cast v0, Ljava/lang/String;

    .line 35012
    :goto_0
    return-object v0

    .line 35006
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 35008
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 35009
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 35010
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->level_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 35012
    goto :goto_0
.end method

.method public getSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 35083
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedSerializedSize:I

    .line 35084
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 35108
    :goto_0
    return v0

    .line 35086
    :cond_0
    const/4 v0, 0x0

    .line 35087
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    .line 35088
    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v2, v1}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35091
    :cond_1
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v3, :cond_2

    .line 35092
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I

    invoke-static {v3, v1}, Lcom/google/protobuf/CodedOutputStream;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 35095
    :cond_2
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v4, :cond_3

    .line 35096
    const/4 v1, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35099
    :cond_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x8

    const/16 v2, 0x8

    if-ne v1, v2, :cond_4

    .line 35100
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getLevelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 35103
    :cond_4
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x10

    const/16 v2, 0x10

    if-ne v1, v2, :cond_5

    .line 35104
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    invoke-virtual {v2}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->getNumber()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/CodedOutputStream;->computeEnumSize(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 35107
    :cond_5
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedSerializedSize:I

    goto :goto_0
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 34970
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    .line 34971
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 34972
    check-cast v0, Ljava/lang/String;

    .line 34980
    :goto_0
    return-object v0

    .line 34974
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 34976
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 34977
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 34978
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->text_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 34980
    goto :goto_0
.end method

.method public getType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
    .locals 1

    .prologue
    .line 35034
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 34947
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHide()Z
    .locals 2

    .prologue
    .line 34957
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLevel()Z
    .locals 2

    .prologue
    .line 34999
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasText()Z
    .locals 2

    .prologue
    .line 34967
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasType()Z
    .locals 2

    .prologue
    .line 35031
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35046
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    .line 35047
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 35058
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 35047
    goto :goto_0

    .line 35049
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 35050
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35051
    goto :goto_0

    .line 35053
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 35054
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 35055
    goto :goto_0

    .line 35057
    :cond_3
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 35058
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35184
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 34877
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;
    .locals 1

    .prologue
    .line 35188
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;)Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 35113
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 35063
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getSerializedSize()I

    .line 35064
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 35065
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 35067
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_1

    .line 35068
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->hide_:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeInt32(II)V

    .line 35070
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_2

    .line 35071
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getTextBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35073
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 35074
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->getLevelBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 35076
    :cond_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_4

    .line 35077
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload;->type_:Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;->getNumber()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeEnum(II)V

    .line 35079
    :cond_4
    return-void
.end method
