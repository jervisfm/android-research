.class public interface abstract Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayloadOrBuilder;
.super Ljava/lang/Object;
.source "SessionMessages.java"

# interfaces
.implements Lcom/google/protobuf/MessageLiteOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InCallAlertPayloadOrBuilder"
.end annotation


# virtual methods
.method public abstract getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
.end method

.method public abstract getHide()I
.end method

.method public abstract getLevel()Ljava/lang/String;
.end method

.method public abstract getText()Ljava/lang/String;
.end method

.method public abstract getType()Lcom/sgiggle/xmpp/SessionMessages$InCallAlertPayload$Type;
.end method

.method public abstract hasBase()Z
.end method

.method public abstract hasHide()Z
.end method

.method public abstract hasLevel()Z
.end method

.method public abstract hasText()Z
.end method

.method public abstract hasType()Z
.end method
