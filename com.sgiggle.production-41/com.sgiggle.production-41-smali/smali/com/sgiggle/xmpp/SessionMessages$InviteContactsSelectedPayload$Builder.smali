.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
.super Lcom/google/protobuf/GeneratedMessageLite$Builder;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/protobuf/GeneratedMessageLite$Builder",
        "<",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;",
        ">;",
        "Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayloadOrBuilder;"
    }
.end annotation


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private hintMsg_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 17958
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;-><init>()V

    .line 18123
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18166
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 18255
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18291
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 17959
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->maybeForceBuilderInitialization()V

    .line 17960
    return-void
.end method

.method static synthetic access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17953
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$22700()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method private buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17999
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    .line 18000
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 18001
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/UninitializedMessageException;->asInvalidProtocolBufferException()Lcom/google/protobuf/InvalidProtocolBufferException;

    move-result-object v0

    throw v0

    .line 18004
    :cond_0
    return-object v0
.end method

.method private static create()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17965
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;-><init>()V

    return-object v0
.end method

.method private ensureInviteeIsMutable()V
    .locals 2

    .prologue
    .line 18169
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 18170
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 18171
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18173
    :cond_0
    return-void
.end method

.method private maybeForceBuilderInitialization()V
    .locals 0

    .prologue
    .line 17963
    return-void
.end method


# virtual methods
.method public addAllInvitee(Ljava/lang/Iterable;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;)",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;"
        }
    .end annotation

    .prologue
    .line 18236
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18237
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->addAll(Ljava/lang/Iterable;Ljava/util/Collection;)V

    .line 18239
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 18229
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18230
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 18232
    return-object p0
.end method

.method public addInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 18212
    if-nez p2, :cond_0

    .line 18213
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18215
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18216
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 18218
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 18222
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18223
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18225
    return-object p0
.end method

.method public addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18202
    if-nez p1, :cond_0

    .line 18203
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18205
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18206
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 18208
    return-object p0
.end method

.method public bridge synthetic build()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public build()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 2

    .prologue
    .line 17990
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    .line 17991
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    .line 17992
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->newUninitializedMessageException(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/UninitializedMessageException;

    move-result-object v0

    throw v0

    .line 17994
    :cond_0
    return-object v0
.end method

.method public bridge synthetic buildPartial()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 5

    .prologue
    .line 18008
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V

    .line 18009
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18010
    const/4 v2, 0x0

    .line 18011
    and-int/lit8 v3, v1, 0x1

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 18012
    or-int/lit8 v2, v2, 0x1

    .line 18014
    :cond_0
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$22902(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18015
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, 0x2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 18016
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 18017
    iget v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v3, v3, -0x3

    iput v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18019
    :cond_1
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23002(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/util/List;)Ljava/util/List;

    .line 18020
    and-int/lit8 v3, v1, 0x4

    const/4 v4, 0x4

    if-ne v3, v4, :cond_2

    .line 18021
    or-int/lit8 v2, v2, 0x2

    .line 18023
    :cond_2
    iget-object v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;
    invoke-static {v0, v3}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23102(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18024
    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_3

    .line 18025
    or-int/lit8 v1, v2, 0x4

    .line 18027
    :goto_0
    iget-object v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;
    invoke-static {v0, v2}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23202(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18028
    #setter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I
    invoke-static {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23302(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;I)I

    .line 18029
    return-object v0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clear()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clear()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clear()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17969
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->clear()Lcom/google/protobuf/GeneratedMessageLite$Builder;

    .line 17970
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17971
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 17972
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 17973
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 17974
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 17975
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 17976
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 17977
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 17978
    return-object p0
.end method

.method public clearBase()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 18159
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18161
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18162
    return-object p0
.end method

.method public clearCorrelationtoken()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 18279
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18280
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18282
    return-object p0
.end method

.method public clearHintMsg()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 18315
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18316
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18318
    return-object p0
.end method

.method public clearInvitee()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 18242
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 18243
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18245
    return-object p0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2

    .prologue
    .line 17982
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->clone()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 18128
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18260
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18261
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 18262
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 18263
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18266
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/GeneratedMessageLite;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17953
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1

    .prologue
    .line 17986
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 18296
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18297
    instance-of v1, v0, Ljava/lang/String;

    if-nez v1, :cond_0

    .line 18298
    check-cast v0, Lcom/google/protobuf/ByteString;

    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v0

    .line 18299
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18302
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 18182
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 18179
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18176
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 18125
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 18257
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 18293
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18057
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hasBase()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 18071
    :goto_0
    return v0

    .line 18061
    :cond_0
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 18063
    goto :goto_0

    :cond_1
    move v0, v2

    .line 18065
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 18066
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 18068
    goto :goto_0

    .line 18065
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 18071
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 18147
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 18149
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18155
    :goto_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18156
    return-object p0

    .line 18152
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    goto :goto_0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17953
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/GeneratedMessageLite;)Lcom/google/protobuf/GeneratedMessageLite$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17953
    check-cast p1, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    invoke-virtual {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite$Builder;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17953
    invoke-virtual {p0, p1, p2}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 18079
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readTag()I

    move-result v0

    .line 18080
    sparse-switch v0, :sswitch_data_0

    .line 18085
    invoke-virtual {p0, p1, p2, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->parseUnknownField(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p0

    .line 18087
    :goto_1
    return-object v0

    :sswitch_0
    move-object v0, p0

    .line 18083
    goto :goto_1

    .line 18092
    :sswitch_1
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    move-result-object v0

    .line 18093
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hasBase()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18094
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;

    .line 18096
    :cond_1
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 18097
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    goto :goto_0

    .line 18101
    :sswitch_2
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;

    move-result-object v0

    .line 18102
    invoke-virtual {p1, v0, p2}, Lcom/google/protobuf/CodedInputStream;->readMessage(Lcom/google/protobuf/MessageLite$Builder;Lcom/google/protobuf/ExtensionRegistryLite;)V

    .line 18103
    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->buildPartial()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->addInvitee(Lcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    goto :goto_0

    .line 18107
    :sswitch_3
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18108
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    goto :goto_0

    .line 18112
    :sswitch_4
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18113
    invoke-virtual {p1}, Lcom/google/protobuf/CodedInputStream;->readBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    goto :goto_0

    .line 18080
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter

    .prologue
    .line 18033
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    if-ne p1, v0, :cond_0

    move-object v0, p0

    .line 18053
    :goto_0
    return-object v0

    .line 18034
    :cond_0
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hasBase()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 18035
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    .line 18037
    :cond_1
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23000(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 18038
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 18039
    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23000(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    .line 18040
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18047
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hasCorrelationtoken()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 18048
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getCorrelationtoken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    .line 18050
    :cond_3
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hasHintMsg()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 18051
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getHintMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    :cond_4
    move-object v0, p0

    .line 18053
    goto :goto_0

    .line 18042
    :cond_5
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18043
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    #getter for: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;
    invoke-static {p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->access$23000(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method public removeInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18248
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18249
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 18251
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18141
    invoke-virtual {p1}, Lcom/sgiggle/xmpp/SessionMessages$Base$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18143
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18144
    return-object p0
.end method

.method public setBase(Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18131
    if-nez p1, :cond_0

    .line 18132
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18134
    :cond_0
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 18136
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18137
    return-object p0
.end method

.method public setCorrelationtoken(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18270
    if-nez p1, :cond_0

    .line 18271
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18273
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18274
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18276
    return-object p0
.end method

.method setCorrelationtoken(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 18285
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18286
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->correlationtoken_:Ljava/lang/Object;

    .line 18288
    return-void
.end method

.method public setHintMsg(Ljava/lang/String;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 18306
    if-nez p1, :cond_0

    .line 18307
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18309
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18310
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18312
    return-object p0
.end method

.method setHintMsg(Lcom/google/protobuf/ByteString;)V
    .locals 1
    .parameter

    .prologue
    .line 18321
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->bitField0_:I

    .line 18322
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->hintMsg_:Ljava/lang/Object;

    .line 18324
    return-void
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 18196
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18197
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-virtual {p2}, Lcom/sgiggle/xmpp/SessionMessages$Invitee$Builder;->build()Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 18199
    return-object p0
.end method

.method public setInvitee(ILcom/sgiggle/xmpp/SessionMessages$Invitee;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 18186
    if-nez p2, :cond_0

    .line 18187
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 18189
    :cond_0
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->ensureInviteeIsMutable()V

    .line 18190
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 18192
    return-object p0
.end method
