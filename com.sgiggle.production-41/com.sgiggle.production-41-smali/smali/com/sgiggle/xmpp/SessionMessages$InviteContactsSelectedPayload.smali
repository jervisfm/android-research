.class public final Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SessionMessages.java"

# interfaces
.implements Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayloadOrBuilder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sgiggle/xmpp/SessionMessages;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InviteContactsSelectedPayload"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    }
.end annotation


# static fields
.field public static final BASE_FIELD_NUMBER:I = 0x1

.field public static final CORRELATIONTOKEN_FIELD_NUMBER:I = 0x3

.field public static final HINT_MSG_FIELD_NUMBER:I = 0x4

.field public static final INVITEE_FIELD_NUMBER:I = 0x2

.field private static final defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;


# instance fields
.field private base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

.field private bitField0_:I

.field private correlationtoken_:Ljava/lang/Object;

.field private hintMsg_:Ljava/lang/Object;

.field private invitee_:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation
.end field

.field private memoizedIsInitialized:B

.field private memoizedSerializedSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18330
    new-instance v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;-><init>(Z)V

    sput-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    .line 18331
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    invoke-direct {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->initFields()V

    .line 18332
    return-void
.end method

.method private constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 17693
    invoke-direct {p0, p1}, Lcom/google/protobuf/GeneratedMessageLite;-><init>(Lcom/google/protobuf/GeneratedMessageLite$Builder;)V

    .line 17808
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    .line 17848
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedSerializedSize:I

    .line 17694
    return-void
.end method

.method synthetic constructor <init>(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;Lcom/sgiggle/xmpp/SessionMessages$1;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    invoke-direct {p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;-><init>(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)V

    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, -0x1

    .line 17695
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    .line 17808
    iput-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    .line 17848
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedSerializedSize:I

    .line 17695
    return-void
.end method

.method static synthetic access$22902(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Lcom/sgiggle/xmpp/SessionMessages$Base;)Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object p1
.end method

.method static synthetic access$23000(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 17688
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$23002(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$23102(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$23202(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    iput-object p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$23302(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17688
    iput p1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    return p1
.end method

.method private getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 17759
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    .line 17760
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17761
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 17763
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    .line 17766
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method public static getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1

    .prologue
    .line 17699
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    return-object v0
.end method

.method private getHintMsgBytes()Lcom/google/protobuf/ByteString;
    .locals 2

    .prologue
    .line 17791
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 17792
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17793
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/ByteString;->copyFromUtf8(Ljava/lang/String;)Lcom/google/protobuf/ByteString;

    move-result-object v0

    .line 17795
    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 17798
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    goto :goto_0
.end method

.method private initFields()V
    .locals 1

    .prologue
    .line 17803
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$Base;->getDefaultInstance()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    .line 17804
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    .line 17805
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    .line 17806
    const-string v0, ""

    iput-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 17807
    return-void
.end method

.method public static newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17946
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->create()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22700()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1
    .parameter

    .prologue
    .line 17949
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17915
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    .line 17916
    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17917
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    .line 17919
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17926
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    .line 17927
    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeDelimitedFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 17928
    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    .line 17930
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17882
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17888
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/ByteString;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17936
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17942
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17904
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Ljava/io/InputStream;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 17910
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom(Ljava/io/InputStream;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([B)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17893
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom([B)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public static parseFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/InvalidProtocolBufferException;
        }
    .end annotation

    .prologue
    .line 17899
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->mergeFrom([BLcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/AbstractMessageLite$Builder;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    #calls: Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->buildParsed()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    invoke-static {v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;->access$22600(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;
    .locals 1

    .prologue
    .line 17714
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    return-object v0
.end method

.method public getCorrelationtoken()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17745
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    .line 17746
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17747
    check-cast v0, Ljava/lang/String;

    .line 17755
    :goto_0
    return-object v0

    .line 17749
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 17751
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 17752
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17753
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->correlationtoken_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 17755
    goto :goto_0
.end method

.method public bridge synthetic getDefaultInstanceForType()Lcom/google/protobuf/MessageLite;
    .locals 1

    .prologue
    .line 17688
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultInstanceForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;
    .locals 1

    .prologue
    .line 17703
    sget-object v0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->defaultInstance:Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;

    return-object v0
.end method

.method public getHintMsg()Ljava/lang/String;
    .locals 2

    .prologue
    .line 17777
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    .line 17778
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 17779
    check-cast v0, Ljava/lang/String;

    .line 17787
    :goto_0
    return-object v0

    .line 17781
    :cond_0
    check-cast v0, Lcom/google/protobuf/ByteString;

    .line 17783
    invoke-virtual {v0}, Lcom/google/protobuf/ByteString;->toStringUtf8()Ljava/lang/String;

    move-result-object v1

    .line 17784
    invoke-static {v0}, Lcom/google/protobuf/Internal;->isValidUtf8(Lcom/google/protobuf/ByteString;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 17785
    iput-object v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hintMsg_:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    .line 17787
    goto :goto_0
.end method

.method public getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;
    .locals 1
    .parameter

    .prologue
    .line 17731
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    return-object v0
.end method

.method public getInviteeCount()I
    .locals 1

    .prologue
    .line 17728
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getInviteeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sgiggle/xmpp/SessionMessages$Invitee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17721
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getInviteeOrBuilder(I)Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;
    .locals 1
    .parameter

    .prologue
    .line 17735
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;

    return-object v0
.end method

.method public getInviteeOrBuilderList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/sgiggle/xmpp/SessionMessages$InviteeOrBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17725
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    return-object v0
.end method

.method public getSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17850
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedSerializedSize:I

    .line 17851
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 17871
    :goto_0
    return v0

    .line 17854
    :cond_0
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_4

    .line 17855
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-static {v3, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    :goto_1
    move v1, v2

    move v2, v0

    .line 17858
    :goto_2
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 17859
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-static {v4, v0}, Lcom/google/protobuf/CodedOutputStream;->computeMessageSize(ILcom/google/protobuf/MessageLite;)I

    move-result v0

    add-int/2addr v0, v2

    .line 17858
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_2

    .line 17862
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_3

    .line 17863
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v0

    add-int/2addr v0, v2

    .line 17866
    :goto_3
    iget v1, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v5, :cond_2

    .line 17867
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-static {v5, v1}, Lcom/google/protobuf/CodedOutputStream;->computeBytesSize(ILcom/google/protobuf/ByteString;)I

    move-result v1

    add-int/2addr v0, v1

    .line 17870
    :cond_2
    iput v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedSerializedSize:I

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public hasBase()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 17711
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCorrelationtoken()Z
    .locals 2

    .prologue
    .line 17742
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHintMsg()Z
    .locals 2

    .prologue
    .line 17774
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isInitialized()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17810
    iget-byte v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    .line 17811
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    if-ne v0, v3, :cond_0

    move v0, v3

    .line 17828
    :goto_0
    return v0

    :cond_0
    move v0, v2

    .line 17811
    goto :goto_0

    .line 17813
    :cond_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->hasBase()Z

    move-result v0

    if-nez v0, :cond_2

    .line 17814
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17815
    goto :goto_0

    .line 17817
    :cond_2
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getBase()Lcom/sgiggle/xmpp/SessionMessages$Base;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sgiggle/xmpp/SessionMessages$Base;->isInitialized()Z

    move-result v0

    if-nez v0, :cond_3

    .line 17818
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17819
    goto :goto_0

    :cond_3
    move v0, v2

    .line 17821
    :goto_1
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getInviteeCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 17822
    invoke-virtual {p0, v0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getInvitee(I)Lcom/sgiggle/xmpp/SessionMessages$Invitee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sgiggle/xmpp/SessionMessages$Invitee;->isInitialized()Z

    move-result v1

    if-nez v1, :cond_4

    .line 17823
    iput-byte v2, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    move v0, v2

    .line 17824
    goto :goto_0

    .line 17821
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 17827
    :cond_5
    iput-byte v3, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->memoizedIsInitialized:B

    move v0, v3

    .line 17828
    goto :goto_0
.end method

.method public bridge synthetic newBuilderForType()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17688
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilderForType()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17947
    invoke-static {}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic toBuilder()Lcom/google/protobuf/MessageLite$Builder;
    .locals 1

    .prologue
    .line 17688
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toBuilder()Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;
    .locals 1

    .prologue
    .line 17951
    invoke-static {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->newBuilder(Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;)Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload$Builder;

    move-result-object v0

    return-object v0
.end method

.method protected writeReplace()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 17876
    invoke-super {p0}, Lcom/google/protobuf/GeneratedMessageLite;->writeReplace()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/CodedOutputStream;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 17833
    invoke-virtual {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getSerializedSize()I

    .line 17834
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 17835
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->base_:Lcom/sgiggle/xmpp/SessionMessages$Base;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 17837
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 17838
    iget-object v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->invitee_:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/MessageLite;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/CodedOutputStream;->writeMessage(ILcom/google/protobuf/MessageLite;)V

    .line 17837
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 17840
    :cond_1
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_2

    .line 17841
    const/4 v0, 0x3

    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getCorrelationtokenBytes()Lcom/google/protobuf/ByteString;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17843
    :cond_2
    iget v0, p0, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->bitField0_:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v3, :cond_3

    .line 17844
    invoke-direct {p0}, Lcom/sgiggle/xmpp/SessionMessages$InviteContactsSelectedPayload;->getHintMsgBytes()Lcom/google/protobuf/ByteString;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/CodedOutputStream;->writeBytes(ILcom/google/protobuf/ByteString;)V

    .line 17846
    :cond_3
    return-void
.end method
